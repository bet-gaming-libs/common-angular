(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('rxjs'), require('@angular/common'), require('big.js'), require('@angular/core'), require('earnbet-common'), require('earnbet-common-front-end')) :
    typeof define === 'function' && define.amd ? define('earnbet-common-angular', ['exports', 'rxjs', '@angular/common', 'big.js', '@angular/core', 'earnbet-common', 'earnbet-common-front-end'], factory) :
    (factory((global['earnbet-common-angular'] = {}),global.rxjs,global.ng.common,global.big_js,global.ng.core,global.earnbetCommon,global.earnbetCommonFrontEnd));
}(this, (function (exports,rxjs,common,big_js,core,earnbetCommon,earnbetCommonFrontEnd) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (b.hasOwnProperty(p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            } }
            function rejected(value) { try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }
    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function () { if (t[0] & 1)
                throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f)
                throw new TypeError("Generator is already executing.");
            while (_)
                try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                        return t;
                    if (y = 0, t)
                        op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0:
                        case 1:
                            t = op;
                            break;
                        case 4:
                            _.label++;
                            return { value: op[1], done: false };
                        case 5:
                            _.label++;
                            y = op[1];
                            op = [0];
                            continue;
                        case 7:
                            op = _.ops.pop();
                            _.trys.pop();
                            continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                _ = 0;
                                continue;
                            }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                _.label = op[1];
                                break;
                            }
                            if (op[0] === 6 && _.label < t[1]) {
                                _.label = t[1];
                                t = op;
                                break;
                            }
                            if (t && _.label < t[2]) {
                                _.label = t[2];
                                _.ops.push(op);
                                break;
                            }
                            if (t[2])
                                _.ops.pop();
                            _.trys.pop();
                            continue;
                    }
                    op = body.call(thisArg, _);
                }
                catch (e) {
                    op = [6, e];
                    y = 0;
                }
                finally {
                    f = t = 0;
                }
            if (op[0] & 5)
                throw op[1];
            return { value: op[0] ? op[1] : void 0, done: true };
        }
    }
    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m)
            return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length)
                    o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/models/business-rules/tokens.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var TokenSymbol = {
        BTC: "BTC",
        ETH: "ETH",
        EOS: "EOS",
        BET: "BET",
        LTC: "LTC",
        XRP: "XRP",
        BCH: "BCH",
        BNB: "BNB",
        WAX: "WAX",
        TRX: "TRX",
        LINK: "LINK",
        DAI: "DAI",
        USDC: "USDC",
        USDT: "USDT",
        FUN: "FUN",
    };

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/base/game-component-base.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var INDEX_OF_MAX_CHIP = 7;
    /**
     * @abstract
     */
    var GameComponentBase = /** @class */ (function () {
        function GameComponentBase(app, googleAnalytics, soundPlayer) {
            this.app = app;
            this.googleAnalytics = googleAnalytics;
            this.soundPlayer = soundPlayer;
            this.autoBetNonce = 0;
            this._isJackpotBet = false;
            this.selectedChip = 1;
            this.showTooltip = false;
            this.processBetStyle = false;
        }
        /**
         * @return {?}
         */
        GameComponentBase.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this.app.scrollToTopOfPage();
                this.app.settings.setListener(this);
                if (this.selectedToken.symbol == 'BET') {
                    this.settings.selectCurrency('EOS');
                }
            };
        /**
         * @param {?} i
         * @return {?}
         */
        GameComponentBase.prototype.selectChip = /**
         * @param {?} i
         * @return {?}
         */
            function (i) {
                this.selectedChip = i;
                this.soundPlayer.play('chipPlace');
                this.onChipSelected();
            };
        /**
         * @protected
         * @return {?}
         */
        GameComponentBase.prototype.onChipSelected = /**
         * @protected
         * @return {?}
         */
            function () { };
        /**
         * @return {?}
         */
        GameComponentBase.prototype.toggleTooltip = /**
         * @return {?}
         */
            function () {
                this.showTooltip = !this.showTooltip;
            };
        /**
         * @return {?}
         */
        GameComponentBase.prototype.documentClick = /**
         * @return {?}
         */
            function () {
                this.showTooltip = false;
            };
        Object.defineProperty(GameComponentBase.prototype, "isJackpotBet", {
            get: /**
             * @return {?}
             */ function () {
                return !this.isJackpotDisabled &&
                    this._isJackpotBet;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                if (this.isJackpotDisabled) {
                    return;
                }
                this._isJackpotBet = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "isJackpotDisabled", {
            get: /**
             * @return {?}
             */ function () {
                /** @type {?} */
                var amount = Number(this.betAmount.decimal);
                return amount < this.selectedToken.jackpot.minimumBet;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} isAutoBet
         * @return {?}
         */
        GameComponentBase.prototype.getSeed = /**
         * @param {?} isAutoBet
         * @return {?}
         */
            function (isAutoBet) {
                // seed for randomness
                /** @type {?} */
                var seed = this.state.seedForRandomness +
                    // add nonce for auto-bet to fairness seed
                    (isAutoBet || true ?
                        ',' + (++this.autoBetNonce) :
                        '');
                return seed;
            };
        /**
         * @param {?} gameName
         * @param {?} betAmount
         * @param {?} txnId
         * @param {?} isAutoBet
         * @return {?}
         */
        GameComponentBase.prototype.trackBetWithGoogleAnalytics = /**
         * @param {?} gameName
         * @param {?} betAmount
         * @param {?} txnId
         * @param {?} isAutoBet
         * @return {?}
         */
            function (gameName, betAmount, txnId, isAutoBet) {
                /** @type {?} */
                var price = this.currency.getPrice(this.selectedToken.symbol);
                /** @type {?} */
                var amountInUSD = price ?
                    (price * Number(betAmount)).toFixed(2) :
                    undefined;
                if (amountInUSD) {
                    this.googleAnalytics.addTransaction(txnId, gameName, this.selectedToken.symbol, amountInUSD, isAutoBet, this.isJackpotBet);
                }
            };
        /**
         * @protected
         * @param {?} result
         * @return {?}
         */
        GameComponentBase.prototype.updateBalances = /**
         * @protected
         * @param {?} result
         * @return {?}
         */
            function (result) {
                return __awaiter(this, void 0, void 0, function () {
                    var bet, profit;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                bet = Number(result.amount) /
                                    this.selectedToken.betTokenAirDropRate;
                                profit = result.profit;
                                if (result.isJackpotBet) {
                                    profit -= Number(this.selectedToken.jackpot.ticketPrice);
                                }
                                /***/
                                this.addToBalance = {
                                    main: profit,
                                    bet: bet
                                };
                                // to animate balance in header
                                this.app.state.onAddToBalance(true, profit);
                                return [4 /*yield*/, earnbetCommon.sleep(500)];
                            case 1:
                                _a.sent();
                                /*** UPDATE TOKEN BALANCES ***/
                                // update main token balance
                                this.wallet.addToBalance(this.selectedToken.symbol, profit);
                                // update BET token balance
                                this.wallet.addToBalance(TokenSymbol.BET, bet);
                                /***/
                                return [4 /*yield*/, earnbetCommon.sleep(1500)];
                            case 2:
                                /***/
                                _a.sent();
                                this.addToBalance = undefined;
                                this.app.state.onAddToBalance(false);
                                return [2 /*return*/];
                        }
                    });
                });
            };
        Object.defineProperty(GameComponentBase.prototype, "tooltipBet", {
            // onTooltipShowBet(){
            //     document.getElementById("tooltipBet").setAttribute('data-tooltip', 'aaa');
            // }
            get: 
            // onTooltipShowBet(){
            //     document.getElementById("tooltipBet").setAttribute('data-tooltip', 'aaa');
            // }
            /**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var tokenAlt = this.selectedToken.symbol;
                /** @type {?} */
                var airDrop = this.selectedToken.betTokenAirDropRate;
                return this.settings.translation.tooltipDividends("1", airDrop + " " + tokenAlt);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "jackpotToolTip", {
            get: /**
             * @return {?}
             */ function () {
                /** @type {?} */
                var token = this.selectedToken;
                /** @type {?} */
                var jackpot = token.jackpot;
                return this.settings.translation.clickThisBoxToReceive1JackpotSpinForPrice(Number(jackpot.ticketPrice) + " " + token.symbol);
                // Currently not using a separate minimum bet for jackpot, 
                // it is currently the same as the standard minimum bet
                //"Bet must be at least "+jackpot.minimumBet+" "+token.symbol+" to enter.";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "isInsufficientFunds", {
            get: /**
             * @return {?}
             */ function () {
                return new big_js.Big(this.currencyBalance).lt(this.selectedToken.minimumBetAmount) ||
                    new big_js.Big(this.betAmount.decimal).gt(this.currencyBalance);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "isMaxChipSelected", {
            get: /**
             * @return {?}
             */ function () {
                return this.selectedChip == INDEX_OF_MAX_CHIP;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "currencyBalance", {
            get: /**
             * @return {?}
             */ function () {
                return this.settings.selectedTokenBalance;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "chips", {
            get: /**
             * @return {?}
             */ function () {
                return this.selectedToken.chips;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "selectedTokenPrice", {
            get: /**
             * @return {?}
             */ function () {
                return this.currency.getPrice(this.selectedToken.symbol);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "currency", {
            get: /**
             * @return {?}
             */ function () {
                return this.app.currency;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "selectedToken", {
            get: /**
             * @return {?}
             */ function () {
                return this.settings.selectedToken;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "wallet", {
            get: /**
             * @return {?}
             */ function () {
                return this.settings.wallet;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "state", {
            get: /**
             * @return {?}
             */ function () {
                return this.app.state;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "translation", {
            get: /**
             * @return {?}
             */ function () {
                return this.settings.translation;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "settings", {
            get: /**
             * @return {?}
             */ function () {
                return this.app.settings;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "alert", {
            get: /**
             * @return {?}
             */ function () {
                return this.app.alert;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameComponentBase.prototype, "bankroll", {
            get: /**
             * @return {?}
             */ function () {
                return this.app.bankroll;
            },
            enumerable: true,
            configurable: true
        });
        GameComponentBase.propDecorators = {
            documentClick: [{ type: core.HostListener, args: ['document:click', [],] }]
        };
        return GameComponentBase;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/eos/eos-error-parser.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var EosErrorParser = /** @class */ (function () {
        function EosErrorParser(data) {
            this._message = data.message;
            try {
                data = JSON.parse(data);
                if (data.error && data.error.details) {
                    this._message = data.error.details[0].message;
                }
            }
            catch (error) {
            }
        }
        Object.defineProperty(EosErrorParser.prototype, "isIncorrectNonce", {
            get: /**
             * @return {?}
             */ function () {
                return this.message ==
                    'assertion failure with message: Incorrect nonce.';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EosErrorParser.prototype, "message", {
            get: /**
             * @return {?}
             */ function () {
                return this._message;
            },
            enumerable: true,
            configurable: true
        });
        return EosErrorParser;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/game/card-controller.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var SUITS = ['hearts', 'diamonds', 'spades', 'clubs'];
    var CardController = /** @class */ (function () {
        function CardController(soundPlayer) {
            this.soundPlayer = soundPlayer;
            this._dealt = false;
            this._flipped = false;
            this._isRevealed = false;
        }
        /**
         * @return {?}
         */
        CardController.prototype.deal = /**
         * @return {?}
         */
            function () {
                this._dealt = true;
                this.soundPlayer.play('cardPlace');
            };
        /**
         * @param {?} data
         * @param {?=} rankNumber
         * @param {?=} cardId
         * @return {?}
         */
        CardController.prototype.reveal = /**
         * @param {?} data
         * @param {?=} rankNumber
         * @param {?=} cardId
         * @return {?}
         */
            function (data, rankNumber, cardId) {
                if (rankNumber === void 0) {
                    rankNumber = undefined;
                }
                if (cardId === void 0) {
                    cardId = undefined;
                }
                this._rank = data.rankName.toLowerCase();
                this._suit = SUITS[data.suit];
                this._rankNumber = rankNumber;
                this._cardId = cardId;
                this._isRevealed = true;
            };
        /**
         * @return {?}
         */
        CardController.prototype.flip = /**
         * @return {?}
         */
            function () {
                this._flipped = true;
                this.soundPlayer.play('cardFlip');
            };
        /**
         * @return {?}
         */
        CardController.prototype.unflip = /**
         * @return {?}
         */
            function () {
                this._flipped = false;
                this.soundPlayer.play('cardFlip');
            };
        /**
         * @return {?}
         */
        CardController.prototype.dealAndFlip = /**
         * @return {?}
         */
            function () {
                this._dealt = true;
                this._flipped = true;
                this.soundPlayer.play('cardFlip');
            };
        Object.defineProperty(CardController.prototype, "dealt", {
            get: /**
             * @return {?}
             */ function () {
                return this._dealt;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CardController.prototype, "flipped", {
            get: /**
             * @return {?}
             */ function () {
                return this._flipped;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CardController.prototype, "suit", {
            get: /**
             * @return {?}
             */ function () {
                return this._suit;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CardController.prototype, "card", {
            get: /**
             * @return {?}
             */ function () {
                return this.rank;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CardController.prototype, "rank", {
            get: /**
             * @return {?}
             */ function () {
                return this._rank;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CardController.prototype, "rankNumber", {
            get: /**
             * @return {?}
             */ function () {
                return this._rankNumber;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CardController.prototype, "cardId", {
            get: /**
             * @return {?}
             */ function () {
                return this._cardId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CardController.prototype, "isRevealed", {
            get: /**
             * @return {?}
             */ function () {
                return this._isRevealed;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CardController.prototype, "data", {
            get: /**
             * @return {?}
             */ function () {
                return {
                    suit: this.suit,
                    dealt: this._dealt,
                    flipped: this.flipped,
                    card: this.card
                };
            },
            enumerable: true,
            configurable: true
        });
        return CardController;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/util/math-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} amount
     * @param {?} digits
     * @param {?=} precision
     * @return {?}
     */
    function toTotalDigits(amount, digits, precision) {
        if (precision === void 0) {
            precision = undefined;
        }
        return numStringToTotalDigits(String(amount), digits, precision);
    }
    /**
     * @param {?} amount
     * @param {?} digits
     * @param {?=} precision
     * @return {?}
     */
    function numStringToTotalDigits(amount, digits, precision) {
        if (precision === void 0) {
            precision = undefined;
        }
        /** @type {?} */
        var rounded = roundStringDownWithPrecision(amount, digits + 1);
        /** @type {?} */
        var numOfDigitsBeforeDecimal = getNumOfDigitsBeforeDecimal(rounded);
        /** @type {?} */
        var numOfDigitsAfterDecimal = digits - numOfDigitsBeforeDecimal;
        if (precision != undefined &&
            numOfDigitsAfterDecimal > precision) {
            numOfDigitsAfterDecimal = precision;
        }
        return numOfDigitsAfterDecimal > 0 ?
            roundStringDownWithPrecision(rounded, numOfDigitsAfterDecimal) :
            rounded.split('.')[0];
    }
    /**
     * @param {?} amount
     * @param {?} precision
     * @return {?}
     */
    function roundDownWithPrecision(amount, precision) {
        //amount = Number(amount);
        return roundStringDownWithPrecision(String(amount), precision);
    }
    /**
     * @param {?} amount
     * @param {?} precision
     * @return {?}
     */
    function roundDownWithoutTrailingZeros(amount, precision) {
        /** @type {?} */
        var valueWithAllDigits = roundStringDownWithPrecision(amount, precision);
        /** @type {?} */
        var parts = valueWithAllDigits.split('.');
        /** @type {?} */
        var digitsBeforeDecimal = parts[0];
        /** @type {?} */
        var digitsAfterDecimal = parts[1];
        /** @type {?} */
        var numOfDecimalDigitsToInclude = 0;
        for (var i = digitsAfterDecimal.length - 1; i > -1; i--) {
            /** @type {?} */
            var digit = digitsAfterDecimal[i];
            if (digit != '0') {
                numOfDecimalDigitsToInclude = i + 1;
                break;
            }
        }
        return numOfDecimalDigitsToInclude > 0 ?
            digitsBeforeDecimal + '.' + digitsAfterDecimal.substr(0, numOfDecimalDigitsToInclude) :
            digitsBeforeDecimal;
    }
    /**
     * @param {?} amount
     * @param {?} precision
     * @return {?}
     */
    function roundStringDownWithPrecision(amount, precision) {
        /** @type {?} */
        var parts = String(amount).split('.');
        /** @type {?} */
        var isWholeNumber = parts.length == 1;
        /** @type {?} */
        var rounded = Number(parts[0] + '.' +
            (isWholeNumber ?
                '0' :
                parts[1].substr(0, precision)));
        /** @type {?} */
        var result = rounded.toFixed(precision);
        return result;
    }
    /**
     * @param {?} number
     * @return {?}
     */
    function getNumOfDigitsBeforeDecimal(number) {
        /** @type {?} */
        var parts = number.split('.');
        return parts[0].length;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/math/integer-math.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var IntBasedNum = /** @class */ (function () {
        function IntBasedNum(precision, decimalValue, bigDecimal) {
            if (decimalValue === void 0) {
                decimalValue = 0;
            }
            if (bigDecimal === void 0) {
                bigDecimal = undefined;
            }
            this.precision = precision;
            /** @type {?} */
            var decimal = bigDecimal == undefined ?
                new big_js.Big(decimalValue).round(precision, 0 /* RoundDown */) :
                bigDecimal;
            this.factor = Math.pow(10, precision);
            this._integer = decimal.times(this.factor).round(0, 0 /* RoundDown */);
        }
        // CLASS
        // CLASS
        /**
         * @param {?} integer
         * @param {?} precision
         * @return {?}
         */
        IntBasedNum.fromInteger =
            // CLASS
            /**
             * @param {?} integer
             * @param {?} precision
             * @return {?}
             */
            function (integer, precision) {
                return new IntBasedNum(precision, undefined, new big_js.Big(integer).div(Math.pow(10, precision)).round(precision, 0 /* RoundDown */));
            };
        /*** MUTATING METHODS ***/
        /**
         * MUTATING METHODS **
         * @param {?} other
         * @return {?}
         */
        IntBasedNum.prototype.addAndReplace = /**
         * MUTATING METHODS **
         * @param {?} other
         * @return {?}
         */
            function (other) {
                if (this.precision != other.precision) {
                    throw new Error('precision of both operands must be the same!');
                }
                /** @type {?} */
                var sum = this.integer.plus(other.integer);
                this._integer = sum;
                //return this;
            };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} other
         * @return {THIS}
         */
        IntBasedNum.prototype.subtractAndReplace = /**
         * @template THIS
         * @this {THIS}
         * @param {?} other
         * @return {THIS}
         */
            function (other) {
                if (( /** @type {?} */(this)).precision != other.precision) {
                    throw new Error('precision of both operands must be the same!');
                }
                /** @type {?} */
                var diff = ( /** @type {?} */(this)).integer.minus(other.integer);
                ( /** @type {?} */(this))._integer = diff;
                return ( /** @type {?} */(this));
            };
        /***/
        /**
         *
         * @param {?} other
         * @return {?}
         */
        IntBasedNum.prototype.multiply = /**
         *
         * @param {?} other
         * @return {?}
         */
            function (other) {
                return integerMultiplication(this, other);
            };
        /**
         * @param {?} decimal
         * @return {?}
         */
        IntBasedNum.prototype.multiplyDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
            function (decimal) {
                /** @type {?} */
                var other = new IntBasedNum(this.precision, decimal);
                return integerMultiplication(this, other);
            };
        /**
         * @param {?} decimal
         * @return {?}
         */
        IntBasedNum.prototype.divideByDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
            function (decimal) {
                /** @type {?} */
                var other = new IntBasedNum(this.precision, decimal.toString());
                return integerDivision(this, other);
            };
        Object.defineProperty(IntBasedNum.prototype, "reciprocal", {
            get: /**
             * @return {?}
             */ function () {
                if (this._reciprocal == undefined) {
                    this._reciprocal = integerDivision(new IntBasedNum(this.precision, '1'), this);
                }
                return this._reciprocal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IntBasedNum.prototype, "opposite_", {
            get: /**
             * @return {?}
             */ function () {
                return integerMultiplication(new IntBasedNum(this.precision, '-1'), this);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IntBasedNum.prototype, "decimalAsNumber", {
            get: /**
             * @return {?}
             */ function () {
                return Number(this.decimal);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IntBasedNum.prototype, "integer", {
            get: /**
             * @return {?}
             */ function () {
                return this._integer;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        IntBasedNum.prototype.toString = /**
         * @return {?}
         */
            function () {
                return this.decimal;
            };
        Object.defineProperty(IntBasedNum.prototype, "decimalWithoutTrailingZeros", {
            get: /**
             * @return {?}
             */ function () {
                return roundDownWithoutTrailingZeros(this.decimal, this.precision);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IntBasedNum.prototype, "decimal", {
            get: /**
             * @return {?}
             */ function () {
                big_js.Big.RM = 0 /* RoundDown */;
                return this.integer.div(this.factor).toFixed(this.precision);
            },
            enumerable: true,
            configurable: true
        });
        return IntBasedNum;
    }());
    var AssetAmount = /** @class */ (function (_super) {
        __extends(AssetAmount, _super);
        function AssetAmount(data) {
            var _this = this;
            /** @type {?} */
            var parts = data.split(' ');
            /** @type {?} */
            var decimal = parts[0];
            /** @type {?} */
            var symbol = parts[1];
            /** @type {?} */
            var precision = decimal.split('.')[1].length;
            _this = _super.call(this, precision, decimal) || this;
            _this._symbol = symbol;
            _this._isZero = Number(decimal) == 0;
            return _this;
        }
        Object.defineProperty(AssetAmount.prototype, "isZero", {
            get: /**
             * @return {?}
             */ function () {
                return this._isZero;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AssetAmount.prototype, "symbol", {
            get: /**
             * @return {?}
             */ function () {
                return this._symbol;
            },
            enumerable: true,
            configurable: true
        });
        return AssetAmount;
    }(IntBasedNum));
    //export 
    /**
     * @template T
     * @param {?} a
     * @param {?} b
     * @return {?}
     */
    function integerMultiplication(a, b) {
        if (a.precision < b.precision) {
            console.log(a.decimal);
            console.log(b.decimal);
            throw new Error('precision of a: ' + a.precision +
                ', precision of b: ' + b.precision +
                "\nprecision of A must be >= B!");
        }
        /** @type {?} */
        var product = a.integer.times(b.integer);
        /** @type {?} */
        var integer = product.div(b.factor)
            .round(0, 0 /* RoundDown */)
            .toString();
        /** @type {?} */
        var result = IntBasedNum.fromInteger(integer, a.precision);
        //console.log('INT Multiplication: '+a+' * '+b+' = '+result);
        return result;
    }
    // when doing division between two eos numbers then disregard
    // the decimal after the integer
    //export 
    /**
     * @template T
     * @param {?} a
     * @param {?} b
     * @return {?}
     */
    function integerDivision(a, b) {
        if (a.precision < b.precision) {
            throw new Error('precision of A must be >= B!');
        }
        /** @type {?} */
        var quotient = a.integer.div(b.integer);
        /** @type {?} */
        var integer = quotient.times(b.factor)
            .round(0, 0 /* RoundDown */)
            .toString();
        /** @type {?} */
        var result = IntBasedNum.fromInteger(integer, a.precision);
        console.log('INT Division: ' + a + ' / ' + b + ' = ' + result);
        return result;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/models/staking/constants.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var betTokenFactor = 10000;

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/models/staking/staking-level-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var stakingLevels = [
        { name: 'shrimp', stakePoints: 1000, houseEdgeReduction: 0.05, baccaratHouseEdge: 1.03 },
        { name: 'crab', stakePoints: 10000, houseEdgeReduction: 0.10, baccaratHouseEdge: 0.99 },
        { name: 'octopus', stakePoints: 25000, houseEdgeReduction: 0.12, baccaratHouseEdge: 0.98 },
        { name: 'squid', stakePoints: 50000, houseEdgeReduction: 0.15, baccaratHouseEdge: 0.96 },
        { name: 'fish', stakePoints: 100000, houseEdgeReduction: 0.20, baccaratHouseEdge: 0.92 },
        { name: 'blowfish', stakePoints: 250000, houseEdgeReduction: 0.25, baccaratHouseEdge: 0.89 },
        { name: 'dolphin', stakePoints: 500000, houseEdgeReduction: 0.35, baccaratHouseEdge: 0.82 },
        { name: 'shark', stakePoints: 1000000, houseEdgeReduction: 0.50, baccaratHouseEdge: 0.71 },
        { name: 'whale', stakePoints: 2500000, houseEdgeReduction: 0.75, baccaratHouseEdge: 0.60 },
        { name: 'blue-whale', stakePoints: 5000000, houseEdgeReduction: 1, baccaratHouseEdge: 0.50 },
    ];
    /**
     * @param {?} stakeWeight
     * @return {?}
     */
    function getHouseEdgeReductionAsDecimal(stakeWeight) {
        /** @type {?} */
        var levelIndex = getStakingLevelIndex(stakeWeight);
        /** @type {?} */
        var level = stakingLevels[levelIndex];
        return level ?
            level.houseEdgeReduction / 100 :
            0;
    }
    /**
     * @param {?} stakeWeight
     * @return {?}
     */
    function getStakingLevelName(stakeWeight) {
        /** @type {?} */
        var levelIndex = getStakingLevelIndex(stakeWeight);
        /** @type {?} */
        var level = stakingLevels[levelIndex];
        return level ?
            level.name :
            undefined;
    }
    /**
     * @param {?} stakeWeight
     * @return {?}
     */
    function getStakingLevelIndex(stakeWeight) {
        /** @type {?} */
        var points = stakeWeight / betTokenFactor;
        /** @type {?} */
        var firstLevel = stakingLevels[0];
        if (points < firstLevel.stakePoints) {
            return -1;
        }
        for (var i = 0; i < stakingLevels.length; i++) {
            /** @type {?} */
            var nextLevel = stakingLevels[i + 1];
            if (!nextLevel ||
                points < nextLevel.stakePoints) {
                return i;
            }
        }
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/models/bets/bet-base.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @abstract
     */
    var /**
     * @abstract
     */ BetBase = /** @class */ (function () {
        function BetBase(data) {
            this.id = data.id;
            this.time = new Date(data.resolvedMilliseconds);
            this.bettor = data.bettor;
            /** @type {?} */
            var parts = data.amount.split(' ');
            this.betAmount = new AssetAmount(data.amount);
            this.amount = this.betAmount.decimalWithoutTrailingZeros;
            this.tokenSymbol = parts[1];
            this.jackpotSpin = data.jackpotSpin;
            this.isJackpotBet = this.jackpotSpin && this.jackpotSpin.length > 0;
            this.stakeWeight =
                data.stakeWeight ?
                    Number(data.stakeWeight) :
                    0;
            this.stakingLevelName = getStakingLevelName(this.stakeWeight);
        }
        return BetBase;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/services/overlay-modal.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var OverlayModalService = /** @class */ (function () {
        function OverlayModalService() {
            this.subject = new rxjs.Subject();
        }
        /**
         * @param {?} message
         * @return {?}
         */
        OverlayModalService.prototype.open = /**
         * @param {?} message
         * @return {?}
         */
            function (message) {
                this.close();
                this.subject.next(message);
            };
        /**
         * @return {?}
         */
        OverlayModalService.prototype.close = /**
         * @return {?}
         */
            function () {
                this.subject.next();
            };
        /**
         * @return {?}
         */
        OverlayModalService.prototype.getMessage = /**
         * @return {?}
         */
            function () {
                return this.subject.asObservable();
            };
        OverlayModalService.decorators = [
            { type: core.Injectable }
        ];
        OverlayModalService.ctorParameters = function () { return []; };
        return OverlayModalService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/blackjack-modal/blackjack-modal.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlackjackModalComponent = /** @class */ (function () {
        function BlackjackModalComponent() {
            this._isClosed = false;
            this.result = undefined;
        }
        /**
         * @param {?} result
         * @return {?}
         */
        BlackjackModalComponent.prototype.showResult = /**
         * @param {?} result
         * @return {?}
         */
            function (result) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this.result = result;
                                this._isClosed = false;
                                return [4 /*yield*/, earnbetCommon.sleep(2 * 1000)];
                            case 1:
                                _a.sent();
                                this.close();
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackModalComponent.prototype.close = /**
         * @return {?}
         */
            function () {
                this._isClosed = true;
            };
        Object.defineProperty(BlackjackModalComponent.prototype, "isOpen", {
            get: /**
             * @return {?}
             */ function () {
                return this.result != undefined &&
                    !this._isClosed;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackModalComponent.prototype, "isWin", {
            get: /**
             * @return {?}
             */ function () {
                return this.result && this.result.isWin;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackModalComponent.prototype, "isPush", {
            get: /**
             * @return {?}
             */ function () {
                return this.result && this.result.isPush;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackModalComponent.prototype, "isBlackjack", {
            get: /**
             * @return {?}
             */ function () {
                return this.result && this.result.isBlackjack;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackModalComponent.prototype, "isLoss", {
            get: /**
             * @return {?}
             */ function () {
                return this.result && this.result.isLoss;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackModalComponent.prototype, "isBusted", {
            get: /**
             * @return {?}
             */ function () {
                return this.result && this.result.isBusted;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackModalComponent.prototype, "isInsuranceSuccess", {
            get: /**
             * @return {?}
             */ function () {
                return this.result && this.result.isInsuranceSuccess;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackModalComponent.prototype, "amountWon", {
            get: /**
             * @return {?}
             */ function () {
                return this.result && this.result.amountWon;
            },
            enumerable: true,
            configurable: true
        });
        BlackjackModalComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'app-blackjack-modal',
                        template: "<div *ngIf=\"isOpen\">\n  <div class=\"bj-modal\" [ngClass]=\"{\n                'red': isBusted || isLoss,\n                'green': isWin || isBlackjack,\n                'yellow': isPush,\n                'blue': isInsuranceSuccess\n              }\">\n  <button class=\"modal-close\" (click)='close()'>&times;</button>\n\n    <div class=\"align-items-center d-flex flex-column h-100 justify-content-center p-3\">\n      <div class=\"win\" *ngIf=\"isWin && !isBlackjack\">\n        <img class=\"mb-2 small-circle\" src=\"assets/img/blackjack/modal/win.png\" alt=\"win\">\n        <h1 class=\"bj-modal-title\">WIN!</h1>\n        <div class=\"points\">+ {{amountWon | number:'1.0-8'}}</div>\n      </div>\n\n      <div class=\"push\" *ngIf=\"isPush\">\n        <img class=\"mb-2 small-circle\" src=\"assets/img/blackjack/modal/push.png\" alt=\"push\">\n        <h1 class=\"bj-modal-title\">PUSH</h1>\n      </div>\n\n      <div class=\"blackjack\" *ngIf=\"isBlackjack\">\n        <img class=\"bj-image\" src=\"assets/img/blackjack/modal/blackjack.png\" alt=\"blackjack\">\n        <div class=\"points\">+ {{amountWon | number:'1.0-8'}}</div>\n      </div>\n\n      <div class=\"busted\" *ngIf=\"isBusted\">\n        <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/busted.png\" alt=\"busted\">\n        <h1 class=\"bj-modal-title\">BUSTED!</h1>\n      </div>\n\n     <div class=\"busted\" *ngIf=\"isLoss\">\n          <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/busted.png\" alt=\"busted\">\n          <h1 class=\"bj-modal-title\">LOSS</h1>\n      </div> \n\n\n      <!-- TODO: confirm state name -->\n      <div class=\"insurance\" *ngIf=\"isInsuranceSuccess\">\n        <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/success.png\" alt=\"insurance success\">\n        <h1 class=\"bj-modal-title\">Insurance Success</h1>\n      </div>\n    </div>\n  </div>\n\n</div>\n  ",
                        styles: [".bj-modal{max-width:305px;height:100%;max-height:200px;width:100%;background-color:#16334a;opacity:.95;border:2px solid transparent;border-radius:8px;letter-spacing:0;font-size:34px;color:#fff;text-align:center;position:absolute;top:50%;left:50%;bottom:50%;right:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);z-index:9999}.bj-modal-wrapper{height:100%;width:100%;position:relative}.bj-image{max-width:175px}.bj-modal.green{border-color:#02f292}.bj-modal.green .bj-modal-title{color:#02f292;margin-bottom:0}.bj-modal.green .points{color:#02f292;font-size:20px;font-weight:900}.bj-modal.blue{border-color:#67ddf2}.bj-modal.blue .bj-modal-title{color:#67ddf2}.bj-modal.yellow{border-color:#ffec77}.bj-modal.yellow .bj-modal-title{color:#ffec77}.bj-modal.red{border-color:#f10260}.bj-modal.red .bj-modal-title{color:#f10260}.small-circle{max-width:66px}.modal-close{font-size:25px;background-color:transparent;position:absolute;top:0;right:0;border:none;color:#fff}"]
                    }] }
        ];
        BlackjackModalComponent.ctorParameters = function () { return []; };
        return BlackjackModalComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/blackjack.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlackjackModule = /** @class */ (function () {
        function BlackjackModule() {
        }
        BlackjackModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [
                            BlackjackModalComponent,
                        ],
                        imports: [
                            common.CommonModule
                        ],
                        exports: [
                            BlackjackModalComponent,
                        ],
                        entryComponents: [
                            BlackjackModalComponent
                        ]
                    },] }
        ];
        return BlackjackModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/main/game/bet-amount.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BetAmount = /** @class */ (function () {
        function BetAmount() {
            this._isMax = false;
            this._amount = 0;
        }
        /**
         * @return {?}
         */
        BetAmount.prototype.reset = /**
         * @return {?}
         */
            function () {
                this.amount = 0;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        BetAmount.prototype.add = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.amount += value;
            };
        /**
         * @return {?}
         */
        BetAmount.prototype.double = /**
         * @return {?}
         */
            function () {
                this.amount *= 2;
            };
        /**
         * @return {?}
         */
        BetAmount.prototype.half = /**
         * @return {?}
         */
            function () {
                this.amount *= 0.5;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        BetAmount.prototype.subtract = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.amount -= value;
                if (this.amount < 0) {
                    this.amount = 0;
                }
            };
        /**
         * @param {?} value
         * @return {?}
         */
        BetAmount.prototype.setToMax = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.amount = value;
                this._isMax = true;
            };
        Object.defineProperty(BetAmount.prototype, "amount", {
            get: /**
             * @return {?}
             */ function () {
                return this._amount;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this._isMax = false;
                this._amount = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BetAmount.prototype, "isMax", {
            get: /**
             * @return {?}
             */ function () {
                return this._isMax;
            },
            enumerable: true,
            configurable: true
        });
        return BetAmount;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/main/game/hand-result.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @abstract
     */
    var /**
     * @abstract
     */ HandResultBase = /** @class */ (function () {
        function HandResultBase(value, betAmount) {
            this.value = value;
            this.amountWon = value * betAmount;
        }
        Object.defineProperty(HandResultBase.prototype, "isBlackjack", {
            get: /**
             * @return {?}
             */ function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HandResultBase.prototype, "isWin", {
            get: /**
             * @return {?}
             */ function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HandResultBase.prototype, "isPush", {
            get: /**
             * @return {?}
             */ function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HandResultBase.prototype, "isLoss", {
            get: /**
             * @return {?}
             */ function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HandResultBase.prototype, "isBusted", {
            get: /**
             * @return {?}
             */ function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HandResultBase.prototype, "isInsuranceSuccess", {
            get: /**
             * @return {?}
             */ function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        return HandResultBase;
    }());
    var BlackjackResult = /** @class */ (function (_super) {
        __extends(BlackjackResult, _super);
        function BlackjackResult(betAmount) {
            return _super.call(this, 1.5, betAmount) || this;
        }
        Object.defineProperty(BlackjackResult.prototype, "isBlackjack", {
            get: /**
             * @return {?}
             */ function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackResult.prototype, "isWin", {
            get: /**
             * @return {?}
             */ function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        return BlackjackResult;
    }(HandResultBase));
    var WinResult = /** @class */ (function (_super) {
        __extends(WinResult, _super);
        function WinResult(betAmount) {
            return _super.call(this, 1, betAmount) || this;
        }
        Object.defineProperty(WinResult.prototype, "isWin", {
            get: /**
             * @return {?}
             */ function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        return WinResult;
    }(HandResultBase));
    var PushResult = /** @class */ (function (_super) {
        __extends(PushResult, _super);
        function PushResult(betAmount) {
            return _super.call(this, 0, betAmount) || this;
        }
        Object.defineProperty(PushResult.prototype, "isPush", {
            get: /**
             * @return {?}
             */ function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        return PushResult;
    }(HandResultBase));
    var LossResult = /** @class */ (function (_super) {
        __extends(LossResult, _super);
        function LossResult(betAmount) {
            return _super.call(this, -1, betAmount) || this;
        }
        Object.defineProperty(LossResult.prototype, "isLoss", {
            get: /**
             * @return {?}
             */ function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        return LossResult;
    }(HandResultBase));
    var BustedResult = /** @class */ (function (_super) {
        __extends(BustedResult, _super);
        function BustedResult(betAmount) {
            return _super.call(this, -1, betAmount) || this;
        }
        Object.defineProperty(BustedResult.prototype, "isBusted", {
            get: /**
             * @return {?}
             */ function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BustedResult.prototype, "isLoss", {
            get: /**
             * @return {?}
             */ function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        return BustedResult;
    }(HandResultBase));
    var InsuranceSuccessResult = /** @class */ (function (_super) {
        __extends(InsuranceSuccessResult, _super);
        function InsuranceSuccessResult(betAmount) {
            return _super.call(this, 0, betAmount) || this;
        }
        Object.defineProperty(InsuranceSuccessResult.prototype, "isInsuranceSuccess", {
            get: /**
             * @return {?}
             */ function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        return InsuranceSuccessResult;
    }(HandResultBase));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/main/game/blackjack-hands.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlackjackHand = /** @class */ (function () {
        function BlackjackHand(soundPlayer) {
            this.soundPlayer = soundPlayer;
            this._cards = [];
            this._numOfAces = 0;
            this.countWithoutAces = 0;
            this._isAllCardsKnown = false;
            this._isFirstCardAnAce = false;
            this.isSplit = false;
        }
        /**
         * @return {?}
         */
        BlackjackHand.prototype.reset = /**
         * @return {?}
         */
            function () {
                this._cards = [];
                this._numOfAces = 0;
                this.countWithoutAces = 0;
                this._isAllCardsKnown = false;
                this._isFirstCardAnAce = false;
                this.isSplit = false;
            };
        /**
         * @param {?} cardId
         * @return {?}
         */
        BlackjackHand.prototype.hit = /**
         * @param {?} cardId
         * @return {?}
         */
            function (cardId) {
                return this.dealCard(cardId, true);
            };
        /**
         * @protected
         * @param {?} cardId
         * @param {?} shouldReveal
         * @param {?=} revealImmediately
         * @return {?}
         */
        BlackjackHand.prototype.dealCard = /**
         * @protected
         * @param {?} cardId
         * @param {?} shouldReveal
         * @param {?=} revealImmediately
         * @return {?}
         */
            function (cardId, shouldReveal, revealImmediately) {
                if (revealImmediately === void 0) {
                    revealImmediately = false;
                }
                return __awaiter(this, void 0, void 0, function () {
                    var card, rankNumber, isAce;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                card = new CardController(this.soundPlayer);
                                this.cards.push(card);
                                rankNumber = cardId % 13;
                                isAce = rankNumber == earnbetCommon.CardRankNumber.ACE;
                                card.reveal({
                                    suit: Math.floor(cardId / 13),
                                    rankName: earnbetCommon.nameOfRank(rankNumber)
                                }, rankNumber, cardId);
                                if (!!revealImmediately)
                                    return [3 /*break*/, 2];
                                return [4 /*yield*/, earnbetCommon.sleep(250)];
                            case 1:
                                _a.sent();
                                _a.label = 2;
                            case 2:
                                if (shouldReveal) {
                                    card.dealAndFlip();
                                }
                                else {
                                    card.deal();
                                }
                                if (!!revealImmediately)
                                    return [3 /*break*/, 4];
                                return [4 /*yield*/, earnbetCommon.sleep(250)];
                            case 3:
                                _a.sent();
                                _a.label = 4;
                            case 4:
                                if (shouldReveal) {
                                    this.addValueOfCard(rankNumber, isAce);
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @private
         * @param {?} rankNumber
         * @param {?} isAce
         * @return {?}
         */
        BlackjackHand.prototype.addValueOfCard = /**
         * @private
         * @param {?} rankNumber
         * @param {?} isAce
         * @return {?}
         */
            function (rankNumber, isAce) {
                /** @type {?} */
                var valueOfCard = 0;
                switch (rankNumber) {
                    case earnbetCommon.CardRankNumber.JACK:
                    case earnbetCommon.CardRankNumber.QUEEN:
                    case earnbetCommon.CardRankNumber.KING:
                        valueOfCard = 10;
                        break;
                    case earnbetCommon.CardRankNumber.ACE:
                        // will determine ace value later
                        break;
                    default:
                        valueOfCard = rankNumber;
                        break;
                }
                this.countWithoutAces += valueOfCard;
                if (isAce) {
                    this._numOfAces++;
                    if (this.numOfCards == 1) {
                        this._isFirstCardAnAce = true;
                    }
                }
            };
        Object.defineProperty(BlackjackHand.prototype, "isBusted", {
            get: /**
             * @return {?}
             */ function () {
                return this.sum > 21;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "is21", {
            get: /**
             * @return {?}
             */ function () {
                return this.sum == 21;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "sumAsString", {
            get: /**
             * @return {?}
             */ function () {
                /** @type {?} */
                var sum = '' + this.sum;
                if (this.isSoftCount) {
                    sum += '/' +
                        (this.countWithoutAces + this.numOfAces);
                }
                return sum;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "sum", {
            get: /**
             * @return {?}
             */ function () {
                /** @type {?} */
                var sum = this.countWithoutAces + this.countOfAces;
                if (this.isBlackJack) {
                    sum = 21;
                }
                return sum;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "countOfAces", {
            get: /**
             * @return {?}
             */ function () {
                if (this.isSoftCount) {
                    /** @type {?} */
                    var largerCount = 11 + (this.numOfAces - 1);
                    if (largerCount + this.countWithoutAces <=
                        21) {
                        return largerCount;
                    }
                }
                return this.numOfAces;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "isSoftCount", {
            get: /**
             * @return {?}
             */ function () {
                return !this.isBlackJack &&
                    this.numOfAces > 0 &&
                    (this.countWithoutAces +
                        11 +
                        (this.numOfAces - 1)) <= 21;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "isBlackJack", {
            get: /**
             * @return {?}
             */ function () {
                return !this.isSplit &&
                    this.numOfCards == 2 &&
                    this.numOfAces == 1 &&
                    this.countWithoutAces == 10;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "isFirstCardAnAce", {
            get: /**
             * @return {?}
             */ function () {
                return this._isFirstCardAnAce;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "numOfAces", {
            get: /**
             * @return {?}
             */ function () {
                return this._numOfAces;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "numOfCards", {
            get: /**
             * @return {?}
             */ function () {
                return this.cards.length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "isAllCardsKnown", {
            get: /**
             * @return {?}
             */ function () {
                return this._isAllCardsKnown;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackHand.prototype, "cards", {
            get: /**
             * @return {?}
             */ function () {
                return this._cards;
            },
            enumerable: true,
            configurable: true
        });
        return BlackjackHand;
    }());
    var BlackjackDealerHand = /** @class */ (function (_super) {
        __extends(BlackjackDealerHand, _super);
        function BlackjackDealerHand() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @param {?} cardId
         * @return {?}
         */
        BlackjackDealerHand.prototype.dealFirstCard = /**
         * @param {?} cardId
         * @return {?}
         */
            function (cardId) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.dealCard(cardId, true)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackDealerHand.prototype.dealAnonymousCard = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                // deal anonymous card for dealer's second card (at first)
                                return [4 /*yield*/, this.dealCard(0, false)];
                            case 1:
                                // deal anonymous card for dealer's second card (at first)
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @param {?} cardId
         * @return {?}
         */
        BlackjackDealerHand.prototype.revealSecondCard = /**
         * @param {?} cardId
         * @return {?}
         */
            function (cardId) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.numOfCards > 1) {
                                    this.cards.pop();
                                }
                                return [4 /*yield*/, this.dealCard(cardId, true)];
                            case 1:
                                _a.sent();
                                this._isAllCardsKnown = true;
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @param {?} cardId
         * @return {?}
         */
        BlackjackDealerHand.prototype.dealAdditionalCard = /**
         * @param {?} cardId
         * @return {?}
         */
            function (cardId) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!this.shouldHit)
                                    return [3 /*break*/, 2];
                                return [4 /*yield*/, this.hit(cardId)];
                            case 1:
                                _a.sent();
                                _a.label = 2;
                            case 2: return [2 /*return*/];
                        }
                    });
                });
            };
        Object.defineProperty(BlackjackDealerHand.prototype, "shouldHit", {
            get: /**
             * @private
             * @return {?}
             */ function () {
                return this.isSoftCount ?
                    this.sum <= 17 :
                    this.sum < 17;
            },
            enumerable: true,
            configurable: true
        });
        return BlackjackDealerHand;
    }(BlackjackHand));
    var BlackjackPlayerHand = /** @class */ (function (_super) {
        __extends(BlackjackPlayerHand, _super);
        function BlackjackPlayerHand(soundPlayer) {
            var _this = _super.call(this, soundPlayer) || this;
            _this.didDoubleDown = false;
            _this._isWaitingForAction = false;
            _this.bet = new BetAmount();
            _this._boughtInsurance = false;
            return _this;
        }
        /**
         * @return {?}
         */
        BlackjackPlayerHand.prototype.reset = /**
         * @return {?}
         */
            function () {
                _super.prototype.reset.call(this);
                this.didDoubleDown = false;
                this._isWaitingForAction = false;
                this.bet.reset();
                this._boughtInsurance = false;
            };
        /**
         * @param {?} cardId
         * @param {?} betAmount
         * @return {?}
         */
        BlackjackPlayerHand.prototype.dealFirstCard = /**
         * @param {?} cardId
         * @param {?} betAmount
         * @return {?}
         */
            function (cardId, betAmount) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this.bet.add(betAmount);
                                return [4 /*yield*/, this.hit(cardId)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @param {?} cardId
         * @return {?}
         */
        BlackjackPlayerHand.prototype.dealSecondCard = /**
         * @param {?} cardId
         * @return {?}
         */
            function (cardId) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.hit(cardId)];
                            case 1:
                                _a.sent();
                                this._isAllCardsKnown = true;
                                if (!this.isBlackJack) {
                                    this._isWaitingForAction = true;
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackPlayerHand.prototype.buyInsurance = /**
         * @return {?}
         */
            function () {
                /*
                this.bet.add(
                    this.bet.amount / 2
                );
                */
                this._boughtInsurance = true;
            };
        /**
         * @param {?} cards
         * @param {?} betAmount
         * @return {?}
         */
        BlackjackPlayerHand.prototype.split = /**
         * @param {?} cards
         * @param {?} betAmount
         * @return {?}
         */
            function (cards, betAmount) {
                return __awaiter(this, void 0, void 0, function () {
                    var isFirstCardAnAce;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this.reset();
                                this.isSplit = true;
                                this.bet.add(betAmount);
                                return [4 /*yield*/, this.dealCard(cards[0], true, true)];
                            case 1:
                                _a.sent();
                                isFirstCardAnAce = this.numOfAces == 1;
                                return [4 /*yield*/, this.hit(cards[1])];
                            case 2:
                                _a.sent();
                                if (isFirstCardAnAce) {
                                    this._isWaitingForAction = false;
                                }
                                this._isAllCardsKnown = true;
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @param {?} cardId
         * @return {?}
         */
        BlackjackPlayerHand.prototype.doubleDown = /**
         * @param {?} cardId
         * @return {?}
         */
            function (cardId) {
                this.didDoubleDown = true;
                this.bet.double();
                return this.hit(cardId, true);
            };
        /**
         * @param {?} cardId
         * @param {?=} isDoubleDown
         * @return {?}
         */
        BlackjackPlayerHand.prototype.hit = /**
         * @param {?} cardId
         * @param {?=} isDoubleDown
         * @return {?}
         */
            function (cardId, isDoubleDown) {
                if (isDoubleDown === void 0) {
                    isDoubleDown = false;
                }
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this._isWaitingForAction = false;
                                return [4 /*yield*/, _super.prototype.hit.call(this, cardId)];
                            case 1:
                                _a.sent();
                                if (this.numOfCards >= 2 &&
                                    !this.isBusted &&
                                    !isDoubleDown) {
                                    this._isWaitingForAction = true;
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackPlayerHand.prototype.stand = /**
         * @return {?}
         */
            function () {
                this._isWaitingForAction = false;
            };
        /**
         * @param {?} dealer
         * @return {?}
         */
        BlackjackPlayerHand.prototype.determineResult = /**
         * @param {?} dealer
         * @return {?}
         */
            function (dealer) {
                /** @type {?} */
                var betAmount = this.bet.amount;
                /** @type {?} */
                var result;
                if (this.isBusted) {
                    result = new BustedResult(betAmount);
                }
                else if (this.isBlackJack) {
                    result = dealer.isBlackJack ?
                        new PushResult(betAmount) :
                        new BlackjackResult(betAmount);
                }
                else if (dealer.isBlackJack) {
                    result = this.boughtInsurance ?
                        new InsuranceSuccessResult(betAmount) :
                        new LossResult(betAmount);
                }
                else if (dealer.isBusted) {
                    result = new WinResult(betAmount);
                }
                else if (this.sum == dealer.sum) {
                    result = new PushResult(betAmount);
                }
                else {
                    result = this.sum > dealer.sum ?
                        new WinResult(betAmount) :
                        new LossResult(betAmount);
                }
                //this.bet.add(result.amountWon);
                return result;
            };
        Object.defineProperty(BlackjackPlayerHand.prototype, "isAbleToDoubleDown", {
            get: /**
             * @return {?}
             */ function () {
                return this.numOfCards == 2 &&
                    !this.didDoubleDown;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackPlayerHand.prototype, "isAbleToSplit", {
            get: /**
             * @return {?}
             */ function () {
                return this.numOfCards == 2 &&
                    this.cards[0].card ==
                        this.cards[1].card;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackPlayerHand.prototype, "isWaitingForAction", {
            get: /**
             * @return {?}
             */ function () {
                return this._isWaitingForAction;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackPlayerHand.prototype, "boughtInsurance", {
            get: /**
             * @return {?}
             */ function () {
                return this._boughtInsurance;
            },
            enumerable: true,
            configurable: true
        });
        return BlackjackPlayerHand;
    }(BlackjackHand));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/main/game/blackjack-controller.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlackjackGameController = /** @class */ (function () {
        function BlackjackGameController(component, server, soundPlayer) {
            this.component = component;
            this.server = server;
            this.bet = new BetAmount();
            this.initialBetAmount = undefined;
            this._isSplit = false;
            this.hand1Result = undefined;
            this.hand2Result = undefined;
            this._promptForInsurance = false;
            this._showResult = false;
            this.dealer = new BlackjackDealerHand(soundPlayer);
            this.hand1 = new BlackjackPlayerHand(soundPlayer);
            this.hand2 = new BlackjackPlayerHand(soundPlayer);
        }
        /**
         * @private
         * @return {?}
         */
        BlackjackGameController.prototype.reset = /**
         * @private
         * @return {?}
         */
            function () {
                this.component.gameResultModal.close();
                this.dealer.reset();
                this.hand1.reset();
                this.hand2.reset();
                this.initialBetAmount = undefined;
                this._isSplit = false;
                this.selectedHandIndex = 0;
                this.hand1Result = undefined;
                this.hand2Result = undefined;
                this._promptForInsurance = false;
                this._showResult = false;
            };
        /**
         * @param {?} cards
         * @param {?} betAmount
         * @return {?}
         */
        BlackjackGameController.prototype.start = /**
         * @param {?} cards
         * @param {?} betAmount
         * @return {?}
         */
            function (cards, betAmount) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this.reset();
                                if (this.bet.amount == 0) {
                                    this.bet.add(betAmount);
                                }
                                // player
                                return [4 /*yield*/, this.hand1.dealFirstCard(cards.playerCards[0], this.bet.amount)];
                            case 1:
                                // player
                                _a.sent();
                                // dealer
                                return [4 /*yield*/, this.dealer.dealFirstCard(cards.dealerCards[0])];
                            case 2:
                                // dealer
                                _a.sent();
                                // player
                                return [4 /*yield*/, this.hand1.dealSecondCard(cards.playerCards[1])];
                            case 3:
                                // player
                                _a.sent();
                                // dealer
                                return [4 /*yield*/, this.dealer.dealAnonymousCard()];
                            case 4:
                                // dealer
                                _a.sent();
                                return [4 /*yield*/, this.checkForInsurance()];
                            case 5:
                                _a.sent();
                                this.initialBetAmount = this.bet.amount;
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackGameController.prototype.checkForInsurance = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!this.dealer.isFirstCardAnAce)
                                    return [3 /*break*/, 1];
                                this._promptForInsurance = true;
                                return [2 /*return*/];
                            case 1: return [4 /*yield*/, this.checkForBlackJack()];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @param {?} yesToInsurance
         * @return {?}
         */
        BlackjackGameController.prototype.respondToInsurance = /**
         * @param {?} yesToInsurance
         * @return {?}
         */
            function (yesToInsurance) {
                return __awaiter(this, void 0, void 0, function () {
                    var isDealerBlackjack;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this._promptForInsurance = false;
                                if (yesToInsurance) {
                                    this.hand1.buyInsurance();
                                }
                                return [4 /*yield*/, this.server.respondToInsurance(yesToInsurance)];
                            case 1:
                                isDealerBlackjack = _a.sent();
                                this.checkForBlackJack(isDealerBlackjack);
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @private
         * @param {?=} isDealerBlackjack
         * @return {?}
         */
        BlackjackGameController.prototype.checkForBlackJack = /**
         * @private
         * @param {?=} isDealerBlackjack
         * @return {?}
         */
            function (isDealerBlackjack) {
                if (isDealerBlackjack === void 0) {
                    isDealerBlackjack = undefined;
                }
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                console.log('isDealerBlackjack: ' + isDealerBlackjack);
                                if (!(isDealerBlackjack == undefined))
                                    return [3 /*break*/, 2];
                                return [4 /*yield*/, this.server.peekForBlackjack()];
                            case 1:
                                isDealerBlackjack = _a.sent();
                                _a.label = 2;
                            case 2:
                                if (isDealerBlackjack || this.hand1.isBlackJack) {
                                    this.conclude(isDealerBlackjack);
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackGameController.prototype.split = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var card1, card2, newCards, card3, card4;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!this.isAbleToSplit) {
                                    return [2 /*return*/];
                                }
                                this._isSplit = true;
                                // split hands
                                card1 = this.hand1.cards[0].cardId;
                                card2 = this.hand1.cards[1].cardId;
                                return [4 /*yield*/, this.server.split()];
                            case 1:
                                newCards = _a.sent();
                                card3 = newCards[0];
                                card4 = newCards[1];
                                return [4 /*yield*/, this.hand1.split([card1, card3], this.initialBetAmount)];
                            case 2:
                                _a.sent();
                                return [4 /*yield*/, this.hand2.split([card2, card4], this.initialBetAmount)];
                            case 3:
                                _a.sent();
                                if (!this.isWaitingForAction) {
                                    this.selectOtherHand();
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackGameController.prototype.doubleDown = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var cardId;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!this.isAbleToDoubleDown)
                                    return [3 /*break*/, 3];
                                return [4 /*yield*/, this.server.doubleDown(this.selectedHandIndex)];
                            case 1:
                                cardId = _a.sent();
                                // deal only 1 more card
                                return [4 /*yield*/, this.currentHand.doubleDown(cardId)];
                            case 2:
                                // deal only 1 more card
                                _a.sent();
                                // move to other hand (if applicable)
                                this.selectOtherHand();
                                _a.label = 3;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackGameController.prototype.hit = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var cardId;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!this.isWaitingForAction)
                                    return [3 /*break*/, 3];
                                return [4 /*yield*/, this.server.hit(this.selectedHandIndex)];
                            case 1:
                                cardId = _a.sent();
                                // deal another card
                                return [4 /*yield*/, this.currentHand.hit(cardId)];
                            case 2:
                                // deal another card
                                _a.sent();
                                if (this.currentHand.isBusted) {
                                    this.selectOtherHand();
                                }
                                else if (this.currentHand.is21) {
                                    this.stand();
                                }
                                _a.label = 3;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackGameController.prototype.stand = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!this.isWaitingForAction)
                                    return [3 /*break*/, 2];
                                return [4 /*yield*/, this.server.stand()];
                            case 1:
                                _a.sent();
                                this.currentHand.stand();
                                this.selectOtherHand();
                                _a.label = 2;
                            case 2: return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @private
         * @return {?}
         */
        BlackjackGameController.prototype.selectOtherHand = /**
         * @private
         * @return {?}
         */
            function () {
                // move to other hand if applicable
                if (this.isSplit) {
                    this.selectedHandIndex =
                        this.selectedHandIndex == 0 ?
                            1 : 0;
                    if (this.currentHand.isWaitingForAction) {
                        return;
                    }
                }
                // otherwise conclude
                this.conclude(false);
            };
        /**
         * @private
         * @param {?} isDealerBlackJack
         * @return {?}
         */
        BlackjackGameController.prototype.conclude = /**
         * @private
         * @param {?} isDealerBlackJack
         * @return {?}
         */
            function (isDealerBlackJack) {
                return __awaiter(this, void 0, void 0, function () {
                    var e_1, _a, isBusted, cardId_1, isBlackjackForHand1, cards, cards_1, cards_1_1, cardId, e_1_1;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                isBusted = this.isSplit ?
                                    (this.hand1.isBusted &&
                                        this.hand2.isBusted) :
                                    this.hand1.isBusted;
                                if (!!isBusted)
                                    return [3 /*break*/, 3];
                                return [4 /*yield*/, this.server.getDealersSecondCard()];
                            case 1:
                                cardId_1 = _b.sent();
                                return [4 /*yield*/, this.dealer.revealSecondCard(cardId_1)];
                            case 2:
                                _b.sent();
                                _b.label = 3;
                            case 3:
                                isBlackjackForHand1 = !this.isSplit &&
                                    this.hand1.isBlackJack;
                                if (!(!isBusted && !isDealerBlackJack && !isBlackjackForHand1))
                                    return [3 /*break*/, 12];
                                // deal additional cards for dealer, if applicable
                                return [4 /*yield*/, this.server.getDealersAdditionalCards()];
                            case 4:
                                cards = _b.sent();
                                _b.label = 5;
                            case 5:
                                _b.trys.push([5, 10, 11, 12]);
                                cards_1 = __values(cards), cards_1_1 = cards_1.next();
                                _b.label = 6;
                            case 6:
                                if (!!cards_1_1.done)
                                    return [3 /*break*/, 9];
                                cardId = cards_1_1.value;
                                return [4 /*yield*/, this.dealer.dealAdditionalCard(cardId)];
                            case 7:
                                _b.sent();
                                _b.label = 8;
                            case 8:
                                cards_1_1 = cards_1.next();
                                return [3 /*break*/, 6];
                            case 9: return [3 /*break*/, 12];
                            case 10:
                                e_1_1 = _b.sent();
                                e_1 = { error: e_1_1 };
                                return [3 /*break*/, 12];
                            case 11:
                                try {
                                    if (cards_1_1 && !cards_1_1.done && (_a = cards_1.return))
                                        _a.call(cards_1);
                                }
                                finally {
                                    if (e_1)
                                        throw e_1.error;
                                }
                                return [7 /*endfinally*/];
                            case 12:
                                /*** check hand 1 result */
                                // determine hand 1 result
                                this.hand1Result = this.hand1.determineResult(this.dealer);
                                //alert('Hand 1 Result: '+this.hand1Result);
                                //let result:IBlackjackResult = this.hand1Result;
                                /*** check the result of the second hand (if applicable) ***/
                                if (this.isSplit) {
                                    this.hand2Result = this.hand2.determineResult(this.dealer);
                                    /*
                                    result = new CombinedHandsResult(
                                        this.hand1Result,
                                        this.hand2Result
                                    );
                                    */
                                    //alert('Hand 2 Result: '+this.hand2Result);
                                }
                                /*** end game: ***/
                                this.initialBetAmount = undefined;
                                this._showResult = true;
                                if (this.hand1Result.isInsuranceSuccess ||
                                    this.hand1Result.isBlackjack) {
                                    this.component.gameResultModal.showResult(this.hand1Result);
                                }
                                else if (this.isSplit && this.hand2Result.isBlackjack) {
                                    this.component.gameResultModal.showResult(this.hand2Result);
                                }
                                return [4 /*yield*/, earnbetCommon.sleep(3000)];
                            case 13:
                                _b.sent();
                                this._showResult = false;
                                this.component.gameResultModal.close();
                                return [2 /*return*/];
                        }
                    });
                });
            };
        Object.defineProperty(BlackjackGameController.prototype, "isAbleToSplit", {
            get: /**
             * @return {?}
             */ function () {
                return !this.isSplit &&
                    this.hand1.isAbleToSplit;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isAbleToDoubleDown", {
            get: /**
             * @return {?}
             */ function () {
                return this.currentHand.isAbleToDoubleDown;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "currentHand", {
            get: /**
             * @private
             * @return {?}
             */ function () {
                return this.selectedHandIndex == 0 ?
                    this.hand1 :
                    this.hand2;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isSplit", {
            get: /**
             * @return {?}
             */ function () {
                return this._isSplit;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isFirstHandSelected", {
            get: /**
             * @return {?}
             */ function () {
                return this.selectedHandIndex == 0 &&
                    this.isWaitingForAction;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isSecondHandSelected", {
            get: /**
             * @return {?}
             */ function () {
                return this.selectedHandIndex == 1 &&
                    this.isWaitingForAction;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isWaitingForAction", {
            get: /**
             * @return {?}
             */ function () {
                return this.isStarted &&
                    this.currentHand.isWaitingForAction &&
                    !this.server.isWaitingForResponse &&
                    !this.isGameOver;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isStarted", {
            get: /**
             * @return {?}
             */ function () {
                return this.isBetPlaced;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "bet1", {
            get: /**
             * @return {?}
             */ function () {
                return this.isBetPlaced ?
                    this.hand1.bet :
                    this.bet;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isLoading", {
            get: /**
             * @return {?}
             */ function () {
                return this.server.isWaitingForResponse || (this.isBetPlaced &&
                    !this.promptForInsurance &&
                    !this.isWaitingForAction);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isAbleToPlaceBet", {
            get: /**
             * @return {?}
             */ function () {
                return !this.isBetPlaced &&
                    !this.server.isWaitingForResponse;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isBetPlaced", {
            get: /**
             * @return {?}
             */ function () {
                return this.initialBetAmount != undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isGameOver", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand1Result != undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isDealerWin", {
            get: /**
             * @return {?}
             */ function () {
                return this.isSplit ?
                    (this.isLossForHand1 &&
                        this.isLossForHand2) :
                    this.isLossForHand1;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isDealerLoss", {
            get: /**
             * @return {?}
             */ function () {
                return this.dealer.isBusted ||
                    (this.isSplit ?
                        (this.isWinForHand1 &&
                            this.isWinForHand2) :
                        this.isWinForHand1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "amountWon1", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand1Result ?
                    this.hand1Result.amountWon :
                    0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "amountWon2", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand2Result ?
                    this.hand2Result.amountWon :
                    0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isWinForHand1", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand1Result && this.hand1Result.isWin;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "showLossForHand1", {
            get: /**
             * @return {?}
             */ function () {
                return this.isSplit ?
                    (this.isGameOver ?
                        this.isLossForHand1 && this.showResult :
                        this.hand1.isBusted) :
                    this.isLossForHand1;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isLossForHand1", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand1.isBusted ||
                    (this.hand1Result && this.hand1Result.isLoss);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isPushForHand1", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand1Result && this.hand1Result.isPush;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isWinForHand2", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand2Result && this.hand2Result.isWin;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isLossForHand2", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand2.isBusted ||
                    (this.hand2Result && this.hand2Result.isLoss);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "isPushForHand2", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand2Result && this.hand2Result.isPush;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "promptForInsurance", {
            get: /**
             * @return {?}
             */ function () {
                return this._promptForInsurance;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "choseInsurance", {
            get: /**
             * @return {?}
             */ function () {
                return this.hand1.boughtInsurance;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackGameController.prototype, "showResult", {
            get: /**
             * @return {?}
             */ function () {
                return this._showResult;
            },
            enumerable: true,
            configurable: true
        });
        return BlackjackGameController;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/util/cards-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @return {?}
     */
    function generateRandomCardId() {
        return getRandomIntegerBetween0AndX(51);
    }
    /**
     * @param {?} x
     * @return {?}
     */
    function getRandomIntegerBetween0AndX(x) {
        return Math.floor(Math.random() * x);
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/server/local-server.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LocalBlackjackServer = /** @class */ (function () {
        function LocalBlackjackServer(component, soundPlayer) {
            this.component = component;
            this.isWaitingForResponse = false;
            this.dealer = new BlackjackDealerHand(soundPlayer);
        }
        /**
         * @return {?}
         */
        LocalBlackjackServer.prototype.resumeExistingGame = /**
         * @return {?}
         */
            function () { };
        /**
         * @param {?} betAmount
         * @return {?}
         */
        LocalBlackjackServer.prototype.placeBet = /**
         * @param {?} betAmount
         * @return {?}
         */
            function (betAmount) {
                /** @type {?} */
                var dealersCard1 = generateRandomCardId();
                /** @type {?} */
                var dealersCard2 = generateRandomCardId();
                //const dealersCard1 = 1;
                //const dealersCard2 = 10;
                this.dealer.reset();
                this.dealer.dealFirstCard(dealersCard1);
                this.dealer.revealSecondCard(dealersCard2);
                /** @type {?} */
                var playersCard1 = generateRandomCardId();
                /** @type {?} */
                var playersCard2 = generateRandomCardId();
                //const playersCard1 = generateRandomCardId();
                //const playersCard2 = playersCard1;
                this.component.game.start({
                    playerCards: [playersCard1, playersCard2],
                    dealerCards: [dealersCard1]
                }, Number(betAmount.decimal));
            };
        /**
         * @param {?} yes
         * @return {?}
         */
        LocalBlackjackServer.prototype.respondToInsurance = /**
         * @param {?} yes
         * @return {?}
         */
            function (yes) {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2 /*return*/, this.dealer.isBlackJack];
                    });
                });
            };
        /**
         * @return {?}
         */
        LocalBlackjackServer.prototype.peekForBlackjack = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2 /*return*/, this.dealer.isBlackJack];
                    });
                });
            };
        /**
         * @return {?}
         */
        LocalBlackjackServer.prototype.getDealersSecondCard = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2 /*return*/, this.dealer.cards[1].cardId];
                    });
                });
            };
        /**
         * @return {?}
         */
        LocalBlackjackServer.prototype.split = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2 /*return*/, [
                                generateRandomCardId(),
                                generateRandomCardId()
                            ]];
                    });
                });
            };
        /**
         * @return {?}
         */
        LocalBlackjackServer.prototype.doubleDown = /**
         * @return {?}
         */
            function () {
                return this.hit();
            };
        /**
         * @return {?}
         */
        LocalBlackjackServer.prototype.hit = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2 /*return*/, generateRandomCardId()];
                    });
                });
            };
        /**
         * @return {?}
         */
        LocalBlackjackServer.prototype.stand = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        // do nothing
                        return [2 /*return*/, true];
                    });
                });
            };
        /**
         * @return {?}
         */
        LocalBlackjackServer.prototype.getDealersAdditionalCards = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var cards, i;
                    return __generator(this, function (_a) {
                        cards = [];
                        for (i = 0; i < 8; i++) {
                            cards.push(generateRandomCardId());
                        }
                        return [2 /*return*/, cards];
                    });
                });
            };
        return LocalBlackjackServer;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/server/remote-server.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var RemoteBlackjackServerBridge = /** @class */ (function () {
        function RemoteBlackjackServerBridge(component, accountNames, app) {
            this.component = component;
            this.accountNames = accountNames;
            this.app = app;
            this.actionId = 0;
            this._isWaitingForResponse = false;
            this.playersAdditionalCardIndex = undefined;
            this.isResuming = false;
        }
        /**
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.resumeExistingGame = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var e_1, _a, wallet, state, _b, _c, command, parts, commandName, _d, e_1_1;
                    return __generator(this, function (_e) {
                        switch (_e.label) {
                            case 0:
                                wallet = this.wallet;
                                if (!wallet.isLoggedIn ||
                                    this.isResuming) {
                                    return [2 /*return*/];
                                }
                                this.isResuming = true;
                                return [4 /*yield*/, this.sendGameCommand(earnbetCommon.BlackjackGameCommand.RESUME_GAME, this.accountData)];
                            case 1:
                                state = _e.sent();
                                console.log('Resumed Game State:');
                                console.log(state);
                                this.app.state.bets.setMyBetId(state.id);
                                return [4 /*yield*/, this.startGame(state)];
                            case 2:
                                _e.sent();
                                // *** START SYNCING: ***
                                this.playersAdditionalCardIndex = 0;
                                _e.label = 3;
                            case 3:
                                _e.trys.push([3, 16, 17, 18]);
                                _b = __values(state.commands), _c = _b.next();
                                _e.label = 4;
                            case 4:
                                if (!!_c.done)
                                    return [3 /*break*/, 15];
                                command = _c.value;
                                this.actionId++;
                                parts = command.split("~");
                                commandName = parts[0];
                                _d = commandName;
                                switch (_d) {
                                    case earnbetCommon.BlackjackGameCommand.YES_TO_INSURANCE: return [3 /*break*/, 5];
                                    case earnbetCommon.BlackjackGameCommand.NO_TO_INSURANCE: return [3 /*break*/, 5];
                                    case earnbetCommon.BlackjackGameCommand.SPLIT: return [3 /*break*/, 6];
                                    case earnbetCommon.BlackjackGameCommand.HIT: return [3 /*break*/, 8];
                                    case earnbetCommon.BlackjackGameCommand.DOUBLE_DOWN: return [3 /*break*/, 10];
                                    case earnbetCommon.BlackjackGameCommand.STAND: return [3 /*break*/, 12];
                                }
                                return [3 /*break*/, 14];
                            case 5:
                                this.game.respondToInsurance(commandName == earnbetCommon.BlackjackGameCommand.YES_TO_INSURANCE);
                                return [3 /*break*/, 14];
                            case 6: return [4 /*yield*/, this.game.split()];
                            case 7:
                                _e.sent();
                                return [3 /*break*/, 14];
                            case 8: return [4 /*yield*/, this.game.hit()];
                            case 9:
                                _e.sent();
                                return [3 /*break*/, 14];
                            case 10: return [4 /*yield*/, this.game.doubleDown()];
                            case 11:
                                _e.sent();
                                return [3 /*break*/, 14];
                            case 12: return [4 /*yield*/, this.game.stand()];
                            case 13:
                                _e.sent();
                                return [3 /*break*/, 14];
                            case 14:
                                _c = _b.next();
                                return [3 /*break*/, 4];
                            case 15: return [3 /*break*/, 18];
                            case 16:
                                e_1_1 = _e.sent();
                                e_1 = { error: e_1_1 };
                                return [3 /*break*/, 18];
                            case 17:
                                try {
                                    if (_c && !_c.done && (_a = _b.return))
                                        _a.call(_b);
                                }
                                finally {
                                    if (e_1)
                                        throw e_1.error;
                                }
                                return [7 /*endfinally*/];
                            case 18:
                                // *** STOP SYNCING: ***
                                this.playersAdditionalCardIndex = undefined;
                                this.isResuming = false;
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @param {?} betAmount
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.placeBet = /**
         * @param {?} betAmount
         * @return {?}
         */
            function (betAmount) {
                var _this = this;
                if (this.component.isInsufficientFunds) {
                    this.app.openInsufficentFundsModal();
                    return;
                }
                this._isWaitingForResponse = true;
                /** @type {?} */
                var component = this.component;
                /** @type {?} */
                var wallet = component.wallet;
                /** @type {?} */
                var memo = String(betAmount.integer) + "-" + wallet.publicKey + "-" + component.state.referrer + "-" + component.getSeed(false);
                big_js.Big.RM = 0 /* RoundDown */;
                /** @type {?} */
                var amtDeposit = new big_js.Big(betAmount.decimal).times("4.5");
                /** @type {?} */
                var amtBalance = component.wallet.getBalance(component.selectedToken.symbol);
                /** @type {?} */
                var precision = component.selectedToken.precision;
                if (amtDeposit.gt(amtBalance)) {
                    // If there is not enough funds for 4.5 then do not deposit the uneven remainder that cant be used
                    /** @type {?} */
                    var factor = new big_js.Big(amtBalance).div(betAmount.decimal).toFixed(0);
                    amtDeposit = new big_js.Big(betAmount.decimal).times(factor);
                }
                /** @type {?} */
                var depositAmount = component.selectedToken.getAssetAmount(amtDeposit.toFixed(precision));
                /** @type {?} */
                var eosAccounts = this.app.eos.accounts;
                /** @type {?} */
                var transfers = [{
                        toAccount: eosAccounts.games.blackjack,
                        amount: depositAmount,
                        memo: memo
                    }];
                if (component.isJackpotBet) {
                    transfers.push({
                        toAccount: eosAccounts.jackpot,
                        amount: component.selectedToken.jackpotTicketPrice,
                        memo: eosAccounts.games.blackjack
                    });
                }
                /** @type {?} */
                var promise = wallet.transfer(transfers);
                promise.then(( /**
                 * @param {?} result
                 * @return {?}
                 */function (result) {
                    /** @type {?} */
                    var txnId = result.transaction_id;
                    _this.placedBetId = component.state.bets.onBetPlaced(txnId);
                    _this.component.trackBetWithGoogleAnalytics('blackjack', betAmount.decimal, txnId, false);
                    _this._isWaitingForResponse = false;
                    _this.waitForGame();
                }), ( /**
                 * @param {?} errorData
                 * @return {?}
                 */function (errorData) {
                    _this._isWaitingForResponse = false;
                    /** @type {?} */
                    var error = new EosErrorParser(errorData);
                    component.alert.error(error.message);
                }));
            };
        /**
         * @private
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.waitForGame = /**
         * @private
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var state;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this._isWaitingForResponse = true;
                                return [4 /*yield*/, this.sendGameCommand(earnbetCommon.BlackjackGameCommand.START_GAME, this.accountData)];
                            case 1:
                                state = _a.sent();
                                return [4 /*yield*/, this.startGame(state)];
                            case 2:
                                _a.sent();
                                this.isResuming = false;
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @private
         * @param {?} state
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.startGame = /**
         * @private
         * @param {?} state
         * @return {?}
         */
            function (state) {
                return __awaiter(this, void 0, void 0, function () {
                    var token;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this.actionId = 0;
                                this._isWaitingForResponse = false;
                                //this.isDealerBlackjack = state.isDealerBlackjack;
                                this.dealersAdditionalCards = state.additionalDealerCards;
                                this.state = state;
                                token = state.initialBet.token;
                                return [4 /*yield*/, this.game.start(state.initialCards, state.initialBet.amountAsInteger /
                                        Math.pow(10, token.precision))];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @param {?} yes
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.respondToInsurance = /**
         * @param {?} yes
         * @return {?}
         */
            function (yes) {
                return __awaiter(this, void 0, void 0, function () {
                    var response, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (!this.isSyncing)
                                    return [3 /*break*/, 1];
                                _a = this.state;
                                return [3 /*break*/, 3];
                            case 1: return [4 /*yield*/, this.sendGameCommand(yes ?
                                    earnbetCommon.BlackjackGameCommand.YES_TO_INSURANCE :
                                    earnbetCommon.BlackjackGameCommand.NO_TO_INSURANCE)];
                            case 2:
                                _a = _b.sent();
                                _b.label = 3;
                            case 3:
                                response = _a;
                                return [2 /*return*/, response.isDealerBlackjack];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.peekForBlackjack = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var response, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (!(this.isSyncing || this.state.isDealerBlackjack != undefined))
                                    return [3 /*break*/, 1];
                                _a = this.state;
                                return [3 /*break*/, 3];
                            case 1: return [4 /*yield*/, this.sendGameCommand(earnbetCommon.BlackjackGameCommand.PEEK_FOR_BLACKJACK)];
                            case 2:
                                _a = _b.sent();
                                _b.label = 3;
                            case 3:
                                response = _a;
                                return [2 /*return*/, response.isDealerBlackjack];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.split = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var response, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (!this.isSyncing)
                                    return [3 /*break*/, 1];
                                _a = this.getAdditionalPlayerCards(2);
                                return [3 /*break*/, 3];
                            case 1: return [4 /*yield*/, this.sendGameCommand(earnbetCommon.BlackjackGameCommand.SPLIT)];
                            case 2:
                                _a = _b.sent();
                                _b.label = 3;
                            case 3:
                                response = _a;
                                return [2 /*return*/, response];
                        }
                    });
                });
            };
        /**
         * @param {?} handIndex
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.hit = /**
         * @param {?} handIndex
         * @return {?}
         */
            function (handIndex) {
                return __awaiter(this, void 0, void 0, function () {
                    var response, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (!this.isSyncing)
                                    return [3 /*break*/, 1];
                                _a = this.getAdditionalPlayerCards(1);
                                return [3 /*break*/, 3];
                            case 1: return [4 /*yield*/, this.sendGameCommand(earnbetCommon.BlackjackGameCommand.HIT)];
                            case 2:
                                _a = _b.sent();
                                _b.label = 3;
                            case 3:
                                response = _a;
                                return [2 /*return*/, response[0]];
                        }
                    });
                });
            };
        /**
         * @param {?} handIndex
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.doubleDown = /**
         * @param {?} handIndex
         * @return {?}
         */
            function (handIndex) {
                return __awaiter(this, void 0, void 0, function () {
                    var response, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (!this.isSyncing)
                                    return [3 /*break*/, 1];
                                _a = this.getAdditionalPlayerCards(1);
                                return [3 /*break*/, 3];
                            case 1: return [4 /*yield*/, this.sendGameCommand(earnbetCommon.BlackjackGameCommand.DOUBLE_DOWN)];
                            case 2:
                                _a = _b.sent();
                                _b.label = 3;
                            case 3:
                                response = _a;
                                return [2 /*return*/, response[0]];
                        }
                    });
                });
            };
        /**
         * @private
         * @param {?} numOfCards
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.getAdditionalPlayerCards = /**
         * @private
         * @param {?} numOfCards
         * @return {?}
         */
            function (numOfCards) {
                /** @type {?} */
                var cardIds = [];
                for (var i = 0; i < numOfCards; i++) {
                    /** @type {?} */
                    var index = this.playersAdditionalCardIndex++;
                    /** @type {?} */
                    var card = this.state.additionalPlayerCards[index];
                    cardIds.push(card);
                }
                return cardIds;
            };
        /**
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.stand = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!!this.isSyncing)
                                    return [3 /*break*/, 2];
                                return [4 /*yield*/, this.sendGameCommand(earnbetCommon.BlackjackGameCommand.STAND)];
                            case 1: return [2 /*return*/, _a.sent()];
                            case 2: return [2 /*return*/, false];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.getDealersSecondCard = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var cards, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                if (!this.isSyncing)
                                    return [3 /*break*/, 1];
                                _a = this.state.additionalDealerCards;
                                return [3 /*break*/, 3];
                            case 1: return [4 /*yield*/, this.sendGameCommand(earnbetCommon.BlackjackGameCommand.GET_DEALERS_CARDS)];
                            case 2:
                                _a = _b.sent();
                                _b.label = 3;
                            case 3:
                                cards = _a;
                                this.dealersAdditionalCards = cards.slice(1);
                                return [2 /*return*/, cards[0]];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.getDealersAdditionalCards = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2 /*return*/, this.dealersAdditionalCards];
                    });
                });
            };
        /**
         * @private
         * @template T
         * @param {?} commandName
         * @param {?=} args
         * @return {?}
         */
        RemoteBlackjackServerBridge.prototype.sendGameCommand = /**
         * @private
         * @template T
         * @param {?} commandName
         * @param {?=} args
         * @return {?}
         */
            function (commandName, args) {
                var _this = this;
                if (args === void 0) {
                    args = [];
                }
                return new Promise(( /**
                 * @param {?} resolve
                 * @param {?} reject
                 * @return {?}
                 */function (resolve, reject) {
                    /** @type {?} */
                    var isResumeGameCommand = commandName == earnbetCommon.BlackjackGameCommand.RESUME_GAME;
                    /** @type {?} */
                    var isStartGameCommand = commandName == earnbetCommon.BlackjackGameCommand.START_GAME;
                    /** @type {?} */
                    var actionId = isResumeGameCommand || isStartGameCommand ?
                        0 :
                        ++_this.actionId;
                    /** @type {?} */
                    var parts = [
                        commandName,
                        isStartGameCommand ?
                            _this.placedBetId :
                            _this.gameId,
                        actionId
                    ].concat(args);
                    // data format:commandName~gameId~actionId~data...
                    /** @type {?} */
                    var data = parts.join('~');
                    if (!isResumeGameCommand) {
                        _this._isWaitingForResponse = true;
                    }
                    _this.wallet.sign(data, '')
                        .then(( /**
                 * @param {?} info
                 * @return {?}
                 */function (info) {
                        //console.log('Sending Game Command: ')
                        //console.log(data);
                        _this.app.blackjack.client.messageManager.sendGameCommand(data, info).then(( /**
                         * @param {?} result
                         * @return {?}
                         */function (result) {
                            _this._isWaitingForResponse = false;
                            resolve(result);
                        })).catch(( /**
                         * @param {?} error
                         * @return {?}
                         */function (error) {
                            _this._isWaitingForResponse = false;
                            _this.alert.error(error.code);
                            reject(error);
                        }));
                    })).catch(( /**
                     * @param {?} error
                     * @return {?}
                     */function (error) {
                        _this._isWaitingForResponse = false;
                        /** @type {?} */
                        var e = new EosErrorParser(error);
                        _this.alert.error(e.message);
                        reject(error);
                    }));
                }));
            };
        Object.defineProperty(RemoteBlackjackServerBridge.prototype, "isSyncing", {
            get: /**
             * @private
             * @return {?}
             */ function () {
                return this.playersAdditionalCardIndex != undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RemoteBlackjackServerBridge.prototype, "gameId", {
            get: /**
             * @private
             * @return {?}
             */ function () {
                return this.state ?
                    this.state.id :
                    '0';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RemoteBlackjackServerBridge.prototype, "isWaitingForResponse", {
            get: /**
             * @return {?}
             */ function () {
                return this._isWaitingForResponse;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RemoteBlackjackServerBridge.prototype, "game", {
            get: /**
             * @private
             * @return {?}
             */ function () {
                return this.component.game;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RemoteBlackjackServerBridge.prototype, "accountData", {
            get: /**
             * @private
             * @return {?}
             */ function () {
                /** @type {?} */
                var wallet = this.wallet;
                return wallet.isEasyAccount ?
                    [this.accountNames.easyAccount, wallet.accountId, wallet.accountName] :
                    [wallet.accountName];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RemoteBlackjackServerBridge.prototype, "wallet", {
            get: /**
             * @private
             * @return {?}
             */ function () {
                return this.app.wallet;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RemoteBlackjackServerBridge.prototype, "alert", {
            get: /**
             * @private
             * @return {?}
             */ function () {
                return this.app.alert;
            },
            enumerable: true,
            configurable: true
        });
        return RemoteBlackjackServerBridge;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/main/blackjack.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /*
    @Component({
        selector: 'app-blackjack',
        templateUrl: './blackjack.component.html',
        encapsulation: ViewEncapsulation.None,
        styleUrls: ['./blackjack.component.scss']
    })
    */
    var BlackjackComponentBase = /** @class */ (function (_super) {
        __extends(BlackjackComponentBase, _super);
        function BlackjackComponentBase(app, renderer, modal, googleAnalytics, soundPlayer, accountNames) {
            var _this = _super.call(this, app, googleAnalytics, soundPlayer) || this;
            _this.renderer = renderer;
            _this.modal = modal;
            // *** For Testing: ***
            //this.useLocalServer();
            _this.useRemoteServer(accountNames);
            _this.game = new BlackjackGameController(_this, _this.server, soundPlayer);
            _this.setListeners();
            return _this;
        }
        /**
         * @private
         * @param {?} soundPlayer
         * @return {?}
         */
        BlackjackComponentBase.prototype.useLocalServer = /**
         * @private
         * @param {?} soundPlayer
         * @return {?}
         */
            function (soundPlayer) {
                this.server = new LocalBlackjackServer(this, soundPlayer);
            };
        /**
         * @private
         * @param {?} acountNames
         * @return {?}
         */
        BlackjackComponentBase.prototype.useRemoteServer = /**
         * @private
         * @param {?} acountNames
         * @return {?}
         */
            function (acountNames) {
                this.server = new RemoteBlackjackServerBridge(this, acountNames, this.app);
            };
        /**
         * @private
         * @return {?}
         */
        BlackjackComponentBase.prototype.setListeners = /**
         * @private
         * @return {?}
         */
            function () {
                this.app.state.setListener(this);
                this.app.blackjack.client.setListener(this);
                this.app.state.bets.setListener(this);
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.onBlackjackClientConnected = /**
         * @return {?}
         */
            function () {
                this.resumeGame();
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.onAuthenticated = /**
         * @return {?}
         */
            function () {
                this.resumeGame();
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.ngAfterViewInit = /**
         * @return {?}
         */
            function () {
                this.renderer.addClass(document.body, 'blackjack-bg');
                this.resumeGame();
                // *** blackjack deposit modal ***
                //this.modal.open('blackjackGameNoticeModal');
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.renderer.removeClass(document.body, 'blackjack-bg');
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.debug = /**
         * @return {?}
         */
            function () {
                console.log(this.game);
            };
        /**
         * @private
         * @return {?}
         */
        BlackjackComponentBase.prototype.resumeGame = /**
         * @private
         * @return {?}
         */
            function () {
                if (this.app.blackjack.client.isConnected) {
                    this.server.resumeExistingGame();
                }
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.onBettingCurrencyChanged = /**
         * @return {?}
         */
            function () {
                this.clearTable();
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.onChipSelected = /**
         * @return {?}
         */
            function () {
                this.addChip();
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.halfOfBetAmount = /**
         * @return {?}
         */
            function () {
                if (this.isBetPlaced) {
                    return;
                }
                /** @type {?} */
                var minimumBet = Number(this.selectedToken.minimumBetAmount);
                if (this.game.bet.amount == minimumBet) {
                    return;
                }
                this.game.bet.half();
                if (this.game.bet.amount < minimumBet) {
                    this.game.bet.amount = minimumBet;
                }
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.doubleBetAmount = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var maxBet;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this.isBetPlaced) {
                                    return [2 /*return*/];
                                }
                                return [4 /*yield*/, this.getMaxBet()];
                            case 1:
                                maxBet = _a.sent();
                                if (this.game.bet.amount == maxBet) {
                                    return [2 /*return*/];
                                }
                                this.game.bet.double();
                                if (this.game.bet.amount > maxBet) {
                                    this.game.bet.setToMax(maxBet);
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.addChip = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var amount, _a, _b;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                if (this.isBetPlaced) {
                                    return [2 /*return*/];
                                }
                                if (!!this.isMaxChipSelected)
                                    return [3 /*break*/, 1];
                                amount = this.chips[this.selectedChip];
                                this.game.bet.add(amount);
                                return [3 /*break*/, 3];
                            case 1:
                                _b = (_a = this.game.bet).setToMax;
                                return [4 /*yield*/, this.getMaxBet()];
                            case 2:
                                _b.apply(_a, [_c.sent()]);
                                _c.label = 3;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.removeChip = /**
         * @return {?}
         */
            function () {
                if (this.isBetPlaced) {
                    return;
                }
                if (!this.isMaxChipSelected) {
                    /** @type {?} */
                    var amount = this.chips[this.selectedChip];
                    this.game.bet.subtract(amount);
                }
                else {
                    this.game.bet.reset();
                }
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.clearTable = /**
         * @return {?}
         */
            function () {
                if (!this.isBetPlaced) {
                    this.game.bet.reset();
                }
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.placeBet = /**
         * @return {?}
         */
            function () {
                if (this.game.isStarted) {
                    return;
                }
                this.alert.clear();
                if (this.game.bet.amount == 0) {
                    this.alert.error('Place Chips on Table to Place Your Bet!');
                    return;
                }
                // *** BROADCAST BET TRANSACTION TO BLOCK CHAIN ***
                this.server.placeBet(this.betAmount);
            };
        /**
         * @param {?} bet
         * @return {?}
         */
        BlackjackComponentBase.prototype.onMyBetResolved = /**
         * @param {?} bet
         * @return {?}
         */
            function (bet) {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, _b;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0: return [4 /*yield*/, earnbetCommon.sleep(2000)];
                            case 1:
                                _c.sent();
                                return [4 /*yield*/, this.updateBalances(bet)];
                            case 2:
                                _c.sent();
                                if (!(!this.isBetPlaced &&
                                    this.game.bet.isMax))
                                    return [3 /*break*/, 4];
                                _b = (_a = this.game.bet).setToMax;
                                return [4 /*yield*/, this.getMaxBet()];
                            case 3:
                                _b.apply(_a, [_c.sent()]);
                                _c.label = 4;
                            case 4: return [2 /*return*/];
                        }
                    });
                });
            };
        /**
         * @return {?}
         */
        BlackjackComponentBase.prototype.getMaxBet = /**
         * @return {?}
         */
            function () {
                return this.bankroll.getMaxBet(4);
            };
        Object.defineProperty(BlackjackComponentBase.prototype, "betAmount", {
            get: /**
             * @return {?}
             */ function () {
                return this.selectedToken.getAssetAmount(this.game.bet.amount.toString());
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackComponentBase.prototype, "isSplit", {
            get: /**
             * @return {?}
             */ function () {
                return this.game.isSplit;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackComponentBase.prototype, "isBetPlaced", {
            get: /**
             * @return {?}
             */ function () {
                return this.game.isBetPlaced;
            },
            enumerable: true,
            configurable: true
        });
        BlackjackComponentBase.propDecorators = {
            gameResultModal: [{ type: core.ViewChild, args: ['gameResultModal',] }]
        };
        return BlackjackComponentBase;
    }(GameComponentBase));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/chip-stack/chip-stack.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /*
    @Component({
        selector: 'bj-chip-stack',
        templateUrl: './chip-stack.component.html',
        styleUrls: ['./chip-stack.component.scss']
    })
    */
    var BlackJackChipStackComponentBase = /** @class */ (function () {
        function BlackJackChipStackComponentBase(app) {
            this.app = app;
            this.isMax = false;
            this.amount = 0;
            this.chipstack = [];
        }
        /**
         * @return {?}
         */
        BlackJackChipStackComponentBase.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        /**
         * @return {?}
         */
        BlackJackChipStackComponentBase.prototype.ngOnChanges = /**
         * @return {?}
         */
            function () {
                this.makeChipStack();
            };
        /**
         * @return {?}
         */
        BlackJackChipStackComponentBase.prototype.makeChipStack = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var chipstack = [];
                // is the amount the max for this bet type
                if (this.isMax) {
                    chipstack.push(this.chips.length);
                }
                else {
                    /** @type {?} */
                    var factor = 1 / this.chips[0];
                    /** @type {?} */
                    var n = Math.round(this.amount * factor);
                    for (var i = this.chips.length - 1; i >= 0; i--) {
                        if ((this.chips[i] * factor) <= n) {
                            /** @type {?} */
                            var r = Math.floor(n / (this.chips[i] * factor));
                            n -= this.chips[i] * factor * r;
                            for (var j = 0; j < r; j++) {
                                chipstack.push(i);
                            }
                        }
                    }
                }
                this.chipstack = chipstack;
            };
        Object.defineProperty(BlackJackChipStackComponentBase.prototype, "chips", {
            get: /**
             * @return {?}
             */ function () {
                return this.settings.selectedToken.chips;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackJackChipStackComponentBase.prototype, "settings", {
            get: /**
             * @return {?}
             */ function () {
                return this.app.settings;
            },
            enumerable: true,
            configurable: true
        });
        BlackJackChipStackComponentBase.propDecorators = {
            isMax: [{ type: core.Input }],
            amount: [{ type: core.Input }]
        };
        return BlackJackChipStackComponentBase;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/blackjack-rules/blackjack-rules.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /*
    @Component({
        selector: "blackjack-rules",
        templateUrl: "./blackjack-rules.component.html",
        styleUrls: ["./blackjack-rules.component.scss"]
    })
    */
    var /*
    @Component({
        selector: "blackjack-rules",
        templateUrl: "./blackjack-rules.component.html",
        styleUrls: ["./blackjack-rules.component.scss"]
    })
    */ BlackjackRulesComponentBase = /** @class */ (function () {
        function BlackjackRulesComponentBase(app, modal) {
            this.app = app;
            this.modal = modal;
        }
        Object.defineProperty(BlackjackRulesComponentBase.prototype, "translation", {
            get: /**
             * @return {?}
             */ function () {
                return this.app.settings.translation;
            },
            enumerable: true,
            configurable: true
        });
        return BlackjackRulesComponentBase;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/blackjack-deposit-modal/blackjack-deposit-modal.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /*
    @Component({
        selector: "blackjack-deposit-modal",
        templateUrl: "./blackjack-deposit-modal.component.html",
        styleUrls: ["./blackjack-deposit-modal.component.scss"]
    })
    */
    var /*
    @Component({
        selector: "blackjack-deposit-modal",
        templateUrl: "./blackjack-deposit-modal.component.html",
        styleUrls: ["./blackjack-deposit-modal.component.scss"]
    })
    */ BlackjackDepositModalBase = /** @class */ (function () {
        function BlackjackDepositModalBase(app, modal) {
            this.app = app;
            this.modal = modal;
        }
        Object.defineProperty(BlackjackDepositModalBase.prototype, "translation", {
            get: /**
             * @return {?}
             */ function () {
                return this.app.settings.translation;
            },
            enumerable: true,
            configurable: true
        });
        return BlackjackDepositModalBase;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/components/blackjack-card/blackjack-card.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /*
    @Component({
        selector: '[blackjack-card]',
        templateUrl: './blackjack-card.component.html',
        styleUrls: ['./blackjack-card.component.scss']
    })
    */
    var BlackjackCardComponentBase = /** @class */ (function () {
        function BlackjackCardComponentBase() {
            this.direction = 'right';
            this.history = false;
        }
        /**
         * @return {?}
         */
        BlackjackCardComponentBase.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(BlackjackCardComponentBase.prototype, "suit", {
            get: /**
             * @return {?}
             */ function () {
                return this.card ?
                    this.card.suit :
                    undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackCardComponentBase.prototype, "rank", {
            get: /**
             * @return {?}
             */ function () {
                return this.card ?
                    this.card.card :
                    undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackCardComponentBase.prototype, "flipped", {
            get: /**
             * @return {?}
             */ function () {
                return this.card ?
                    this.card.flipped :
                    undefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackCardComponentBase.prototype, "dealt", {
            get: /**
             * @return {?}
             */ function () {
                return this.card ?
                    this.card.dealt :
                    undefined;
            },
            enumerable: true,
            configurable: true
        });
        BlackjackCardComponentBase.propDecorators = {
            card: [{ type: core.Input }],
            direction: [{ type: core.Input }],
            history: [{ type: core.Input }]
        };
        return BlackjackCardComponentBase;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/_common/models/bets/blackjack-bet.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlackjackBetResult = /** @class */ (function (_super) {
        __extends(BlackjackBetResult, _super);
        function BlackjackBetResult(data) {
            var _this = _super.call(this, data) || this;
            _this.game = 'blackjack';
            _this.description = describeResults(data.playerResults, data.dealerHandResult);
            /** @type {?} */
            var totalWagered = new AssetAmount(data.totalWagered);
            /** @type {?} */
            var totalPayout = new AssetAmount(data.totalPayout);
            _this.payout = totalPayout.decimalWithoutTrailingZeros;
            _this.isWin = totalPayout.integer.gte(totalWagered.integer);
            _this.profit = Number(totalPayout.subtractAndReplace(totalWagered).decimal);
            return _this;
        }
        return BlackjackBetResult;
    }(BetBase));
    /**
     * @param {?} playerResults
     * @param {?} dealerHandResult
     * @return {?}
     */
    function describeResults(playerResults, dealerHandResult) {
        var e_1, _a;
        /** @type {?} */
        var parts = [];
        try {
            for (var playerResults_1 = __values(playerResults), playerResults_1_1 = playerResults_1.next(); !playerResults_1_1.done; playerResults_1_1 = playerResults_1.next()) {
                var result = playerResults_1_1.value;
                parts.push(describeResult(result, dealerHandResult));
            }
        }
        catch (e_1_1) {
            e_1 = { error: e_1_1 };
        }
        finally {
            try {
                if (playerResults_1_1 && !playerResults_1_1.done && (_a = playerResults_1.return))
                    _a.call(playerResults_1);
            }
            finally {
                if (e_1)
                    throw e_1.error;
            }
        }
        return parts.join(' ~ ');
    }
    /**
     * @param {?} player
     * @param {?} dealer
     * @return {?}
     */
    function describeResult(player, dealer) {
        /** @type {?} */
        var left = '' + player.sum;
        /** @type {?} */
        var sign;
        /** @type {?} */
        var right = '' + dealer.sum;
        if (player.isBlackjack) {
            left = 'BJ';
        }
        if (dealer.isBlackjack) {
            right = 'BJ';
        }
        if (player.sum > 21) {
            sign = '>';
            right = '21';
        }
        else if (player.sum < dealer.sum) {
            sign = '<';
        }
        else if (player.sum > dealer.sum) {
            sign = '>';
        }
        else {
            // sums are equal (it may be a push) 
            if (player.sum < 21) {
                // PUSH if less than 21
                sign = '=';
            }
            else {
                // BOTH SUMS ARE EQUAL TO 21
                // PUSH if both have BJ
                if (player.isBlackjack && dealer.isBlackjack) {
                    sign = '=';
                }
                else if (player.isBlackjack) {
                    sign = '>';
                }
                else {
                    // dealer blackjack
                    sign = '<';
                }
            }
        }
        return left + ' ' + sign + ' ' + right;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/server/bj-client.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NAMESPACE = '/blackjack';
    var BlackjackClient = /** @class */ (function (_super) {
        __extends(BlackjackClient, _super);
        function BlackjackClient(app) {
            var _this = _super.call(this, {
                host: 'bjs.eosbet.io',
                port: 443,
                ssl: true
            }, NAMESPACE) || this;
            _this.app = app;
            _this._isConnected = false;
            return _this;
        }
        /**
         * @return {?}
         */
        BlackjackClient.prototype.connect = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var socket = this.connectSocket();
                this._messageManager = new BlackjackMessageManager(socket, this);
            };
        /**
         * @param {?} value
         * @return {?}
         */
        BlackjackClient.prototype.setListener = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.listener = value;
            };
        /**
         * @protected
         * @return {?}
         */
        BlackjackClient.prototype.onConnected = /**
         * @protected
         * @return {?}
         */
            function () {
                this._isConnected = true;
                console.log('blackjack client connected to Blackjack Server!');
                if (this.listener) {
                    this.listener.onBlackjackClientConnected();
                }
            };
        /*** MESSAGE LISTENERS: ***/
        /**
         * MESSAGE LISTENERS: **
         * @param {?} data
         * @return {?}
         */
        BlackjackClient.prototype.onResolveBetResult = /**
         * MESSAGE LISTENERS: **
         * @param {?} data
         * @return {?}
         */
            function (data) {
                this.app.state.bets.onBetResolved(new BlackjackBetResult(data));
            };
        /***/
        /**
         *
         * @protected
         * @return {?}
         */
        BlackjackClient.prototype.onDisconnected = /**
         *
         * @protected
         * @return {?}
         */
            function () {
                this._isConnected = false;
            };
        Object.defineProperty(BlackjackClient.prototype, "messageManager", {
            get: /**
             * @return {?}
             */ function () {
                return this._messageManager;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BlackjackClient.prototype, "isConnected", {
            get: /**
             * @return {?}
             */ function () {
                return this._isConnected;
            },
            enumerable: true,
            configurable: true
        });
        return BlackjackClient;
    }(earnbetCommonFrontEnd.SocketClientBase));
    var BlackjackMessageManager = /** @class */ (function (_super) {
        __extends(BlackjackMessageManager, _super);
        function BlackjackMessageManager(socket, client) {
            return _super.call(this, socket, earnbetCommon.BlackjackMessage.PREFIX, client) || this;
        }
        /**
         * @template T
         * @param {?} data
         * @param {?} info
         * @return {?}
         */
        BlackjackMessageManager.prototype.sendGameCommand = /**
         * @template T
         * @param {?} data
         * @param {?} info
         * @return {?}
         */
            function (data, info) {
                return this.makeRequest(earnbetCommon.BlackjackMessage.GAME_COMMAND, data, info);
            };
        return BlackjackMessageManager;
    }(earnbetCommon.SocketMessageManager));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/blackjack/services/blackjack-service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlackjackAppService = /** @class */ (function () {
        function BlackjackAppService(app) {
            this.client = new BlackjackClient(app);
        }
        /**
         * @return {?}
         */
        BlackjackAppService.prototype.init = /**
         * @return {?}
         */
            function () {
                this.client.connect();
            };
        return BlackjackAppService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: earnbet-common-angular.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.BlackjackComponentBase = BlackjackComponentBase;
    exports.BlackJackChipStackComponentBase = BlackJackChipStackComponentBase;
    exports.BlackjackRulesComponentBase = BlackjackRulesComponentBase;
    exports.BlackjackDepositModalBase = BlackjackDepositModalBase;
    exports.BlackjackCardComponentBase = BlackjackCardComponentBase;
    exports.BlackjackModalComponent = BlackjackModalComponent;
    exports.BlackjackAppService = BlackjackAppService;
    exports.GameComponentBase = GameComponentBase;
    exports.EosErrorParser = EosErrorParser;
    exports.CardController = CardController;
    exports.TokenSymbol = TokenSymbol;
    exports.BetBase = BetBase;
    exports.getHouseEdgeReductionAsDecimal = getHouseEdgeReductionAsDecimal;
    exports.getStakingLevelName = getStakingLevelName;
    exports.getStakingLevelIndex = getStakingLevelIndex;
    exports.stakingLevels = stakingLevels;
    exports.betTokenFactor = betTokenFactor;
    exports.IntBasedNum = IntBasedNum;
    exports.AssetAmount = AssetAmount;
    exports.toTotalDigits = toTotalDigits;
    exports.numStringToTotalDigits = numStringToTotalDigits;
    exports.roundDownWithPrecision = roundDownWithPrecision;
    exports.roundDownWithoutTrailingZeros = roundDownWithoutTrailingZeros;
    exports.roundStringDownWithPrecision = roundStringDownWithPrecision;
    exports.OverlayModalService = OverlayModalService;
    exports.BlackjackModule = BlackjackModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=earnbet-common-angular.umd.js.map