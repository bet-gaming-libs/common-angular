/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/base/game-component-base.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Big } from 'big.js';
import { HostListener } from '@angular/core';
import { sleep } from 'earnbet-common';
import { TokenSymbol } from '../models/business-rules/tokens';
/** @type {?} */
const INDEX_OF_MAX_CHIP = 7;
/**
 * @record
 */
export function IBetAmount() { }
if (false) {
    /** @type {?} */
    IBetAmount.prototype.symbol;
    /** @type {?} */
    IBetAmount.prototype.decimal;
}
/**
 * @abstract
 */
export class GameComponentBase {
    /**
     * @param {?} app
     * @param {?} googleAnalytics
     * @param {?} soundPlayer
     */
    constructor(app, googleAnalytics, soundPlayer) {
        this.app = app;
        this.googleAnalytics = googleAnalytics;
        this.soundPlayer = soundPlayer;
        this.autoBetNonce = 0;
        this._isJackpotBet = false;
        this.selectedChip = 1;
        this.showTooltip = false;
        this.processBetStyle = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.app.scrollToTopOfPage();
        this.app.settings.setListener(this);
        if (this.selectedToken.symbol == 'BET') {
            this.settings.selectCurrency('EOS');
        }
    }
    /**
     * @param {?} i
     * @return {?}
     */
    selectChip(i) {
        this.selectedChip = i;
        this.soundPlayer.play('chipPlace');
        this.onChipSelected();
    }
    /**
     * @protected
     * @return {?}
     */
    onChipSelected() { }
    /**
     * @return {?}
     */
    toggleTooltip() {
        this.showTooltip = !this.showTooltip;
    }
    /**
     * @return {?}
     */
    documentClick() {
        this.showTooltip = false;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isJackpotBet(value) {
        if (this.isJackpotDisabled) {
            return;
        }
        this._isJackpotBet = value;
    }
    /**
     * @return {?}
     */
    get isJackpotBet() {
        return !this.isJackpotDisabled &&
            this._isJackpotBet;
    }
    /**
     * @return {?}
     */
    get isJackpotDisabled() {
        /** @type {?} */
        const amount = Number(this.betAmount.decimal);
        return amount < this.selectedToken.jackpot.minimumBet;
    }
    /**
     * @param {?} isAutoBet
     * @return {?}
     */
    getSeed(isAutoBet) {
        // seed for randomness
        /** @type {?} */
        const seed = this.state.seedForRandomness +
            // add nonce for auto-bet to fairness seed
            (isAutoBet || true ?
                ',' + (++this.autoBetNonce) :
                '');
        return seed;
    }
    /**
     * @param {?} gameName
     * @param {?} betAmount
     * @param {?} txnId
     * @param {?} isAutoBet
     * @return {?}
     */
    trackBetWithGoogleAnalytics(gameName, betAmount, txnId, isAutoBet) {
        /** @type {?} */
        const price = this.currency.getPrice(this.selectedToken.symbol);
        /** @type {?} */
        const amountInUSD = price ?
            (price * Number(betAmount)).toFixed(2) :
            undefined;
        if (amountInUSD) {
            this.googleAnalytics.addTransaction(txnId, gameName, this.selectedToken.symbol, amountInUSD, isAutoBet, this.isJackpotBet);
        }
    }
    /**
     * @protected
     * @param {?} result
     * @return {?}
     */
    updateBalances(result) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /*** CALCULATE NEW TOKEN BALANCES IN ADVANCE
             *  of the Bet being Resolved on-chain ***/
            /**
             * CALCULATE NEW TOKEN BALANCES IN ADVANCE
             *  of the Bet being Resolved on-chain **
             * @type {?}
             */
            const bet = Number(result.amount) /
                this.selectedToken.betTokenAirDropRate;
            /** @type {?} */
            let profit = result.profit;
            if (result.isJackpotBet) {
                profit -= Number(this.selectedToken.jackpot.ticketPrice);
            }
            /***/
            this.addToBalance = {
                main: profit,
                bet
            };
            // to animate balance in header
            this.app.state.onAddToBalance(true, profit);
            yield sleep(500);
            /*** UPDATE TOKEN BALANCES ***/
            // update main token balance
            this.wallet.addToBalance(this.selectedToken.symbol, profit);
            // update BET token balance
            this.wallet.addToBalance(TokenSymbol.BET, bet);
            /***/
            yield sleep(1500);
            this.addToBalance = undefined;
            this.app.state.onAddToBalance(false);
        });
    }
    // onTooltipShowBet(){
    //     document.getElementById("tooltipBet").setAttribute('data-tooltip', 'aaa');
    // }
    /**
     * @return {?}
     */
    get tooltipBet() {
        /** @type {?} */
        const tokenAlt = this.selectedToken.symbol;
        /** @type {?} */
        const airDrop = this.selectedToken.betTokenAirDropRate;
        return this.settings.translation.tooltipDividends("1", `${airDrop} ${tokenAlt}`);
    }
    /**
     * @return {?}
     */
    get jackpotToolTip() {
        /** @type {?} */
        const token = this.selectedToken;
        /** @type {?} */
        const jackpot = token.jackpot;
        return this.settings.translation.clickThisBoxToReceive1JackpotSpinForPrice(Number(jackpot.ticketPrice) + " " + token.symbol);
        // Currently not using a separate minimum bet for jackpot, 
        // it is currently the same as the standard minimum bet
        //"Bet must be at least "+jackpot.minimumBet+" "+token.symbol+" to enter.";
    }
    /**
     * @return {?}
     */
    get isInsufficientFunds() {
        return new Big(this.currencyBalance).lt(this.selectedToken.minimumBetAmount) ||
            new Big(this.betAmount.decimal).gt(this.currencyBalance);
    }
    /**
     * @return {?}
     */
    get isMaxChipSelected() {
        return this.selectedChip == INDEX_OF_MAX_CHIP;
    }
    /**
     * @return {?}
     */
    get currencyBalance() {
        return this.settings.selectedTokenBalance;
    }
    /**
     * @return {?}
     */
    get chips() {
        return this.selectedToken.chips;
    }
    /**
     * @return {?}
     */
    get selectedTokenPrice() {
        return this.currency.getPrice(this.selectedToken.symbol);
    }
    /**
     * @return {?}
     */
    get currency() {
        return this.app.currency;
    }
    /**
     * @return {?}
     */
    get selectedToken() {
        return this.settings.selectedToken;
    }
    /**
     * @return {?}
     */
    get wallet() {
        return this.settings.wallet;
    }
    /**
     * @return {?}
     */
    get state() {
        return this.app.state;
    }
    /**
     * @return {?}
     */
    get translation() {
        return this.settings.translation;
    }
    /**
     * @return {?}
     */
    get settings() {
        return this.app.settings;
    }
    /**
     * @return {?}
     */
    get alert() {
        return this.app.alert;
    }
    /**
     * @return {?}
     */
    get bankroll() {
        return this.app.bankroll;
    }
}
GameComponentBase.propDecorators = {
    documentClick: [{ type: HostListener, args: ['document:click', [],] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    GameComponentBase.prototype.autoBetNonce;
    /**
     * @type {?}
     * @private
     */
    GameComponentBase.prototype._isJackpotBet;
    /** @type {?} */
    GameComponentBase.prototype.selectedChip;
    /** @type {?} */
    GameComponentBase.prototype.showTooltip;
    /** @type {?} */
    GameComponentBase.prototype.processBetStyle;
    /** @type {?} */
    GameComponentBase.prototype.addToBalance;
    /**
     * @type {?}
     * @protected
     */
    GameComponentBase.prototype.app;
    /**
     * @type {?}
     * @protected
     */
    GameComponentBase.prototype.googleAnalytics;
    /**
     * @type {?}
     * @protected
     */
    GameComponentBase.prototype.soundPlayer;
    /**
     * @abstract
     * @return {?}
     */
    GameComponentBase.prototype.onBettingCurrencyChanged = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    GameComponentBase.prototype.betAmount = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS1jb21wb25lbnQtYmFzZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9iYXNlL2dhbWUtY29tcG9uZW50LWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUU3QixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTdDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU12QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUNBQWlDLENBQUM7O01BUXhELGlCQUFpQixHQUFHLENBQUM7Ozs7QUFHM0IsZ0NBSUM7OztJQUZHLDRCQUFjOztJQUNkLDZCQUFlOzs7OztBQUduQixNQUFNOzs7Ozs7SUFVRixZQUNjLEdBQW1CLEVBQ25CLGVBQXVDLEVBQ3ZDLFdBQXdCO1FBRnhCLFFBQUcsR0FBSCxHQUFHLENBQWdCO1FBQ25CLG9CQUFlLEdBQWYsZUFBZSxDQUF3QjtRQUN2QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVg5QixpQkFBWSxHQUFHLENBQUMsQ0FBQztRQUNqQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUM5QixpQkFBWSxHQUFHLENBQUMsQ0FBQztRQUNWLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLG9CQUFlLEdBQUcsS0FBSyxDQUFDO0lBUy9CLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRTdCLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVwQyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLEtBQUssRUFBRTtZQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7Ozs7O0lBSUQsVUFBVSxDQUFDLENBQVE7UUFDZixJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztRQUV0QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUVuQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFDUyxjQUFjLEtBQUksQ0FBQzs7OztJQUV0QixhQUFhO1FBQ2hCLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ3pDLENBQUM7Ozs7SUFHRCxhQUFhO1FBQ1QsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQzs7Ozs7SUFFRCxJQUFJLFlBQVksQ0FBQyxLQUFhO1FBQzFCLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQ3hCLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQy9CLENBQUM7Ozs7SUFDRCxJQUFJLFlBQVk7UUFDWixPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQjtZQUN0QixJQUFJLENBQUMsYUFBYSxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxJQUFJLGlCQUFpQjs7Y0FDWCxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDO1FBRTdDLE9BQU8sTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztJQUMxRCxDQUFDOzs7OztJQUlELE9BQU8sQ0FBQyxTQUFpQjs7O2NBRWYsSUFBSSxHQUNOLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCO1lBQzVCLDBDQUEwQztZQUMxQyxDQUNJLFNBQVMsSUFBSSxJQUFJLENBQUMsQ0FBQztnQkFDZixHQUFHLEdBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixFQUFFLENBQ1Q7UUFFTCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7Ozs7OztJQUVELDJCQUEyQixDQUN2QixRQUFlLEVBQ2YsU0FBZ0IsRUFDaEIsS0FBWSxFQUNaLFNBQWlCOztjQUVYLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FDaEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQzVCOztjQUVLLFdBQVcsR0FDYixLQUFLLENBQUMsQ0FBQztZQUNILENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLFNBQVM7UUFHakIsSUFBSSxXQUFXLEVBQUU7WUFDYixJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FDL0IsS0FBSyxFQUNMLFFBQVEsRUFDUixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFDekIsV0FBVyxFQUNYLFNBQVMsRUFDVCxJQUFJLENBQUMsWUFBWSxDQUNwQixDQUFDO1NBQ0w7SUFDTCxDQUFDOzs7Ozs7SUFFZSxjQUFjLENBQUMsTUFBaUI7O1lBQzVDO3VEQUMyQzs7Ozs7O2tCQUVyQyxHQUFHLEdBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1COztnQkFFdEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNO1lBRTFCLElBQUksTUFBTSxDQUFDLFlBQVksRUFBRTtnQkFDckIsTUFBTSxJQUFJLE1BQU0sQ0FDWixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQ3pDLENBQUM7YUFDTDtZQUNELEtBQUs7WUFHTCxJQUFJLENBQUMsWUFBWSxHQUFHO2dCQUNoQixJQUFJLEVBQUUsTUFBTTtnQkFDWixHQUFHO2FBQ04sQ0FBQztZQUVGLCtCQUErQjtZQUMvQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRTNDLE1BQU0sS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBR2pCLCtCQUErQjtZQUMvQiw0QkFBNEI7WUFDNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUN6QixNQUFNLENBQ1QsQ0FBQztZQUVGLDJCQUEyQjtZQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FDcEIsV0FBVyxDQUFDLEdBQUcsRUFDZixHQUFHLENBQ04sQ0FBQTtZQUNELEtBQUs7WUFHTCxNQUFNLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVsQixJQUFJLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQztZQUM5QixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekMsQ0FBQztLQUFBOzs7Ozs7O0lBS0QsSUFBSSxVQUFVOztjQUNKLFFBQVEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU07O2NBQ3BDLE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQjtRQUN0RCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBQyxHQUFHLE9BQU8sSUFBSSxRQUFRLEVBQUUsQ0FBQyxDQUFBO0lBQ25GLENBQUM7Ozs7SUFFRCxJQUFJLGNBQWM7O2NBQ1IsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhOztjQUMxQixPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU87UUFFN0IsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyx5Q0FBeUMsQ0FDdEUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBQyxHQUFHLEdBQUMsS0FBSyxDQUFDLE1BQU0sQ0FDL0MsQ0FBQztRQUNGLDJEQUEyRDtRQUN2RCx1REFBdUQ7UUFDM0QsMkVBQTJFO0lBQy9FLENBQUM7Ozs7SUFFRCxJQUFJLG1CQUFtQjtRQUNuQixPQUFPLElBQUksR0FBRyxDQUNWLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQ3hCLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQ3RDO1lBQ0QsSUFBSSxHQUFHLENBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQ3pCLENBQUMsRUFBRSxDQUNBLElBQUksQ0FBQyxlQUFlLENBQ3ZCLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsSUFBSSxpQkFBaUI7UUFDakIsT0FBTyxJQUFJLENBQUMsWUFBWSxJQUFJLGlCQUFpQixDQUFDO0lBQ2xELENBQUM7Ozs7SUFFRCxJQUFJLGVBQWU7UUFDZixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7SUFDcEMsQ0FBQzs7OztJQUVELElBQUksa0JBQWtCO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQ3pCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUM1QixDQUFDO0lBQ04sQ0FBQzs7OztJQUNELElBQUksUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUM7SUFDN0IsQ0FBQzs7OztJQUVELElBQUksYUFBYTtRQUNiLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7SUFDdkMsQ0FBQzs7OztJQUVELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7SUFDaEMsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7SUFDMUIsQ0FBQzs7OztJQUVELElBQUksV0FBVztRQUNYLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7SUFDckMsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUM7SUFDN0IsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7SUFDMUIsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUM7SUFDN0IsQ0FBQzs7OzRCQXJNQSxZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRTs7Ozs7OztJQXhDbEMseUNBQXlCOzs7OztJQUN6QiwwQ0FBOEI7O0lBQzlCLHlDQUFpQjs7SUFDakIsd0NBQTJCOztJQUMzQiw0Q0FBK0I7O0lBRS9CLHlDQUFrQzs7Ozs7SUFHOUIsZ0NBQTZCOzs7OztJQUM3Qiw0Q0FBaUQ7Ozs7O0lBQ2pELHdDQUFrQzs7Ozs7SUFjdEMsdUVBQXlDOzs7Ozs7SUFxQ3pDLHdEQUE4QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJpZyB9IGZyb20gJ2JpZy5qcyc7XG5cbmltcG9ydCB7IEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBzbGVlcCB9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuaW1wb3J0IHsgSUJldFJlc3VsdCB9IGZyb20gXCIuLi9tb2RlbHMvYmV0cy9pbnRlcmZhY2VzXCI7XG5pbXBvcnQgeyBJQWRkVG9CYWxhbmNlQW1vdW50cyB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJR29vZ2xlQW5hbHl0aWNzU2VydmljZSwgSU1haW5BcHBDb250ZXh0LCBJQ3VycmVuY3lTZXJ2aWNlLCBJQWxlcnRTZXJ2aWNlLCBJQmFua3JvbGxTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQXBwU2V0dGluZ3NMaXN0ZW5lciwgSUFwcFN0YXRlLCBJQXBwU2V0dGluZ3MgfSBmcm9tICcuLi9tb2RlbHMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBUb2tlblN5bWJvbCB9IGZyb20gJy4uL21vZGVscy9idXNpbmVzcy1ydWxlcy90b2tlbnMnO1xuaW1wb3J0IHsgSVNvdW5kUGxheWVyIH0gZnJvbSAnLi4vbWVkaWEvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQmV0dGluZ1Rva2VuIH0gZnJvbSAnLi4vbW9kZWxzL2J1c2luZXNzLXJ1bGVzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSVdhbGxldFNlcnZpY2VCYXNlIH0gZnJvbSAnLi4vd2FsbGV0cy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElUcmFuc2xhdGlvbkJhc2UgfSBmcm9tICcuLi90cmFuc2xhdGlvbnMvaW50ZXJmYWNlcyc7XG5cblxuXG5jb25zdCBJTkRFWF9PRl9NQVhfQ0hJUCA9IDc7XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQmV0QW1vdW50XG57XG4gICAgc3ltYm9sOnN0cmluZztcbiAgICBkZWNpbWFsOnN0cmluZztcbn1cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEdhbWVDb21wb25lbnRCYXNlIGltcGxlbWVudHMgSUFwcFNldHRpbmdzTGlzdGVuZXJcbntcbiAgICBwcml2YXRlIGF1dG9CZXROb25jZSA9IDA7XG4gICAgcHJpdmF0ZSBfaXNKYWNrcG90QmV0ID0gZmFsc2U7XG4gICAgc2VsZWN0ZWRDaGlwID0gMTtcbiAgICBwdWJsaWMgc2hvd1Rvb2x0aXAgPSBmYWxzZTtcbiAgICBwdWJsaWMgcHJvY2Vzc0JldFN0eWxlID0gZmFsc2U7XG5cbiAgICBhZGRUb0JhbGFuY2U6SUFkZFRvQmFsYW5jZUFtb3VudHM7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGFwcDpJTWFpbkFwcENvbnRleHQsXG4gICAgICAgIHByb3RlY3RlZCBnb29nbGVBbmFseXRpY3M6SUdvb2dsZUFuYWx5dGljc1NlcnZpY2UsXG4gICAgICAgIHByb3RlY3RlZCBzb3VuZFBsYXllcjpJU291bmRQbGF5ZXJcbiAgICApIHtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5hcHAuc2Nyb2xsVG9Ub3BPZlBhZ2UoKTtcblxuICAgICAgICB0aGlzLmFwcC5zZXR0aW5ncy5zZXRMaXN0ZW5lcih0aGlzKTtcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkVG9rZW4uc3ltYm9sID09ICdCRVQnKSB7XG4gICAgICAgICAgICB0aGlzLnNldHRpbmdzLnNlbGVjdEN1cnJlbmN5KCdFT1MnKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFic3RyYWN0IG9uQmV0dGluZ0N1cnJlbmN5Q2hhbmdlZCgpOnZvaWQ7XG5cbiAgICBzZWxlY3RDaGlwKGk6bnVtYmVyKXtcbiAgICAgICAgdGhpcy5zZWxlY3RlZENoaXAgPSBpO1xuXG4gICAgICAgIHRoaXMuc291bmRQbGF5ZXIucGxheSgnY2hpcFBsYWNlJyk7XG5cbiAgICAgICAgdGhpcy5vbkNoaXBTZWxlY3RlZCgpO1xuICAgIH1cbiAgICBwcm90ZWN0ZWQgb25DaGlwU2VsZWN0ZWQoKSB7fVxuXG4gICAgcHVibGljIHRvZ2dsZVRvb2x0aXAoKSB7XG4gICAgICAgIHRoaXMuc2hvd1Rvb2x0aXAgPSAhdGhpcy5zaG93VG9vbHRpcDtcbiAgICB9XG5cbiAgICBASG9zdExpc3RlbmVyKCdkb2N1bWVudDpjbGljaycsIFtdKVxuICAgIGRvY3VtZW50Q2xpY2soKSB7XG4gICAgICAgIHRoaXMuc2hvd1Rvb2x0aXAgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBzZXQgaXNKYWNrcG90QmV0KHZhbHVlOmJvb2xlYW4pIHtcbiAgICAgICAgaWYgKHRoaXMuaXNKYWNrcG90RGlzYWJsZWQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuX2lzSmFja3BvdEJldCA9IHZhbHVlO1xuICAgIH1cbiAgICBnZXQgaXNKYWNrcG90QmV0KCkge1xuICAgICAgICByZXR1cm4gIXRoaXMuaXNKYWNrcG90RGlzYWJsZWQgJiZcbiAgICAgICAgICAgICAgICB0aGlzLl9pc0phY2twb3RCZXQ7XG4gICAgfVxuXG4gICAgZ2V0IGlzSmFja3BvdERpc2FibGVkKCkge1xuICAgICAgICBjb25zdCBhbW91bnQgPSBOdW1iZXIodGhpcy5iZXRBbW91bnQuZGVjaW1hbCk7XG5cbiAgICAgICAgcmV0dXJuIGFtb3VudCA8IHRoaXMuc2VsZWN0ZWRUb2tlbi5qYWNrcG90Lm1pbmltdW1CZXQ7XG4gICAgfVxuICAgIHByb3RlY3RlZCBhYnN0cmFjdCBnZXQgYmV0QW1vdW50KCk6SUJldEFtb3VudDtcblxuXG4gICAgZ2V0U2VlZChpc0F1dG9CZXQ6Ym9vbGVhbik6c3RyaW5nIHtcbiAgICAgICAgLy8gc2VlZCBmb3IgcmFuZG9tbmVzc1xuICAgICAgICBjb25zdCBzZWVkID0gXG4gICAgICAgICAgICB0aGlzLnN0YXRlLnNlZWRGb3JSYW5kb21uZXNzICtcbiAgICAgICAgICAgIC8vIGFkZCBub25jZSBmb3IgYXV0by1iZXQgdG8gZmFpcm5lc3Mgc2VlZFxuICAgICAgICAgICAgKFxuICAgICAgICAgICAgICAgIGlzQXV0b0JldCB8fCB0cnVlID8gXG4gICAgICAgICAgICAgICAgICAgICcsJysgKCsrdGhpcy5hdXRvQmV0Tm9uY2UpIDogXG4gICAgICAgICAgICAgICAgICAgICcnXG4gICAgICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiBzZWVkO1xuICAgIH1cblxuICAgIHRyYWNrQmV0V2l0aEdvb2dsZUFuYWx5dGljcyhcbiAgICAgICAgZ2FtZU5hbWU6c3RyaW5nLFxuICAgICAgICBiZXRBbW91bnQ6c3RyaW5nLFxuICAgICAgICB0eG5JZDpzdHJpbmcsXG4gICAgICAgIGlzQXV0b0JldDpib29sZWFuXG4gICAgKSB7XG4gICAgICAgIGNvbnN0IHByaWNlID0gdGhpcy5jdXJyZW5jeS5nZXRQcmljZShcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRUb2tlbi5zeW1ib2xcbiAgICAgICAgKTtcblxuICAgICAgICBjb25zdCBhbW91bnRJblVTRDpzdHJpbmcgPVxuICAgICAgICAgICAgcHJpY2UgP1xuICAgICAgICAgICAgICAgIChwcmljZSAqIE51bWJlcihiZXRBbW91bnQpKS50b0ZpeGVkKDIpIDpcbiAgICAgICAgICAgICAgICB1bmRlZmluZWQ7XG5cbiAgICAgICAgXG4gICAgICAgIGlmIChhbW91bnRJblVTRCkge1xuICAgICAgICAgICAgdGhpcy5nb29nbGVBbmFseXRpY3MuYWRkVHJhbnNhY3Rpb24oXG4gICAgICAgICAgICAgICAgdHhuSWQsXG4gICAgICAgICAgICAgICAgZ2FtZU5hbWUsXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRva2VuLnN5bWJvbCxcbiAgICAgICAgICAgICAgICBhbW91bnRJblVTRCxcbiAgICAgICAgICAgICAgICBpc0F1dG9CZXQsXG4gICAgICAgICAgICAgICAgdGhpcy5pc0phY2twb3RCZXRcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgYXN5bmMgdXBkYXRlQmFsYW5jZXMocmVzdWx0OklCZXRSZXN1bHQpIHtcbiAgICAgICAgLyoqKiBDQUxDVUxBVEUgTkVXIFRPS0VOIEJBTEFOQ0VTIElOIEFEVkFOQ0UgIFxuICAgICAgICAgKiAgb2YgdGhlIEJldCBiZWluZyBSZXNvbHZlZCBvbi1jaGFpbiAqKiovXG5cbiAgICAgICAgY29uc3QgYmV0ID0gXG4gICAgICAgICAgICBOdW1iZXIocmVzdWx0LmFtb3VudCkgL1xuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRva2VuLmJldFRva2VuQWlyRHJvcFJhdGU7XG5cbiAgICAgICAgbGV0IHByb2ZpdCA9IHJlc3VsdC5wcm9maXQ7XG5cbiAgICAgICAgaWYgKHJlc3VsdC5pc0phY2twb3RCZXQpIHtcbiAgICAgICAgICAgIHByb2ZpdCAtPSBOdW1iZXIoXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRva2VuLmphY2twb3QudGlja2V0UHJpY2VcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICAgICAgLyoqKi9cblxuXG4gICAgICAgIHRoaXMuYWRkVG9CYWxhbmNlID0ge1xuICAgICAgICAgICAgbWFpbjogcHJvZml0LFxuICAgICAgICAgICAgYmV0XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gdG8gYW5pbWF0ZSBiYWxhbmNlIGluIGhlYWRlclxuICAgICAgICB0aGlzLmFwcC5zdGF0ZS5vbkFkZFRvQmFsYW5jZSh0cnVlLHByb2ZpdCk7XG5cbiAgICAgICAgYXdhaXQgc2xlZXAoNTAwKTtcblxuXG4gICAgICAgIC8qKiogVVBEQVRFIFRPS0VOIEJBTEFOQ0VTICoqKi9cbiAgICAgICAgLy8gdXBkYXRlIG1haW4gdG9rZW4gYmFsYW5jZVxuICAgICAgICB0aGlzLndhbGxldC5hZGRUb0JhbGFuY2UoXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkVG9rZW4uc3ltYm9sLFxuICAgICAgICAgICAgcHJvZml0XG4gICAgICAgICk7XG5cbiAgICAgICAgLy8gdXBkYXRlIEJFVCB0b2tlbiBiYWxhbmNlXG4gICAgICAgIHRoaXMud2FsbGV0LmFkZFRvQmFsYW5jZShcbiAgICAgICAgICAgIFRva2VuU3ltYm9sLkJFVCxcbiAgICAgICAgICAgIGJldFxuICAgICAgICApXG4gICAgICAgIC8qKiovXG5cblxuICAgICAgICBhd2FpdCBzbGVlcCgxNTAwKTtcblxuICAgICAgICB0aGlzLmFkZFRvQmFsYW5jZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgdGhpcy5hcHAuc3RhdGUub25BZGRUb0JhbGFuY2UoZmFsc2UpO1xuICAgIH1cblxuICAgIC8vIG9uVG9vbHRpcFNob3dCZXQoKXtcbiAgICAvLyAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0b29sdGlwQmV0XCIpLnNldEF0dHJpYnV0ZSgnZGF0YS10b29sdGlwJywgJ2FhYScpO1xuICAgIC8vIH1cbiAgICBnZXQgdG9vbHRpcEJldCgpe1xuICAgICAgICBjb25zdCB0b2tlbkFsdCA9IHRoaXMuc2VsZWN0ZWRUb2tlbi5zeW1ib2w7XG4gICAgICAgIGNvbnN0IGFpckRyb3AgPSB0aGlzLnNlbGVjdGVkVG9rZW4uYmV0VG9rZW5BaXJEcm9wUmF0ZTtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MudHJhbnNsYXRpb24udG9vbHRpcERpdmlkZW5kcyhcIjFcIixgJHthaXJEcm9wfSAke3Rva2VuQWx0fWApXG4gICAgfVxuXG4gICAgZ2V0IGphY2twb3RUb29sVGlwKCkge1xuICAgICAgICBjb25zdCB0b2tlbiA9IHRoaXMuc2VsZWN0ZWRUb2tlbjtcbiAgICAgICAgY29uc3QgamFja3BvdCA9IHRva2VuLmphY2twb3Q7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MudHJhbnNsYXRpb24uY2xpY2tUaGlzQm94VG9SZWNlaXZlMUphY2twb3RTcGluRm9yUHJpY2UoXG4gICAgICAgICAgICBOdW1iZXIoamFja3BvdC50aWNrZXRQcmljZSkrXCIgXCIrdG9rZW4uc3ltYm9sXG4gICAgICAgICk7XG4gICAgICAgIC8vIEN1cnJlbnRseSBub3QgdXNpbmcgYSBzZXBhcmF0ZSBtaW5pbXVtIGJldCBmb3IgamFja3BvdCwgXG4gICAgICAgICAgICAvLyBpdCBpcyBjdXJyZW50bHkgdGhlIHNhbWUgYXMgdGhlIHN0YW5kYXJkIG1pbmltdW0gYmV0XG4gICAgICAgIC8vXCJCZXQgbXVzdCBiZSBhdCBsZWFzdCBcIitqYWNrcG90Lm1pbmltdW1CZXQrXCIgXCIrdG9rZW4uc3ltYm9sK1wiIHRvIGVudGVyLlwiO1xuICAgIH1cblxuICAgIGdldCBpc0luc3VmZmljaWVudEZ1bmRzKCk6Ym9vbGVhbiB7XG4gICAgICAgIHJldHVybiBuZXcgQmlnKFxuICAgICAgICAgICAgdGhpcy5jdXJyZW5jeUJhbGFuY2UpLmx0KFxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRva2VuLm1pbmltdW1CZXRBbW91bnRcbiAgICAgICAgKSB8fFxuICAgICAgICBuZXcgQmlnKFxuICAgICAgICAgICAgdGhpcy5iZXRBbW91bnQuZGVjaW1hbFxuICAgICAgICApLmd0KFxuICAgICAgICAgICAgdGhpcy5jdXJyZW5jeUJhbGFuY2VcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBnZXQgaXNNYXhDaGlwU2VsZWN0ZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkQ2hpcCA9PSBJTkRFWF9PRl9NQVhfQ0hJUDtcbiAgICB9XG5cbiAgICBnZXQgY3VycmVuY3lCYWxhbmNlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5zZWxlY3RlZFRva2VuQmFsYW5jZTtcbiAgICB9XG5cbiAgICBnZXQgY2hpcHMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkVG9rZW4uY2hpcHM7XG4gICAgfVxuXG4gICAgZ2V0IHNlbGVjdGVkVG9rZW5QcmljZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVuY3kuZ2V0UHJpY2UoXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkVG9rZW4uc3ltYm9sXG4gICAgICAgICk7XG4gICAgfVxuICAgIGdldCBjdXJyZW5jeSgpOklDdXJyZW5jeVNlcnZpY2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5hcHAuY3VycmVuY3k7XG4gICAgfVxuXG4gICAgZ2V0IHNlbGVjdGVkVG9rZW4oKTpJQmV0dGluZ1Rva2VuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3Muc2VsZWN0ZWRUb2tlbjtcbiAgICB9XG5cbiAgICBnZXQgd2FsbGV0KCk6SVdhbGxldFNlcnZpY2VCYXNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3Mud2FsbGV0O1xuICAgIH1cblxuICAgIGdldCBzdGF0ZSgpOklBcHBTdGF0ZSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC5zdGF0ZTtcbiAgICB9XG5cbiAgICBnZXQgdHJhbnNsYXRpb24oKTpJVHJhbnNsYXRpb25CYXNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MudHJhbnNsYXRpb247XG4gICAgfVxuXG4gICAgZ2V0IHNldHRpbmdzKCk6SUFwcFNldHRpbmdzIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBwLnNldHRpbmdzO1xuICAgIH1cblxuICAgIGdldCBhbGVydCgpOklBbGVydFNlcnZpY2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5hcHAuYWxlcnQ7XG4gICAgfVxuXG4gICAgZ2V0IGJhbmtyb2xsKCk6SUJhbmtyb2xsU2VydmljZSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC5iYW5rcm9sbDtcbiAgICB9XG59Il19