/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/base/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IAutoBetCriteria() { }
if (false) {
    /** @type {?} */
    IAutoBetCriteria.prototype.value;
}
/**
 * @record
 */
export function IAutoBetCommand() { }
if (false) {
    /** @type {?} */
    IAutoBetCommand.prototype.name;
    /** @type {?} */
    IAutoBetCommand.prototype.value;
}
/**
 * @record
 */
export function IAutoBetSettings() { }
if (false) {
    /** @type {?} */
    IAutoBetSettings.prototype.originalBetAmount;
    /** @type {?} */
    IAutoBetSettings.prototype.currentBetAmount;
    /** @type {?} */
    IAutoBetSettings.prototype.stopCounter;
    /** @type {?} */
    IAutoBetSettings.prototype.stopProfit;
    /** @type {?} */
    IAutoBetSettings.prototype.stopLoss;
    /** @type {?} */
    IAutoBetSettings.prototype.onWin;
    /** @type {?} */
    IAutoBetSettings.prototype.onLoss;
}
/**
 * @record
 */
export function IAutoBetTracker() { }
if (false) {
    /** @type {?} */
    IAutoBetTracker.prototype.isLastBetAWin;
    /** @type {?} */
    IAutoBetTracker.prototype.numOfBets;
    /** @type {?} */
    IAutoBetTracker.prototype.totalProfitOrLoss;
}
/**
 * @record
 */
export function IAutoBetResult() { }
if (false) {
    /** @type {?} */
    IAutoBetResult.prototype.isWin;
    /** @type {?} */
    IAutoBetResult.prototype.id;
    /** @type {?} */
    IAutoBetResult.prototype.profit;
}
/**
 * @record
 */
export function IAddToBalanceAmounts() { }
if (false) {
    /** @type {?} */
    IAddToBalanceAmounts.prototype.main;
    /** @type {?} */
    IAddToBalanceAmounts.prototype.bet;
}
/**
 * @record
 */
export function IStateForBetForm() { }
if (false) {
    /**
     * @param {?} listener
     * @return {?}
     */
    IStateForBetForm.prototype.setListener = function (listener) { };
}
/**
 * @record
 */
export function IWalletServiceForBetFormBase() { }
if (false) {
    /** @type {?} */
    IWalletServiceForBetFormBase.prototype.isLoggedIn;
    /** @type {?} */
    IWalletServiceForBetFormBase.prototype.betBalance;
}
/**
 * @record
 */
export function ISettingsForBetFormBase() { }
if (false) {
    /** @type {?} */
    ISettingsForBetFormBase.prototype.selectedToken;
    /** @type {?} */
    ISettingsForBetFormBase.prototype.selectedTokenBalance;
    /** @type {?} */
    ISettingsForBetFormBase.prototype.translation;
    /** @type {?} */
    ISettingsForBetFormBase.prototype.wallet;
    /**
     * @param {?} listener
     * @return {?}
     */
    ISettingsForBetFormBase.prototype.setListener = function (listener) { };
}
/**
 * @record
 */
export function IBetFormContextBase() { }
if (false) {
    /** @type {?} */
    IBetFormContextBase.prototype.settings;
    /** @type {?} */
    IBetFormContextBase.prototype.bankroll;
    /** @type {?} */
    IBetFormContextBase.prototype.state;
}
/**
 * @record
 */
export function IGameComponent() { }
if (false) {
    /** @type {?} */
    IGameComponent.prototype.isJackpotBet;
    /** @type {?} */
    IGameComponent.prototype.state;
    /** @type {?} */
    IGameComponent.prototype.selectedToken;
    /** @type {?} */
    IGameComponent.prototype.selectedTokenPrice;
    /** @type {?} */
    IGameComponent.prototype.isInsufficientFunds;
    /**
     * @param {?} isAutoBet
     * @return {?}
     */
    IGameComponent.prototype.getSeed = function (isAutoBet) { };
    /**
     * @param {?} gameName
     * @param {?} betAmount
     * @param {?} txnId
     * @param {?} isAutoBet
     * @return {?}
     */
    IGameComponent.prototype.trackBetWithGoogleAnalytics = function (gameName, betAmount, txnId, isAutoBet) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9iYXNlL2ludGVyZmFjZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFNQSxzQ0FFQzs7O0lBREMsaUNBQWM7Ozs7O0FBSWhCLHFDQUdDOzs7SUFGQywrQkFBYTs7SUFDYixnQ0FBYzs7Ozs7QUFJaEIsc0NBVUM7OztJQVRDLDZDQUEwQjs7SUFDMUIsNENBQXlCOztJQUV6Qix1Q0FBOEI7O0lBQzlCLHNDQUE2Qjs7SUFDN0Isb0NBQTJCOztJQUUzQixpQ0FBdUI7O0lBQ3ZCLGtDQUF3Qjs7Ozs7QUFHMUIscUNBSUM7OztJQUhDLHdDQUF1Qjs7SUFDdkIsb0NBQWtCOztJQUNsQiw0Q0FBMEI7Ozs7O0FBRzVCLG9DQUlDOzs7SUFIQywrQkFBZTs7SUFDZiw0QkFBVzs7SUFDWCxnQ0FBZTs7Ozs7QUFLakIsMENBSUM7OztJQUZHLG9DQUFZOztJQUNaLG1DQUFXOzs7OztBQVVmLHNDQUdDOzs7Ozs7SUFEQyxpRUFBNkM7Ozs7O0FBRy9DLGtEQUlDOzs7SUFGQyxrREFBNEI7O0lBQzVCLGtEQUEyQjs7Ozs7QUFHN0IsNkNBUUM7OztJQU5DLGdEQUFxQzs7SUFDckMsdURBQXFDOztJQUNyQyw4Q0FBc0M7O0lBQ3RDLHlDQUE2Qzs7Ozs7SUFFN0Msd0VBQWdEOzs7OztBQUdsRCx5Q0FLQzs7O0lBSEcsdUNBQTBDOztJQUMxQyx1Q0FBbUM7O0lBQ25DLG9DQUFnQzs7Ozs7QUFJcEMsb0NBZ0JDOzs7SUFkQyxzQ0FBOEI7O0lBQzlCLCtCQUF5Qjs7SUFDekIsdUNBQXFDOztJQUNyQyw0Q0FBbUM7O0lBQ25DLDZDQUFxQzs7Ozs7SUFFckMsNERBQWtDOzs7Ozs7OztJQUVsQyw0R0FLTyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElBcHBTZXR0aW5nc0xpc3RlbmVyLCBJQXBwU3RhdGVMaXN0ZW5lciwgSUFwcFN0YXRlIH0gZnJvbSBcIi4uL21vZGVscy9pbnRlcmZhY2VzXCI7XG5pbXBvcnQgeyBJQmFua3JvbGxTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IElUcmFuc2xhdGlvbkJhc2UgfSBmcm9tICcuLi90cmFuc2xhdGlvbnMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQmV0dGluZ1Rva2VuIH0gZnJvbSAnLi4vbW9kZWxzL2J1c2luZXNzLXJ1bGVzL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUF1dG9CZXRDcml0ZXJpYSB7XG4gIHZhbHVlOiBudW1iZXI7XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQXV0b0JldENvbW1hbmQge1xuICBuYW1lOiBzdHJpbmc7XG4gIHZhbHVlOiBudW1iZXI7XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQXV0b0JldFNldHRpbmdzIHtcbiAgb3JpZ2luYWxCZXRBbW91bnQ6IG51bWJlcjtcbiAgY3VycmVudEJldEFtb3VudDogbnVtYmVyO1xuXG4gIHN0b3BDb3VudGVyOiBJQXV0b0JldENyaXRlcmlhO1xuICBzdG9wUHJvZml0OiBJQXV0b0JldENyaXRlcmlhO1xuICBzdG9wTG9zczogSUF1dG9CZXRDcml0ZXJpYTtcblxuICBvbldpbjogSUF1dG9CZXRDb21tYW5kO1xuICBvbkxvc3M6IElBdXRvQmV0Q29tbWFuZDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQXV0b0JldFRyYWNrZXIge1xuICBpc0xhc3RCZXRBV2luOiBib29sZWFuO1xuICBudW1PZkJldHM6IG51bWJlcjtcbiAgdG90YWxQcm9maXRPckxvc3M6IG51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQXV0b0JldFJlc3VsdCB7XG4gIGlzV2luOiBib29sZWFuO1xuICBpZDogc3RyaW5nO1xuICBwcm9maXQ6IG51bWJlcjtcbn1cblxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUFkZFRvQmFsYW5jZUFtb3VudHNcbntcbiAgICBtYWluOm51bWJlcjtcbiAgICBiZXQ6bnVtYmVyO1xufVxuXG5cbi8vZXhwb3J0IFxudHlwZSBOdW1iZXJQcm92aWRlciA9ICgpID0+IFByb21pc2U8c3RyaW5nPjtcblxuXG5cblxuZXhwb3J0IGludGVyZmFjZSBJU3RhdGVGb3JCZXRGb3JtXG57XG4gIHNldExpc3RlbmVyKGxpc3RlbmVyOklBcHBTdGF0ZUxpc3RlbmVyKTp2b2lkO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElXYWxsZXRTZXJ2aWNlRm9yQmV0Rm9ybUJhc2VcbntcbiAgcmVhZG9ubHkgaXNMb2dnZWRJbjpib29sZWFuO1xuICByZWFkb25seSBiZXRCYWxhbmNlOnN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJU2V0dGluZ3NGb3JCZXRGb3JtQmFzZVxue1xuICByZWFkb25seSBzZWxlY3RlZFRva2VuOklCZXR0aW5nVG9rZW47XG4gIHJlYWRvbmx5IHNlbGVjdGVkVG9rZW5CYWxhbmNlOnN0cmluZztcbiAgcmVhZG9ubHkgdHJhbnNsYXRpb246SVRyYW5zbGF0aW9uQmFzZTtcbiAgcmVhZG9ubHkgd2FsbGV0OklXYWxsZXRTZXJ2aWNlRm9yQmV0Rm9ybUJhc2U7XG5cbiAgc2V0TGlzdGVuZXIobGlzdGVuZXI6SUFwcFNldHRpbmdzTGlzdGVuZXIpOnZvaWQ7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUJldEZvcm1Db250ZXh0QmFzZVxue1xuICAgIHJlYWRvbmx5IHNldHRpbmdzOklTZXR0aW5nc0ZvckJldEZvcm1CYXNlO1xuICAgIHJlYWRvbmx5IGJhbmtyb2xsOklCYW5rcm9sbFNlcnZpY2U7XG4gICAgcmVhZG9ubHkgc3RhdGU6SVN0YXRlRm9yQmV0Rm9ybTtcbn1cblxuXG5leHBvcnQgaW50ZXJmYWNlIElHYW1lQ29tcG9uZW50XG57XG4gIHJlYWRvbmx5IGlzSmFja3BvdEJldDpib29sZWFuO1xuICByZWFkb25seSBzdGF0ZTpJQXBwU3RhdGU7XG4gIHJlYWRvbmx5IHNlbGVjdGVkVG9rZW46SUJldHRpbmdUb2tlbjtcbiAgcmVhZG9ubHkgc2VsZWN0ZWRUb2tlblByaWNlOm51bWJlcjtcbiAgcmVhZG9ubHkgaXNJbnN1ZmZpY2llbnRGdW5kczpib29sZWFuO1xuXG4gIGdldFNlZWQoaXNBdXRvQmV0OmJvb2xlYW4pOnN0cmluZztcblxuICB0cmFja0JldFdpdGhHb29nbGVBbmFseXRpY3MoXG4gICAgZ2FtZU5hbWU6c3RyaW5nLFxuICAgIGJldEFtb3VudDpzdHJpbmcsXG4gICAgdHhuSWQ6c3RyaW5nLFxuICAgIGlzQXV0b0JldDpib29sZWFuXG4gICk6dm9pZDtcbn0iXX0=