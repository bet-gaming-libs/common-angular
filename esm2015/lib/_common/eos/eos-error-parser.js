/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/eos/eos-error-parser.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class EosErrorParser {
    /**
     * @param {?} data
     */
    constructor(data) {
        this._message = data.message;
        try {
            data = JSON.parse(data);
            if (data.error && data.error.details) {
                this._message = data.error.details[0].message;
            }
        }
        catch (error) {
        }
    }
    /**
     * @return {?}
     */
    get isIncorrectNonce() {
        return this.message ==
            'assertion failure with message: Incorrect nonce.';
    }
    /**
     * @return {?}
     */
    get message() {
        return this._message;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    EosErrorParser.prototype._message;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLWVycm9yLXBhcnNlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9lb3MvZW9zLWVycm9yLXBhcnNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE1BQU07Ozs7SUFJRixZQUFZLElBQVE7UUFDaEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBRTdCLElBQUk7WUFDQSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV4QixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO2FBQ2pEO1NBQ0o7UUFBQyxPQUFPLEtBQUssRUFBRTtTQUNmO0lBQ0wsQ0FBQzs7OztJQUVELElBQUksZ0JBQWdCO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLE9BQU87WUFDZixrREFBa0QsQ0FBQztJQUMzRCxDQUFDOzs7O0lBRUQsSUFBSSxPQUFPO1FBQ1AsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7Q0FDSjs7Ozs7O0lBdkJHLGtDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBFb3NFcnJvclBhcnNlclxue1xuICAgIHByaXZhdGUgX21lc3NhZ2U6c3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IoZGF0YTphbnkpIHtcbiAgICAgICAgdGhpcy5fbWVzc2FnZSA9IGRhdGEubWVzc2FnZTtcblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UoZGF0YSk7XG5cbiAgICAgICAgICAgIGlmIChkYXRhLmVycm9yICYmIGRhdGEuZXJyb3IuZGV0YWlscykge1xuICAgICAgICAgICAgICAgIHRoaXMuX21lc3NhZ2UgPSBkYXRhLmVycm9yLmRldGFpbHNbMF0ubWVzc2FnZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldCBpc0luY29ycmVjdE5vbmNlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5tZXNzYWdlID09XG4gICAgICAgICAgICAnYXNzZXJ0aW9uIGZhaWx1cmUgd2l0aCBtZXNzYWdlOiBJbmNvcnJlY3Qgbm9uY2UuJztcbiAgICB9XG5cbiAgICBnZXQgbWVzc2FnZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX21lc3NhZ2U7XG4gICAgfVxufSJdfQ==