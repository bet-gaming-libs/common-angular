/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/eos/eos-numbers.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEOSAssetAmount() { }
if (false) {
    /** @type {?} */
    IEOSAssetAmount.prototype.symbol;
    /** @type {?} */
    IEOSAssetAmount.prototype.contract;
    /** @type {?} */
    IEOSAssetAmount.prototype.integer;
    /** @type {?} */
    IEOSAssetAmount.prototype.quantity;
    /** @type {?} */
    IEOSAssetAmount.prototype.precision;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLW51bWJlcnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL19jb21tb24vZW9zL2Vvcy1udW1iZXJzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEscUNBT0M7OztJQUxHLGlDQUFjOztJQUNkLG1DQUFnQjs7SUFDaEIsa0NBQWU7O0lBQ2YsbUNBQWdCOztJQUNoQixvQ0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElFT1NBc3NldEFtb3VudFxue1xuICAgIHN5bWJvbDpzdHJpbmc7XG4gICAgY29udHJhY3Q6c3RyaW5nO1xuICAgIGludGVnZXI6bnVtYmVyO1xuICAgIHF1YW50aXR5OnN0cmluZztcbiAgICBwcmVjaXNpb246bnVtYmVyO1xufSJdfQ==