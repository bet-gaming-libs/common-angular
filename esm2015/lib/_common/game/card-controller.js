/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/game/card-controller.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const SUITS = ['hearts', 'diamonds', 'spades', 'clubs'];
export class CardController {
    /**
     * @param {?} soundPlayer
     */
    constructor(soundPlayer) {
        this.soundPlayer = soundPlayer;
        this._dealt = false;
        this._flipped = false;
        this._isRevealed = false;
    }
    /**
     * @return {?}
     */
    deal() {
        this._dealt = true;
        this.soundPlayer.play('cardPlace');
    }
    /**
     * @param {?} data
     * @param {?=} rankNumber
     * @param {?=} cardId
     * @return {?}
     */
    reveal(data, rankNumber = undefined, cardId = undefined) {
        this._rank = data.rankName.toLowerCase();
        this._suit = SUITS[data.suit];
        this._rankNumber = rankNumber;
        this._cardId = cardId;
        this._isRevealed = true;
    }
    /**
     * @return {?}
     */
    flip() {
        this._flipped = true;
        this.soundPlayer.play('cardFlip');
    }
    /**
     * @return {?}
     */
    unflip() {
        this._flipped = false;
        this.soundPlayer.play('cardFlip');
    }
    /**
     * @return {?}
     */
    dealAndFlip() {
        this._dealt = true;
        this._flipped = true;
        this.soundPlayer.play('cardFlip');
    }
    /**
     * @return {?}
     */
    get dealt() {
        return this._dealt;
    }
    /**
     * @return {?}
     */
    get flipped() {
        return this._flipped;
    }
    /**
     * @return {?}
     */
    get suit() {
        return this._suit;
    }
    /**
     * @return {?}
     */
    get card() {
        return this.rank;
    }
    /**
     * @return {?}
     */
    get rank() {
        return this._rank;
    }
    /**
     * @return {?}
     */
    get rankNumber() {
        return this._rankNumber;
    }
    /**
     * @return {?}
     */
    get cardId() {
        return this._cardId;
    }
    /**
     * @return {?}
     */
    get isRevealed() {
        return this._isRevealed;
    }
    /**
     * @return {?}
     */
    get data() {
        return {
            suit: this.suit,
            dealt: this._dealt,
            flipped: this.flipped,
            card: this.card
        };
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._dealt;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._flipped;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._isRevealed;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._suit;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._rank;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._rankNumber;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._cardId;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype.soundPlayer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL2dhbWUvY2FyZC1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztNQU1NLEtBQUssR0FBRyxDQUFDLFFBQVEsRUFBQyxVQUFVLEVBQUMsUUFBUSxFQUFDLE9BQU8sQ0FBQztBQUdwRCxNQUFNOzs7O0lBV0YsWUFBb0IsV0FBd0I7UUFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFUcEMsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7SUFRNUIsQ0FBQzs7OztJQUVELElBQUk7UUFDQSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7Ozs7O0lBRUQsTUFBTSxDQUNGLElBQWMsRUFDZCxhQUFrQixTQUFTLEVBQzNCLFNBQWMsU0FBUztRQUV2QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDO1FBQzlCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBRXRCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCxJQUFJO1FBQ0EsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7OztJQUNELE1BQU07UUFDRixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxJQUFJLEtBQUs7UUFDTCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQzs7OztJQUNELElBQUksT0FBTztRQUNQLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ0osT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7Ozs7SUFDRCxJQUFJLElBQUk7UUFDSixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQzs7OztJQUNELElBQUksSUFBSTtRQUNKLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7O0lBQ0QsSUFBSSxVQUFVO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7Ozs7SUFDRCxJQUFJLE1BQU07UUFDTixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNWLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ0osT0FBTztZQUNILElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtZQUNmLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNsQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1NBQ2xCLENBQUM7SUFDTixDQUFDO0NBQ0o7Ozs7OztJQWhGRyxnQ0FBdUI7Ozs7O0lBQ3ZCLGtDQUF5Qjs7Ozs7SUFDekIscUNBQTRCOzs7OztJQUU1QiwrQkFBcUI7Ozs7O0lBQ3JCLCtCQUFxQjs7Ozs7SUFDckIscUNBQTJCOzs7OztJQUMzQixpQ0FBdUI7Ozs7O0lBRVgscUNBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSUNhcmREYXRhIH0gZnJvbSBcImVhcm5iZXQtY29tbW9uXCI7XG5cbmltcG9ydCB7IElDYXJkQ29udHJvbGxlciB9IGZyb20gXCIuL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IElTb3VuZFBsYXllciB9IGZyb20gJy4uL21lZGlhL2ludGVyZmFjZXMnO1xuXG5cbmNvbnN0IFNVSVRTID0gWydoZWFydHMnLCdkaWFtb25kcycsJ3NwYWRlcycsJ2NsdWJzJ107XG5cblxuZXhwb3J0IGNsYXNzIENhcmRDb250cm9sbGVyXG57XG4gICAgcHJpdmF0ZSBfZGVhbHQgPSBmYWxzZTtcbiAgICBwcml2YXRlIF9mbGlwcGVkID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBfaXNSZXZlYWxlZCA9IGZhbHNlO1xuXG4gICAgcHJpdmF0ZSBfc3VpdDpzdHJpbmc7XG4gICAgcHJpdmF0ZSBfcmFuazpzdHJpbmc7XG4gICAgcHJpdmF0ZSBfcmFua051bWJlcjpudW1iZXI7XG4gICAgcHJpdmF0ZSBfY2FyZElkOm51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc291bmRQbGF5ZXI6SVNvdW5kUGxheWVyKSB7IFxuICAgIH1cblxuICAgIGRlYWwoKSB7XG4gICAgICAgIHRoaXMuX2RlYWx0ID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5zb3VuZFBsYXllci5wbGF5KCdjYXJkUGxhY2UnKTtcbiAgICB9XG5cbiAgICByZXZlYWwoXG4gICAgICAgIGRhdGE6SUNhcmREYXRhLFxuICAgICAgICByYW5rTnVtYmVyOm51bWJlcj11bmRlZmluZWQsXG4gICAgICAgIGNhcmRJZDpudW1iZXI9dW5kZWZpbmVkXG4gICAgKSB7XG4gICAgICAgIHRoaXMuX3JhbmsgPSBkYXRhLnJhbmtOYW1lLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgIHRoaXMuX3N1aXQgPSBTVUlUU1tkYXRhLnN1aXRdO1xuICAgICAgICB0aGlzLl9yYW5rTnVtYmVyID0gcmFua051bWJlcjtcbiAgICAgICAgdGhpcy5fY2FyZElkID0gY2FyZElkO1xuXG4gICAgICAgIHRoaXMuX2lzUmV2ZWFsZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIGZsaXAoKSB7XG4gICAgICAgIHRoaXMuX2ZsaXBwZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLnNvdW5kUGxheWVyLnBsYXkoJ2NhcmRGbGlwJyk7XG4gICAgfVxuICAgIHVuZmxpcCgpIHtcbiAgICAgICAgdGhpcy5fZmxpcHBlZCA9IGZhbHNlO1xuICAgICAgICB0aGlzLnNvdW5kUGxheWVyLnBsYXkoJ2NhcmRGbGlwJyk7XG4gICAgfVxuXG4gICAgZGVhbEFuZEZsaXAoKSB7XG4gICAgICAgIHRoaXMuX2RlYWx0ID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5fZmxpcHBlZCA9IHRydWU7XG4gICAgICAgIHRoaXMuc291bmRQbGF5ZXIucGxheSgnY2FyZEZsaXAnKTtcbiAgICB9XG5cbiAgICBnZXQgZGVhbHQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9kZWFsdDtcbiAgICB9XG4gICAgZ2V0IGZsaXBwZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9mbGlwcGVkO1xuICAgIH1cblxuICAgIGdldCBzdWl0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fc3VpdDtcbiAgICB9XG4gICAgZ2V0IGNhcmQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJhbms7XG4gICAgfVxuICAgIGdldCByYW5rKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fcmFuaztcbiAgICB9XG4gICAgZ2V0IHJhbmtOdW1iZXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9yYW5rTnVtYmVyO1xuICAgIH1cbiAgICBnZXQgY2FyZElkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fY2FyZElkO1xuICAgIH1cblxuICAgIGdldCBpc1JldmVhbGVkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faXNSZXZlYWxlZDtcbiAgICB9XG5cbiAgICBnZXQgZGF0YSgpOklDYXJkQ29udHJvbGxlciB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBzdWl0OiB0aGlzLnN1aXQsXG4gICAgICAgICAgICBkZWFsdDogdGhpcy5fZGVhbHQsXG4gICAgICAgICAgICBmbGlwcGVkOiB0aGlzLmZsaXBwZWQsXG4gICAgICAgICAgICBjYXJkOiB0aGlzLmNhcmRcbiAgICAgICAgfTtcbiAgICB9XG59Il19