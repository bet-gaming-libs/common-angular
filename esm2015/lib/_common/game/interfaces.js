/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/game/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ICardController() { }
if (false) {
    /** @type {?} */
    ICardController.prototype.suit;
    /** @type {?} */
    ICardController.prototype.dealt;
    /** @type {?} */
    ICardController.prototype.card;
    /** @type {?} */
    ICardController.prototype.flipped;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9nYW1lL2ludGVyZmFjZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxxQ0FNQzs7O0lBSkcsK0JBQVk7O0lBQ1osZ0NBQWM7O0lBQ2QsK0JBQVk7O0lBQ1osa0NBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJQ2FyZENvbnRyb2xsZXJcbntcbiAgICBzdWl0OnN0cmluZztcbiAgICBkZWFsdDpib29sZWFuO1xuICAgIGNhcmQ6c3RyaW5nO1xuICAgIGZsaXBwZWQ6Ym9vbGVhbjtcbn0iXX0=