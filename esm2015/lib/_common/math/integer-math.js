/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/math/integer-math.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Big } from 'big.js';
import { roundDownWithoutTrailingZeros } from '../util/math-util';
export class IntBasedNum {
    /**
     * @param {?} precision
     * @param {?=} decimalValue
     * @param {?=} bigDecimal
     */
    constructor(precision, decimalValue = 0, bigDecimal = undefined) {
        this.precision = precision;
        /** @type {?} */
        const decimal = bigDecimal == undefined ?
            new Big(decimalValue).round(precision, 0 /* RoundDown */) :
            bigDecimal;
        this.factor = Math.pow(10, precision);
        this._integer = decimal.times(this.factor).round(0, 0 /* RoundDown */);
    }
    // CLASS
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    static fromInteger(integer, precision) {
        return new IntBasedNum(precision, undefined, new Big(integer).div(Math.pow(10, precision)).round(precision, 0 /* RoundDown */));
    }
    /**
     * MUTATING METHODS **
     * @param {?} other
     * @return {?}
     */
    addAndReplace(other) {
        if (this.precision != other.precision) {
            throw new Error('precision of both operands must be the same!');
        }
        /** @type {?} */
        const sum = this.integer.plus(other.integer);
        this._integer = sum;
        //return this;
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} other
     * @return {THIS}
     */
    subtractAndReplace(other) {
        if ((/** @type {?} */ (this)).precision != other.precision) {
            throw new Error('precision of both operands must be the same!');
        }
        /** @type {?} */
        const diff = (/** @type {?} */ (this)).integer.minus(other.integer);
        (/** @type {?} */ (this))._integer = diff;
        return (/** @type {?} */ (this));
    }
    /**
     *
     * @param {?} other
     * @return {?}
     */
    multiply(other) {
        return integerMultiplication(this, other);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    multiplyDecimal(decimal) {
        /** @type {?} */
        const other = new IntBasedNum(this.precision, decimal);
        return integerMultiplication(this, other);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    divideByDecimal(decimal) {
        /** @type {?} */
        const other = new IntBasedNum(this.precision, decimal.toString());
        return integerDivision(this, other);
    }
    /**
     * @return {?}
     */
    get reciprocal() {
        if (this._reciprocal == undefined) {
            this._reciprocal = integerDivision(new IntBasedNum(this.precision, '1'), this);
        }
        return this._reciprocal;
    }
    /**
     * @return {?}
     */
    get opposite_() {
        return integerMultiplication(new IntBasedNum(this.precision, '-1'), this);
    }
    /**
     * @return {?}
     */
    get decimalAsNumber() {
        return Number(this.decimal);
    }
    /**
     * @return {?}
     */
    get integer() {
        return this._integer;
    }
    /**
     * @return {?}
     */
    toString() {
        return this.decimal;
    }
    /**
     * @return {?}
     */
    get decimalWithoutTrailingZeros() {
        return roundDownWithoutTrailingZeros(this.decimal, this.precision);
    }
    /**
     * @return {?}
     */
    get decimal() {
        Big.RM = 0 /* RoundDown */;
        return this.integer.div(this.factor).toFixed(this.precision);
    }
}
if (false) {
    /** @type {?} */
    IntBasedNum.prototype.factor;
    /**
     * @type {?}
     * @private
     */
    IntBasedNum.prototype._integer;
    /**
     * @type {?}
     * @private
     */
    IntBasedNum.prototype._reciprocal;
    /** @type {?} */
    IntBasedNum.prototype.precision;
}
export class AssetAmount extends IntBasedNum {
    /**
     * @param {?} data
     */
    constructor(data) {
        /** @type {?} */
        const parts = data.split(' ');
        /** @type {?} */
        const decimal = parts[0];
        /** @type {?} */
        const symbol = parts[1];
        /** @type {?} */
        const precision = decimal.split('.')[1].length;
        super(precision, decimal);
        this._symbol = symbol;
        this._isZero = Number(decimal) == 0;
    }
    /**
     * @return {?}
     */
    get isZero() {
        return this._isZero;
    }
    /**
     * @return {?}
     */
    get symbol() {
        return this._symbol;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    AssetAmount.prototype._symbol;
    /**
     * @type {?}
     * @private
     */
    AssetAmount.prototype._isZero;
}
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerAddition(a, b) {
    if (a.precision != b.precision) {
        throw new Error('precision of a: ' + a.precision +
            ', precision of b: ' + b.precision +
            "\nprecision of both operands must be the same!");
    }
    /** @type {?} */
    const sum = a.integer.plus(b.integer);
    return IntBasedNum.fromInteger(sum.toString(), a.precision);
}
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerSubtraction(a, b) {
    if (a.precision != b.precision) {
        throw new Error('precision of both operands must be the same!');
    }
    /** @type {?} */
    const diff = a.integer.minus(b.integer);
    return IntBasedNum.fromInteger(diff.toString(), a.precision);
}
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerMultiplication(a, b) {
    if (a.precision < b.precision) {
        console.log(a.decimal);
        console.log(b.decimal);
        throw new Error('precision of a: ' + a.precision +
            ', precision of b: ' + b.precision +
            "\nprecision of A must be >= B!");
    }
    /** @type {?} */
    const product = a.integer.times(b.integer);
    /** @type {?} */
    const integer = product.div(b.factor)
        .round(0, 0 /* RoundDown */)
        .toString();
    /** @type {?} */
    const result = IntBasedNum.fromInteger(integer, a.precision);
    //console.log('INT Multiplication: '+a+' * '+b+' = '+result);
    return result;
}
// when doing division between two eos numbers then disregard
// the decimal after the integer
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerDivision(a, b) {
    if (a.precision < b.precision) {
        throw new Error('precision of A must be >= B!');
    }
    /** @type {?} */
    const quotient = a.integer.div(b.integer);
    /** @type {?} */
    const integer = quotient.times(b.factor)
        .round(0, 0 /* RoundDown */)
        .toString();
    /** @type {?} */
    const result = IntBasedNum.fromInteger(integer, a.precision);
    console.log('INT Division: ' + a + ' / ' + b + ' = ' + result);
    return result;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZWdlci1tYXRoLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL21hdGgvaW50ZWdlci1tYXRoLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBUSxFQUFFLEdBQUcsRUFBZ0IsTUFBTSxRQUFRLENBQUM7QUFDNUMsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFHbEUsTUFBTTs7Ozs7O0lBMEJGLFlBQ2EsU0FBZ0IsRUFDekIsZUFBNkIsQ0FBQyxFQUM5QixhQUFpQixTQUFTO1FBRmpCLGNBQVMsR0FBVCxTQUFTLENBQU87O2NBSW5CLE9BQU8sR0FDVCxVQUFVLElBQUksU0FBUyxDQUFDLENBQUM7WUFDckIsSUFBSSxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUN2QixTQUFTLG9CQUVaLENBQUMsQ0FBQztZQUNILFVBQVU7UUFFbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBQyxTQUFTLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLG9CQUF3QixDQUFDO0lBQy9FLENBQUM7Ozs7Ozs7SUF0Q0QsTUFBTSxDQUFDLFdBQVcsQ0FDZCxPQUFjLEVBQ2QsU0FBZ0I7UUFFaEIsT0FBTyxJQUFJLFdBQVcsQ0FDbEIsU0FBUyxFQUNULFNBQVMsRUFDVCxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFDLFNBQVMsQ0FBQyxDQUN6QixDQUFDLEtBQUssQ0FDSCxTQUFTLG9CQUVaLENBQ0osQ0FBQztJQUNOLENBQUM7Ozs7OztJQTRCRCxhQUFhLENBQUMsS0FBaUI7UUFDM0IsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDbkMsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1NBQ25FOztjQUdLLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQzVDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO1FBRXBCLGNBQWM7SUFDbEIsQ0FBQzs7Ozs7OztJQUVELGtCQUFrQixDQUFDLEtBQWlCO1FBQ2hDLElBQUksbUJBQUEsSUFBSSxFQUFBLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDbkMsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1NBQ25FOztjQUdLLElBQUksR0FBRyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7UUFDOUMsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUVyQixPQUFPLG1CQUFBLElBQUksRUFBQSxDQUFDO0lBQ2hCLENBQUM7Ozs7OztJQUlELFFBQVEsQ0FBQyxLQUFpQjtRQUN0QixPQUFPLHFCQUFxQixDQUN4QixJQUFJLEVBQ0osS0FBSyxDQUNSLENBQUM7SUFDTixDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxPQUFjOztjQUNwQixLQUFLLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBQyxPQUFPLENBQUM7UUFFckQsT0FBTyxxQkFBcUIsQ0FDeEIsSUFBSSxFQUNKLEtBQUssQ0FDUixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsT0FBYzs7Y0FDcEIsS0FBSyxHQUFHLElBQUksV0FBVyxDQUN6QixJQUFJLENBQUMsU0FBUyxFQUNkLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FDckI7UUFFRCxPQUFPLGVBQWUsQ0FDbEIsSUFBSSxFQUNKLEtBQUssQ0FDUixDQUFDO0lBQ04sQ0FBQzs7OztJQUdELElBQUksVUFBVTtRQUNWLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxTQUFTLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQzlCLElBQUksV0FBVyxDQUNYLElBQUksQ0FBQyxTQUFTLEVBQ2QsR0FBRyxDQUNOLEVBQ0QsSUFBSSxDQUNQLENBQUM7U0FDTDtRQUVELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsSUFBSSxTQUFTO1FBQ1QsT0FBTyxxQkFBcUIsQ0FDeEIsSUFBSSxXQUFXLENBQ1gsSUFBSSxDQUFDLFNBQVMsRUFDZCxJQUFJLENBQ1AsRUFDRCxJQUFJLENBQ1AsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxJQUFJLGVBQWU7UUFDZixPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7OztJQUVELElBQUksT0FBTztRQUNQLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsSUFBSSwyQkFBMkI7UUFDM0IsT0FBTyw2QkFBNkIsQ0FDaEMsSUFBSSxDQUFDLE9BQU8sRUFDWixJQUFJLENBQUMsU0FBUyxDQUNqQixDQUFDO0lBQ04sQ0FBQzs7OztJQUVELElBQUksT0FBTztRQUNQLEdBQUcsQ0FBQyxFQUFFLG9CQUF5QixDQUFDO1FBRWhDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQ25CLElBQUksQ0FBQyxNQUFNLENBQ2QsQ0FBQyxPQUFPLENBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FDakIsQ0FBQztJQUNOLENBQUM7Q0FvQko7OztJQXRKRyw2QkFBdUI7Ozs7O0lBQ3ZCLCtCQUFxQjs7Ozs7SUFFckIsa0NBQWdDOztJQUc1QixnQ0FBeUI7O0FBa0pqQyxNQUFNLGtCQUFtQixTQUFRLFdBQVc7Ozs7SUFPeEMsWUFBWSxJQUFXOztjQUNiLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs7Y0FFdkIsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7O2NBQ2xCLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDOztjQUVqQixTQUFTLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNO1FBRzlDLEtBQUssQ0FBQyxTQUFTLEVBQUMsT0FBTyxDQUFDLENBQUM7UUFHekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFFdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Ozs7SUFFRCxJQUFJLE1BQU07UUFDTixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQzs7OztJQUVELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDO0NBQ0o7Ozs7OztJQTVCRyw4QkFBdUI7Ozs7O0lBRXZCLDhCQUF3Qjs7Ozs7Ozs7O0FBK0I1Qix5QkFDSSxDQUFHLEVBQUMsQ0FBRztJQUVQLElBQUksQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsU0FBUyxFQUFFO1FBQzVCLE1BQU0sSUFBSSxLQUFLLENBQ1gsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLFNBQVM7WUFDaEMsb0JBQW9CLEdBQUcsQ0FBQyxDQUFDLFNBQVM7WUFDbEMsZ0RBQWdELENBQ25ELENBQUM7S0FDTDs7VUFJSyxHQUFHLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztJQUVyQyxPQUFPLFdBQVcsQ0FBQyxXQUFXLENBQzFCLEdBQUcsQ0FBQyxRQUFRLEVBQUUsRUFDZCxDQUFDLENBQUMsU0FBUyxDQUNkLENBQUM7QUFDTixDQUFDOzs7Ozs7OztBQUdELDRCQUNJLENBQUcsRUFBQyxDQUFHO0lBRVAsSUFBSSxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxTQUFTLEVBQUU7UUFDNUIsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO0tBQ25FOztVQUlLLElBQUksR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO0lBRXZDLE9BQU8sV0FBVyxDQUFDLFdBQVcsQ0FDMUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUNmLENBQUMsQ0FBQyxTQUFTLENBQ2QsQ0FBQztBQUNOLENBQUM7Ozs7Ozs7O0FBR0QsK0JBQ0ksQ0FBRyxFQUFDLENBQUc7SUFFUCxJQUFJLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLFNBQVMsRUFBRTtRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV2QixNQUFNLElBQUksS0FBSyxDQUNYLGtCQUFrQixHQUFHLENBQUMsQ0FBQyxTQUFTO1lBQ2hDLG9CQUFvQixHQUFHLENBQUMsQ0FBQyxTQUFTO1lBQ2xDLGdDQUFnQyxDQUNuQyxDQUFDO0tBQ0w7O1VBSUssT0FBTyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7O1VBRXBDLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7U0FDcEIsS0FBSyxDQUFDLENBQUMsb0JBQXdCO1NBQy9CLFFBQVEsRUFBRTs7VUFFckIsTUFBTSxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQ2xDLE9BQU8sRUFDUCxDQUFDLENBQUMsU0FBUyxDQUNkO0lBRUQsNkRBQTZEO0lBRTdELE9BQU8sTUFBTSxDQUFDO0FBQ2xCLENBQUM7Ozs7Ozs7Ozs7QUFNRCx5QkFDSSxDQUFHLEVBQUMsQ0FBRztJQUVQLElBQUksQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFO1FBQzNCLE1BQU0sSUFBSSxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztLQUNuRDs7VUFJSyxRQUFRLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQzs7VUFFbkMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztTQUN2QixLQUFLLENBQUMsQ0FBQyxvQkFBd0I7U0FDL0IsUUFBUSxFQUFFOztVQUVyQixNQUFNLEdBQUcsV0FBVyxDQUFDLFdBQVcsQ0FDbEMsT0FBTyxFQUNQLENBQUMsQ0FBQyxTQUFTLENBQ2Q7SUFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFDLENBQUMsR0FBQyxLQUFLLEdBQUMsQ0FBQyxHQUFDLEtBQUssR0FBQyxNQUFNLENBQUMsQ0FBQztJQUVyRCxPQUFPLE1BQU0sQ0FBQztBQUNsQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICB7IEJpZywgUm91bmRpbmdNb2RlIH0gZnJvbSAnYmlnLmpzJztcbmltcG9ydCB7IHJvdW5kRG93bldpdGhvdXRUcmFpbGluZ1plcm9zIH0gZnJvbSAnLi4vdXRpbC9tYXRoLXV0aWwnO1xuXG5cbmV4cG9ydCBjbGFzcyBJbnRCYXNlZE51bVxue1xuICAgIC8vIENMQVNTXG4gICAgc3RhdGljIGZyb21JbnRlZ2VyKFxuICAgICAgICBpbnRlZ2VyOnN0cmluZyxcbiAgICAgICAgcHJlY2lzaW9uOm51bWJlclxuICAgICkge1xuICAgICAgICByZXR1cm4gbmV3IEludEJhc2VkTnVtKFxuICAgICAgICAgICAgcHJlY2lzaW9uLFxuICAgICAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICAgICAgbmV3IEJpZyhpbnRlZ2VyKS5kaXYoXG4gICAgICAgICAgICAgICAgTWF0aC5wb3coMTAscHJlY2lzaW9uKVxuICAgICAgICAgICAgKS5yb3VuZChcbiAgICAgICAgICAgICAgICBwcmVjaXNpb24sXG4gICAgICAgICAgICAgICAgUm91bmRpbmdNb2RlLlJvdW5kRG93blxuICAgICAgICAgICAgKVxuICAgICAgICApO1xuICAgIH1cblxuXG4gICAgLy8gSU5TVEFOQ0VcbiAgICByZWFkb25seSBmYWN0b3I6bnVtYmVyO1xuICAgIHByaXZhdGUgX2ludGVnZXI6QmlnO1xuXG4gICAgcHJpdmF0ZSBfcmVjaXByb2NhbDpJbnRCYXNlZE51bTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBwcmVjaXNpb246bnVtYmVyLFxuICAgICAgICBkZWNpbWFsVmFsdWU6bnVtYmVyfHN0cmluZyA9IDAsXG4gICAgICAgIGJpZ0RlY2ltYWw6QmlnID0gdW5kZWZpbmVkXG4gICAgKSB7XG4gICAgICAgIGNvbnN0IGRlY2ltYWwgPVxuICAgICAgICAgICAgYmlnRGVjaW1hbCA9PSB1bmRlZmluZWQgP1xuICAgICAgICAgICAgICAgIG5ldyBCaWcoZGVjaW1hbFZhbHVlKS5yb3VuZChcbiAgICAgICAgICAgICAgICAgICAgcHJlY2lzaW9uLFxuICAgICAgICAgICAgICAgICAgICBSb3VuZGluZ01vZGUuUm91bmREb3duXG4gICAgICAgICAgICAgICAgKSA6XG4gICAgICAgICAgICAgICAgYmlnRGVjaW1hbDtcblxuICAgICAgICB0aGlzLmZhY3RvciA9IE1hdGgucG93KDEwLHByZWNpc2lvbik7XG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBkZWNpbWFsLnRpbWVzKHRoaXMuZmFjdG9yKS5yb3VuZCgwLFJvdW5kaW5nTW9kZS5Sb3VuZERvd24pO1xuICAgIH1cblxuXG4gICAgLyoqKiBNVVRBVElORyBNRVRIT0RTICoqKi9cbiAgICBhZGRBbmRSZXBsYWNlKG90aGVyOkludEJhc2VkTnVtKSB7XG4gICAgICAgIGlmICh0aGlzLnByZWNpc2lvbiAhPSBvdGhlci5wcmVjaXNpb24pIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcigncHJlY2lzaW9uIG9mIGJvdGggb3BlcmFuZHMgbXVzdCBiZSB0aGUgc2FtZSEnKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgY29uc3Qgc3VtID0gdGhpcy5pbnRlZ2VyLnBsdXMob3RoZXIuaW50ZWdlcik7XG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBzdW07XG5cbiAgICAgICAgLy9yZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBzdWJ0cmFjdEFuZFJlcGxhY2Uob3RoZXI6SW50QmFzZWROdW0pIHtcbiAgICAgICAgaWYgKHRoaXMucHJlY2lzaW9uICE9IG90aGVyLnByZWNpc2lvbikge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdwcmVjaXNpb24gb2YgYm90aCBvcGVyYW5kcyBtdXN0IGJlIHRoZSBzYW1lIScpO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCBkaWZmID0gdGhpcy5pbnRlZ2VyLm1pbnVzKG90aGVyLmludGVnZXIpO1xuICAgICAgICB0aGlzLl9pbnRlZ2VyID0gZGlmZjtcblxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9IFxuICAgIC8qKiovXG5cblxuICAgIG11bHRpcGx5KG90aGVyOkludEJhc2VkTnVtKSB7XG4gICAgICAgIHJldHVybiBpbnRlZ2VyTXVsdGlwbGljYXRpb24oXG4gICAgICAgICAgICB0aGlzLFxuICAgICAgICAgICAgb3RoZXJcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBtdWx0aXBseURlY2ltYWwoZGVjaW1hbDpudW1iZXIpIHtcbiAgICAgICAgY29uc3Qgb3RoZXIgPSBuZXcgSW50QmFzZWROdW0odGhpcy5wcmVjaXNpb24sZGVjaW1hbCk7XG5cbiAgICAgICAgcmV0dXJuIGludGVnZXJNdWx0aXBsaWNhdGlvbihcbiAgICAgICAgICAgIHRoaXMsXG4gICAgICAgICAgICBvdGhlclxuICAgICAgICApO1xuICAgIH1cblxuICAgIGRpdmlkZUJ5RGVjaW1hbChkZWNpbWFsOm51bWJlcikge1xuICAgICAgICBjb25zdCBvdGhlciA9IG5ldyBJbnRCYXNlZE51bShcbiAgICAgICAgICAgIHRoaXMucHJlY2lzaW9uLFxuICAgICAgICAgICAgZGVjaW1hbC50b1N0cmluZygpXG4gICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIGludGVnZXJEaXZpc2lvbihcbiAgICAgICAgICAgIHRoaXMsXG4gICAgICAgICAgICBvdGhlclxuICAgICAgICApO1xuICAgIH1cblxuXG4gICAgZ2V0IHJlY2lwcm9jYWwoKTpJbnRCYXNlZE51bSB7XG4gICAgICAgIGlmICh0aGlzLl9yZWNpcHJvY2FsID09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fcmVjaXByb2NhbCA9IGludGVnZXJEaXZpc2lvbihcbiAgICAgICAgICAgICAgICBuZXcgSW50QmFzZWROdW0oXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJlY2lzaW9uLFxuICAgICAgICAgICAgICAgICAgICAnMSdcbiAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgIHRoaXNcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdGhpcy5fcmVjaXByb2NhbDtcbiAgICB9XG5cbiAgICBnZXQgb3Bwb3NpdGVfKCk6SW50QmFzZWROdW0ge1xuICAgICAgICByZXR1cm4gaW50ZWdlck11bHRpcGxpY2F0aW9uKFxuICAgICAgICAgICAgbmV3IEludEJhc2VkTnVtKFxuICAgICAgICAgICAgICAgIHRoaXMucHJlY2lzaW9uLFxuICAgICAgICAgICAgICAgICctMSdcbiAgICAgICAgICAgICksXG4gICAgICAgICAgICB0aGlzXG4gICAgICAgICk7XG4gICAgfVxuICAgIFxuICAgIGdldCBkZWNpbWFsQXNOdW1iZXIoKSB7XG4gICAgICAgIHJldHVybiBOdW1iZXIodGhpcy5kZWNpbWFsKTtcbiAgICB9XG5cbiAgICBnZXQgaW50ZWdlcigpOkJpZyB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pbnRlZ2VyO1xuICAgIH1cblxuICAgIHRvU3RyaW5nKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNpbWFsO1xuICAgIH1cblxuICAgIGdldCBkZWNpbWFsV2l0aG91dFRyYWlsaW5nWmVyb3MoKSB7XG4gICAgICAgIHJldHVybiByb3VuZERvd25XaXRob3V0VHJhaWxpbmdaZXJvcyhcbiAgICAgICAgICAgIHRoaXMuZGVjaW1hbCxcbiAgICAgICAgICAgIHRoaXMucHJlY2lzaW9uXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgZ2V0IGRlY2ltYWwoKSB7XG4gICAgICAgIEJpZy5STSA9IFJvdW5kaW5nTW9kZS5Sb3VuZERvd247XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuaW50ZWdlci5kaXYoXG4gICAgICAgICAgICB0aGlzLmZhY3RvclxuICAgICAgICApLnRvRml4ZWQoXG4gICAgICAgICAgICB0aGlzLnByZWNpc2lvblxuICAgICAgICApO1xuICAgIH1cblxuICAgIC8qXG4gICAgdG9IaWdoZXJQcmVjaXNpb24obmV3UHJlY2lzaW9uOm51bWJlcik6SW50QmFzZWROdW0ge1xuICAgICAgICBpZiAobmV3UHJlY2lzaW9uIDwgdGhpcy5wcmVjaXNpb24pIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignTmV3IFByZWNpc2lvbiBNdXN0IGJlIEhpZ2hlciEnKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgcmV0dXJuIEludEJhc2VkTnVtLmZyb21JbnRlZ2VyKFxuICAgICAgICAgICAgdGhpcy5pbnRlZ2VyLnRpbWVzKFxuICAgICAgICAgICAgICAgIE1hdGgucG93KFxuICAgICAgICAgICAgICAgICAgICAxMCxcbiAgICAgICAgICAgICAgICAgICAgbmV3UHJlY2lzaW9uLXRoaXMucHJlY2lzaW9uXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKS50b1N0cmluZygpLFxuICAgICAgICAgICAgbmV3UHJlY2lzaW9uXG4gICAgICAgICk7XG4gICAgfVxuICAgICovXG59XG5cbmV4cG9ydCBjbGFzcyBBc3NldEFtb3VudCBleHRlbmRzIEludEJhc2VkTnVtXG57XG4gICAgLy9wcml2YXRlIF9kZWNpbWFsOnN0cmluZztcbiAgICBwcml2YXRlIF9zeW1ib2w6c3RyaW5nO1xuXG4gICAgcHJpdmF0ZSBfaXNaZXJvOmJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3RvcihkYXRhOnN0cmluZykge1xuICAgICAgICBjb25zdCBwYXJ0cyA9IGRhdGEuc3BsaXQoJyAnKTtcblxuICAgICAgICBjb25zdCBkZWNpbWFsID0gcGFydHNbMF07XG4gICAgICAgIGNvbnN0IHN5bWJvbCA9IHBhcnRzWzFdO1xuXG4gICAgICAgIGNvbnN0IHByZWNpc2lvbiA9IGRlY2ltYWwuc3BsaXQoJy4nKVsxXS5sZW5ndGg7XG5cblxuICAgICAgICBzdXBlcihwcmVjaXNpb24sZGVjaW1hbCk7XG5cbiAgICAgICAgXG4gICAgICAgIHRoaXMuX3N5bWJvbCA9IHN5bWJvbDtcblxuICAgICAgICB0aGlzLl9pc1plcm8gPSBOdW1iZXIoZGVjaW1hbCkgPT0gMDtcbiAgICB9XG5cbiAgICBnZXQgaXNaZXJvKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faXNaZXJvO1xuICAgIH1cblxuICAgIGdldCBzeW1ib2woKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9zeW1ib2w7XG4gICAgfVxufVxuXG5cblxuLy9leHBvcnQgXG5mdW5jdGlvbiBpbnRlZ2VyQWRkaXRpb248VCBleHRlbmRzIEludEJhc2VkTnVtPihcbiAgICBhOlQsYjpUXG4pOkludEJhc2VkTnVtIHtcbiAgICBpZiAoYS5wcmVjaXNpb24gIT0gYi5wcmVjaXNpb24pIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgICAgICAgJ3ByZWNpc2lvbiBvZiBhOiAnICsgYS5wcmVjaXNpb24gK1xuICAgICAgICAgICAgJywgcHJlY2lzaW9uIG9mIGI6ICcgKyBiLnByZWNpc2lvbiArXG4gICAgICAgICAgICBcIlxcbnByZWNpc2lvbiBvZiBib3RoIG9wZXJhbmRzIG11c3QgYmUgdGhlIHNhbWUhXCJcbiAgICAgICAgKTtcbiAgICB9XG5cblxuXG4gICAgY29uc3Qgc3VtID0gYS5pbnRlZ2VyLnBsdXMoYi5pbnRlZ2VyKTtcblxuICAgIHJldHVybiBJbnRCYXNlZE51bS5mcm9tSW50ZWdlcihcbiAgICAgICAgc3VtLnRvU3RyaW5nKCksXG4gICAgICAgIGEucHJlY2lzaW9uXG4gICAgKTtcbn1cblxuLy9leHBvcnQgXG5mdW5jdGlvbiBpbnRlZ2VyU3VidHJhY3Rpb248VCBleHRlbmRzIEludEJhc2VkTnVtPihcbiAgICBhOlQsYjpUXG4pOkludEJhc2VkTnVtIHtcbiAgICBpZiAoYS5wcmVjaXNpb24gIT0gYi5wcmVjaXNpb24pIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdwcmVjaXNpb24gb2YgYm90aCBvcGVyYW5kcyBtdXN0IGJlIHRoZSBzYW1lIScpO1xuICAgIH1cblxuXG5cbiAgICBjb25zdCBkaWZmID0gYS5pbnRlZ2VyLm1pbnVzKGIuaW50ZWdlcik7XG5cbiAgICByZXR1cm4gSW50QmFzZWROdW0uZnJvbUludGVnZXIoXG4gICAgICAgIGRpZmYudG9TdHJpbmcoKSxcbiAgICAgICAgYS5wcmVjaXNpb25cbiAgICApO1xufVxuXG4vL2V4cG9ydCBcbmZ1bmN0aW9uIGludGVnZXJNdWx0aXBsaWNhdGlvbjxUIGV4dGVuZHMgSW50QmFzZWROdW0+KFxuICAgIGE6VCxiOlRcbik6SW50QmFzZWROdW0ge1xuICAgIGlmIChhLnByZWNpc2lvbiA8IGIucHJlY2lzaW9uKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGEuZGVjaW1hbCk7XG4gICAgICAgIGNvbnNvbGUubG9nKGIuZGVjaW1hbCk7XG5cbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgICAgICAgJ3ByZWNpc2lvbiBvZiBhOiAnICsgYS5wcmVjaXNpb24gK1xuICAgICAgICAgICAgJywgcHJlY2lzaW9uIG9mIGI6ICcgKyBiLnByZWNpc2lvbiArXG4gICAgICAgICAgICBcIlxcbnByZWNpc2lvbiBvZiBBIG11c3QgYmUgPj0gQiFcIlxuICAgICAgICApO1xuICAgIH1cblxuXG5cbiAgICBjb25zdCBwcm9kdWN0ID0gYS5pbnRlZ2VyLnRpbWVzKGIuaW50ZWdlcik7XG5cbiAgICBjb25zdCBpbnRlZ2VyID0gcHJvZHVjdC5kaXYoYi5mYWN0b3IpXG4gICAgICAgICAgICAgICAgICAgIC5yb3VuZCgwLFJvdW5kaW5nTW9kZS5Sb3VuZERvd24pXG4gICAgICAgICAgICAgICAgICAgIC50b1N0cmluZygpO1xuXG4gICAgY29uc3QgcmVzdWx0ID0gSW50QmFzZWROdW0uZnJvbUludGVnZXIoXG4gICAgICAgIGludGVnZXIsXG4gICAgICAgIGEucHJlY2lzaW9uXG4gICAgKTtcblxuICAgIC8vY29uc29sZS5sb2coJ0lOVCBNdWx0aXBsaWNhdGlvbjogJythKycgKiAnK2IrJyA9ICcrcmVzdWx0KTtcblxuICAgIHJldHVybiByZXN1bHQ7XG59XG5cblxuLy8gd2hlbiBkb2luZyBkaXZpc2lvbiBiZXR3ZWVuIHR3byBlb3MgbnVtYmVycyB0aGVuIGRpc3JlZ2FyZFxuLy8gdGhlIGRlY2ltYWwgYWZ0ZXIgdGhlIGludGVnZXJcbi8vZXhwb3J0IFxuZnVuY3Rpb24gaW50ZWdlckRpdmlzaW9uPFQgZXh0ZW5kcyBJbnRCYXNlZE51bT4oXG4gICAgYTpULGI6VFxuKTpJbnRCYXNlZE51bSB7XG4gICAgaWYgKGEucHJlY2lzaW9uIDwgYi5wcmVjaXNpb24pIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdwcmVjaXNpb24gb2YgQSBtdXN0IGJlID49IEIhJyk7XG4gICAgfVxuXG5cblxuICAgIGNvbnN0IHF1b3RpZW50ID0gYS5pbnRlZ2VyLmRpdihiLmludGVnZXIpO1xuXG4gICAgY29uc3QgaW50ZWdlciA9IHF1b3RpZW50LnRpbWVzKGIuZmFjdG9yKVxuICAgICAgICAgICAgICAgICAgICAucm91bmQoMCxSb3VuZGluZ01vZGUuUm91bmREb3duKVxuICAgICAgICAgICAgICAgICAgICAudG9TdHJpbmcoKTtcbiAgICBcbiAgICBjb25zdCByZXN1bHQgPSBJbnRCYXNlZE51bS5mcm9tSW50ZWdlcihcbiAgICAgICAgaW50ZWdlcixcbiAgICAgICAgYS5wcmVjaXNpb25cbiAgICApO1xuXG4gICAgY29uc29sZS5sb2coJ0lOVCBEaXZpc2lvbjogJythKycgLyAnK2IrJyA9ICcrcmVzdWx0KTtcblxuICAgIHJldHVybiByZXN1bHQ7XG59XG4iXX0=