/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/bets/bet-base.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { AssetAmount } from "../../math/integer-math";
import { getStakingLevelName } from '../staking/staking-level-data';
/**
 * @abstract
 */
export class BetBase {
    /**
     * @param {?} data
     */
    constructor(data) {
        this.id = data.id;
        this.time = new Date(data.resolvedMilliseconds);
        this.bettor = data.bettor;
        /** @type {?} */
        const parts = data.amount.split(' ');
        this.betAmount = new AssetAmount(data.amount);
        this.amount = this.betAmount.decimalWithoutTrailingZeros;
        this.tokenSymbol = parts[1];
        this.jackpotSpin = data.jackpotSpin;
        this.isJackpotBet = this.jackpotSpin && this.jackpotSpin.length > 0;
        this.stakeWeight =
            data.stakeWeight ?
                Number(data.stakeWeight) :
                0;
        this.stakingLevelName = getStakingLevelName(this.stakeWeight);
    }
}
if (false) {
    /** @type {?} */
    BetBase.prototype.id;
    /** @type {?} */
    BetBase.prototype.time;
    /** @type {?} */
    BetBase.prototype.bettor;
    /** @type {?} */
    BetBase.prototype.amount;
    /** @type {?} */
    BetBase.prototype.tokenSymbol;
    /** @type {?} */
    BetBase.prototype.betAmount;
    /** @type {?} */
    BetBase.prototype.payout;
    /** @type {?} */
    BetBase.prototype.jackpotSpin;
    /** @type {?} */
    BetBase.prototype.isJackpotBet;
    /** @type {?} */
    BetBase.prototype.stakeWeight;
    /** @type {?} */
    BetBase.prototype.stakingLevelName;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmV0LWJhc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL19jb21tb24vbW9kZWxzL2JldHMvYmV0LWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDdEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sK0JBQStCLENBQUM7Ozs7QUFHcEUsTUFBTTs7OztJQWtCRixZQUFZLElBQWlCO1FBQ3pCLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBRWhELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQzs7Y0FFcEIsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUVwQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUU5QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsMkJBQTJCLENBQUM7UUFFekQsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFFcEUsSUFBSSxDQUFDLFdBQVc7WUFDWixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2xCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDMUIsQ0FBQyxDQUFDO1FBRU4sSUFBSSxDQUFDLGdCQUFnQixHQUFHLG1CQUFtQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNsRSxDQUFDO0NBQ0o7OztJQXhDRyxxQkFBbUI7O0lBQ25CLHVCQUFtQjs7SUFDbkIseUJBQXVCOztJQUV2Qix5QkFBdUI7O0lBQ3ZCLDhCQUE0Qjs7SUFFNUIsNEJBQStCOztJQUMvQix5QkFBdUI7O0lBRXZCLDhCQUE0Qjs7SUFDNUIsK0JBQThCOztJQUU5Qiw4QkFBNEI7O0lBQzVCLG1DQUFpQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElSZXNvbHZlZEJldCB9IGZyb20gXCJlYXJuYmV0LWNvbW1vblwiO1xuaW1wb3J0IHsgQXNzZXRBbW91bnQgfSBmcm9tIFwiLi4vLi4vbWF0aC9pbnRlZ2VyLW1hdGhcIjtcbmltcG9ydCB7IGdldFN0YWtpbmdMZXZlbE5hbWUgfSBmcm9tICcuLi9zdGFraW5nL3N0YWtpbmctbGV2ZWwtZGF0YSc7XG5cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEJldEJhc2VcbntcbiAgICByZWFkb25seSBpZDpzdHJpbmc7XG4gICAgcmVhZG9ubHkgdGltZTpEYXRlO1xuICAgIHJlYWRvbmx5IGJldHRvcjpzdHJpbmc7XG5cbiAgICByZWFkb25seSBhbW91bnQ6c3RyaW5nO1xuICAgIHJlYWRvbmx5IHRva2VuU3ltYm9sOnN0cmluZztcblxuICAgIHJlYWRvbmx5IGJldEFtb3VudDpBc3NldEFtb3VudDtcbiAgICByZWFkb25seSBwYXlvdXQ6c3RyaW5nO1xuXG4gICAgcmVhZG9ubHkgamFja3BvdFNwaW46c3RyaW5nO1xuICAgIHJlYWRvbmx5IGlzSmFja3BvdEJldDpib29sZWFuO1xuXG4gICAgcmVhZG9ubHkgc3Rha2VXZWlnaHQ6bnVtYmVyO1xuICAgIHJlYWRvbmx5IHN0YWtpbmdMZXZlbE5hbWU6c3RyaW5nO1xuICAgIFxuICAgIGNvbnN0cnVjdG9yKGRhdGE6SVJlc29sdmVkQmV0KSB7XG4gICAgICAgIHRoaXMuaWQgPSBkYXRhLmlkO1xuICAgICAgICB0aGlzLnRpbWUgPSBuZXcgRGF0ZShkYXRhLnJlc29sdmVkTWlsbGlzZWNvbmRzKTtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuYmV0dG9yID0gZGF0YS5iZXR0b3I7XG5cbiAgICAgICAgY29uc3QgcGFydHMgPSBkYXRhLmFtb3VudC5zcGxpdCgnICcpO1xuXG4gICAgICAgIHRoaXMuYmV0QW1vdW50ID0gbmV3IEFzc2V0QW1vdW50KGRhdGEuYW1vdW50KTtcblxuICAgICAgICB0aGlzLmFtb3VudCA9IHRoaXMuYmV0QW1vdW50LmRlY2ltYWxXaXRob3V0VHJhaWxpbmdaZXJvcztcblxuICAgICAgICB0aGlzLnRva2VuU3ltYm9sID0gcGFydHNbMV07XG5cbiAgICAgICAgdGhpcy5qYWNrcG90U3BpbiA9IGRhdGEuamFja3BvdFNwaW47XG4gICAgICAgIHRoaXMuaXNKYWNrcG90QmV0ID0gdGhpcy5qYWNrcG90U3BpbiAmJiB0aGlzLmphY2twb3RTcGluLmxlbmd0aCA+IDA7XG5cbiAgICAgICAgdGhpcy5zdGFrZVdlaWdodCA9IFxuICAgICAgICAgICAgZGF0YS5zdGFrZVdlaWdodCA/XG4gICAgICAgICAgICBOdW1iZXIoZGF0YS5zdGFrZVdlaWdodCkgOlxuICAgICAgICAgICAgMDtcblxuICAgICAgICB0aGlzLnN0YWtpbmdMZXZlbE5hbWUgPSBnZXRTdGFraW5nTGV2ZWxOYW1lKHRoaXMuc3Rha2VXZWlnaHQpO1xuICAgIH1cbn0iXX0=