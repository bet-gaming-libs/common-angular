/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/bets/blackjack-bet.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BetBase } from "./bet-base";
import { AssetAmount } from '../../math/integer-math';
export class BlackjackBetResult extends BetBase {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data);
        this.game = 'blackjack';
        this.description = describeResults(data.playerResults, data.dealerHandResult);
        /** @type {?} */
        const totalWagered = new AssetAmount(data.totalWagered);
        /** @type {?} */
        const totalPayout = new AssetAmount(data.totalPayout);
        this.payout = totalPayout.decimalWithoutTrailingZeros;
        this.isWin = totalPayout.integer.gte(totalWagered.integer);
        this.profit = Number(totalPayout.subtractAndReplace(totalWagered).decimal);
    }
}
if (false) {
    /** @type {?} */
    BlackjackBetResult.prototype.game;
    /** @type {?} */
    BlackjackBetResult.prototype.description;
    /** @type {?} */
    BlackjackBetResult.prototype.isWin;
    /** @type {?} */
    BlackjackBetResult.prototype.payout;
    /** @type {?} */
    BlackjackBetResult.prototype.profit;
}
/**
 * @param {?} playerResults
 * @param {?} dealerHandResult
 * @return {?}
 */
function describeResults(playerResults, dealerHandResult) {
    /** @type {?} */
    const parts = [];
    for (var result of playerResults) {
        parts.push(describeResult(result, dealerHandResult));
    }
    return parts.join(' ~ ');
}
/**
 * @param {?} player
 * @param {?} dealer
 * @return {?}
 */
function describeResult(player, dealer) {
    /** @type {?} */
    let left = '' + player.sum;
    /** @type {?} */
    let sign;
    /** @type {?} */
    let right = '' + dealer.sum;
    if (player.isBlackjack) {
        left = 'BJ';
    }
    if (dealer.isBlackjack) {
        right = 'BJ';
    }
    if (player.sum > 21) {
        sign = '>';
        right = '21';
    }
    else if (player.sum < dealer.sum) {
        sign = '<';
    }
    else if (player.sum > dealer.sum) {
        sign = '>';
    }
    else {
        // sums are equal (it may be a push) 
        if (player.sum < 21) {
            // PUSH if less than 21
            sign = '=';
        }
        else {
            // BOTH SUMS ARE EQUAL TO 21
            // PUSH if both have BJ
            if (player.isBlackjack && dealer.isBlackjack) {
                sign = '=';
            }
            else if (player.isBlackjack) {
                sign = '>';
            }
            else {
                // dealer blackjack
                sign = '<';
            }
        }
    }
    return left + ' ' + sign + ' ' + right;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWJldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9tb2RlbHMvYmV0cy9ibGFja2phY2stYmV0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBRUEsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLFlBQVksQ0FBQztBQUVyQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFHdEQsTUFBTSx5QkFBMEIsU0FBUSxPQUFPOzs7O0lBUzNDLFlBQVksSUFBMEI7UUFDbEMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBUlAsU0FBSSxHQUFHLFdBQVcsQ0FBQztRQVd4QixJQUFJLENBQUMsV0FBVyxHQUFHLGVBQWUsQ0FDOUIsSUFBSSxDQUFDLGFBQWEsRUFDbEIsSUFBSSxDQUFDLGdCQUFnQixDQUN4QixDQUFDOztjQUVJLFlBQVksR0FBRyxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDOztjQUNqRCxXQUFXLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUVyRCxJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQywyQkFBMkIsQ0FBQztRQUV0RCxJQUFJLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUNoQyxZQUFZLENBQUMsT0FBTyxDQUN2QixDQUFDO1FBRUYsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQ2hCLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLENBQ3ZELENBQUM7SUFDTixDQUFDO0NBQ0o7OztJQTdCRyxrQ0FBNEI7O0lBRTVCLHlDQUE0Qjs7SUFDNUIsbUNBQXVCOztJQUN2QixvQ0FBdUI7O0lBQ3ZCLG9DQUF1Qjs7Ozs7OztBQTJCM0IseUJBQ0ksYUFBb0MsRUFDcEMsZ0JBQXFDOztVQUUvQixLQUFLLEdBQVksRUFBRTtJQUV6QixLQUFLLElBQUksTUFBTSxJQUFJLGFBQWEsRUFBRTtRQUM5QixLQUFLLENBQUMsSUFBSSxDQUNOLGNBQWMsQ0FDVixNQUFNLEVBQ04sZ0JBQWdCLENBQ25CLENBQ0osQ0FBQztLQUNMO0lBRUQsT0FBTyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQzdCLENBQUM7Ozs7OztBQUVELHdCQUNJLE1BQTJCLEVBQzNCLE1BQTJCOztRQUV2QixJQUFJLEdBQVUsRUFBRSxHQUFDLE1BQU0sQ0FBQyxHQUFHOztRQUMzQixJQUFXOztRQUNYLEtBQUssR0FBVSxFQUFFLEdBQUMsTUFBTSxDQUFDLEdBQUc7SUFHaEMsSUFBSSxNQUFNLENBQUMsV0FBVyxFQUFFO1FBQ3BCLElBQUksR0FBRyxJQUFJLENBQUM7S0FDZjtJQUNELElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRTtRQUNwQixLQUFLLEdBQUcsSUFBSSxDQUFDO0tBQ2hCO0lBR0QsSUFBSSxNQUFNLENBQUMsR0FBRyxHQUFHLEVBQUUsRUFBRTtRQUNqQixJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQ1gsS0FBSyxHQUFHLElBQUksQ0FBQztLQUNoQjtTQUNJLElBQUksTUFBTSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxFQUFFO1FBQzlCLElBQUksR0FBRyxHQUFHLENBQUM7S0FDZDtTQUNJLElBQUksTUFBTSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxFQUFFO1FBQzlCLElBQUksR0FBRyxHQUFHLENBQUM7S0FDZDtTQUNJO1FBQ0QscUNBQXFDO1FBQ3JDLElBQUksTUFBTSxDQUFDLEdBQUcsR0FBRyxFQUFFLEVBQUU7WUFDakIsdUJBQXVCO1lBQ3ZCLElBQUksR0FBRyxHQUFHLENBQUM7U0FDZDthQUFNO1lBQ0gsNEJBQTRCO1lBRTVCLHVCQUF1QjtZQUN2QixJQUFJLE1BQU0sQ0FBQyxXQUFXLElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRTtnQkFDMUMsSUFBSSxHQUFHLEdBQUcsQ0FBQzthQUNkO2lCQUNJLElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRTtnQkFDekIsSUFBSSxHQUFHLEdBQUcsQ0FBQzthQUNkO2lCQUNJO2dCQUNELG1CQUFtQjtnQkFDbkIsSUFBSSxHQUFHLEdBQUcsQ0FBQzthQUNkO1NBQ0o7S0FDSjtJQUVELE9BQU8sSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQztBQUMzQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJUmVzb2x2ZWRCbGFja2phY2tCZXQsIElCbGFja2phY2tIYW5kUmVzdWx0fSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5cbmltcG9ydCB7IEJldEJhc2UgfSBmcm9tIFwiLi9iZXQtYmFzZVwiO1xuaW1wb3J0IHsgSUJldFJlc3VsdCB9IGZyb20gXCIuL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IEFzc2V0QW1vdW50IH0gZnJvbSAnLi4vLi4vbWF0aC9pbnRlZ2VyLW1hdGgnO1xuXG5cbmV4cG9ydCBjbGFzcyBCbGFja2phY2tCZXRSZXN1bHQgZXh0ZW5kcyBCZXRCYXNlIGltcGxlbWVudHMgSUJldFJlc3VsdFxue1xuICAgIHJlYWRvbmx5IGdhbWUgPSAnYmxhY2tqYWNrJztcblxuICAgIHJlYWRvbmx5IGRlc2NyaXB0aW9uOnN0cmluZztcbiAgICByZWFkb25seSBpc1dpbjpib29sZWFuO1xuICAgIHJlYWRvbmx5IHBheW91dDpzdHJpbmc7XG4gICAgcmVhZG9ubHkgcHJvZml0Om51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKGRhdGE6SVJlc29sdmVkQmxhY2tqYWNrQmV0KSB7XG4gICAgICAgIHN1cGVyKGRhdGEpO1xuXG4gICAgICAgIFxuICAgICAgICB0aGlzLmRlc2NyaXB0aW9uID0gZGVzY3JpYmVSZXN1bHRzKFxuICAgICAgICAgICAgZGF0YS5wbGF5ZXJSZXN1bHRzLFxuICAgICAgICAgICAgZGF0YS5kZWFsZXJIYW5kUmVzdWx0XG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgdG90YWxXYWdlcmVkID0gbmV3IEFzc2V0QW1vdW50KGRhdGEudG90YWxXYWdlcmVkKTtcbiAgICAgICAgY29uc3QgdG90YWxQYXlvdXQgPSBuZXcgQXNzZXRBbW91bnQoZGF0YS50b3RhbFBheW91dCk7XG5cbiAgICAgICAgdGhpcy5wYXlvdXQgPSB0b3RhbFBheW91dC5kZWNpbWFsV2l0aG91dFRyYWlsaW5nWmVyb3M7XG5cbiAgICAgICAgdGhpcy5pc1dpbiA9IHRvdGFsUGF5b3V0LmludGVnZXIuZ3RlKFxuICAgICAgICAgICAgdG90YWxXYWdlcmVkLmludGVnZXJcbiAgICAgICAgKTtcblxuICAgICAgICB0aGlzLnByb2ZpdCA9IE51bWJlcihcbiAgICAgICAgICAgIHRvdGFsUGF5b3V0LnN1YnRyYWN0QW5kUmVwbGFjZSh0b3RhbFdhZ2VyZWQpLmRlY2ltYWxcbiAgICAgICAgKTtcbiAgICB9XG59XG5cblxuZnVuY3Rpb24gZGVzY3JpYmVSZXN1bHRzKFxuICAgIHBsYXllclJlc3VsdHM6SUJsYWNramFja0hhbmRSZXN1bHRbXSxcbiAgICBkZWFsZXJIYW5kUmVzdWx0OklCbGFja2phY2tIYW5kUmVzdWx0XG4pIHtcbiAgICBjb25zdCBwYXJ0czpzdHJpbmdbXSA9IFtdO1xuXG4gICAgZm9yICh2YXIgcmVzdWx0IG9mIHBsYXllclJlc3VsdHMpIHtcbiAgICAgICAgcGFydHMucHVzaChcbiAgICAgICAgICAgIGRlc2NyaWJlUmVzdWx0KFxuICAgICAgICAgICAgICAgIHJlc3VsdCxcbiAgICAgICAgICAgICAgICBkZWFsZXJIYW5kUmVzdWx0XG4gICAgICAgICAgICApXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHBhcnRzLmpvaW4oJyB+ICcpO1xufVxuXG5mdW5jdGlvbiBkZXNjcmliZVJlc3VsdChcbiAgICBwbGF5ZXI6SUJsYWNramFja0hhbmRSZXN1bHQsXG4gICAgZGVhbGVyOklCbGFja2phY2tIYW5kUmVzdWx0XG4pOnN0cmluZyB7XG4gICAgbGV0IGxlZnQ6c3RyaW5nID0gJycrcGxheWVyLnN1bTtcbiAgICBsZXQgc2lnbjpzdHJpbmc7XG4gICAgbGV0IHJpZ2h0OnN0cmluZyA9ICcnK2RlYWxlci5zdW07XG5cblxuICAgIGlmIChwbGF5ZXIuaXNCbGFja2phY2spIHtcbiAgICAgICAgbGVmdCA9ICdCSic7XG4gICAgfVxuICAgIGlmIChkZWFsZXIuaXNCbGFja2phY2spIHtcbiAgICAgICAgcmlnaHQgPSAnQkonO1xuICAgIH1cblxuXG4gICAgaWYgKHBsYXllci5zdW0gPiAyMSkge1xuICAgICAgICBzaWduID0gJz4nO1xuICAgICAgICByaWdodCA9ICcyMSc7XG4gICAgfVxuICAgIGVsc2UgaWYgKHBsYXllci5zdW0gPCBkZWFsZXIuc3VtKSB7XG4gICAgICAgIHNpZ24gPSAnPCc7XG4gICAgfVxuICAgIGVsc2UgaWYgKHBsYXllci5zdW0gPiBkZWFsZXIuc3VtKSB7XG4gICAgICAgIHNpZ24gPSAnPic7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgICAvLyBzdW1zIGFyZSBlcXVhbCAoaXQgbWF5IGJlIGEgcHVzaCkgXG4gICAgICAgIGlmIChwbGF5ZXIuc3VtIDwgMjEpIHtcbiAgICAgICAgICAgIC8vIFBVU0ggaWYgbGVzcyB0aGFuIDIxXG4gICAgICAgICAgICBzaWduID0gJz0nO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gQk9USCBTVU1TIEFSRSBFUVVBTCBUTyAyMVxuXG4gICAgICAgICAgICAvLyBQVVNIIGlmIGJvdGggaGF2ZSBCSlxuICAgICAgICAgICAgaWYgKHBsYXllci5pc0JsYWNramFjayAmJiBkZWFsZXIuaXNCbGFja2phY2spIHtcbiAgICAgICAgICAgICAgICBzaWduID0gJz0nO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAocGxheWVyLmlzQmxhY2tqYWNrKSB7XG4gICAgICAgICAgICAgICAgc2lnbiA9ICc+JztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vIGRlYWxlciBibGFja2phY2tcbiAgICAgICAgICAgICAgICBzaWduID0gJzwnO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGxlZnQgKyAnICcgKyBzaWduICsgJyAnICsgcmlnaHQ7XG59Il19