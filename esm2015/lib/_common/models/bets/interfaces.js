/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/bets/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IBetResult() { }
if (false) {
    /** @type {?} */
    IBetResult.prototype.id;
    /** @type {?} */
    IBetResult.prototype.game;
    /** @type {?} */
    IBetResult.prototype.time;
    /** @type {?} */
    IBetResult.prototype.bettor;
    /** @type {?} */
    IBetResult.prototype.description;
    /** @type {?} */
    IBetResult.prototype.tokenSymbol;
    /** @type {?} */
    IBetResult.prototype.amount;
    /** @type {?} */
    IBetResult.prototype.isWin;
    /** @type {?} */
    IBetResult.prototype.payout;
    /** @type {?} */
    IBetResult.prototype.profit;
    /** @type {?} */
    IBetResult.prototype.jackpotSpin;
    /** @type {?} */
    IBetResult.prototype.isJackpotBet;
    /** @type {?|undefined} */
    IBetResult.prototype.betTxnId;
    /** @type {?|undefined} */
    IBetResult.prototype.resolveTxnId;
    /** @type {?|undefined} */
    IBetResult.prototype.chainId;
}
/**
 * @record
 */
export function IDiceBetResult() { }
/**
 * @record
 */
export function ICrashBetResult() { }
/**
 * @record
 */
export function IBaccaratBetResult() { }
/**
 * @record
 */
export function IBetsListener() { }
if (false) {
    /**
     * @param {?} bet
     * @return {?}
     */
    IBetsListener.prototype.onMyBetResolved = function (bet) { };
}
/**
 * @record
 */
export function IResolvedBets() { }
if (false) {
    /**
     * @param {?} listener
     * @return {?}
     */
    IResolvedBets.prototype.setListener = function (listener) { };
    /**
     * @param {?} txnId
     * @return {?}
     */
    IResolvedBets.prototype.onBetPlaced = function (txnId) { };
    /**
     * @param {?} id
     * @return {?}
     */
    IResolvedBets.prototype.setMyBetId = function (id) { };
    /**
     * @param {?} newBet
     * @return {?}
     */
    IResolvedBets.prototype.onBetResolved = function (newBet) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9tb2RlbHMvYmV0cy9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsZ0NBb0JDOzs7SUFsQkcsd0JBQVU7O0lBQ1YsMEJBQVk7O0lBQ1osMEJBQVU7O0lBQ1YsNEJBQWM7O0lBRWQsaUNBQW1COztJQUNuQixpQ0FBbUI7O0lBQ25CLDRCQUFjOztJQUNkLDJCQUFjOztJQUNkLDRCQUFjOztJQUVkLDRCQUFjOztJQUNkLGlDQUFtQjs7SUFDbkIsa0NBQXFCOztJQUVyQiw4QkFBaUI7O0lBQ2pCLGtDQUFxQjs7SUFDckIsNkJBQWdCOzs7OztBQUdwQixvQ0FHQzs7OztBQUVELHFDQUdDOzs7O0FBRUQsd0NBR0M7Ozs7QUFHRCxtQ0FHQzs7Ozs7O0lBREcsNkRBQThDOzs7OztBQUdsRCxtQ0FNQzs7Ozs7O0lBSkcsOERBQXlDOzs7OztJQUN6QywyREFBaUM7Ozs7O0lBQ2pDLHVEQUEyQjs7Ozs7SUFDM0IsOERBQStDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJQmV0UmVzdWx0XG57XG4gICAgaWQ6c3RyaW5nO1xuICAgIGdhbWU6c3RyaW5nO1xuICAgIHRpbWU6RGF0ZTtcbiAgICBiZXR0b3I6c3RyaW5nO1xuICAgIFxuICAgIGRlc2NyaXB0aW9uOnN0cmluZztcbiAgICB0b2tlblN5bWJvbDpzdHJpbmc7XG4gICAgYW1vdW50OnN0cmluZztcbiAgICBpc1dpbjpib29sZWFuO1xuICAgIHBheW91dDpzdHJpbmc7XG5cbiAgICBwcm9maXQ6bnVtYmVyO1xuICAgIGphY2twb3RTcGluOnN0cmluZztcbiAgICBpc0phY2twb3RCZXQ6Ym9vbGVhbjtcblxuICAgIGJldFR4bklkPzpzdHJpbmc7XG4gICAgcmVzb2x2ZVR4bklkPzpzdHJpbmc7XG4gICAgY2hhaW5JZD86bnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElEaWNlQmV0UmVzdWx0IGV4dGVuZHMgSUJldFJlc3VsdFxue1xuXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUNyYXNoQmV0UmVzdWx0IGV4dGVuZHMgSUJldFJlc3VsdFxue1xuXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUJhY2NhcmF0QmV0UmVzdWx0IGV4dGVuZHMgSUJldFJlc3VsdFxue1xuXG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQmV0c0xpc3RlbmVyXG57XG4gICAgb25NeUJldFJlc29sdmVkKGJldDpJQmV0UmVzdWx0KTpQcm9taXNlPHZvaWQ+O1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElSZXNvbHZlZEJldHNcbntcbiAgICBzZXRMaXN0ZW5lcihsaXN0ZW5lcjpJQmV0c0xpc3RlbmVyKTp2b2lkO1xuICAgIG9uQmV0UGxhY2VkKHR4bklkOnN0cmluZyk6c3RyaW5nO1xuICAgIHNldE15QmV0SWQoaWQ6c3RyaW5nKTp2b2lkO1xuICAgIG9uQmV0UmVzb2x2ZWQobmV3QmV0OklCZXRSZXN1bHQpOlByb21pc2U8dm9pZD47XG59Il19