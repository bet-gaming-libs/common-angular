/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/staking/staking-level-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { betTokenFactor } from "./constants";
/**
 * @record
 */
export function IStakingLevelData() { }
if (false) {
    /** @type {?} */
    IStakingLevelData.prototype.name;
    /** @type {?} */
    IStakingLevelData.prototype.stakePoints;
    /** @type {?} */
    IStakingLevelData.prototype.houseEdgeReduction;
    /** @type {?} */
    IStakingLevelData.prototype.baccaratHouseEdge;
}
/** @type {?} */
export const stakingLevels = [
    { name: 'shrimp', stakePoints: 1000, houseEdgeReduction: 0.05, baccaratHouseEdge: 1.03 },
    { name: 'crab', stakePoints: 10000, houseEdgeReduction: 0.10, baccaratHouseEdge: 0.99 },
    { name: 'octopus', stakePoints: 25000, houseEdgeReduction: 0.12, baccaratHouseEdge: 0.98 },
    { name: 'squid', stakePoints: 50000, houseEdgeReduction: 0.15, baccaratHouseEdge: 0.96 },
    { name: 'fish', stakePoints: 100000, houseEdgeReduction: 0.20, baccaratHouseEdge: 0.92 },
    { name: 'blowfish', stakePoints: 250000, houseEdgeReduction: 0.25, baccaratHouseEdge: 0.89 },
    { name: 'dolphin', stakePoints: 500000, houseEdgeReduction: 0.35, baccaratHouseEdge: 0.82 },
    { name: 'shark', stakePoints: 1000000, houseEdgeReduction: 0.50, baccaratHouseEdge: 0.71 },
    { name: 'whale', stakePoints: 2500000, houseEdgeReduction: 0.75, baccaratHouseEdge: 0.60 },
    { name: 'blue-whale', stakePoints: 5000000, houseEdgeReduction: 1, baccaratHouseEdge: 0.50 },
];
/**
 * @param {?} stakeWeight
 * @return {?}
 */
export function getHouseEdgeReductionAsDecimal(stakeWeight) {
    /** @type {?} */
    const levelIndex = getStakingLevelIndex(stakeWeight);
    /** @type {?} */
    const level = stakingLevels[levelIndex];
    return level ?
        level.houseEdgeReduction / 100 :
        0;
}
/**
 * @param {?} stakeWeight
 * @return {?}
 */
export function getStakingLevelName(stakeWeight) {
    /** @type {?} */
    const levelIndex = getStakingLevelIndex(stakeWeight);
    /** @type {?} */
    const level = stakingLevels[levelIndex];
    return level ?
        level.name :
        undefined;
}
/**
 * @param {?} stakeWeight
 * @return {?}
 */
export function getStakingLevelIndex(stakeWeight) {
    /** @type {?} */
    const points = stakeWeight / betTokenFactor;
    /** @type {?} */
    const firstLevel = stakingLevels[0];
    if (points < firstLevel.stakePoints) {
        return -1;
    }
    for (let i = 0; i < stakingLevels.length; i++) {
        /** @type {?} */
        const nextLevel = stakingLevels[i + 1];
        if (!nextLevel ||
            points < nextLevel.stakePoints) {
            return i;
        }
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Rha2luZy1sZXZlbC1kYXRhLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL21vZGVscy9zdGFraW5nL3N0YWtpbmctbGV2ZWwtZGF0YS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxhQUFhLENBQUM7Ozs7QUFHN0MsdUNBTUM7OztJQUpHLGlDQUFZOztJQUNaLHdDQUFtQjs7SUFDbkIsK0NBQTBCOztJQUMxQiw4Q0FBeUI7OztBQUk3QixNQUFNLE9BQU8sYUFBYSxHQUF1QjtJQUM3QyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQU0sV0FBVyxFQUFFLElBQUksRUFBTyxrQkFBa0IsRUFBRSxJQUFJLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxFQUFDO0lBQzlGLEVBQUMsSUFBSSxFQUFDLE1BQU0sRUFBUSxXQUFXLEVBQUUsS0FBSyxFQUFNLGtCQUFrQixFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxJQUFJLEVBQUM7SUFDOUYsRUFBQyxJQUFJLEVBQUMsU0FBUyxFQUFLLFdBQVcsRUFBRSxLQUFLLEVBQU0sa0JBQWtCLEVBQUUsSUFBSSxFQUFFLGlCQUFpQixFQUFFLElBQUksRUFBQztJQUM5RixFQUFDLElBQUksRUFBQyxPQUFPLEVBQU8sV0FBVyxFQUFFLEtBQUssRUFBTSxrQkFBa0IsRUFBRSxJQUFJLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxFQUFDO0lBQzlGLEVBQUMsSUFBSSxFQUFDLE1BQU0sRUFBUSxXQUFXLEVBQUUsTUFBTSxFQUFLLGtCQUFrQixFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxJQUFJLEVBQUM7SUFDOUYsRUFBQyxJQUFJLEVBQUMsVUFBVSxFQUFJLFdBQVcsRUFBRSxNQUFNLEVBQUssa0JBQWtCLEVBQUUsSUFBSSxFQUFFLGlCQUFpQixFQUFFLElBQUksRUFBQztJQUM5RixFQUFDLElBQUksRUFBQyxTQUFTLEVBQUssV0FBVyxFQUFFLE1BQU0sRUFBSyxrQkFBa0IsRUFBRSxJQUFJLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxFQUFDO0lBQzlGLEVBQUMsSUFBSSxFQUFDLE9BQU8sRUFBTyxXQUFXLEVBQUUsT0FBTyxFQUFJLGtCQUFrQixFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxJQUFJLEVBQUM7SUFDOUYsRUFBQyxJQUFJLEVBQUMsT0FBTyxFQUFPLFdBQVcsRUFBRSxPQUFPLEVBQUksa0JBQWtCLEVBQUUsSUFBSSxFQUFFLGlCQUFpQixFQUFFLElBQUksRUFBQztJQUM5RixFQUFDLElBQUksRUFBQyxZQUFZLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBSSxrQkFBa0IsRUFBRSxDQUFDLEVBQUssaUJBQWlCLEVBQUUsSUFBSSxFQUFDO0NBQ2pHOzs7OztBQUdELE1BQU0seUNBQXlDLFdBQWtCOztVQUN2RCxVQUFVLEdBQUcsb0JBQW9CLENBQUMsV0FBVyxDQUFDOztVQUU5QyxLQUFLLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQztJQUV2QyxPQUFPLEtBQUssQ0FBQyxDQUFDO1FBQ04sS0FBSyxDQUFDLGtCQUFrQixHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLENBQUMsQ0FBQztBQUNkLENBQUM7Ozs7O0FBRUQsTUFBTSw4QkFBOEIsV0FBa0I7O1VBQzVDLFVBQVUsR0FBRyxvQkFBb0IsQ0FBQyxXQUFXLENBQUM7O1VBRTlDLEtBQUssR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDO0lBRXZDLE9BQU8sS0FBSyxDQUFDLENBQUM7UUFDTixLQUFLLENBQUMsSUFBSSxDQUFBLENBQUM7UUFDWCxTQUFTLENBQUM7QUFDdEIsQ0FBQzs7Ozs7QUFFRCxNQUFNLCtCQUErQixXQUFrQjs7VUFDN0MsTUFBTSxHQUFHLFdBQVcsR0FBRyxjQUFjOztVQUVyQyxVQUFVLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQztJQUVuQyxJQUFJLE1BQU0sR0FBRyxVQUFVLENBQUMsV0FBVyxFQUFFO1FBQ2pDLE9BQU8sQ0FBQyxDQUFDLENBQUM7S0FDYjtJQUdELEtBQUssSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztjQUNuQyxTQUFTLEdBQUcsYUFBYSxDQUFDLENBQUMsR0FBQyxDQUFDLENBQUM7UUFFcEMsSUFDSSxDQUFDLFNBQVM7WUFDVixNQUFNLEdBQUcsU0FBUyxDQUFDLFdBQVcsRUFDaEM7WUFDRSxPQUFPLENBQUMsQ0FBQztTQUNaO0tBQ0o7QUFDTCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgYmV0VG9rZW5GYWN0b3IgfSBmcm9tIFwiLi9jb25zdGFudHNcIjtcblxuXG5leHBvcnQgaW50ZXJmYWNlIElTdGFraW5nTGV2ZWxEYXRhXG57XG4gICAgbmFtZTpzdHJpbmc7XG4gICAgc3Rha2VQb2ludHM6bnVtYmVyO1xuICAgIGhvdXNlRWRnZVJlZHVjdGlvbjpudW1iZXI7XG4gICAgYmFjY2FyYXRIb3VzZUVkZ2U6bnVtYmVyO1xufVxuXG5cbmV4cG9ydCBjb25zdCBzdGFraW5nTGV2ZWxzOklTdGFraW5nTGV2ZWxEYXRhW10gPSBbXG4gICAge25hbWU6J3NocmltcCcsICAgICBzdGFrZVBvaW50czogMTAwMCwgICAgICBob3VzZUVkZ2VSZWR1Y3Rpb246IDAuMDUsIGJhY2NhcmF0SG91c2VFZGdlOiAxLjAzfSxcbiAgICB7bmFtZTonY3JhYicsICAgICAgIHN0YWtlUG9pbnRzOiAxMDAwMCwgICAgIGhvdXNlRWRnZVJlZHVjdGlvbjogMC4xMCwgYmFjY2FyYXRIb3VzZUVkZ2U6IDAuOTl9LFxuICAgIHtuYW1lOidvY3RvcHVzJywgICAgc3Rha2VQb2ludHM6IDI1MDAwLCAgICAgaG91c2VFZGdlUmVkdWN0aW9uOiAwLjEyLCBiYWNjYXJhdEhvdXNlRWRnZTogMC45OH0sXG4gICAge25hbWU6J3NxdWlkJywgICAgICBzdGFrZVBvaW50czogNTAwMDAsICAgICBob3VzZUVkZ2VSZWR1Y3Rpb246IDAuMTUsIGJhY2NhcmF0SG91c2VFZGdlOiAwLjk2fSxcbiAgICB7bmFtZTonZmlzaCcsICAgICAgIHN0YWtlUG9pbnRzOiAxMDAwMDAsICAgIGhvdXNlRWRnZVJlZHVjdGlvbjogMC4yMCwgYmFjY2FyYXRIb3VzZUVkZ2U6IDAuOTJ9LFxuICAgIHtuYW1lOidibG93ZmlzaCcsICAgc3Rha2VQb2ludHM6IDI1MDAwMCwgICAgaG91c2VFZGdlUmVkdWN0aW9uOiAwLjI1LCBiYWNjYXJhdEhvdXNlRWRnZTogMC44OX0sXG4gICAge25hbWU6J2RvbHBoaW4nLCAgICBzdGFrZVBvaW50czogNTAwMDAwLCAgICBob3VzZUVkZ2VSZWR1Y3Rpb246IDAuMzUsIGJhY2NhcmF0SG91c2VFZGdlOiAwLjgyfSxcbiAgICB7bmFtZTonc2hhcmsnLCAgICAgIHN0YWtlUG9pbnRzOiAxMDAwMDAwLCAgIGhvdXNlRWRnZVJlZHVjdGlvbjogMC41MCwgYmFjY2FyYXRIb3VzZUVkZ2U6IDAuNzF9LFxuICAgIHtuYW1lOid3aGFsZScsICAgICAgc3Rha2VQb2ludHM6IDI1MDAwMDAsICAgaG91c2VFZGdlUmVkdWN0aW9uOiAwLjc1LCBiYWNjYXJhdEhvdXNlRWRnZTogMC42MH0sXG4gICAge25hbWU6J2JsdWUtd2hhbGUnLCBzdGFrZVBvaW50czogNTAwMDAwMCwgICBob3VzZUVkZ2VSZWR1Y3Rpb246IDEsICAgIGJhY2NhcmF0SG91c2VFZGdlOiAwLjUwfSxcbl07XG5cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEhvdXNlRWRnZVJlZHVjdGlvbkFzRGVjaW1hbChzdGFrZVdlaWdodDpudW1iZXIpOm51bWJlciB7XG4gICAgY29uc3QgbGV2ZWxJbmRleCA9IGdldFN0YWtpbmdMZXZlbEluZGV4KHN0YWtlV2VpZ2h0KTtcblxuICAgIGNvbnN0IGxldmVsID0gc3Rha2luZ0xldmVsc1tsZXZlbEluZGV4XTtcblxuICAgIHJldHVybiBsZXZlbCA/XG4gICAgICAgICAgICBsZXZlbC5ob3VzZUVkZ2VSZWR1Y3Rpb24gLyAxMDAgOlxuICAgICAgICAgICAgMDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFN0YWtpbmdMZXZlbE5hbWUoc3Rha2VXZWlnaHQ6bnVtYmVyKTpzdHJpbmcge1xuICAgIGNvbnN0IGxldmVsSW5kZXggPSBnZXRTdGFraW5nTGV2ZWxJbmRleChzdGFrZVdlaWdodCk7XG5cbiAgICBjb25zdCBsZXZlbCA9IHN0YWtpbmdMZXZlbHNbbGV2ZWxJbmRleF07XG4gICAgXG4gICAgcmV0dXJuIGxldmVsID9cbiAgICAgICAgICAgIGxldmVsLm5hbWU6XG4gICAgICAgICAgICB1bmRlZmluZWQ7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRTdGFraW5nTGV2ZWxJbmRleChzdGFrZVdlaWdodDpudW1iZXIpOm51bWJlciB7XG4gICAgY29uc3QgcG9pbnRzID0gc3Rha2VXZWlnaHQgLyBiZXRUb2tlbkZhY3RvcjtcblxuICAgIGNvbnN0IGZpcnN0TGV2ZWwgPSBzdGFraW5nTGV2ZWxzWzBdO1xuXG4gICAgaWYgKHBvaW50cyA8IGZpcnN0TGV2ZWwuc3Rha2VQb2ludHMpIHtcbiAgICAgICAgcmV0dXJuIC0xO1xuICAgIH1cblxuXG4gICAgZm9yIChsZXQgaT0wOyBpIDwgc3Rha2luZ0xldmVscy5sZW5ndGg7IGkrKykge1xuICAgICAgICBjb25zdCBuZXh0TGV2ZWwgPSBzdGFraW5nTGV2ZWxzW2krMV07XG5cbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgIW5leHRMZXZlbCB8fFxuICAgICAgICAgICAgcG9pbnRzIDwgbmV4dExldmVsLnN0YWtlUG9pbnRzXG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuIGk7XG4gICAgICAgIH1cbiAgICB9XG59Il19