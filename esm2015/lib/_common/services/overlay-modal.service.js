/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/services/overlay-modal.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
export class OverlayModalService {
    constructor() {
        this.subject = new Subject();
    }
    /**
     * @param {?} message
     * @return {?}
     */
    open(message) {
        this.close();
        this.subject.next(message);
    }
    /**
     * @return {?}
     */
    close() {
        this.subject.next();
    }
    /**
     * @return {?}
     */
    getMessage() {
        return this.subject.asObservable();
    }
}
OverlayModalService.decorators = [
    { type: Injectable }
];
OverlayModalService.ctorParameters = () => [];
if (false) {
    /**
     * @type {?}
     * @private
     */
    OverlayModalService.prototype.subject;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3ZlcmxheS1tb2RhbC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL3NlcnZpY2VzL292ZXJsYXktbW9kYWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUczQyxNQUFNO0lBR0Y7UUFGUSxZQUFPLEdBQUcsSUFBSSxPQUFPLEVBQVUsQ0FBQztJQUd4QyxDQUFDOzs7OztJQUNELElBQUksQ0FBQyxPQUFlO1FBQ2hCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUViLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsVUFBVTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN2QyxDQUFDOzs7WUFsQkosVUFBVTs7Ozs7Ozs7SUFFUCxzQ0FBd0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBPdmVybGF5TW9kYWxTZXJ2aWNlIHtcbiAgICBwcml2YXRlIHN1YmplY3QgPSBuZXcgU3ViamVjdDxzdHJpbmc+KCk7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICB9XG4gICAgb3BlbihtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5jbG9zZSgpO1xuXG4gICAgICAgIHRoaXMuc3ViamVjdC5uZXh0KG1lc3NhZ2UpO1xuICAgIH1cblxuICAgIGNsb3NlKCl7XG4gICAgICAgIHRoaXMuc3ViamVjdC5uZXh0KCk7XG4gICAgfVxuXG4gICAgZ2V0TWVzc2FnZSgpOiBPYnNlcnZhYmxlPHN0cmluZz4ge1xuICAgICAgICByZXR1cm4gdGhpcy5zdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuICAgIH1cbn0iXX0=