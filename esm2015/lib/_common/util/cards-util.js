/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/util/cards-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @return {?}
 */
export function generateRandomCardId() {
    return getRandomIntegerBetween0AndX(51);
}
/**
 * @param {?} x
 * @return {?}
 */
export function getRandomIntegerBetween0AndX(x) {
    return Math.floor(Math.random() * x);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZHMtdXRpbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi91dGlsL2NhcmRzLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxNQUFNO0lBQ0YsT0FBTyw0QkFBNEIsQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUM1QyxDQUFDOzs7OztBQUVELE1BQU0sdUNBQXVDLENBQVE7SUFDakQsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUUsQ0FBQztBQUMzQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIGdlbmVyYXRlUmFuZG9tQ2FyZElkKCk6bnVtYmVyIHtcbiAgICByZXR1cm4gZ2V0UmFuZG9tSW50ZWdlckJldHdlZW4wQW5kWCg1MSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRSYW5kb21JbnRlZ2VyQmV0d2VlbjBBbmRYKHg6bnVtYmVyKTpudW1iZXIge1xuICAgIHJldHVybiBNYXRoLmZsb29yKCBNYXRoLnJhbmRvbSgpICogeCApO1xufSJdfQ==