/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/util/math-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} amount
 * @param {?} digits
 * @param {?=} precision
 * @return {?}
 */
export function toTotalDigits(amount, digits, precision = undefined) {
    return numStringToTotalDigits(String(amount), digits, precision);
}
/**
 * @param {?} amount
 * @param {?} digits
 * @param {?=} precision
 * @return {?}
 */
export function numStringToTotalDigits(amount, digits, precision = undefined) {
    /** @type {?} */
    const rounded = roundStringDownWithPrecision(amount, digits + 1);
    /** @type {?} */
    const numOfDigitsBeforeDecimal = getNumOfDigitsBeforeDecimal(rounded);
    /** @type {?} */
    let numOfDigitsAfterDecimal = digits - numOfDigitsBeforeDecimal;
    if (precision != undefined &&
        numOfDigitsAfterDecimal > precision) {
        numOfDigitsAfterDecimal = precision;
    }
    return numOfDigitsAfterDecimal > 0 ?
        roundStringDownWithPrecision(rounded, numOfDigitsAfterDecimal) :
        rounded.split('.')[0];
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
export function roundDownWithPrecision(amount, precision) {
    //amount = Number(amount);
    return roundStringDownWithPrecision(String(amount), precision);
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
export function roundDownWithoutTrailingZeros(amount, precision) {
    /** @type {?} */
    const valueWithAllDigits = roundStringDownWithPrecision(amount, precision);
    /** @type {?} */
    const parts = valueWithAllDigits.split('.');
    /** @type {?} */
    const digitsBeforeDecimal = parts[0];
    /** @type {?} */
    const digitsAfterDecimal = parts[1];
    /** @type {?} */
    let numOfDecimalDigitsToInclude = 0;
    for (var i = digitsAfterDecimal.length - 1; i > -1; i--) {
        /** @type {?} */
        const digit = digitsAfterDecimal[i];
        if (digit != '0') {
            numOfDecimalDigitsToInclude = i + 1;
            break;
        }
    }
    return numOfDecimalDigitsToInclude > 0 ?
        digitsBeforeDecimal + '.' + digitsAfterDecimal.substr(0, numOfDecimalDigitsToInclude) :
        digitsBeforeDecimal;
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
export function roundStringDownWithPrecision(amount, precision) {
    /** @type {?} */
    var parts = String(amount).split('.');
    /** @type {?} */
    var isWholeNumber = parts.length == 1;
    /** @type {?} */
    var rounded = Number(parts[0] + '.' +
        (isWholeNumber ?
            '0' :
            parts[1].substr(0, precision)));
    /** @type {?} */
    var result = rounded.toFixed(precision);
    return result;
}
/**
 * @param {?} number
 * @return {?}
 */
function getNumOfDigitsBeforeDecimal(number) {
    /** @type {?} */
    const parts = number.split('.');
    return parts[0].length;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0aC11dGlsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL3V0aWwvbWF0aC11dGlsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsTUFBTSx3QkFBd0IsTUFBYSxFQUFDLE1BQWEsRUFBQyxZQUFpQixTQUFTO0lBQ2hGLE9BQU8sc0JBQXNCLENBQ3pCLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFDZCxNQUFNLEVBQ04sU0FBUyxDQUNaLENBQUM7QUFDTixDQUFDOzs7Ozs7O0FBRUQsTUFBTSxpQ0FBaUMsTUFBYSxFQUFDLE1BQWEsRUFBQyxZQUFpQixTQUFTOztVQUNuRixPQUFPLEdBQUcsNEJBQTRCLENBQUMsTUFBTSxFQUFDLE1BQU0sR0FBQyxDQUFDLENBQUM7O1VBRXZELHdCQUF3QixHQUFHLDJCQUEyQixDQUFDLE9BQU8sQ0FBQzs7UUFFakUsdUJBQXVCLEdBQUcsTUFBTSxHQUFHLHdCQUF3QjtJQUUvRCxJQUNJLFNBQVMsSUFBSSxTQUFTO1FBQ3RCLHVCQUF1QixHQUFHLFNBQVMsRUFDckM7UUFDRSx1QkFBdUIsR0FBRyxTQUFTLENBQUM7S0FDdkM7SUFFRCxPQUFPLHVCQUF1QixHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzVCLDRCQUE0QixDQUN4QixPQUFPLEVBQ1AsdUJBQXVCLENBQzFCLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDbEMsQ0FBQzs7Ozs7O0FBR0QsTUFBTSxpQ0FBaUMsTUFBYSxFQUFDLFNBQWdCO0lBQ2pFLDBCQUEwQjtJQUUxQixPQUFPLDRCQUE0QixDQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBQyxTQUFTLENBQUUsQ0FBQztBQUNwRSxDQUFDOzs7Ozs7QUFFRCxNQUFNLHdDQUF3QyxNQUFhLEVBQUMsU0FBZ0I7O1VBQ2xFLGtCQUFrQixHQUFHLDRCQUE0QixDQUFDLE1BQU0sRUFBQyxTQUFTLENBQUM7O1VBRW5FLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOztVQUNyQyxtQkFBbUIsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDOztVQUM5QixrQkFBa0IsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDOztRQUUvQiwyQkFBMkIsR0FBVSxDQUFDO0lBRTFDLEtBQUssSUFBSSxDQUFDLEdBQUMsa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O2NBQzdDLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7UUFFbkMsSUFBSSxLQUFLLElBQUksR0FBRyxFQUFFO1lBQ2QsMkJBQTJCLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUVwQyxNQUFNO1NBQ1Q7S0FDSjtJQUVELE9BQU8sMkJBQTJCLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDaEMsbUJBQW1CLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDO1FBQ3RGLG1CQUFtQixDQUFDO0FBQ2hDLENBQUM7Ozs7OztBQUVELE1BQU0sdUNBQXVDLE1BQWEsRUFBQyxTQUFnQjs7UUFDbkUsS0FBSyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOztRQUVqQyxhQUFhLEdBQ2IsS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDOztRQUVqQixPQUFPLEdBQUcsTUFBTSxDQUNoQixLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRztRQUNkLENBQ0ksYUFBYSxDQUFDLENBQUM7WUFDWCxHQUFHLENBQUMsQ0FBQztZQUNMLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLFNBQVMsQ0FBQyxDQUNuQyxDQUNKOztRQUVHLE1BQU0sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQztJQUV2QyxPQUFPLE1BQU0sQ0FBQztBQUVsQixDQUFDOzs7OztBQUVELHFDQUFxQyxNQUFhOztVQUN4QyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7SUFFL0IsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO0FBQzNCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZnVuY3Rpb24gdG9Ub3RhbERpZ2l0cyhhbW91bnQ6bnVtYmVyLGRpZ2l0czpudW1iZXIscHJlY2lzaW9uOm51bWJlcj11bmRlZmluZWQpIHtcbiAgICByZXR1cm4gbnVtU3RyaW5nVG9Ub3RhbERpZ2l0cyhcbiAgICAgICAgU3RyaW5nKGFtb3VudCksXG4gICAgICAgIGRpZ2l0cyxcbiAgICAgICAgcHJlY2lzaW9uXG4gICAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIG51bVN0cmluZ1RvVG90YWxEaWdpdHMoYW1vdW50OnN0cmluZyxkaWdpdHM6bnVtYmVyLHByZWNpc2lvbjpudW1iZXI9dW5kZWZpbmVkKSB7XG4gICAgY29uc3Qgcm91bmRlZCA9IHJvdW5kU3RyaW5nRG93bldpdGhQcmVjaXNpb24oYW1vdW50LGRpZ2l0cysxKTsgIFxuICAgIFxuICAgIGNvbnN0IG51bU9mRGlnaXRzQmVmb3JlRGVjaW1hbCA9IGdldE51bU9mRGlnaXRzQmVmb3JlRGVjaW1hbChyb3VuZGVkKTtcblxuICAgIGxldCBudW1PZkRpZ2l0c0FmdGVyRGVjaW1hbCA9IGRpZ2l0cyAtIG51bU9mRGlnaXRzQmVmb3JlRGVjaW1hbDtcblxuICAgIGlmIChcbiAgICAgICAgcHJlY2lzaW9uICE9IHVuZGVmaW5lZCAmJlxuICAgICAgICBudW1PZkRpZ2l0c0FmdGVyRGVjaW1hbCA+IHByZWNpc2lvblxuICAgICkge1xuICAgICAgICBudW1PZkRpZ2l0c0FmdGVyRGVjaW1hbCA9IHByZWNpc2lvbjtcbiAgICB9XG5cbiAgICByZXR1cm4gbnVtT2ZEaWdpdHNBZnRlckRlY2ltYWwgPiAwID9cbiAgICAgICAgICAgIHJvdW5kU3RyaW5nRG93bldpdGhQcmVjaXNpb24oXG4gICAgICAgICAgICAgICAgcm91bmRlZCxcbiAgICAgICAgICAgICAgICBudW1PZkRpZ2l0c0FmdGVyRGVjaW1hbFxuICAgICAgICAgICAgKSA6XG4gICAgICAgICAgICByb3VuZGVkLnNwbGl0KCcuJylbMF07XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIHJvdW5kRG93bldpdGhQcmVjaXNpb24oYW1vdW50Om51bWJlcixwcmVjaXNpb246bnVtYmVyKSB7XG4gICAgLy9hbW91bnQgPSBOdW1iZXIoYW1vdW50KTtcblxuICAgIHJldHVybiByb3VuZFN0cmluZ0Rvd25XaXRoUHJlY2lzaW9uKCBTdHJpbmcoYW1vdW50KSxwcmVjaXNpb24gKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHJvdW5kRG93bldpdGhvdXRUcmFpbGluZ1plcm9zKGFtb3VudDpzdHJpbmcscHJlY2lzaW9uOm51bWJlcik6c3RyaW5nIHtcbiAgICBjb25zdCB2YWx1ZVdpdGhBbGxEaWdpdHMgPSByb3VuZFN0cmluZ0Rvd25XaXRoUHJlY2lzaW9uKGFtb3VudCxwcmVjaXNpb24pO1xuXG4gICAgY29uc3QgcGFydHMgPSB2YWx1ZVdpdGhBbGxEaWdpdHMuc3BsaXQoJy4nKTtcbiAgICBjb25zdCBkaWdpdHNCZWZvcmVEZWNpbWFsID0gcGFydHNbMF07XG4gICAgY29uc3QgZGlnaXRzQWZ0ZXJEZWNpbWFsID0gcGFydHNbMV07XG5cbiAgICBsZXQgbnVtT2ZEZWNpbWFsRGlnaXRzVG9JbmNsdWRlOm51bWJlciA9IDA7XG5cbiAgICBmb3IgKHZhciBpPWRpZ2l0c0FmdGVyRGVjaW1hbC5sZW5ndGggLSAxOyBpID4gLTE7IGktLSkge1xuICAgICAgICBjb25zdCBkaWdpdCA9IGRpZ2l0c0FmdGVyRGVjaW1hbFtpXTtcblxuICAgICAgICBpZiAoZGlnaXQgIT0gJzAnKSB7XG4gICAgICAgICAgICBudW1PZkRlY2ltYWxEaWdpdHNUb0luY2x1ZGUgPSBpICsgMTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgcmV0dXJuIG51bU9mRGVjaW1hbERpZ2l0c1RvSW5jbHVkZSA+IDAgP1xuICAgICAgICAgICAgZGlnaXRzQmVmb3JlRGVjaW1hbCArICcuJyArIGRpZ2l0c0FmdGVyRGVjaW1hbC5zdWJzdHIoMCxudW1PZkRlY2ltYWxEaWdpdHNUb0luY2x1ZGUpIDpcbiAgICAgICAgICAgIGRpZ2l0c0JlZm9yZURlY2ltYWw7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiByb3VuZFN0cmluZ0Rvd25XaXRoUHJlY2lzaW9uKGFtb3VudDpzdHJpbmcscHJlY2lzaW9uOm51bWJlcikge1xuICAgIHZhciBwYXJ0cyA9IFN0cmluZyhhbW91bnQpLnNwbGl0KCcuJyk7XG5cbiAgICB2YXIgaXNXaG9sZU51bWJlciA9XG4gICAgICAgIHBhcnRzLmxlbmd0aCA9PSAxO1xuXG4gICAgdmFyIHJvdW5kZWQgPSBOdW1iZXIoIFxuICAgICAgICBwYXJ0c1swXSArICcuJyArIFxuICAgICAgICAoXG4gICAgICAgICAgICBpc1dob2xlTnVtYmVyID9cbiAgICAgICAgICAgICAgICAnMCcgOlxuICAgICAgICAgICAgICAgIHBhcnRzWzFdLnN1YnN0cigwLHByZWNpc2lvbikgXG4gICAgICAgIClcbiAgICApO1xuICAgIFxuICAgIHZhciByZXN1bHQgPSByb3VuZGVkLnRvRml4ZWQocHJlY2lzaW9uKTtcbiAgICBcbiAgICByZXR1cm4gcmVzdWx0O1xuXG59XG5cbmZ1bmN0aW9uIGdldE51bU9mRGlnaXRzQmVmb3JlRGVjaW1hbChudW1iZXI6c3RyaW5nKSB7XG4gICAgY29uc3QgcGFydHMgPSBudW1iZXIuc3BsaXQoJy4nKTtcblxuICAgIHJldHVybiBwYXJ0c1swXS5sZW5ndGg7XG59Il19