/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-card/blackjack-card.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Input } from '@angular/core';
/*
@Component({
    selector: '[blackjack-card]',
    templateUrl: './blackjack-card.component.html',
    styleUrls: ['./blackjack-card.component.scss']
})
*/
export class BlackjackCardComponentBase {
    constructor() {
        this.direction = 'right';
        this.history = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    get suit() {
        return this.card ?
            this.card.suit :
            undefined;
    }
    /**
     * @return {?}
     */
    get rank() {
        return this.card ?
            this.card.card :
            undefined;
    }
    /**
     * @return {?}
     */
    get flipped() {
        return this.card ?
            this.card.flipped :
            undefined;
    }
    /**
     * @return {?}
     */
    get dealt() {
        return this.card ?
            this.card.dealt :
            undefined;
    }
}
BlackjackCardComponentBase.propDecorators = {
    card: [{ type: Input }],
    direction: [{ type: Input }],
    history: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlackjackCardComponentBase.prototype.card;
    /** @type {?} */
    BlackjackCardComponentBase.prototype.direction;
    /** @type {?} */
    BlackjackCardComponentBase.prototype.history;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWNhcmQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svY29tcG9uZW50cy9ibGFja2phY2stY2FyZC9ibGFja2phY2stY2FyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7OztBQVd0QyxNQUFNO0lBV0Y7UUFMQSxjQUFTLEdBQVcsT0FBTyxDQUFDO1FBRzVCLFlBQU8sR0FBRyxLQUFLLENBQUM7SUFFQSxDQUFDOzs7O0lBRWpCLFFBQVE7SUFDUixDQUFDOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ0osT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDVixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hCLFNBQVMsQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ0osT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDVixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hCLFNBQVMsQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsSUFBSSxPQUFPO1FBQ1AsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDVixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25CLFNBQVMsQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsSUFBSSxLQUFLO1FBQ0wsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDVixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pCLFNBQVMsQ0FBQztJQUN0QixDQUFDOzs7bUJBcENBLEtBQUs7d0JBR0wsS0FBSztzQkFHTCxLQUFLOzs7O0lBTk4sMENBQ3FCOztJQUVyQiwrQ0FDNEI7O0lBRTVCLDZDQUNnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IElDYXJkQ29udHJvbGxlciB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vZ2FtZS9pbnRlcmZhY2VzJztcblxuLypcbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnW2JsYWNramFjay1jYXJkXScsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2JsYWNramFjay1jYXJkLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9ibGFja2phY2stY2FyZC5jb21wb25lbnQuc2NzcyddXG59KVxuKi9cbmV4cG9ydCBjbGFzcyBCbGFja2phY2tDYXJkQ29tcG9uZW50QmFzZVxue1xuICAgIEBJbnB1dCgpXG4gICAgY2FyZDpJQ2FyZENvbnRyb2xsZXI7XG5cbiAgICBASW5wdXQoKVxuICAgIGRpcmVjdGlvbjogc3RyaW5nID0gJ3JpZ2h0JztcblxuICAgIEBJbnB1dCgpXG4gICAgaGlzdG9yeSA9IGZhbHNlO1xuXG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgIH1cblxuICAgIGdldCBzdWl0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jYXJkID9cbiAgICAgICAgICAgICAgICB0aGlzLmNhcmQuc3VpdCA6XG4gICAgICAgICAgICAgICAgdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIGdldCByYW5rKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jYXJkID9cbiAgICAgICAgICAgICAgICB0aGlzLmNhcmQuY2FyZCA6XG4gICAgICAgICAgICAgICAgdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIGdldCBmbGlwcGVkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jYXJkID9cbiAgICAgICAgICAgICAgICB0aGlzLmNhcmQuZmxpcHBlZCA6XG4gICAgICAgICAgICAgICAgdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIGdldCBkZWFsdCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZCA/XG4gICAgICAgICAgICAgICAgdGhpcy5jYXJkLmRlYWx0IDpcbiAgICAgICAgICAgICAgICB1bmRlZmluZWQ7XG4gICAgfVxuXG59XG4iXX0=