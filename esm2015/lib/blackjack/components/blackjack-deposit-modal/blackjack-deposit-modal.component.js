/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-deposit-modal/blackjack-deposit-modal.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
@Component({
    selector: "blackjack-deposit-modal",
    templateUrl: "./blackjack-deposit-modal.component.html",
    styleUrls: ["./blackjack-deposit-modal.component.scss"]
})
*/
export class BlackjackDepositModalBase {
    /**
     * @param {?} app
     * @param {?} modal
     */
    constructor(app, modal) {
        this.app = app;
        this.modal = modal;
    }
    /**
     * @return {?}
     */
    get translation() {
        return this.app.settings.translation;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackDepositModalBase.prototype.app;
    /** @type {?} */
    BlackjackDepositModalBase.prototype.modal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWRlcG9zaXQtbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svY29tcG9uZW50cy9ibGFja2phY2stZGVwb3NpdC1tb2RhbC9ibGFja2phY2stZGVwb3NpdC1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBYUEsTUFBTTs7Ozs7SUFFSixZQUNVLEdBQW1CLEVBQ2xCLEtBQXlCO1FBRDFCLFFBQUcsR0FBSCxHQUFHLENBQWdCO1FBQ2xCLFVBQUssR0FBTCxLQUFLLENBQW9CO0lBRXBDLENBQUM7Ozs7SUFFRCxJQUFJLFdBQVc7UUFDYixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztJQUN2QyxDQUFDO0NBQ0Y7Ozs7OztJQVJHLHdDQUEyQjs7SUFDM0IsMENBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuaW1wb3J0IHsgSU1haW5BcHBDb250ZXh0IH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IE92ZXJsYXlNb2RhbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9fY29tbW9uL3NlcnZpY2VzL292ZXJsYXktbW9kYWwuc2VydmljZSc7XG5pbXBvcnQgeyBJVHJhbnNsYXRpb25CYXNlIH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi90cmFuc2xhdGlvbnMvaW50ZXJmYWNlcyc7XG5cbi8qXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJibGFja2phY2stZGVwb3NpdC1tb2RhbFwiLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vYmxhY2tqYWNrLWRlcG9zaXQtbW9kYWwuY29tcG9uZW50Lmh0bWxcIixcbiAgICBzdHlsZVVybHM6IFtcIi4vYmxhY2tqYWNrLWRlcG9zaXQtbW9kYWwuY29tcG9uZW50LnNjc3NcIl1cbn0pXG4qL1xuZXhwb3J0IGNsYXNzIEJsYWNramFja0RlcG9zaXRNb2RhbEJhc2VcbntcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBhcHA6SU1haW5BcHBDb250ZXh0LFxuICAgIHJlYWRvbmx5IG1vZGFsOk92ZXJsYXlNb2RhbFNlcnZpY2VcbiAgKSB7XG4gIH1cblxuICBnZXQgdHJhbnNsYXRpb24oKTpJVHJhbnNsYXRpb25CYXNlIHtcbiAgICByZXR1cm4gdGhpcy5hcHAuc2V0dGluZ3MudHJhbnNsYXRpb247XG4gIH1cbn0iXX0=