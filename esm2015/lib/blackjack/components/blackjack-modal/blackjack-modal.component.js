/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-modal/blackjack-modal.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { sleep } from 'earnbet-common';
export class BlackjackModalComponent {
    constructor() {
        this._isClosed = false;
        this.result = undefined;
    }
    /**
     * @param {?} result
     * @return {?}
     */
    showResult(result) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.result = result;
            this._isClosed = false;
            yield sleep(2 * 1000);
            this.close();
        });
    }
    /**
     * @return {?}
     */
    close() {
        this._isClosed = true;
    }
    /**
     * @return {?}
     */
    get isOpen() {
        return this.result != undefined &&
            !this._isClosed;
    }
    /**
     * @return {?}
     */
    get isWin() {
        return this.result && this.result.isWin;
    }
    /**
     * @return {?}
     */
    get isPush() {
        return this.result && this.result.isPush;
    }
    /**
     * @return {?}
     */
    get isBlackjack() {
        return this.result && this.result.isBlackjack;
    }
    /**
     * @return {?}
     */
    get isLoss() {
        return this.result && this.result.isLoss;
    }
    /**
     * @return {?}
     */
    get isBusted() {
        return this.result && this.result.isBusted;
    }
    /**
     * @return {?}
     */
    get isInsuranceSuccess() {
        return this.result && this.result.isInsuranceSuccess;
    }
    /**
     * @return {?}
     */
    get amountWon() {
        return this.result && this.result.amountWon;
    }
}
BlackjackModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-blackjack-modal',
                template: "<div *ngIf=\"isOpen\">\n  <div class=\"bj-modal\" [ngClass]=\"{\n                'red': isBusted || isLoss,\n                'green': isWin || isBlackjack,\n                'yellow': isPush,\n                'blue': isInsuranceSuccess\n              }\">\n  <button class=\"modal-close\" (click)='close()'>&times;</button>\n\n    <div class=\"align-items-center d-flex flex-column h-100 justify-content-center p-3\">\n      <div class=\"win\" *ngIf=\"isWin && !isBlackjack\">\n        <img class=\"mb-2 small-circle\" src=\"assets/img/blackjack/modal/win.png\" alt=\"win\">\n        <h1 class=\"bj-modal-title\">WIN!</h1>\n        <div class=\"points\">+ {{amountWon | number:'1.0-8'}}</div>\n      </div>\n\n      <div class=\"push\" *ngIf=\"isPush\">\n        <img class=\"mb-2 small-circle\" src=\"assets/img/blackjack/modal/push.png\" alt=\"push\">\n        <h1 class=\"bj-modal-title\">PUSH</h1>\n      </div>\n\n      <div class=\"blackjack\" *ngIf=\"isBlackjack\">\n        <img class=\"bj-image\" src=\"assets/img/blackjack/modal/blackjack.png\" alt=\"blackjack\">\n        <div class=\"points\">+ {{amountWon | number:'1.0-8'}}</div>\n      </div>\n\n      <div class=\"busted\" *ngIf=\"isBusted\">\n        <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/busted.png\" alt=\"busted\">\n        <h1 class=\"bj-modal-title\">BUSTED!</h1>\n      </div>\n\n     <div class=\"busted\" *ngIf=\"isLoss\">\n          <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/busted.png\" alt=\"busted\">\n          <h1 class=\"bj-modal-title\">LOSS</h1>\n      </div> \n\n\n      <!-- TODO: confirm state name -->\n      <div class=\"insurance\" *ngIf=\"isInsuranceSuccess\">\n        <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/success.png\" alt=\"insurance success\">\n        <h1 class=\"bj-modal-title\">Insurance Success</h1>\n      </div>\n    </div>\n  </div>\n\n</div>\n  ",
                styles: [".bj-modal{max-width:305px;height:100%;max-height:200px;width:100%;background-color:#16334a;opacity:.95;border:2px solid transparent;border-radius:8px;letter-spacing:0;font-size:34px;color:#fff;text-align:center;position:absolute;top:50%;left:50%;bottom:50%;right:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);z-index:9999}.bj-modal-wrapper{height:100%;width:100%;position:relative}.bj-image{max-width:175px}.bj-modal.green{border-color:#02f292}.bj-modal.green .bj-modal-title{color:#02f292;margin-bottom:0}.bj-modal.green .points{color:#02f292;font-size:20px;font-weight:900}.bj-modal.blue{border-color:#67ddf2}.bj-modal.blue .bj-modal-title{color:#67ddf2}.bj-modal.yellow{border-color:#ffec77}.bj-modal.yellow .bj-modal-title{color:#ffec77}.bj-modal.red{border-color:#f10260}.bj-modal.red .bj-modal-title{color:#f10260}.small-circle{max-width:66px}.modal-close{font-size:25px;background-color:transparent;position:absolute;top:0;right:0;border:none;color:#fff}"]
            }] }
];
BlackjackModalComponent.ctorParameters = () => [];
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackModalComponent.prototype._isClosed;
    /**
     * @type {?}
     * @private
     */
    BlackjackModalComponent.prototype.result;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2NvbXBvbmVudHMvYmxhY2tqYWNrLW1vZGFsL2JsYWNramFjay1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUxQyxPQUFPLEVBQUMsS0FBSyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFVckMsTUFBTTtJQUlGO1FBSFEsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixXQUFNLEdBQW9CLFNBQVMsQ0FBQztJQUc1QyxDQUFDOzs7OztJQUVLLFVBQVUsQ0FBQyxNQUF1Qjs7WUFDcEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFFdkIsTUFBTSxLQUFLLENBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQixDQUFDO0tBQUE7Ozs7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7SUFDMUIsQ0FBQzs7OztJQUdELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxTQUFTO1lBQ3ZCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsSUFBSSxLQUFLO1FBQ0wsT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQzVDLENBQUM7Ozs7SUFFRCxJQUFJLE1BQU07UUFDTixPQUFPLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDN0MsQ0FBQzs7OztJQUVELElBQUksV0FBVztRQUNYLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztJQUNsRCxDQUFDOzs7O0lBRUQsSUFBSSxNQUFNO1FBQ04sT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQzdDLENBQUM7Ozs7SUFFRCxJQUFJLFFBQVE7UUFDUixPQUFPLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDL0MsQ0FBQzs7OztJQUVELElBQUksa0JBQWtCO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDO0lBQ3pELENBQUM7Ozs7SUFFRCxJQUFJLFNBQVM7UUFDVCxPQUFPLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7SUFDaEQsQ0FBQzs7O1lBeERKLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUscUJBQXFCO2dCQUMvQixxNURBQStDOzthQUVoRDs7Ozs7Ozs7SUFFRyw0Q0FBMEI7Ozs7O0lBQzFCLHlDQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQge3NsZWVwfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5cbmltcG9ydCB7IElCbGFja2phY2tSZXN1bHQgfSBmcm9tICcuLi9tYWluL2dhbWUvaGFuZC1yZXN1bHQnO1xuXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1ibGFja2phY2stbW9kYWwnLFxuICB0ZW1wbGF0ZVVybDogJy4vYmxhY2tqYWNrLW1vZGFsLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vYmxhY2tqYWNrLW1vZGFsLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrTW9kYWxDb21wb25lbnQgIHtcbiAgICBwcml2YXRlIF9pc0Nsb3NlZCA9IGZhbHNlO1xuICAgIHByaXZhdGUgcmVzdWx0OklCbGFja2phY2tSZXN1bHQgPSB1bmRlZmluZWQ7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICB9XG5cbiAgICBhc3luYyBzaG93UmVzdWx0KHJlc3VsdDpJQmxhY2tqYWNrUmVzdWx0KSB7XG4gICAgICAgIHRoaXMucmVzdWx0ID0gcmVzdWx0O1xuICAgICAgICB0aGlzLl9pc0Nsb3NlZCA9IGZhbHNlO1xuXG4gICAgICAgIGF3YWl0IHNsZWVwKDIqMTAwMCk7XG4gICAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICB9XG5cbiAgICBjbG9zZSgpIHtcbiAgICAgICAgdGhpcy5faXNDbG9zZWQgPSB0cnVlO1xuICAgIH1cblxuXG4gICAgZ2V0IGlzT3BlbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzdWx0ICE9IHVuZGVmaW5lZCAmJlxuICAgICAgICAgICAgICAgICF0aGlzLl9pc0Nsb3NlZDtcbiAgICB9XG5cbiAgICBnZXQgaXNXaW4oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdCAmJiB0aGlzLnJlc3VsdC5pc1dpbjtcbiAgICB9XG5cbiAgICBnZXQgaXNQdXNoKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHQgJiYgdGhpcy5yZXN1bHQuaXNQdXNoO1xuICAgIH1cblxuICAgIGdldCBpc0JsYWNramFjaygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzdWx0ICYmIHRoaXMucmVzdWx0LmlzQmxhY2tqYWNrO1xuICAgIH1cblxuICAgIGdldCBpc0xvc3MoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdCAmJiB0aGlzLnJlc3VsdC5pc0xvc3M7XG4gICAgfVxuXG4gICAgZ2V0IGlzQnVzdGVkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHQgJiYgdGhpcy5yZXN1bHQuaXNCdXN0ZWQ7XG4gICAgfVxuXG4gICAgZ2V0IGlzSW5zdXJhbmNlU3VjY2VzcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzdWx0ICYmIHRoaXMucmVzdWx0LmlzSW5zdXJhbmNlU3VjY2VzcztcbiAgICB9XG5cbiAgICBnZXQgYW1vdW50V29uKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHQgJiYgdGhpcy5yZXN1bHQuYW1vdW50V29uO1xuICAgIH1cbn1cbiJdfQ==