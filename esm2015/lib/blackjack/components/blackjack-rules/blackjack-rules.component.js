/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-rules/blackjack-rules.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
@Component({
    selector: "blackjack-rules",
    templateUrl: "./blackjack-rules.component.html",
    styleUrls: ["./blackjack-rules.component.scss"]
})
*/
export class BlackjackRulesComponentBase {
    /**
     * @param {?} app
     * @param {?} modal
     */
    constructor(app, modal) {
        this.app = app;
        this.modal = modal;
    }
    /**
     * @return {?}
     */
    get translation() {
        return this.app.settings.translation;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackRulesComponentBase.prototype.app;
    /** @type {?} */
    BlackjackRulesComponentBase.prototype.modal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLXJ1bGVzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2NvbXBvbmVudHMvYmxhY2tqYWNrLXJ1bGVzL2JsYWNramFjay1ydWxlcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBYUEsTUFBTTs7Ozs7SUFFSixZQUNVLEdBQW1CLEVBQ2xCLEtBQXlCO1FBRDFCLFFBQUcsR0FBSCxHQUFHLENBQWdCO1FBQ2xCLFVBQUssR0FBTCxLQUFLLENBQW9CO0lBRXBDLENBQUM7Ozs7SUFFRCxJQUFJLFdBQVc7UUFDYixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztJQUN2QyxDQUFDO0NBQ0Y7Ozs7OztJQVJHLDBDQUEyQjs7SUFDM0IsNENBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuaW1wb3J0IHsgSU1haW5BcHBDb250ZXh0IH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IE92ZXJsYXlNb2RhbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9fY29tbW9uL3NlcnZpY2VzL292ZXJsYXktbW9kYWwuc2VydmljZSc7XG5pbXBvcnQgeyBJVHJhbnNsYXRpb25CYXNlIH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi90cmFuc2xhdGlvbnMvaW50ZXJmYWNlcyc7XG5cbi8qXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJibGFja2phY2stcnVsZXNcIixcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2JsYWNramFjay1ydWxlcy5jb21wb25lbnQuaHRtbFwiLFxuICAgIHN0eWxlVXJsczogW1wiLi9ibGFja2phY2stcnVsZXMuY29tcG9uZW50LnNjc3NcIl1cbn0pXG4qL1xuZXhwb3J0IGNsYXNzIEJsYWNramFja1J1bGVzQ29tcG9uZW50QmFzZVxue1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGFwcDpJTWFpbkFwcENvbnRleHQsXG4gICAgcmVhZG9ubHkgbW9kYWw6T3ZlcmxheU1vZGFsU2VydmljZVxuICApIHtcbiAgfVxuXG4gIGdldCB0cmFuc2xhdGlvbigpOklUcmFuc2xhdGlvbkJhc2Uge1xuICAgIHJldHVybiB0aGlzLmFwcC5zZXR0aW5ncy50cmFuc2xhdGlvbjtcbiAgfVxufVxuIl19