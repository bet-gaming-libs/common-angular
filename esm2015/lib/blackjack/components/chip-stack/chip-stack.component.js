/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/chip-stack/chip-stack.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Input } from '@angular/core';
/*
@Component({
    selector: 'bj-chip-stack',
    templateUrl: './chip-stack.component.html',
    styleUrls: ['./chip-stack.component.scss']
})
*/
export class BlackJackChipStackComponentBase {
    /**
     * @param {?} app
     */
    constructor(app) {
        this.app = app;
        this.isMax = false;
        this.amount = 0;
        this.chipstack = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.makeChipStack();
    }
    /**
     * @return {?}
     */
    makeChipStack() {
        /** @type {?} */
        let chipstack = [];
        // is the amount the max for this bet type
        if (this.isMax) {
            chipstack.push(this.chips.length);
        }
        else {
            /** @type {?} */
            const factor = 1 / this.chips[0];
            /** @type {?} */
            let n = Math.round(this.amount * factor);
            for (let i = this.chips.length - 1; i >= 0; i--) {
                if ((this.chips[i] * factor) <= n) {
                    /** @type {?} */
                    let r = Math.floor(n / (this.chips[i] * factor));
                    n -= this.chips[i] * factor * r;
                    for (var j = 0; j < r; j++) {
                        chipstack.push(i);
                    }
                }
            }
        }
        this.chipstack = chipstack;
    }
    /**
     * @return {?}
     */
    get chips() {
        return this.settings.selectedToken.chips;
    }
    /**
     * @return {?}
     */
    get settings() {
        return this.app.settings;
    }
}
BlackJackChipStackComponentBase.propDecorators = {
    isMax: [{ type: Input }],
    amount: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlackJackChipStackComponentBase.prototype.isMax;
    /** @type {?} */
    BlackJackChipStackComponentBase.prototype.amount;
    /** @type {?} */
    BlackJackChipStackComponentBase.prototype.chipstack;
    /**
     * @type {?}
     * @private
     */
    BlackJackChipStackComponentBase.prototype.app;
}
/**
 * @record
 */
export function IChipsInfo() { }
if (false) {
    /** @type {?} */
    IChipsInfo.prototype.isMax;
    /** @type {?} */
    IChipsInfo.prototype.amount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpcC1zdGFjay5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL2JsYWNramFjay9jb21wb25lbnRzL2NoaXAtc3RhY2svY2hpcC1zdGFjay5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQWEsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQzs7Ozs7Ozs7QUFZcEUsTUFBTTs7OztJQVVGLFlBQW9CLEdBQW1CO1FBQW5CLFFBQUcsR0FBSCxHQUFHLENBQWdCO1FBUHZDLFVBQUssR0FBRyxLQUFLLENBQUM7UUFFZCxXQUFNLEdBQUcsQ0FBQyxDQUFDO1FBR1gsY0FBUyxHQUFHLEVBQUUsQ0FBQztJQUdmLENBQUM7Ozs7SUFFRCxRQUFRO0lBQ1IsQ0FBQzs7OztJQUNELFdBQVc7UUFDUCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELGFBQWE7O1lBQ0wsU0FBUyxHQUFHLEVBQUU7UUFFbEIsMENBQTBDO1FBQzFDLElBQUcsSUFBSSxDQUFDLEtBQUssRUFBQztZQUNWLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNyQzthQUFNOztrQkFDRyxNQUFNLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOztnQkFFNUIsQ0FBQyxHQUFVLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBQyxNQUFNLENBQUM7WUFDN0MsS0FBSSxJQUFJLENBQUMsR0FBUSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBQztnQkFDNUMsSUFBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUMsTUFBTSxDQUFDLElBQUUsQ0FBQyxFQUFDOzt3QkFDckIsQ0FBQyxHQUFVLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDbkQsQ0FBQyxJQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQztvQkFDMUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTt3QkFDeEIsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDckI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7SUFDL0IsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDO0lBQzdDLENBQUM7Ozs7SUFFRCxJQUFJLFFBQVE7UUFDUixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDO0lBQzdCLENBQUM7OztvQkE5Q0EsS0FBSztxQkFFTCxLQUFLOzs7O0lBRk4sZ0RBQ2M7O0lBQ2QsaURBQ1c7O0lBR1gsb0RBQWU7Ozs7O0lBRUgsOENBQTJCOzs7OztBQTBDM0MsZ0NBSUM7OztJQUZHLDJCQUFjOztJQUNkLDRCQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgSU1haW5BcHBDb250ZXh0IH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElBcHBTZXR0aW5ncyB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vbW9kZWxzL2ludGVyZmFjZXMnO1xuXG4vKlxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdiai1jaGlwLXN0YWNrJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2hpcC1zdGFjay5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY2hpcC1zdGFjay5jb21wb25lbnQuc2NzcyddXG59KVxuKi9cbmV4cG9ydCBjbGFzcyBCbGFja0phY2tDaGlwU3RhY2tDb21wb25lbnRCYXNlIGltcGxlbWVudHMgT25DaGFuZ2VzLCBPbkluaXRcbntcbiAgICBASW5wdXQoKVxuICAgIGlzTWF4ID0gZmFsc2U7XG4gICAgQElucHV0KClcbiAgICBhbW91bnQgPSAwO1xuXG5cbiAgICBjaGlwc3RhY2sgPSBbXTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBwOklNYWluQXBwQ29udGV4dCkge1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgIH1cbiAgICBuZ09uQ2hhbmdlcygpIHtcbiAgICAgICAgdGhpcy5tYWtlQ2hpcFN0YWNrKCk7XG4gICAgfVxuXG4gICAgbWFrZUNoaXBTdGFjaygpe1xuICAgICAgICBsZXQgY2hpcHN0YWNrID0gW107XG5cbiAgICAgICAgLy8gaXMgdGhlIGFtb3VudCB0aGUgbWF4IGZvciB0aGlzIGJldCB0eXBlXG4gICAgICAgIGlmKHRoaXMuaXNNYXgpe1xuICAgICAgICAgICAgY2hpcHN0YWNrLnB1c2godGhpcy5jaGlwcy5sZW5ndGgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc3QgZmFjdG9yID0gMSAvIHRoaXMuY2hpcHNbMF07XG5cbiAgICAgICAgICAgIGxldCBuOm51bWJlciA9IE1hdGgucm91bmQodGhpcy5hbW91bnQqZmFjdG9yKTtcbiAgICAgICAgICAgIGZvcihsZXQgaTpudW1iZXI9dGhpcy5jaGlwcy5sZW5ndGgtMTsgaT49MDsgaS0tKXtcbiAgICAgICAgICAgICAgICBpZigodGhpcy5jaGlwc1tpXSpmYWN0b3IpPD1uKXtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHI6bnVtYmVyID0gTWF0aC5mbG9vcihuLyh0aGlzLmNoaXBzW2ldKmZhY3RvcikpO1xuICAgICAgICAgICAgICAgICAgICBuLT10aGlzLmNoaXBzW2ldKmZhY3RvcipyO1xuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IHI7IGorKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2hpcHN0YWNrLnB1c2goaSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jaGlwc3RhY2sgPSBjaGlwc3RhY2s7XG4gICAgfVxuXG4gICAgZ2V0IGNoaXBzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5zZWxlY3RlZFRva2VuLmNoaXBzO1xuICAgIH1cblxuICAgIGdldCBzZXR0aW5ncygpOklBcHBTZXR0aW5ncyB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC5zZXR0aW5ncztcbiAgICB9XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQ2hpcHNJbmZvXG57XG4gICAgaXNNYXg6Ym9vbGVhbjtcbiAgICBhbW91bnQ6bnVtYmVyO1xufVxuIl19