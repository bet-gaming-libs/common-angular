/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IBlackjackModalComponent() { }
if (false) {
    /**
     * @param {?} results
     * @return {?}
     */
    IBlackjackModalComponent.prototype.showResult = function (results) { };
    /**
     * @return {?}
     */
    IBlackjackModalComponent.prototype.close = function () { };
}
/**
 * @record
 */
export function IBlackjackComponent() { }
if (false) {
    /** @type {?} */
    IBlackjackComponent.prototype.game;
    /** @type {?} */
    IBlackjackComponent.prototype.gameResultModal;
    /** @type {?} */
    IBlackjackComponent.prototype.alert;
    /** @type {?} */
    IBlackjackComponent.prototype.wallet;
    /**
     * @return {?}
     */
    IBlackjackComponent.prototype.getMaxBet = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2NvbXBvbmVudHMvaW50ZXJmYWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQU9BLDhDQUlDOzs7Ozs7SUFGRyx1RUFBbUQ7Ozs7SUFDbkQsMkRBQWE7Ozs7O0FBR2pCLHlDQVFDOzs7SUFORyxtQ0FBc0M7O0lBQ3RDLDhDQUFrRDs7SUFDbEQsb0NBQTZCOztJQUM3QixxQ0FBbUM7Ozs7SUFFbkMsMERBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSUJsYWNramFja1Jlc3VsdCB9IGZyb20gJy4vbWFpbi9nYW1lL2hhbmQtcmVzdWx0JztcbmltcG9ydCB7IEJsYWNramFja0dhbWVDb250cm9sbGVyIH0gZnJvbSAnLi9tYWluL2dhbWUvYmxhY2tqYWNrLWNvbnRyb2xsZXInO1xuaW1wb3J0IHsgSUdhbWVDb21wb25lbnQgfSBmcm9tICcuLi8uLi9fY29tbW9uL2Jhc2UvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQWxlcnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElXYWxsZXRTZXJ2aWNlQmFzZSB9IGZyb20gJy4uLy4uL19jb21tb24vd2FsbGV0cy9pbnRlcmZhY2VzJztcblxuXG5leHBvcnQgaW50ZXJmYWNlIElCbGFja2phY2tNb2RhbENvbXBvbmVudFxue1xuICAgIHNob3dSZXN1bHQocmVzdWx0czpJQmxhY2tqYWNrUmVzdWx0KTpQcm9taXNlPHZvaWQ+O1xuICAgIGNsb3NlKCk6dm9pZDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQmxhY2tqYWNrQ29tcG9uZW50IGV4dGVuZHMgSUdhbWVDb21wb25lbnRcbntcbiAgICByZWFkb25seSBnYW1lOkJsYWNramFja0dhbWVDb250cm9sbGVyO1xuICAgIHJlYWRvbmx5IGdhbWVSZXN1bHRNb2RhbDpJQmxhY2tqYWNrTW9kYWxDb21wb25lbnQ7XG4gICAgcmVhZG9ubHkgYWxlcnQ6SUFsZXJ0U2VydmljZTtcbiAgICByZWFkb25seSB3YWxsZXQ6SVdhbGxldFNlcnZpY2VCYXNlO1xuXG4gICAgZ2V0TWF4QmV0KCk6UHJvbWlzZTxudW1iZXI+O1xufVxuIl19