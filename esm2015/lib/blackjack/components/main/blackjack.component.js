/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/blackjack.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ViewChild } from '@angular/core';
import { sleep } from 'earnbet-common';
import { BlackjackGameController } from './game/blackjack-controller';
import { BlackjackModalComponent } from '../blackjack-modal/blackjack-modal.component';
import { LocalBlackjackServer } from '../../server/local-server';
import { RemoteBlackjackServerBridge } from '../../server/remote-server';
import { GameComponentBase } from '../../../_common/base/game-component-base';
/*
@Component({
    selector: 'app-blackjack',
    templateUrl: './blackjack.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./blackjack.component.scss']
})
*/
export class BlackjackComponentBase extends GameComponentBase {
    /**
     * @param {?} app
     * @param {?} renderer
     * @param {?} modal
     * @param {?} googleAnalytics
     * @param {?} soundPlayer
     * @param {?} accountNames
     */
    constructor(app, renderer, modal, googleAnalytics, soundPlayer, accountNames) {
        super(app, googleAnalytics, soundPlayer);
        this.renderer = renderer;
        this.modal = modal;
        // *** For Testing: ***
        //this.useLocalServer();
        this.useRemoteServer(accountNames);
        this.game = new BlackjackGameController(this, this.server, soundPlayer);
        this.setListeners();
    }
    /**
     * @private
     * @param {?} soundPlayer
     * @return {?}
     */
    useLocalServer(soundPlayer) {
        this.server = new LocalBlackjackServer(this, soundPlayer);
    }
    /**
     * @private
     * @param {?} acountNames
     * @return {?}
     */
    useRemoteServer(acountNames) {
        this.server = new RemoteBlackjackServerBridge(this, acountNames, this.app);
    }
    /**
     * @private
     * @return {?}
     */
    setListeners() {
        this.app.state.setListener(this);
        this.app.blackjack.client.setListener(this);
        this.app.state.bets.setListener(this);
    }
    /**
     * @return {?}
     */
    onBlackjackClientConnected() {
        this.resumeGame();
    }
    /**
     * @return {?}
     */
    onAuthenticated() {
        this.resumeGame();
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.renderer.addClass(document.body, 'blackjack-bg');
        this.resumeGame();
        // *** blackjack deposit modal ***
        //this.modal.open('blackjackGameNoticeModal');
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.renderer.removeClass(document.body, 'blackjack-bg');
    }
    /**
     * @return {?}
     */
    debug() {
        console.log(this.game);
    }
    /**
     * @private
     * @return {?}
     */
    resumeGame() {
        if (this.app.blackjack.client.isConnected) {
            this.server.resumeExistingGame();
        }
    }
    /**
     * @return {?}
     */
    onBettingCurrencyChanged() {
        this.clearTable();
    }
    /**
     * @return {?}
     */
    onChipSelected() {
        this.addChip();
    }
    /**
     * @return {?}
     */
    halfOfBetAmount() {
        if (this.isBetPlaced) {
            return;
        }
        /** @type {?} */
        const minimumBet = Number(this.selectedToken.minimumBetAmount);
        if (this.game.bet.amount == minimumBet) {
            return;
        }
        this.game.bet.half();
        if (this.game.bet.amount < minimumBet) {
            this.game.bet.amount = minimumBet;
        }
    }
    /**
     * @return {?}
     */
    doubleBetAmount() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.isBetPlaced) {
                return;
            }
            /** @type {?} */
            const maxBet = yield this.getMaxBet();
            if (this.game.bet.amount == maxBet) {
                return;
            }
            this.game.bet.double();
            if (this.game.bet.amount > maxBet) {
                this.game.bet.setToMax(maxBet);
            }
        });
    }
    /**
     * @return {?}
     */
    addChip() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.isBetPlaced) {
                return;
            }
            if (!this.isMaxChipSelected) {
                /** @type {?} */
                const amount = this.chips[this.selectedChip];
                this.game.bet.add(amount);
            }
            else {
                this.game.bet.setToMax(yield this.getMaxBet());
            }
        });
    }
    /**
     * @return {?}
     */
    removeChip() {
        if (this.isBetPlaced) {
            return;
        }
        if (!this.isMaxChipSelected) {
            /** @type {?} */
            const amount = this.chips[this.selectedChip];
            this.game.bet.subtract(amount);
        }
        else {
            this.game.bet.reset();
        }
    }
    /**
     * @return {?}
     */
    clearTable() {
        if (!this.isBetPlaced) {
            this.game.bet.reset();
        }
    }
    /**
     * @return {?}
     */
    placeBet() {
        if (this.game.isStarted) {
            return;
        }
        this.alert.clear();
        if (this.game.bet.amount == 0) {
            this.alert.error('Place Chips on Table to Place Your Bet!');
            return;
        }
        // *** BROADCAST BET TRANSACTION TO BLOCK CHAIN ***
        this.server.placeBet(this.betAmount);
    }
    /**
     * @param {?} bet
     * @return {?}
     */
    onMyBetResolved(bet) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield sleep(2000);
            yield this.updateBalances(bet);
            // *** recalculate max bet ***
            if (!this.isBetPlaced &&
                this.game.bet.isMax) {
                this.game.bet.setToMax(yield this.getMaxBet());
            }
        });
    }
    /**
     * @return {?}
     */
    getMaxBet() {
        return this.bankroll.getMaxBet(4);
    }
    /**
     * @return {?}
     */
    get betAmount() {
        return this.selectedToken.getAssetAmount(this.game.bet.amount.toString());
    }
    /**
     * @return {?}
     */
    get isSplit() {
        return this.game.isSplit;
    }
    /**
     * @return {?}
     */
    get isBetPlaced() {
        return this.game.isBetPlaced;
    }
}
BlackjackComponentBase.propDecorators = {
    gameResultModal: [{ type: ViewChild, args: ['gameResultModal',] }]
};
if (false) {
    /** @type {?} */
    BlackjackComponentBase.prototype.gameResultModal;
    /**
     * @type {?}
     * @private
     */
    BlackjackComponentBase.prototype.server;
    /** @type {?} */
    BlackjackComponentBase.prototype.game;
    /**
     * @type {?}
     * @private
     */
    BlackjackComponentBase.prototype.renderer;
    /** @type {?} */
    BlackjackComponentBase.prototype.modal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2NvbXBvbmVudHMvbWFpbi9ibGFja2phY2suY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBMkMsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRW5GLE9BQU8sRUFBRSxLQUFLLEVBQWlCLE1BQU0sZ0JBQWdCLENBQUM7QUFFdEQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDdkYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFHakUsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFekUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7Ozs7Ozs7OztBQWtCOUUsTUFBTSw2QkFBOEIsU0FBUSxpQkFBaUI7Ozs7Ozs7OztJQVV6RCxZQUNJLEdBQW1CLEVBQ1gsUUFBbUIsRUFDbEIsS0FBMEIsRUFDbkMsZUFBdUMsRUFDdkMsV0FBd0IsRUFDeEIsWUFBNkI7UUFFN0IsS0FBSyxDQUFDLEdBQUcsRUFBQyxlQUFlLEVBQUMsV0FBVyxDQUFDLENBQUM7UUFOL0IsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUNsQixVQUFLLEdBQUwsS0FBSyxDQUFxQjtRQVFuQyx1QkFBdUI7UUFDdkIsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUM7UUFHbkMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLHVCQUF1QixDQUFDLElBQUksRUFBQyxJQUFJLENBQUMsTUFBTSxFQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRXRFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7Ozs7SUFDTyxjQUFjLENBQUMsV0FBd0I7UUFDM0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLG9CQUFvQixDQUFDLElBQUksRUFBQyxXQUFXLENBQUMsQ0FBQztJQUM3RCxDQUFDOzs7Ozs7SUFDTyxlQUFlLENBQUMsV0FBNEI7UUFDaEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLDJCQUEyQixDQUN6QyxJQUFJLEVBQ0osV0FBVyxFQUNYLElBQUksQ0FBQyxHQUFHLENBQ1gsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRU8sWUFBWTtRQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7SUFFRCwwQkFBMEI7UUFDdEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ1gsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ1gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQztRQUV0RCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFHbEIsa0NBQWtDO1FBQ2xDLDhDQUE4QztJQUNsRCxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUM7SUFDN0QsQ0FBQzs7OztJQUdELEtBQUs7UUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVPLFVBQVU7UUFDZCxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUU7WUFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBQ3BDO0lBQ0wsQ0FBQzs7OztJQUdELHdCQUF3QjtRQUNwQixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQUVELGNBQWM7UUFDVixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkIsQ0FBQzs7OztJQUVELGVBQWU7UUFDWCxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbEIsT0FBTztTQUNWOztjQUdLLFVBQVUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUU5RCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxVQUFVLEVBQUU7WUFDcEMsT0FBTztTQUNWO1FBR0QsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFckIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsVUFBVSxFQUFFO1lBQ25DLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7U0FDckM7SUFDTCxDQUFDOzs7O0lBRUssZUFBZTs7WUFDakIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQixPQUFPO2FBQ1Y7O2tCQUdLLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFFckMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksTUFBTSxFQUFFO2dCQUNoQyxPQUFPO2FBQ1Y7WUFHRCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUV2QixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxNQUFNLEVBQUU7Z0JBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNsQztRQUNMLENBQUM7S0FBQTs7OztJQUVLLE9BQU87O1lBQ1QsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQixPQUFPO2FBQ1Y7WUFHRCxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFOztzQkFDbkIsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFFNUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzdCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FDbEIsTUFBTSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQ3pCLENBQUM7YUFDTDtRQUNMLENBQUM7S0FBQTs7OztJQUVELFVBQVU7UUFDTixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbEIsT0FBTztTQUNWO1FBR0QsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTs7a0JBQ25CLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7WUFFNUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ2xDO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN6QjtJQUNMLENBQUM7Ozs7SUFFRCxVQUFVO1FBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDekI7SUFDTCxDQUFDOzs7O0lBSUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDckIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUVuQixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQ1oseUNBQXlDLENBQzVDLENBQUM7WUFDRixPQUFPO1NBQ1Y7UUFHRCxtREFBbUQ7UUFDbkQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQ2hCLElBQUksQ0FBQyxTQUFTLENBQ2pCLENBQUM7SUFDTixDQUFDOzs7OztJQUVLLGVBQWUsQ0FBQyxHQUFzQjs7WUFDeEMsTUFBTSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFbEIsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRS9CLDhCQUE4QjtZQUM5QixJQUNJLENBQUMsSUFBSSxDQUFDLFdBQVc7Z0JBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFDckI7Z0JBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUNsQixNQUFNLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FDekIsQ0FBQzthQUNMO1FBQ0wsQ0FBQztLQUFBOzs7O0lBR0QsU0FBUztRQUNMLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7OztJQUdELElBQUksU0FBUztRQUNULE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQ3BDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FDbEMsQ0FBQztJQUNOLENBQUM7Ozs7SUFHRCxJQUFJLE9BQU87UUFDUCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQzdCLENBQUM7Ozs7SUFFRCxJQUFJLFdBQVc7UUFDWCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ2pDLENBQUM7Ozs4QkE5TkEsU0FBUyxTQUFDLGlCQUFpQjs7OztJQUE1QixpREFDd0M7Ozs7O0lBR3hDLHdDQUFnQzs7SUFDaEMsc0NBQXNDOzs7OztJQUlsQywwQ0FBMkI7O0lBQzNCLHVDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgUmVuZGVyZXIyLCBWaWV3RW5jYXBzdWxhdGlvbiwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IHNsZWVwLEVPU0Fzc2V0QW1vdW50IH0gZnJvbSAnZWFybmJldC1jb21tb24nO1xuXG5pbXBvcnQgeyBCbGFja2phY2tHYW1lQ29udHJvbGxlciB9IGZyb20gJy4vZ2FtZS9ibGFja2phY2stY29udHJvbGxlcic7XG5pbXBvcnQgeyBCbGFja2phY2tNb2RhbENvbXBvbmVudCB9IGZyb20gJy4uL2JsYWNramFjay1tb2RhbC9ibGFja2phY2stbW9kYWwuY29tcG9uZW50JztcbmltcG9ydCB7IExvY2FsQmxhY2tqYWNrU2VydmVyIH0gZnJvbSAnLi4vLi4vc2VydmVyL2xvY2FsLXNlcnZlcic7XG5pbXBvcnQgeyBJQmxhY2tqYWNrU2VydmVyIH0gZnJvbSAnLi4vLi4vc2VydmVyL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSUJsYWNramFja0NvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBSZW1vdGVCbGFja2phY2tTZXJ2ZXJCcmlkZ2UgfSBmcm9tICcuLi8uLi9zZXJ2ZXIvcmVtb3RlLXNlcnZlcic7XG5pbXBvcnQgeyBJQmxhY2tqYWNrQ2xpZW50TGlzdGVuZXIgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IEdhbWVDb21wb25lbnRCYXNlIH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9iYXNlL2dhbWUtY29tcG9uZW50LWJhc2UnO1xuaW1wb3J0IHsgSUJldHNMaXN0ZW5lciB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vbW9kZWxzL2JldHMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQXBwU3RhdGVMaXN0ZW5lciB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vbW9kZWxzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSU1haW5BcHBDb250ZXh0LCBJR29vZ2xlQW5hbHl0aWNzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vc2VydmljZXMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBPdmVybGF5TW9kYWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9vdmVybGF5LW1vZGFsLnNlcnZpY2UnO1xuaW1wb3J0IHsgQmxhY2tqYWNrQmV0UmVzdWx0IH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9tb2RlbHMvYmV0cy9ibGFja2phY2stYmV0JztcbmltcG9ydCB7IElTb3VuZFBsYXllciB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vbWVkaWEvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJRW9zQWNjb3VudE5hbWVzIH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9tb2RlbHMvYnVzaW5lc3MtcnVsZXMvaW50ZXJmYWNlcyc7XG5cblxuLypcbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYXBwLWJsYWNramFjaycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2JsYWNramFjay5jb21wb25lbnQuaHRtbCcsXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgICBzdHlsZVVybHM6IFsnLi9ibGFja2phY2suY29tcG9uZW50LnNjc3MnXVxufSlcbiovXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrQ29tcG9uZW50QmFzZSBleHRlbmRzIEdhbWVDb21wb25lbnRCYXNlXG4gICAgaW1wbGVtZW50cyBJQmxhY2tqYWNrQ29tcG9uZW50LElCZXRzTGlzdGVuZXIsSUFwcFN0YXRlTGlzdGVuZXIsSUJsYWNramFja0NsaWVudExpc3RlbmVyXG57XG4gICAgQFZpZXdDaGlsZCgnZ2FtZVJlc3VsdE1vZGFsJylcbiAgICBnYW1lUmVzdWx0TW9kYWw6QmxhY2tqYWNrTW9kYWxDb21wb25lbnQ7XG5cblxuICAgIHByaXZhdGUgc2VydmVyOklCbGFja2phY2tTZXJ2ZXI7XG4gICAgcmVhZG9ubHkgZ2FtZTpCbGFja2phY2tHYW1lQ29udHJvbGxlcjtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBhcHA6SU1haW5BcHBDb250ZXh0LFxuICAgICAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsXG4gICAgICAgIHJlYWRvbmx5IG1vZGFsOiBPdmVybGF5TW9kYWxTZXJ2aWNlLFxuICAgICAgICBnb29nbGVBbmFseXRpY3M6SUdvb2dsZUFuYWx5dGljc1NlcnZpY2UsXG4gICAgICAgIHNvdW5kUGxheWVyOklTb3VuZFBsYXllcixcbiAgICAgICAgYWNjb3VudE5hbWVzOklFb3NBY2NvdW50TmFtZXNcbiAgICApIHtcbiAgICAgICAgc3VwZXIoYXBwLGdvb2dsZUFuYWx5dGljcyxzb3VuZFBsYXllcik7XG5cblxuICAgICAgICAvLyAqKiogRm9yIFRlc3Rpbmc6ICoqKlxuICAgICAgICAvL3RoaXMudXNlTG9jYWxTZXJ2ZXIoKTtcbiAgICAgICAgdGhpcy51c2VSZW1vdGVTZXJ2ZXIoYWNjb3VudE5hbWVzKTtcblxuXG4gICAgICAgIHRoaXMuZ2FtZSA9IG5ldyBCbGFja2phY2tHYW1lQ29udHJvbGxlcih0aGlzLHRoaXMuc2VydmVyLHNvdW5kUGxheWVyKTtcblxuICAgICAgICB0aGlzLnNldExpc3RlbmVycygpO1xuICAgIH1cbiAgICBwcml2YXRlIHVzZUxvY2FsU2VydmVyKHNvdW5kUGxheWVyOklTb3VuZFBsYXllcikge1xuICAgICAgICB0aGlzLnNlcnZlciA9IG5ldyBMb2NhbEJsYWNramFja1NlcnZlcih0aGlzLHNvdW5kUGxheWVyKTtcbiAgICB9XG4gICAgcHJpdmF0ZSB1c2VSZW1vdGVTZXJ2ZXIoYWNvdW50TmFtZXM6SUVvc0FjY291bnROYW1lcykge1xuICAgICAgICB0aGlzLnNlcnZlciA9IG5ldyBSZW1vdGVCbGFja2phY2tTZXJ2ZXJCcmlkZ2UoXG4gICAgICAgICAgICB0aGlzLFxuICAgICAgICAgICAgYWNvdW50TmFtZXMsXG4gICAgICAgICAgICB0aGlzLmFwcFxuICAgICAgICApO1xuICAgIH1cblxuICAgIHByaXZhdGUgc2V0TGlzdGVuZXJzKCkge1xuICAgICAgICB0aGlzLmFwcC5zdGF0ZS5zZXRMaXN0ZW5lcih0aGlzKTtcbiAgICAgICAgdGhpcy5hcHAuYmxhY2tqYWNrLmNsaWVudC5zZXRMaXN0ZW5lcih0aGlzKTtcbiAgICAgICAgdGhpcy5hcHAuc3RhdGUuYmV0cy5zZXRMaXN0ZW5lcih0aGlzKTtcbiAgICB9XG5cbiAgICBvbkJsYWNramFja0NsaWVudENvbm5lY3RlZCgpIHtcbiAgICAgICAgdGhpcy5yZXN1bWVHYW1lKCk7XG4gICAgfVxuXG4gICAgb25BdXRoZW50aWNhdGVkKCkge1xuICAgICAgICB0aGlzLnJlc3VtZUdhbWUoKTtcbiAgICB9XG5cbiAgICBuZ0FmdGVyVmlld0luaXQgKCkge1xuICAgICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKGRvY3VtZW50LmJvZHksICdibGFja2phY2stYmcnKTtcblxuICAgICAgICB0aGlzLnJlc3VtZUdhbWUoKTtcblxuXG4gICAgICAgIC8vICoqKiBibGFja2phY2sgZGVwb3NpdCBtb2RhbCAqKipcbiAgICAgICAgLy90aGlzLm1vZGFsLm9wZW4oJ2JsYWNramFja0dhbWVOb3RpY2VNb2RhbCcpO1xuICAgIH1cblxuICAgIG5nT25EZXN0cm95ICgpIHtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5yZW1vdmVDbGFzcyhkb2N1bWVudC5ib2R5LCAnYmxhY2tqYWNrLWJnJyk7XG4gICAgfVxuXG5cbiAgICBkZWJ1ZygpIHtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5nYW1lKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJlc3VtZUdhbWUoKSB7XG4gICAgICAgIGlmICh0aGlzLmFwcC5ibGFja2phY2suY2xpZW50LmlzQ29ubmVjdGVkKSB7XG4gICAgICAgICAgICB0aGlzLnNlcnZlci5yZXN1bWVFeGlzdGluZ0dhbWUoKTtcbiAgICAgICAgfVxuICAgIH1cblxuXG4gICAgb25CZXR0aW5nQ3VycmVuY3lDaGFuZ2VkKCkge1xuICAgICAgICB0aGlzLmNsZWFyVGFibGUoKTtcbiAgICB9XG5cbiAgICBvbkNoaXBTZWxlY3RlZCgpIHtcbiAgICAgICAgdGhpcy5hZGRDaGlwKCk7XG4gICAgfVxuXG4gICAgaGFsZk9mQmV0QW1vdW50KCkge1xuICAgICAgICBpZiAodGhpcy5pc0JldFBsYWNlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCBtaW5pbXVtQmV0ID0gTnVtYmVyKHRoaXMuc2VsZWN0ZWRUb2tlbi5taW5pbXVtQmV0QW1vdW50KTtcblxuICAgICAgICBpZiAodGhpcy5nYW1lLmJldC5hbW91bnQgPT0gbWluaW11bUJldCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICB0aGlzLmdhbWUuYmV0LmhhbGYoKTtcblxuICAgICAgICBpZiAodGhpcy5nYW1lLmJldC5hbW91bnQgPCBtaW5pbXVtQmV0KSB7XG4gICAgICAgICAgICB0aGlzLmdhbWUuYmV0LmFtb3VudCA9IG1pbmltdW1CZXQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhc3luYyBkb3VibGVCZXRBbW91bnQoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzQmV0UGxhY2VkKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IG1heEJldCA9IGF3YWl0IHRoaXMuZ2V0TWF4QmV0KCk7XG5cbiAgICAgICAgaWYgKHRoaXMuZ2FtZS5iZXQuYW1vdW50ID09IG1heEJldCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICB0aGlzLmdhbWUuYmV0LmRvdWJsZSgpO1xuXG4gICAgICAgIGlmICh0aGlzLmdhbWUuYmV0LmFtb3VudCA+IG1heEJldCkge1xuICAgICAgICAgICAgdGhpcy5nYW1lLmJldC5zZXRUb01heChtYXhCZXQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYXN5bmMgYWRkQ2hpcCgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNCZXRQbGFjZWQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgaWYgKCF0aGlzLmlzTWF4Q2hpcFNlbGVjdGVkKSB7XG4gICAgICAgICAgICBjb25zdCBhbW91bnQgPSB0aGlzLmNoaXBzW3RoaXMuc2VsZWN0ZWRDaGlwXTtcblxuICAgICAgICAgICAgdGhpcy5nYW1lLmJldC5hZGQoYW1vdW50KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQuc2V0VG9NYXgoXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5nZXRNYXhCZXQoKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJlbW92ZUNoaXAoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzQmV0UGxhY2VkKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuXG4gICAgICAgIGlmICghdGhpcy5pc01heENoaXBTZWxlY3RlZCkge1xuICAgICAgICAgICAgY29uc3QgYW1vdW50ID0gdGhpcy5jaGlwc1t0aGlzLnNlbGVjdGVkQ2hpcF07XG5cbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQuc3VidHJhY3QoYW1vdW50KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQucmVzZXQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGNsZWFyVGFibGUoKSB7XG4gICAgICAgIGlmICghdGhpcy5pc0JldFBsYWNlZCkge1xuICAgICAgICAgICAgdGhpcy5nYW1lLmJldC5yZXNldCgpO1xuICAgICAgICB9XG4gICAgfVxuXG5cblxuICAgIHBsYWNlQmV0KCkge1xuICAgICAgICBpZiAodGhpcy5nYW1lLmlzU3RhcnRlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5hbGVydC5jbGVhcigpO1xuXG4gICAgICAgIGlmICh0aGlzLmdhbWUuYmV0LmFtb3VudCA9PSAwKSB7XG4gICAgICAgICAgICB0aGlzLmFsZXJ0LmVycm9yKFxuICAgICAgICAgICAgICAgICdQbGFjZSBDaGlwcyBvbiBUYWJsZSB0byBQbGFjZSBZb3VyIEJldCEnXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICAvLyAqKiogQlJPQURDQVNUIEJFVCBUUkFOU0FDVElPTiBUTyBCTE9DSyBDSEFJTiAqKipcbiAgICAgICAgdGhpcy5zZXJ2ZXIucGxhY2VCZXQoXG4gICAgICAgICAgICB0aGlzLmJldEFtb3VudFxuICAgICAgICApO1xuICAgIH1cblxuICAgIGFzeW5jIG9uTXlCZXRSZXNvbHZlZChiZXQ6QmxhY2tqYWNrQmV0UmVzdWx0KSB7XG4gICAgICAgIGF3YWl0IHNsZWVwKDIwMDApO1xuXG4gICAgICAgIGF3YWl0IHRoaXMudXBkYXRlQmFsYW5jZXMoYmV0KTtcblxuICAgICAgICAvLyAqKiogcmVjYWxjdWxhdGUgbWF4IGJldCAqKipcbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgIXRoaXMuaXNCZXRQbGFjZWQgJiZcbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQuaXNNYXhcbiAgICAgICAgKSB7XG4gICAgICAgICAgICB0aGlzLmdhbWUuYmV0LnNldFRvTWF4KFxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuZ2V0TWF4QmV0KClcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIGdldE1heEJldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYmFua3JvbGwuZ2V0TWF4QmV0KDQpO1xuICAgIH1cblxuXG4gICAgZ2V0IGJldEFtb3VudCgpOkVPU0Fzc2V0QW1vdW50IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRUb2tlbi5nZXRBc3NldEFtb3VudChcbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQuYW1vdW50LnRvU3RyaW5nKClcbiAgICAgICAgKTtcbiAgICB9XG5cblxuICAgIGdldCBpc1NwbGl0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5nYW1lLmlzU3BsaXQ7XG4gICAgfVxuXG4gICAgZ2V0IGlzQmV0UGxhY2VkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5nYW1lLmlzQmV0UGxhY2VkO1xuICAgIH1cbn0iXX0=