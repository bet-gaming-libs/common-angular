/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/bet-amount.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class BetAmount {
    constructor() {
        this._isMax = false;
        this._amount = 0;
    }
    /**
     * @return {?}
     */
    reset() {
        this.amount = 0;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    add(value) {
        this.amount += value;
    }
    /**
     * @return {?}
     */
    double() {
        this.amount *= 2;
    }
    /**
     * @return {?}
     */
    half() {
        this.amount *= 0.5;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    subtract(value) {
        this.amount -= value;
        if (this.amount < 0) {
            this.amount = 0;
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setToMax(value) {
        this.amount = value;
        this._isMax = true;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set amount(value) {
        this._isMax = false;
        this._amount = value;
    }
    /**
     * @return {?}
     */
    get amount() {
        return this._amount;
    }
    /**
     * @return {?}
     */
    get isMax() {
        return this._isMax;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    BetAmount.prototype._isMax;
    /**
     * @type {?}
     * @private
     */
    BetAmount.prototype._amount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmV0LWFtb3VudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2NvbXBvbmVudHMvbWFpbi9nYW1lL2JldC1hbW91bnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxNQUFNO0lBTUY7UUFKUSxXQUFNLEdBQUcsS0FBSyxDQUFDO1FBRWYsWUFBTyxHQUFHLENBQUMsQ0FBQztJQUdwQixDQUFDOzs7O0lBRUQsS0FBSztRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3BCLENBQUM7Ozs7O0lBRUQsR0FBRyxDQUFDLEtBQVk7UUFDWixJQUFJLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQztJQUN6QixDQUFDOzs7O0lBQ0QsTUFBTTtRQUNGLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO0lBQ3JCLENBQUM7Ozs7SUFDRCxJQUFJO1FBQ0EsSUFBSSxDQUFDLE1BQU0sSUFBSSxHQUFHLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBWTtRQUNqQixJQUFJLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQztRQUVyQixJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1NBQ25CO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBWTtRQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUN2QixDQUFDOzs7OztJQUVELElBQUksTUFBTSxDQUFDLEtBQVk7UUFDbkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDOzs7O0lBQ0QsSUFBSSxLQUFLO1FBQ0wsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7Q0FDSjs7Ozs7O0lBN0NHLDJCQUF1Qjs7Ozs7SUFFdkIsNEJBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEJldEFtb3VudFxue1xuICAgIHByaXZhdGUgX2lzTWF4ID0gZmFsc2U7XG5cbiAgICBwcml2YXRlIF9hbW91bnQgPSAwO1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgfVxuXG4gICAgcmVzZXQoKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ID0gMDtcbiAgICB9XG5cbiAgICBhZGQodmFsdWU6bnVtYmVyKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ICs9IHZhbHVlO1xuICAgIH1cbiAgICBkb3VibGUoKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ICo9IDI7XG4gICAgfVxuICAgIGhhbGYoKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ICo9IDAuNTtcbiAgICB9XG5cbiAgICBzdWJ0cmFjdCh2YWx1ZTpudW1iZXIpIHtcbiAgICAgICAgdGhpcy5hbW91bnQgLT0gdmFsdWU7XG5cbiAgICAgICAgaWYgKHRoaXMuYW1vdW50IDwgMCkge1xuICAgICAgICAgICAgdGhpcy5hbW91bnQgPSAwO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc2V0VG9NYXgodmFsdWU6bnVtYmVyKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ID0gdmFsdWU7XG4gICAgICAgIHRoaXMuX2lzTWF4ID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBzZXQgYW1vdW50KHZhbHVlOm51bWJlcikge1xuICAgICAgICB0aGlzLl9pc01heCA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9hbW91bnQgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICBnZXQgYW1vdW50KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fYW1vdW50O1xuICAgIH1cbiAgICBnZXQgaXNNYXgoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc01heDtcbiAgICB9XG59Il19