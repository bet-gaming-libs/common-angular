/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/blackjack-controller.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { sleep } from "earnbet-common";
import { BlackjackDealerHand, BlackjackPlayerHand } from './blackjack-hands';
import { BetAmount } from './bet-amount';
export class BlackjackGameController {
    /**
     * @param {?} component
     * @param {?} server
     * @param {?} soundPlayer
     */
    constructor(component, server, soundPlayer) {
        this.component = component;
        this.server = server;
        this.bet = new BetAmount();
        this.initialBetAmount = undefined;
        this._isSplit = false;
        this.hand1Result = undefined;
        this.hand2Result = undefined;
        this._promptForInsurance = false;
        this._showResult = false;
        this.dealer = new BlackjackDealerHand(soundPlayer);
        this.hand1 = new BlackjackPlayerHand(soundPlayer);
        this.hand2 = new BlackjackPlayerHand(soundPlayer);
    }
    /**
     * @private
     * @return {?}
     */
    reset() {
        this.component.gameResultModal.close();
        this.dealer.reset();
        this.hand1.reset();
        this.hand2.reset();
        this.initialBetAmount = undefined;
        this._isSplit = false;
        this.selectedHandIndex = 0;
        this.hand1Result = undefined;
        this.hand2Result = undefined;
        this._promptForInsurance = false;
        this._showResult = false;
    }
    /**
     * @param {?} cards
     * @param {?} betAmount
     * @return {?}
     */
    start(cards, betAmount) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.reset();
            if (this.bet.amount == 0) {
                this.bet.add(betAmount);
            }
            // player
            yield this.hand1.dealFirstCard(cards.playerCards[0], this.bet.amount);
            // dealer
            yield this.dealer.dealFirstCard(cards.dealerCards[0]);
            // player
            yield this.hand1.dealSecondCard(cards.playerCards[1]);
            // dealer
            yield this.dealer.dealAnonymousCard();
            yield this.checkForInsurance();
            this.initialBetAmount = this.bet.amount;
        });
    }
    /**
     * @return {?}
     */
    checkForInsurance() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // if dealer is showing ace, offer insurance
            if (this.dealer.isFirstCardAnAce) {
                this._promptForInsurance = true;
                return;
            }
            else {
                yield this.checkForBlackJack();
            }
        });
    }
    /**
     * @param {?} yesToInsurance
     * @return {?}
     */
    respondToInsurance(yesToInsurance) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this._promptForInsurance = false;
            if (yesToInsurance) {
                this.hand1.buyInsurance();
            }
            /** @type {?} */
            const isDealerBlackjack = yield this.server.respondToInsurance(yesToInsurance);
            this.checkForBlackJack(isDealerBlackjack);
        });
    }
    /**
     * @private
     * @param {?=} isDealerBlackjack
     * @return {?}
     */
    checkForBlackJack(isDealerBlackjack = undefined) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            console.log('isDealerBlackjack: ' + isDealerBlackjack);
            // *** peek for blackjack ***
            if (isDealerBlackjack == undefined) {
                isDealerBlackjack = yield this.server.peekForBlackjack();
            }
            if (isDealerBlackjack || this.hand1.isBlackJack) {
                this.conclude(isDealerBlackjack);
            }
        });
    }
    /**
     * @return {?}
     */
    split() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isAbleToSplit) {
                return;
            }
            this._isSplit = true;
            // split hands
            /** @type {?} */
            const card1 = this.hand1.cards[0].cardId;
            /** @type {?} */
            const card2 = this.hand1.cards[1].cardId;
            /** @type {?} */
            const newCards = yield this.server.split();
            /** @type {?} */
            const card3 = newCards[0];
            /** @type {?} */
            const card4 = newCards[1];
            yield this.hand1.split([card1, card3], this.initialBetAmount);
            yield this.hand2.split([card2, card4], this.initialBetAmount);
            if (!this.isWaitingForAction) {
                this.selectOtherHand();
            }
        });
    }
    /**
     * @return {?}
     */
    doubleDown() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.isAbleToDoubleDown) {
                /** @type {?} */
                const cardId = yield this.server.doubleDown(this.selectedHandIndex);
                // deal only 1 more card
                yield this.currentHand.doubleDown(cardId);
                // move to other hand (if applicable)
                this.selectOtherHand();
            }
        });
    }
    /**
     * @return {?}
     */
    hit() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.isWaitingForAction) {
                /** @type {?} */
                const cardId = yield this.server.hit(this.selectedHandIndex);
                // deal another card
                yield this.currentHand.hit(cardId);
                if (this.currentHand.isBusted) {
                    this.selectOtherHand();
                }
                else if (this.currentHand.is21) {
                    this.stand();
                }
            }
        });
    }
    /**
     * @return {?}
     */
    stand() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.isWaitingForAction) {
                yield this.server.stand();
                this.currentHand.stand();
                this.selectOtherHand();
            }
        });
    }
    /**
     * @private
     * @return {?}
     */
    selectOtherHand() {
        // move to other hand if applicable
        if (this.isSplit) {
            this.selectedHandIndex =
                this.selectedHandIndex == 0 ?
                    1 : 0;
            if (this.currentHand.isWaitingForAction) {
                return;
            }
        }
        // otherwise conclude
        this.conclude(false);
    }
    /**
     * @private
     * @param {?} isDealerBlackJack
     * @return {?}
     */
    conclude(isDealerBlackJack) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const isBusted = this.isSplit ?
                (this.hand1.isBusted &&
                    this.hand2.isBusted) :
                this.hand1.isBusted;
            if (!isBusted) {
                /** @type {?} */
                const cardId = yield this.server.getDealersSecondCard();
                yield this.dealer.revealSecondCard(cardId);
            }
            /** @type {?} */
            const isBlackjackForHand1 = !this.isSplit &&
                this.hand1.isBlackJack;
            if (!isBusted && !isDealerBlackJack && !isBlackjackForHand1) {
                // deal additional cards for dealer, if applicable
                /** @type {?} */
                const cards = yield this.server.getDealersAdditionalCards();
                for (var cardId of cards) {
                    yield this.dealer.dealAdditionalCard(cardId);
                }
            }
            /*** check hand 1 result */
            // determine hand 1 result
            this.hand1Result = this.hand1.determineResult(this.dealer);
            //alert('Hand 1 Result: '+this.hand1Result);
            //let result:IBlackjackResult = this.hand1Result;
            /*** check the result of the second hand (if applicable) ***/
            if (this.isSplit) {
                this.hand2Result = this.hand2.determineResult(this.dealer);
                /*
                result = new CombinedHandsResult(
                    this.hand1Result,
                    this.hand2Result
                );
                */
                //alert('Hand 2 Result: '+this.hand2Result);
            }
            /*** end game: ***/
            this.initialBetAmount = undefined;
            this._showResult = true;
            if (this.hand1Result.isInsuranceSuccess ||
                this.hand1Result.isBlackjack) {
                this.component.gameResultModal.showResult(this.hand1Result);
            }
            else if (this.isSplit && this.hand2Result.isBlackjack) {
                this.component.gameResultModal.showResult(this.hand2Result);
            }
            yield sleep(3000);
            this._showResult = false;
            this.component.gameResultModal.close();
            //await this.component.gameResultModal.showResult(result);
            /*
            await sleep(3000);
            if (!this.isBetPlaced) {
                this.reset();
            }
            */
        });
    }
    /**
     * @return {?}
     */
    get isAbleToSplit() {
        return !this.isSplit &&
            this.hand1.isAbleToSplit;
    }
    /**
     * @return {?}
     */
    get isAbleToDoubleDown() {
        return this.currentHand.isAbleToDoubleDown;
    }
    /**
     * @private
     * @return {?}
     */
    get currentHand() {
        return this.selectedHandIndex == 0 ?
            this.hand1 :
            this.hand2;
    }
    /**
     * @return {?}
     */
    get isSplit() {
        return this._isSplit;
    }
    /**
     * @return {?}
     */
    get isFirstHandSelected() {
        return this.selectedHandIndex == 0 &&
            this.isWaitingForAction;
    }
    /**
     * @return {?}
     */
    get isSecondHandSelected() {
        return this.selectedHandIndex == 1 &&
            this.isWaitingForAction;
    }
    /**
     * @return {?}
     */
    get isWaitingForAction() {
        return this.isStarted &&
            this.currentHand.isWaitingForAction &&
            !this.server.isWaitingForResponse &&
            !this.isGameOver;
    }
    /**
     * @return {?}
     */
    get isStarted() {
        return this.isBetPlaced;
    }
    /**
     * @return {?}
     */
    get bet1() {
        return this.isBetPlaced ?
            this.hand1.bet :
            this.bet;
    }
    /**
     * @return {?}
     */
    get isLoading() {
        return this.server.isWaitingForResponse || (this.isBetPlaced &&
            !this.promptForInsurance &&
            !this.isWaitingForAction);
    }
    /**
     * @return {?}
     */
    get isAbleToPlaceBet() {
        return !this.isBetPlaced &&
            !this.server.isWaitingForResponse;
    }
    /**
     * @return {?}
     */
    get isBetPlaced() {
        return this.initialBetAmount != undefined;
    }
    /**
     * @return {?}
     */
    get isGameOver() {
        return this.hand1Result != undefined;
    }
    /**
     * @return {?}
     */
    get isDealerWin() {
        return this.isSplit ?
            (this.isLossForHand1 &&
                this.isLossForHand2) :
            this.isLossForHand1;
    }
    /**
     * @return {?}
     */
    get isDealerLoss() {
        return this.dealer.isBusted ||
            (this.isSplit ?
                (this.isWinForHand1 &&
                    this.isWinForHand2) :
                this.isWinForHand1);
    }
    /**
     * @return {?}
     */
    get amountWon1() {
        return this.hand1Result ?
            this.hand1Result.amountWon :
            0;
    }
    /**
     * @return {?}
     */
    get amountWon2() {
        return this.hand2Result ?
            this.hand2Result.amountWon :
            0;
    }
    /**
     * @return {?}
     */
    get isWinForHand1() {
        return this.hand1Result && this.hand1Result.isWin;
    }
    /**
     * @return {?}
     */
    get showLossForHand1() {
        return this.isSplit ?
            (this.isGameOver ?
                this.isLossForHand1 && this.showResult :
                this.hand1.isBusted) :
            this.isLossForHand1;
    }
    /**
     * @return {?}
     */
    get isLossForHand1() {
        return this.hand1.isBusted ||
            (this.hand1Result && this.hand1Result.isLoss);
    }
    /**
     * @return {?}
     */
    get isPushForHand1() {
        return this.hand1Result && this.hand1Result.isPush;
    }
    /**
     * @return {?}
     */
    get isWinForHand2() {
        return this.hand2Result && this.hand2Result.isWin;
    }
    /**
     * @return {?}
     */
    get isLossForHand2() {
        return this.hand2.isBusted ||
            (this.hand2Result && this.hand2Result.isLoss);
    }
    /**
     * @return {?}
     */
    get isPushForHand2() {
        return this.hand2Result && this.hand2Result.isPush;
    }
    /**
     * @return {?}
     */
    get promptForInsurance() {
        return this._promptForInsurance;
    }
    /**
     * @return {?}
     */
    get choseInsurance() {
        return this.hand1.boughtInsurance;
    }
    /**
     * @return {?}
     */
    get showResult() {
        return this._showResult;
    }
}
if (false) {
    /** @type {?} */
    BlackjackGameController.prototype.dealer;
    /** @type {?} */
    BlackjackGameController.prototype.hand1;
    /** @type {?} */
    BlackjackGameController.prototype.hand2;
    /** @type {?} */
    BlackjackGameController.prototype.bet;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.initialBetAmount;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype._isSplit;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.selectedHandIndex;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.hand1Result;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.hand2Result;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype._promptForInsurance;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype._showResult;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.component;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.server;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL2JsYWNramFjay9jb21wb25lbnRzL21haW4vZ2FtZS9ibGFja2phY2stY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQUUsS0FBSyxFQUEwQixNQUFNLGdCQUFnQixDQUFDO0FBRS9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQzdFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFPekMsTUFBTTs7Ozs7O0lBaUJGLFlBQ1ksU0FBNkIsRUFDN0IsTUFBdUIsRUFDL0IsV0FBd0I7UUFGaEIsY0FBUyxHQUFULFNBQVMsQ0FBb0I7UUFDN0IsV0FBTSxHQUFOLE1BQU0sQ0FBaUI7UUFiMUIsUUFBRyxHQUFHLElBQUksU0FBUyxFQUFFLENBQUM7UUFDdkIscUJBQWdCLEdBQVUsU0FBUyxDQUFDO1FBQ3BDLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFHakIsZ0JBQVcsR0FBb0IsU0FBUyxDQUFDO1FBQ3pDLGdCQUFXLEdBQW9CLFNBQVMsQ0FBQztRQUN6Qyx3QkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDNUIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFReEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksbUJBQW1CLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Ozs7SUFFTyxLQUFLO1FBQ1QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFbkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFNBQVMsQ0FBQztRQUNsQyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDO1FBRTNCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1FBQzdCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQzs7Ozs7O0lBRUssS0FBSyxDQUFDLEtBQTRCLEVBQUMsU0FBZ0I7O1lBQ3JELElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUdiLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN0QixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUMzQjtZQUdELFNBQVM7WUFDVCxNQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUMxQixLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FDbEIsQ0FBQztZQUNGLFNBQVM7WUFDVCxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV0RCxTQUFTO1lBQ1QsTUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEQsU0FBUztZQUNULE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBRXRDLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFHL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDO1FBQzVDLENBQUM7S0FBQTs7OztJQUVLLGlCQUFpQjs7WUFDbkIsNENBQTRDO1lBQzVDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDOUIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztnQkFFaEMsT0FBTzthQUNWO2lCQUFNO2dCQUNILE1BQU0sSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7YUFDbEM7UUFDTCxDQUFDO0tBQUE7Ozs7O0lBRUssa0JBQWtCLENBQUMsY0FBc0I7O1lBQzNDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7WUFFakMsSUFBSSxjQUFjLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDN0I7O2tCQUVLLGlCQUFpQixHQUNuQixNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDO1lBRXhELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzlDLENBQUM7S0FBQTs7Ozs7O0lBRWEsaUJBQWlCLENBQUMsb0JBQTRCLFNBQVM7O1lBQ2pFLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEdBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUdyRCw2QkFBNkI7WUFDN0IsSUFBSSxpQkFBaUIsSUFBSSxTQUFTLEVBQUU7Z0JBQ2hDLGlCQUFpQixHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2FBQzVEO1lBRUQsSUFBSSxpQkFBaUIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRTtnQkFDN0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2FBQ3BDO1FBQ0wsQ0FBQztLQUFBOzs7O0lBSUssS0FBSzs7WUFDUCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDckIsT0FBTzthQUNWO1lBR0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7OztrQkFJZixLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTTs7a0JBQ2xDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNOztrQkFFbEMsUUFBUSxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUU7O2tCQUVwQyxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQzs7a0JBQ25CLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBRXpCLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQ2xCLENBQUMsS0FBSyxFQUFDLEtBQUssQ0FBQyxFQUNiLElBQUksQ0FBQyxnQkFBZ0IsQ0FDeEIsQ0FBQztZQUNGLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQ2xCLENBQUMsS0FBSyxFQUFDLEtBQUssQ0FBQyxFQUNiLElBQUksQ0FBQyxnQkFBZ0IsQ0FDeEIsQ0FBQztZQUVGLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUMxQjtRQUNMLENBQUM7S0FBQTs7OztJQUVLLFVBQVU7O1lBQ1osSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7O3NCQUNuQixNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FDdkMsSUFBSSxDQUFDLGlCQUFpQixDQUN6QjtnQkFFRCx3QkFBd0I7Z0JBQ3hCLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBRTFDLHFDQUFxQztnQkFDckMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQzFCO1FBQ0wsQ0FBQztLQUFBOzs7O0lBRUssR0FBRzs7WUFDTCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTs7c0JBQ25CLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUNoQyxJQUFJLENBQUMsaUJBQWlCLENBQ3pCO2dCQUVELG9CQUFvQjtnQkFDcEIsTUFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFFbkMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRTtvQkFDM0IsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2lCQUMxQjtxQkFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFO29CQUM1QixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7aUJBQ2hCO2FBQ0o7UUFDTCxDQUFDO0tBQUE7Ozs7SUFFSyxLQUFLOztZQUNQLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO2dCQUN6QixNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBRTFCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBRXpCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUMxQjtRQUNMLENBQUM7S0FBQTs7Ozs7SUFFTyxlQUFlO1FBQ25CLG1DQUFtQztRQUNuQyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZCxJQUFJLENBQUMsaUJBQWlCO2dCQUNsQixJQUFJLENBQUMsaUJBQWlCLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixFQUFFO2dCQUNyQyxPQUFPO2FBQ1Y7U0FDSjtRQUVELHFCQUFxQjtRQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUVhLFFBQVEsQ0FBQyxpQkFBeUI7OztrQkFDdEMsUUFBUSxHQUNWLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDVixDQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUTtvQkFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQ3RCLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVE7WUFFM0IsSUFBSSxDQUFDLFFBQVEsRUFBRTs7c0JBQ0wsTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRTtnQkFFdkQsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzlDOztrQkFFSyxtQkFBbUIsR0FDckIsQ0FBQyxJQUFJLENBQUMsT0FBTztnQkFDYixJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVc7WUFHMUIsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsbUJBQW1CLEVBQUU7OztzQkFFbkQsS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsRUFBRTtnQkFFM0QsS0FBSyxJQUFJLE1BQU0sSUFBSSxLQUFLLEVBQUU7b0JBQ3RCLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFFaEQ7YUFDSjtZQUdELDJCQUEyQjtZQUMzQiwwQkFBMEI7WUFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFM0QsNENBQTRDO1lBQzVDLGlEQUFpRDtZQUdqRCw2REFBNkQ7WUFDN0QsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNkLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUUzRDs7Ozs7a0JBS0U7Z0JBRUYsNENBQTRDO2FBQy9DO1lBSUQsbUJBQW1CO1lBQ25CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7WUFFbEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFHeEIsSUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQjtnQkFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQzlCO2dCQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDL0Q7aUJBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFO2dCQUNuRCxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQy9EO1lBR0QsTUFBTSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFbEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7WUFFekIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUM7WUFNdkMsMERBQTBEO1lBRTFEOzs7OztjQUtFO1FBQ04sQ0FBQztLQUFBOzs7O0lBSUQsSUFBSSxhQUFhO1FBQ2IsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPO1lBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUM7SUFDckMsQ0FBQzs7OztJQUVELElBQUksa0JBQWtCO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQztJQUMvQyxDQUFDOzs7OztJQUVELElBQVksV0FBVztRQUNuQixPQUFPLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDWixJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFFRCxJQUFJLE9BQU87UUFDUCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELElBQUksbUJBQW1CO1FBQ25CLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUM7WUFDMUIsSUFBSSxDQUFDLGtCQUFrQixDQUFDO0lBQ3BDLENBQUM7Ozs7SUFDRCxJQUFJLG9CQUFvQjtRQUNwQixPQUFPLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxDQUFDO1lBQzFCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztJQUNwQyxDQUFDOzs7O0lBRUQsSUFBSSxrQkFBa0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsU0FBUztZQUNiLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCO1lBQ25DLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0I7WUFDakMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQzdCLENBQUM7Ozs7SUFFRCxJQUFJLFNBQVM7UUFDVCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVELElBQUksSUFBSTtRQUNKLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUNyQixDQUFDOzs7O0lBRUQsSUFBSSxTQUFTO1FBQ1QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLG9CQUFvQixJQUFJLENBQ3ZDLElBQUksQ0FBQyxXQUFXO1lBQ2hCLENBQUMsSUFBSSxDQUFDLGtCQUFrQjtZQUN4QixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FDM0IsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxJQUFJLGdCQUFnQjtRQUNoQixPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVc7WUFDaEIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDO0lBQzlDLENBQUM7Ozs7SUFFRCxJQUFJLFdBQVc7UUFDWCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxTQUFTLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNWLE9BQU8sSUFBSSxDQUFDLFdBQVcsSUFBSSxTQUFTLENBQUM7SUFDekMsQ0FBQzs7OztJQUVELElBQUksV0FBVztRQUNYLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2IsQ0FDSSxJQUFJLENBQUMsY0FBYztnQkFDbkIsSUFBSSxDQUFDLGNBQWMsQ0FDdEIsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUVoQyxDQUFDOzs7O0lBRUQsSUFBSSxZQUFZO1FBQ1osT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVE7WUFDdkIsQ0FDSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2QsQ0FDSSxJQUFJLENBQUMsYUFBYTtvQkFDbEIsSUFBSSxDQUFDLGFBQWEsQ0FDckIsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxhQUFhLENBQ3JCLENBQUM7SUFDVixDQUFDOzs7O0lBRUQsSUFBSSxVQUFVO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1QixDQUFDLENBQUM7SUFDZCxDQUFDOzs7O0lBQ0QsSUFBSSxVQUFVO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1QixDQUFDLENBQUM7SUFDZCxDQUFDOzs7O0lBRUQsSUFBSSxhQUFhO1FBQ2IsT0FBTyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO0lBQ3RELENBQUM7Ozs7SUFFRCxJQUFJLGdCQUFnQjtRQUNoQixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNiLENBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNiLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FDMUIsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQsSUFBSSxjQUFjO1FBQ2QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVE7WUFDbEIsQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUQsQ0FBQzs7OztJQUNELElBQUksY0FBYztRQUNkLE9BQU8sSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQztJQUN2RCxDQUFDOzs7O0lBRUQsSUFBSSxhQUFhO1FBQ2IsT0FBTyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO0lBQ3RELENBQUM7Ozs7SUFDRCxJQUFJLGNBQWM7UUFDZCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUTtZQUNsQixDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7O0lBQ0QsSUFBSSxjQUFjO1FBQ2QsT0FBTyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO0lBQ3ZELENBQUM7Ozs7SUFFRCxJQUFJLGtCQUFrQjtRQUNsQixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztJQUNwQyxDQUFDOzs7O0lBQ0QsSUFBSSxjQUFjO1FBQ2QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQztJQUN0QyxDQUFDOzs7O0lBRUQsSUFBSSxVQUFVO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7Q0FDSjs7O0lBcGJHLHlDQUFvQzs7SUFDcEMsd0NBQW1DOztJQUNuQyx3Q0FBbUM7O0lBRW5DLHNDQUErQjs7Ozs7SUFDL0IsbURBQTRDOzs7OztJQUM1QywyQ0FBeUI7Ozs7O0lBQ3pCLG9EQUFpQzs7Ozs7SUFFakMsOENBQWlEOzs7OztJQUNqRCw4Q0FBaUQ7Ozs7O0lBQ2pELHNEQUFvQzs7Ozs7SUFDcEMsOENBQTRCOzs7OztJQUl4Qiw0Q0FBcUM7Ozs7O0lBQ3JDLHlDQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHNsZWVwLCBJQmxhY2tqYWNrSW5pdGlhbENhcmRzIH0gZnJvbSBcImVhcm5iZXQtY29tbW9uXCI7XG5cbmltcG9ydCB7IEJsYWNramFja0RlYWxlckhhbmQsIEJsYWNramFja1BsYXllckhhbmQgfSBmcm9tICcuL2JsYWNramFjay1oYW5kcyc7XG5pbXBvcnQgeyBCZXRBbW91bnQgfSBmcm9tICcuL2JldC1hbW91bnQnO1xuaW1wb3J0IHsgSUJsYWNramFja1Jlc3VsdCB9IGZyb20gJy4vaGFuZC1yZXN1bHQnO1xuaW1wb3J0IHsgSUJsYWNramFja0NvbXBvbmVudCB9IGZyb20gJy4uLy4uL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSUJsYWNramFja1NlcnZlciB9IGZyb20gJy4uLy4uLy4uL3NlcnZlci9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElTb3VuZFBsYXllciB9IGZyb20gJy4uLy4uLy4uLy4uL19jb21tb24vbWVkaWEvaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGNsYXNzIEJsYWNramFja0dhbWVDb250cm9sbGVyXG57XG4gICAgcmVhZG9ubHkgZGVhbGVyOkJsYWNramFja0RlYWxlckhhbmQ7XG4gICAgcmVhZG9ubHkgaGFuZDE6QmxhY2tqYWNrUGxheWVySGFuZDtcbiAgICByZWFkb25seSBoYW5kMjpCbGFja2phY2tQbGF5ZXJIYW5kO1xuXG4gICAgcmVhZG9ubHkgYmV0ID0gbmV3IEJldEFtb3VudCgpO1xuICAgIHByaXZhdGUgaW5pdGlhbEJldEFtb3VudDpudW1iZXIgPSB1bmRlZmluZWQ7XG4gICAgcHJpdmF0ZSBfaXNTcGxpdCA9IGZhbHNlO1xuICAgIHByaXZhdGUgc2VsZWN0ZWRIYW5kSW5kZXg6bnVtYmVyO1xuXG4gICAgcHJpdmF0ZSBoYW5kMVJlc3VsdDpJQmxhY2tqYWNrUmVzdWx0ID0gdW5kZWZpbmVkO1xuICAgIHByaXZhdGUgaGFuZDJSZXN1bHQ6SUJsYWNramFja1Jlc3VsdCA9IHVuZGVmaW5lZDtcbiAgICBwcml2YXRlIF9wcm9tcHRGb3JJbnN1cmFuY2UgPSBmYWxzZTtcbiAgICBwcml2YXRlIF9zaG93UmVzdWx0ID0gZmFsc2U7XG5cblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGNvbXBvbmVudDpJQmxhY2tqYWNrQ29tcG9uZW50LFxuICAgICAgICBwcml2YXRlIHNlcnZlcjpJQmxhY2tqYWNrU2VydmVyLFxuICAgICAgICBzb3VuZFBsYXllcjpJU291bmRQbGF5ZXJcbiAgICApIHtcbiAgICAgICAgdGhpcy5kZWFsZXIgPSBuZXcgQmxhY2tqYWNrRGVhbGVySGFuZChzb3VuZFBsYXllcik7XG4gICAgICAgIHRoaXMuaGFuZDEgPSBuZXcgQmxhY2tqYWNrUGxheWVySGFuZChzb3VuZFBsYXllcik7XG4gICAgICAgIHRoaXMuaGFuZDIgPSBuZXcgQmxhY2tqYWNrUGxheWVySGFuZChzb3VuZFBsYXllcik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSByZXNldCgpIHtcbiAgICAgICAgdGhpcy5jb21wb25lbnQuZ2FtZVJlc3VsdE1vZGFsLmNsb3NlKCk7XG5cbiAgICAgICAgdGhpcy5kZWFsZXIucmVzZXQoKTtcbiAgICAgICAgdGhpcy5oYW5kMS5yZXNldCgpO1xuICAgICAgICB0aGlzLmhhbmQyLnJlc2V0KCk7XG5cbiAgICAgICAgdGhpcy5pbml0aWFsQmV0QW1vdW50ID0gdW5kZWZpbmVkO1xuICAgICAgICB0aGlzLl9pc1NwbGl0ID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRIYW5kSW5kZXggPSAwO1xuXG4gICAgICAgIHRoaXMuaGFuZDFSZXN1bHQgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuaGFuZDJSZXN1bHQgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuX3Byb21wdEZvckluc3VyYW5jZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9zaG93UmVzdWx0ID0gZmFsc2U7XG4gICAgfVxuXG4gICAgYXN5bmMgc3RhcnQoY2FyZHM6SUJsYWNramFja0luaXRpYWxDYXJkcyxiZXRBbW91bnQ6bnVtYmVyKSB7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcblxuXG4gICAgICAgIGlmICh0aGlzLmJldC5hbW91bnQgPT0gMCkge1xuICAgICAgICAgICAgdGhpcy5iZXQuYWRkKGJldEFtb3VudCk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIC8vIHBsYXllclxuICAgICAgICBhd2FpdCB0aGlzLmhhbmQxLmRlYWxGaXJzdENhcmQoXG4gICAgICAgICAgICBjYXJkcy5wbGF5ZXJDYXJkc1swXSxcbiAgICAgICAgICAgIHRoaXMuYmV0LmFtb3VudFxuICAgICAgICApO1xuICAgICAgICAvLyBkZWFsZXJcbiAgICAgICAgYXdhaXQgdGhpcy5kZWFsZXIuZGVhbEZpcnN0Q2FyZChjYXJkcy5kZWFsZXJDYXJkc1swXSk7XG5cbiAgICAgICAgLy8gcGxheWVyXG4gICAgICAgIGF3YWl0IHRoaXMuaGFuZDEuZGVhbFNlY29uZENhcmQoY2FyZHMucGxheWVyQ2FyZHNbMV0pO1xuICAgICAgICAvLyBkZWFsZXJcbiAgICAgICAgYXdhaXQgdGhpcy5kZWFsZXIuZGVhbEFub255bW91c0NhcmQoKTtcblxuICAgICAgICBhd2FpdCB0aGlzLmNoZWNrRm9ySW5zdXJhbmNlKCk7XG5cblxuICAgICAgICB0aGlzLmluaXRpYWxCZXRBbW91bnQgPSB0aGlzLmJldC5hbW91bnQ7XG4gICAgfVxuXG4gICAgYXN5bmMgY2hlY2tGb3JJbnN1cmFuY2UoKSB7XG4gICAgICAgIC8vIGlmIGRlYWxlciBpcyBzaG93aW5nIGFjZSwgb2ZmZXIgaW5zdXJhbmNlXG4gICAgICAgIGlmICh0aGlzLmRlYWxlci5pc0ZpcnN0Q2FyZEFuQWNlKSB7XG4gICAgICAgICAgICB0aGlzLl9wcm9tcHRGb3JJbnN1cmFuY2UgPSB0cnVlO1xuXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLmNoZWNrRm9yQmxhY2tKYWNrKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhc3luYyByZXNwb25kVG9JbnN1cmFuY2UoeWVzVG9JbnN1cmFuY2U6Ym9vbGVhbikge1xuICAgICAgICB0aGlzLl9wcm9tcHRGb3JJbnN1cmFuY2UgPSBmYWxzZTtcblxuICAgICAgICBpZiAoeWVzVG9JbnN1cmFuY2UpIHtcbiAgICAgICAgICAgIHRoaXMuaGFuZDEuYnV5SW5zdXJhbmNlKCk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBpc0RlYWxlckJsYWNramFjayA9XG4gICAgICAgICAgICBhd2FpdCB0aGlzLnNlcnZlci5yZXNwb25kVG9JbnN1cmFuY2UoeWVzVG9JbnN1cmFuY2UpO1xuXG4gICAgICAgIHRoaXMuY2hlY2tGb3JCbGFja0phY2soaXNEZWFsZXJCbGFja2phY2spO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgY2hlY2tGb3JCbGFja0phY2soaXNEZWFsZXJCbGFja2phY2s6Ym9vbGVhbiA9IHVuZGVmaW5lZCkge1xuICAgICAgICBjb25zb2xlLmxvZygnaXNEZWFsZXJCbGFja2phY2s6ICcraXNEZWFsZXJCbGFja2phY2spO1xuXG5cbiAgICAgICAgLy8gKioqIHBlZWsgZm9yIGJsYWNramFjayAqKipcbiAgICAgICAgaWYgKGlzRGVhbGVyQmxhY2tqYWNrID09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaXNEZWFsZXJCbGFja2phY2sgPSBhd2FpdCB0aGlzLnNlcnZlci5wZWVrRm9yQmxhY2tqYWNrKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaXNEZWFsZXJCbGFja2phY2sgfHwgdGhpcy5oYW5kMS5pc0JsYWNrSmFjaykge1xuICAgICAgICAgICAgdGhpcy5jb25jbHVkZShpc0RlYWxlckJsYWNramFjayk7XG4gICAgICAgIH1cbiAgICB9XG5cblxuXG4gICAgYXN5bmMgc3BsaXQoKSB7XG4gICAgICAgIGlmICghdGhpcy5pc0FibGVUb1NwbGl0KSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuXG4gICAgICAgIHRoaXMuX2lzU3BsaXQgPSB0cnVlO1xuXG5cbiAgICAgICAgLy8gc3BsaXQgaGFuZHNcbiAgICAgICAgY29uc3QgY2FyZDEgPSB0aGlzLmhhbmQxLmNhcmRzWzBdLmNhcmRJZDtcbiAgICAgICAgY29uc3QgY2FyZDIgPSB0aGlzLmhhbmQxLmNhcmRzWzFdLmNhcmRJZDtcblxuICAgICAgICBjb25zdCBuZXdDYXJkcyA9IGF3YWl0IHRoaXMuc2VydmVyLnNwbGl0KCk7XG5cbiAgICAgICAgY29uc3QgY2FyZDMgPSBuZXdDYXJkc1swXTtcbiAgICAgICAgY29uc3QgY2FyZDQgPSBuZXdDYXJkc1sxXTtcblxuICAgICAgICBhd2FpdCB0aGlzLmhhbmQxLnNwbGl0KFxuICAgICAgICAgICAgW2NhcmQxLGNhcmQzXSxcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbEJldEFtb3VudFxuICAgICAgICApO1xuICAgICAgICBhd2FpdCB0aGlzLmhhbmQyLnNwbGl0KFxuICAgICAgICAgICAgW2NhcmQyLGNhcmQ0XSxcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbEJldEFtb3VudFxuICAgICAgICApO1xuXG4gICAgICAgIGlmICghdGhpcy5pc1dhaXRpbmdGb3JBY3Rpb24pIHtcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0T3RoZXJIYW5kKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhc3luYyBkb3VibGVEb3duKCkge1xuICAgICAgICBpZiAodGhpcy5pc0FibGVUb0RvdWJsZURvd24pIHtcbiAgICAgICAgICAgIGNvbnN0IGNhcmRJZCA9IGF3YWl0IHRoaXMuc2VydmVyLmRvdWJsZURvd24oXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEhhbmRJbmRleFxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgLy8gZGVhbCBvbmx5IDEgbW9yZSBjYXJkXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmN1cnJlbnRIYW5kLmRvdWJsZURvd24oY2FyZElkKTtcblxuICAgICAgICAgICAgLy8gbW92ZSB0byBvdGhlciBoYW5kIChpZiBhcHBsaWNhYmxlKVxuICAgICAgICAgICAgdGhpcy5zZWxlY3RPdGhlckhhbmQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIGhpdCgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNXYWl0aW5nRm9yQWN0aW9uKSB7XG4gICAgICAgICAgICBjb25zdCBjYXJkSWQgPSBhd2FpdCB0aGlzLnNlcnZlci5oaXQoXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEhhbmRJbmRleFxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgLy8gZGVhbCBhbm90aGVyIGNhcmRcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuY3VycmVudEhhbmQuaGl0KGNhcmRJZCk7XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRIYW5kLmlzQnVzdGVkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RPdGhlckhhbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuY3VycmVudEhhbmQuaXMyMSkge1xuICAgICAgICAgICAgICAgIHRoaXMuc3RhbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIHN0YW5kKCkge1xuICAgICAgICBpZiAodGhpcy5pc1dhaXRpbmdGb3JBY3Rpb24pIHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuc2VydmVyLnN0YW5kKCk7XG5cbiAgICAgICAgICAgIHRoaXMuY3VycmVudEhhbmQuc3RhbmQoKTtcblxuICAgICAgICAgICAgdGhpcy5zZWxlY3RPdGhlckhhbmQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgc2VsZWN0T3RoZXJIYW5kKCkge1xuICAgICAgICAvLyBtb3ZlIHRvIG90aGVyIGhhbmQgaWYgYXBwbGljYWJsZVxuICAgICAgICBpZiAodGhpcy5pc1NwbGl0KSB7XG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkSGFuZEluZGV4ID1cbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkSGFuZEluZGV4ID09IDAgP1xuICAgICAgICAgICAgICAgICAgICAxIDogMDtcblxuICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudEhhbmQuaXNXYWl0aW5nRm9yQWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLy8gb3RoZXJ3aXNlIGNvbmNsdWRlXG4gICAgICAgIHRoaXMuY29uY2x1ZGUoZmFsc2UpO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgY29uY2x1ZGUoaXNEZWFsZXJCbGFja0phY2s6Ym9vbGVhbikge1xuICAgICAgICBjb25zdCBpc0J1c3RlZCA9XG4gICAgICAgICAgICB0aGlzLmlzU3BsaXQgP1xuICAgICAgICAgICAgICAgIChcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5pc0J1c3RlZCAmJlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmhhbmQyLmlzQnVzdGVkXG4gICAgICAgICAgICAgICAgKSA6XG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5pc0J1c3RlZDtcblxuICAgICAgICBpZiAoIWlzQnVzdGVkKSB7XG4gICAgICAgICAgICBjb25zdCBjYXJkSWQgPSBhd2FpdCB0aGlzLnNlcnZlci5nZXREZWFsZXJzU2Vjb25kQ2FyZCgpO1xuXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmRlYWxlci5yZXZlYWxTZWNvbmRDYXJkKGNhcmRJZCk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBpc0JsYWNramFja0ZvckhhbmQxID1cbiAgICAgICAgICAgICF0aGlzLmlzU3BsaXQgJiZcbiAgICAgICAgICAgIHRoaXMuaGFuZDEuaXNCbGFja0phY2s7XG5cblxuICAgICAgICBpZiAoIWlzQnVzdGVkICYmICFpc0RlYWxlckJsYWNrSmFjayAmJiAhaXNCbGFja2phY2tGb3JIYW5kMSkge1xuICAgICAgICAgICAgLy8gZGVhbCBhZGRpdGlvbmFsIGNhcmRzIGZvciBkZWFsZXIsIGlmIGFwcGxpY2FibGVcbiAgICAgICAgICAgIGNvbnN0IGNhcmRzID0gYXdhaXQgdGhpcy5zZXJ2ZXIuZ2V0RGVhbGVyc0FkZGl0aW9uYWxDYXJkcygpO1xuXG4gICAgICAgICAgICBmb3IgKHZhciBjYXJkSWQgb2YgY2FyZHMpIHtcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmRlYWxlci5kZWFsQWRkaXRpb25hbENhcmQoY2FyZElkKTtcblxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cblxuICAgICAgICAvKioqIGNoZWNrIGhhbmQgMSByZXN1bHQgKi9cbiAgICAgICAgLy8gZGV0ZXJtaW5lIGhhbmQgMSByZXN1bHRcbiAgICAgICAgdGhpcy5oYW5kMVJlc3VsdCA9IHRoaXMuaGFuZDEuZGV0ZXJtaW5lUmVzdWx0KHRoaXMuZGVhbGVyKTtcblxuICAgICAgICAvL2FsZXJ0KCdIYW5kIDEgUmVzdWx0OiAnK3RoaXMuaGFuZDFSZXN1bHQpO1xuICAgICAgICAvL2xldCByZXN1bHQ6SUJsYWNramFja1Jlc3VsdCA9IHRoaXMuaGFuZDFSZXN1bHQ7XG5cblxuICAgICAgICAvKioqIGNoZWNrIHRoZSByZXN1bHQgb2YgdGhlIHNlY29uZCBoYW5kIChpZiBhcHBsaWNhYmxlKSAqKiovXG4gICAgICAgIGlmICh0aGlzLmlzU3BsaXQpIHtcbiAgICAgICAgICAgIHRoaXMuaGFuZDJSZXN1bHQgPSB0aGlzLmhhbmQyLmRldGVybWluZVJlc3VsdCh0aGlzLmRlYWxlcik7XG5cbiAgICAgICAgICAgIC8qXG4gICAgICAgICAgICByZXN1bHQgPSBuZXcgQ29tYmluZWRIYW5kc1Jlc3VsdChcbiAgICAgICAgICAgICAgICB0aGlzLmhhbmQxUmVzdWx0LFxuICAgICAgICAgICAgICAgIHRoaXMuaGFuZDJSZXN1bHRcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICAvL2FsZXJ0KCdIYW5kIDIgUmVzdWx0OiAnK3RoaXMuaGFuZDJSZXN1bHQpO1xuICAgICAgICB9XG5cblxuXG4gICAgICAgIC8qKiogZW5kIGdhbWU6ICoqKi9cbiAgICAgICAgdGhpcy5pbml0aWFsQmV0QW1vdW50ID0gdW5kZWZpbmVkO1xuXG4gICAgICAgIHRoaXMuX3Nob3dSZXN1bHQgPSB0cnVlO1xuXG5cbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgdGhpcy5oYW5kMVJlc3VsdC5pc0luc3VyYW5jZVN1Y2Nlc3MgfHxcbiAgICAgICAgICAgIHRoaXMuaGFuZDFSZXN1bHQuaXNCbGFja2phY2tcbiAgICAgICAgKSB7XG4gICAgICAgICAgICB0aGlzLmNvbXBvbmVudC5nYW1lUmVzdWx0TW9kYWwuc2hvd1Jlc3VsdCh0aGlzLmhhbmQxUmVzdWx0KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLmlzU3BsaXQgJiYgdGhpcy5oYW5kMlJlc3VsdC5pc0JsYWNramFjaykge1xuICAgICAgICAgICAgdGhpcy5jb21wb25lbnQuZ2FtZVJlc3VsdE1vZGFsLnNob3dSZXN1bHQodGhpcy5oYW5kMlJlc3VsdCk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGF3YWl0IHNsZWVwKDMwMDApO1xuXG4gICAgICAgIHRoaXMuX3Nob3dSZXN1bHQgPSBmYWxzZTtcblxuICAgICAgICB0aGlzLmNvbXBvbmVudC5nYW1lUmVzdWx0TW9kYWwuY2xvc2UoKTtcblxuXG5cblxuXG4gICAgICAgIC8vYXdhaXQgdGhpcy5jb21wb25lbnQuZ2FtZVJlc3VsdE1vZGFsLnNob3dSZXN1bHQocmVzdWx0KTtcblxuICAgICAgICAvKlxuICAgICAgICBhd2FpdCBzbGVlcCgzMDAwKTtcbiAgICAgICAgaWYgKCF0aGlzLmlzQmV0UGxhY2VkKSB7XG4gICAgICAgICAgICB0aGlzLnJlc2V0KCk7XG4gICAgICAgIH1cbiAgICAgICAgKi9cbiAgICB9XG5cblxuXG4gICAgZ2V0IGlzQWJsZVRvU3BsaXQoKSB7XG4gICAgICAgIHJldHVybiAhdGhpcy5pc1NwbGl0ICYmXG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5pc0FibGVUb1NwbGl0O1xuICAgIH1cblxuICAgIGdldCBpc0FibGVUb0RvdWJsZURvd24oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRIYW5kLmlzQWJsZVRvRG91YmxlRG93bjtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCBjdXJyZW50SGFuZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRIYW5kSW5kZXggPT0gMCA/XG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMSA6XG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMjtcbiAgICB9XG5cbiAgICBnZXQgaXNTcGxpdCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzU3BsaXQ7XG4gICAgfVxuXG4gICAgZ2V0IGlzRmlyc3RIYW5kU2VsZWN0ZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkSGFuZEluZGV4ID09IDAgJiZcbiAgICAgICAgICAgICAgICB0aGlzLmlzV2FpdGluZ0ZvckFjdGlvbjtcbiAgICB9XG4gICAgZ2V0IGlzU2Vjb25kSGFuZFNlbGVjdGVkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZEhhbmRJbmRleCA9PSAxICYmXG4gICAgICAgICAgICAgICAgdGhpcy5pc1dhaXRpbmdGb3JBY3Rpb247XG4gICAgfVxuXG4gICAgZ2V0IGlzV2FpdGluZ0ZvckFjdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTdGFydGVkICYmXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50SGFuZC5pc1dhaXRpbmdGb3JBY3Rpb24gJiZcbiAgICAgICAgICAgICAgICAhdGhpcy5zZXJ2ZXIuaXNXYWl0aW5nRm9yUmVzcG9uc2UgJiZcbiAgICAgICAgICAgICAgICAhdGhpcy5pc0dhbWVPdmVyO1xuICAgIH1cblxuICAgIGdldCBpc1N0YXJ0ZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzQmV0UGxhY2VkO1xuICAgIH1cblxuICAgIGdldCBiZXQxKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc0JldFBsYWNlZCA/XG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5iZXQgOlxuICAgICAgICAgICAgICAgIHRoaXMuYmV0O1xuICAgIH1cblxuICAgIGdldCBpc0xvYWRpbmcoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlcnZlci5pc1dhaXRpbmdGb3JSZXNwb25zZSB8fCAoXG4gICAgICAgICAgICB0aGlzLmlzQmV0UGxhY2VkICYmXG4gICAgICAgICAgICAhdGhpcy5wcm9tcHRGb3JJbnN1cmFuY2UgJiZcbiAgICAgICAgICAgICF0aGlzLmlzV2FpdGluZ0ZvckFjdGlvblxuICAgICAgICApO1xuICAgIH1cblxuICAgIGdldCBpc0FibGVUb1BsYWNlQmV0KCkge1xuICAgICAgICByZXR1cm4gIXRoaXMuaXNCZXRQbGFjZWQgJiZcbiAgICAgICAgICAgICAgICAhdGhpcy5zZXJ2ZXIuaXNXYWl0aW5nRm9yUmVzcG9uc2U7XG4gICAgfVxuXG4gICAgZ2V0IGlzQmV0UGxhY2VkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pbml0aWFsQmV0QW1vdW50ICE9IHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICBnZXQgaXNHYW1lT3ZlcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaGFuZDFSZXN1bHQgIT0gdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIGdldCBpc0RlYWxlcldpbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTcGxpdCA/XG4gICAgICAgICAgICAgICAgKFxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzTG9zc0ZvckhhbmQxICYmXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNMb3NzRm9ySGFuZDJcbiAgICAgICAgICAgICAgICApIDpcbiAgICAgICAgICAgICAgICB0aGlzLmlzTG9zc0ZvckhhbmQxO1xuXG4gICAgfVxuXG4gICAgZ2V0IGlzRGVhbGVyTG9zcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVhbGVyLmlzQnVzdGVkIHx8XG4gICAgICAgICAgICAoXG4gICAgICAgICAgICAgICAgdGhpcy5pc1NwbGl0ID9cbiAgICAgICAgICAgICAgICAoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNXaW5Gb3JIYW5kMSAmJlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzV2luRm9ySGFuZDJcbiAgICAgICAgICAgICAgICApIDpcbiAgICAgICAgICAgICAgICB0aGlzLmlzV2luRm9ySGFuZDFcbiAgICAgICAgICAgICk7XG4gICAgfVxuXG4gICAgZ2V0IGFtb3VudFdvbjEoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhbmQxUmVzdWx0ID9cbiAgICAgICAgICAgICAgICB0aGlzLmhhbmQxUmVzdWx0LmFtb3VudFdvbiA6XG4gICAgICAgICAgICAgICAgMDtcbiAgICB9XG4gICAgZ2V0IGFtb3VudFdvbjIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhbmQyUmVzdWx0ID9cbiAgICAgICAgICAgICAgICB0aGlzLmhhbmQyUmVzdWx0LmFtb3VudFdvbiA6XG4gICAgICAgICAgICAgICAgMDtcbiAgICB9XG5cbiAgICBnZXQgaXNXaW5Gb3JIYW5kMSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaGFuZDFSZXN1bHQgJiYgdGhpcy5oYW5kMVJlc3VsdC5pc1dpbjtcbiAgICB9XG5cbiAgICBnZXQgc2hvd0xvc3NGb3JIYW5kMSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTcGxpdCA/XG4gICAgICAgICAgICAgICAgKFxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzR2FtZU92ZXIgP1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0xvc3NGb3JIYW5kMSAmJiB0aGlzLnNob3dSZXN1bHQgOlxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5pc0J1c3RlZFxuICAgICAgICAgICAgICAgICkgOlxuICAgICAgICAgICAgICAgIHRoaXMuaXNMb3NzRm9ySGFuZDE7XG4gICAgfVxuXG4gICAgZ2V0IGlzTG9zc0ZvckhhbmQxKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5oYW5kMS5pc0J1c3RlZCB8fFxuICAgICAgICAgICAgICAgICh0aGlzLmhhbmQxUmVzdWx0ICYmIHRoaXMuaGFuZDFSZXN1bHQuaXNMb3NzKTtcbiAgICB9XG4gICAgZ2V0IGlzUHVzaEZvckhhbmQxKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5oYW5kMVJlc3VsdCAmJiB0aGlzLmhhbmQxUmVzdWx0LmlzUHVzaDtcbiAgICB9XG5cbiAgICBnZXQgaXNXaW5Gb3JIYW5kMigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaGFuZDJSZXN1bHQgJiYgdGhpcy5oYW5kMlJlc3VsdC5pc1dpbjtcbiAgICB9XG4gICAgZ2V0IGlzTG9zc0ZvckhhbmQyKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5oYW5kMi5pc0J1c3RlZCB8fFxuICAgICAgICAgICAgICAgICh0aGlzLmhhbmQyUmVzdWx0ICYmIHRoaXMuaGFuZDJSZXN1bHQuaXNMb3NzKTtcbiAgICB9XG4gICAgZ2V0IGlzUHVzaEZvckhhbmQyKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5oYW5kMlJlc3VsdCAmJiB0aGlzLmhhbmQyUmVzdWx0LmlzUHVzaDtcbiAgICB9XG5cbiAgICBnZXQgcHJvbXB0Rm9ySW5zdXJhbmNlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fcHJvbXB0Rm9ySW5zdXJhbmNlO1xuICAgIH1cbiAgICBnZXQgY2hvc2VJbnN1cmFuY2UoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhbmQxLmJvdWdodEluc3VyYW5jZTtcbiAgICB9XG5cbiAgICBnZXQgc2hvd1Jlc3VsdCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3Nob3dSZXN1bHQ7XG4gICAgfVxufVxuIl19