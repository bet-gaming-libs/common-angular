/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/blackjack-hands.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { nameOfRank, sleep, CardRankNumber } from "earnbet-common";
import { CardController } from '../../../../_common/game/card-controller';
import { BetAmount } from './bet-amount';
import { BustedResult, PushResult, BlackjackResult, InsuranceSuccessResult, LossResult, WinResult } from './hand-result';
class BlackjackHand {
    /**
     * @param {?} soundPlayer
     */
    constructor(soundPlayer) {
        this.soundPlayer = soundPlayer;
        this._cards = [];
        this._numOfAces = 0;
        this.countWithoutAces = 0;
        this._isAllCardsKnown = false;
        this._isFirstCardAnAce = false;
        this.isSplit = false;
    }
    /**
     * @return {?}
     */
    reset() {
        this._cards = [];
        this._numOfAces = 0;
        this.countWithoutAces = 0;
        this._isAllCardsKnown = false;
        this._isFirstCardAnAce = false;
        this.isSplit = false;
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    hit(cardId) {
        return this.dealCard(cardId, true);
    }
    /**
     * @protected
     * @param {?} cardId
     * @param {?} shouldReveal
     * @param {?=} revealImmediately
     * @return {?}
     */
    dealCard(cardId, shouldReveal, revealImmediately = false) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const card = new CardController(this.soundPlayer);
            this.cards.push(card);
            /** @type {?} */
            const rankNumber = cardId % 13;
            /** @type {?} */
            const isAce = rankNumber == CardRankNumber.ACE;
            card.reveal({
                suit: Math.floor(cardId / 13),
                rankName: nameOfRank(rankNumber)
            }, rankNumber, cardId);
            if (!revealImmediately) {
                yield sleep(250);
            }
            if (shouldReveal) {
                card.dealAndFlip();
            }
            else {
                card.deal();
            }
            if (!revealImmediately) {
                yield sleep(250);
            }
            if (shouldReveal) {
                this.addValueOfCard(rankNumber, isAce);
            }
        });
    }
    /**
     * @private
     * @param {?} rankNumber
     * @param {?} isAce
     * @return {?}
     */
    addValueOfCard(rankNumber, isAce) {
        /** @type {?} */
        let valueOfCard = 0;
        switch (rankNumber) {
            case CardRankNumber.JACK:
            case CardRankNumber.QUEEN:
            case CardRankNumber.KING:
                valueOfCard = 10;
                break;
            case CardRankNumber.ACE:
                // will determine ace value later
                break;
            default:
                valueOfCard = rankNumber;
                break;
        }
        this.countWithoutAces += valueOfCard;
        if (isAce) {
            this._numOfAces++;
            if (this.numOfCards == 1) {
                this._isFirstCardAnAce = true;
            }
        }
    }
    /**
     * @return {?}
     */
    get isBusted() {
        return this.sum > 21;
    }
    /**
     * @return {?}
     */
    get is21() {
        return this.sum == 21;
    }
    /**
     * @return {?}
     */
    get sumAsString() {
        /** @type {?} */
        let sum = '' + this.sum;
        if (this.isSoftCount) {
            sum += '/' +
                (this.countWithoutAces + this.numOfAces);
        }
        ;
        return sum;
    }
    /**
     * @return {?}
     */
    get sum() {
        /** @type {?} */
        let sum = this.countWithoutAces + this.countOfAces;
        if (this.isBlackJack) {
            sum = 21;
        }
        return sum;
    }
    /**
     * @return {?}
     */
    get countOfAces() {
        if (this.isSoftCount) {
            /** @type {?} */
            const largerCount = 11 + (this.numOfAces - 1);
            if (largerCount + this.countWithoutAces <=
                21) {
                return largerCount;
            }
        }
        return this.numOfAces;
    }
    /**
     * @return {?}
     */
    get isSoftCount() {
        return !this.isBlackJack &&
            this.numOfAces > 0 &&
            (this.countWithoutAces +
                11 +
                (this.numOfAces - 1)) <= 21;
    }
    /**
     * @return {?}
     */
    get isBlackJack() {
        return !this.isSplit &&
            this.numOfCards == 2 &&
            this.numOfAces == 1 &&
            this.countWithoutAces == 10;
    }
    /**
     * @return {?}
     */
    get isFirstCardAnAce() {
        return this._isFirstCardAnAce;
    }
    /**
     * @return {?}
     */
    get numOfAces() {
        return this._numOfAces;
    }
    /**
     * @return {?}
     */
    get numOfCards() {
        return this.cards.length;
    }
    /**
     * @return {?}
     */
    get isAllCardsKnown() {
        return this._isAllCardsKnown;
    }
    /**
     * @return {?}
     */
    get cards() {
        return this._cards;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype._cards;
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype._numOfAces;
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype.countWithoutAces;
    /**
     * @type {?}
     * @protected
     */
    BlackjackHand.prototype._isAllCardsKnown;
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype._isFirstCardAnAce;
    /**
     * @type {?}
     * @protected
     */
    BlackjackHand.prototype.isSplit;
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype.soundPlayer;
}
export class BlackjackDealerHand extends BlackjackHand {
    /**
     * @param {?} cardId
     * @return {?}
     */
    dealFirstCard(cardId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield this.dealCard(cardId, true);
        });
    }
    /**
     * @return {?}
     */
    dealAnonymousCard() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // deal anonymous card for dealer's second card (at first)
            yield this.dealCard(0, false);
        });
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    revealSecondCard(cardId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.numOfCards > 1) {
                this.cards.pop();
            }
            yield this.dealCard(cardId, true);
            this._isAllCardsKnown = true;
        });
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    dealAdditionalCard(cardId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.shouldHit) {
                yield this.hit(cardId);
            }
        });
    }
    /**
     * @private
     * @return {?}
     */
    get shouldHit() {
        return this.isSoftCount ?
            this.sum <= 17 :
            this.sum < 17;
    }
}
export class BlackjackPlayerHand extends BlackjackHand {
    /**
     * @param {?} soundPlayer
     */
    constructor(soundPlayer) {
        super(soundPlayer);
        this.didDoubleDown = false;
        this._isWaitingForAction = false;
        this.bet = new BetAmount();
        this._boughtInsurance = false;
    }
    /**
     * @return {?}
     */
    reset() {
        super.reset();
        this.didDoubleDown = false;
        this._isWaitingForAction = false;
        this.bet.reset();
        this._boughtInsurance = false;
    }
    /**
     * @param {?} cardId
     * @param {?} betAmount
     * @return {?}
     */
    dealFirstCard(cardId, betAmount) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.bet.add(betAmount);
            yield this.hit(cardId);
        });
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    dealSecondCard(cardId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield this.hit(cardId);
            this._isAllCardsKnown = true;
            if (!this.isBlackJack) {
                this._isWaitingForAction = true;
            }
        });
    }
    /**
     * @return {?}
     */
    buyInsurance() {
        /*
        this.bet.add(
            this.bet.amount / 2
        );
        */
        this._boughtInsurance = true;
    }
    /**
     * @param {?} cards
     * @param {?} betAmount
     * @return {?}
     */
    split(cards, betAmount) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.reset();
            this.isSplit = true;
            this.bet.add(betAmount);
            yield this.dealCard(cards[0], true, true);
            /** @type {?} */
            const isFirstCardAnAce = this.numOfAces == 1;
            yield this.hit(cards[1]);
            if (isFirstCardAnAce) {
                this._isWaitingForAction = false;
            }
            this._isAllCardsKnown = true;
        });
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    doubleDown(cardId) {
        this.didDoubleDown = true;
        this.bet.double();
        return this.hit(cardId, true);
    }
    /**
     * @param {?} cardId
     * @param {?=} isDoubleDown
     * @return {?}
     */
    hit(cardId, isDoubleDown = false) {
        const _super = name => super[name];
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this._isWaitingForAction = false;
            yield _super("hit").call(this, cardId);
            if (this.numOfCards >= 2 &&
                !this.isBusted &&
                !isDoubleDown) {
                this._isWaitingForAction = true;
            }
        });
    }
    /**
     * @return {?}
     */
    stand() {
        this._isWaitingForAction = false;
    }
    /**
     * @param {?} dealer
     * @return {?}
     */
    determineResult(dealer) {
        /** @type {?} */
        const betAmount = this.bet.amount;
        /** @type {?} */
        let result;
        if (this.isBusted) {
            result = new BustedResult(betAmount);
        }
        else if (this.isBlackJack) {
            result = dealer.isBlackJack ?
                new PushResult(betAmount) :
                new BlackjackResult(betAmount);
        }
        else if (dealer.isBlackJack) {
            result = this.boughtInsurance ?
                new InsuranceSuccessResult(betAmount) :
                new LossResult(betAmount);
        }
        else if (dealer.isBusted) {
            result = new WinResult(betAmount);
        }
        else if (this.sum == dealer.sum) {
            result = new PushResult(betAmount);
        }
        else {
            result = this.sum > dealer.sum ?
                new WinResult(betAmount) :
                new LossResult(betAmount);
        }
        //this.bet.add(result.amountWon);
        return result;
    }
    /**
     * @return {?}
     */
    get isAbleToDoubleDown() {
        return this.numOfCards == 2 &&
            !this.didDoubleDown;
    }
    /**
     * @return {?}
     */
    get isAbleToSplit() {
        return this.numOfCards == 2 &&
            this.cards[0].card ==
                this.cards[1].card;
    }
    /**
     * @return {?}
     */
    get isWaitingForAction() {
        return this._isWaitingForAction;
    }
    /**
     * @return {?}
     */
    get boughtInsurance() {
        return this._boughtInsurance;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackPlayerHand.prototype.didDoubleDown;
    /**
     * @type {?}
     * @private
     */
    BlackjackPlayerHand.prototype._isWaitingForAction;
    /** @type {?} */
    BlackjackPlayerHand.prototype.bet;
    /**
     * @type {?}
     * @private
     */
    BlackjackPlayerHand.prototype._boughtInsurance;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWhhbmRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svY29tcG9uZW50cy9tYWluL2dhbWUvYmxhY2tqYWNrLWhhbmRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRW5FLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3pDLE9BQU8sRUFBb0IsWUFBWSxFQUFFLFVBQVUsRUFBRSxlQUFlLEVBQUUsc0JBQXNCLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkzSTs7OztJQVNJLFlBQW9CLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBUHBDLFdBQU0sR0FBb0IsRUFBRSxDQUFDO1FBQzdCLGVBQVUsR0FBRyxDQUFDLENBQUM7UUFDZixxQkFBZ0IsR0FBRyxDQUFDLENBQUM7UUFDbkIscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzNCLHNCQUFpQixHQUFHLEtBQUssQ0FBQztRQUN4QixZQUFPLEdBQUcsS0FBSyxDQUFDO0lBRzFCLENBQUM7Ozs7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQzs7Ozs7SUFFRCxHQUFHLENBQUMsTUFBYTtRQUNiLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Ozs7Ozs7SUFFZSxRQUFRLENBQ3BCLE1BQWEsRUFBQyxZQUFvQixFQUNsQyxpQkFBaUIsR0FBRyxLQUFLOzs7a0JBRW5CLElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ2pELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOztrQkFFaEIsVUFBVSxHQUFHLE1BQU0sR0FBRyxFQUFFOztrQkFFeEIsS0FBSyxHQUFHLFVBQVUsSUFBSSxjQUFjLENBQUMsR0FBRztZQUU5QyxJQUFJLENBQUMsTUFBTSxDQUNQO2dCQUNJLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7Z0JBQzdCLFFBQVEsRUFBRSxVQUFVLENBQUMsVUFBVSxDQUFDO2FBQ25DLEVBQ0QsVUFBVSxFQUNWLE1BQU0sQ0FDVCxDQUFDO1lBRUYsSUFBSSxDQUFDLGlCQUFpQixFQUFFO2dCQUNwQixNQUFNLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNwQjtZQUVELElBQUksWUFBWSxFQUFFO2dCQUNkLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUN0QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDZjtZQUVELElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDcEIsTUFBTSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDcEI7WUFFRCxJQUFJLFlBQVksRUFBRTtnQkFDZCxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBQyxLQUFLLENBQUMsQ0FBQzthQUN6QztRQUNMLENBQUM7S0FBQTs7Ozs7OztJQUNPLGNBQWMsQ0FBQyxVQUFpQixFQUFDLEtBQWE7O1lBQzlDLFdBQVcsR0FBRyxDQUFDO1FBRW5CLFFBQVEsVUFBVSxFQUFFO1lBQ2hCLEtBQUssY0FBYyxDQUFDLElBQUksQ0FBQztZQUN6QixLQUFLLGNBQWMsQ0FBQyxLQUFLLENBQUM7WUFDMUIsS0FBSyxjQUFjLENBQUMsSUFBSTtnQkFDcEIsV0FBVyxHQUFHLEVBQUUsQ0FBQztnQkFDakIsTUFBTTtZQUVWLEtBQUssY0FBYyxDQUFDLEdBQUc7Z0JBQ25CLGlDQUFpQztnQkFDakMsTUFBTTtZQUVWO2dCQUNJLFdBQVcsR0FBRyxVQUFVLENBQUM7Z0JBQ3pCLE1BQU07U0FDYjtRQUVELElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxXQUFXLENBQUM7UUFFckMsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFFbEIsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsRUFBRTtnQkFDdEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQzthQUNqQztTQUNKO0lBQ0wsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELElBQUksSUFBSTtRQUNKLE9BQU8sSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUM7SUFDMUIsQ0FBQzs7OztJQUVELElBQUksV0FBVzs7WUFDUCxHQUFHLEdBQUcsRUFBRSxHQUFDLElBQUksQ0FBQyxHQUFHO1FBRXJCLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixHQUFHLElBQUssR0FBRztnQkFDUCxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDaEQ7UUFBQSxDQUFDO1FBRUYsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDOzs7O0lBRUQsSUFBSSxHQUFHOztZQUNDLEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVc7UUFFbEQsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ2xCLEdBQUcsR0FBRyxFQUFFLENBQUM7U0FDWjtRQUVELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVELElBQUksV0FBVztRQUNYLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTs7a0JBQ1osV0FBVyxHQUFHLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1lBRTdDLElBQ0ksV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0I7Z0JBQ25DLEVBQUUsRUFDSjtnQkFDRSxPQUFPLFdBQVcsQ0FBQzthQUN0QjtTQUNKO1FBRUQsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7Ozs7SUFFRCxJQUFJLFdBQVc7UUFDWCxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVc7WUFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDO1lBQ3RCLENBQ0ksSUFBSSxDQUFDLGdCQUFnQjtnQkFDckIsRUFBRTtnQkFDRixDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQ3ZCLElBQUksRUFBRSxDQUFDO0lBQ1osQ0FBQzs7OztJQUVELElBQUksV0FBVztRQUNYLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTztZQUNaLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQztZQUNwQixJQUFJLENBQUMsU0FBUyxJQUFJLENBQUM7WUFDbkIsSUFBSSxDQUFDLGdCQUFnQixJQUFJLEVBQUUsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUQsSUFBSSxnQkFBZ0I7UUFDaEIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVELElBQUksU0FBUztRQUNULE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDOzs7O0lBRUQsSUFBSSxVQUFVO1FBQ1YsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUM3QixDQUFDOzs7O0lBRUQsSUFBSSxlQUFlO1FBQ2YsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDakMsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0NBQ0o7Ozs7OztJQTNLRywrQkFBcUM7Ozs7O0lBQ3JDLG1DQUF1Qjs7Ozs7SUFDdkIseUNBQTZCOzs7OztJQUM3Qix5Q0FBbUM7Ozs7O0lBQ25DLDBDQUFrQzs7Ozs7SUFDbEMsZ0NBQTBCOzs7OztJQUVkLG9DQUFnQzs7QUFzS2hELE1BQU0sMEJBQTJCLFNBQVEsYUFBYTs7Ozs7SUFFNUMsYUFBYSxDQUFDLE1BQWE7O1lBQzdCLE1BQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsQ0FBQztLQUFBOzs7O0lBRUssaUJBQWlCOztZQUNuQiwwREFBMEQ7WUFDMUQsTUFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBQyxLQUFLLENBQUMsQ0FBQztRQUNqQyxDQUFDO0tBQUE7Ozs7O0lBRUssZ0JBQWdCLENBQUMsTUFBYTs7WUFDaEMsSUFBSSxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsRUFBRTtnQkFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUNwQjtZQUVELE1BQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLENBQUM7WUFFakMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUNqQyxDQUFDO0tBQUE7Ozs7O0lBRUssa0JBQWtCLENBQUMsTUFBYTs7WUFDbEMsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNoQixNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDMUI7UUFDTCxDQUFDO0tBQUE7Ozs7O0lBRUQsSUFBWSxTQUFTO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2pCLElBQUksQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQztDQUNKO0FBRUQsTUFBTSwwQkFBMkIsU0FBUSxhQUFhOzs7O0lBT2xELFlBQVksV0FBd0I7UUFDaEMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBTmYsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsd0JBQW1CLEdBQUcsS0FBSyxDQUFDO1FBQzNCLFFBQUcsR0FBRyxJQUFJLFNBQVMsRUFBRSxDQUFDO1FBQ3ZCLHFCQUFnQixHQUFHLEtBQUssQ0FBQztJQUlqQyxDQUFDOzs7O0lBRUQsS0FBSztRQUNELEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUVkLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDakMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO0lBQ2xDLENBQUM7Ozs7OztJQUVLLGFBQWEsQ0FBQyxNQUFhLEVBQUMsU0FBZ0I7O1lBQzlDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRXhCLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMzQixDQUFDO0tBQUE7Ozs7O0lBQ0ssY0FBYyxDQUFDLE1BQWE7O1lBQzlCLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUV2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1lBRTdCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNuQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO2FBQ25DO1FBQ0wsQ0FBQztLQUFBOzs7O0lBRUQsWUFBWTtRQUNSOzs7O1VBSUU7UUFDRixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7Ozs7OztJQUVLLEtBQUssQ0FBQyxLQUFjLEVBQUMsU0FBZ0I7O1lBQ3ZDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUViLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBRXBCLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRXhCLE1BQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxDQUFDOztrQkFFbEMsZ0JBQWdCLEdBQ2xCLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQztZQUV2QixNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFekIsSUFBSSxnQkFBZ0IsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQzthQUNwQztZQUVELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDakMsQ0FBQztLQUFBOzs7OztJQUdELFVBQVUsQ0FBQyxNQUFhO1FBQ3BCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7UUFFbEIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7Ozs7SUFFSyxHQUFHLENBQUMsTUFBYSxFQUFDLFlBQVksR0FBRyxLQUFLOzs7WUFDeEMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztZQUVqQyxNQUFNLGFBQVMsWUFBQyxNQUFNLENBQUMsQ0FBQztZQUV4QixJQUNJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQztnQkFDcEIsQ0FBQyxJQUFJLENBQUMsUUFBUTtnQkFDZCxDQUFDLFlBQVksRUFDZjtnQkFDRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO2FBQ25DO1FBQ0wsQ0FBQztLQUFBOzs7O0lBRUQsS0FBSztRQUNELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7SUFDckMsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsTUFBMEI7O2NBQ2hDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU07O1lBRTdCLE1BQXVCO1FBRTNCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNmLE1BQU0sR0FBRyxJQUFJLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN4QzthQUNJLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUN2QixNQUFNLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNyQixJQUFJLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixJQUFJLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUMxQzthQUNJLElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRTtZQUN6QixNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN2QixJQUFJLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZDLElBQUksVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3JDO2FBQ0ksSUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO1lBQ3RCLE1BQU0sR0FBRyxJQUFJLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNyQzthQUNJLElBQUksSUFBSSxDQUFDLEdBQUcsSUFBSSxNQUFNLENBQUMsR0FBRyxFQUFFO1lBQzdCLE1BQU0sR0FBRyxJQUFJLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN0QzthQUNJO1lBQ0QsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM1QixJQUFJLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUMxQixJQUFJLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNqQztRQUVELGlDQUFpQztRQUVqQyxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7O0lBR0QsSUFBSSxrQkFBa0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUM7WUFDbkIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQ2hDLENBQUM7Ozs7SUFFRCxJQUFJLGFBQWE7UUFDYixPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQztZQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7Z0JBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxJQUFJLGtCQUFrQjtRQUNsQixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztJQUNwQyxDQUFDOzs7O0lBRUQsSUFBSSxlQUFlO1FBQ2YsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDakMsQ0FBQztDQUNKOzs7Ozs7SUEvSUcsNENBQThCOzs7OztJQUM5QixrREFBb0M7O0lBQ3BDLGtDQUErQjs7Ozs7SUFDL0IsK0NBQWlDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgbmFtZU9mUmFuaywgc2xlZXAsIENhcmRSYW5rTnVtYmVyIH0gZnJvbSBcImVhcm5iZXQtY29tbW9uXCI7XG5cbmltcG9ydCB7IENhcmRDb250cm9sbGVyIH0gZnJvbSAnLi4vLi4vLi4vLi4vX2NvbW1vbi9nYW1lL2NhcmQtY29udHJvbGxlcic7XG5pbXBvcnQgeyBCZXRBbW91bnQgfSBmcm9tICcuL2JldC1hbW91bnQnO1xuaW1wb3J0IHsgSUJsYWNramFja1Jlc3VsdCwgQnVzdGVkUmVzdWx0LCBQdXNoUmVzdWx0LCBCbGFja2phY2tSZXN1bHQsIEluc3VyYW5jZVN1Y2Nlc3NSZXN1bHQsIExvc3NSZXN1bHQsIFdpblJlc3VsdCB9IGZyb20gJy4vaGFuZC1yZXN1bHQnO1xuaW1wb3J0IHsgSVNvdW5kUGxheWVyIH0gZnJvbSAnLi4vLi4vLi4vLi4vX2NvbW1vbi9tZWRpYS9pbnRlcmZhY2VzJztcblxuXG5jbGFzcyBCbGFja2phY2tIYW5kXG57XG4gICAgcHJpdmF0ZSBfY2FyZHM6Q2FyZENvbnRyb2xsZXJbXSA9IFtdO1xuICAgIHByaXZhdGUgX251bU9mQWNlcyA9IDA7XG4gICAgcHJpdmF0ZSBjb3VudFdpdGhvdXRBY2VzID0gMDtcbiAgICBwcm90ZWN0ZWQgX2lzQWxsQ2FyZHNLbm93biA9IGZhbHNlO1xuICAgIHByaXZhdGUgX2lzRmlyc3RDYXJkQW5BY2UgPSBmYWxzZTtcbiAgICBwcm90ZWN0ZWQgaXNTcGxpdCA9IGZhbHNlO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBzb3VuZFBsYXllcjpJU291bmRQbGF5ZXIpIHtcbiAgICB9XG5cbiAgICByZXNldCgpIHtcbiAgICAgICAgdGhpcy5fY2FyZHMgPSBbXTtcbiAgICAgICAgdGhpcy5fbnVtT2ZBY2VzID0gMDtcbiAgICAgICAgdGhpcy5jb3VudFdpdGhvdXRBY2VzID0gMDtcbiAgICAgICAgdGhpcy5faXNBbGxDYXJkc0tub3duID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2lzRmlyc3RDYXJkQW5BY2UgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5pc1NwbGl0ID0gZmFsc2U7XG4gICAgfVxuXG4gICAgaGl0KGNhcmRJZDpudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVhbENhcmQoY2FyZElkLHRydWUpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBhc3luYyBkZWFsQ2FyZChcbiAgICAgICAgY2FyZElkOm51bWJlcixzaG91bGRSZXZlYWw6Ym9vbGVhbixcbiAgICAgICAgcmV2ZWFsSW1tZWRpYXRlbHkgPSBmYWxzZVxuICAgICkge1xuICAgICAgICBjb25zdCBjYXJkID0gbmV3IENhcmRDb250cm9sbGVyKHRoaXMuc291bmRQbGF5ZXIpO1xuICAgICAgICB0aGlzLmNhcmRzLnB1c2goY2FyZCk7XG5cbiAgICAgICAgY29uc3QgcmFua051bWJlciA9IGNhcmRJZCAlIDEzO1xuXG4gICAgICAgIGNvbnN0IGlzQWNlID0gcmFua051bWJlciA9PSBDYXJkUmFua051bWJlci5BQ0U7XG5cbiAgICAgICAgY2FyZC5yZXZlYWwoXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgc3VpdDogTWF0aC5mbG9vcihjYXJkSWQgLyAxMyksXG4gICAgICAgICAgICAgICAgcmFua05hbWU6IG5hbWVPZlJhbmsocmFua051bWJlcilcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICByYW5rTnVtYmVyLFxuICAgICAgICAgICAgY2FyZElkXG4gICAgICAgICk7XG5cbiAgICAgICAgaWYgKCFyZXZlYWxJbW1lZGlhdGVseSkge1xuICAgICAgICAgICAgYXdhaXQgc2xlZXAoMjUwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChzaG91bGRSZXZlYWwpIHtcbiAgICAgICAgICAgIGNhcmQuZGVhbEFuZEZsaXAoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNhcmQuZGVhbCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFyZXZlYWxJbW1lZGlhdGVseSkge1xuICAgICAgICAgICAgYXdhaXQgc2xlZXAoMjUwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChzaG91bGRSZXZlYWwpIHtcbiAgICAgICAgICAgIHRoaXMuYWRkVmFsdWVPZkNhcmQocmFua051bWJlcixpc0FjZSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcHJpdmF0ZSBhZGRWYWx1ZU9mQ2FyZChyYW5rTnVtYmVyOm51bWJlcixpc0FjZTpib29sZWFuKSB7XG4gICAgICAgIGxldCB2YWx1ZU9mQ2FyZCA9IDA7XG5cbiAgICAgICAgc3dpdGNoIChyYW5rTnVtYmVyKSB7XG4gICAgICAgICAgICBjYXNlIENhcmRSYW5rTnVtYmVyLkpBQ0s6XG4gICAgICAgICAgICBjYXNlIENhcmRSYW5rTnVtYmVyLlFVRUVOOlxuICAgICAgICAgICAgY2FzZSBDYXJkUmFua051bWJlci5LSU5HOlxuICAgICAgICAgICAgICAgIHZhbHVlT2ZDYXJkID0gMTA7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgQ2FyZFJhbmtOdW1iZXIuQUNFOlxuICAgICAgICAgICAgICAgIC8vIHdpbGwgZGV0ZXJtaW5lIGFjZSB2YWx1ZSBsYXRlclxuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHZhbHVlT2ZDYXJkID0gcmFua051bWJlcjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuY291bnRXaXRob3V0QWNlcyArPSB2YWx1ZU9mQ2FyZDtcblxuICAgICAgICBpZiAoaXNBY2UpIHtcbiAgICAgICAgICAgIHRoaXMuX251bU9mQWNlcysrO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5udW1PZkNhcmRzID09IDEpIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9pc0ZpcnN0Q2FyZEFuQWNlID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldCBpc0J1c3RlZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3VtID4gMjE7XG4gICAgfVxuXG4gICAgZ2V0IGlzMjEoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN1bSA9PSAyMTtcbiAgICB9XG5cbiAgICBnZXQgc3VtQXNTdHJpbmcoKTpzdHJpbmcge1xuICAgICAgICBsZXQgc3VtID0gJycrdGhpcy5zdW07XG5cbiAgICAgICAgaWYgKHRoaXMuaXNTb2Z0Q291bnQpIHtcbiAgICAgICAgICAgIHN1bSArPSAgJy8nICtcbiAgICAgICAgICAgICAgICAodGhpcy5jb3VudFdpdGhvdXRBY2VzICsgdGhpcy5udW1PZkFjZXMpO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBzdW07XG4gICAgfVxuXG4gICAgZ2V0IHN1bSgpOm51bWJlciB7XG4gICAgICAgIGxldCBzdW0gPSB0aGlzLmNvdW50V2l0aG91dEFjZXMgKyB0aGlzLmNvdW50T2ZBY2VzO1xuXG4gICAgICAgIGlmICh0aGlzLmlzQmxhY2tKYWNrKSB7XG4gICAgICAgICAgICBzdW0gPSAyMTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBzdW07XG4gICAgfVxuXG4gICAgZ2V0IGNvdW50T2ZBY2VzKCk6bnVtYmVyIHtcbiAgICAgICAgaWYgKHRoaXMuaXNTb2Z0Q291bnQpIHtcbiAgICAgICAgICAgIGNvbnN0IGxhcmdlckNvdW50ID0gMTEgKyAodGhpcy5udW1PZkFjZXMgLSAxKTtcblxuICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICAgIGxhcmdlckNvdW50ICsgdGhpcy5jb3VudFdpdGhvdXRBY2VzIDw9XG4gICAgICAgICAgICAgICAgMjFcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgIHJldHVybiBsYXJnZXJDb3VudDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLm51bU9mQWNlcztcbiAgICB9XG5cbiAgICBnZXQgaXNTb2Z0Q291bnQoKSB7XG4gICAgICAgIHJldHVybiAhdGhpcy5pc0JsYWNrSmFjayAmJlxuICAgICAgICAgICAgdGhpcy5udW1PZkFjZXMgPiAwICYmXG4gICAgICAgIChcbiAgICAgICAgICAgIHRoaXMuY291bnRXaXRob3V0QWNlcyArXG4gICAgICAgICAgICAxMSArXG4gICAgICAgICAgICAodGhpcy5udW1PZkFjZXMgLSAxKVxuICAgICAgICApIDw9IDIxO1xuICAgIH1cblxuICAgIGdldCBpc0JsYWNrSmFjaygpIHtcbiAgICAgICAgcmV0dXJuICF0aGlzLmlzU3BsaXQgJiZcbiAgICAgICAgICAgICAgICB0aGlzLm51bU9mQ2FyZHMgPT0gMiAmJlxuICAgICAgICAgICAgICAgIHRoaXMubnVtT2ZBY2VzID09IDEgJiZcbiAgICAgICAgICAgICAgICB0aGlzLmNvdW50V2l0aG91dEFjZXMgPT0gMTA7XG4gICAgfVxuXG4gICAgZ2V0IGlzRmlyc3RDYXJkQW5BY2UoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc0ZpcnN0Q2FyZEFuQWNlO1xuICAgIH1cblxuICAgIGdldCBudW1PZkFjZXMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9udW1PZkFjZXM7XG4gICAgfVxuXG4gICAgZ2V0IG51bU9mQ2FyZHMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNhcmRzLmxlbmd0aDtcbiAgICB9XG5cbiAgICBnZXQgaXNBbGxDYXJkc0tub3duKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faXNBbGxDYXJkc0tub3duO1xuICAgIH1cblxuICAgIGdldCBjYXJkcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NhcmRzO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEJsYWNramFja0RlYWxlckhhbmQgZXh0ZW5kcyBCbGFja2phY2tIYW5kXG57XG4gICAgYXN5bmMgZGVhbEZpcnN0Q2FyZChjYXJkSWQ6bnVtYmVyKSB7XG4gICAgICAgIGF3YWl0IHRoaXMuZGVhbENhcmQoY2FyZElkLHRydWUpO1xuICAgIH1cblxuICAgIGFzeW5jIGRlYWxBbm9ueW1vdXNDYXJkKCkge1xuICAgICAgICAvLyBkZWFsIGFub255bW91cyBjYXJkIGZvciBkZWFsZXIncyBzZWNvbmQgY2FyZCAoYXQgZmlyc3QpXG4gICAgICAgIGF3YWl0IHRoaXMuZGVhbENhcmQoMCxmYWxzZSk7XG4gICAgfVxuXG4gICAgYXN5bmMgcmV2ZWFsU2Vjb25kQ2FyZChjYXJkSWQ6bnVtYmVyKSB7XG4gICAgICAgIGlmICh0aGlzLm51bU9mQ2FyZHMgPiAxKSB7XG4gICAgICAgICAgICB0aGlzLmNhcmRzLnBvcCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgYXdhaXQgdGhpcy5kZWFsQ2FyZChjYXJkSWQsdHJ1ZSk7XG5cbiAgICAgICAgdGhpcy5faXNBbGxDYXJkc0tub3duID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBhc3luYyBkZWFsQWRkaXRpb25hbENhcmQoY2FyZElkOm51bWJlcikge1xuICAgICAgICBpZiAodGhpcy5zaG91bGRIaXQpIHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuaGl0KGNhcmRJZCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCBzaG91bGRIaXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzU29mdENvdW50ID9cbiAgICAgICAgICAgICAgICB0aGlzLnN1bSA8PSAxNyA6XG4gICAgICAgICAgICAgICAgdGhpcy5zdW0gPCAxNztcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBCbGFja2phY2tQbGF5ZXJIYW5kIGV4dGVuZHMgQmxhY2tqYWNrSGFuZFxue1xuICAgIHByaXZhdGUgZGlkRG91YmxlRG93biA9IGZhbHNlO1xuICAgIHByaXZhdGUgX2lzV2FpdGluZ0ZvckFjdGlvbiA9IGZhbHNlO1xuICAgIHJlYWRvbmx5IGJldCA9IG5ldyBCZXRBbW91bnQoKTtcbiAgICBwcml2YXRlIF9ib3VnaHRJbnN1cmFuY2UgPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKHNvdW5kUGxheWVyOklTb3VuZFBsYXllcikge1xuICAgICAgICBzdXBlcihzb3VuZFBsYXllcik7XG4gICAgfVxuXG4gICAgcmVzZXQoKSB7XG4gICAgICAgIHN1cGVyLnJlc2V0KCk7XG5cbiAgICAgICAgdGhpcy5kaWREb3VibGVEb3duID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvckFjdGlvbiA9IGZhbHNlO1xuICAgICAgICB0aGlzLmJldC5yZXNldCgpO1xuICAgICAgICB0aGlzLl9ib3VnaHRJbnN1cmFuY2UgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBhc3luYyBkZWFsRmlyc3RDYXJkKGNhcmRJZDpudW1iZXIsYmV0QW1vdW50Om51bWJlcikge1xuICAgICAgICB0aGlzLmJldC5hZGQoYmV0QW1vdW50KTtcblxuICAgICAgICBhd2FpdCB0aGlzLmhpdChjYXJkSWQpO1xuICAgIH1cbiAgICBhc3luYyBkZWFsU2Vjb25kQ2FyZChjYXJkSWQ6bnVtYmVyKSB7XG4gICAgICAgIGF3YWl0IHRoaXMuaGl0KGNhcmRJZCk7XG5cbiAgICAgICAgdGhpcy5faXNBbGxDYXJkc0tub3duID0gdHJ1ZTtcblxuICAgICAgICBpZiAoIXRoaXMuaXNCbGFja0phY2spIHtcbiAgICAgICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvckFjdGlvbiA9IHRydWU7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBidXlJbnN1cmFuY2UoKSB7XG4gICAgICAgIC8qXG4gICAgICAgIHRoaXMuYmV0LmFkZChcbiAgICAgICAgICAgIHRoaXMuYmV0LmFtb3VudCAvIDJcbiAgICAgICAgKTtcbiAgICAgICAgKi9cbiAgICAgICAgdGhpcy5fYm91Z2h0SW5zdXJhbmNlID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBhc3luYyBzcGxpdChjYXJkczpudW1iZXJbXSxiZXRBbW91bnQ6bnVtYmVyKSB7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcblxuICAgICAgICB0aGlzLmlzU3BsaXQgPSB0cnVlO1xuXG4gICAgICAgIHRoaXMuYmV0LmFkZChiZXRBbW91bnQpO1xuXG4gICAgICAgIGF3YWl0IHRoaXMuZGVhbENhcmQoY2FyZHNbMF0sdHJ1ZSx0cnVlKTtcblxuICAgICAgICBjb25zdCBpc0ZpcnN0Q2FyZEFuQWNlID1cbiAgICAgICAgICAgIHRoaXMubnVtT2ZBY2VzID09IDE7XG5cbiAgICAgICAgYXdhaXQgdGhpcy5oaXQoY2FyZHNbMV0pO1xuXG4gICAgICAgIGlmIChpc0ZpcnN0Q2FyZEFuQWNlKSB7XG4gICAgICAgICAgICB0aGlzLl9pc1dhaXRpbmdGb3JBY3Rpb24gPSBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuX2lzQWxsQ2FyZHNLbm93biA9IHRydWU7XG4gICAgfVxuXG5cbiAgICBkb3VibGVEb3duKGNhcmRJZDpudW1iZXIpIHtcbiAgICAgICAgdGhpcy5kaWREb3VibGVEb3duID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5iZXQuZG91YmxlKCk7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuaGl0KGNhcmRJZCx0cnVlKTtcbiAgICB9XG5cbiAgICBhc3luYyBoaXQoY2FyZElkOm51bWJlcixpc0RvdWJsZURvd24gPSBmYWxzZSkge1xuICAgICAgICB0aGlzLl9pc1dhaXRpbmdGb3JBY3Rpb24gPSBmYWxzZTtcblxuICAgICAgICBhd2FpdCBzdXBlci5oaXQoY2FyZElkKTtcblxuICAgICAgICBpZiAoXG4gICAgICAgICAgICB0aGlzLm51bU9mQ2FyZHMgPj0gMiAmJlxuICAgICAgICAgICAgIXRoaXMuaXNCdXN0ZWQgJiZcbiAgICAgICAgICAgICFpc0RvdWJsZURvd25cbiAgICAgICAgKSB7XG4gICAgICAgICAgICB0aGlzLl9pc1dhaXRpbmdGb3JBY3Rpb24gPSB0cnVlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc3RhbmQoKSB7XG4gICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvckFjdGlvbiA9IGZhbHNlO1xuICAgIH1cblxuICAgIGRldGVybWluZVJlc3VsdChkZWFsZXI6QmxhY2tqYWNrRGVhbGVySGFuZCk6SUJsYWNramFja1Jlc3VsdCB7XG4gICAgICAgIGNvbnN0IGJldEFtb3VudCA9IHRoaXMuYmV0LmFtb3VudDtcblxuICAgICAgICBsZXQgcmVzdWx0OklCbGFja2phY2tSZXN1bHQ7XG5cbiAgICAgICAgaWYgKHRoaXMuaXNCdXN0ZWQpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IG5ldyBCdXN0ZWRSZXN1bHQoYmV0QW1vdW50KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLmlzQmxhY2tKYWNrKSB7XG4gICAgICAgICAgICByZXN1bHQgPSBkZWFsZXIuaXNCbGFja0phY2sgP1xuICAgICAgICAgICAgICAgICAgICBuZXcgUHVzaFJlc3VsdChiZXRBbW91bnQpIDpcbiAgICAgICAgICAgICAgICAgICAgbmV3IEJsYWNramFja1Jlc3VsdChiZXRBbW91bnQpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKGRlYWxlci5pc0JsYWNrSmFjaykge1xuICAgICAgICAgICAgcmVzdWx0ID0gdGhpcy5ib3VnaHRJbnN1cmFuY2UgP1xuICAgICAgICAgICAgICAgICAgICBuZXcgSW5zdXJhbmNlU3VjY2Vzc1Jlc3VsdChiZXRBbW91bnQpIDpcbiAgICAgICAgICAgICAgICAgICAgbmV3IExvc3NSZXN1bHQoYmV0QW1vdW50KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChkZWFsZXIuaXNCdXN0ZWQpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IG5ldyBXaW5SZXN1bHQoYmV0QW1vdW50KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLnN1bSA9PSBkZWFsZXIuc3VtKSB7XG4gICAgICAgICAgICByZXN1bHQgPSBuZXcgUHVzaFJlc3VsdChiZXRBbW91bnQpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmVzdWx0ID0gdGhpcy5zdW0gPiBkZWFsZXIuc3VtID9cbiAgICAgICAgICAgICAgICBuZXcgV2luUmVzdWx0KGJldEFtb3VudCkgOlxuICAgICAgICAgICAgICAgIG5ldyBMb3NzUmVzdWx0KGJldEFtb3VudCk7XG4gICAgICAgIH1cblxuICAgICAgICAvL3RoaXMuYmV0LmFkZChyZXN1bHQuYW1vdW50V29uKTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH1cblxuXG4gICAgZ2V0IGlzQWJsZVRvRG91YmxlRG93bigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubnVtT2ZDYXJkcyA9PSAyICYmXG4gICAgICAgICAgICAgICAgIXRoaXMuZGlkRG91YmxlRG93bjtcbiAgICB9XG5cbiAgICBnZXQgaXNBYmxlVG9TcGxpdCgpOmJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5udW1PZkNhcmRzID09IDIgJiZcbiAgICAgICAgICAgICAgICB0aGlzLmNhcmRzWzBdLmNhcmQgPT1cbiAgICAgICAgICAgICAgICB0aGlzLmNhcmRzWzFdLmNhcmQ7XG4gICAgfVxuXG4gICAgZ2V0IGlzV2FpdGluZ0ZvckFjdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzV2FpdGluZ0ZvckFjdGlvbjtcbiAgICB9XG5cbiAgICBnZXQgYm91Z2h0SW5zdXJhbmNlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fYm91Z2h0SW5zdXJhbmNlO1xuICAgIH1cbn1cbiJdfQ==