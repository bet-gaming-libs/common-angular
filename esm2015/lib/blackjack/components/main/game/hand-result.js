/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/hand-result.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IBlackjackResult() { }
if (false) {
    /** @type {?} */
    IBlackjackResult.prototype.isBlackjack;
    /** @type {?} */
    IBlackjackResult.prototype.isWin;
    /** @type {?} */
    IBlackjackResult.prototype.isPush;
    /** @type {?} */
    IBlackjackResult.prototype.isLoss;
    /** @type {?} */
    IBlackjackResult.prototype.isBusted;
    /** @type {?} */
    IBlackjackResult.prototype.isInsuranceSuccess;
    /** @type {?} */
    IBlackjackResult.prototype.value;
    /** @type {?} */
    IBlackjackResult.prototype.amountWon;
}
/**
 * @abstract
 */
class HandResultBase {
    /**
     * @param {?} value
     * @param {?} betAmount
     */
    constructor(value, betAmount) {
        this.value = value;
        this.amountWon = value * betAmount;
    }
    /**
     * @return {?}
     */
    get isBlackjack() {
        return false;
    }
    /**
     * @return {?}
     */
    get isWin() {
        return false;
    }
    /**
     * @return {?}
     */
    get isPush() {
        return false;
    }
    /**
     * @return {?}
     */
    get isLoss() {
        return false;
    }
    /**
     * @return {?}
     */
    get isBusted() {
        return false;
    }
    /**
     * @return {?}
     */
    get isInsuranceSuccess() {
        return false;
    }
}
if (false) {
    /** @type {?} */
    HandResultBase.prototype.amountWon;
    /** @type {?} */
    HandResultBase.prototype.value;
}
export class BlackjackResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(1.5, betAmount);
    }
    /**
     * @return {?}
     */
    get isBlackjack() {
        return true;
    }
    /**
     * @return {?}
     */
    get isWin() {
        return true;
    }
}
export class WinResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(1, betAmount);
    }
    /**
     * @return {?}
     */
    get isWin() {
        return true;
    }
}
export class PushResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(0, betAmount);
    }
    /**
     * @return {?}
     */
    get isPush() {
        return true;
    }
}
export class LossResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(-1, betAmount);
    }
    /**
     * @return {?}
     */
    get isLoss() {
        return true;
    }
}
export class BustedResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(-1, betAmount);
    }
    /**
     * @return {?}
     */
    get isBusted() {
        return true;
    }
    /**
     * @return {?}
     */
    get isLoss() {
        return true;
    }
}
export class InsuranceSuccessResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(0, betAmount);
    }
    /**
     * @return {?}
     */
    get isInsuranceSuccess() {
        return true;
    }
}
export class CombinedHandsResult {
    /**
     * @param {?} result1
     * @param {?} result2
     */
    constructor(result1, result2) {
        this.isInsuranceSuccess = false;
        this.value = result1.value + result2.value;
        this.amountWon = result1.amountWon + result2.amountWon;
        this.isPush = this.amountWon == 0;
        this.isBlackjack = this.value >= 1.5;
        this.isWin = !this.isBlackjack && this.value > 0;
        this.isBusted = result1.isBusted && result2.isBusted;
        this.isLoss = !this.isBusted && this.value < 0;
    }
}
if (false) {
    /** @type {?} */
    CombinedHandsResult.prototype.isInsuranceSuccess;
    /** @type {?} */
    CombinedHandsResult.prototype.value;
    /** @type {?} */
    CombinedHandsResult.prototype.amountWon;
    /** @type {?} */
    CombinedHandsResult.prototype.isPush;
    /** @type {?} */
    CombinedHandsResult.prototype.isBlackjack;
    /** @type {?} */
    CombinedHandsResult.prototype.isWin;
    /** @type {?} */
    CombinedHandsResult.prototype.isBusted;
    /** @type {?} */
    CombinedHandsResult.prototype.isLoss;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGFuZC1yZXN1bHQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL2JsYWNramFjay9jb21wb25lbnRzL21haW4vZ2FtZS9oYW5kLXJlc3VsdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLHNDQVdDOzs7SUFURyx1Q0FBb0I7O0lBQ3BCLGlDQUFjOztJQUNkLGtDQUFlOztJQUNmLGtDQUFlOztJQUNmLG9DQUFpQjs7SUFDakIsOENBQTJCOztJQUUzQixpQ0FBYTs7SUFDYixxQ0FBaUI7Ozs7O0FBSXJCOzs7OztJQUlJLFlBQ2EsS0FBWSxFQUNyQixTQUFnQjtRQURQLFVBQUssR0FBTCxLQUFLLENBQU87UUFHckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLEdBQUcsU0FBUyxDQUFDO0lBQ3ZDLENBQUM7Ozs7SUFFRCxJQUFJLFdBQVc7UUFDWCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7O0lBQ0QsSUFBSSxLQUFLO1FBQ0wsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUNELElBQUksTUFBTTtRQUNOLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7SUFDRCxJQUFJLE1BQU07UUFDTixPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7O0lBQ0QsSUFBSSxRQUFRO1FBQ1IsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUNELElBQUksa0JBQWtCO1FBQ2xCLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Q0FDSjs7O0lBM0JHLG1DQUEwQjs7SUFHdEIsK0JBQXFCOztBQTBCN0IsTUFBTSxzQkFBdUIsU0FBUSxjQUFjOzs7O0lBRS9DLFlBQVksU0FBZ0I7UUFDeEIsS0FBSyxDQUFDLEdBQUcsRUFBQyxTQUFTLENBQUMsQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsSUFBSSxXQUFXO1FBQ1gsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7OztJQUNELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Q0FDSjtBQUVELE1BQU0sZ0JBQWlCLFNBQVEsY0FBYzs7OztJQUV6QyxZQUFZLFNBQWdCO1FBQ3hCLEtBQUssQ0FBQyxDQUFDLEVBQUMsU0FBUyxDQUFDLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Q0FDSjtBQUVELE1BQU0saUJBQWtCLFNBQVEsY0FBYzs7OztJQUUxQyxZQUFZLFNBQWdCO1FBQ3hCLEtBQUssQ0FBQyxDQUFDLEVBQUMsU0FBUyxDQUFDLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Q0FDSjtBQUVELE1BQU0saUJBQWtCLFNBQVEsY0FBYzs7OztJQUUxQyxZQUFZLFNBQWdCO1FBQ3hCLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBQyxTQUFTLENBQUMsQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsSUFBSSxNQUFNO1FBQ04sT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKO0FBRUQsTUFBTSxtQkFBb0IsU0FBUSxjQUFjOzs7O0lBRTVDLFlBQVksU0FBZ0I7UUFDeEIsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7Ozs7SUFFRCxJQUFJLFFBQVE7UUFDUixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7O0lBQ0QsSUFBSSxNQUFNO1FBQ04sT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKO0FBRUQsTUFBTSw2QkFBOEIsU0FBUSxjQUFjOzs7O0lBRXRELFlBQVksU0FBZ0I7UUFDeEIsS0FBSyxDQUFDLENBQUMsRUFBQyxTQUFTLENBQUMsQ0FBQztJQUN2QixDQUFDOzs7O0lBRUQsSUFBSSxrQkFBa0I7UUFDbEIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKO0FBRUQsTUFBTTs7Ozs7SUFhRixZQUNJLE9BQXdCLEVBQ3hCLE9BQXdCO1FBYm5CLHVCQUFrQixHQUFHLEtBQUssQ0FBQztRQWVoQyxJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUUzQyxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQztRQUV2RCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxHQUFHLENBQUM7UUFDckMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUM7UUFDckQsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDbkQsQ0FBQztDQUNKOzs7SUF6QkcsaURBQW9DOztJQUVwQyxvQ0FBc0I7O0lBQ3RCLHdDQUEwQjs7SUFFMUIscUNBQXdCOztJQUN4QiwwQ0FBNkI7O0lBQzdCLG9DQUF1Qjs7SUFDdkIsdUNBQTBCOztJQUMxQixxQ0FBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElCbGFja2phY2tSZXN1bHRcbntcbiAgICBpc0JsYWNramFjazpib29sZWFuO1xuICAgIGlzV2luOmJvb2xlYW47XG4gICAgaXNQdXNoOmJvb2xlYW47XG4gICAgaXNMb3NzOmJvb2xlYW47XG4gICAgaXNCdXN0ZWQ6Ym9vbGVhbjtcbiAgICBpc0luc3VyYW5jZVN1Y2Nlc3M6Ym9vbGVhbjtcblxuICAgIHZhbHVlOm51bWJlcjtcbiAgICBhbW91bnRXb246bnVtYmVyO1xufVxuXG5cbmFic3RyYWN0IGNsYXNzIEhhbmRSZXN1bHRCYXNlXG57XG4gICAgcmVhZG9ubHkgYW1vdW50V29uOm51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSB2YWx1ZTpudW1iZXIsXG4gICAgICAgIGJldEFtb3VudDpudW1iZXJcbiAgICApIHtcbiAgICAgICAgdGhpcy5hbW91bnRXb24gPSB2YWx1ZSAqIGJldEFtb3VudDtcbiAgICB9XG5cbiAgICBnZXQgaXNCbGFja2phY2soKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgZ2V0IGlzV2luKCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGdldCBpc1B1c2goKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgZ2V0IGlzTG9zcygpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBnZXQgaXNCdXN0ZWQoKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgZ2V0IGlzSW5zdXJhbmNlU3VjY2VzcygpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEJsYWNramFja1Jlc3VsdCBleHRlbmRzIEhhbmRSZXN1bHRCYXNlIGltcGxlbWVudHMgSUJsYWNramFja1Jlc3VsdFxue1xuICAgIGNvbnN0cnVjdG9yKGJldEFtb3VudDpudW1iZXIpIHtcbiAgICAgICAgc3VwZXIoMS41LGJldEFtb3VudCk7XG4gICAgfVxuXG4gICAgZ2V0IGlzQmxhY2tqYWNrKCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgZ2V0IGlzV2luKCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBXaW5SZXN1bHQgZXh0ZW5kcyBIYW5kUmVzdWx0QmFzZSBpbXBsZW1lbnRzIElCbGFja2phY2tSZXN1bHRcbntcbiAgICBjb25zdHJ1Y3RvcihiZXRBbW91bnQ6bnVtYmVyKSB7XG4gICAgICAgIHN1cGVyKDEsYmV0QW1vdW50KTtcbiAgICB9XG5cbiAgICBnZXQgaXNXaW4oKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIFB1c2hSZXN1bHQgZXh0ZW5kcyBIYW5kUmVzdWx0QmFzZSBpbXBsZW1lbnRzIElCbGFja2phY2tSZXN1bHRcbntcbiAgICBjb25zdHJ1Y3RvcihiZXRBbW91bnQ6bnVtYmVyKSB7XG4gICAgICAgIHN1cGVyKDAsYmV0QW1vdW50KTtcbiAgICB9XG5cbiAgICBnZXQgaXNQdXNoKCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBMb3NzUmVzdWx0IGV4dGVuZHMgSGFuZFJlc3VsdEJhc2UgaW1wbGVtZW50cyBJQmxhY2tqYWNrUmVzdWx0XG57XG4gICAgY29uc3RydWN0b3IoYmV0QW1vdW50Om51bWJlcikge1xuICAgICAgICBzdXBlcigtMSxiZXRBbW91bnQpO1xuICAgIH1cblxuICAgIGdldCBpc0xvc3MoKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEJ1c3RlZFJlc3VsdCBleHRlbmRzIEhhbmRSZXN1bHRCYXNlIGltcGxlbWVudHMgSUJsYWNramFja1Jlc3VsdFxue1xuICAgIGNvbnN0cnVjdG9yKGJldEFtb3VudDpudW1iZXIpIHtcbiAgICAgICAgc3VwZXIoLTEsYmV0QW1vdW50KTtcbiAgICB9XG5cbiAgICBnZXQgaXNCdXN0ZWQoKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICBnZXQgaXNMb3NzKCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBJbnN1cmFuY2VTdWNjZXNzUmVzdWx0IGV4dGVuZHMgSGFuZFJlc3VsdEJhc2UgaW1wbGVtZW50cyBJQmxhY2tqYWNrUmVzdWx0XG57XG4gICAgY29uc3RydWN0b3IoYmV0QW1vdW50Om51bWJlcikge1xuICAgICAgICBzdXBlcigwLGJldEFtb3VudCk7XG4gICAgfVxuXG4gICAgZ2V0IGlzSW5zdXJhbmNlU3VjY2VzcygpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgQ29tYmluZWRIYW5kc1Jlc3VsdCBpbXBsZW1lbnRzIElCbGFja2phY2tSZXN1bHRcbntcbiAgICByZWFkb25seSBpc0luc3VyYW5jZVN1Y2Nlc3MgPSBmYWxzZTtcblxuICAgIHJlYWRvbmx5IHZhbHVlOm51bWJlcjtcbiAgICByZWFkb25seSBhbW91bnRXb246bnVtYmVyO1xuXG4gICAgcmVhZG9ubHkgaXNQdXNoOmJvb2xlYW47XG4gICAgcmVhZG9ubHkgaXNCbGFja2phY2s6Ym9vbGVhbjtcbiAgICByZWFkb25seSBpc1dpbjpib29sZWFuO1xuICAgIHJlYWRvbmx5IGlzQnVzdGVkOmJvb2xlYW47XG4gICAgcmVhZG9ubHkgaXNMb3NzOmJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcmVzdWx0MTpJQmxhY2tqYWNrUmVzdWx0LFxuICAgICAgICByZXN1bHQyOklCbGFja2phY2tSZXN1bHRcbiAgICApIHtcbiAgICAgICAgdGhpcy52YWx1ZSA9IHJlc3VsdDEudmFsdWUgKyByZXN1bHQyLnZhbHVlO1xuXG4gICAgICAgIHRoaXMuYW1vdW50V29uID0gcmVzdWx0MS5hbW91bnRXb24gKyByZXN1bHQyLmFtb3VudFdvbjtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuaXNQdXNoID0gdGhpcy5hbW91bnRXb24gPT0gMDtcbiAgICAgICAgdGhpcy5pc0JsYWNramFjayA9IHRoaXMudmFsdWUgPj0gMS41O1xuICAgICAgICB0aGlzLmlzV2luID0gIXRoaXMuaXNCbGFja2phY2sgJiYgdGhpcy52YWx1ZSA+IDA7XG4gICAgICAgIHRoaXMuaXNCdXN0ZWQgPSByZXN1bHQxLmlzQnVzdGVkICYmIHJlc3VsdDIuaXNCdXN0ZWQ7XG4gICAgICAgIHRoaXMuaXNMb3NzID0gIXRoaXMuaXNCdXN0ZWQgJiYgdGhpcy52YWx1ZSA8IDA7XG4gICAgfVxufSJdfQ==