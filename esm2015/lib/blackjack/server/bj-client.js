/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/bj-client.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { SocketMessageManager, BlackjackMessage } from 'earnbet-common';
import { SocketClientBase } from 'earnbet-common-front-end';
import { BlackjackBetResult } from '../../_common/models/bets/blackjack-bet';
/** @type {?} */
const NAMESPACE = '/blackjack';
export class BlackjackClient extends SocketClientBase {
    /**
     * @param {?} app
     */
    constructor(app) {
        super({
            host: 'bjs.eosbet.io',
            port: 443,
            ssl: true
        }, NAMESPACE);
        this.app = app;
        this._isConnected = false;
    }
    /**
     * @return {?}
     */
    connect() {
        /** @type {?} */
        var socket = this.connectSocket();
        this._messageManager = new BlackjackMessageManager(socket, this);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setListener(value) {
        this.listener = value;
    }
    /**
     * @protected
     * @return {?}
     */
    onConnected() {
        this._isConnected = true;
        console.log('blackjack client connected to Blackjack Server!');
        if (this.listener) {
            this.listener.onBlackjackClientConnected();
        }
    }
    /**
     * MESSAGE LISTENERS: **
     * @param {?} data
     * @return {?}
     */
    onResolveBetResult(data) {
        this.app.state.bets.onBetResolved(new BlackjackBetResult(data));
    }
    /**
     *
     * @protected
     * @return {?}
     */
    onDisconnected() {
        this._isConnected = false;
    }
    /**
     * @return {?}
     */
    get messageManager() {
        return this._messageManager;
    }
    /**
     * @return {?}
     */
    get isConnected() {
        return this._isConnected;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackClient.prototype._isConnected;
    /**
     * @type {?}
     * @private
     */
    BlackjackClient.prototype._messageManager;
    /**
     * @type {?}
     * @private
     */
    BlackjackClient.prototype.listener;
    /**
     * @type {?}
     * @private
     */
    BlackjackClient.prototype.app;
}
export class BlackjackMessageManager extends SocketMessageManager {
    /**
     * @param {?} socket
     * @param {?} client
     */
    constructor(socket, client) {
        super(socket, BlackjackMessage.PREFIX, client);
    }
    /**
     * @template T
     * @param {?} data
     * @param {?} info
     * @return {?}
     */
    sendGameCommand(data, info) {
        return this.makeRequest(BlackjackMessage.GAME_COMMAND, data, info);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmotY2xpZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svc2VydmVyL2JqLWNsaWVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxvQkFBb0IsRUFBQyxnQkFBZ0IsRUFBa0QsTUFBTSxnQkFBZ0IsQ0FBQztBQUN2SCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUkxRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQzs7TUFJdkUsU0FBUyxHQUFHLFlBQVk7QUFFOUIsTUFBTSxzQkFBdUIsU0FBUSxnQkFBZ0I7Ozs7SUFPakQsWUFBb0IsR0FBbUI7UUFDbkMsS0FBSyxDQUNEO1lBQ0ksSUFBSSxFQUFFLGVBQWU7WUFDckIsSUFBSSxFQUFFLEdBQUc7WUFDVCxHQUFHLEVBQUUsSUFBSTtTQUNaLEVBQ0QsU0FBUyxDQUNaLENBQUM7UUFSYyxRQUFHLEdBQUgsR0FBRyxDQUFnQjtRQUwvQixpQkFBWSxHQUFHLEtBQUssQ0FBQztJQWM3QixDQUFDOzs7O0lBRUQsT0FBTzs7WUFDQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRTtRQUNqQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksdUJBQXVCLENBQzlDLE1BQU0sRUFBQyxJQUFJLENBQ2QsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLEtBQThCO1FBQ3RDLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBRVMsV0FBVztRQUNqQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUV6QixPQUFPLENBQUMsR0FBRyxDQUFDLGlEQUFpRCxDQUFDLENBQUM7UUFFL0QsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsSUFBSSxDQUFDLFFBQVEsQ0FBQywwQkFBMEIsRUFBRSxDQUFDO1NBQzlDO0lBQ0wsQ0FBQzs7Ozs7O0lBS0Qsa0JBQWtCLENBQUMsSUFBMEI7UUFDekMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FDN0IsSUFBSSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FDL0IsQ0FBQztJQUNOLENBQUM7Ozs7OztJQU1TLGNBQWM7UUFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQzs7OztJQUdELElBQUksY0FBYztRQUNkLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQsSUFBSSxXQUFXO1FBQ1gsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7Q0FDSjs7Ozs7O0lBOURHLHVDQUE2Qjs7Ozs7SUFFN0IsMENBQWdEOzs7OztJQUNoRCxtQ0FBMEM7Ozs7O0lBRTlCLDhCQUEyQjs7QUE0RDNDLE1BQU0sOEJBQStCLFNBQVEsb0JBQW9COzs7OztJQUU3RCxZQUNJLE1BQTRCLEVBQzVCLE1BQXNCO1FBRXRCLEtBQUssQ0FBQyxNQUFNLEVBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7Ozs7SUFFRCxlQUFlLENBQ1gsSUFBVyxFQUFDLElBQW1CO1FBRS9CLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FDbkIsZ0JBQWdCLENBQUMsWUFBWSxFQUM3QixJQUFJLEVBQ0osSUFBSSxDQUNQLENBQUM7SUFDTixDQUFDO0NBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTb2NrZXRNZXNzYWdlTWFuYWdlcixCbGFja2phY2tNZXNzYWdlLCBJU2lnbmF0dXJlSW5mbywgSVJlc29sdmVkQmxhY2tqYWNrQmV0LElQcm9taXNlIH0gZnJvbSAnZWFybmJldC1jb21tb24nO1xuaW1wb3J0IHtTb2NrZXRDbGllbnRCYXNlfSBmcm9tICdlYXJuYmV0LWNvbW1vbi1mcm9udC1lbmQnO1xuXG5pbXBvcnQgeyBJQmxhY2tqYWNrQ2xpZW50TGlzdGVuZXIgfSBmcm9tICcuLi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElNYWluQXBwQ29udGV4dCB9IGZyb20gJy4uLy4uL19jb21tb24vc2VydmljZXMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBCbGFja2phY2tCZXRSZXN1bHQgfSBmcm9tICcuLi8uLi9fY29tbW9uL21vZGVscy9iZXRzL2JsYWNramFjay1iZXQnO1xuXG5cblxuY29uc3QgTkFNRVNQQUNFID0gJy9ibGFja2phY2snO1xuXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrQ2xpZW50IGV4dGVuZHMgU29ja2V0Q2xpZW50QmFzZVxue1xuICAgIHByaXZhdGUgX2lzQ29ubmVjdGVkID0gZmFsc2U7XG5cbiAgICBwcml2YXRlIF9tZXNzYWdlTWFuYWdlcjpCbGFja2phY2tNZXNzYWdlTWFuYWdlcjtcbiAgICBwcml2YXRlIGxpc3RlbmVyOklCbGFja2phY2tDbGllbnRMaXN0ZW5lcjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBwOklNYWluQXBwQ29udGV4dCkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBob3N0OiAnYmpzLmVvc2JldC5pbycsXG4gICAgICAgICAgICAgICAgcG9ydDogNDQzLFxuICAgICAgICAgICAgICAgIHNzbDogdHJ1ZVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIE5BTUVTUEFDRVxuICAgICAgICApO1xuICAgIH1cblxuICAgIGNvbm5lY3QoKSB7XG4gICAgICAgIHZhciBzb2NrZXQgPSB0aGlzLmNvbm5lY3RTb2NrZXQoKTtcbiAgICAgICAgdGhpcy5fbWVzc2FnZU1hbmFnZXIgPSBuZXcgQmxhY2tqYWNrTWVzc2FnZU1hbmFnZXIoXG4gICAgICAgICAgICBzb2NrZXQsdGhpc1xuICAgICAgICApO1xuICAgIH1cblxuICAgIHNldExpc3RlbmVyKHZhbHVlOklCbGFja2phY2tDbGllbnRMaXN0ZW5lcikge1xuICAgICAgICB0aGlzLmxpc3RlbmVyID0gdmFsdWU7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIG9uQ29ubmVjdGVkKCk6IHZvaWQge1xuICAgICAgICB0aGlzLl9pc0Nvbm5lY3RlZCA9IHRydWU7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ2JsYWNramFjayBjbGllbnQgY29ubmVjdGVkIHRvIEJsYWNramFjayBTZXJ2ZXIhJyk7XG5cbiAgICAgICAgaWYgKHRoaXMubGlzdGVuZXIpIHtcbiAgICAgICAgICAgIHRoaXMubGlzdGVuZXIub25CbGFja2phY2tDbGllbnRDb25uZWN0ZWQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuXG5cbiAgICAvKioqIE1FU1NBR0UgTElTVEVORVJTOiAqKiovXG4gICAgb25SZXNvbHZlQmV0UmVzdWx0KGRhdGE6SVJlc29sdmVkQmxhY2tqYWNrQmV0KSB7XG4gICAgICAgIHRoaXMuYXBwLnN0YXRlLmJldHMub25CZXRSZXNvbHZlZChcbiAgICAgICAgICAgIG5ldyBCbGFja2phY2tCZXRSZXN1bHQoZGF0YSlcbiAgICAgICAgKTtcbiAgICB9XG4gICAgLyoqKi9cblxuXG5cblxuICAgIHByb3RlY3RlZCBvbkRpc2Nvbm5lY3RlZCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5faXNDb25uZWN0ZWQgPSBmYWxzZTtcbiAgICB9XG5cblxuICAgIGdldCBtZXNzYWdlTWFuYWdlcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX21lc3NhZ2VNYW5hZ2VyO1xuICAgIH1cblxuICAgIGdldCBpc0Nvbm5lY3RlZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzQ29ubmVjdGVkO1xuICAgIH1cbn1cblxuXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrTWVzc2FnZU1hbmFnZXIgZXh0ZW5kcyBTb2NrZXRNZXNzYWdlTWFuYWdlclxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBzb2NrZXQ6U29ja2V0SU9DbGllbnQuU29ja2V0LFxuICAgICAgICBjbGllbnQ6QmxhY2tqYWNrQ2xpZW50XG4gICAgKSB7XG4gICAgICAgIHN1cGVyKHNvY2tldCxCbGFja2phY2tNZXNzYWdlLlBSRUZJWCxjbGllbnQpO1xuICAgIH1cblxuICAgIHNlbmRHYW1lQ29tbWFuZDxUPihcbiAgICAgICAgZGF0YTpzdHJpbmcsaW5mbzpJU2lnbmF0dXJlSW5mb1xuICAgICk6SVByb21pc2U8VD4ge1xuICAgICAgICByZXR1cm4gdGhpcy5tYWtlUmVxdWVzdDxUPihcbiAgICAgICAgICAgIEJsYWNramFja01lc3NhZ2UuR0FNRV9DT01NQU5ELFxuICAgICAgICAgICAgZGF0YSxcbiAgICAgICAgICAgIGluZm9cbiAgICAgICAgKTtcbiAgICB9XG59XG4iXX0=