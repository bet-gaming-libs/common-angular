/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/local-server.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { BlackjackDealerHand } from '../components/main/game/blackjack-hands';
import { generateRandomCardId } from '../../_common/util/cards-util';
export class LocalBlackjackServer {
    /**
     * @param {?} component
     * @param {?} soundPlayer
     */
    constructor(component, soundPlayer) {
        this.component = component;
        this.isWaitingForResponse = false;
        this.dealer = new BlackjackDealerHand(soundPlayer);
    }
    /**
     * @return {?}
     */
    resumeExistingGame() { }
    /**
     * @param {?} betAmount
     * @return {?}
     */
    placeBet(betAmount) {
        /** @type {?} */
        const dealersCard1 = generateRandomCardId();
        /** @type {?} */
        const dealersCard2 = generateRandomCardId();
        //const dealersCard1 = 1;
        //const dealersCard2 = 10;
        this.dealer.reset();
        this.dealer.dealFirstCard(dealersCard1);
        this.dealer.revealSecondCard(dealersCard2);
        /** @type {?} */
        const playersCard1 = generateRandomCardId();
        /** @type {?} */
        const playersCard2 = generateRandomCardId();
        //const playersCard1 = generateRandomCardId();
        //const playersCard2 = playersCard1;
        this.component.game.start({
            playerCards: [playersCard1, playersCard2],
            dealerCards: [dealersCard1]
        }, Number(betAmount.decimal));
    }
    /**
     * @param {?} yes
     * @return {?}
     */
    respondToInsurance(yes) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return this.dealer.isBlackJack;
        });
    }
    /**
     * @return {?}
     */
    peekForBlackjack() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return this.dealer.isBlackJack;
        });
    }
    /**
     * @return {?}
     */
    getDealersSecondCard() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return this.dealer.cards[1].cardId;
        });
    }
    /**
     * @return {?}
     */
    split() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return [
                generateRandomCardId(),
                generateRandomCardId()
            ];
        });
    }
    /**
     * @return {?}
     */
    doubleDown() {
        return this.hit();
    }
    /**
     * @return {?}
     */
    hit() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return generateRandomCardId();
        });
    }
    /**
     * @return {?}
     */
    stand() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // do nothing
            return true;
        });
    }
    /**
     * @return {?}
     */
    getDealersAdditionalCards() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const cards = [];
            for (var i = 0; i < 8; i++) {
                cards.push(generateRandomCardId());
            }
            return cards;
        });
    }
}
if (false) {
    /** @type {?} */
    LocalBlackjackServer.prototype.isWaitingForResponse;
    /**
     * @type {?}
     * @private
     */
    LocalBlackjackServer.prototype.dealer;
    /**
     * @type {?}
     * @private
     */
    LocalBlackjackServer.prototype.component;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtc2VydmVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svc2VydmVyL2xvY2FsLXNlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFHQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUU5RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUlyRSxNQUFNOzs7OztJQU1GLFlBQ1ksU0FBNkIsRUFDckMsV0FBd0I7UUFEaEIsY0FBUyxHQUFULFNBQVMsQ0FBb0I7UUFMaEMseUJBQW9CLEdBQUcsS0FBSyxDQUFDO1FBUWxDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUN2RCxDQUFDOzs7O0lBRUQsa0JBQWtCLEtBQUksQ0FBQzs7Ozs7SUFFdkIsUUFBUSxDQUFDLFNBQXdCOztjQUN2QixZQUFZLEdBQUcsb0JBQW9CLEVBQUU7O2NBQ3JDLFlBQVksR0FBRyxvQkFBb0IsRUFBRTtRQUUzQyx5QkFBeUI7UUFDekIsMEJBQTBCO1FBRzFCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsQ0FBQzs7Y0FHckMsWUFBWSxHQUFHLG9CQUFvQixFQUFFOztjQUNyQyxZQUFZLEdBQUcsb0JBQW9CLEVBQUU7UUFFM0MsOENBQThDO1FBQzlDLG9DQUFvQztRQUdwQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQ3JCO1lBQ0ksV0FBVyxFQUFFLENBQUMsWUFBWSxFQUFDLFlBQVksQ0FBQztZQUN4QyxXQUFXLEVBQUUsQ0FBQyxZQUFZLENBQUM7U0FDOUIsRUFDRCxNQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUM1QixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFHSyxrQkFBa0IsQ0FBQyxHQUFZOztZQUNqQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ25DLENBQUM7S0FBQTs7OztJQUVLLGdCQUFnQjs7WUFDbEIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUNuQyxDQUFDO0tBQUE7Ozs7SUFHSyxvQkFBb0I7O1lBQ3RCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQ3ZDLENBQUM7S0FBQTs7OztJQUVLLEtBQUs7O1lBQ1AsT0FBTztnQkFDSCxvQkFBb0IsRUFBRTtnQkFDdEIsb0JBQW9CLEVBQUU7YUFDekIsQ0FBQztRQUNOLENBQUM7S0FBQTs7OztJQUNELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7O0lBQ0ssR0FBRzs7WUFDTCxPQUFPLG9CQUFvQixFQUFFLENBQUM7UUFDbEMsQ0FBQztLQUFBOzs7O0lBRUssS0FBSzs7WUFDUCxhQUFhO1lBQ2IsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztLQUFBOzs7O0lBR0sseUJBQXlCOzs7a0JBQ3JCLEtBQUssR0FBWSxFQUFFO1lBRXpCLEtBQUssSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3RCLEtBQUssQ0FBQyxJQUFJLENBQUUsb0JBQW9CLEVBQUUsQ0FBRSxDQUFDO2FBQ3hDO1lBRUQsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQztLQUFBO0NBQ0o7OztJQXJGRyxvREFBc0M7Ozs7O0lBRXRDLHNDQUFtQzs7Ozs7SUFHL0IseUNBQXFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRU9TQXNzZXRBbW91bnQgfSBmcm9tIFwiZWFybmJldC1jb21tb25cIjtcblxuaW1wb3J0IHsgSUJsYWNramFja1NlcnZlciB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBCbGFja2phY2tEZWFsZXJIYW5kIH0gZnJvbSAnLi4vY29tcG9uZW50cy9tYWluL2dhbWUvYmxhY2tqYWNrLWhhbmRzJztcbmltcG9ydCB7IElCbGFja2phY2tDb21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgZ2VuZXJhdGVSYW5kb21DYXJkSWQgfSBmcm9tICcuLi8uLi9fY29tbW9uL3V0aWwvY2FyZHMtdXRpbCc7XG5pbXBvcnQgeyBJU291bmRQbGF5ZXIgfSBmcm9tICcuLi8uLi9fY29tbW9uL21lZGlhL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBjbGFzcyBMb2NhbEJsYWNramFja1NlcnZlciBpbXBsZW1lbnRzIElCbGFja2phY2tTZXJ2ZXJcbntcbiAgICByZWFkb25seSBpc1dhaXRpbmdGb3JSZXNwb25zZSA9IGZhbHNlO1xuXG4gICAgcHJpdmF0ZSBkZWFsZXI6QmxhY2tqYWNrRGVhbGVySGFuZDtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGNvbXBvbmVudDpJQmxhY2tqYWNrQ29tcG9uZW50LFxuICAgICAgICBzb3VuZFBsYXllcjpJU291bmRQbGF5ZXJcbiAgICApIHtcbiAgICAgICAgdGhpcy5kZWFsZXIgPSBuZXcgQmxhY2tqYWNrRGVhbGVySGFuZChzb3VuZFBsYXllcik7XG4gICAgfVxuXG4gICAgcmVzdW1lRXhpc3RpbmdHYW1lKCkge31cblxuICAgIHBsYWNlQmV0KGJldEFtb3VudDpFT1NBc3NldEFtb3VudCkge1xuICAgICAgICBjb25zdCBkZWFsZXJzQ2FyZDEgPSBnZW5lcmF0ZVJhbmRvbUNhcmRJZCgpO1xuICAgICAgICBjb25zdCBkZWFsZXJzQ2FyZDIgPSBnZW5lcmF0ZVJhbmRvbUNhcmRJZCgpO1xuXG4gICAgICAgIC8vY29uc3QgZGVhbGVyc0NhcmQxID0gMTtcbiAgICAgICAgLy9jb25zdCBkZWFsZXJzQ2FyZDIgPSAxMDtcblxuXG4gICAgICAgIHRoaXMuZGVhbGVyLnJlc2V0KCk7XG5cbiAgICAgICAgdGhpcy5kZWFsZXIuZGVhbEZpcnN0Q2FyZChkZWFsZXJzQ2FyZDEpO1xuICAgICAgICB0aGlzLmRlYWxlci5yZXZlYWxTZWNvbmRDYXJkKGRlYWxlcnNDYXJkMik7XG5cblxuICAgICAgICBjb25zdCBwbGF5ZXJzQ2FyZDEgPSBnZW5lcmF0ZVJhbmRvbUNhcmRJZCgpO1xuICAgICAgICBjb25zdCBwbGF5ZXJzQ2FyZDIgPSBnZW5lcmF0ZVJhbmRvbUNhcmRJZCgpO1xuXG4gICAgICAgIC8vY29uc3QgcGxheWVyc0NhcmQxID0gZ2VuZXJhdGVSYW5kb21DYXJkSWQoKTtcbiAgICAgICAgLy9jb25zdCBwbGF5ZXJzQ2FyZDIgPSBwbGF5ZXJzQ2FyZDE7XG5cblxuICAgICAgICB0aGlzLmNvbXBvbmVudC5nYW1lLnN0YXJ0KFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBsYXllckNhcmRzOiBbcGxheWVyc0NhcmQxLHBsYXllcnNDYXJkMl0sXG4gICAgICAgICAgICAgICAgZGVhbGVyQ2FyZHM6IFtkZWFsZXJzQ2FyZDFdXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgTnVtYmVyKGJldEFtb3VudC5kZWNpbWFsKVxuICAgICAgICApO1xuICAgIH1cblxuXG4gICAgYXN5bmMgcmVzcG9uZFRvSW5zdXJhbmNlKHllczogYm9vbGVhbikge1xuICAgICAgICByZXR1cm4gdGhpcy5kZWFsZXIuaXNCbGFja0phY2s7XG4gICAgfVxuXG4gICAgYXN5bmMgcGVla0ZvckJsYWNramFjaygpOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVhbGVyLmlzQmxhY2tKYWNrO1xuICAgIH1cblxuXG4gICAgYXN5bmMgZ2V0RGVhbGVyc1NlY29uZENhcmQoKTogUHJvbWlzZTxudW1iZXI+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVhbGVyLmNhcmRzWzFdLmNhcmRJZDtcbiAgICB9XG5cbiAgICBhc3luYyBzcGxpdCgpOiBQcm9taXNlPG51bWJlcltdPiB7XG4gICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICBnZW5lcmF0ZVJhbmRvbUNhcmRJZCgpLFxuICAgICAgICAgICAgZ2VuZXJhdGVSYW5kb21DYXJkSWQoKVxuICAgICAgICBdO1xuICAgIH1cbiAgICBkb3VibGVEb3duKCk6IFByb21pc2U8bnVtYmVyPiB7XG4gICAgICAgIHJldHVybiB0aGlzLmhpdCgpO1xuICAgIH1cbiAgICBhc3luYyBoaXQoKTogUHJvbWlzZTxudW1iZXI+IHtcbiAgICAgICAgcmV0dXJuIGdlbmVyYXRlUmFuZG9tQ2FyZElkKCk7XG4gICAgfVxuXG4gICAgYXN5bmMgc3RhbmQoKSB7XG4gICAgICAgIC8vIGRvIG5vdGhpbmdcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG5cbiAgICBhc3luYyBnZXREZWFsZXJzQWRkaXRpb25hbENhcmRzKCk6IFByb21pc2U8bnVtYmVyW10+IHtcbiAgICAgICAgY29uc3QgY2FyZHM6bnVtYmVyW10gPSBbXTtcblxuICAgICAgICBmb3IgKHZhciBpPTA7IGkgPCA4OyBpKyspIHtcbiAgICAgICAgICAgIGNhcmRzLnB1c2goIGdlbmVyYXRlUmFuZG9tQ2FyZElkKCkgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBjYXJkcztcbiAgICB9XG59XG4iXX0=