/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/remote-server.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Big } from 'big.js';
import { BlackjackGameCommand } from "earnbet-common";
import { EosErrorParser } from '../../_common/eos/eos-error-parser';
export class RemoteBlackjackServerBridge {
    /**
     * @param {?} component
     * @param {?} accountNames
     * @param {?} app
     */
    constructor(component, accountNames, app) {
        this.component = component;
        this.accountNames = accountNames;
        this.app = app;
        this.actionId = 0;
        this._isWaitingForResponse = false;
        this.playersAdditionalCardIndex = undefined;
        this.isResuming = false;
    }
    /**
     * @return {?}
     */
    resumeExistingGame() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const wallet = this.wallet;
            if (!wallet.isLoggedIn ||
                this.isResuming) {
                return;
            }
            this.isResuming = true;
            /** @type {?} */
            const state = yield this.sendGameCommand(BlackjackGameCommand.RESUME_GAME, this.accountData);
            console.log('Resumed Game State:');
            console.log(state);
            this.app.state.bets.setMyBetId(state.id);
            yield this.startGame(state);
            // *** START SYNCING: ***
            this.playersAdditionalCardIndex = 0;
            for (var command of state.commands) {
                this.actionId++;
                /** @type {?} */
                const parts = command.split("~");
                /** @type {?} */
                const commandName = parts[0];
                //console.log(commandName);
                switch (commandName) {
                    case BlackjackGameCommand.YES_TO_INSURANCE:
                    case BlackjackGameCommand.NO_TO_INSURANCE:
                        this.game.respondToInsurance(commandName == BlackjackGameCommand.YES_TO_INSURANCE);
                        break;
                    case BlackjackGameCommand.SPLIT:
                        yield this.game.split();
                        break;
                    case BlackjackGameCommand.HIT:
                        yield this.game.hit();
                        break;
                    case BlackjackGameCommand.DOUBLE_DOWN:
                        yield this.game.doubleDown();
                        break;
                    case BlackjackGameCommand.STAND:
                        yield this.game.stand();
                        break;
                }
            }
            // *** STOP SYNCING: ***
            this.playersAdditionalCardIndex = undefined;
            this.isResuming = false;
        });
    }
    /**
     * @param {?} betAmount
     * @return {?}
     */
    placeBet(betAmount) {
        if (this.component.isInsufficientFunds) {
            this.app.openInsufficentFundsModal();
            return;
        }
        this._isWaitingForResponse = true;
        /** @type {?} */
        const component = this.component;
        /** @type {?} */
        const wallet = component.wallet;
        /** @type {?} */
        const memo = `${String(betAmount.integer)}-${wallet.publicKey}-${component.state.referrer}-${component.getSeed(false)}`;
        Big.RM = 0 /* RoundDown */;
        /** @type {?} */
        let amtDeposit = new Big(betAmount.decimal).times("4.5");
        /** @type {?} */
        const amtBalance = component.wallet.getBalance(component.selectedToken.symbol);
        /** @type {?} */
        const precision = component.selectedToken.precision;
        if (amtDeposit.gt(amtBalance)) {
            // If there is not enough funds for 4.5 then do not deposit the uneven remainder that cant be used
            /** @type {?} */
            const factor = new Big(amtBalance).div(betAmount.decimal).toFixed(0);
            amtDeposit = new Big(betAmount.decimal).times(factor);
        }
        /** @type {?} */
        const depositAmount = component.selectedToken.getAssetAmount(amtDeposit.toFixed(precision));
        /** @type {?} */
        const eosAccounts = this.app.eos.accounts;
        /** @type {?} */
        const transfers = [{
                toAccount: eosAccounts.games.blackjack,
                amount: depositAmount,
                memo
            }];
        if (component.isJackpotBet) {
            transfers.push({
                toAccount: eosAccounts.jackpot,
                amount: component.selectedToken.jackpotTicketPrice,
                memo: eosAccounts.games.blackjack
            });
        }
        /** @type {?} */
        const promise = wallet.transfer(transfers);
        promise.then((/**
         * @param {?} result
         * @return {?}
         */
        (result) => {
            /** @type {?} */
            const txnId = result.transaction_id;
            this.placedBetId = component.state.bets.onBetPlaced(txnId);
            this.component.trackBetWithGoogleAnalytics('blackjack', betAmount.decimal, txnId, false);
            this._isWaitingForResponse = false;
            this.waitForGame();
        }), (/**
         * @param {?} errorData
         * @return {?}
         */
        (errorData) => {
            this._isWaitingForResponse = false;
            /** @type {?} */
            const error = new EosErrorParser(errorData);
            component.alert.error(error.message);
        }));
    }
    /**
     * @private
     * @return {?}
     */
    waitForGame() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this._isWaitingForResponse = true;
            /** @type {?} */
            const state = yield this.sendGameCommand(BlackjackGameCommand.START_GAME, this.accountData);
            yield this.startGame(state);
            this.isResuming = false;
        });
    }
    /**
     * @private
     * @param {?} state
     * @return {?}
     */
    startGame(state) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.actionId = 0;
            this._isWaitingForResponse = false;
            //this.isDealerBlackjack = state.isDealerBlackjack;
            this.dealersAdditionalCards = state.additionalDealerCards;
            this.state = state;
            /** @type {?} */
            const token = state.initialBet.token;
            yield this.game.start(state.initialCards, state.initialBet.amountAsInteger /
                Math.pow(10, token.precision));
        });
    }
    /**
     * @param {?} yes
     * @return {?}
     */
    respondToInsurance(yes) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing ?
                this.state :
                yield this.sendGameCommand(yes ?
                    BlackjackGameCommand.YES_TO_INSURANCE :
                    BlackjackGameCommand.NO_TO_INSURANCE);
            return response.isDealerBlackjack;
        });
    }
    /**
     * @return {?}
     */
    peekForBlackjack() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing || this.state.isDealerBlackjack != undefined ?
                this.state :
                yield this.sendGameCommand(BlackjackGameCommand.PEEK_FOR_BLACKJACK);
            return response.isDealerBlackjack;
        });
    }
    /**
     * @return {?}
     */
    split() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing ?
                this.getAdditionalPlayerCards(2) :
                yield this.sendGameCommand(BlackjackGameCommand.SPLIT);
            return response;
        });
    }
    /**
     * @param {?} handIndex
     * @return {?}
     */
    hit(handIndex) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing ?
                this.getAdditionalPlayerCards(1) :
                yield this.sendGameCommand(BlackjackGameCommand.HIT);
            return response[0];
        });
    }
    /**
     * @param {?} handIndex
     * @return {?}
     */
    doubleDown(handIndex) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing ?
                this.getAdditionalPlayerCards(1) :
                yield this.sendGameCommand(BlackjackGameCommand.DOUBLE_DOWN);
            return response[0];
        });
    }
    /**
     * @private
     * @param {?} numOfCards
     * @return {?}
     */
    getAdditionalPlayerCards(numOfCards) {
        /** @type {?} */
        const cardIds = [];
        for (var i = 0; i < numOfCards; i++) {
            /** @type {?} */
            const index = this.playersAdditionalCardIndex++;
            /** @type {?} */
            const card = this.state.additionalPlayerCards[index];
            cardIds.push(card);
        }
        return cardIds;
    }
    /**
     * @return {?}
     */
    stand() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isSyncing) {
                return yield this.sendGameCommand(BlackjackGameCommand.STAND);
            }
            else {
                return false;
            }
        });
    }
    /**
     * @return {?}
     */
    getDealersSecondCard() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const cards = this.isSyncing ?
                this.state.additionalDealerCards :
                yield this.sendGameCommand(BlackjackGameCommand.GET_DEALERS_CARDS);
            this.dealersAdditionalCards = cards.slice(1);
            return cards[0];
        });
    }
    /**
     * @return {?}
     */
    getDealersAdditionalCards() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return this.dealersAdditionalCards;
        });
    }
    /**
     * @private
     * @template T
     * @param {?} commandName
     * @param {?=} args
     * @return {?}
     */
    sendGameCommand(commandName, args = []) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            const isResumeGameCommand = commandName == BlackjackGameCommand.RESUME_GAME;
            /** @type {?} */
            const isStartGameCommand = commandName == BlackjackGameCommand.START_GAME;
            /** @type {?} */
            const actionId = isResumeGameCommand || isStartGameCommand ?
                0 :
                ++this.actionId;
            /** @type {?} */
            const parts = [
                commandName,
                isStartGameCommand ?
                    this.placedBetId :
                    this.gameId,
                actionId
            ].concat(args);
            // data format:commandName~gameId~actionId~data...
            /** @type {?} */
            const data = parts.join('~');
            if (!isResumeGameCommand) {
                this._isWaitingForResponse = true;
            }
            this.wallet.sign(data, '')
                .then((/**
             * @param {?} info
             * @return {?}
             */
            (info) => {
                //console.log('Sending Game Command: ')
                //console.log(data);
                this.app.blackjack.client.messageManager.sendGameCommand(data, info).then((/**
                 * @param {?} result
                 * @return {?}
                 */
                (result) => {
                    this._isWaitingForResponse = false;
                    resolve(result);
                })).catch((/**
                 * @param {?} error
                 * @return {?}
                 */
                (error) => {
                    this._isWaitingForResponse = false;
                    this.alert.error(error.code);
                    reject(error);
                }));
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                this._isWaitingForResponse = false;
                /** @type {?} */
                const e = new EosErrorParser(error);
                this.alert.error(e.message);
                reject(error);
            }));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    get isSyncing() {
        return this.playersAdditionalCardIndex != undefined;
    }
    /**
     * @private
     * @return {?}
     */
    get gameId() {
        return this.state ?
            this.state.id :
            '0';
    }
    /**
     * @return {?}
     */
    get isWaitingForResponse() {
        return this._isWaitingForResponse;
    }
    /**
     * @private
     * @return {?}
     */
    get game() {
        return this.component.game;
    }
    /**
     * @private
     * @return {?}
     */
    get accountData() {
        /** @type {?} */
        const wallet = this.wallet;
        return wallet.isEasyAccount ?
            [this.accountNames.easyAccount, wallet.accountId, wallet.accountName] :
            [wallet.accountName];
    }
    /**
     * @private
     * @return {?}
     */
    get wallet() {
        return this.app.wallet;
    }
    /**
     * @private
     * @return {?}
     */
    get alert() {
        return this.app.alert;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.placedBetId;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.actionId;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype._isWaitingForResponse;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.state;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.dealersAdditionalCards;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.playersAdditionalCardIndex;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.isResuming;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.component;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.accountNames;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.app;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVtb3RlLXNlcnZlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL3NlcnZlci9yZW1vdGUtc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxHQUFHLEVBQWdCLE1BQU0sUUFBUSxDQUFDO0FBRzNDLE9BQU8sRUFBa0Isb0JBQW9CLEVBQXVCLE1BQU0sZ0JBQWdCLENBQUM7QUFLM0YsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBSXBFLE1BQU07Ozs7OztJQVdGLFlBQ1ksU0FBOEIsRUFDOUIsWUFBNkIsRUFDN0IsR0FBbUI7UUFGbkIsY0FBUyxHQUFULFNBQVMsQ0FBcUI7UUFDOUIsaUJBQVksR0FBWixZQUFZLENBQWlCO1FBQzdCLFFBQUcsR0FBSCxHQUFHLENBQWdCO1FBWHZCLGFBQVEsR0FBRyxDQUFDLENBQUM7UUFFYiwwQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFHOUIsK0JBQTBCLEdBQVUsU0FBUyxDQUFDO1FBQzlDLGVBQVUsR0FBRyxLQUFLLENBQUM7SUFPM0IsQ0FBQzs7OztJQUVLLGtCQUFrQjs7O2tCQUNkLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTTtZQUUxQixJQUNJLENBQUMsTUFBTSxDQUFDLFVBQVU7Z0JBQ2xCLElBQUksQ0FBQyxVQUFVLEVBQ2pCO2dCQUNFLE9BQU87YUFDVjtZQUdELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDOztrQkFHakIsS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FDcEMsb0JBQW9CLENBQUMsV0FBVyxFQUNoQyxJQUFJLENBQUMsV0FBVyxDQUNuQjtZQUdELE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBR25CLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBR3pDLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUc1Qix5QkFBeUI7WUFDekIsSUFBSSxDQUFDLDBCQUEwQixHQUFHLENBQUMsQ0FBQztZQUVwQyxLQUFLLElBQUksT0FBTyxJQUFJLEtBQUssQ0FBQyxRQUFRLEVBQUU7Z0JBQ2hDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzs7c0JBRVYsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOztzQkFFMUIsV0FBVyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBRTVCLDJCQUEyQjtnQkFFM0IsUUFBUSxXQUFXLEVBQUU7b0JBQ2pCLEtBQUssb0JBQW9CLENBQUMsZ0JBQWdCLENBQUM7b0JBQzNDLEtBQUssb0JBQW9CLENBQUMsZUFBZTt3QkFDckMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FDeEIsV0FBVyxJQUFJLG9CQUFvQixDQUFDLGdCQUFnQixDQUN2RCxDQUFBO3dCQUNELE1BQU07b0JBR1YsS0FBSyxvQkFBb0IsQ0FBQyxLQUFLO3dCQUMzQixNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBQ3hCLE1BQU07b0JBRVYsS0FBSyxvQkFBb0IsQ0FBQyxHQUFHO3dCQUN6QixNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7d0JBQ3RCLE1BQU07b0JBRVYsS0FBSyxvQkFBb0IsQ0FBQyxXQUFXO3dCQUNqQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7d0JBQzdCLE1BQU07b0JBRVYsS0FBSyxvQkFBb0IsQ0FBQyxLQUFLO3dCQUMzQixNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBQ3hCLE1BQU07aUJBQ2I7YUFDSjtZQUVELHdCQUF3QjtZQUN4QixJQUFJLENBQUMsMEJBQTBCLEdBQUcsU0FBUyxDQUFDO1lBRTVDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQzVCLENBQUM7S0FBQTs7Ozs7SUFFRCxRQUFRLENBQUMsU0FBeUI7UUFDOUIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixFQUFFO1lBQ3BDLElBQUksQ0FBQyxHQUFHLENBQUMseUJBQXlCLEVBQUUsQ0FBQztZQUVyQyxPQUFPO1NBQ1Y7UUFHRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDOztjQUc1QixTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVM7O2NBQzFCLE1BQU0sR0FBRyxTQUFTLENBQUMsTUFBTTs7Y0FHekIsSUFBSSxHQUFHLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7UUFFdkgsR0FBRyxDQUFDLEVBQUUsb0JBQXlCLENBQUM7O1lBQzVCLFVBQVUsR0FBRyxJQUFJLEdBQUcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQzs7Y0FDbEQsVUFBVSxHQUFXLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDOztjQUNoRixTQUFTLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQyxTQUFTO1FBRW5ELElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBRTs7O2tCQUVyQixNQUFNLEdBQUcsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3BFLFVBQVUsR0FBRyxJQUFJLEdBQUcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3pEOztjQUVLLGFBQWEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FDeEQsVUFBVSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FDaEM7O2NBRUssV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVE7O2NBRW5DLFNBQVMsR0FBRyxDQUFDO2dCQUNmLFNBQVMsRUFBRSxXQUFXLENBQUMsS0FBSyxDQUFDLFNBQVM7Z0JBQ3RDLE1BQU0sRUFBRSxhQUFhO2dCQUNyQixJQUFJO2FBQ1AsQ0FBQztRQUVGLElBQUksU0FBUyxDQUFDLFlBQVksRUFBRTtZQUN4QixTQUFTLENBQUMsSUFBSSxDQUFDO2dCQUNYLFNBQVMsRUFBRSxXQUFXLENBQUMsT0FBTztnQkFDOUIsTUFBTSxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsa0JBQWtCO2dCQUNsRCxJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTO2FBQ3BDLENBQUMsQ0FBQztTQUNOOztjQUdLLE9BQU8sR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztRQUUxQyxPQUFPLENBQUMsSUFBSTs7OztRQUNSLENBQUMsTUFBTSxFQUFFLEVBQUU7O2tCQUNELEtBQUssR0FBRyxNQUFNLENBQUMsY0FBYztZQUVuQyxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUczRCxJQUFJLENBQUMsU0FBUyxDQUFDLDJCQUEyQixDQUN0QyxXQUFXLEVBQ1gsU0FBUyxDQUFDLE9BQU8sRUFDakIsS0FBSyxFQUNMLEtBQUssQ0FDUixDQUFDO1lBR0YsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztZQUVuQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdkIsQ0FBQzs7OztRQUNELENBQUMsU0FBUyxFQUFFLEVBQUU7WUFDVixJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDOztrQkFFN0IsS0FBSyxHQUFHLElBQUksY0FBYyxDQUFDLFNBQVMsQ0FBQztZQUUzQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekMsQ0FBQyxFQUNKLENBQUM7SUFDTixDQUFDOzs7OztJQUVhLFdBQVc7O1lBQ3JCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7O2tCQUU1QixLQUFLLEdBQUcsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUNwQyxvQkFBb0IsQ0FBQyxVQUFVLEVBQy9CLElBQUksQ0FBQyxXQUFXLENBQ25CO1lBRUQsTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRTVCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQzVCLENBQUM7S0FBQTs7Ozs7O0lBRWEsU0FBUyxDQUFDLEtBQXlCOztZQUM3QyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1lBRW5DLG1EQUFtRDtZQUNuRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDLHFCQUFxQixDQUFDO1lBRTFELElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDOztrQkFFYixLQUFLLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLO1lBRXBDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQ2pCLEtBQUssQ0FBQyxZQUFZLEVBQ2xCLEtBQUssQ0FBQyxVQUFVLENBQUMsZUFBZTtnQkFDNUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUNuQyxDQUFDO1FBQ04sQ0FBQztLQUFBOzs7OztJQUVLLGtCQUFrQixDQUFDLEdBQVk7OztrQkFDM0IsUUFBUSxHQUNWLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDWixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ1osTUFBTSxJQUFJLENBQUMsZUFBZSxDQUN0QixHQUFHLENBQUMsQ0FBQztvQkFDRCxvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO29CQUN2QyxvQkFBb0IsQ0FBQyxlQUFlLENBQzNDO1lBRVQsT0FBTyxRQUFRLENBQUMsaUJBQWlCLENBQUM7UUFDdEMsQ0FBQztLQUFBOzs7O0lBRUssZ0JBQWdCOzs7a0JBQ1osUUFBUSxHQUNWLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsSUFBSSxTQUFTLENBQUMsQ0FBQztnQkFDekQsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNaLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FDdEIsb0JBQW9CLENBQUMsa0JBQWtCLENBQzFDO1lBRVQsT0FBTyxRQUFRLENBQUMsaUJBQWlCLENBQUM7UUFDdEMsQ0FBQztLQUFBOzs7O0lBR0ssS0FBSzs7O2tCQUNELFFBQVEsR0FDVixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ1osSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FDdEIsb0JBQW9CLENBQUMsS0FBSyxDQUM3QjtZQUVULE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUM7S0FBQTs7Ozs7SUFFSyxHQUFHLENBQUMsU0FBaUI7OztrQkFDakIsUUFBUSxHQUNWLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDWixJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUN0QixvQkFBb0IsQ0FBQyxHQUFHLENBQzNCO1lBRVQsT0FBTyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdkIsQ0FBQztLQUFBOzs7OztJQUVLLFVBQVUsQ0FBQyxTQUFpQjs7O2tCQUN4QixRQUFRLEdBQ1YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNaLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxNQUFNLElBQUksQ0FBQyxlQUFlLENBQ3RCLG9CQUFvQixDQUFDLFdBQVcsQ0FDbkM7WUFFVCxPQUFPLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QixDQUFDO0tBQUE7Ozs7OztJQUVPLHdCQUF3QixDQUFDLFVBQWlCOztjQUN4QyxPQUFPLEdBQVksRUFBRTtRQUUzQixLQUFLLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFOztrQkFDekIsS0FBSyxHQUFHLElBQUksQ0FBQywwQkFBMEIsRUFBRTs7a0JBRXpDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQztZQUVwRCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3RCO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQzs7OztJQUVLLEtBQUs7O1lBQ1AsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ2pCLE9BQU8sTUFBTSxJQUFJLENBQUMsZUFBZSxDQUM3QixvQkFBb0IsQ0FBQyxLQUFLLENBQzdCLENBQUM7YUFDTDtpQkFBTTtnQkFDSCxPQUFPLEtBQUssQ0FBQzthQUNoQjtRQUNMLENBQUM7S0FBQTs7OztJQUVLLG9CQUFvQjs7O2tCQUNoQixLQUFLLEdBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNaLElBQUksQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDbEMsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUN0QixvQkFBb0IsQ0FBQyxpQkFBaUIsQ0FDekM7WUFFVCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUU3QyxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNwQixDQUFDO0tBQUE7Ozs7SUFFSyx5QkFBeUI7O1lBQzNCLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDO1FBQ3ZDLENBQUM7S0FBQTs7Ozs7Ozs7SUFHTyxlQUFlLENBQ25CLFdBQWtCLEVBQ2xCLE9BQWdCLEVBQUU7UUFFbEIsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUssQ0FBQyxPQUFPLEVBQUMsTUFBTSxFQUFDLEVBQUU7O2tCQUMvQixtQkFBbUIsR0FDckIsV0FBVyxJQUFJLG9CQUFvQixDQUFDLFdBQVc7O2tCQUU3QyxrQkFBa0IsR0FDcEIsV0FBVyxJQUFJLG9CQUFvQixDQUFDLFVBQVU7O2tCQUc1QyxRQUFRLEdBQ1YsbUJBQW1CLElBQUksa0JBQWtCLENBQUMsQ0FBQztnQkFDdkMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsRUFBRSxJQUFJLENBQUMsUUFBUTs7a0JBRWpCLEtBQUssR0FBRztnQkFDVixXQUFXO2dCQUNYLGtCQUFrQixDQUFDLENBQUM7b0JBQ2hCLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDbEIsSUFBSSxDQUFDLE1BQU07Z0JBQ2YsUUFBUTthQUNYLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQzs7O2tCQUVSLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUU1QixJQUFJLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7YUFDckM7WUFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUMsRUFBRSxDQUFDO2lCQUN4QixJQUFJOzs7O1lBQUUsQ0FBQyxJQUFJLEVBQUMsRUFBRTtnQkFDWCx1Q0FBdUM7Z0JBQ3ZDLG9CQUFvQjtnQkFFcEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQ3BELElBQUksRUFDSixJQUFJLENBQ1AsQ0FBQyxJQUFJOzs7O2dCQUFFLENBQUMsTUFBTSxFQUFDLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztvQkFFbkMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNwQixDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O2dCQUFFLENBQUMsS0FBSyxFQUFDLEVBQUU7b0JBQ2YsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztvQkFFbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUU3QixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2xCLENBQUMsRUFBQyxDQUFDO1lBQ1AsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztZQUFFLENBQUMsS0FBSyxFQUFDLEVBQUU7Z0JBQ2YsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQzs7c0JBRTdCLENBQUMsR0FBRyxJQUFJLGNBQWMsQ0FBQyxLQUFLLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFFNUIsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2xCLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELElBQVksU0FBUztRQUNqQixPQUFPLElBQUksQ0FBQywwQkFBMEIsSUFBSSxTQUFTLENBQUM7SUFDeEQsQ0FBQzs7Ozs7SUFFRCxJQUFZLE1BQU07UUFDZCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNYLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDZixHQUFHLENBQUM7SUFDaEIsQ0FBQzs7OztJQUVELElBQUksb0JBQW9CO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDO0lBQ3RDLENBQUM7Ozs7O0lBRUQsSUFBWSxJQUFJO1FBQ1osT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztJQUMvQixDQUFDOzs7OztJQUVELElBQVksV0FBVzs7Y0FDYixNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU07UUFFMUIsT0FBUSxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDdEIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBQyxNQUFNLENBQUMsU0FBUyxFQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRUQsSUFBWSxNQUFNO1FBQ2QsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQztJQUMzQixDQUFDOzs7OztJQUVELElBQVksS0FBSztRQUNiLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7SUFDMUIsQ0FBQztDQUNKOzs7Ozs7SUE1WUcsa0RBQTJCOzs7OztJQUMzQiwrQ0FBcUI7Ozs7O0lBRXJCLDREQUFzQzs7Ozs7SUFDdEMsNENBQWtDOzs7OztJQUNsQyw2REFBd0M7Ozs7O0lBQ3hDLGlFQUFzRDs7Ozs7SUFDdEQsaURBQTJCOzs7OztJQUd2QixnREFBc0M7Ozs7O0lBQ3RDLG1EQUFxQzs7Ozs7SUFDckMsMENBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmlnLCBSb3VuZGluZ01vZGUgfSBmcm9tICdiaWcuanMnO1xuXG5cbmltcG9ydCB7IEVPU0Fzc2V0QW1vdW50LCBCbGFja2phY2tHYW1lQ29tbWFuZCwgSUJsYWNramFja0dhbWVTdGF0ZSB9IGZyb20gXCJlYXJuYmV0LWNvbW1vblwiO1xuXG5pbXBvcnQgeyBJQmxhY2tqYWNrU2VydmVyIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElCbGFja2phY2tDb21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSU1haW5BcHBDb250ZXh0IH0gZnJvbSAnLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IEVvc0Vycm9yUGFyc2VyIH0gZnJvbSAnLi4vLi4vX2NvbW1vbi9lb3MvZW9zLWVycm9yLXBhcnNlcic7XG5pbXBvcnQgeyBJRW9zQWNjb3VudE5hbWVzIH0gZnJvbSAnLi4vLi4vX2NvbW1vbi9tb2RlbHMvYnVzaW5lc3MtcnVsZXMvaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGNsYXNzIFJlbW90ZUJsYWNramFja1NlcnZlckJyaWRnZSBpbXBsZW1lbnRzIElCbGFja2phY2tTZXJ2ZXJcbntcbiAgICBwcml2YXRlIHBsYWNlZEJldElkOnN0cmluZztcbiAgICBwcml2YXRlIGFjdGlvbklkID0gMDtcblxuICAgIHByaXZhdGUgX2lzV2FpdGluZ0ZvclJlc3BvbnNlID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBzdGF0ZTpJQmxhY2tqYWNrR2FtZVN0YXRlO1xuICAgIHByaXZhdGUgZGVhbGVyc0FkZGl0aW9uYWxDYXJkczpudW1iZXJbXTtcbiAgICBwcml2YXRlIHBsYXllcnNBZGRpdGlvbmFsQ2FyZEluZGV4Om51bWJlciA9IHVuZGVmaW5lZDtcbiAgICBwcml2YXRlIGlzUmVzdW1pbmcgPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGNvbXBvbmVudDogSUJsYWNramFja0NvbXBvbmVudCxcbiAgICAgICAgcHJpdmF0ZSBhY2NvdW50TmFtZXM6SUVvc0FjY291bnROYW1lcyxcbiAgICAgICAgcHJpdmF0ZSBhcHA6SU1haW5BcHBDb250ZXh0XG4gICAgKSB7XG4gICAgfVxuXG4gICAgYXN5bmMgcmVzdW1lRXhpc3RpbmdHYW1lKCkge1xuICAgICAgICBjb25zdCB3YWxsZXQgPSB0aGlzLndhbGxldDtcblxuICAgICAgICBpZiAoXG4gICAgICAgICAgICAhd2FsbGV0LmlzTG9nZ2VkSW4gfHxcbiAgICAgICAgICAgIHRoaXMuaXNSZXN1bWluZ1xuICAgICAgICApIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgdGhpcy5pc1Jlc3VtaW5nID0gdHJ1ZTtcblxuXG4gICAgICAgIGNvbnN0IHN0YXRlID0gYXdhaXQgdGhpcy5zZW5kR2FtZUNvbW1hbmQ8SUJsYWNramFja0dhbWVTdGF0ZT4oXG4gICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5SRVNVTUVfR0FNRSxcbiAgICAgICAgICAgIHRoaXMuYWNjb3VudERhdGFcbiAgICAgICAgKTtcblxuXG4gICAgICAgIGNvbnNvbGUubG9nKCdSZXN1bWVkIEdhbWUgU3RhdGU6Jyk7XG4gICAgICAgIGNvbnNvbGUubG9nKHN0YXRlKTtcblxuXG4gICAgICAgIHRoaXMuYXBwLnN0YXRlLmJldHMuc2V0TXlCZXRJZChzdGF0ZS5pZCk7XG5cblxuICAgICAgICBhd2FpdCB0aGlzLnN0YXJ0R2FtZShzdGF0ZSk7XG5cblxuICAgICAgICAvLyAqKiogU1RBUlQgU1lOQ0lORzogKioqXG4gICAgICAgIHRoaXMucGxheWVyc0FkZGl0aW9uYWxDYXJkSW5kZXggPSAwO1xuXG4gICAgICAgIGZvciAodmFyIGNvbW1hbmQgb2Ygc3RhdGUuY29tbWFuZHMpIHtcbiAgICAgICAgICAgIHRoaXMuYWN0aW9uSWQrKztcblxuICAgICAgICAgICAgY29uc3QgcGFydHMgPSBjb21tYW5kLnNwbGl0KFwiflwiKTtcblxuICAgICAgICAgICAgY29uc3QgY29tbWFuZE5hbWUgPSBwYXJ0c1swXTtcblxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhjb21tYW5kTmFtZSk7XG5cbiAgICAgICAgICAgIHN3aXRjaCAoY29tbWFuZE5hbWUpIHtcbiAgICAgICAgICAgICAgICBjYXNlIEJsYWNramFja0dhbWVDb21tYW5kLllFU19UT19JTlNVUkFOQ0U6XG4gICAgICAgICAgICAgICAgY2FzZSBCbGFja2phY2tHYW1lQ29tbWFuZC5OT19UT19JTlNVUkFOQ0U6XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2FtZS5yZXNwb25kVG9JbnN1cmFuY2UoXG4gICAgICAgICAgICAgICAgICAgICAgICBjb21tYW5kTmFtZSA9PSBCbGFja2phY2tHYW1lQ29tbWFuZC5ZRVNfVE9fSU5TVVJBTkNFXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cblxuICAgICAgICAgICAgICAgIGNhc2UgQmxhY2tqYWNrR2FtZUNvbW1hbmQuU1BMSVQ6XG4gICAgICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuZ2FtZS5zcGxpdCgpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgICAgIGNhc2UgQmxhY2tqYWNrR2FtZUNvbW1hbmQuSElUOlxuICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmdhbWUuaGl0KCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICAgICAgY2FzZSBCbGFja2phY2tHYW1lQ29tbWFuZC5ET1VCTEVfRE9XTjpcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5nYW1lLmRvdWJsZURvd24oKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBjYXNlIEJsYWNramFja0dhbWVDb21tYW5kLlNUQU5EOlxuICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmdhbWUuc3RhbmQoKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAvLyAqKiogU1RPUCBTWU5DSU5HOiAqKipcbiAgICAgICAgdGhpcy5wbGF5ZXJzQWRkaXRpb25hbENhcmRJbmRleCA9IHVuZGVmaW5lZDtcblxuICAgICAgICB0aGlzLmlzUmVzdW1pbmcgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBwbGFjZUJldChiZXRBbW91bnQ6IEVPU0Fzc2V0QW1vdW50KSB7XG4gICAgICAgIGlmICh0aGlzLmNvbXBvbmVudC5pc0luc3VmZmljaWVudEZ1bmRzKSB7XG4gICAgICAgICAgICB0aGlzLmFwcC5vcGVuSW5zdWZmaWNlbnRGdW5kc01vZGFsKCk7XG5cbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yUmVzcG9uc2UgPSB0cnVlO1xuXG5cbiAgICAgICAgY29uc3QgY29tcG9uZW50ID0gdGhpcy5jb21wb25lbnQ7XG4gICAgICAgIGNvbnN0IHdhbGxldCA9IGNvbXBvbmVudC53YWxsZXQ7XG5cblxuICAgICAgICBjb25zdCBtZW1vID0gYCR7U3RyaW5nKGJldEFtb3VudC5pbnRlZ2VyKX0tJHt3YWxsZXQucHVibGljS2V5fS0ke2NvbXBvbmVudC5zdGF0ZS5yZWZlcnJlcn0tJHtjb21wb25lbnQuZ2V0U2VlZChmYWxzZSl9YDtcblxuICAgICAgICBCaWcuUk0gPSBSb3VuZGluZ01vZGUuUm91bmREb3duO1xuICAgICAgICBsZXQgYW10RGVwb3NpdCA9IG5ldyBCaWcoYmV0QW1vdW50LmRlY2ltYWwpLnRpbWVzKFwiNC41XCIpO1xuICAgICAgICBjb25zdCBhbXRCYWxhbmNlOiBzdHJpbmcgPSBjb21wb25lbnQud2FsbGV0LmdldEJhbGFuY2UoY29tcG9uZW50LnNlbGVjdGVkVG9rZW4uc3ltYm9sKTtcbiAgICAgICAgY29uc3QgcHJlY2lzaW9uID0gY29tcG9uZW50LnNlbGVjdGVkVG9rZW4ucHJlY2lzaW9uO1xuXG4gICAgICAgIGlmIChhbXREZXBvc2l0Lmd0KGFtdEJhbGFuY2UpKSB7XG4gICAgICAgICAgICAvLyBJZiB0aGVyZSBpcyBub3QgZW5vdWdoIGZ1bmRzIGZvciA0LjUgdGhlbiBkbyBub3QgZGVwb3NpdCB0aGUgdW5ldmVuIHJlbWFpbmRlciB0aGF0IGNhbnQgYmUgdXNlZFxuICAgICAgICAgICAgY29uc3QgZmFjdG9yID0gbmV3IEJpZyhhbXRCYWxhbmNlKS5kaXYoYmV0QW1vdW50LmRlY2ltYWwpLnRvRml4ZWQoMCk7XG4gICAgICAgICAgICBhbXREZXBvc2l0ID0gbmV3IEJpZyhiZXRBbW91bnQuZGVjaW1hbCkudGltZXMoZmFjdG9yKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGRlcG9zaXRBbW91bnQgPSBjb21wb25lbnQuc2VsZWN0ZWRUb2tlbi5nZXRBc3NldEFtb3VudChcbiAgICAgICAgICAgIGFtdERlcG9zaXQudG9GaXhlZChwcmVjaXNpb24pXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgZW9zQWNjb3VudHMgPSB0aGlzLmFwcC5lb3MuYWNjb3VudHM7XG5cbiAgICAgICAgY29uc3QgdHJhbnNmZXJzID0gW3tcbiAgICAgICAgICAgIHRvQWNjb3VudDogZW9zQWNjb3VudHMuZ2FtZXMuYmxhY2tqYWNrLFxuICAgICAgICAgICAgYW1vdW50OiBkZXBvc2l0QW1vdW50LFxuICAgICAgICAgICAgbWVtb1xuICAgICAgICB9XTtcblxuICAgICAgICBpZiAoY29tcG9uZW50LmlzSmFja3BvdEJldCkge1xuICAgICAgICAgICAgdHJhbnNmZXJzLnB1c2goe1xuICAgICAgICAgICAgICAgIHRvQWNjb3VudDogZW9zQWNjb3VudHMuamFja3BvdCxcbiAgICAgICAgICAgICAgICBhbW91bnQ6IGNvbXBvbmVudC5zZWxlY3RlZFRva2VuLmphY2twb3RUaWNrZXRQcmljZSxcbiAgICAgICAgICAgICAgICBtZW1vOiBlb3NBY2NvdW50cy5nYW1lcy5ibGFja2phY2tcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCBwcm9taXNlID0gd2FsbGV0LnRyYW5zZmVyKHRyYW5zZmVycyk7XG5cbiAgICAgICAgcHJvbWlzZS50aGVuKFxuICAgICAgICAgICAgKHJlc3VsdCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHR4bklkID0gcmVzdWx0LnRyYW5zYWN0aW9uX2lkO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5wbGFjZWRCZXRJZCA9IGNvbXBvbmVudC5zdGF0ZS5iZXRzLm9uQmV0UGxhY2VkKHR4bklkKTtcblxuXG4gICAgICAgICAgICAgICAgdGhpcy5jb21wb25lbnQudHJhY2tCZXRXaXRoR29vZ2xlQW5hbHl0aWNzKFxuICAgICAgICAgICAgICAgICAgICAnYmxhY2tqYWNrJyxcbiAgICAgICAgICAgICAgICAgICAgYmV0QW1vdW50LmRlY2ltYWwsXG4gICAgICAgICAgICAgICAgICAgIHR4bklkLFxuICAgICAgICAgICAgICAgICAgICBmYWxzZVxuICAgICAgICAgICAgICAgICk7XG5cblxuICAgICAgICAgICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvclJlc3BvbnNlID0gZmFsc2U7XG5cbiAgICAgICAgICAgICAgICB0aGlzLndhaXRGb3JHYW1lKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgKGVycm9yRGF0YSkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvclJlc3BvbnNlID0gZmFsc2U7XG5cbiAgICAgICAgICAgICAgICBjb25zdCBlcnJvciA9IG5ldyBFb3NFcnJvclBhcnNlcihlcnJvckRhdGEpO1xuXG4gICAgICAgICAgICAgICAgY29tcG9uZW50LmFsZXJ0LmVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICApO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgd2FpdEZvckdhbWUoKSB7XG4gICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvclJlc3BvbnNlID0gdHJ1ZTtcblxuICAgICAgICBjb25zdCBzdGF0ZSA9IGF3YWl0IHRoaXMuc2VuZEdhbWVDb21tYW5kPElCbGFja2phY2tHYW1lU3RhdGU+KFxuICAgICAgICAgICAgQmxhY2tqYWNrR2FtZUNvbW1hbmQuU1RBUlRfR0FNRSxcbiAgICAgICAgICAgIHRoaXMuYWNjb3VudERhdGFcbiAgICAgICAgKTtcblxuICAgICAgICBhd2FpdCB0aGlzLnN0YXJ0R2FtZShzdGF0ZSk7XG5cbiAgICAgICAgdGhpcy5pc1Jlc3VtaW5nID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBzdGFydEdhbWUoc3RhdGU6SUJsYWNramFja0dhbWVTdGF0ZSkge1xuICAgICAgICB0aGlzLmFjdGlvbklkID0gMDtcbiAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yUmVzcG9uc2UgPSBmYWxzZTtcblxuICAgICAgICAvL3RoaXMuaXNEZWFsZXJCbGFja2phY2sgPSBzdGF0ZS5pc0RlYWxlckJsYWNramFjaztcbiAgICAgICAgdGhpcy5kZWFsZXJzQWRkaXRpb25hbENhcmRzID0gc3RhdGUuYWRkaXRpb25hbERlYWxlckNhcmRzO1xuXG4gICAgICAgIHRoaXMuc3RhdGUgPSBzdGF0ZTtcblxuICAgICAgICBjb25zdCB0b2tlbiA9IHN0YXRlLmluaXRpYWxCZXQudG9rZW47XG5cbiAgICAgICAgYXdhaXQgdGhpcy5nYW1lLnN0YXJ0KFxuICAgICAgICAgICAgc3RhdGUuaW5pdGlhbENhcmRzLFxuICAgICAgICAgICAgc3RhdGUuaW5pdGlhbEJldC5hbW91bnRBc0ludGVnZXIgL1xuICAgICAgICAgICAgICAgIE1hdGgucG93KDEwLHRva2VuLnByZWNpc2lvbilcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBhc3luYyByZXNwb25kVG9JbnN1cmFuY2UoeWVzOiBib29sZWFuKTpQcm9taXNlPGJvb2xlYW4+IHtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPVxuICAgICAgICAgICAgdGhpcy5pc1N5bmNpbmcgP1xuICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUgOlxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuc2VuZEdhbWVDb21tYW5kPHtpc0RlYWxlckJsYWNramFjazpib29sZWFufT4oXG4gICAgICAgICAgICAgICAgICAgIHllcyA/XG4gICAgICAgICAgICAgICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5ZRVNfVE9fSU5TVVJBTkNFIDpcbiAgICAgICAgICAgICAgICAgICAgICAgIEJsYWNramFja0dhbWVDb21tYW5kLk5PX1RPX0lOU1VSQU5DRVxuICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmlzRGVhbGVyQmxhY2tqYWNrO1xuICAgIH1cblxuICAgIGFzeW5jIHBlZWtGb3JCbGFja2phY2soKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID1cbiAgICAgICAgICAgIHRoaXMuaXNTeW5jaW5nIHx8IHRoaXMuc3RhdGUuaXNEZWFsZXJCbGFja2phY2sgIT0gdW5kZWZpbmVkID9cbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlIDpcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnNlbmRHYW1lQ29tbWFuZDx7aXNEZWFsZXJCbGFja2phY2s6Ym9vbGVhbn0+KFxuICAgICAgICAgICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5QRUVLX0ZPUl9CTEFDS0pBQ0tcbiAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiByZXNwb25zZS5pc0RlYWxlckJsYWNramFjaztcbiAgICB9XG5cblxuICAgIGFzeW5jIHNwbGl0KCk6IFByb21pc2U8bnVtYmVyW10+IHtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPVxuICAgICAgICAgICAgdGhpcy5pc1N5bmNpbmcgP1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0QWRkaXRpb25hbFBsYXllckNhcmRzKDIpIDpcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnNlbmRHYW1lQ29tbWFuZDxudW1iZXJbXT4oXG4gICAgICAgICAgICAgICAgICAgIEJsYWNramFja0dhbWVDb21tYW5kLlNQTElUXG4gICAgICAgICAgICAgICAgKTtcblxuICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgfVxuXG4gICAgYXN5bmMgaGl0KGhhbmRJbmRleDogbnVtYmVyKTogUHJvbWlzZTxudW1iZXI+IHtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPVxuICAgICAgICAgICAgdGhpcy5pc1N5bmNpbmcgP1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0QWRkaXRpb25hbFBsYXllckNhcmRzKDEpIDpcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnNlbmRHYW1lQ29tbWFuZDxudW1iZXJbXT4oXG4gICAgICAgICAgICAgICAgICAgIEJsYWNramFja0dhbWVDb21tYW5kLkhJVFxuICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlWzBdO1xuICAgIH1cblxuICAgIGFzeW5jIGRvdWJsZURvd24oaGFuZEluZGV4OiBudW1iZXIpOiBQcm9taXNlPG51bWJlcj4ge1xuICAgICAgICBjb25zdCByZXNwb25zZSA9XG4gICAgICAgICAgICB0aGlzLmlzU3luY2luZyA/XG4gICAgICAgICAgICAgICAgdGhpcy5nZXRBZGRpdGlvbmFsUGxheWVyQ2FyZHMoMSkgOlxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuc2VuZEdhbWVDb21tYW5kPG51bWJlcltdPihcbiAgICAgICAgICAgICAgICAgICAgQmxhY2tqYWNrR2FtZUNvbW1hbmQuRE9VQkxFX0RPV05cbiAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiByZXNwb25zZVswXTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldEFkZGl0aW9uYWxQbGF5ZXJDYXJkcyhudW1PZkNhcmRzOm51bWJlcik6bnVtYmVyW10ge1xuICAgICAgICBjb25zdCBjYXJkSWRzOm51bWJlcltdID0gW107XG5cbiAgICAgICAgZm9yICh2YXIgaT0wOyBpIDwgbnVtT2ZDYXJkczsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMucGxheWVyc0FkZGl0aW9uYWxDYXJkSW5kZXgrKztcblxuICAgICAgICAgICAgY29uc3QgY2FyZCA9IHRoaXMuc3RhdGUuYWRkaXRpb25hbFBsYXllckNhcmRzW2luZGV4XTtcblxuICAgICAgICAgICAgY2FyZElkcy5wdXNoKGNhcmQpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGNhcmRJZHM7XG4gICAgfVxuXG4gICAgYXN5bmMgc3RhbmQoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgICAgIGlmICghdGhpcy5pc1N5bmNpbmcpIHtcbiAgICAgICAgICAgIHJldHVybiBhd2FpdCB0aGlzLnNlbmRHYW1lQ29tbWFuZDxib29sZWFuPihcbiAgICAgICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5TVEFORFxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIGdldERlYWxlcnNTZWNvbmRDYXJkKCk6IFByb21pc2U8bnVtYmVyPiB7XG4gICAgICAgIGNvbnN0IGNhcmRzID1cbiAgICAgICAgICAgIHRoaXMuaXNTeW5jaW5nID9cbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmFkZGl0aW9uYWxEZWFsZXJDYXJkcyA6XG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5zZW5kR2FtZUNvbW1hbmQ8bnVtYmVyW10+KFxuICAgICAgICAgICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5HRVRfREVBTEVSU19DQVJEU1xuICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5kZWFsZXJzQWRkaXRpb25hbENhcmRzID0gY2FyZHMuc2xpY2UoMSk7XG5cbiAgICAgICAgcmV0dXJuIGNhcmRzWzBdO1xuICAgIH1cblxuICAgIGFzeW5jIGdldERlYWxlcnNBZGRpdGlvbmFsQ2FyZHMoKTogUHJvbWlzZTxudW1iZXJbXT4ge1xuICAgICAgICByZXR1cm4gdGhpcy5kZWFsZXJzQWRkaXRpb25hbENhcmRzO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBzZW5kR2FtZUNvbW1hbmQ8VD4oXG4gICAgICAgIGNvbW1hbmROYW1lOnN0cmluZyxcbiAgICAgICAgYXJnczpzdHJpbmdbXSA9IFtdXG4gICAgKTpQcm9taXNlPFQ+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPFQ+KCAocmVzb2x2ZSxyZWplY3QpPT57XG4gICAgICAgICAgICBjb25zdCBpc1Jlc3VtZUdhbWVDb21tYW5kID1cbiAgICAgICAgICAgICAgICBjb21tYW5kTmFtZSA9PSBCbGFja2phY2tHYW1lQ29tbWFuZC5SRVNVTUVfR0FNRTtcblxuICAgICAgICAgICAgY29uc3QgaXNTdGFydEdhbWVDb21tYW5kID1cbiAgICAgICAgICAgICAgICBjb21tYW5kTmFtZSA9PSBCbGFja2phY2tHYW1lQ29tbWFuZC5TVEFSVF9HQU1FO1xuXG5cbiAgICAgICAgICAgIGNvbnN0IGFjdGlvbklkID1cbiAgICAgICAgICAgICAgICBpc1Jlc3VtZUdhbWVDb21tYW5kIHx8IGlzU3RhcnRHYW1lQ29tbWFuZCA/XG4gICAgICAgICAgICAgICAgICAgIDAgOlxuICAgICAgICAgICAgICAgICAgICArK3RoaXMuYWN0aW9uSWQ7XG5cbiAgICAgICAgICAgIGNvbnN0IHBhcnRzID0gW1xuICAgICAgICAgICAgICAgIGNvbW1hbmROYW1lLFxuICAgICAgICAgICAgICAgIGlzU3RhcnRHYW1lQ29tbWFuZCA/XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucGxhY2VkQmV0SWQgOlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdhbWVJZCxcbiAgICAgICAgICAgICAgICBhY3Rpb25JZFxuICAgICAgICAgICAgXS5jb25jYXQoYXJncyk7XG4gICAgICAgICAgICAvLyBkYXRhIGZvcm1hdDpjb21tYW5kTmFtZX5nYW1lSWR+YWN0aW9uSWR+ZGF0YS4uLlxuICAgICAgICAgICAgY29uc3QgZGF0YSA9IHBhcnRzLmpvaW4oJ34nKTtcblxuICAgICAgICAgICAgaWYgKCFpc1Jlc3VtZUdhbWVDb21tYW5kKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yUmVzcG9uc2UgPSB0cnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLndhbGxldC5zaWduKGRhdGEsJycpXG4gICAgICAgICAgICAudGhlbiggKGluZm8pPT57XG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZygnU2VuZGluZyBHYW1lIENvbW1hbmQ6ICcpXG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgICAgIHRoaXMuYXBwLmJsYWNramFjay5jbGllbnQubWVzc2FnZU1hbmFnZXIuc2VuZEdhbWVDb21tYW5kPFQ+KFxuICAgICAgICAgICAgICAgICAgICBkYXRhLFxuICAgICAgICAgICAgICAgICAgICBpbmZvXG4gICAgICAgICAgICAgICAgKS50aGVuKCAocmVzdWx0KT0+e1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9pc1dhaXRpbmdGb3JSZXNwb25zZSA9IGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9KS5jYXRjaCggKGVycm9yKT0+e1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9pc1dhaXRpbmdGb3JSZXNwb25zZSA9IGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWxlcnQuZXJyb3IoZXJyb3IuY29kZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pLmNhdGNoKCAoZXJyb3IpPT57XG4gICAgICAgICAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yUmVzcG9uc2UgPSBmYWxzZTtcblxuICAgICAgICAgICAgICAgIGNvbnN0IGUgPSBuZXcgRW9zRXJyb3JQYXJzZXIoZXJyb3IpO1xuICAgICAgICAgICAgICAgIHRoaXMuYWxlcnQuZXJyb3IoZS5tZXNzYWdlKTtcblxuICAgICAgICAgICAgICAgIHJlamVjdChlcnJvcik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgaXNTeW5jaW5nKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5wbGF5ZXJzQWRkaXRpb25hbENhcmRJbmRleCAhPSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgZ2FtZUlkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZSA/XG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5pZCA6XG4gICAgICAgICAgICAgICAgJzAnO1xuICAgIH1cblxuICAgIGdldCBpc1dhaXRpbmdGb3JSZXNwb25zZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzV2FpdGluZ0ZvclJlc3BvbnNlO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0IGdhbWUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbXBvbmVudC5nYW1lO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0IGFjY291bnREYXRhKCkge1xuICAgICAgICBjb25zdCB3YWxsZXQgPSB0aGlzLndhbGxldDtcblxuICAgICAgICByZXR1cm4gIHdhbGxldC5pc0Vhc3lBY2NvdW50ID9cbiAgICAgICAgICAgICAgICBbdGhpcy5hY2NvdW50TmFtZXMuZWFzeUFjY291bnQsd2FsbGV0LmFjY291bnRJZCx3YWxsZXQuYWNjb3VudE5hbWVdIDpcbiAgICAgICAgICAgICAgICBbd2FsbGV0LmFjY291bnROYW1lXTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCB3YWxsZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC53YWxsZXQ7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgYWxlcnQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC5hbGVydDtcbiAgICB9XG59XG4iXX0=