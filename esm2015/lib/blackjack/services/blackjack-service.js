/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/services/blackjack-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BlackjackClient } from '../server/bj-client';
export class BlackjackAppService {
    /**
     * @param {?} app
     */
    constructor(app) {
        this.client = new BlackjackClient(app);
    }
    /**
     * @return {?}
     */
    init() {
        this.client.connect();
    }
}
if (false) {
    /** @type {?} */
    BlackjackAppService.prototype.client;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLXNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL2JsYWNramFjay9zZXJ2aWNlcy9ibGFja2phY2stc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUt0RCxNQUFNOzs7O0lBSUYsWUFBWSxHQUFvQjtRQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNDLENBQUM7Ozs7SUFFRCxJQUFJO1FBQ0EsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUMxQixDQUFDO0NBQ0o7OztJQVRHLHFDQUFpQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJsYWNramFja0NsaWVudCB9IGZyb20gJy4uL3NlcnZlci9iai1jbGllbnQnO1xuaW1wb3J0IHsgSU1haW5BcHBDb250ZXh0IH0gZnJvbSAnLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElCbGFja2phY2tBcHBTZXJ2aWNlIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcblxuXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrQXBwU2VydmljZSBpbXBsZW1lbnRzIElCbGFja2phY2tBcHBTZXJ2aWNlXG57XG4gICAgcmVhZG9ubHkgY2xpZW50OiBCbGFja2phY2tDbGllbnQ7XG5cbiAgICBjb25zdHJ1Y3RvcihhcHA6IElNYWluQXBwQ29udGV4dCkge1xuICAgICAgICB0aGlzLmNsaWVudCA9IG5ldyBCbGFja2phY2tDbGllbnQoYXBwKTtcbiAgICB9XG5cbiAgICBpbml0KCkge1xuICAgICAgICB0aGlzLmNsaWVudC5jb25uZWN0KCk7XG4gICAgfVxufVxuIl19