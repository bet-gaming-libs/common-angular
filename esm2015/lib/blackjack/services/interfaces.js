/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/services/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IBlackjackClientListener() { }
if (false) {
    /**
     * @return {?}
     */
    IBlackjackClientListener.prototype.onBlackjackClientConnected = function () { };
}
/**
 * @record
 */
export function IBlackjackClient() { }
if (false) {
    /** @type {?} */
    IBlackjackClient.prototype.isConnected;
    /** @type {?} */
    IBlackjackClient.prototype.messageManager;
    /**
     * @param {?} listener
     * @return {?}
     */
    IBlackjackClient.prototype.setListener = function (listener) { };
}
/**
 * @record
 */
export function IBlackjackAppService() { }
if (false) {
    /** @type {?} */
    IBlackjackAppService.prototype.client;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL3NlcnZpY2VzL2ludGVyZmFjZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQSw4Q0FHQzs7Ozs7SUFERyxnRkFBa0M7Ozs7O0FBR3RDLHNDQU1DOzs7SUFKRyx1Q0FBNkI7O0lBQzdCLDBDQUFnRDs7Ozs7SUFFaEQsaUVBQW9EOzs7OztBQUd4RCwwQ0FHQzs7O0lBREcsc0NBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmxhY2tqYWNrTWVzc2FnZU1hbmFnZXIgfSBmcm9tICcuLi9zZXJ2ZXIvYmotY2xpZW50JztcblxuXG5leHBvcnQgaW50ZXJmYWNlIElCbGFja2phY2tDbGllbnRMaXN0ZW5lclxue1xuICAgIG9uQmxhY2tqYWNrQ2xpZW50Q29ubmVjdGVkKCk6dm9pZDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQmxhY2tqYWNrQ2xpZW50XG57XG4gICAgcmVhZG9ubHkgaXNDb25uZWN0ZWQ6Ym9vbGVhbjtcbiAgICByZWFkb25seSBtZXNzYWdlTWFuYWdlcjpCbGFja2phY2tNZXNzYWdlTWFuYWdlcjtcbiAgICBcbiAgICBzZXRMaXN0ZW5lcihsaXN0ZW5lcjpJQmxhY2tqYWNrQ2xpZW50TGlzdGVuZXIpOnZvaWQ7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUJsYWNramFja0FwcFNlcnZpY2VcbntcbiAgICBjbGllbnQ6SUJsYWNramFja0NsaWVudDtcbn0iXX0=