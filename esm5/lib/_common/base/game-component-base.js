/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/base/game-component-base.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Big } from 'big.js';
import { HostListener } from '@angular/core';
import { sleep } from 'earnbet-common';
import { TokenSymbol } from '../models/business-rules/tokens';
/** @type {?} */
var INDEX_OF_MAX_CHIP = 7;
/**
 * @record
 */
export function IBetAmount() { }
if (false) {
    /** @type {?} */
    IBetAmount.prototype.symbol;
    /** @type {?} */
    IBetAmount.prototype.decimal;
}
/**
 * @abstract
 */
var GameComponentBase = /** @class */ (function () {
    function GameComponentBase(app, googleAnalytics, soundPlayer) {
        this.app = app;
        this.googleAnalytics = googleAnalytics;
        this.soundPlayer = soundPlayer;
        this.autoBetNonce = 0;
        this._isJackpotBet = false;
        this.selectedChip = 1;
        this.showTooltip = false;
        this.processBetStyle = false;
    }
    /**
     * @return {?}
     */
    GameComponentBase.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.app.scrollToTopOfPage();
        this.app.settings.setListener(this);
        if (this.selectedToken.symbol == 'BET') {
            this.settings.selectCurrency('EOS');
        }
    };
    /**
     * @param {?} i
     * @return {?}
     */
    GameComponentBase.prototype.selectChip = /**
     * @param {?} i
     * @return {?}
     */
    function (i) {
        this.selectedChip = i;
        this.soundPlayer.play('chipPlace');
        this.onChipSelected();
    };
    /**
     * @protected
     * @return {?}
     */
    GameComponentBase.prototype.onChipSelected = /**
     * @protected
     * @return {?}
     */
    function () { };
    /**
     * @return {?}
     */
    GameComponentBase.prototype.toggleTooltip = /**
     * @return {?}
     */
    function () {
        this.showTooltip = !this.showTooltip;
    };
    /**
     * @return {?}
     */
    GameComponentBase.prototype.documentClick = /**
     * @return {?}
     */
    function () {
        this.showTooltip = false;
    };
    Object.defineProperty(GameComponentBase.prototype, "isJackpotBet", {
        get: /**
         * @return {?}
         */
        function () {
            return !this.isJackpotDisabled &&
                this._isJackpotBet;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (this.isJackpotDisabled) {
                return;
            }
            this._isJackpotBet = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "isJackpotDisabled", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var amount = Number(this.betAmount.decimal);
            return amount < this.selectedToken.jackpot.minimumBet;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} isAutoBet
     * @return {?}
     */
    GameComponentBase.prototype.getSeed = /**
     * @param {?} isAutoBet
     * @return {?}
     */
    function (isAutoBet) {
        // seed for randomness
        /** @type {?} */
        var seed = this.state.seedForRandomness +
            // add nonce for auto-bet to fairness seed
            (isAutoBet || true ?
                ',' + (++this.autoBetNonce) :
                '');
        return seed;
    };
    /**
     * @param {?} gameName
     * @param {?} betAmount
     * @param {?} txnId
     * @param {?} isAutoBet
     * @return {?}
     */
    GameComponentBase.prototype.trackBetWithGoogleAnalytics = /**
     * @param {?} gameName
     * @param {?} betAmount
     * @param {?} txnId
     * @param {?} isAutoBet
     * @return {?}
     */
    function (gameName, betAmount, txnId, isAutoBet) {
        /** @type {?} */
        var price = this.currency.getPrice(this.selectedToken.symbol);
        /** @type {?} */
        var amountInUSD = price ?
            (price * Number(betAmount)).toFixed(2) :
            undefined;
        if (amountInUSD) {
            this.googleAnalytics.addTransaction(txnId, gameName, this.selectedToken.symbol, amountInUSD, isAutoBet, this.isJackpotBet);
        }
    };
    /**
     * @protected
     * @param {?} result
     * @return {?}
     */
    GameComponentBase.prototype.updateBalances = /**
     * @protected
     * @param {?} result
     * @return {?}
     */
    function (result) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var bet, profit;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        bet = Number(result.amount) /
                            this.selectedToken.betTokenAirDropRate;
                        profit = result.profit;
                        if (result.isJackpotBet) {
                            profit -= Number(this.selectedToken.jackpot.ticketPrice);
                        }
                        /***/
                        this.addToBalance = {
                            main: profit,
                            bet: bet
                        };
                        // to animate balance in header
                        this.app.state.onAddToBalance(true, profit);
                        return [4 /*yield*/, sleep(500)];
                    case 1:
                        _a.sent();
                        /*** UPDATE TOKEN BALANCES ***/
                        // update main token balance
                        this.wallet.addToBalance(this.selectedToken.symbol, profit);
                        // update BET token balance
                        this.wallet.addToBalance(TokenSymbol.BET, bet);
                        /***/
                        return [4 /*yield*/, sleep(1500)];
                    case 2:
                        /***/
                        _a.sent();
                        this.addToBalance = undefined;
                        this.app.state.onAddToBalance(false);
                        return [2 /*return*/];
                }
            });
        });
    };
    Object.defineProperty(GameComponentBase.prototype, "tooltipBet", {
        // onTooltipShowBet(){
        //     document.getElementById("tooltipBet").setAttribute('data-tooltip', 'aaa');
        // }
        get: 
        // onTooltipShowBet(){
        //     document.getElementById("tooltipBet").setAttribute('data-tooltip', 'aaa');
        // }
        /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var tokenAlt = this.selectedToken.symbol;
            /** @type {?} */
            var airDrop = this.selectedToken.betTokenAirDropRate;
            return this.settings.translation.tooltipDividends("1", airDrop + " " + tokenAlt);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "jackpotToolTip", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var token = this.selectedToken;
            /** @type {?} */
            var jackpot = token.jackpot;
            return this.settings.translation.clickThisBoxToReceive1JackpotSpinForPrice(Number(jackpot.ticketPrice) + " " + token.symbol);
            // Currently not using a separate minimum bet for jackpot, 
            // it is currently the same as the standard minimum bet
            //"Bet must be at least "+jackpot.minimumBet+" "+token.symbol+" to enter.";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "isInsufficientFunds", {
        get: /**
         * @return {?}
         */
        function () {
            return new Big(this.currencyBalance).lt(this.selectedToken.minimumBetAmount) ||
                new Big(this.betAmount.decimal).gt(this.currencyBalance);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "isMaxChipSelected", {
        get: /**
         * @return {?}
         */
        function () {
            return this.selectedChip == INDEX_OF_MAX_CHIP;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "currencyBalance", {
        get: /**
         * @return {?}
         */
        function () {
            return this.settings.selectedTokenBalance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "chips", {
        get: /**
         * @return {?}
         */
        function () {
            return this.selectedToken.chips;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "selectedTokenPrice", {
        get: /**
         * @return {?}
         */
        function () {
            return this.currency.getPrice(this.selectedToken.symbol);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "currency", {
        get: /**
         * @return {?}
         */
        function () {
            return this.app.currency;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "selectedToken", {
        get: /**
         * @return {?}
         */
        function () {
            return this.settings.selectedToken;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "wallet", {
        get: /**
         * @return {?}
         */
        function () {
            return this.settings.wallet;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "state", {
        get: /**
         * @return {?}
         */
        function () {
            return this.app.state;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "translation", {
        get: /**
         * @return {?}
         */
        function () {
            return this.settings.translation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "settings", {
        get: /**
         * @return {?}
         */
        function () {
            return this.app.settings;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "alert", {
        get: /**
         * @return {?}
         */
        function () {
            return this.app.alert;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentBase.prototype, "bankroll", {
        get: /**
         * @return {?}
         */
        function () {
            return this.app.bankroll;
        },
        enumerable: true,
        configurable: true
    });
    GameComponentBase.propDecorators = {
        documentClick: [{ type: HostListener, args: ['document:click', [],] }]
    };
    return GameComponentBase;
}());
export { GameComponentBase };
if (false) {
    /**
     * @type {?}
     * @private
     */
    GameComponentBase.prototype.autoBetNonce;
    /**
     * @type {?}
     * @private
     */
    GameComponentBase.prototype._isJackpotBet;
    /** @type {?} */
    GameComponentBase.prototype.selectedChip;
    /** @type {?} */
    GameComponentBase.prototype.showTooltip;
    /** @type {?} */
    GameComponentBase.prototype.processBetStyle;
    /** @type {?} */
    GameComponentBase.prototype.addToBalance;
    /**
     * @type {?}
     * @protected
     */
    GameComponentBase.prototype.app;
    /**
     * @type {?}
     * @protected
     */
    GameComponentBase.prototype.googleAnalytics;
    /**
     * @type {?}
     * @protected
     */
    GameComponentBase.prototype.soundPlayer;
    /**
     * @abstract
     * @return {?}
     */
    GameComponentBase.prototype.onBettingCurrencyChanged = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    GameComponentBase.prototype.betAmount = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS1jb21wb25lbnQtYmFzZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9iYXNlL2dhbWUtY29tcG9uZW50LWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUU3QixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTdDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU12QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUNBQWlDLENBQUM7O0lBUXhELGlCQUFpQixHQUFHLENBQUM7Ozs7QUFHM0IsZ0NBSUM7OztJQUZHLDRCQUFjOztJQUNkLDZCQUFlOzs7OztBQUduQjtJQVVJLDJCQUNjLEdBQW1CLEVBQ25CLGVBQXVDLEVBQ3ZDLFdBQXdCO1FBRnhCLFFBQUcsR0FBSCxHQUFHLENBQWdCO1FBQ25CLG9CQUFlLEdBQWYsZUFBZSxDQUF3QjtRQUN2QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVg5QixpQkFBWSxHQUFHLENBQUMsQ0FBQztRQUNqQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUM5QixpQkFBWSxHQUFHLENBQUMsQ0FBQztRQUNWLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLG9CQUFlLEdBQUcsS0FBSyxDQUFDO0lBUy9CLENBQUM7Ozs7SUFFRCxvQ0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFFN0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXBDLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLElBQUksS0FBSyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQzs7Ozs7SUFJRCxzQ0FBVTs7OztJQUFWLFVBQVcsQ0FBUTtRQUNmLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO1FBRXRCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRW5DLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMxQixDQUFDOzs7OztJQUNTLDBDQUFjOzs7O0lBQXhCLGNBQTRCLENBQUM7Ozs7SUFFdEIseUNBQWE7OztJQUFwQjtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ3pDLENBQUM7Ozs7SUFHRCx5Q0FBYTs7O0lBRGI7UUFFSSxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUQsc0JBQUksMkNBQVk7Ozs7UUFPaEI7WUFDSSxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQjtnQkFDdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUMvQixDQUFDOzs7OztRQVZELFVBQWlCLEtBQWE7WUFDMUIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQ3hCLE9BQU87YUFDVjtZQUVELElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQy9CLENBQUM7OztPQUFBO0lBTUQsc0JBQUksZ0RBQWlCOzs7O1FBQXJCOztnQkFDVSxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDO1lBRTdDLE9BQU8sTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztRQUMxRCxDQUFDOzs7T0FBQTs7Ozs7SUFJRCxtQ0FBTzs7OztJQUFQLFVBQVEsU0FBaUI7OztZQUVmLElBQUksR0FDTixJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQjtZQUM1QiwwQ0FBMEM7WUFDMUMsQ0FDSSxTQUFTLElBQUksSUFBSSxDQUFDLENBQUM7Z0JBQ2YsR0FBRyxHQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDNUIsRUFBRSxDQUNUO1FBRUwsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7Ozs7SUFFRCx1REFBMkI7Ozs7Ozs7SUFBM0IsVUFDSSxRQUFlLEVBQ2YsU0FBZ0IsRUFDaEIsS0FBWSxFQUNaLFNBQWlCOztZQUVYLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FDaEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQzVCOztZQUVLLFdBQVcsR0FDYixLQUFLLENBQUMsQ0FBQztZQUNILENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLFNBQVM7UUFHakIsSUFBSSxXQUFXLEVBQUU7WUFDYixJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FDL0IsS0FBSyxFQUNMLFFBQVEsRUFDUixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFDekIsV0FBVyxFQUNYLFNBQVMsRUFDVCxJQUFJLENBQUMsWUFBWSxDQUNwQixDQUFDO1NBQ0w7SUFDTCxDQUFDOzs7Ozs7SUFFZSwwQ0FBYzs7Ozs7SUFBOUIsVUFBK0IsTUFBaUI7Ozs7Ozt3QkFJdEMsR0FBRyxHQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDOzRCQUNyQixJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQjt3QkFFdEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNO3dCQUUxQixJQUFJLE1BQU0sQ0FBQyxZQUFZLEVBQUU7NEJBQ3JCLE1BQU0sSUFBSSxNQUFNLENBQ1osSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUN6QyxDQUFDO3lCQUNMO3dCQUNELEtBQUs7d0JBR0wsSUFBSSxDQUFDLFlBQVksR0FBRzs0QkFDaEIsSUFBSSxFQUFFLE1BQU07NEJBQ1osR0FBRyxLQUFBO3lCQUNOLENBQUM7d0JBRUYsK0JBQStCO3dCQUMvQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUUzQyxxQkFBTSxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUE7O3dCQUFoQixTQUFnQixDQUFDO3dCQUdqQiwrQkFBK0I7d0JBQy9CLDRCQUE0Qjt3QkFDNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUN6QixNQUFNLENBQ1QsQ0FBQzt3QkFFRiwyQkFBMkI7d0JBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUNwQixXQUFXLENBQUMsR0FBRyxFQUNmLEdBQUcsQ0FDTixDQUFBO3dCQUNELEtBQUs7d0JBR0wscUJBQU0sS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFBOzt3QkFIakIsS0FBSzt3QkFHTCxTQUFpQixDQUFDO3dCQUVsQixJQUFJLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQzt3QkFDOUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7OztLQUN4QztJQUtELHNCQUFJLHlDQUFVO1FBSGQsc0JBQXNCO1FBQ3RCLGlGQUFpRjtRQUNqRixJQUFJOzs7Ozs7OztRQUNKOztnQkFDVSxRQUFRLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNOztnQkFDcEMsT0FBTyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CO1lBQ3RELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFJLE9BQU8sU0FBSSxRQUFVLENBQUMsQ0FBQTtRQUNuRixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDZDQUFjOzs7O1FBQWxCOztnQkFDVSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWE7O2dCQUMxQixPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU87WUFFN0IsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyx5Q0FBeUMsQ0FDdEUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBQyxHQUFHLEdBQUMsS0FBSyxDQUFDLE1BQU0sQ0FDL0MsQ0FBQztZQUNGLDJEQUEyRDtZQUN2RCx1REFBdUQ7WUFDM0QsMkVBQTJFO1FBQy9FLENBQUM7OztPQUFBO0lBRUQsc0JBQUksa0RBQW1COzs7O1FBQXZCO1lBQ0ksT0FBTyxJQUFJLEdBQUcsQ0FDVixJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUN4QixJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUN0QztnQkFDRCxJQUFJLEdBQUcsQ0FDSCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FDekIsQ0FBQyxFQUFFLENBQ0EsSUFBSSxDQUFDLGVBQWUsQ0FDdkIsQ0FBQztRQUNOLENBQUM7OztPQUFBO0lBRUQsc0JBQUksZ0RBQWlCOzs7O1FBQXJCO1lBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxJQUFJLGlCQUFpQixDQUFDO1FBQ2xELENBQUM7OztPQUFBO0lBRUQsc0JBQUksOENBQWU7Ozs7UUFBbkI7WUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUM7UUFDOUMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxvQ0FBSzs7OztRQUFUO1lBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQztRQUNwQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGlEQUFrQjs7OztRQUF0QjtZQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQ3pCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUM1QixDQUFDO1FBQ04sQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSx1Q0FBUTs7OztRQUFaO1lBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUM3QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDRDQUFhOzs7O1FBQWpCO1lBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQztRQUN2QyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHFDQUFNOzs7O1FBQVY7WUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO1FBQ2hDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksb0NBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7UUFDMUIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwwQ0FBVzs7OztRQUFmO1lBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztRQUNyQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHVDQUFROzs7O1FBQVo7WUFDSSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDO1FBQzdCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksb0NBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7UUFDMUIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx1Q0FBUTs7OztRQUFaO1lBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUM3QixDQUFDOzs7T0FBQTs7Z0NBck1BLFlBQVksU0FBQyxnQkFBZ0IsRUFBRSxFQUFFOztJQXNNdEMsd0JBQUM7Q0FBQSxBQWhQRCxJQWdQQztTQWhQcUIsaUJBQWlCOzs7Ozs7SUFFbkMseUNBQXlCOzs7OztJQUN6QiwwQ0FBOEI7O0lBQzlCLHlDQUFpQjs7SUFDakIsd0NBQTJCOztJQUMzQiw0Q0FBK0I7O0lBRS9CLHlDQUFrQzs7Ozs7SUFHOUIsZ0NBQTZCOzs7OztJQUM3Qiw0Q0FBaUQ7Ozs7O0lBQ2pELHdDQUFrQzs7Ozs7SUFjdEMsdUVBQXlDOzs7Ozs7SUFxQ3pDLHdEQUE4QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJpZyB9IGZyb20gJ2JpZy5qcyc7XG5cbmltcG9ydCB7IEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBzbGVlcCB9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuaW1wb3J0IHsgSUJldFJlc3VsdCB9IGZyb20gXCIuLi9tb2RlbHMvYmV0cy9pbnRlcmZhY2VzXCI7XG5pbXBvcnQgeyBJQWRkVG9CYWxhbmNlQW1vdW50cyB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJR29vZ2xlQW5hbHl0aWNzU2VydmljZSwgSU1haW5BcHBDb250ZXh0LCBJQ3VycmVuY3lTZXJ2aWNlLCBJQWxlcnRTZXJ2aWNlLCBJQmFua3JvbGxTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQXBwU2V0dGluZ3NMaXN0ZW5lciwgSUFwcFN0YXRlLCBJQXBwU2V0dGluZ3MgfSBmcm9tICcuLi9tb2RlbHMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBUb2tlblN5bWJvbCB9IGZyb20gJy4uL21vZGVscy9idXNpbmVzcy1ydWxlcy90b2tlbnMnO1xuaW1wb3J0IHsgSVNvdW5kUGxheWVyIH0gZnJvbSAnLi4vbWVkaWEvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQmV0dGluZ1Rva2VuIH0gZnJvbSAnLi4vbW9kZWxzL2J1c2luZXNzLXJ1bGVzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSVdhbGxldFNlcnZpY2VCYXNlIH0gZnJvbSAnLi4vd2FsbGV0cy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElUcmFuc2xhdGlvbkJhc2UgfSBmcm9tICcuLi90cmFuc2xhdGlvbnMvaW50ZXJmYWNlcyc7XG5cblxuXG5jb25zdCBJTkRFWF9PRl9NQVhfQ0hJUCA9IDc7XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQmV0QW1vdW50XG57XG4gICAgc3ltYm9sOnN0cmluZztcbiAgICBkZWNpbWFsOnN0cmluZztcbn1cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEdhbWVDb21wb25lbnRCYXNlIGltcGxlbWVudHMgSUFwcFNldHRpbmdzTGlzdGVuZXJcbntcbiAgICBwcml2YXRlIGF1dG9CZXROb25jZSA9IDA7XG4gICAgcHJpdmF0ZSBfaXNKYWNrcG90QmV0ID0gZmFsc2U7XG4gICAgc2VsZWN0ZWRDaGlwID0gMTtcbiAgICBwdWJsaWMgc2hvd1Rvb2x0aXAgPSBmYWxzZTtcbiAgICBwdWJsaWMgcHJvY2Vzc0JldFN0eWxlID0gZmFsc2U7XG5cbiAgICBhZGRUb0JhbGFuY2U6SUFkZFRvQmFsYW5jZUFtb3VudHM7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGFwcDpJTWFpbkFwcENvbnRleHQsXG4gICAgICAgIHByb3RlY3RlZCBnb29nbGVBbmFseXRpY3M6SUdvb2dsZUFuYWx5dGljc1NlcnZpY2UsXG4gICAgICAgIHByb3RlY3RlZCBzb3VuZFBsYXllcjpJU291bmRQbGF5ZXJcbiAgICApIHtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5hcHAuc2Nyb2xsVG9Ub3BPZlBhZ2UoKTtcblxuICAgICAgICB0aGlzLmFwcC5zZXR0aW5ncy5zZXRMaXN0ZW5lcih0aGlzKTtcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkVG9rZW4uc3ltYm9sID09ICdCRVQnKSB7XG4gICAgICAgICAgICB0aGlzLnNldHRpbmdzLnNlbGVjdEN1cnJlbmN5KCdFT1MnKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFic3RyYWN0IG9uQmV0dGluZ0N1cnJlbmN5Q2hhbmdlZCgpOnZvaWQ7XG5cbiAgICBzZWxlY3RDaGlwKGk6bnVtYmVyKXtcbiAgICAgICAgdGhpcy5zZWxlY3RlZENoaXAgPSBpO1xuXG4gICAgICAgIHRoaXMuc291bmRQbGF5ZXIucGxheSgnY2hpcFBsYWNlJyk7XG5cbiAgICAgICAgdGhpcy5vbkNoaXBTZWxlY3RlZCgpO1xuICAgIH1cbiAgICBwcm90ZWN0ZWQgb25DaGlwU2VsZWN0ZWQoKSB7fVxuXG4gICAgcHVibGljIHRvZ2dsZVRvb2x0aXAoKSB7XG4gICAgICAgIHRoaXMuc2hvd1Rvb2x0aXAgPSAhdGhpcy5zaG93VG9vbHRpcDtcbiAgICB9XG5cbiAgICBASG9zdExpc3RlbmVyKCdkb2N1bWVudDpjbGljaycsIFtdKVxuICAgIGRvY3VtZW50Q2xpY2soKSB7XG4gICAgICAgIHRoaXMuc2hvd1Rvb2x0aXAgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBzZXQgaXNKYWNrcG90QmV0KHZhbHVlOmJvb2xlYW4pIHtcbiAgICAgICAgaWYgKHRoaXMuaXNKYWNrcG90RGlzYWJsZWQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuX2lzSmFja3BvdEJldCA9IHZhbHVlO1xuICAgIH1cbiAgICBnZXQgaXNKYWNrcG90QmV0KCkge1xuICAgICAgICByZXR1cm4gIXRoaXMuaXNKYWNrcG90RGlzYWJsZWQgJiZcbiAgICAgICAgICAgICAgICB0aGlzLl9pc0phY2twb3RCZXQ7XG4gICAgfVxuXG4gICAgZ2V0IGlzSmFja3BvdERpc2FibGVkKCkge1xuICAgICAgICBjb25zdCBhbW91bnQgPSBOdW1iZXIodGhpcy5iZXRBbW91bnQuZGVjaW1hbCk7XG5cbiAgICAgICAgcmV0dXJuIGFtb3VudCA8IHRoaXMuc2VsZWN0ZWRUb2tlbi5qYWNrcG90Lm1pbmltdW1CZXQ7XG4gICAgfVxuICAgIHByb3RlY3RlZCBhYnN0cmFjdCBnZXQgYmV0QW1vdW50KCk6SUJldEFtb3VudDtcblxuXG4gICAgZ2V0U2VlZChpc0F1dG9CZXQ6Ym9vbGVhbik6c3RyaW5nIHtcbiAgICAgICAgLy8gc2VlZCBmb3IgcmFuZG9tbmVzc1xuICAgICAgICBjb25zdCBzZWVkID0gXG4gICAgICAgICAgICB0aGlzLnN0YXRlLnNlZWRGb3JSYW5kb21uZXNzICtcbiAgICAgICAgICAgIC8vIGFkZCBub25jZSBmb3IgYXV0by1iZXQgdG8gZmFpcm5lc3Mgc2VlZFxuICAgICAgICAgICAgKFxuICAgICAgICAgICAgICAgIGlzQXV0b0JldCB8fCB0cnVlID8gXG4gICAgICAgICAgICAgICAgICAgICcsJysgKCsrdGhpcy5hdXRvQmV0Tm9uY2UpIDogXG4gICAgICAgICAgICAgICAgICAgICcnXG4gICAgICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiBzZWVkO1xuICAgIH1cblxuICAgIHRyYWNrQmV0V2l0aEdvb2dsZUFuYWx5dGljcyhcbiAgICAgICAgZ2FtZU5hbWU6c3RyaW5nLFxuICAgICAgICBiZXRBbW91bnQ6c3RyaW5nLFxuICAgICAgICB0eG5JZDpzdHJpbmcsXG4gICAgICAgIGlzQXV0b0JldDpib29sZWFuXG4gICAgKSB7XG4gICAgICAgIGNvbnN0IHByaWNlID0gdGhpcy5jdXJyZW5jeS5nZXRQcmljZShcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRUb2tlbi5zeW1ib2xcbiAgICAgICAgKTtcblxuICAgICAgICBjb25zdCBhbW91bnRJblVTRDpzdHJpbmcgPVxuICAgICAgICAgICAgcHJpY2UgP1xuICAgICAgICAgICAgICAgIChwcmljZSAqIE51bWJlcihiZXRBbW91bnQpKS50b0ZpeGVkKDIpIDpcbiAgICAgICAgICAgICAgICB1bmRlZmluZWQ7XG5cbiAgICAgICAgXG4gICAgICAgIGlmIChhbW91bnRJblVTRCkge1xuICAgICAgICAgICAgdGhpcy5nb29nbGVBbmFseXRpY3MuYWRkVHJhbnNhY3Rpb24oXG4gICAgICAgICAgICAgICAgdHhuSWQsXG4gICAgICAgICAgICAgICAgZ2FtZU5hbWUsXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRva2VuLnN5bWJvbCxcbiAgICAgICAgICAgICAgICBhbW91bnRJblVTRCxcbiAgICAgICAgICAgICAgICBpc0F1dG9CZXQsXG4gICAgICAgICAgICAgICAgdGhpcy5pc0phY2twb3RCZXRcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgYXN5bmMgdXBkYXRlQmFsYW5jZXMocmVzdWx0OklCZXRSZXN1bHQpIHtcbiAgICAgICAgLyoqKiBDQUxDVUxBVEUgTkVXIFRPS0VOIEJBTEFOQ0VTIElOIEFEVkFOQ0UgIFxuICAgICAgICAgKiAgb2YgdGhlIEJldCBiZWluZyBSZXNvbHZlZCBvbi1jaGFpbiAqKiovXG5cbiAgICAgICAgY29uc3QgYmV0ID0gXG4gICAgICAgICAgICBOdW1iZXIocmVzdWx0LmFtb3VudCkgL1xuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRva2VuLmJldFRva2VuQWlyRHJvcFJhdGU7XG5cbiAgICAgICAgbGV0IHByb2ZpdCA9IHJlc3VsdC5wcm9maXQ7XG5cbiAgICAgICAgaWYgKHJlc3VsdC5pc0phY2twb3RCZXQpIHtcbiAgICAgICAgICAgIHByb2ZpdCAtPSBOdW1iZXIoXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRva2VuLmphY2twb3QudGlja2V0UHJpY2VcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICAgICAgLyoqKi9cblxuXG4gICAgICAgIHRoaXMuYWRkVG9CYWxhbmNlID0ge1xuICAgICAgICAgICAgbWFpbjogcHJvZml0LFxuICAgICAgICAgICAgYmV0XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gdG8gYW5pbWF0ZSBiYWxhbmNlIGluIGhlYWRlclxuICAgICAgICB0aGlzLmFwcC5zdGF0ZS5vbkFkZFRvQmFsYW5jZSh0cnVlLHByb2ZpdCk7XG5cbiAgICAgICAgYXdhaXQgc2xlZXAoNTAwKTtcblxuXG4gICAgICAgIC8qKiogVVBEQVRFIFRPS0VOIEJBTEFOQ0VTICoqKi9cbiAgICAgICAgLy8gdXBkYXRlIG1haW4gdG9rZW4gYmFsYW5jZVxuICAgICAgICB0aGlzLndhbGxldC5hZGRUb0JhbGFuY2UoXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkVG9rZW4uc3ltYm9sLFxuICAgICAgICAgICAgcHJvZml0XG4gICAgICAgICk7XG5cbiAgICAgICAgLy8gdXBkYXRlIEJFVCB0b2tlbiBiYWxhbmNlXG4gICAgICAgIHRoaXMud2FsbGV0LmFkZFRvQmFsYW5jZShcbiAgICAgICAgICAgIFRva2VuU3ltYm9sLkJFVCxcbiAgICAgICAgICAgIGJldFxuICAgICAgICApXG4gICAgICAgIC8qKiovXG5cblxuICAgICAgICBhd2FpdCBzbGVlcCgxNTAwKTtcblxuICAgICAgICB0aGlzLmFkZFRvQmFsYW5jZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgdGhpcy5hcHAuc3RhdGUub25BZGRUb0JhbGFuY2UoZmFsc2UpO1xuICAgIH1cblxuICAgIC8vIG9uVG9vbHRpcFNob3dCZXQoKXtcbiAgICAvLyAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0b29sdGlwQmV0XCIpLnNldEF0dHJpYnV0ZSgnZGF0YS10b29sdGlwJywgJ2FhYScpO1xuICAgIC8vIH1cbiAgICBnZXQgdG9vbHRpcEJldCgpe1xuICAgICAgICBjb25zdCB0b2tlbkFsdCA9IHRoaXMuc2VsZWN0ZWRUb2tlbi5zeW1ib2w7XG4gICAgICAgIGNvbnN0IGFpckRyb3AgPSB0aGlzLnNlbGVjdGVkVG9rZW4uYmV0VG9rZW5BaXJEcm9wUmF0ZTtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MudHJhbnNsYXRpb24udG9vbHRpcERpdmlkZW5kcyhcIjFcIixgJHthaXJEcm9wfSAke3Rva2VuQWx0fWApXG4gICAgfVxuXG4gICAgZ2V0IGphY2twb3RUb29sVGlwKCkge1xuICAgICAgICBjb25zdCB0b2tlbiA9IHRoaXMuc2VsZWN0ZWRUb2tlbjtcbiAgICAgICAgY29uc3QgamFja3BvdCA9IHRva2VuLmphY2twb3Q7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MudHJhbnNsYXRpb24uY2xpY2tUaGlzQm94VG9SZWNlaXZlMUphY2twb3RTcGluRm9yUHJpY2UoXG4gICAgICAgICAgICBOdW1iZXIoamFja3BvdC50aWNrZXRQcmljZSkrXCIgXCIrdG9rZW4uc3ltYm9sXG4gICAgICAgICk7XG4gICAgICAgIC8vIEN1cnJlbnRseSBub3QgdXNpbmcgYSBzZXBhcmF0ZSBtaW5pbXVtIGJldCBmb3IgamFja3BvdCwgXG4gICAgICAgICAgICAvLyBpdCBpcyBjdXJyZW50bHkgdGhlIHNhbWUgYXMgdGhlIHN0YW5kYXJkIG1pbmltdW0gYmV0XG4gICAgICAgIC8vXCJCZXQgbXVzdCBiZSBhdCBsZWFzdCBcIitqYWNrcG90Lm1pbmltdW1CZXQrXCIgXCIrdG9rZW4uc3ltYm9sK1wiIHRvIGVudGVyLlwiO1xuICAgIH1cblxuICAgIGdldCBpc0luc3VmZmljaWVudEZ1bmRzKCk6Ym9vbGVhbiB7XG4gICAgICAgIHJldHVybiBuZXcgQmlnKFxuICAgICAgICAgICAgdGhpcy5jdXJyZW5jeUJhbGFuY2UpLmx0KFxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRva2VuLm1pbmltdW1CZXRBbW91bnRcbiAgICAgICAgKSB8fFxuICAgICAgICBuZXcgQmlnKFxuICAgICAgICAgICAgdGhpcy5iZXRBbW91bnQuZGVjaW1hbFxuICAgICAgICApLmd0KFxuICAgICAgICAgICAgdGhpcy5jdXJyZW5jeUJhbGFuY2VcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBnZXQgaXNNYXhDaGlwU2VsZWN0ZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkQ2hpcCA9PSBJTkRFWF9PRl9NQVhfQ0hJUDtcbiAgICB9XG5cbiAgICBnZXQgY3VycmVuY3lCYWxhbmNlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5zZWxlY3RlZFRva2VuQmFsYW5jZTtcbiAgICB9XG5cbiAgICBnZXQgY2hpcHMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkVG9rZW4uY2hpcHM7XG4gICAgfVxuXG4gICAgZ2V0IHNlbGVjdGVkVG9rZW5QcmljZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVuY3kuZ2V0UHJpY2UoXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkVG9rZW4uc3ltYm9sXG4gICAgICAgICk7XG4gICAgfVxuICAgIGdldCBjdXJyZW5jeSgpOklDdXJyZW5jeVNlcnZpY2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5hcHAuY3VycmVuY3k7XG4gICAgfVxuXG4gICAgZ2V0IHNlbGVjdGVkVG9rZW4oKTpJQmV0dGluZ1Rva2VuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3Muc2VsZWN0ZWRUb2tlbjtcbiAgICB9XG5cbiAgICBnZXQgd2FsbGV0KCk6SVdhbGxldFNlcnZpY2VCYXNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3Mud2FsbGV0O1xuICAgIH1cblxuICAgIGdldCBzdGF0ZSgpOklBcHBTdGF0ZSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC5zdGF0ZTtcbiAgICB9XG5cbiAgICBnZXQgdHJhbnNsYXRpb24oKTpJVHJhbnNsYXRpb25CYXNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MudHJhbnNsYXRpb247XG4gICAgfVxuXG4gICAgZ2V0IHNldHRpbmdzKCk6SUFwcFNldHRpbmdzIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBwLnNldHRpbmdzO1xuICAgIH1cblxuICAgIGdldCBhbGVydCgpOklBbGVydFNlcnZpY2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5hcHAuYWxlcnQ7XG4gICAgfVxuXG4gICAgZ2V0IGJhbmtyb2xsKCk6SUJhbmtyb2xsU2VydmljZSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC5iYW5rcm9sbDtcbiAgICB9XG59Il19