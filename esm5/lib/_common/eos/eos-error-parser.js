/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/eos/eos-error-parser.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var EosErrorParser = /** @class */ (function () {
    function EosErrorParser(data) {
        this._message = data.message;
        try {
            data = JSON.parse(data);
            if (data.error && data.error.details) {
                this._message = data.error.details[0].message;
            }
        }
        catch (error) {
        }
    }
    Object.defineProperty(EosErrorParser.prototype, "isIncorrectNonce", {
        get: /**
         * @return {?}
         */
        function () {
            return this.message ==
                'assertion failure with message: Incorrect nonce.';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EosErrorParser.prototype, "message", {
        get: /**
         * @return {?}
         */
        function () {
            return this._message;
        },
        enumerable: true,
        configurable: true
    });
    return EosErrorParser;
}());
export { EosErrorParser };
if (false) {
    /**
     * @type {?}
     * @private
     */
    EosErrorParser.prototype._message;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLWVycm9yLXBhcnNlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9lb3MvZW9zLWVycm9yLXBhcnNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0lBSUksd0JBQVksSUFBUTtRQUNoQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFFN0IsSUFBSTtZQUNBLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXhCLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtnQkFDbEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7YUFDakQ7U0FDSjtRQUFDLE9BQU8sS0FBSyxFQUFFO1NBQ2Y7SUFDTCxDQUFDO0lBRUQsc0JBQUksNENBQWdCOzs7O1FBQXBCO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTztnQkFDZixrREFBa0QsQ0FBQztRQUMzRCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLG1DQUFPOzs7O1FBQVg7WUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekIsQ0FBQzs7O09BQUE7SUFDTCxxQkFBQztBQUFELENBQUMsQUF6QkQsSUF5QkM7Ozs7Ozs7SUF2Qkcsa0NBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEVvc0Vycm9yUGFyc2VyXG57XG4gICAgcHJpdmF0ZSBfbWVzc2FnZTpzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcihkYXRhOmFueSkge1xuICAgICAgICB0aGlzLl9tZXNzYWdlID0gZGF0YS5tZXNzYWdlO1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZShkYXRhKTtcblxuICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3IgJiYgZGF0YS5lcnJvci5kZXRhaWxzKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fbWVzc2FnZSA9IGRhdGEuZXJyb3IuZGV0YWlsc1swXS5tZXNzYWdlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0IGlzSW5jb3JyZWN0Tm9uY2UoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm1lc3NhZ2UgPT1cbiAgICAgICAgICAgICdhc3NlcnRpb24gZmFpbHVyZSB3aXRoIG1lc3NhZ2U6IEluY29ycmVjdCBub25jZS4nO1xuICAgIH1cblxuICAgIGdldCBtZXNzYWdlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fbWVzc2FnZTtcbiAgICB9XG59Il19