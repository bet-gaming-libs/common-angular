/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/eos/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEosService() { }
if (false) {
    /** @type {?} */
    IEosService.prototype.network;
    /** @type {?} */
    IEosService.prototype.httpsHost;
    /** @type {?} */
    IEosService.prototype.config;
    /** @type {?} */
    IEosService.prototype.accounts;
    /**
     * @param {?} tokenContract
     * @param {?} tokenHolderAccount
     * @param {?} tokenSymbol
     * @param {?=} table
     * @return {?}
     */
    IEosService.prototype.getTokenBalance = function (tokenContract, tokenHolderAccount, tokenSymbol, table) { };
    /**
     * @param {?} easyAccountId
     * @return {?}
     */
    IEosService.prototype.getEasyAccountInfoRow = function (easyAccountId) { };
    /**
     * @param {?} tokenSymbol
     * @param {?} easyAccountId
     * @return {?}
     */
    IEosService.prototype.getEasyAccountCurrencyBalanceRow = function (tokenSymbol, easyAccountId) { };
    /**
     * @param {?} easyAccountId
     * @return {?}
     */
    IEosService.prototype.getEasyAccountBetBalanceRow = function (easyAccountId) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9lb3MvaW50ZXJmYWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUtBLGlDQTBCQzs7O0lBeEJHLDhCQU1FOztJQUNGLGdDQUFpQjs7SUFFakIsNkJBQXFDOztJQUVyQywrQkFBbUM7Ozs7Ozs7O0lBR25DLDZHQUtrQjs7Ozs7SUFFbEIsMkVBQTJFOzs7Ozs7SUFDM0UsbUdBQW9IOzs7OztJQUNwSCxpRkFBdUYiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0lFb3NDb25maWdQYXJhbWV0ZXJzLCBJRWFzeUFjY291bnRCZXRCYWxhbmNlUm93LCBJRWFzeUFjY291bnRJbmZvUm93LCBJRWFzeUFjY291bnRDdXJyZW5jeUJhbGFuY2VSb3d9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuaW1wb3J0IHsgSUVvc0FjY291bnROYW1lcyB9IGZyb20gJy4uL21vZGVscy9idXNpbmVzcy1ydWxlcy9pbnRlcmZhY2VzJztcblxuXG5leHBvcnQgaW50ZXJmYWNlIElFb3NTZXJ2aWNlXG57XG4gICAgcmVhZG9ubHkgbmV0d29yazoge1xuICAgICAgICBibG9ja2NoYWluOnN0cmluZztcbiAgICAgICAgaG9zdDpzdHJpbmc7XG4gICAgICAgIHBvcnQ6bnVtYmVyO1xuICAgICAgICBwcm90b2NvbDpzdHJpbmc7XG4gICAgICAgIGNoYWluSWQ6c3RyaW5nO1xuICAgIH07XG4gICAgaHR0cHNIb3N0OnN0cmluZztcblxuICAgIHJlYWRvbmx5IGNvbmZpZzpJRW9zQ29uZmlnUGFyYW1ldGVycztcblxuICAgIHJlYWRvbmx5IGFjY291bnRzOklFb3NBY2NvdW50TmFtZXM7XG5cblxuICAgIGdldFRva2VuQmFsYW5jZShcbiAgICAgICAgdG9rZW5Db250cmFjdDpzdHJpbmcsXG4gICAgICAgIHRva2VuSG9sZGVyQWNjb3VudDpzdHJpbmcsXG4gICAgICAgIHRva2VuU3ltYm9sOnN0cmluZyxcbiAgICAgICAgdGFibGU/OnN0cmluZ1xuICAgICk6UHJvbWlzZTxzdHJpbmc+OyAgXG5cbiAgICBnZXRFYXN5QWNjb3VudEluZm9Sb3coZWFzeUFjY291bnRJZDpzdHJpbmcpOlByb21pc2U8SUVhc3lBY2NvdW50SW5mb1Jvd1tdPjtcbiAgICBnZXRFYXN5QWNjb3VudEN1cnJlbmN5QmFsYW5jZVJvdyh0b2tlblN5bWJvbDpzdHJpbmcsZWFzeUFjY291bnRJZDpzdHJpbmcpOlByb21pc2U8SUVhc3lBY2NvdW50Q3VycmVuY3lCYWxhbmNlUm93W10+O1xuICAgIGdldEVhc3lBY2NvdW50QmV0QmFsYW5jZVJvdyhlYXN5QWNjb3VudElkOnN0cmluZyk6UHJvbWlzZTxJRWFzeUFjY291bnRCZXRCYWxhbmNlUm93W10+O1xufSJdfQ==