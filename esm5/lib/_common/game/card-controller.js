/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/game/card-controller.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var SUITS = ['hearts', 'diamonds', 'spades', 'clubs'];
var CardController = /** @class */ (function () {
    function CardController(soundPlayer) {
        this.soundPlayer = soundPlayer;
        this._dealt = false;
        this._flipped = false;
        this._isRevealed = false;
    }
    /**
     * @return {?}
     */
    CardController.prototype.deal = /**
     * @return {?}
     */
    function () {
        this._dealt = true;
        this.soundPlayer.play('cardPlace');
    };
    /**
     * @param {?} data
     * @param {?=} rankNumber
     * @param {?=} cardId
     * @return {?}
     */
    CardController.prototype.reveal = /**
     * @param {?} data
     * @param {?=} rankNumber
     * @param {?=} cardId
     * @return {?}
     */
    function (data, rankNumber, cardId) {
        if (rankNumber === void 0) { rankNumber = undefined; }
        if (cardId === void 0) { cardId = undefined; }
        this._rank = data.rankName.toLowerCase();
        this._suit = SUITS[data.suit];
        this._rankNumber = rankNumber;
        this._cardId = cardId;
        this._isRevealed = true;
    };
    /**
     * @return {?}
     */
    CardController.prototype.flip = /**
     * @return {?}
     */
    function () {
        this._flipped = true;
        this.soundPlayer.play('cardFlip');
    };
    /**
     * @return {?}
     */
    CardController.prototype.unflip = /**
     * @return {?}
     */
    function () {
        this._flipped = false;
        this.soundPlayer.play('cardFlip');
    };
    /**
     * @return {?}
     */
    CardController.prototype.dealAndFlip = /**
     * @return {?}
     */
    function () {
        this._dealt = true;
        this._flipped = true;
        this.soundPlayer.play('cardFlip');
    };
    Object.defineProperty(CardController.prototype, "dealt", {
        get: /**
         * @return {?}
         */
        function () {
            return this._dealt;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardController.prototype, "flipped", {
        get: /**
         * @return {?}
         */
        function () {
            return this._flipped;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardController.prototype, "suit", {
        get: /**
         * @return {?}
         */
        function () {
            return this._suit;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardController.prototype, "card", {
        get: /**
         * @return {?}
         */
        function () {
            return this.rank;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardController.prototype, "rank", {
        get: /**
         * @return {?}
         */
        function () {
            return this._rank;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardController.prototype, "rankNumber", {
        get: /**
         * @return {?}
         */
        function () {
            return this._rankNumber;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardController.prototype, "cardId", {
        get: /**
         * @return {?}
         */
        function () {
            return this._cardId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardController.prototype, "isRevealed", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isRevealed;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardController.prototype, "data", {
        get: /**
         * @return {?}
         */
        function () {
            return {
                suit: this.suit,
                dealt: this._dealt,
                flipped: this.flipped,
                card: this.card
            };
        },
        enumerable: true,
        configurable: true
    });
    return CardController;
}());
export { CardController };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._dealt;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._flipped;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._isRevealed;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._suit;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._rank;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._rankNumber;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype._cardId;
    /**
     * @type {?}
     * @private
     */
    CardController.prototype.soundPlayer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL2dhbWUvY2FyZC1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztJQU1NLEtBQUssR0FBRyxDQUFDLFFBQVEsRUFBQyxVQUFVLEVBQUMsUUFBUSxFQUFDLE9BQU8sQ0FBQztBQUdwRDtJQVdJLHdCQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVRwQyxXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2YsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixnQkFBVyxHQUFHLEtBQUssQ0FBQztJQVE1QixDQUFDOzs7O0lBRUQsNkJBQUk7OztJQUFKO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7OztJQUVELCtCQUFNOzs7Ozs7SUFBTixVQUNJLElBQWMsRUFDZCxVQUEyQixFQUMzQixNQUF1QjtRQUR2QiwyQkFBQSxFQUFBLHNCQUEyQjtRQUMzQix1QkFBQSxFQUFBLGtCQUF1QjtRQUV2QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDO1FBQzlCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBRXRCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCw2QkFBSTs7O0lBQUo7UUFDSSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7O0lBQ0QsK0JBQU07OztJQUFOO1FBQ0ksSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7OztJQUVELG9DQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFFRCxzQkFBSSxpQ0FBSzs7OztRQUFUO1lBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUM7OztPQUFBO0lBQ0Qsc0JBQUksbUNBQU87Ozs7UUFBWDtZQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGdDQUFJOzs7O1FBQVI7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSxnQ0FBSTs7OztRQUFSO1lBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3JCLENBQUM7OztPQUFBO0lBQ0Qsc0JBQUksZ0NBQUk7Ozs7UUFBUjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDOzs7T0FBQTtJQUNELHNCQUFJLHNDQUFVOzs7O1FBQWQ7WUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDNUIsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSxrQ0FBTTs7OztRQUFWO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3hCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksc0NBQVU7Ozs7UUFBZDtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUM1QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGdDQUFJOzs7O1FBQVI7WUFDSSxPQUFPO2dCQUNILElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtnQkFDZixLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU07Z0JBQ2xCLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztnQkFDckIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO2FBQ2xCLENBQUM7UUFDTixDQUFDOzs7T0FBQTtJQUNMLHFCQUFDO0FBQUQsQ0FBQyxBQWxGRCxJQWtGQzs7Ozs7OztJQWhGRyxnQ0FBdUI7Ozs7O0lBQ3ZCLGtDQUF5Qjs7Ozs7SUFDekIscUNBQTRCOzs7OztJQUU1QiwrQkFBcUI7Ozs7O0lBQ3JCLCtCQUFxQjs7Ozs7SUFDckIscUNBQTJCOzs7OztJQUMzQixpQ0FBdUI7Ozs7O0lBRVgscUNBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSUNhcmREYXRhIH0gZnJvbSBcImVhcm5iZXQtY29tbW9uXCI7XG5cbmltcG9ydCB7IElDYXJkQ29udHJvbGxlciB9IGZyb20gXCIuL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IElTb3VuZFBsYXllciB9IGZyb20gJy4uL21lZGlhL2ludGVyZmFjZXMnO1xuXG5cbmNvbnN0IFNVSVRTID0gWydoZWFydHMnLCdkaWFtb25kcycsJ3NwYWRlcycsJ2NsdWJzJ107XG5cblxuZXhwb3J0IGNsYXNzIENhcmRDb250cm9sbGVyXG57XG4gICAgcHJpdmF0ZSBfZGVhbHQgPSBmYWxzZTtcbiAgICBwcml2YXRlIF9mbGlwcGVkID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBfaXNSZXZlYWxlZCA9IGZhbHNlO1xuXG4gICAgcHJpdmF0ZSBfc3VpdDpzdHJpbmc7XG4gICAgcHJpdmF0ZSBfcmFuazpzdHJpbmc7XG4gICAgcHJpdmF0ZSBfcmFua051bWJlcjpudW1iZXI7XG4gICAgcHJpdmF0ZSBfY2FyZElkOm51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc291bmRQbGF5ZXI6SVNvdW5kUGxheWVyKSB7IFxuICAgIH1cblxuICAgIGRlYWwoKSB7XG4gICAgICAgIHRoaXMuX2RlYWx0ID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5zb3VuZFBsYXllci5wbGF5KCdjYXJkUGxhY2UnKTtcbiAgICB9XG5cbiAgICByZXZlYWwoXG4gICAgICAgIGRhdGE6SUNhcmREYXRhLFxuICAgICAgICByYW5rTnVtYmVyOm51bWJlcj11bmRlZmluZWQsXG4gICAgICAgIGNhcmRJZDpudW1iZXI9dW5kZWZpbmVkXG4gICAgKSB7XG4gICAgICAgIHRoaXMuX3JhbmsgPSBkYXRhLnJhbmtOYW1lLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgIHRoaXMuX3N1aXQgPSBTVUlUU1tkYXRhLnN1aXRdO1xuICAgICAgICB0aGlzLl9yYW5rTnVtYmVyID0gcmFua051bWJlcjtcbiAgICAgICAgdGhpcy5fY2FyZElkID0gY2FyZElkO1xuXG4gICAgICAgIHRoaXMuX2lzUmV2ZWFsZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIGZsaXAoKSB7XG4gICAgICAgIHRoaXMuX2ZsaXBwZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLnNvdW5kUGxheWVyLnBsYXkoJ2NhcmRGbGlwJyk7XG4gICAgfVxuICAgIHVuZmxpcCgpIHtcbiAgICAgICAgdGhpcy5fZmxpcHBlZCA9IGZhbHNlO1xuICAgICAgICB0aGlzLnNvdW5kUGxheWVyLnBsYXkoJ2NhcmRGbGlwJyk7XG4gICAgfVxuXG4gICAgZGVhbEFuZEZsaXAoKSB7XG4gICAgICAgIHRoaXMuX2RlYWx0ID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5fZmxpcHBlZCA9IHRydWU7XG4gICAgICAgIHRoaXMuc291bmRQbGF5ZXIucGxheSgnY2FyZEZsaXAnKTtcbiAgICB9XG5cbiAgICBnZXQgZGVhbHQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9kZWFsdDtcbiAgICB9XG4gICAgZ2V0IGZsaXBwZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9mbGlwcGVkO1xuICAgIH1cblxuICAgIGdldCBzdWl0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fc3VpdDtcbiAgICB9XG4gICAgZ2V0IGNhcmQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJhbms7XG4gICAgfVxuICAgIGdldCByYW5rKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fcmFuaztcbiAgICB9XG4gICAgZ2V0IHJhbmtOdW1iZXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9yYW5rTnVtYmVyO1xuICAgIH1cbiAgICBnZXQgY2FyZElkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fY2FyZElkO1xuICAgIH1cblxuICAgIGdldCBpc1JldmVhbGVkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faXNSZXZlYWxlZDtcbiAgICB9XG5cbiAgICBnZXQgZGF0YSgpOklDYXJkQ29udHJvbGxlciB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBzdWl0OiB0aGlzLnN1aXQsXG4gICAgICAgICAgICBkZWFsdDogdGhpcy5fZGVhbHQsXG4gICAgICAgICAgICBmbGlwcGVkOiB0aGlzLmZsaXBwZWQsXG4gICAgICAgICAgICBjYXJkOiB0aGlzLmNhcmRcbiAgICAgICAgfTtcbiAgICB9XG59Il19