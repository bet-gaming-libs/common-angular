/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/math/integer-math.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Big } from 'big.js';
import { roundDownWithoutTrailingZeros } from '../util/math-util';
var IntBasedNum = /** @class */ (function () {
    function IntBasedNum(precision, decimalValue, bigDecimal) {
        if (decimalValue === void 0) { decimalValue = 0; }
        if (bigDecimal === void 0) { bigDecimal = undefined; }
        this.precision = precision;
        /** @type {?} */
        var decimal = bigDecimal == undefined ?
            new Big(decimalValue).round(precision, 0 /* RoundDown */) :
            bigDecimal;
        this.factor = Math.pow(10, precision);
        this._integer = decimal.times(this.factor).round(0, 0 /* RoundDown */);
    }
    // CLASS
    // CLASS
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    IntBasedNum.fromInteger = 
    // CLASS
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    function (integer, precision) {
        return new IntBasedNum(precision, undefined, new Big(integer).div(Math.pow(10, precision)).round(precision, 0 /* RoundDown */));
    };
    /*** MUTATING METHODS ***/
    /**
     * MUTATING METHODS **
     * @param {?} other
     * @return {?}
     */
    IntBasedNum.prototype.addAndReplace = /**
     * MUTATING METHODS **
     * @param {?} other
     * @return {?}
     */
    function (other) {
        if (this.precision != other.precision) {
            throw new Error('precision of both operands must be the same!');
        }
        /** @type {?} */
        var sum = this.integer.plus(other.integer);
        this._integer = sum;
        //return this;
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} other
     * @return {THIS}
     */
    IntBasedNum.prototype.subtractAndReplace = /**
     * @template THIS
     * @this {THIS}
     * @param {?} other
     * @return {THIS}
     */
    function (other) {
        if ((/** @type {?} */ (this)).precision != other.precision) {
            throw new Error('precision of both operands must be the same!');
        }
        /** @type {?} */
        var diff = (/** @type {?} */ (this)).integer.minus(other.integer);
        (/** @type {?} */ (this))._integer = diff;
        return (/** @type {?} */ (this));
    };
    /***/
    /**
     *
     * @param {?} other
     * @return {?}
     */
    IntBasedNum.prototype.multiply = /**
     *
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return integerMultiplication(this, other);
    };
    /**
     * @param {?} decimal
     * @return {?}
     */
    IntBasedNum.prototype.multiplyDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        /** @type {?} */
        var other = new IntBasedNum(this.precision, decimal);
        return integerMultiplication(this, other);
    };
    /**
     * @param {?} decimal
     * @return {?}
     */
    IntBasedNum.prototype.divideByDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        /** @type {?} */
        var other = new IntBasedNum(this.precision, decimal.toString());
        return integerDivision(this, other);
    };
    Object.defineProperty(IntBasedNum.prototype, "reciprocal", {
        get: /**
         * @return {?}
         */
        function () {
            if (this._reciprocal == undefined) {
                this._reciprocal = integerDivision(new IntBasedNum(this.precision, '1'), this);
            }
            return this._reciprocal;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IntBasedNum.prototype, "opposite_", {
        get: /**
         * @return {?}
         */
        function () {
            return integerMultiplication(new IntBasedNum(this.precision, '-1'), this);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IntBasedNum.prototype, "decimalAsNumber", {
        get: /**
         * @return {?}
         */
        function () {
            return Number(this.decimal);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IntBasedNum.prototype, "integer", {
        get: /**
         * @return {?}
         */
        function () {
            return this._integer;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    IntBasedNum.prototype.toString = /**
     * @return {?}
     */
    function () {
        return this.decimal;
    };
    Object.defineProperty(IntBasedNum.prototype, "decimalWithoutTrailingZeros", {
        get: /**
         * @return {?}
         */
        function () {
            return roundDownWithoutTrailingZeros(this.decimal, this.precision);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IntBasedNum.prototype, "decimal", {
        get: /**
         * @return {?}
         */
        function () {
            Big.RM = 0 /* RoundDown */;
            return this.integer.div(this.factor).toFixed(this.precision);
        },
        enumerable: true,
        configurable: true
    });
    return IntBasedNum;
}());
export { IntBasedNum };
if (false) {
    /** @type {?} */
    IntBasedNum.prototype.factor;
    /**
     * @type {?}
     * @private
     */
    IntBasedNum.prototype._integer;
    /**
     * @type {?}
     * @private
     */
    IntBasedNum.prototype._reciprocal;
    /** @type {?} */
    IntBasedNum.prototype.precision;
}
var AssetAmount = /** @class */ (function (_super) {
    tslib_1.__extends(AssetAmount, _super);
    function AssetAmount(data) {
        var _this = this;
        /** @type {?} */
        var parts = data.split(' ');
        /** @type {?} */
        var decimal = parts[0];
        /** @type {?} */
        var symbol = parts[1];
        /** @type {?} */
        var precision = decimal.split('.')[1].length;
        _this = _super.call(this, precision, decimal) || this;
        _this._symbol = symbol;
        _this._isZero = Number(decimal) == 0;
        return _this;
    }
    Object.defineProperty(AssetAmount.prototype, "isZero", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isZero;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetAmount.prototype, "symbol", {
        get: /**
         * @return {?}
         */
        function () {
            return this._symbol;
        },
        enumerable: true,
        configurable: true
    });
    return AssetAmount;
}(IntBasedNum));
export { AssetAmount };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AssetAmount.prototype._symbol;
    /**
     * @type {?}
     * @private
     */
    AssetAmount.prototype._isZero;
}
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerAddition(a, b) {
    if (a.precision != b.precision) {
        throw new Error('precision of a: ' + a.precision +
            ', precision of b: ' + b.precision +
            "\nprecision of both operands must be the same!");
    }
    /** @type {?} */
    var sum = a.integer.plus(b.integer);
    return IntBasedNum.fromInteger(sum.toString(), a.precision);
}
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerSubtraction(a, b) {
    if (a.precision != b.precision) {
        throw new Error('precision of both operands must be the same!');
    }
    /** @type {?} */
    var diff = a.integer.minus(b.integer);
    return IntBasedNum.fromInteger(diff.toString(), a.precision);
}
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerMultiplication(a, b) {
    if (a.precision < b.precision) {
        console.log(a.decimal);
        console.log(b.decimal);
        throw new Error('precision of a: ' + a.precision +
            ', precision of b: ' + b.precision +
            "\nprecision of A must be >= B!");
    }
    /** @type {?} */
    var product = a.integer.times(b.integer);
    /** @type {?} */
    var integer = product.div(b.factor)
        .round(0, 0 /* RoundDown */)
        .toString();
    /** @type {?} */
    var result = IntBasedNum.fromInteger(integer, a.precision);
    //console.log('INT Multiplication: '+a+' * '+b+' = '+result);
    return result;
}
// when doing division between two eos numbers then disregard
// the decimal after the integer
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerDivision(a, b) {
    if (a.precision < b.precision) {
        throw new Error('precision of A must be >= B!');
    }
    /** @type {?} */
    var quotient = a.integer.div(b.integer);
    /** @type {?} */
    var integer = quotient.times(b.factor)
        .round(0, 0 /* RoundDown */)
        .toString();
    /** @type {?} */
    var result = IntBasedNum.fromInteger(integer, a.precision);
    console.log('INT Division: ' + a + ' / ' + b + ' = ' + result);
    return result;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZWdlci1tYXRoLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL21hdGgvaW50ZWdlci1tYXRoLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQVEsRUFBRSxHQUFHLEVBQWdCLE1BQU0sUUFBUSxDQUFDO0FBQzVDLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBR2xFO0lBMEJJLHFCQUNhLFNBQWdCLEVBQ3pCLFlBQThCLEVBQzlCLFVBQTBCO1FBRDFCLDZCQUFBLEVBQUEsZ0JBQThCO1FBQzlCLDJCQUFBLEVBQUEsc0JBQTBCO1FBRmpCLGNBQVMsR0FBVCxTQUFTLENBQU87O1lBSW5CLE9BQU8sR0FDVCxVQUFVLElBQUksU0FBUyxDQUFDLENBQUM7WUFDckIsSUFBSSxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUN2QixTQUFTLG9CQUVaLENBQUMsQ0FBQztZQUNILFVBQVU7UUFFbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBQyxTQUFTLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLG9CQUF3QixDQUFDO0lBQy9FLENBQUM7SUF2Q0QsUUFBUTs7Ozs7OztJQUNELHVCQUFXOzs7Ozs7O0lBQWxCLFVBQ0ksT0FBYyxFQUNkLFNBQWdCO1FBRWhCLE9BQU8sSUFBSSxXQUFXLENBQ2xCLFNBQVMsRUFDVCxTQUFTLEVBQ1QsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBQyxTQUFTLENBQUMsQ0FDekIsQ0FBQyxLQUFLLENBQ0gsU0FBUyxvQkFFWixDQUNKLENBQUM7SUFDTixDQUFDO0lBMkJELDBCQUEwQjs7Ozs7O0lBQzFCLG1DQUFhOzs7OztJQUFiLFVBQWMsS0FBaUI7UUFDM0IsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDbkMsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1NBQ25FOztZQUdLLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQzVDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO1FBRXBCLGNBQWM7SUFDbEIsQ0FBQzs7Ozs7OztJQUVELHdDQUFrQjs7Ozs7O0lBQWxCLFVBQW1CLEtBQWlCO1FBQ2hDLElBQUksbUJBQUEsSUFBSSxFQUFBLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDbkMsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1NBQ25FOztZQUdLLElBQUksR0FBRyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7UUFDOUMsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUVyQixPQUFPLG1CQUFBLElBQUksRUFBQSxDQUFDO0lBQ2hCLENBQUM7SUFDRCxLQUFLOzs7Ozs7SUFHTCw4QkFBUTs7Ozs7SUFBUixVQUFTLEtBQWlCO1FBQ3RCLE9BQU8scUJBQXFCLENBQ3hCLElBQUksRUFDSixLQUFLLENBQ1IsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQscUNBQWU7Ozs7SUFBZixVQUFnQixPQUFjOztZQUNwQixLQUFLLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBQyxPQUFPLENBQUM7UUFFckQsT0FBTyxxQkFBcUIsQ0FDeEIsSUFBSSxFQUNKLEtBQUssQ0FDUixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFRCxxQ0FBZTs7OztJQUFmLFVBQWdCLE9BQWM7O1lBQ3BCLEtBQUssR0FBRyxJQUFJLFdBQVcsQ0FDekIsSUFBSSxDQUFDLFNBQVMsRUFDZCxPQUFPLENBQUMsUUFBUSxFQUFFLENBQ3JCO1FBRUQsT0FBTyxlQUFlLENBQ2xCLElBQUksRUFDSixLQUFLLENBQ1IsQ0FBQztJQUNOLENBQUM7SUFHRCxzQkFBSSxtQ0FBVTs7OztRQUFkO1lBQ0ksSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLFNBQVMsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQzlCLElBQUksV0FBVyxDQUNYLElBQUksQ0FBQyxTQUFTLEVBQ2QsR0FBRyxDQUNOLEVBQ0QsSUFBSSxDQUNQLENBQUM7YUFDTDtZQUVELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUM1QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGtDQUFTOzs7O1FBQWI7WUFDSSxPQUFPLHFCQUFxQixDQUN4QixJQUFJLFdBQVcsQ0FDWCxJQUFJLENBQUMsU0FBUyxFQUNkLElBQUksQ0FDUCxFQUNELElBQUksQ0FDUCxDQUFDO1FBQ04sQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx3Q0FBZTs7OztRQUFuQjtZQUNJLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNoQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGdDQUFPOzs7O1FBQVg7WUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekIsQ0FBQzs7O09BQUE7Ozs7SUFFRCw4QkFBUTs7O0lBQVI7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQUVELHNCQUFJLG9EQUEyQjs7OztRQUEvQjtZQUNJLE9BQU8sNkJBQTZCLENBQ2hDLElBQUksQ0FBQyxPQUFPLEVBQ1osSUFBSSxDQUFDLFNBQVMsQ0FDakIsQ0FBQztRQUNOLENBQUM7OztPQUFBO0lBRUQsc0JBQUksZ0NBQU87Ozs7UUFBWDtZQUNJLEdBQUcsQ0FBQyxFQUFFLG9CQUF5QixDQUFDO1lBRWhDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQ25CLElBQUksQ0FBQyxNQUFNLENBQ2QsQ0FBQyxPQUFPLENBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FDakIsQ0FBQztRQUNOLENBQUM7OztPQUFBO0lBb0JMLGtCQUFDO0FBQUQsQ0FBQyxBQTNLRCxJQTJLQzs7OztJQXRKRyw2QkFBdUI7Ozs7O0lBQ3ZCLCtCQUFxQjs7Ozs7SUFFckIsa0NBQWdDOztJQUc1QixnQ0FBeUI7O0FBa0pqQztJQUFpQyx1Q0FBVztJQU94QyxxQkFBWSxJQUFXO1FBQXZCLGlCQWVDOztZQWRTLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs7WUFFdkIsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7O1lBQ2xCLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDOztZQUVqQixTQUFTLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNO1FBRzlDLFFBQUEsa0JBQU0sU0FBUyxFQUFDLE9BQU8sQ0FBQyxTQUFDO1FBR3pCLEtBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBRXRCLEtBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzs7SUFDeEMsQ0FBQztJQUVELHNCQUFJLCtCQUFNOzs7O1FBQVY7WUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDeEIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQkFBTTs7OztRQUFWO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3hCLENBQUM7OztPQUFBO0lBQ0wsa0JBQUM7QUFBRCxDQUFDLEFBL0JELENBQWlDLFdBQVcsR0ErQjNDOzs7Ozs7O0lBNUJHLDhCQUF1Qjs7Ozs7SUFFdkIsOEJBQXdCOzs7Ozs7Ozs7QUErQjVCLHlCQUNJLENBQUcsRUFBQyxDQUFHO0lBRVAsSUFBSSxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxTQUFTLEVBQUU7UUFDNUIsTUFBTSxJQUFJLEtBQUssQ0FDWCxrQkFBa0IsR0FBRyxDQUFDLENBQUMsU0FBUztZQUNoQyxvQkFBb0IsR0FBRyxDQUFDLENBQUMsU0FBUztZQUNsQyxnREFBZ0QsQ0FDbkQsQ0FBQztLQUNMOztRQUlLLEdBQUcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO0lBRXJDLE9BQU8sV0FBVyxDQUFDLFdBQVcsQ0FDMUIsR0FBRyxDQUFDLFFBQVEsRUFBRSxFQUNkLENBQUMsQ0FBQyxTQUFTLENBQ2QsQ0FBQztBQUNOLENBQUM7Ozs7Ozs7O0FBR0QsNEJBQ0ksQ0FBRyxFQUFDLENBQUc7SUFFUCxJQUFJLENBQUMsQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLFNBQVMsRUFBRTtRQUM1QixNQUFNLElBQUksS0FBSyxDQUFDLDhDQUE4QyxDQUFDLENBQUM7S0FDbkU7O1FBSUssSUFBSSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7SUFFdkMsT0FBTyxXQUFXLENBQUMsV0FBVyxDQUMxQixJQUFJLENBQUMsUUFBUSxFQUFFLEVBQ2YsQ0FBQyxDQUFDLFNBQVMsQ0FDZCxDQUFDO0FBQ04sQ0FBQzs7Ozs7Ozs7QUFHRCwrQkFDSSxDQUFHLEVBQUMsQ0FBRztJQUVQLElBQUksQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXZCLE1BQU0sSUFBSSxLQUFLLENBQ1gsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLFNBQVM7WUFDaEMsb0JBQW9CLEdBQUcsQ0FBQyxDQUFDLFNBQVM7WUFDbEMsZ0NBQWdDLENBQ25DLENBQUM7S0FDTDs7UUFJSyxPQUFPLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQzs7UUFFcEMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztTQUNwQixLQUFLLENBQUMsQ0FBQyxvQkFBd0I7U0FDL0IsUUFBUSxFQUFFOztRQUVyQixNQUFNLEdBQUcsV0FBVyxDQUFDLFdBQVcsQ0FDbEMsT0FBTyxFQUNQLENBQUMsQ0FBQyxTQUFTLENBQ2Q7SUFFRCw2REFBNkQ7SUFFN0QsT0FBTyxNQUFNLENBQUM7QUFDbEIsQ0FBQzs7Ozs7Ozs7OztBQU1ELHlCQUNJLENBQUcsRUFBQyxDQUFHO0lBRVAsSUFBSSxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUU7UUFDM0IsTUFBTSxJQUFJLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0tBQ25EOztRQUlLLFFBQVEsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDOztRQUVuQyxPQUFPLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1NBQ3ZCLEtBQUssQ0FBQyxDQUFDLG9CQUF3QjtTQUMvQixRQUFRLEVBQUU7O1FBRXJCLE1BQU0sR0FBRyxXQUFXLENBQUMsV0FBVyxDQUNsQyxPQUFPLEVBQ1AsQ0FBQyxDQUFDLFNBQVMsQ0FDZDtJQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEdBQUMsQ0FBQyxHQUFDLEtBQUssR0FBQyxDQUFDLEdBQUMsS0FBSyxHQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRXJELE9BQU8sTUFBTSxDQUFDO0FBQ2xCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgIHsgQmlnLCBSb3VuZGluZ01vZGUgfSBmcm9tICdiaWcuanMnO1xuaW1wb3J0IHsgcm91bmREb3duV2l0aG91dFRyYWlsaW5nWmVyb3MgfSBmcm9tICcuLi91dGlsL21hdGgtdXRpbCc7XG5cblxuZXhwb3J0IGNsYXNzIEludEJhc2VkTnVtXG57XG4gICAgLy8gQ0xBU1NcbiAgICBzdGF0aWMgZnJvbUludGVnZXIoXG4gICAgICAgIGludGVnZXI6c3RyaW5nLFxuICAgICAgICBwcmVjaXNpb246bnVtYmVyXG4gICAgKSB7XG4gICAgICAgIHJldHVybiBuZXcgSW50QmFzZWROdW0oXG4gICAgICAgICAgICBwcmVjaXNpb24sXG4gICAgICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgICAgICBuZXcgQmlnKGludGVnZXIpLmRpdihcbiAgICAgICAgICAgICAgICBNYXRoLnBvdygxMCxwcmVjaXNpb24pXG4gICAgICAgICAgICApLnJvdW5kKFxuICAgICAgICAgICAgICAgIHByZWNpc2lvbixcbiAgICAgICAgICAgICAgICBSb3VuZGluZ01vZGUuUm91bmREb3duXG4gICAgICAgICAgICApXG4gICAgICAgICk7XG4gICAgfVxuXG5cbiAgICAvLyBJTlNUQU5DRVxuICAgIHJlYWRvbmx5IGZhY3RvcjpudW1iZXI7XG4gICAgcHJpdmF0ZSBfaW50ZWdlcjpCaWc7XG5cbiAgICBwcml2YXRlIF9yZWNpcHJvY2FsOkludEJhc2VkTnVtO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHJlYWRvbmx5IHByZWNpc2lvbjpudW1iZXIsXG4gICAgICAgIGRlY2ltYWxWYWx1ZTpudW1iZXJ8c3RyaW5nID0gMCxcbiAgICAgICAgYmlnRGVjaW1hbDpCaWcgPSB1bmRlZmluZWRcbiAgICApIHtcbiAgICAgICAgY29uc3QgZGVjaW1hbCA9XG4gICAgICAgICAgICBiaWdEZWNpbWFsID09IHVuZGVmaW5lZCA/XG4gICAgICAgICAgICAgICAgbmV3IEJpZyhkZWNpbWFsVmFsdWUpLnJvdW5kKFxuICAgICAgICAgICAgICAgICAgICBwcmVjaXNpb24sXG4gICAgICAgICAgICAgICAgICAgIFJvdW5kaW5nTW9kZS5Sb3VuZERvd25cbiAgICAgICAgICAgICAgICApIDpcbiAgICAgICAgICAgICAgICBiaWdEZWNpbWFsO1xuXG4gICAgICAgIHRoaXMuZmFjdG9yID0gTWF0aC5wb3coMTAscHJlY2lzaW9uKTtcbiAgICAgICAgdGhpcy5faW50ZWdlciA9IGRlY2ltYWwudGltZXModGhpcy5mYWN0b3IpLnJvdW5kKDAsUm91bmRpbmdNb2RlLlJvdW5kRG93bik7XG4gICAgfVxuXG5cbiAgICAvKioqIE1VVEFUSU5HIE1FVEhPRFMgKioqL1xuICAgIGFkZEFuZFJlcGxhY2Uob3RoZXI6SW50QmFzZWROdW0pIHtcbiAgICAgICAgaWYgKHRoaXMucHJlY2lzaW9uICE9IG90aGVyLnByZWNpc2lvbikge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdwcmVjaXNpb24gb2YgYm90aCBvcGVyYW5kcyBtdXN0IGJlIHRoZSBzYW1lIScpO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCBzdW0gPSB0aGlzLmludGVnZXIucGx1cyhvdGhlci5pbnRlZ2VyKTtcbiAgICAgICAgdGhpcy5faW50ZWdlciA9IHN1bTtcblxuICAgICAgICAvL3JldHVybiB0aGlzO1xuICAgIH1cblxuICAgIHN1YnRyYWN0QW5kUmVwbGFjZShvdGhlcjpJbnRCYXNlZE51bSkge1xuICAgICAgICBpZiAodGhpcy5wcmVjaXNpb24gIT0gb3RoZXIucHJlY2lzaW9uKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ3ByZWNpc2lvbiBvZiBib3RoIG9wZXJhbmRzIG11c3QgYmUgdGhlIHNhbWUhJyk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IGRpZmYgPSB0aGlzLmludGVnZXIubWludXMob3RoZXIuaW50ZWdlcik7XG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBkaWZmO1xuXG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH0gXG4gICAgLyoqKi9cblxuXG4gICAgbXVsdGlwbHkob3RoZXI6SW50QmFzZWROdW0pIHtcbiAgICAgICAgcmV0dXJuIGludGVnZXJNdWx0aXBsaWNhdGlvbihcbiAgICAgICAgICAgIHRoaXMsXG4gICAgICAgICAgICBvdGhlclxuICAgICAgICApO1xuICAgIH1cblxuICAgIG11bHRpcGx5RGVjaW1hbChkZWNpbWFsOm51bWJlcikge1xuICAgICAgICBjb25zdCBvdGhlciA9IG5ldyBJbnRCYXNlZE51bSh0aGlzLnByZWNpc2lvbixkZWNpbWFsKTtcblxuICAgICAgICByZXR1cm4gaW50ZWdlck11bHRpcGxpY2F0aW9uKFxuICAgICAgICAgICAgdGhpcyxcbiAgICAgICAgICAgIG90aGVyXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgZGl2aWRlQnlEZWNpbWFsKGRlY2ltYWw6bnVtYmVyKSB7XG4gICAgICAgIGNvbnN0IG90aGVyID0gbmV3IEludEJhc2VkTnVtKFxuICAgICAgICAgICAgdGhpcy5wcmVjaXNpb24sXG4gICAgICAgICAgICBkZWNpbWFsLnRvU3RyaW5nKClcbiAgICAgICAgKTtcblxuICAgICAgICByZXR1cm4gaW50ZWdlckRpdmlzaW9uKFxuICAgICAgICAgICAgdGhpcyxcbiAgICAgICAgICAgIG90aGVyXG4gICAgICAgICk7XG4gICAgfVxuXG5cbiAgICBnZXQgcmVjaXByb2NhbCgpOkludEJhc2VkTnVtIHtcbiAgICAgICAgaWYgKHRoaXMuX3JlY2lwcm9jYWwgPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl9yZWNpcHJvY2FsID0gaW50ZWdlckRpdmlzaW9uKFxuICAgICAgICAgICAgICAgIG5ldyBJbnRCYXNlZE51bShcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcmVjaXNpb24sXG4gICAgICAgICAgICAgICAgICAgICcxJ1xuICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgdGhpc1xuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLl9yZWNpcHJvY2FsO1xuICAgIH1cblxuICAgIGdldCBvcHBvc2l0ZV8oKTpJbnRCYXNlZE51bSB7XG4gICAgICAgIHJldHVybiBpbnRlZ2VyTXVsdGlwbGljYXRpb24oXG4gICAgICAgICAgICBuZXcgSW50QmFzZWROdW0oXG4gICAgICAgICAgICAgICAgdGhpcy5wcmVjaXNpb24sXG4gICAgICAgICAgICAgICAgJy0xJ1xuICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIHRoaXNcbiAgICAgICAgKTtcbiAgICB9XG4gICAgXG4gICAgZ2V0IGRlY2ltYWxBc051bWJlcigpIHtcbiAgICAgICAgcmV0dXJuIE51bWJlcih0aGlzLmRlY2ltYWwpO1xuICAgIH1cblxuICAgIGdldCBpbnRlZ2VyKCk6QmlnIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2ludGVnZXI7XG4gICAgfVxuXG4gICAgdG9TdHJpbmcoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRlY2ltYWw7XG4gICAgfVxuXG4gICAgZ2V0IGRlY2ltYWxXaXRob3V0VHJhaWxpbmdaZXJvcygpIHtcbiAgICAgICAgcmV0dXJuIHJvdW5kRG93bldpdGhvdXRUcmFpbGluZ1plcm9zKFxuICAgICAgICAgICAgdGhpcy5kZWNpbWFsLFxuICAgICAgICAgICAgdGhpcy5wcmVjaXNpb25cbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBnZXQgZGVjaW1hbCgpIHtcbiAgICAgICAgQmlnLlJNID0gUm91bmRpbmdNb2RlLlJvdW5kRG93bjtcblxuICAgICAgICByZXR1cm4gdGhpcy5pbnRlZ2VyLmRpdihcbiAgICAgICAgICAgIHRoaXMuZmFjdG9yXG4gICAgICAgICkudG9GaXhlZChcbiAgICAgICAgICAgIHRoaXMucHJlY2lzaW9uXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgLypcbiAgICB0b0hpZ2hlclByZWNpc2lvbihuZXdQcmVjaXNpb246bnVtYmVyKTpJbnRCYXNlZE51bSB7XG4gICAgICAgIGlmIChuZXdQcmVjaXNpb24gPCB0aGlzLnByZWNpc2lvbikge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdOZXcgUHJlY2lzaW9uIE11c3QgYmUgSGlnaGVyIScpO1xuICAgICAgICB9XG5cblxuICAgICAgICByZXR1cm4gSW50QmFzZWROdW0uZnJvbUludGVnZXIoXG4gICAgICAgICAgICB0aGlzLmludGVnZXIudGltZXMoXG4gICAgICAgICAgICAgICAgTWF0aC5wb3coXG4gICAgICAgICAgICAgICAgICAgIDEwLFxuICAgICAgICAgICAgICAgICAgICBuZXdQcmVjaXNpb24tdGhpcy5wcmVjaXNpb25cbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApLnRvU3RyaW5nKCksXG4gICAgICAgICAgICBuZXdQcmVjaXNpb25cbiAgICAgICAgKTtcbiAgICB9XG4gICAgKi9cbn1cblxuZXhwb3J0IGNsYXNzIEFzc2V0QW1vdW50IGV4dGVuZHMgSW50QmFzZWROdW1cbntcbiAgICAvL3ByaXZhdGUgX2RlY2ltYWw6c3RyaW5nO1xuICAgIHByaXZhdGUgX3N5bWJvbDpzdHJpbmc7XG5cbiAgICBwcml2YXRlIF9pc1plcm86Ym9vbGVhbjtcblxuICAgIGNvbnN0cnVjdG9yKGRhdGE6c3RyaW5nKSB7XG4gICAgICAgIGNvbnN0IHBhcnRzID0gZGF0YS5zcGxpdCgnICcpO1xuXG4gICAgICAgIGNvbnN0IGRlY2ltYWwgPSBwYXJ0c1swXTtcbiAgICAgICAgY29uc3Qgc3ltYm9sID0gcGFydHNbMV07XG5cbiAgICAgICAgY29uc3QgcHJlY2lzaW9uID0gZGVjaW1hbC5zcGxpdCgnLicpWzFdLmxlbmd0aDtcblxuXG4gICAgICAgIHN1cGVyKHByZWNpc2lvbixkZWNpbWFsKTtcblxuICAgICAgICBcbiAgICAgICAgdGhpcy5fc3ltYm9sID0gc3ltYm9sO1xuXG4gICAgICAgIHRoaXMuX2lzWmVybyA9IE51bWJlcihkZWNpbWFsKSA9PSAwO1xuICAgIH1cblxuICAgIGdldCBpc1plcm8oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc1plcm87XG4gICAgfVxuXG4gICAgZ2V0IHN5bWJvbCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3N5bWJvbDtcbiAgICB9XG59XG5cblxuXG4vL2V4cG9ydCBcbmZ1bmN0aW9uIGludGVnZXJBZGRpdGlvbjxUIGV4dGVuZHMgSW50QmFzZWROdW0+KFxuICAgIGE6VCxiOlRcbik6SW50QmFzZWROdW0ge1xuICAgIGlmIChhLnByZWNpc2lvbiAhPSBiLnByZWNpc2lvbikge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAgICAgICAncHJlY2lzaW9uIG9mIGE6ICcgKyBhLnByZWNpc2lvbiArXG4gICAgICAgICAgICAnLCBwcmVjaXNpb24gb2YgYjogJyArIGIucHJlY2lzaW9uICtcbiAgICAgICAgICAgIFwiXFxucHJlY2lzaW9uIG9mIGJvdGggb3BlcmFuZHMgbXVzdCBiZSB0aGUgc2FtZSFcIlxuICAgICAgICApO1xuICAgIH1cblxuXG5cbiAgICBjb25zdCBzdW0gPSBhLmludGVnZXIucGx1cyhiLmludGVnZXIpO1xuXG4gICAgcmV0dXJuIEludEJhc2VkTnVtLmZyb21JbnRlZ2VyKFxuICAgICAgICBzdW0udG9TdHJpbmcoKSxcbiAgICAgICAgYS5wcmVjaXNpb25cbiAgICApO1xufVxuXG4vL2V4cG9ydCBcbmZ1bmN0aW9uIGludGVnZXJTdWJ0cmFjdGlvbjxUIGV4dGVuZHMgSW50QmFzZWROdW0+KFxuICAgIGE6VCxiOlRcbik6SW50QmFzZWROdW0ge1xuICAgIGlmIChhLnByZWNpc2lvbiAhPSBiLnByZWNpc2lvbikge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ3ByZWNpc2lvbiBvZiBib3RoIG9wZXJhbmRzIG11c3QgYmUgdGhlIHNhbWUhJyk7XG4gICAgfVxuXG5cblxuICAgIGNvbnN0IGRpZmYgPSBhLmludGVnZXIubWludXMoYi5pbnRlZ2VyKTtcblxuICAgIHJldHVybiBJbnRCYXNlZE51bS5mcm9tSW50ZWdlcihcbiAgICAgICAgZGlmZi50b1N0cmluZygpLFxuICAgICAgICBhLnByZWNpc2lvblxuICAgICk7XG59XG5cbi8vZXhwb3J0IFxuZnVuY3Rpb24gaW50ZWdlck11bHRpcGxpY2F0aW9uPFQgZXh0ZW5kcyBJbnRCYXNlZE51bT4oXG4gICAgYTpULGI6VFxuKTpJbnRCYXNlZE51bSB7XG4gICAgaWYgKGEucHJlY2lzaW9uIDwgYi5wcmVjaXNpb24pIHtcbiAgICAgICAgY29uc29sZS5sb2coYS5kZWNpbWFsKTtcbiAgICAgICAgY29uc29sZS5sb2coYi5kZWNpbWFsKTtcblxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAgICAgICAncHJlY2lzaW9uIG9mIGE6ICcgKyBhLnByZWNpc2lvbiArXG4gICAgICAgICAgICAnLCBwcmVjaXNpb24gb2YgYjogJyArIGIucHJlY2lzaW9uICtcbiAgICAgICAgICAgIFwiXFxucHJlY2lzaW9uIG9mIEEgbXVzdCBiZSA+PSBCIVwiXG4gICAgICAgICk7XG4gICAgfVxuXG5cblxuICAgIGNvbnN0IHByb2R1Y3QgPSBhLmludGVnZXIudGltZXMoYi5pbnRlZ2VyKTtcblxuICAgIGNvbnN0IGludGVnZXIgPSBwcm9kdWN0LmRpdihiLmZhY3RvcilcbiAgICAgICAgICAgICAgICAgICAgLnJvdW5kKDAsUm91bmRpbmdNb2RlLlJvdW5kRG93bilcbiAgICAgICAgICAgICAgICAgICAgLnRvU3RyaW5nKCk7XG5cbiAgICBjb25zdCByZXN1bHQgPSBJbnRCYXNlZE51bS5mcm9tSW50ZWdlcihcbiAgICAgICAgaW50ZWdlcixcbiAgICAgICAgYS5wcmVjaXNpb25cbiAgICApO1xuXG4gICAgLy9jb25zb2xlLmxvZygnSU5UIE11bHRpcGxpY2F0aW9uOiAnK2ErJyAqICcrYisnID0gJytyZXN1bHQpO1xuXG4gICAgcmV0dXJuIHJlc3VsdDtcbn1cblxuXG4vLyB3aGVuIGRvaW5nIGRpdmlzaW9uIGJldHdlZW4gdHdvIGVvcyBudW1iZXJzIHRoZW4gZGlzcmVnYXJkXG4vLyB0aGUgZGVjaW1hbCBhZnRlciB0aGUgaW50ZWdlclxuLy9leHBvcnQgXG5mdW5jdGlvbiBpbnRlZ2VyRGl2aXNpb248VCBleHRlbmRzIEludEJhc2VkTnVtPihcbiAgICBhOlQsYjpUXG4pOkludEJhc2VkTnVtIHtcbiAgICBpZiAoYS5wcmVjaXNpb24gPCBiLnByZWNpc2lvbikge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ3ByZWNpc2lvbiBvZiBBIG11c3QgYmUgPj0gQiEnKTtcbiAgICB9XG5cblxuXG4gICAgY29uc3QgcXVvdGllbnQgPSBhLmludGVnZXIuZGl2KGIuaW50ZWdlcik7XG5cbiAgICBjb25zdCBpbnRlZ2VyID0gcXVvdGllbnQudGltZXMoYi5mYWN0b3IpXG4gICAgICAgICAgICAgICAgICAgIC5yb3VuZCgwLFJvdW5kaW5nTW9kZS5Sb3VuZERvd24pXG4gICAgICAgICAgICAgICAgICAgIC50b1N0cmluZygpO1xuICAgIFxuICAgIGNvbnN0IHJlc3VsdCA9IEludEJhc2VkTnVtLmZyb21JbnRlZ2VyKFxuICAgICAgICBpbnRlZ2VyLFxuICAgICAgICBhLnByZWNpc2lvblxuICAgICk7XG5cbiAgICBjb25zb2xlLmxvZygnSU5UIERpdmlzaW9uOiAnK2ErJyAvICcrYisnID0gJytyZXN1bHQpO1xuXG4gICAgcmV0dXJuIHJlc3VsdDtcbn1cbiJdfQ==