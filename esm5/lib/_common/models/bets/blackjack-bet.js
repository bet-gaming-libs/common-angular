/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/bets/blackjack-bet.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { BetBase } from "./bet-base";
import { AssetAmount } from '../../math/integer-math';
var BlackjackBetResult = /** @class */ (function (_super) {
    tslib_1.__extends(BlackjackBetResult, _super);
    function BlackjackBetResult(data) {
        var _this = _super.call(this, data) || this;
        _this.game = 'blackjack';
        _this.description = describeResults(data.playerResults, data.dealerHandResult);
        /** @type {?} */
        var totalWagered = new AssetAmount(data.totalWagered);
        /** @type {?} */
        var totalPayout = new AssetAmount(data.totalPayout);
        _this.payout = totalPayout.decimalWithoutTrailingZeros;
        _this.isWin = totalPayout.integer.gte(totalWagered.integer);
        _this.profit = Number(totalPayout.subtractAndReplace(totalWagered).decimal);
        return _this;
    }
    return BlackjackBetResult;
}(BetBase));
export { BlackjackBetResult };
if (false) {
    /** @type {?} */
    BlackjackBetResult.prototype.game;
    /** @type {?} */
    BlackjackBetResult.prototype.description;
    /** @type {?} */
    BlackjackBetResult.prototype.isWin;
    /** @type {?} */
    BlackjackBetResult.prototype.payout;
    /** @type {?} */
    BlackjackBetResult.prototype.profit;
}
/**
 * @param {?} playerResults
 * @param {?} dealerHandResult
 * @return {?}
 */
function describeResults(playerResults, dealerHandResult) {
    var e_1, _a;
    /** @type {?} */
    var parts = [];
    try {
        for (var playerResults_1 = tslib_1.__values(playerResults), playerResults_1_1 = playerResults_1.next(); !playerResults_1_1.done; playerResults_1_1 = playerResults_1.next()) {
            var result = playerResults_1_1.value;
            parts.push(describeResult(result, dealerHandResult));
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (playerResults_1_1 && !playerResults_1_1.done && (_a = playerResults_1.return)) _a.call(playerResults_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return parts.join(' ~ ');
}
/**
 * @param {?} player
 * @param {?} dealer
 * @return {?}
 */
function describeResult(player, dealer) {
    /** @type {?} */
    var left = '' + player.sum;
    /** @type {?} */
    var sign;
    /** @type {?} */
    var right = '' + dealer.sum;
    if (player.isBlackjack) {
        left = 'BJ';
    }
    if (dealer.isBlackjack) {
        right = 'BJ';
    }
    if (player.sum > 21) {
        sign = '>';
        right = '21';
    }
    else if (player.sum < dealer.sum) {
        sign = '<';
    }
    else if (player.sum > dealer.sum) {
        sign = '>';
    }
    else {
        // sums are equal (it may be a push) 
        if (player.sum < 21) {
            // PUSH if less than 21
            sign = '=';
        }
        else {
            // BOTH SUMS ARE EQUAL TO 21
            // PUSH if both have BJ
            if (player.isBlackjack && dealer.isBlackjack) {
                sign = '=';
            }
            else if (player.isBlackjack) {
                sign = '>';
            }
            else {
                // dealer blackjack
                sign = '<';
            }
        }
    }
    return left + ' ' + sign + ' ' + right;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWJldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9tb2RlbHMvYmV0cy9ibGFja2phY2stYmV0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFFckMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBR3REO0lBQXdDLDhDQUFPO0lBUzNDLDRCQUFZLElBQTBCO1FBQXRDLFlBQ0ksa0JBQU0sSUFBSSxDQUFDLFNBb0JkO1FBNUJRLFVBQUksR0FBRyxXQUFXLENBQUM7UUFXeEIsS0FBSSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQzlCLElBQUksQ0FBQyxhQUFhLEVBQ2xCLElBQUksQ0FBQyxnQkFBZ0IsQ0FDeEIsQ0FBQzs7WUFFSSxZQUFZLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQzs7WUFDakQsV0FBVyxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7UUFFckQsS0FBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsMkJBQTJCLENBQUM7UUFFdEQsS0FBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FDaEMsWUFBWSxDQUFDLE9BQU8sQ0FDdkIsQ0FBQztRQUVGLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUNoQixXQUFXLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUMsT0FBTyxDQUN2RCxDQUFDOztJQUNOLENBQUM7SUFDTCx5QkFBQztBQUFELENBQUMsQUEvQkQsQ0FBd0MsT0FBTyxHQStCOUM7Ozs7SUE3Qkcsa0NBQTRCOztJQUU1Qix5Q0FBNEI7O0lBQzVCLG1DQUF1Qjs7SUFDdkIsb0NBQXVCOztJQUN2QixvQ0FBdUI7Ozs7Ozs7QUEyQjNCLHlCQUNJLGFBQW9DLEVBQ3BDLGdCQUFxQzs7O1FBRS9CLEtBQUssR0FBWSxFQUFFOztRQUV6QixLQUFtQixJQUFBLGtCQUFBLGlCQUFBLGFBQWEsQ0FBQSw0Q0FBQSx1RUFBRTtZQUE3QixJQUFJLE1BQU0sMEJBQUE7WUFDWCxLQUFLLENBQUMsSUFBSSxDQUNOLGNBQWMsQ0FDVixNQUFNLEVBQ04sZ0JBQWdCLENBQ25CLENBQ0osQ0FBQztTQUNMOzs7Ozs7Ozs7SUFFRCxPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDN0IsQ0FBQzs7Ozs7O0FBRUQsd0JBQ0ksTUFBMkIsRUFDM0IsTUFBMkI7O1FBRXZCLElBQUksR0FBVSxFQUFFLEdBQUMsTUFBTSxDQUFDLEdBQUc7O1FBQzNCLElBQVc7O1FBQ1gsS0FBSyxHQUFVLEVBQUUsR0FBQyxNQUFNLENBQUMsR0FBRztJQUdoQyxJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUU7UUFDcEIsSUFBSSxHQUFHLElBQUksQ0FBQztLQUNmO0lBQ0QsSUFBSSxNQUFNLENBQUMsV0FBVyxFQUFFO1FBQ3BCLEtBQUssR0FBRyxJQUFJLENBQUM7S0FDaEI7SUFHRCxJQUFJLE1BQU0sQ0FBQyxHQUFHLEdBQUcsRUFBRSxFQUFFO1FBQ2pCLElBQUksR0FBRyxHQUFHLENBQUM7UUFDWCxLQUFLLEdBQUcsSUFBSSxDQUFDO0tBQ2hCO1NBQ0ksSUFBSSxNQUFNLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxHQUFHLEVBQUU7UUFDOUIsSUFBSSxHQUFHLEdBQUcsQ0FBQztLQUNkO1NBQ0ksSUFBSSxNQUFNLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxHQUFHLEVBQUU7UUFDOUIsSUFBSSxHQUFHLEdBQUcsQ0FBQztLQUNkO1NBQ0k7UUFDRCxxQ0FBcUM7UUFDckMsSUFBSSxNQUFNLENBQUMsR0FBRyxHQUFHLEVBQUUsRUFBRTtZQUNqQix1QkFBdUI7WUFDdkIsSUFBSSxHQUFHLEdBQUcsQ0FBQztTQUNkO2FBQU07WUFDSCw0QkFBNEI7WUFFNUIsdUJBQXVCO1lBQ3ZCLElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxNQUFNLENBQUMsV0FBVyxFQUFFO2dCQUMxQyxJQUFJLEdBQUcsR0FBRyxDQUFDO2FBQ2Q7aUJBQ0ksSUFBSSxNQUFNLENBQUMsV0FBVyxFQUFFO2dCQUN6QixJQUFJLEdBQUcsR0FBRyxDQUFDO2FBQ2Q7aUJBQ0k7Z0JBQ0QsbUJBQW1CO2dCQUNuQixJQUFJLEdBQUcsR0FBRyxDQUFDO2FBQ2Q7U0FDSjtLQUNKO0lBRUQsT0FBTyxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDO0FBQzNDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0lSZXNvbHZlZEJsYWNramFja0JldCwgSUJsYWNramFja0hhbmRSZXN1bHR9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuaW1wb3J0IHsgQmV0QmFzZSB9IGZyb20gXCIuL2JldC1iYXNlXCI7XG5pbXBvcnQgeyBJQmV0UmVzdWx0IH0gZnJvbSBcIi4vaW50ZXJmYWNlc1wiO1xuaW1wb3J0IHsgQXNzZXRBbW91bnQgfSBmcm9tICcuLi8uLi9tYXRoL2ludGVnZXItbWF0aCc7XG5cblxuZXhwb3J0IGNsYXNzIEJsYWNramFja0JldFJlc3VsdCBleHRlbmRzIEJldEJhc2UgaW1wbGVtZW50cyBJQmV0UmVzdWx0XG57XG4gICAgcmVhZG9ubHkgZ2FtZSA9ICdibGFja2phY2snO1xuXG4gICAgcmVhZG9ubHkgZGVzY3JpcHRpb246c3RyaW5nO1xuICAgIHJlYWRvbmx5IGlzV2luOmJvb2xlYW47XG4gICAgcmVhZG9ubHkgcGF5b3V0OnN0cmluZztcbiAgICByZWFkb25seSBwcm9maXQ6bnVtYmVyO1xuXG4gICAgY29uc3RydWN0b3IoZGF0YTpJUmVzb2x2ZWRCbGFja2phY2tCZXQpIHtcbiAgICAgICAgc3VwZXIoZGF0YSk7XG5cbiAgICAgICAgXG4gICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSBkZXNjcmliZVJlc3VsdHMoXG4gICAgICAgICAgICBkYXRhLnBsYXllclJlc3VsdHMsXG4gICAgICAgICAgICBkYXRhLmRlYWxlckhhbmRSZXN1bHRcbiAgICAgICAgKTtcblxuICAgICAgICBjb25zdCB0b3RhbFdhZ2VyZWQgPSBuZXcgQXNzZXRBbW91bnQoZGF0YS50b3RhbFdhZ2VyZWQpO1xuICAgICAgICBjb25zdCB0b3RhbFBheW91dCA9IG5ldyBBc3NldEFtb3VudChkYXRhLnRvdGFsUGF5b3V0KTtcblxuICAgICAgICB0aGlzLnBheW91dCA9IHRvdGFsUGF5b3V0LmRlY2ltYWxXaXRob3V0VHJhaWxpbmdaZXJvcztcblxuICAgICAgICB0aGlzLmlzV2luID0gdG90YWxQYXlvdXQuaW50ZWdlci5ndGUoXG4gICAgICAgICAgICB0b3RhbFdhZ2VyZWQuaW50ZWdlclxuICAgICAgICApO1xuXG4gICAgICAgIHRoaXMucHJvZml0ID0gTnVtYmVyKFxuICAgICAgICAgICAgdG90YWxQYXlvdXQuc3VidHJhY3RBbmRSZXBsYWNlKHRvdGFsV2FnZXJlZCkuZGVjaW1hbFxuICAgICAgICApO1xuICAgIH1cbn1cblxuXG5mdW5jdGlvbiBkZXNjcmliZVJlc3VsdHMoXG4gICAgcGxheWVyUmVzdWx0czpJQmxhY2tqYWNrSGFuZFJlc3VsdFtdLFxuICAgIGRlYWxlckhhbmRSZXN1bHQ6SUJsYWNramFja0hhbmRSZXN1bHRcbikge1xuICAgIGNvbnN0IHBhcnRzOnN0cmluZ1tdID0gW107XG5cbiAgICBmb3IgKHZhciByZXN1bHQgb2YgcGxheWVyUmVzdWx0cykge1xuICAgICAgICBwYXJ0cy5wdXNoKFxuICAgICAgICAgICAgZGVzY3JpYmVSZXN1bHQoXG4gICAgICAgICAgICAgICAgcmVzdWx0LFxuICAgICAgICAgICAgICAgIGRlYWxlckhhbmRSZXN1bHRcbiAgICAgICAgICAgIClcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICByZXR1cm4gcGFydHMuam9pbignIH4gJyk7XG59XG5cbmZ1bmN0aW9uIGRlc2NyaWJlUmVzdWx0KFxuICAgIHBsYXllcjpJQmxhY2tqYWNrSGFuZFJlc3VsdCxcbiAgICBkZWFsZXI6SUJsYWNramFja0hhbmRSZXN1bHRcbik6c3RyaW5nIHtcbiAgICBsZXQgbGVmdDpzdHJpbmcgPSAnJytwbGF5ZXIuc3VtO1xuICAgIGxldCBzaWduOnN0cmluZztcbiAgICBsZXQgcmlnaHQ6c3RyaW5nID0gJycrZGVhbGVyLnN1bTtcblxuXG4gICAgaWYgKHBsYXllci5pc0JsYWNramFjaykge1xuICAgICAgICBsZWZ0ID0gJ0JKJztcbiAgICB9XG4gICAgaWYgKGRlYWxlci5pc0JsYWNramFjaykge1xuICAgICAgICByaWdodCA9ICdCSic7XG4gICAgfVxuXG5cbiAgICBpZiAocGxheWVyLnN1bSA+IDIxKSB7XG4gICAgICAgIHNpZ24gPSAnPic7XG4gICAgICAgIHJpZ2h0ID0gJzIxJztcbiAgICB9XG4gICAgZWxzZSBpZiAocGxheWVyLnN1bSA8IGRlYWxlci5zdW0pIHtcbiAgICAgICAgc2lnbiA9ICc8JztcbiAgICB9XG4gICAgZWxzZSBpZiAocGxheWVyLnN1bSA+IGRlYWxlci5zdW0pIHtcbiAgICAgICAgc2lnbiA9ICc+JztcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICAgIC8vIHN1bXMgYXJlIGVxdWFsIChpdCBtYXkgYmUgYSBwdXNoKSBcbiAgICAgICAgaWYgKHBsYXllci5zdW0gPCAyMSkge1xuICAgICAgICAgICAgLy8gUFVTSCBpZiBsZXNzIHRoYW4gMjFcbiAgICAgICAgICAgIHNpZ24gPSAnPSc7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAvLyBCT1RIIFNVTVMgQVJFIEVRVUFMIFRPIDIxXG5cbiAgICAgICAgICAgIC8vIFBVU0ggaWYgYm90aCBoYXZlIEJKXG4gICAgICAgICAgICBpZiAocGxheWVyLmlzQmxhY2tqYWNrICYmIGRlYWxlci5pc0JsYWNramFjaykge1xuICAgICAgICAgICAgICAgIHNpZ24gPSAnPSc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmIChwbGF5ZXIuaXNCbGFja2phY2spIHtcbiAgICAgICAgICAgICAgICBzaWduID0gJz4nO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgLy8gZGVhbGVyIGJsYWNramFja1xuICAgICAgICAgICAgICAgIHNpZ24gPSAnPCc7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gbGVmdCArICcgJyArIHNpZ24gKyAnICcgKyByaWdodDtcbn0iXX0=