/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/business-rules/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEosAccountNames() { }
if (false) {
    /** @type {?} */
    IEosAccountNames.prototype.eosbetcasino;
    /** @type {?} */
    IEosAccountNames.prototype.burnedTokens;
    /** @type {?} */
    IEosAccountNames.prototype.bankroll;
    /** @type {?} */
    IEosAccountNames.prototype.easyAccount;
    /** @type {?} */
    IEosAccountNames.prototype.jackpot;
    /** @type {?} */
    IEosAccountNames.prototype.betToken;
    /** @type {?} */
    IEosAccountNames.prototype.withdraw;
    /** @type {?} */
    IEosAccountNames.prototype.tokens;
    /** @type {?} */
    IEosAccountNames.prototype.games;
}
/**
 * @record
 */
export function ICurrencyTokenData() { }
if (false) {
    /** @type {?} */
    ICurrencyTokenData.prototype.symbol;
    /** @type {?} */
    ICurrencyTokenData.prototype.contract;
    /** @type {?} */
    ICurrencyTokenData.prototype.precision;
}
/**
 * @record
 */
export function ICurrencyToken() { }
if (false) {
    /**
     * @param {?} value
     * @return {?}
     */
    ICurrencyToken.prototype.getAssetAmount = function (value) { };
}
/**
 * @record
 */
export function IBettingTokenData() { }
if (false) {
    /** @type {?} */
    IBettingTokenData.prototype.isNativeEosToken;
    /** @type {?} */
    IBettingTokenData.prototype.minimumBetAmount;
    /** @type {?} */
    IBettingTokenData.prototype.maximumWinAmount;
    /** @type {?} */
    IBettingTokenData.prototype.jackpot;
    /** @type {?} */
    IBettingTokenData.prototype.betTokenAirDropRate;
    /** @type {?} */
    IBettingTokenData.prototype.highRollerAmount;
    /** @type {?} */
    IBettingTokenData.prototype.universalDepositAddress;
    /** @type {?} */
    IBettingTokenData.prototype.pendingDepositTimeInMinutes;
    /** @type {?} */
    IBettingTokenData.prototype.minimumWithdrawAmount;
    /** @type {?} */
    IBettingTokenData.prototype.supportMemoForWithdraw;
}
/**
 * @record
 */
export function IBettingToken() { }
if (false) {
    /** @type {?} */
    IBettingToken.prototype.jackpotTicketPrice;
    /** @type {?} */
    IBettingToken.prototype.chips;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9tb2RlbHMvYnVzaW5lc3MtcnVsZXMvaW50ZXJmYWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBLHNDQWtCQzs7O0lBaEJHLHdDQUFvQjs7SUFDcEIsd0NBQW9COztJQUNwQixvQ0FBZ0I7O0lBQ2hCLHVDQUFtQjs7SUFDbkIsbUNBQWU7O0lBQ2Ysb0NBQWdCOztJQUNoQixvQ0FBZ0I7O0lBQ2hCLGtDQUFjOztJQUVkLGlDQU1DOzs7OztBQUlMLHdDQUtDOzs7SUFIRyxvQ0FBZTs7SUFDZixzQ0FBaUI7O0lBQ2pCLHVDQUFrQjs7Ozs7QUFJdEIsb0NBR0M7Ozs7OztJQURHLCtEQUE0Qzs7Ozs7QUFJaEQsdUNBbUJDOzs7SUFqQkcsNkNBQXlCOztJQUl6Qiw2Q0FBd0I7O0lBQ3hCLDZDQUF3Qjs7SUFDeEIsb0NBR0U7O0lBQ0YsZ0RBQTJCOztJQUMzQiw2Q0FBd0I7O0lBRXhCLG9EQUErQjs7SUFDL0Isd0RBQW1DOztJQUNuQyxrREFBNkI7O0lBQzdCLG1EQUErQjs7Ozs7QUFJbkMsbUNBR0M7OztJQUZHLDJDQUEyQzs7SUFDM0MsOEJBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRU9TQXNzZXRBbW91bnQgfSBmcm9tIFwiZWFybmJldC1jb21tb25cIjtcblxuXG5leHBvcnQgaW50ZXJmYWNlIElFb3NBY2NvdW50TmFtZXNcbntcbiAgICBlb3NiZXRjYXNpbm86c3RyaW5nO1xuICAgIGJ1cm5lZFRva2VuczpzdHJpbmc7XG4gICAgYmFua3JvbGw6c3RyaW5nO1xuICAgIGVhc3lBY2NvdW50OnN0cmluZztcbiAgICBqYWNrcG90OnN0cmluZztcbiAgICBiZXRUb2tlbjpzdHJpbmc7XG4gICAgd2l0aGRyYXc6c3RyaW5nO1xuICAgIHRva2VuczpzdHJpbmc7XG5cbiAgICBnYW1lczoge1xuICAgICAgICBkaWNlOnN0cmluZztcbiAgICAgICAgY3Jhc2g6c3RyaW5nO1xuICAgICAgICBiYWNjYXJhdDpzdHJpbmc7XG4gICAgICAgIGhpbG86c3RyaW5nO1xuICAgICAgICBibGFja2phY2s6c3RyaW5nO1xuICAgIH1cbn1cblxuXG5leHBvcnQgaW50ZXJmYWNlIElDdXJyZW5jeVRva2VuRGF0YVxue1xuICAgIHN5bWJvbDogc3RyaW5nO1xuICAgIGNvbnRyYWN0OiBzdHJpbmc7XG4gICAgcHJlY2lzaW9uOiBudW1iZXI7XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQ3VycmVuY3lUb2tlbiBleHRlbmRzIElDdXJyZW5jeVRva2VuRGF0YVxue1xuICAgIGdldEFzc2V0QW1vdW50KHZhbHVlOnN0cmluZyk6RU9TQXNzZXRBbW91bnQ7XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQmV0dGluZ1Rva2VuRGF0YSBleHRlbmRzIElDdXJyZW5jeVRva2VuRGF0YVxue1xuICAgIGlzTmF0aXZlRW9zVG9rZW46Ym9vbGVhbjtcblxuICAgIC8vY29pbk1hcmtldENhcFRpY2tlcjpzdHJpbmc7XG5cbiAgICBtaW5pbXVtQmV0QW1vdW50OnN0cmluZztcbiAgICBtYXhpbXVtV2luQW1vdW50Om51bWJlcjtcbiAgICBqYWNrcG90OiB7XG4gICAgICAgIG1pbmltdW1CZXQ6bnVtYmVyO1xuICAgICAgICB0aWNrZXRQcmljZTpzdHJpbmc7XG4gICAgfSxcbiAgICBiZXRUb2tlbkFpckRyb3BSYXRlOm51bWJlcjtcbiAgICBoaWdoUm9sbGVyQW1vdW50Om51bWJlcjtcblxuICAgIHVuaXZlcnNhbERlcG9zaXRBZGRyZXNzOnN0cmluZztcbiAgICBwZW5kaW5nRGVwb3NpdFRpbWVJbk1pbnV0ZXM6bnVtYmVyO1xuICAgIG1pbmltdW1XaXRoZHJhd0Ftb3VudDpudW1iZXI7XG4gICAgc3VwcG9ydE1lbW9Gb3JXaXRoZHJhdzpib29sZWFuO1xufVxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUJldHRpbmdUb2tlbiBleHRlbmRzIElDdXJyZW5jeVRva2VuLElCZXR0aW5nVG9rZW5EYXRhIHtcbiAgICByZWFkb25seSBqYWNrcG90VGlja2V0UHJpY2U6RU9TQXNzZXRBbW91bnQ7XG4gICAgcmVhZG9ubHkgY2hpcHM6bnVtYmVyW107XG59Il19