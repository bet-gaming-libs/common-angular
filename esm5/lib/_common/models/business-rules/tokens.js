/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/business-rules/tokens.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var TokenSymbol = {
    BTC: "BTC",
    ETH: "ETH",
    EOS: "EOS",
    BET: "BET",
    LTC: "LTC",
    XRP: "XRP",
    BCH: "BCH",
    BNB: "BNB",
    WAX: "WAX",
    TRX: "TRX",
    LINK: "LINK",
    DAI: "DAI",
    USDC: "USDC",
    USDT: "USDT",
    FUN: "FUN",
};
export { TokenSymbol };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9rZW5zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL21vZGVscy9idXNpbmVzcy1ydWxlcy90b2tlbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsSUFBWSxXQUFXO0lBRW5CLEdBQUcsT0FBUTtJQUNYLEdBQUcsT0FBUTtJQUNYLEdBQUcsT0FBUTtJQUNYLEdBQUcsT0FBUTtJQUNYLEdBQUcsT0FBUTtJQUNYLEdBQUcsT0FBUTtJQUNYLEdBQUcsT0FBUTtJQUNYLEdBQUcsT0FBUTtJQUNYLEdBQUcsT0FBUTtJQUNYLEdBQUcsT0FBUTtJQUNYLElBQUksUUFBUztJQUViLEdBQUcsT0FBUTtJQUNYLElBQUksUUFBUztJQUNiLElBQUksUUFBUztJQUViLEdBQUcsT0FBUTtFQUNkIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gVG9rZW5TeW1ib2xcbntcbiAgICBCVEMgPSAnQlRDJyxcbiAgICBFVEggPSAnRVRIJyxcbiAgICBFT1MgPSAnRU9TJyxcbiAgICBCRVQgPSAnQkVUJyxcbiAgICBMVEMgPSAnTFRDJyxcbiAgICBYUlAgPSAnWFJQJyxcbiAgICBCQ0ggPSAnQkNIJyxcbiAgICBCTkIgPSAnQk5CJyxcbiAgICBXQVggPSAnV0FYJyxcbiAgICBUUlggPSAnVFJYJyxcbiAgICBMSU5LID0gJ0xJTksnLFxuXG4gICAgREFJID0gJ0RBSScsXG4gICAgVVNEQyA9ICdVU0RDJyxcbiAgICBVU0RUID0gJ1VTRFQnLFxuXG4gICAgRlVOID0gJ0ZVTidcbn0iXX0=