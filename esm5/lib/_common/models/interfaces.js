/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IAppSettingsListener() { }
if (false) {
    /**
     * @return {?}
     */
    IAppSettingsListener.prototype.onBettingCurrencyChanged = function () { };
}
/**
 * @record
 */
export function IAppSettings() { }
if (false) {
    /** @type {?} */
    IAppSettings.prototype.translation;
    /** @type {?} */
    IAppSettings.prototype.selectedToken;
    /** @type {?} */
    IAppSettings.prototype.selectedTokenBalance;
    /** @type {?} */
    IAppSettings.prototype.wallet;
    /**
     * @param {?} listener
     * @return {?}
     */
    IAppSettings.prototype.setListener = function (listener) { };
    /**
     * @param {?} symbol
     * @return {?}
     */
    IAppSettings.prototype.selectCurrency = function (symbol) { };
}
/**
 * @record
 */
export function IAppStateListener() { }
if (false) {
    /**
     * @return {?}
     */
    IAppStateListener.prototype.onAuthenticated = function () { };
}
/**
 * @record
 */
export function IAppState() { }
if (false) {
    /** @type {?} */
    IAppState.prototype.bets;
    /** @type {?} */
    IAppState.prototype.referrer;
    /** @type {?} */
    IAppState.prototype.seedForRandomness;
    /**
     * @param {?} toggle
     * @param {?=} amt
     * @return {?}
     */
    IAppState.prototype.onAddToBalance = function (toggle, amt) { };
    /**
     * @param {?} listener
     * @return {?}
     */
    IAppState.prototype.setListener = function (listener) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9tb2RlbHMvaW50ZXJmYWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQU1BLDBDQUdDOzs7OztJQURHLDBFQUFnQzs7Ozs7QUFHcEMsa0NBU0M7OztJQVBHLG1DQUFzQzs7SUFDdEMscUNBQXFDOztJQUNyQyw0Q0FBcUM7O0lBQ3JDLDhCQUFtQzs7Ozs7SUFFbkMsNkRBQWdEOzs7OztJQUNoRCw4REFBbUM7Ozs7O0FBSXZDLHVDQUdDOzs7OztJQURHLDhEQUF1Qjs7Ozs7QUFHM0IsK0JBUUM7OztJQU5HLHlCQUE0Qjs7SUFDNUIsNkJBQXlCOztJQUN6QixzQ0FBeUI7Ozs7OztJQUV6QixnRUFBNkQ7Ozs7O0lBQzdELDBEQUE2QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElUcmFuc2xhdGlvbkJhc2UgfSBmcm9tICcuLi90cmFuc2xhdGlvbnMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQmV0dGluZ1Rva2VuIH0gZnJvbSAnLi9idXNpbmVzcy1ydWxlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElXYWxsZXRTZXJ2aWNlQmFzZSB9IGZyb20gJy4uL3dhbGxldHMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJUmVzb2x2ZWRCZXRzIH0gZnJvbSAnLi9iZXRzL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUFwcFNldHRpbmdzTGlzdGVuZXJcbntcbiAgICBvbkJldHRpbmdDdXJyZW5jeUNoYW5nZWQoKTp2b2lkO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElBcHBTZXR0aW5nc1xue1xuICAgIHJlYWRvbmx5IHRyYW5zbGF0aW9uOklUcmFuc2xhdGlvbkJhc2U7XG4gICAgcmVhZG9ubHkgc2VsZWN0ZWRUb2tlbjpJQmV0dGluZ1Rva2VuO1xuICAgIHJlYWRvbmx5IHNlbGVjdGVkVG9rZW5CYWxhbmNlOnN0cmluZztcbiAgICByZWFkb25seSB3YWxsZXQ6SVdhbGxldFNlcnZpY2VCYXNlO1xuXG4gICAgc2V0TGlzdGVuZXIobGlzdGVuZXI6SUFwcFNldHRpbmdzTGlzdGVuZXIpOnZvaWQ7XG4gICAgc2VsZWN0Q3VycmVuY3koc3ltYm9sOnN0cmluZyk6dm9pZDtcbn1cblxuXG5leHBvcnQgaW50ZXJmYWNlIElBcHBTdGF0ZUxpc3RlbmVyXG57XG4gICAgb25BdXRoZW50aWNhdGVkKCk6dm9pZDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQXBwU3RhdGVcbntcbiAgICByZWFkb25seSBiZXRzOklSZXNvbHZlZEJldHM7XG4gICAgcmVhZG9ubHkgcmVmZXJyZXI6c3RyaW5nO1xuICAgIHNlZWRGb3JSYW5kb21uZXNzOnN0cmluZztcblxuICAgIG9uQWRkVG9CYWxhbmNlKHRvZ2dsZTogYm9vbGVhbiwgYW10PzogbnVtYmVyIHwgc3RyaW5nKTogdm9pZDtcbiAgICBzZXRMaXN0ZW5lcihsaXN0ZW5lcjpJQXBwU3RhdGVMaXN0ZW5lcik6dm9pZDtcbn0iXX0=