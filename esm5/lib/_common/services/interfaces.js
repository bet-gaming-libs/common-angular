/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/services/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEosAppContext() { }
if (false) {
    /** @type {?} */
    IEosAppContext.prototype.appId;
    /** @type {?} */
    IEosAppContext.prototype.eos;
    /** @type {?} */
    IEosAppContext.prototype.wallet;
    /**
     * @return {?}
     */
    IEosAppContext.prototype.onAuthenticated = function () { };
}
/**
 * @record
 */
export function IAlertService() { }
if (false) {
    /**
     * @param {?} message
     * @return {?}
     */
    IAlertService.prototype.error = function (message) { };
    /**
     * @param {?} message
     * @return {?}
     */
    IAlertService.prototype.success = function (message) { };
    /**
     * @return {?}
     */
    IAlertService.prototype.clear = function () { };
}
/**
 * @record
 */
export function IBankrollService() { }
if (false) {
    /**
     * @param {?} payoutFactor
     * @return {?}
     */
    IBankrollService.prototype.getMaxBet = function (payoutFactor) { };
}
/**
 * @record
 */
export function ICurrencyService() { }
if (false) {
    /**
     * @param {?} tokenSymbol
     * @return {?}
     */
    ICurrencyService.prototype.getPrice = function (tokenSymbol) { };
}
/**
 * @record
 */
export function IMainAppContext() { }
if (false) {
    /** @type {?} */
    IMainAppContext.prototype.state;
    /** @type {?} */
    IMainAppContext.prototype.settings;
    /** @type {?} */
    IMainAppContext.prototype.alert;
    /** @type {?} */
    IMainAppContext.prototype.bankroll;
    /** @type {?} */
    IMainAppContext.prototype.currency;
    /** @type {?} */
    IMainAppContext.prototype.blackjack;
    /**
     * @return {?}
     */
    IMainAppContext.prototype.scrollToTopOfPage = function () { };
    /**
     * @return {?}
     */
    IMainAppContext.prototype.openInsufficentFundsModal = function () { };
}
/**
 * @record
 */
export function IChatAppContext() { }
if (false) {
    /**
     * @param {?} data
     * @param {?} isNewMessage
     * @return {?}
     */
    IChatAppContext.prototype.onChatMessage = function (data, isNewMessage) { };
    /**
     * @return {?}
     */
    IChatAppContext.prototype.getRecentMessages = function () { };
}
/**
 * @record
 */
export function IChatComponent() { }
if (false) {
    /**
     * @return {?}
     */
    IChatComponent.prototype.scrollToBottom = function () { };
}
/**
 * @record
 */
export function IGoogleAnalyticsService() { }
if (false) {
    /**
     * @param {?} txnId
     * @param {?} game
     * @param {?} currency
     * @param {?} amountInUSD
     * @param {?} isAutoBet
     * @param {?} isJackpot
     * @return {?}
     */
    IGoogleAnalyticsService.prototype.addTransaction = function (txnId, game, currency, amountInUSD, isAutoBet, isJackpot) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi9zZXJ2aWNlcy9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBUUEsb0NBT0M7OztJQUxHLCtCQUFzQjs7SUFDdEIsNkJBQXlCOztJQUN6QixnQ0FBbUM7Ozs7SUFFbkMsMkRBQXVCOzs7OztBQUczQixtQ0FLQzs7Ozs7O0lBSEcsdURBQTJCOzs7OztJQUMzQix5REFBNkI7Ozs7SUFDN0IsZ0RBQWE7Ozs7O0FBR2pCLHNDQUdDOzs7Ozs7SUFERyxtRUFBK0M7Ozs7O0FBR25ELHNDQUdDOzs7Ozs7SUFERyxpRUFBb0M7Ozs7O0FBR3hDLHFDQWtCQzs7O0lBaEJHLGdDQUF5Qjs7SUFDekIsbUNBQStCOztJQUMvQixnQ0FBNkI7O0lBQzdCLG1DQUFtQzs7SUFDbkMsbUNBQW1DOztJQUVuQyxvQ0FBd0M7Ozs7SUFFeEMsOERBQXlCOzs7O0lBQ3pCLHNFQUFpQzs7Ozs7QUFVckMscUNBSUM7Ozs7Ozs7SUFGRyw0RUFBb0U7Ozs7SUFDcEUsOERBQXlCOzs7OztBQUc3QixvQ0FHQzs7Ozs7SUFERywwREFBc0I7Ozs7O0FBRzFCLDZDQVVDOzs7Ozs7Ozs7OztJQVJHLDJIQU9PIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJVmFsaWRhdGVkQ2hhdE1lc3NhZ2V9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuaW1wb3J0IHsgSUVvc1NlcnZpY2UgfSBmcm9tICcuLi9lb3MvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJV2FsbGV0U2VydmljZUJhc2UgfSBmcm9tICcuLi93YWxsZXRzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSUFwcFNldHRpbmdzLCBJQXBwU3RhdGUgfSBmcm9tICcuLi9tb2RlbHMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQmxhY2tqYWNrQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL2JsYWNramFjay9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcblxuXG5leHBvcnQgaW50ZXJmYWNlIElFb3NBcHBDb250ZXh0XG57XG4gICAgcmVhZG9ubHkgYXBwSWQ6c3RyaW5nO1xuICAgIHJlYWRvbmx5IGVvczpJRW9zU2VydmljZTtcbiAgICByZWFkb25seSB3YWxsZXQ6SVdhbGxldFNlcnZpY2VCYXNlO1xuXG4gICAgb25BdXRoZW50aWNhdGVkKCk6dm9pZDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQWxlcnRTZXJ2aWNlXG57XG4gICAgZXJyb3IobWVzc2FnZTpzdHJpbmcpOnZvaWQ7XG4gICAgc3VjY2VzcyhtZXNzYWdlOnN0cmluZyk6dm9pZDtcbiAgICBjbGVhcigpOnZvaWQ7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUJhbmtyb2xsU2VydmljZVxue1xuICAgIGdldE1heEJldChwYXlvdXRGYWN0b3I6bnVtYmVyKTpQcm9taXNlPG51bWJlcj47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUN1cnJlbmN5U2VydmljZVxue1xuICAgIGdldFByaWNlKHRva2VuU3ltYm9sOnN0cmluZyk6bnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElNYWluQXBwQ29udGV4dCBleHRlbmRzIElFb3NBcHBDb250ZXh0XG57XG4gICAgcmVhZG9ubHkgc3RhdGU6SUFwcFN0YXRlO1xuICAgIHJlYWRvbmx5IHNldHRpbmdzOklBcHBTZXR0aW5ncztcbiAgICByZWFkb25seSBhbGVydDpJQWxlcnRTZXJ2aWNlO1xuICAgIHJlYWRvbmx5IGJhbmtyb2xsOklCYW5rcm9sbFNlcnZpY2U7XG4gICAgcmVhZG9ubHkgY3VycmVuY3k6SUN1cnJlbmN5U2VydmljZTtcblxuICAgIHJlYWRvbmx5IGJsYWNramFjazpJQmxhY2tqYWNrQXBwU2VydmljZTtcblxuICAgIHNjcm9sbFRvVG9wT2ZQYWdlKCk6dm9pZDtcbiAgICBvcGVuSW5zdWZmaWNlbnRGdW5kc01vZGFsKCk6dm9pZDtcblxuXG4gICAgLypcbiAgICBsb2dpbldpdGhTY2F0dGVyKCk6dm9pZDtcbiAgICBnZXRVc2VySWQoKTogUHJvbWlzZTxudW1iZXI+O1xuICAgICovXG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQ2hhdEFwcENvbnRleHQgXG57XG4gICAgb25DaGF0TWVzc2FnZShkYXRhOklWYWxpZGF0ZWRDaGF0TWVzc2FnZSxpc05ld01lc3NhZ2U6Ym9vbGVhbik6dm9pZDtcbiAgICBnZXRSZWNlbnRNZXNzYWdlcygpOnZvaWQ7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUNoYXRDb21wb25lbnRcbntcbiAgICBzY3JvbGxUb0JvdHRvbSgpOnZvaWQ7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUdvb2dsZUFuYWx5dGljc1NlcnZpY2VcbntcbiAgICBhZGRUcmFuc2FjdGlvbihcbiAgICAgICAgdHhuSWQ6c3RyaW5nLFxuICAgICAgICBnYW1lOnN0cmluZyxcbiAgICAgICAgY3VycmVuY3k6c3RyaW5nLFxuICAgICAgICBhbW91bnRJblVTRDpzdHJpbmcsXG4gICAgICAgIGlzQXV0b0JldDpib29sZWFuLFxuICAgICAgICBpc0phY2twb3Q6Ym9vbGVhblxuICAgICk6dm9pZDtcbn0iXX0=