/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/services/overlay-modal.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
var OverlayModalService = /** @class */ (function () {
    function OverlayModalService() {
        this.subject = new Subject();
    }
    /**
     * @param {?} message
     * @return {?}
     */
    OverlayModalService.prototype.open = /**
     * @param {?} message
     * @return {?}
     */
    function (message) {
        this.close();
        this.subject.next(message);
    };
    /**
     * @return {?}
     */
    OverlayModalService.prototype.close = /**
     * @return {?}
     */
    function () {
        this.subject.next();
    };
    /**
     * @return {?}
     */
    OverlayModalService.prototype.getMessage = /**
     * @return {?}
     */
    function () {
        return this.subject.asObservable();
    };
    OverlayModalService.decorators = [
        { type: Injectable }
    ];
    OverlayModalService.ctorParameters = function () { return []; };
    return OverlayModalService;
}());
export { OverlayModalService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    OverlayModalService.prototype.subject;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3ZlcmxheS1tb2RhbC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL3NlcnZpY2VzL292ZXJsYXktbW9kYWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUUzQztJQUlJO1FBRlEsWUFBTyxHQUFHLElBQUksT0FBTyxFQUFVLENBQUM7SUFHeEMsQ0FBQzs7Ozs7SUFDRCxrQ0FBSTs7OztJQUFKLFVBQUssT0FBZTtRQUNoQixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFYixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvQixDQUFDOzs7O0lBRUQsbUNBQUs7OztJQUFMO1FBQ0ksSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsd0NBQVU7OztJQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3ZDLENBQUM7O2dCQWxCSixVQUFVOzs7SUFtQlgsMEJBQUM7Q0FBQSxBQW5CRCxJQW1CQztTQWxCWSxtQkFBbUI7Ozs7OztJQUM1QixzQ0FBd0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBPdmVybGF5TW9kYWxTZXJ2aWNlIHtcbiAgICBwcml2YXRlIHN1YmplY3QgPSBuZXcgU3ViamVjdDxzdHJpbmc+KCk7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICB9XG4gICAgb3BlbihtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5jbG9zZSgpO1xuXG4gICAgICAgIHRoaXMuc3ViamVjdC5uZXh0KG1lc3NhZ2UpO1xuICAgIH1cblxuICAgIGNsb3NlKCl7XG4gICAgICAgIHRoaXMuc3ViamVjdC5uZXh0KCk7XG4gICAgfVxuXG4gICAgZ2V0TWVzc2FnZSgpOiBPYnNlcnZhYmxlPHN0cmluZz4ge1xuICAgICAgICByZXR1cm4gdGhpcy5zdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuICAgIH1cbn0iXX0=