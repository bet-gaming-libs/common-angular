/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/translations/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ITranslationBase() { }
if (false) {
    /** @type {?} */
    ITranslationBase.prototype.theMinimumBetIs;
    /** @type {?} */
    ITranslationBase.prototype.theMaximumBetIs;
    /**
     * @param {?} betTokens
     * @param {?} otherTokens
     * @return {?}
     */
    ITranslationBase.prototype.tooltipDividends = function (betTokens, otherTokens) { };
    /**
     * @param {?} price
     * @return {?}
     */
    ITranslationBase.prototype.clickThisBoxToReceive1JackpotSpinForPrice = function (price) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi90cmFuc2xhdGlvbnMvaW50ZXJmYWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLHNDQU9DOzs7SUFMRywyQ0FBdUI7O0lBQ3ZCLDJDQUF1Qjs7Ozs7O0lBRXZCLG9GQUE4RDs7Ozs7SUFDOUQsNEZBQStEIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJVHJhbnNsYXRpb25CYXNlXG57XG4gICAgdGhlTWluaW11bUJldElzOnN0cmluZztcbiAgICB0aGVNYXhpbXVtQmV0SXM6c3RyaW5nO1xuICAgIFxuICAgIHRvb2x0aXBEaXZpZGVuZHMoYmV0VG9rZW5zOnN0cmluZywgb3RoZXJUb2tlbnM6c3RyaW5nKTpzdHJpbmc7XG4gICAgY2xpY2tUaGlzQm94VG9SZWNlaXZlMUphY2twb3RTcGluRm9yUHJpY2UocHJpY2U6c3RyaW5nKTpzdHJpbmc7XG59Il19