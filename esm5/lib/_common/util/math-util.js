/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/util/math-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} amount
 * @param {?} digits
 * @param {?=} precision
 * @return {?}
 */
export function toTotalDigits(amount, digits, precision) {
    if (precision === void 0) { precision = undefined; }
    return numStringToTotalDigits(String(amount), digits, precision);
}
/**
 * @param {?} amount
 * @param {?} digits
 * @param {?=} precision
 * @return {?}
 */
export function numStringToTotalDigits(amount, digits, precision) {
    if (precision === void 0) { precision = undefined; }
    /** @type {?} */
    var rounded = roundStringDownWithPrecision(amount, digits + 1);
    /** @type {?} */
    var numOfDigitsBeforeDecimal = getNumOfDigitsBeforeDecimal(rounded);
    /** @type {?} */
    var numOfDigitsAfterDecimal = digits - numOfDigitsBeforeDecimal;
    if (precision != undefined &&
        numOfDigitsAfterDecimal > precision) {
        numOfDigitsAfterDecimal = precision;
    }
    return numOfDigitsAfterDecimal > 0 ?
        roundStringDownWithPrecision(rounded, numOfDigitsAfterDecimal) :
        rounded.split('.')[0];
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
export function roundDownWithPrecision(amount, precision) {
    //amount = Number(amount);
    return roundStringDownWithPrecision(String(amount), precision);
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
export function roundDownWithoutTrailingZeros(amount, precision) {
    /** @type {?} */
    var valueWithAllDigits = roundStringDownWithPrecision(amount, precision);
    /** @type {?} */
    var parts = valueWithAllDigits.split('.');
    /** @type {?} */
    var digitsBeforeDecimal = parts[0];
    /** @type {?} */
    var digitsAfterDecimal = parts[1];
    /** @type {?} */
    var numOfDecimalDigitsToInclude = 0;
    for (var i = digitsAfterDecimal.length - 1; i > -1; i--) {
        /** @type {?} */
        var digit = digitsAfterDecimal[i];
        if (digit != '0') {
            numOfDecimalDigitsToInclude = i + 1;
            break;
        }
    }
    return numOfDecimalDigitsToInclude > 0 ?
        digitsBeforeDecimal + '.' + digitsAfterDecimal.substr(0, numOfDecimalDigitsToInclude) :
        digitsBeforeDecimal;
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
export function roundStringDownWithPrecision(amount, precision) {
    /** @type {?} */
    var parts = String(amount).split('.');
    /** @type {?} */
    var isWholeNumber = parts.length == 1;
    /** @type {?} */
    var rounded = Number(parts[0] + '.' +
        (isWholeNumber ?
            '0' :
            parts[1].substr(0, precision)));
    /** @type {?} */
    var result = rounded.toFixed(precision);
    return result;
}
/**
 * @param {?} number
 * @return {?}
 */
function getNumOfDigitsBeforeDecimal(number) {
    /** @type {?} */
    var parts = number.split('.');
    return parts[0].length;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0aC11dGlsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9fY29tbW9uL3V0aWwvbWF0aC11dGlsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsTUFBTSx3QkFBd0IsTUFBYSxFQUFDLE1BQWEsRUFBQyxTQUEwQjtJQUExQiwwQkFBQSxFQUFBLHFCQUEwQjtJQUNoRixPQUFPLHNCQUFzQixDQUN6QixNQUFNLENBQUMsTUFBTSxDQUFDLEVBQ2QsTUFBTSxFQUNOLFNBQVMsQ0FDWixDQUFDO0FBQ04sQ0FBQzs7Ozs7OztBQUVELE1BQU0saUNBQWlDLE1BQWEsRUFBQyxNQUFhLEVBQUMsU0FBMEI7SUFBMUIsMEJBQUEsRUFBQSxxQkFBMEI7O1FBQ25GLE9BQU8sR0FBRyw0QkFBNEIsQ0FBQyxNQUFNLEVBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQzs7UUFFdkQsd0JBQXdCLEdBQUcsMkJBQTJCLENBQUMsT0FBTyxDQUFDOztRQUVqRSx1QkFBdUIsR0FBRyxNQUFNLEdBQUcsd0JBQXdCO0lBRS9ELElBQ0ksU0FBUyxJQUFJLFNBQVM7UUFDdEIsdUJBQXVCLEdBQUcsU0FBUyxFQUNyQztRQUNFLHVCQUF1QixHQUFHLFNBQVMsQ0FBQztLQUN2QztJQUVELE9BQU8sdUJBQXVCLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDNUIsNEJBQTRCLENBQ3hCLE9BQU8sRUFDUCx1QkFBdUIsQ0FDMUIsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUNsQyxDQUFDOzs7Ozs7QUFHRCxNQUFNLGlDQUFpQyxNQUFhLEVBQUMsU0FBZ0I7SUFDakUsMEJBQTBCO0lBRTFCLE9BQU8sNEJBQTRCLENBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFDLFNBQVMsQ0FBRSxDQUFDO0FBQ3BFLENBQUM7Ozs7OztBQUVELE1BQU0sd0NBQXdDLE1BQWEsRUFBQyxTQUFnQjs7UUFDbEUsa0JBQWtCLEdBQUcsNEJBQTRCLENBQUMsTUFBTSxFQUFDLFNBQVMsQ0FBQzs7UUFFbkUsS0FBSyxHQUFHLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7O1FBQ3JDLG1CQUFtQixHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7O1FBQzlCLGtCQUFrQixHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7O1FBRS9CLDJCQUEyQixHQUFVLENBQUM7SUFFMUMsS0FBSyxJQUFJLENBQUMsR0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTs7WUFDN0MsS0FBSyxHQUFHLGtCQUFrQixDQUFDLENBQUMsQ0FBQztRQUVuQyxJQUFJLEtBQUssSUFBSSxHQUFHLEVBQUU7WUFDZCwyQkFBMkIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXBDLE1BQU07U0FDVDtLQUNKO0lBRUQsT0FBTywyQkFBMkIsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNoQyxtQkFBbUIsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUM7UUFDdEYsbUJBQW1CLENBQUM7QUFDaEMsQ0FBQzs7Ozs7O0FBRUQsTUFBTSx1Q0FBdUMsTUFBYSxFQUFDLFNBQWdCOztRQUNuRSxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7O1FBRWpDLGFBQWEsR0FDYixLQUFLLENBQUMsTUFBTSxJQUFJLENBQUM7O1FBRWpCLE9BQU8sR0FBRyxNQUFNLENBQ2hCLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHO1FBQ2QsQ0FDSSxhQUFhLENBQUMsQ0FBQztZQUNYLEdBQUcsQ0FBQyxDQUFDO1lBQ0wsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsU0FBUyxDQUFDLENBQ25DLENBQ0o7O1FBRUcsTUFBTSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO0lBRXZDLE9BQU8sTUFBTSxDQUFDO0FBRWxCLENBQUM7Ozs7O0FBRUQscUNBQXFDLE1BQWE7O1FBQ3hDLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztJQUUvQixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFDM0IsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBmdW5jdGlvbiB0b1RvdGFsRGlnaXRzKGFtb3VudDpudW1iZXIsZGlnaXRzOm51bWJlcixwcmVjaXNpb246bnVtYmVyPXVuZGVmaW5lZCkge1xuICAgIHJldHVybiBudW1TdHJpbmdUb1RvdGFsRGlnaXRzKFxuICAgICAgICBTdHJpbmcoYW1vdW50KSxcbiAgICAgICAgZGlnaXRzLFxuICAgICAgICBwcmVjaXNpb25cbiAgICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbnVtU3RyaW5nVG9Ub3RhbERpZ2l0cyhhbW91bnQ6c3RyaW5nLGRpZ2l0czpudW1iZXIscHJlY2lzaW9uOm51bWJlcj11bmRlZmluZWQpIHtcbiAgICBjb25zdCByb3VuZGVkID0gcm91bmRTdHJpbmdEb3duV2l0aFByZWNpc2lvbihhbW91bnQsZGlnaXRzKzEpOyAgXG4gICAgXG4gICAgY29uc3QgbnVtT2ZEaWdpdHNCZWZvcmVEZWNpbWFsID0gZ2V0TnVtT2ZEaWdpdHNCZWZvcmVEZWNpbWFsKHJvdW5kZWQpO1xuXG4gICAgbGV0IG51bU9mRGlnaXRzQWZ0ZXJEZWNpbWFsID0gZGlnaXRzIC0gbnVtT2ZEaWdpdHNCZWZvcmVEZWNpbWFsO1xuXG4gICAgaWYgKFxuICAgICAgICBwcmVjaXNpb24gIT0gdW5kZWZpbmVkICYmXG4gICAgICAgIG51bU9mRGlnaXRzQWZ0ZXJEZWNpbWFsID4gcHJlY2lzaW9uXG4gICAgKSB7XG4gICAgICAgIG51bU9mRGlnaXRzQWZ0ZXJEZWNpbWFsID0gcHJlY2lzaW9uO1xuICAgIH1cblxuICAgIHJldHVybiBudW1PZkRpZ2l0c0FmdGVyRGVjaW1hbCA+IDAgP1xuICAgICAgICAgICAgcm91bmRTdHJpbmdEb3duV2l0aFByZWNpc2lvbihcbiAgICAgICAgICAgICAgICByb3VuZGVkLFxuICAgICAgICAgICAgICAgIG51bU9mRGlnaXRzQWZ0ZXJEZWNpbWFsXG4gICAgICAgICAgICApIDpcbiAgICAgICAgICAgIHJvdW5kZWQuc3BsaXQoJy4nKVswXTtcbn1cblxuXG5leHBvcnQgZnVuY3Rpb24gcm91bmREb3duV2l0aFByZWNpc2lvbihhbW91bnQ6bnVtYmVyLHByZWNpc2lvbjpudW1iZXIpIHtcbiAgICAvL2Ftb3VudCA9IE51bWJlcihhbW91bnQpO1xuXG4gICAgcmV0dXJuIHJvdW5kU3RyaW5nRG93bldpdGhQcmVjaXNpb24oIFN0cmluZyhhbW91bnQpLHByZWNpc2lvbiApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcm91bmREb3duV2l0aG91dFRyYWlsaW5nWmVyb3MoYW1vdW50OnN0cmluZyxwcmVjaXNpb246bnVtYmVyKTpzdHJpbmcge1xuICAgIGNvbnN0IHZhbHVlV2l0aEFsbERpZ2l0cyA9IHJvdW5kU3RyaW5nRG93bldpdGhQcmVjaXNpb24oYW1vdW50LHByZWNpc2lvbik7XG5cbiAgICBjb25zdCBwYXJ0cyA9IHZhbHVlV2l0aEFsbERpZ2l0cy5zcGxpdCgnLicpO1xuICAgIGNvbnN0IGRpZ2l0c0JlZm9yZURlY2ltYWwgPSBwYXJ0c1swXTtcbiAgICBjb25zdCBkaWdpdHNBZnRlckRlY2ltYWwgPSBwYXJ0c1sxXTtcblxuICAgIGxldCBudW1PZkRlY2ltYWxEaWdpdHNUb0luY2x1ZGU6bnVtYmVyID0gMDtcblxuICAgIGZvciAodmFyIGk9ZGlnaXRzQWZ0ZXJEZWNpbWFsLmxlbmd0aCAtIDE7IGkgPiAtMTsgaS0tKSB7XG4gICAgICAgIGNvbnN0IGRpZ2l0ID0gZGlnaXRzQWZ0ZXJEZWNpbWFsW2ldO1xuXG4gICAgICAgIGlmIChkaWdpdCAhPSAnMCcpIHtcbiAgICAgICAgICAgIG51bU9mRGVjaW1hbERpZ2l0c1RvSW5jbHVkZSA9IGkgKyAxO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgIH1cbiAgICBcbiAgICByZXR1cm4gbnVtT2ZEZWNpbWFsRGlnaXRzVG9JbmNsdWRlID4gMCA/XG4gICAgICAgICAgICBkaWdpdHNCZWZvcmVEZWNpbWFsICsgJy4nICsgZGlnaXRzQWZ0ZXJEZWNpbWFsLnN1YnN0cigwLG51bU9mRGVjaW1hbERpZ2l0c1RvSW5jbHVkZSkgOlxuICAgICAgICAgICAgZGlnaXRzQmVmb3JlRGVjaW1hbDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHJvdW5kU3RyaW5nRG93bldpdGhQcmVjaXNpb24oYW1vdW50OnN0cmluZyxwcmVjaXNpb246bnVtYmVyKSB7XG4gICAgdmFyIHBhcnRzID0gU3RyaW5nKGFtb3VudCkuc3BsaXQoJy4nKTtcblxuICAgIHZhciBpc1dob2xlTnVtYmVyID1cbiAgICAgICAgcGFydHMubGVuZ3RoID09IDE7XG5cbiAgICB2YXIgcm91bmRlZCA9IE51bWJlciggXG4gICAgICAgIHBhcnRzWzBdICsgJy4nICsgXG4gICAgICAgIChcbiAgICAgICAgICAgIGlzV2hvbGVOdW1iZXIgP1xuICAgICAgICAgICAgICAgICcwJyA6XG4gICAgICAgICAgICAgICAgcGFydHNbMV0uc3Vic3RyKDAscHJlY2lzaW9uKSBcbiAgICAgICAgKVxuICAgICk7XG4gICAgXG4gICAgdmFyIHJlc3VsdCA9IHJvdW5kZWQudG9GaXhlZChwcmVjaXNpb24pO1xuICAgIFxuICAgIHJldHVybiByZXN1bHQ7XG5cbn1cblxuZnVuY3Rpb24gZ2V0TnVtT2ZEaWdpdHNCZWZvcmVEZWNpbWFsKG51bWJlcjpzdHJpbmcpIHtcbiAgICBjb25zdCBwYXJ0cyA9IG51bWJlci5zcGxpdCgnLicpO1xuXG4gICAgcmV0dXJuIHBhcnRzWzBdLmxlbmd0aDtcbn0iXX0=