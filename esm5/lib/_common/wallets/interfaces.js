/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/wallets/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IWalletTransferData() { }
if (false) {
    /** @type {?} */
    IWalletTransferData.prototype.toAccount;
    /** @type {?} */
    IWalletTransferData.prototype.amount;
    /** @type {?} */
    IWalletTransferData.prototype.memo;
}
/**
 * @record
 */
export function IWalletServiceBase() { }
if (false) {
    /** @type {?} */
    IWalletServiceBase.prototype.isPasswordRequiredForLogin;
    /** @type {?} */
    IWalletServiceBase.prototype.loginPrompt;
    /** @type {?} */
    IWalletServiceBase.prototype.isLoggedIn;
    /** @type {?} */
    IWalletServiceBase.prototype.hasLogout;
    /** @type {?} */
    IWalletServiceBase.prototype.accountKeyForReferralTable;
    /** @type {?} */
    IWalletServiceBase.prototype.myReferralCode;
    /** @type {?} */
    IWalletServiceBase.prototype.canClaimDividends;
    /** @type {?} */
    IWalletServiceBase.prototype.isEasyAccount;
    /** @type {?} */
    IWalletServiceBase.prototype.accountName;
    /** @type {?} */
    IWalletServiceBase.prototype.uniqueAccountName;
    /** @type {?} */
    IWalletServiceBase.prototype.accountId;
    /** @type {?} */
    IWalletServiceBase.prototype.publicKey;
    /** @type {?} */
    IWalletServiceBase.prototype.canSafelySign;
    /** @type {?} */
    IWalletServiceBase.prototype.betBalance;
    /**
     * @return {?}
     */
    IWalletServiceBase.prototype.login = function () { };
    /**
     * @return {?}
     */
    IWalletServiceBase.prototype.syncTokenBalances = function () { };
    /**
     * @param {?} tokenSymbol
     * @param {?} amount
     * @return {?}
     */
    IWalletServiceBase.prototype.addToBalance = function (tokenSymbol, amount) { };
    /**
     * @param {?} data
     * @return {?}
     */
    IWalletServiceBase.prototype.transfer = function (data) { };
    /**
     * @param {?} message
     * @param {?} reason
     * @return {?}
     */
    IWalletServiceBase.prototype.sign = function (message, reason) { };
    /**
     * @return {?}
     */
    IWalletServiceBase.prototype.logout = function () { };
    /**
     * @param {?} referrer
     * @return {?}
     */
    IWalletServiceBase.prototype.isReferrerMe = function (referrer) { };
    /**
     * @param {?} tokenSymbol
     * @return {?}
     */
    IWalletServiceBase.prototype.getBalance = function (tokenSymbol) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvX2NvbW1vbi93YWxsZXRzL2ludGVyZmFjZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFJQSx5Q0FLQzs7O0lBSEcsd0NBQWlCOztJQUNqQixxQ0FBdUI7O0lBQ3ZCLG1DQUFZOzs7OztBQUloQix3Q0EwQ0M7OztJQXhDRyx3REFBNEM7O0lBQzVDLHlDQUE0Qjs7SUFFNUIsd0NBQTRCOztJQUM1Qix1Q0FBMkI7O0lBRTNCLHdEQUEyQzs7SUFDM0MsNENBQStCOztJQUUvQiwrQ0FBbUM7O0lBRW5DLDJDQUErQjs7SUFDL0IseUNBQTRCOztJQUM1QiwrQ0FBa0M7O0lBQ2xDLHVDQUEwQjs7SUFFMUIsdUNBQTBCOztJQUMxQiwyQ0FBK0I7O0lBRS9CLHdDQUEyQjs7OztJQUczQixxREFBc0I7Ozs7SUFFdEIsaUVBQXlCOzs7Ozs7SUFFekIsK0VBQW9EOzs7OztJQUdwRCw0REFFOEI7Ozs7OztJQUU5QixtRUFBMkQ7Ozs7SUFFM0Qsc0RBQWM7Ozs7O0lBRWQsb0VBQXNDOzs7OztJQUV0QyxxRUFBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0lUcmFuc2FjdGlvblJlc3VsdCwgSVNpZ25hdHVyZUluZm8gfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5pbXBvcnQge0lFT1NBc3NldEFtb3VudH0gZnJvbSAnLi4vZW9zL2Vvcy1udW1iZXJzJztcblxuXG5leHBvcnQgaW50ZXJmYWNlIElXYWxsZXRUcmFuc2ZlckRhdGFcbntcbiAgICB0b0FjY291bnQ6c3RyaW5nO1xuICAgIGFtb3VudDpJRU9TQXNzZXRBbW91bnQ7XG4gICAgbWVtbzpzdHJpbmc7XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJV2FsbGV0U2VydmljZUJhc2VcbntcbiAgICByZWFkb25seSBpc1Bhc3N3b3JkUmVxdWlyZWRGb3JMb2dpbjpib29sZWFuO1xuICAgIHJlYWRvbmx5IGxvZ2luUHJvbXB0OnN0cmluZztcblxuICAgIHJlYWRvbmx5IGlzTG9nZ2VkSW46Ym9vbGVhbjtcbiAgICByZWFkb25seSBoYXNMb2dvdXQ6Ym9vbGVhbjtcblxuICAgIHJlYWRvbmx5IGFjY291bnRLZXlGb3JSZWZlcnJhbFRhYmxlOnN0cmluZztcbiAgICByZWFkb25seSBteVJlZmVycmFsQ29kZTpzdHJpbmc7XG5cbiAgICByZWFkb25seSBjYW5DbGFpbURpdmlkZW5kczpib29sZWFuO1xuICAgIFxuICAgIHJlYWRvbmx5IGlzRWFzeUFjY291bnQ6Ym9vbGVhbjtcbiAgICByZWFkb25seSBhY2NvdW50TmFtZTpzdHJpbmc7XG4gICAgcmVhZG9ubHkgdW5pcXVlQWNjb3VudE5hbWU6c3RyaW5nO1xuICAgIHJlYWRvbmx5IGFjY291bnRJZDpzdHJpbmc7XG5cbiAgICByZWFkb25seSBwdWJsaWNLZXk6c3RyaW5nO1xuICAgIHJlYWRvbmx5IGNhblNhZmVseVNpZ246Ym9vbGVhbjtcblxuICAgIHJlYWRvbmx5IGJldEJhbGFuY2U6c3RyaW5nO1xuXG4gICAgXG4gICAgbG9naW4oKTpQcm9taXNlPHZvaWQ+O1xuXG4gICAgc3luY1Rva2VuQmFsYW5jZXMoKTp2b2lkO1xuXG4gICAgYWRkVG9CYWxhbmNlKHRva2VuU3ltYm9sOnN0cmluZyxhbW91bnQ6bnVtYmVyKTp2b2lkO1xuICAgIFxuICAgIFxuICAgIHRyYW5zZmVyKFxuICAgICAgICBkYXRhOklXYWxsZXRUcmFuc2ZlckRhdGFbXVxuICAgICk6UHJvbWlzZTxJVHJhbnNhY3Rpb25SZXN1bHQ+O1xuXG4gICAgc2lnbihtZXNzYWdlOnN0cmluZyxyZWFzb246c3RyaW5nKTpQcm9taXNlPElTaWduYXR1cmVJbmZvPjtcblxuICAgIGxvZ291dCgpOnZvaWQ7XG5cbiAgICBpc1JlZmVycmVyTWUocmVmZXJyZXI6c3RyaW5nKTpib29sZWFuO1xuXG4gICAgZ2V0QmFsYW5jZSh0b2tlblN5bWJvbDpzdHJpbmcpOnN0cmluZztcbn1cblxuLypcbmV4cG9ydCBpbnRlcmZhY2UgSVdhbGxldE1hbmFnZXJcbntcbiAgICByZWFkb25seSB3YWxsZXQ6SVdhbGxldFNlcnZpY2U7XG5cbiAgICByZWFkb25seSBpc0xvZ2dlZEluVG9FYXN5QWNjb3VudDpib29sZWFuO1xuICAgIHJlYWRvbmx5IGlzRWFzeUFjY291bnRDcmVhdGVkOmJvb2xlYW47XG4gICAgcmVhZG9ubHkgZWFzeUFjY291bnROYW1lOnN0cmluZztcblxuICAgIGFmdGVyVmlld0luaXQoKTp2b2lkO1xuICAgIHVzZVNjYXR0ZXIoKTp2b2lkO1xuICAgIHVzZUVhc3lBY2NvdW50KCk6dm9pZDtcbn1cbiovIl19