/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/blackjack.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlackjackModalComponent } from './components/blackjack-modal/blackjack-modal.component';
var BlackjackModule = /** @class */ (function () {
    function BlackjackModule() {
    }
    BlackjackModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        BlackjackModalComponent,
                    ],
                    imports: [
                        CommonModule
                    ],
                    exports: [
                        BlackjackModalComponent,
                    ],
                    entryComponents: [
                        BlackjackModalComponent
                    ]
                },] }
    ];
    return BlackjackModule;
}());
export { BlackjackModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2JsYWNramFjay5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUs3QyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUdqRztJQUFBO0lBMEJBLENBQUM7O2dCQTFCQSxRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFO3dCQUNaLHVCQUF1QjtxQkFDeEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLFlBQVk7cUJBQ2I7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLHVCQUF1QjtxQkFDeEI7b0JBRUQsZUFBZSxFQUFFO3dCQUNmLHVCQUF1QjtxQkFDeEI7aUJBQ0Y7O0lBWUQsc0JBQUM7Q0FBQSxBQTFCRCxJQTBCQztTQVhZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cblxuaW1wb3J0IHsgSUJsYWNramFja01vZHVsZVNlcnZpY2VzIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcblxuaW1wb3J0IHsgQmxhY2tqYWNrTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvYmxhY2tqYWNrLW1vZGFsL2JsYWNramFjay1tb2RhbC5jb21wb25lbnQnO1xuXG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEJsYWNramFja01vZGFsQ29tcG9uZW50LFxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBCbGFja2phY2tNb2RhbENvbXBvbmVudCxcbiAgXSxcblxuICBlbnRyeUNvbXBvbmVudHM6IFtcbiAgICBCbGFja2phY2tNb2RhbENvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIEJsYWNramFja01vZHVsZSB7XG4gIC8qXG4gIHN0YXRpYyBmb3JSb290KHsgYXBwU2VydmljZSB9OiBJQmxhY2tqYWNrTW9kdWxlU2VydmljZXMgKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBCbGFja2phY2tNb2R1bGUsXG4gICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgeyAuLi5hcHBTZXJ2aWNlLCBwcm92aWRlOiAnQXBwU2VydmljZScgfSxcbiAgICAgIF0sXG4gICAgfTtcbiAgfVxuICAqL1xufVxuIl19