/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-card/blackjack-card.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Input } from '@angular/core';
/*
@Component({
    selector: '[blackjack-card]',
    templateUrl: './blackjack-card.component.html',
    styleUrls: ['./blackjack-card.component.scss']
})
*/
var BlackjackCardComponentBase = /** @class */ (function () {
    function BlackjackCardComponentBase() {
        this.direction = 'right';
        this.history = false;
    }
    /**
     * @return {?}
     */
    BlackjackCardComponentBase.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(BlackjackCardComponentBase.prototype, "suit", {
        get: /**
         * @return {?}
         */
        function () {
            return this.card ?
                this.card.suit :
                undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackCardComponentBase.prototype, "rank", {
        get: /**
         * @return {?}
         */
        function () {
            return this.card ?
                this.card.card :
                undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackCardComponentBase.prototype, "flipped", {
        get: /**
         * @return {?}
         */
        function () {
            return this.card ?
                this.card.flipped :
                undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackCardComponentBase.prototype, "dealt", {
        get: /**
         * @return {?}
         */
        function () {
            return this.card ?
                this.card.dealt :
                undefined;
        },
        enumerable: true,
        configurable: true
    });
    BlackjackCardComponentBase.propDecorators = {
        card: [{ type: Input }],
        direction: [{ type: Input }],
        history: [{ type: Input }]
    };
    return BlackjackCardComponentBase;
}());
export { BlackjackCardComponentBase };
if (false) {
    /** @type {?} */
    BlackjackCardComponentBase.prototype.card;
    /** @type {?} */
    BlackjackCardComponentBase.prototype.direction;
    /** @type {?} */
    BlackjackCardComponentBase.prototype.history;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWNhcmQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svY29tcG9uZW50cy9ibGFja2phY2stY2FyZC9ibGFja2phY2stY2FyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7OztBQVd0QztJQVdJO1FBTEEsY0FBUyxHQUFXLE9BQU8sQ0FBQztRQUc1QixZQUFPLEdBQUcsS0FBSyxDQUFDO0lBRUEsQ0FBQzs7OztJQUVqQiw2Q0FBUTs7O0lBQVI7SUFDQSxDQUFDO0lBRUQsc0JBQUksNENBQUk7Ozs7UUFBUjtZQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2hCLFNBQVMsQ0FBQztRQUN0QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDRDQUFJOzs7O1FBQVI7WUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDVixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQixTQUFTLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQ0FBTzs7OztRQUFYO1lBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDbkIsU0FBUyxDQUFDO1FBQ3RCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksNkNBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2pCLFNBQVMsQ0FBQztRQUN0QixDQUFDOzs7T0FBQTs7dUJBcENBLEtBQUs7NEJBR0wsS0FBSzswQkFHTCxLQUFLOztJQWdDVixpQ0FBQztDQUFBLEFBeENELElBd0NDO1NBeENZLDBCQUEwQjs7O0lBRW5DLDBDQUNxQjs7SUFFckIsK0NBQzRCOztJQUU1Qiw2Q0FDZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBJQ2FyZENvbnRyb2xsZXIgfSBmcm9tICcuLi8uLi8uLi9fY29tbW9uL2dhbWUvaW50ZXJmYWNlcyc7XG5cbi8qXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ1tibGFja2phY2stY2FyZF0nLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9ibGFja2phY2stY2FyZC5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vYmxhY2tqYWNrLWNhcmQuY29tcG9uZW50LnNjc3MnXVxufSlcbiovXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrQ2FyZENvbXBvbmVudEJhc2VcbntcbiAgICBASW5wdXQoKVxuICAgIGNhcmQ6SUNhcmRDb250cm9sbGVyO1xuXG4gICAgQElucHV0KClcbiAgICBkaXJlY3Rpb246IHN0cmluZyA9ICdyaWdodCc7XG5cbiAgICBASW5wdXQoKVxuICAgIGhpc3RvcnkgPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICB9XG5cbiAgICBnZXQgc3VpdCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZCA/XG4gICAgICAgICAgICAgICAgdGhpcy5jYXJkLnN1aXQgOlxuICAgICAgICAgICAgICAgIHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICBnZXQgcmFuaygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZCA/XG4gICAgICAgICAgICAgICAgdGhpcy5jYXJkLmNhcmQgOlxuICAgICAgICAgICAgICAgIHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICBnZXQgZmxpcHBlZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZCA/XG4gICAgICAgICAgICAgICAgdGhpcy5jYXJkLmZsaXBwZWQgOlxuICAgICAgICAgICAgICAgIHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICBnZXQgZGVhbHQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNhcmQgP1xuICAgICAgICAgICAgICAgIHRoaXMuY2FyZC5kZWFsdCA6XG4gICAgICAgICAgICAgICAgdW5kZWZpbmVkO1xuICAgIH1cblxufVxuIl19