/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-deposit-modal/blackjack-deposit-modal.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
@Component({
    selector: "blackjack-deposit-modal",
    templateUrl: "./blackjack-deposit-modal.component.html",
    styleUrls: ["./blackjack-deposit-modal.component.scss"]
})
*/
var /*
@Component({
    selector: "blackjack-deposit-modal",
    templateUrl: "./blackjack-deposit-modal.component.html",
    styleUrls: ["./blackjack-deposit-modal.component.scss"]
})
*/
BlackjackDepositModalBase = /** @class */ (function () {
    function BlackjackDepositModalBase(app, modal) {
        this.app = app;
        this.modal = modal;
    }
    Object.defineProperty(BlackjackDepositModalBase.prototype, "translation", {
        get: /**
         * @return {?}
         */
        function () {
            return this.app.settings.translation;
        },
        enumerable: true,
        configurable: true
    });
    return BlackjackDepositModalBase;
}());
/*
@Component({
    selector: "blackjack-deposit-modal",
    templateUrl: "./blackjack-deposit-modal.component.html",
    styleUrls: ["./blackjack-deposit-modal.component.scss"]
})
*/
export { BlackjackDepositModalBase };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackDepositModalBase.prototype.app;
    /** @type {?} */
    BlackjackDepositModalBase.prototype.modal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWRlcG9zaXQtbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svY29tcG9uZW50cy9ibGFja2phY2stZGVwb3NpdC1tb2RhbC9ibGFja2phY2stZGVwb3NpdC1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBYUE7Ozs7Ozs7O0lBRUUsbUNBQ1UsR0FBbUIsRUFDbEIsS0FBeUI7UUFEMUIsUUFBRyxHQUFILEdBQUcsQ0FBZ0I7UUFDbEIsVUFBSyxHQUFMLEtBQUssQ0FBb0I7SUFFcEMsQ0FBQztJQUVELHNCQUFJLGtEQUFXOzs7O1FBQWY7WUFDRSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztRQUN2QyxDQUFDOzs7T0FBQTtJQUNILGdDQUFDO0FBQUQsQ0FBQyxBQVhELElBV0M7Ozs7Ozs7Ozs7Ozs7O0lBUkcsd0NBQTJCOztJQUMzQiwwQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5pbXBvcnQgeyBJTWFpbkFwcENvbnRleHQgfSBmcm9tICcuLi8uLi8uLi9fY29tbW9uL3NlcnZpY2VzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgT3ZlcmxheU1vZGFsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vc2VydmljZXMvb3ZlcmxheS1tb2RhbC5zZXJ2aWNlJztcbmltcG9ydCB7IElUcmFuc2xhdGlvbkJhc2UgfSBmcm9tICcuLi8uLi8uLi9fY29tbW9uL3RyYW5zbGF0aW9ucy9pbnRlcmZhY2VzJztcblxuLypcbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiBcImJsYWNramFjay1kZXBvc2l0LW1vZGFsXCIsXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9ibGFja2phY2stZGVwb3NpdC1tb2RhbC5jb21wb25lbnQuaHRtbFwiLFxuICAgIHN0eWxlVXJsczogW1wiLi9ibGFja2phY2stZGVwb3NpdC1tb2RhbC5jb21wb25lbnQuc2Nzc1wiXVxufSlcbiovXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrRGVwb3NpdE1vZGFsQmFzZVxue1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGFwcDpJTWFpbkFwcENvbnRleHQsXG4gICAgcmVhZG9ubHkgbW9kYWw6T3ZlcmxheU1vZGFsU2VydmljZVxuICApIHtcbiAgfVxuXG4gIGdldCB0cmFuc2xhdGlvbigpOklUcmFuc2xhdGlvbkJhc2Uge1xuICAgIHJldHVybiB0aGlzLmFwcC5zZXR0aW5ncy50cmFuc2xhdGlvbjtcbiAgfVxufSJdfQ==