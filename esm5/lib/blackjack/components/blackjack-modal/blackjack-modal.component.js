/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-modal/blackjack-modal.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { sleep } from 'earnbet-common';
var BlackjackModalComponent = /** @class */ (function () {
    function BlackjackModalComponent() {
        this._isClosed = false;
        this.result = undefined;
    }
    /**
     * @param {?} result
     * @return {?}
     */
    BlackjackModalComponent.prototype.showResult = /**
     * @param {?} result
     * @return {?}
     */
    function (result) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.result = result;
                        this._isClosed = false;
                        return [4 /*yield*/, sleep(2 * 1000)];
                    case 1:
                        _a.sent();
                        this.close();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackModalComponent.prototype.close = /**
     * @return {?}
     */
    function () {
        this._isClosed = true;
    };
    Object.defineProperty(BlackjackModalComponent.prototype, "isOpen", {
        get: /**
         * @return {?}
         */
        function () {
            return this.result != undefined &&
                !this._isClosed;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackModalComponent.prototype, "isWin", {
        get: /**
         * @return {?}
         */
        function () {
            return this.result && this.result.isWin;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackModalComponent.prototype, "isPush", {
        get: /**
         * @return {?}
         */
        function () {
            return this.result && this.result.isPush;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackModalComponent.prototype, "isBlackjack", {
        get: /**
         * @return {?}
         */
        function () {
            return this.result && this.result.isBlackjack;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackModalComponent.prototype, "isLoss", {
        get: /**
         * @return {?}
         */
        function () {
            return this.result && this.result.isLoss;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackModalComponent.prototype, "isBusted", {
        get: /**
         * @return {?}
         */
        function () {
            return this.result && this.result.isBusted;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackModalComponent.prototype, "isInsuranceSuccess", {
        get: /**
         * @return {?}
         */
        function () {
            return this.result && this.result.isInsuranceSuccess;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackModalComponent.prototype, "amountWon", {
        get: /**
         * @return {?}
         */
        function () {
            return this.result && this.result.amountWon;
        },
        enumerable: true,
        configurable: true
    });
    BlackjackModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-blackjack-modal',
                    template: "<div *ngIf=\"isOpen\">\n  <div class=\"bj-modal\" [ngClass]=\"{\n                'red': isBusted || isLoss,\n                'green': isWin || isBlackjack,\n                'yellow': isPush,\n                'blue': isInsuranceSuccess\n              }\">\n  <button class=\"modal-close\" (click)='close()'>&times;</button>\n\n    <div class=\"align-items-center d-flex flex-column h-100 justify-content-center p-3\">\n      <div class=\"win\" *ngIf=\"isWin && !isBlackjack\">\n        <img class=\"mb-2 small-circle\" src=\"assets/img/blackjack/modal/win.png\" alt=\"win\">\n        <h1 class=\"bj-modal-title\">WIN!</h1>\n        <div class=\"points\">+ {{amountWon | number:'1.0-8'}}</div>\n      </div>\n\n      <div class=\"push\" *ngIf=\"isPush\">\n        <img class=\"mb-2 small-circle\" src=\"assets/img/blackjack/modal/push.png\" alt=\"push\">\n        <h1 class=\"bj-modal-title\">PUSH</h1>\n      </div>\n\n      <div class=\"blackjack\" *ngIf=\"isBlackjack\">\n        <img class=\"bj-image\" src=\"assets/img/blackjack/modal/blackjack.png\" alt=\"blackjack\">\n        <div class=\"points\">+ {{amountWon | number:'1.0-8'}}</div>\n      </div>\n\n      <div class=\"busted\" *ngIf=\"isBusted\">\n        <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/busted.png\" alt=\"busted\">\n        <h1 class=\"bj-modal-title\">BUSTED!</h1>\n      </div>\n\n     <div class=\"busted\" *ngIf=\"isLoss\">\n          <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/busted.png\" alt=\"busted\">\n          <h1 class=\"bj-modal-title\">LOSS</h1>\n      </div> \n\n\n      <!-- TODO: confirm state name -->\n      <div class=\"insurance\" *ngIf=\"isInsuranceSuccess\">\n        <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/success.png\" alt=\"insurance success\">\n        <h1 class=\"bj-modal-title\">Insurance Success</h1>\n      </div>\n    </div>\n  </div>\n\n</div>\n  ",
                    styles: [".bj-modal{max-width:305px;height:100%;max-height:200px;width:100%;background-color:#16334a;opacity:.95;border:2px solid transparent;border-radius:8px;letter-spacing:0;font-size:34px;color:#fff;text-align:center;position:absolute;top:50%;left:50%;bottom:50%;right:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);z-index:9999}.bj-modal-wrapper{height:100%;width:100%;position:relative}.bj-image{max-width:175px}.bj-modal.green{border-color:#02f292}.bj-modal.green .bj-modal-title{color:#02f292;margin-bottom:0}.bj-modal.green .points{color:#02f292;font-size:20px;font-weight:900}.bj-modal.blue{border-color:#67ddf2}.bj-modal.blue .bj-modal-title{color:#67ddf2}.bj-modal.yellow{border-color:#ffec77}.bj-modal.yellow .bj-modal-title{color:#ffec77}.bj-modal.red{border-color:#f10260}.bj-modal.red .bj-modal-title{color:#f10260}.small-circle{max-width:66px}.modal-close{font-size:25px;background-color:transparent;position:absolute;top:0;right:0;border:none;color:#fff}"]
                }] }
    ];
    BlackjackModalComponent.ctorParameters = function () { return []; };
    return BlackjackModalComponent;
}());
export { BlackjackModalComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackModalComponent.prototype._isClosed;
    /**
     * @type {?}
     * @private
     */
    BlackjackModalComponent.prototype.result;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2NvbXBvbmVudHMvYmxhY2tqYWNrLW1vZGFsL2JsYWNramFjay1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUxQyxPQUFPLEVBQUMsS0FBSyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFLckM7SUFTSTtRQUhRLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsV0FBTSxHQUFvQixTQUFTLENBQUM7SUFHNUMsQ0FBQzs7Ozs7SUFFSyw0Q0FBVTs7OztJQUFoQixVQUFpQixNQUF1Qjs7Ozs7d0JBQ3BDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO3dCQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzt3QkFFdkIscUJBQU0sS0FBSyxDQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsRUFBQTs7d0JBQW5CLFNBQW1CLENBQUM7d0JBQ3BCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7Ozs7S0FDaEI7Ozs7SUFFRCx1Q0FBSzs7O0lBQUw7UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBR0Qsc0JBQUksMkNBQU07Ozs7UUFBVjtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxTQUFTO2dCQUN2QixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDNUIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwwQ0FBSzs7OztRQUFUO1lBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQzVDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMkNBQU07Ozs7UUFBVjtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUM3QyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGdEQUFXOzs7O1FBQWY7WUFDSSxPQUFPLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFDbEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwyQ0FBTTs7OztRQUFWO1lBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzdDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksNkNBQVE7Ozs7UUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUMvQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHVEQUFrQjs7OztRQUF0QjtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDO1FBQ3pELENBQUM7OztPQUFBO0lBRUQsc0JBQUksOENBQVM7Ozs7UUFBYjtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUNoRCxDQUFDOzs7T0FBQTs7Z0JBeERKLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixxNURBQStDOztpQkFFaEQ7OztJQXFERCw4QkFBQztDQUFBLEFBekRELElBeURDO1NBcERZLHVCQUF1Qjs7Ozs7O0lBQ2hDLDRDQUEwQjs7Ozs7SUFDMUIseUNBQTRDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7c2xlZXB9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuaW1wb3J0IHsgSUJsYWNramFja1Jlc3VsdCB9IGZyb20gJy4uL21haW4vZ2FtZS9oYW5kLXJlc3VsdCc7XG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWJsYWNramFjay1tb2RhbCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9ibGFja2phY2stbW9kYWwuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9ibGFja2phY2stbW9kYWwuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBCbGFja2phY2tNb2RhbENvbXBvbmVudCAge1xuICAgIHByaXZhdGUgX2lzQ2xvc2VkID0gZmFsc2U7XG4gICAgcHJpdmF0ZSByZXN1bHQ6SUJsYWNramFja1Jlc3VsdCA9IHVuZGVmaW5lZDtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgIH1cblxuICAgIGFzeW5jIHNob3dSZXN1bHQocmVzdWx0OklCbGFja2phY2tSZXN1bHQpIHtcbiAgICAgICAgdGhpcy5yZXN1bHQgPSByZXN1bHQ7XG4gICAgICAgIHRoaXMuX2lzQ2xvc2VkID0gZmFsc2U7XG5cbiAgICAgICAgYXdhaXQgc2xlZXAoMioxMDAwKTtcbiAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgIH1cblxuICAgIGNsb3NlKCkge1xuICAgICAgICB0aGlzLl9pc0Nsb3NlZCA9IHRydWU7XG4gICAgfVxuXG5cbiAgICBnZXQgaXNPcGVuKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHQgIT0gdW5kZWZpbmVkICYmXG4gICAgICAgICAgICAgICAgIXRoaXMuX2lzQ2xvc2VkO1xuICAgIH1cblxuICAgIGdldCBpc1dpbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzdWx0ICYmIHRoaXMucmVzdWx0LmlzV2luO1xuICAgIH1cblxuICAgIGdldCBpc1B1c2goKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdCAmJiB0aGlzLnJlc3VsdC5pc1B1c2g7XG4gICAgfVxuXG4gICAgZ2V0IGlzQmxhY2tqYWNrKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHQgJiYgdGhpcy5yZXN1bHQuaXNCbGFja2phY2s7XG4gICAgfVxuXG4gICAgZ2V0IGlzTG9zcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzdWx0ICYmIHRoaXMucmVzdWx0LmlzTG9zcztcbiAgICB9XG5cbiAgICBnZXQgaXNCdXN0ZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdCAmJiB0aGlzLnJlc3VsdC5pc0J1c3RlZDtcbiAgICB9XG5cbiAgICBnZXQgaXNJbnN1cmFuY2VTdWNjZXNzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHQgJiYgdGhpcy5yZXN1bHQuaXNJbnN1cmFuY2VTdWNjZXNzO1xuICAgIH1cblxuICAgIGdldCBhbW91bnRXb24oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdCAmJiB0aGlzLnJlc3VsdC5hbW91bnRXb247XG4gICAgfVxufVxuIl19