/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-rules/blackjack-rules.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
@Component({
    selector: "blackjack-rules",
    templateUrl: "./blackjack-rules.component.html",
    styleUrls: ["./blackjack-rules.component.scss"]
})
*/
var /*
@Component({
    selector: "blackjack-rules",
    templateUrl: "./blackjack-rules.component.html",
    styleUrls: ["./blackjack-rules.component.scss"]
})
*/
BlackjackRulesComponentBase = /** @class */ (function () {
    function BlackjackRulesComponentBase(app, modal) {
        this.app = app;
        this.modal = modal;
    }
    Object.defineProperty(BlackjackRulesComponentBase.prototype, "translation", {
        get: /**
         * @return {?}
         */
        function () {
            return this.app.settings.translation;
        },
        enumerable: true,
        configurable: true
    });
    return BlackjackRulesComponentBase;
}());
/*
@Component({
    selector: "blackjack-rules",
    templateUrl: "./blackjack-rules.component.html",
    styleUrls: ["./blackjack-rules.component.scss"]
})
*/
export { BlackjackRulesComponentBase };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackRulesComponentBase.prototype.app;
    /** @type {?} */
    BlackjackRulesComponentBase.prototype.modal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLXJ1bGVzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2NvbXBvbmVudHMvYmxhY2tqYWNrLXJ1bGVzL2JsYWNramFjay1ydWxlcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBYUE7Ozs7Ozs7O0lBRUUscUNBQ1UsR0FBbUIsRUFDbEIsS0FBeUI7UUFEMUIsUUFBRyxHQUFILEdBQUcsQ0FBZ0I7UUFDbEIsVUFBSyxHQUFMLEtBQUssQ0FBb0I7SUFFcEMsQ0FBQztJQUVELHNCQUFJLG9EQUFXOzs7O1FBQWY7WUFDRSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztRQUN2QyxDQUFDOzs7T0FBQTtJQUNILGtDQUFDO0FBQUQsQ0FBQyxBQVhELElBV0M7Ozs7Ozs7Ozs7Ozs7O0lBUkcsMENBQTJCOztJQUMzQiw0Q0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5pbXBvcnQgeyBJTWFpbkFwcENvbnRleHQgfSBmcm9tICcuLi8uLi8uLi9fY29tbW9uL3NlcnZpY2VzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgT3ZlcmxheU1vZGFsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vc2VydmljZXMvb3ZlcmxheS1tb2RhbC5zZXJ2aWNlJztcbmltcG9ydCB7IElUcmFuc2xhdGlvbkJhc2UgfSBmcm9tICcuLi8uLi8uLi9fY29tbW9uL3RyYW5zbGF0aW9ucy9pbnRlcmZhY2VzJztcblxuLypcbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiBcImJsYWNramFjay1ydWxlc1wiLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vYmxhY2tqYWNrLXJ1bGVzLmNvbXBvbmVudC5odG1sXCIsXG4gICAgc3R5bGVVcmxzOiBbXCIuL2JsYWNramFjay1ydWxlcy5jb21wb25lbnQuc2Nzc1wiXVxufSlcbiovXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrUnVsZXNDb21wb25lbnRCYXNlXG57XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgYXBwOklNYWluQXBwQ29udGV4dCxcbiAgICByZWFkb25seSBtb2RhbDpPdmVybGF5TW9kYWxTZXJ2aWNlXG4gICkge1xuICB9XG5cbiAgZ2V0IHRyYW5zbGF0aW9uKCk6SVRyYW5zbGF0aW9uQmFzZSB7XG4gICAgcmV0dXJuIHRoaXMuYXBwLnNldHRpbmdzLnRyYW5zbGF0aW9uO1xuICB9XG59XG4iXX0=