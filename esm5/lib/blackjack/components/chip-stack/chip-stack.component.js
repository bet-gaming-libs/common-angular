/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/chip-stack/chip-stack.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Input } from '@angular/core';
/*
@Component({
    selector: 'bj-chip-stack',
    templateUrl: './chip-stack.component.html',
    styleUrls: ['./chip-stack.component.scss']
})
*/
var BlackJackChipStackComponentBase = /** @class */ (function () {
    function BlackJackChipStackComponentBase(app) {
        this.app = app;
        this.isMax = false;
        this.amount = 0;
        this.chipstack = [];
    }
    /**
     * @return {?}
     */
    BlackJackChipStackComponentBase.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    BlackJackChipStackComponentBase.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        this.makeChipStack();
    };
    /**
     * @return {?}
     */
    BlackJackChipStackComponentBase.prototype.makeChipStack = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var chipstack = [];
        // is the amount the max for this bet type
        if (this.isMax) {
            chipstack.push(this.chips.length);
        }
        else {
            /** @type {?} */
            var factor = 1 / this.chips[0];
            /** @type {?} */
            var n = Math.round(this.amount * factor);
            for (var i = this.chips.length - 1; i >= 0; i--) {
                if ((this.chips[i] * factor) <= n) {
                    /** @type {?} */
                    var r = Math.floor(n / (this.chips[i] * factor));
                    n -= this.chips[i] * factor * r;
                    for (var j = 0; j < r; j++) {
                        chipstack.push(i);
                    }
                }
            }
        }
        this.chipstack = chipstack;
    };
    Object.defineProperty(BlackJackChipStackComponentBase.prototype, "chips", {
        get: /**
         * @return {?}
         */
        function () {
            return this.settings.selectedToken.chips;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackJackChipStackComponentBase.prototype, "settings", {
        get: /**
         * @return {?}
         */
        function () {
            return this.app.settings;
        },
        enumerable: true,
        configurable: true
    });
    BlackJackChipStackComponentBase.propDecorators = {
        isMax: [{ type: Input }],
        amount: [{ type: Input }]
    };
    return BlackJackChipStackComponentBase;
}());
export { BlackJackChipStackComponentBase };
if (false) {
    /** @type {?} */
    BlackJackChipStackComponentBase.prototype.isMax;
    /** @type {?} */
    BlackJackChipStackComponentBase.prototype.amount;
    /** @type {?} */
    BlackJackChipStackComponentBase.prototype.chipstack;
    /**
     * @type {?}
     * @private
     */
    BlackJackChipStackComponentBase.prototype.app;
}
/**
 * @record
 */
export function IChipsInfo() { }
if (false) {
    /** @type {?} */
    IChipsInfo.prototype.isMax;
    /** @type {?} */
    IChipsInfo.prototype.amount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpcC1zdGFjay5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL2JsYWNramFjay9jb21wb25lbnRzL2NoaXAtc3RhY2svY2hpcC1zdGFjay5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQWEsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQzs7Ozs7Ozs7QUFZcEU7SUFVSSx5Q0FBb0IsR0FBbUI7UUFBbkIsUUFBRyxHQUFILEdBQUcsQ0FBZ0I7UUFQdkMsVUFBSyxHQUFHLEtBQUssQ0FBQztRQUVkLFdBQU0sR0FBRyxDQUFDLENBQUM7UUFHWCxjQUFTLEdBQUcsRUFBRSxDQUFDO0lBR2YsQ0FBQzs7OztJQUVELGtEQUFROzs7SUFBUjtJQUNBLENBQUM7Ozs7SUFDRCxxREFBVzs7O0lBQVg7UUFDSSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELHVEQUFhOzs7SUFBYjs7WUFDUSxTQUFTLEdBQUcsRUFBRTtRQUVsQiwwQ0FBMEM7UUFDMUMsSUFBRyxJQUFJLENBQUMsS0FBSyxFQUFDO1lBQ1YsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3JDO2FBQU07O2dCQUNHLE1BQU0sR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7O2dCQUU1QixDQUFDLEdBQVUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFDLE1BQU0sQ0FBQztZQUM3QyxLQUFJLElBQUksQ0FBQyxHQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFDLENBQUMsRUFBRSxDQUFDLElBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFDO2dCQUM1QyxJQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBQyxNQUFNLENBQUMsSUFBRSxDQUFDLEVBQUM7O3dCQUNyQixDQUFDLEdBQVUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNuRCxDQUFDLElBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDO29CQUMxQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUN4QixTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNyQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztJQUMvQixDQUFDO0lBRUQsc0JBQUksa0RBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDO1FBQzdDLENBQUM7OztPQUFBO0lBRUQsc0JBQUkscURBQVE7Ozs7UUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUM7UUFDN0IsQ0FBQzs7O09BQUE7O3dCQTlDQSxLQUFLO3lCQUVMLEtBQUs7O0lBNkNWLHNDQUFDO0NBQUEsQUFqREQsSUFpREM7U0FqRFksK0JBQStCOzs7SUFFeEMsZ0RBQ2M7O0lBQ2QsaURBQ1c7O0lBR1gsb0RBQWU7Ozs7O0lBRUgsOENBQTJCOzs7OztBQTBDM0MsZ0NBSUM7OztJQUZHLDJCQUFjOztJQUNkLDRCQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgSU1haW5BcHBDb250ZXh0IH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElBcHBTZXR0aW5ncyB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vbW9kZWxzL2ludGVyZmFjZXMnO1xuXG4vKlxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdiai1jaGlwLXN0YWNrJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2hpcC1zdGFjay5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY2hpcC1zdGFjay5jb21wb25lbnQuc2NzcyddXG59KVxuKi9cbmV4cG9ydCBjbGFzcyBCbGFja0phY2tDaGlwU3RhY2tDb21wb25lbnRCYXNlIGltcGxlbWVudHMgT25DaGFuZ2VzLCBPbkluaXRcbntcbiAgICBASW5wdXQoKVxuICAgIGlzTWF4ID0gZmFsc2U7XG4gICAgQElucHV0KClcbiAgICBhbW91bnQgPSAwO1xuXG5cbiAgICBjaGlwc3RhY2sgPSBbXTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBwOklNYWluQXBwQ29udGV4dCkge1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgIH1cbiAgICBuZ09uQ2hhbmdlcygpIHtcbiAgICAgICAgdGhpcy5tYWtlQ2hpcFN0YWNrKCk7XG4gICAgfVxuXG4gICAgbWFrZUNoaXBTdGFjaygpe1xuICAgICAgICBsZXQgY2hpcHN0YWNrID0gW107XG5cbiAgICAgICAgLy8gaXMgdGhlIGFtb3VudCB0aGUgbWF4IGZvciB0aGlzIGJldCB0eXBlXG4gICAgICAgIGlmKHRoaXMuaXNNYXgpe1xuICAgICAgICAgICAgY2hpcHN0YWNrLnB1c2godGhpcy5jaGlwcy5sZW5ndGgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc3QgZmFjdG9yID0gMSAvIHRoaXMuY2hpcHNbMF07XG5cbiAgICAgICAgICAgIGxldCBuOm51bWJlciA9IE1hdGgucm91bmQodGhpcy5hbW91bnQqZmFjdG9yKTtcbiAgICAgICAgICAgIGZvcihsZXQgaTpudW1iZXI9dGhpcy5jaGlwcy5sZW5ndGgtMTsgaT49MDsgaS0tKXtcbiAgICAgICAgICAgICAgICBpZigodGhpcy5jaGlwc1tpXSpmYWN0b3IpPD1uKXtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHI6bnVtYmVyID0gTWF0aC5mbG9vcihuLyh0aGlzLmNoaXBzW2ldKmZhY3RvcikpO1xuICAgICAgICAgICAgICAgICAgICBuLT10aGlzLmNoaXBzW2ldKmZhY3RvcipyO1xuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IHI7IGorKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2hpcHN0YWNrLnB1c2goaSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jaGlwc3RhY2sgPSBjaGlwc3RhY2s7XG4gICAgfVxuXG4gICAgZ2V0IGNoaXBzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5zZWxlY3RlZFRva2VuLmNoaXBzO1xuICAgIH1cblxuICAgIGdldCBzZXR0aW5ncygpOklBcHBTZXR0aW5ncyB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC5zZXR0aW5ncztcbiAgICB9XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQ2hpcHNJbmZvXG57XG4gICAgaXNNYXg6Ym9vbGVhbjtcbiAgICBhbW91bnQ6bnVtYmVyO1xufVxuIl19