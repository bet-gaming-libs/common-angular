/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/blackjack.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ViewChild } from '@angular/core';
import { sleep } from 'earnbet-common';
import { BlackjackGameController } from './game/blackjack-controller';
import { BlackjackModalComponent } from '../blackjack-modal/blackjack-modal.component';
import { LocalBlackjackServer } from '../../server/local-server';
import { RemoteBlackjackServerBridge } from '../../server/remote-server';
import { GameComponentBase } from '../../../_common/base/game-component-base';
/*
@Component({
    selector: 'app-blackjack',
    templateUrl: './blackjack.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./blackjack.component.scss']
})
*/
var BlackjackComponentBase = /** @class */ (function (_super) {
    tslib_1.__extends(BlackjackComponentBase, _super);
    function BlackjackComponentBase(app, renderer, modal, googleAnalytics, soundPlayer, accountNames) {
        var _this = _super.call(this, app, googleAnalytics, soundPlayer) || this;
        _this.renderer = renderer;
        _this.modal = modal;
        // *** For Testing: ***
        //this.useLocalServer();
        _this.useRemoteServer(accountNames);
        _this.game = new BlackjackGameController(_this, _this.server, soundPlayer);
        _this.setListeners();
        return _this;
    }
    /**
     * @private
     * @param {?} soundPlayer
     * @return {?}
     */
    BlackjackComponentBase.prototype.useLocalServer = /**
     * @private
     * @param {?} soundPlayer
     * @return {?}
     */
    function (soundPlayer) {
        this.server = new LocalBlackjackServer(this, soundPlayer);
    };
    /**
     * @private
     * @param {?} acountNames
     * @return {?}
     */
    BlackjackComponentBase.prototype.useRemoteServer = /**
     * @private
     * @param {?} acountNames
     * @return {?}
     */
    function (acountNames) {
        this.server = new RemoteBlackjackServerBridge(this, acountNames, this.app);
    };
    /**
     * @private
     * @return {?}
     */
    BlackjackComponentBase.prototype.setListeners = /**
     * @private
     * @return {?}
     */
    function () {
        this.app.state.setListener(this);
        this.app.blackjack.client.setListener(this);
        this.app.state.bets.setListener(this);
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.onBlackjackClientConnected = /**
     * @return {?}
     */
    function () {
        this.resumeGame();
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.onAuthenticated = /**
     * @return {?}
     */
    function () {
        this.resumeGame();
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        this.renderer.addClass(document.body, 'blackjack-bg');
        this.resumeGame();
        // *** blackjack deposit modal ***
        //this.modal.open('blackjackGameNoticeModal');
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.renderer.removeClass(document.body, 'blackjack-bg');
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.debug = /**
     * @return {?}
     */
    function () {
        console.log(this.game);
    };
    /**
     * @private
     * @return {?}
     */
    BlackjackComponentBase.prototype.resumeGame = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.app.blackjack.client.isConnected) {
            this.server.resumeExistingGame();
        }
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.onBettingCurrencyChanged = /**
     * @return {?}
     */
    function () {
        this.clearTable();
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.onChipSelected = /**
     * @return {?}
     */
    function () {
        this.addChip();
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.halfOfBetAmount = /**
     * @return {?}
     */
    function () {
        if (this.isBetPlaced) {
            return;
        }
        /** @type {?} */
        var minimumBet = Number(this.selectedToken.minimumBetAmount);
        if (this.game.bet.amount == minimumBet) {
            return;
        }
        this.game.bet.half();
        if (this.game.bet.amount < minimumBet) {
            this.game.bet.amount = minimumBet;
        }
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.doubleBetAmount = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var maxBet;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.isBetPlaced) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.getMaxBet()];
                    case 1:
                        maxBet = _a.sent();
                        if (this.game.bet.amount == maxBet) {
                            return [2 /*return*/];
                        }
                        this.game.bet.double();
                        if (this.game.bet.amount > maxBet) {
                            this.game.bet.setToMax(maxBet);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.addChip = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var amount, _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (this.isBetPlaced) {
                            return [2 /*return*/];
                        }
                        if (!!this.isMaxChipSelected) return [3 /*break*/, 1];
                        amount = this.chips[this.selectedChip];
                        this.game.bet.add(amount);
                        return [3 /*break*/, 3];
                    case 1:
                        _b = (_a = this.game.bet).setToMax;
                        return [4 /*yield*/, this.getMaxBet()];
                    case 2:
                        _b.apply(_a, [_c.sent()]);
                        _c.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.removeChip = /**
     * @return {?}
     */
    function () {
        if (this.isBetPlaced) {
            return;
        }
        if (!this.isMaxChipSelected) {
            /** @type {?} */
            var amount = this.chips[this.selectedChip];
            this.game.bet.subtract(amount);
        }
        else {
            this.game.bet.reset();
        }
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.clearTable = /**
     * @return {?}
     */
    function () {
        if (!this.isBetPlaced) {
            this.game.bet.reset();
        }
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.placeBet = /**
     * @return {?}
     */
    function () {
        if (this.game.isStarted) {
            return;
        }
        this.alert.clear();
        if (this.game.bet.amount == 0) {
            this.alert.error('Place Chips on Table to Place Your Bet!');
            return;
        }
        // *** BROADCAST BET TRANSACTION TO BLOCK CHAIN ***
        this.server.placeBet(this.betAmount);
    };
    /**
     * @param {?} bet
     * @return {?}
     */
    BlackjackComponentBase.prototype.onMyBetResolved = /**
     * @param {?} bet
     * @return {?}
     */
    function (bet) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, sleep(2000)];
                    case 1:
                        _c.sent();
                        return [4 /*yield*/, this.updateBalances(bet)];
                    case 2:
                        _c.sent();
                        if (!(!this.isBetPlaced &&
                            this.game.bet.isMax)) return [3 /*break*/, 4];
                        _b = (_a = this.game.bet).setToMax;
                        return [4 /*yield*/, this.getMaxBet()];
                    case 3:
                        _b.apply(_a, [_c.sent()]);
                        _c.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackComponentBase.prototype.getMaxBet = /**
     * @return {?}
     */
    function () {
        return this.bankroll.getMaxBet(4);
    };
    Object.defineProperty(BlackjackComponentBase.prototype, "betAmount", {
        get: /**
         * @return {?}
         */
        function () {
            return this.selectedToken.getAssetAmount(this.game.bet.amount.toString());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackComponentBase.prototype, "isSplit", {
        get: /**
         * @return {?}
         */
        function () {
            return this.game.isSplit;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackComponentBase.prototype, "isBetPlaced", {
        get: /**
         * @return {?}
         */
        function () {
            return this.game.isBetPlaced;
        },
        enumerable: true,
        configurable: true
    });
    BlackjackComponentBase.propDecorators = {
        gameResultModal: [{ type: ViewChild, args: ['gameResultModal',] }]
    };
    return BlackjackComponentBase;
}(GameComponentBase));
export { BlackjackComponentBase };
if (false) {
    /** @type {?} */
    BlackjackComponentBase.prototype.gameResultModal;
    /**
     * @type {?}
     * @private
     */
    BlackjackComponentBase.prototype.server;
    /** @type {?} */
    BlackjackComponentBase.prototype.game;
    /**
     * @type {?}
     * @private
     */
    BlackjackComponentBase.prototype.renderer;
    /** @type {?} */
    BlackjackComponentBase.prototype.modal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2NvbXBvbmVudHMvbWFpbi9ibGFja2phY2suY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBMkMsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRW5GLE9BQU8sRUFBRSxLQUFLLEVBQWlCLE1BQU0sZ0JBQWdCLENBQUM7QUFFdEQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDdkYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFHakUsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFekUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7Ozs7Ozs7OztBQWtCOUU7SUFBNEMsa0RBQWlCO0lBVXpELGdDQUNJLEdBQW1CLEVBQ1gsUUFBbUIsRUFDbEIsS0FBMEIsRUFDbkMsZUFBdUMsRUFDdkMsV0FBd0IsRUFDeEIsWUFBNkI7UUFOakMsWUFRSSxrQkFBTSxHQUFHLEVBQUMsZUFBZSxFQUFDLFdBQVcsQ0FBQyxTQVd6QztRQWpCVyxjQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ2xCLFdBQUssR0FBTCxLQUFLLENBQXFCO1FBUW5DLHVCQUF1QjtRQUN2Qix3QkFBd0I7UUFDeEIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUduQyxLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksdUJBQXVCLENBQUMsS0FBSSxFQUFDLEtBQUksQ0FBQyxNQUFNLEVBQUMsV0FBVyxDQUFDLENBQUM7UUFFdEUsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDOztJQUN4QixDQUFDOzs7Ozs7SUFDTywrQ0FBYzs7Ozs7SUFBdEIsVUFBdUIsV0FBd0I7UUFDM0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLG9CQUFvQixDQUFDLElBQUksRUFBQyxXQUFXLENBQUMsQ0FBQztJQUM3RCxDQUFDOzs7Ozs7SUFDTyxnREFBZTs7Ozs7SUFBdkIsVUFBd0IsV0FBNEI7UUFDaEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLDJCQUEyQixDQUN6QyxJQUFJLEVBQ0osV0FBVyxFQUNYLElBQUksQ0FBQyxHQUFHLENBQ1gsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRU8sNkNBQVk7Ozs7SUFBcEI7UUFDSSxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7SUFFRCwyREFBMEI7OztJQUExQjtRQUNJLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsZ0RBQWU7OztJQUFmO1FBQ0ksSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7SUFFRCxnREFBZTs7O0lBQWY7UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBRXRELElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUdsQixrQ0FBa0M7UUFDbEMsOENBQThDO0lBQ2xELENBQUM7Ozs7SUFFRCw0Q0FBVzs7O0lBQVg7UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7SUFHRCxzQ0FBSzs7O0lBQUw7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVPLDJDQUFVOzs7O0lBQWxCO1FBQ0ksSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxNQUFNLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUNwQztJQUNMLENBQUM7Ozs7SUFHRCx5REFBd0I7OztJQUF4QjtRQUNJLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsK0NBQWM7OztJQUFkO1FBQ0ksSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ25CLENBQUM7Ozs7SUFFRCxnREFBZTs7O0lBQWY7UUFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbEIsT0FBTztTQUNWOztZQUdLLFVBQVUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUU5RCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxVQUFVLEVBQUU7WUFDcEMsT0FBTztTQUNWO1FBR0QsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFckIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsVUFBVSxFQUFFO1lBQ25DLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7U0FDckM7SUFDTCxDQUFDOzs7O0lBRUssZ0RBQWU7OztJQUFyQjs7Ozs7O3dCQUNJLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTs0QkFDbEIsc0JBQU87eUJBQ1Y7d0JBR2MscUJBQU0sSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFBL0IsTUFBTSxHQUFHLFNBQXNCO3dCQUVyQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxNQUFNLEVBQUU7NEJBQ2hDLHNCQUFPO3lCQUNWO3dCQUdELElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUV2QixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxNQUFNLEVBQUU7NEJBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQzt5QkFDbEM7Ozs7O0tBQ0o7Ozs7SUFFSyx3Q0FBTzs7O0lBQWI7Ozs7Ozt3QkFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7NEJBQ2xCLHNCQUFPO3lCQUNWOzZCQUdHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUF2Qix3QkFBdUI7d0JBQ2pCLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7d0JBRTVDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQzs7O3dCQUUxQixLQUFBLENBQUEsS0FBQSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQSxDQUFDLFFBQVEsQ0FBQTt3QkFDbEIscUJBQU0sSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFEMUIsY0FDSSxTQUFzQixFQUN6QixDQUFDOzs7Ozs7S0FFVDs7OztJQUVELDJDQUFVOzs7SUFBVjtRQUNJLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixPQUFPO1NBQ1Y7UUFHRCxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFOztnQkFDbkIsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUU1QyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDbEM7YUFBTTtZQUNILElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQzs7OztJQUVELDJDQUFVOzs7SUFBVjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQzs7OztJQUlELHlDQUFROzs7SUFBUjtRQUNJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDckIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUVuQixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQ1oseUNBQXlDLENBQzVDLENBQUM7WUFDRixPQUFPO1NBQ1Y7UUFHRCxtREFBbUQ7UUFDbkQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQ2hCLElBQUksQ0FBQyxTQUFTLENBQ2pCLENBQUM7SUFDTixDQUFDOzs7OztJQUVLLGdEQUFlOzs7O0lBQXJCLFVBQXNCLEdBQXNCOzs7Ozs0QkFDeEMscUJBQU0sS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFBOzt3QkFBakIsU0FBaUIsQ0FBQzt3QkFFbEIscUJBQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBQTs7d0JBQTlCLFNBQThCLENBQUM7NkJBSTNCLENBQUEsQ0FBQyxJQUFJLENBQUMsV0FBVzs0QkFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFBLEVBRG5CLHdCQUNtQjt3QkFFbkIsS0FBQSxDQUFBLEtBQUEsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUEsQ0FBQyxRQUFRLENBQUE7d0JBQ2xCLHFCQUFNLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBRDFCLGNBQ0ksU0FBc0IsRUFDekIsQ0FBQzs7Ozs7O0tBRVQ7Ozs7SUFHRCwwQ0FBUzs7O0lBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFHRCxzQkFBSSw2Q0FBUzs7OztRQUFiO1lBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUNsQyxDQUFDO1FBQ04sQ0FBQzs7O09BQUE7SUFHRCxzQkFBSSwyQ0FBTzs7OztRQUFYO1lBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUM3QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLCtDQUFXOzs7O1FBQWY7WUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ2pDLENBQUM7OztPQUFBOztrQ0E5TkEsU0FBUyxTQUFDLGlCQUFpQjs7SUErTmhDLDZCQUFDO0NBQUEsQUFsT0QsQ0FBNEMsaUJBQWlCLEdBa081RDtTQWxPWSxzQkFBc0I7OztJQUcvQixpREFDd0M7Ozs7O0lBR3hDLHdDQUFnQzs7SUFDaEMsc0NBQXNDOzs7OztJQUlsQywwQ0FBMkI7O0lBQzNCLHVDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgUmVuZGVyZXIyLCBWaWV3RW5jYXBzdWxhdGlvbiwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IHNsZWVwLEVPU0Fzc2V0QW1vdW50IH0gZnJvbSAnZWFybmJldC1jb21tb24nO1xuXG5pbXBvcnQgeyBCbGFja2phY2tHYW1lQ29udHJvbGxlciB9IGZyb20gJy4vZ2FtZS9ibGFja2phY2stY29udHJvbGxlcic7XG5pbXBvcnQgeyBCbGFja2phY2tNb2RhbENvbXBvbmVudCB9IGZyb20gJy4uL2JsYWNramFjay1tb2RhbC9ibGFja2phY2stbW9kYWwuY29tcG9uZW50JztcbmltcG9ydCB7IExvY2FsQmxhY2tqYWNrU2VydmVyIH0gZnJvbSAnLi4vLi4vc2VydmVyL2xvY2FsLXNlcnZlcic7XG5pbXBvcnQgeyBJQmxhY2tqYWNrU2VydmVyIH0gZnJvbSAnLi4vLi4vc2VydmVyL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSUJsYWNramFja0NvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBSZW1vdGVCbGFja2phY2tTZXJ2ZXJCcmlkZ2UgfSBmcm9tICcuLi8uLi9zZXJ2ZXIvcmVtb3RlLXNlcnZlcic7XG5pbXBvcnQgeyBJQmxhY2tqYWNrQ2xpZW50TGlzdGVuZXIgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IEdhbWVDb21wb25lbnRCYXNlIH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9iYXNlL2dhbWUtY29tcG9uZW50LWJhc2UnO1xuaW1wb3J0IHsgSUJldHNMaXN0ZW5lciB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vbW9kZWxzL2JldHMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQXBwU3RhdGVMaXN0ZW5lciB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vbW9kZWxzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSU1haW5BcHBDb250ZXh0LCBJR29vZ2xlQW5hbHl0aWNzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vc2VydmljZXMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBPdmVybGF5TW9kYWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9vdmVybGF5LW1vZGFsLnNlcnZpY2UnO1xuaW1wb3J0IHsgQmxhY2tqYWNrQmV0UmVzdWx0IH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9tb2RlbHMvYmV0cy9ibGFja2phY2stYmV0JztcbmltcG9ydCB7IElTb3VuZFBsYXllciB9IGZyb20gJy4uLy4uLy4uL19jb21tb24vbWVkaWEvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJRW9zQWNjb3VudE5hbWVzIH0gZnJvbSAnLi4vLi4vLi4vX2NvbW1vbi9tb2RlbHMvYnVzaW5lc3MtcnVsZXMvaW50ZXJmYWNlcyc7XG5cblxuLypcbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYXBwLWJsYWNramFjaycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2JsYWNramFjay5jb21wb25lbnQuaHRtbCcsXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgICBzdHlsZVVybHM6IFsnLi9ibGFja2phY2suY29tcG9uZW50LnNjc3MnXVxufSlcbiovXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrQ29tcG9uZW50QmFzZSBleHRlbmRzIEdhbWVDb21wb25lbnRCYXNlXG4gICAgaW1wbGVtZW50cyBJQmxhY2tqYWNrQ29tcG9uZW50LElCZXRzTGlzdGVuZXIsSUFwcFN0YXRlTGlzdGVuZXIsSUJsYWNramFja0NsaWVudExpc3RlbmVyXG57XG4gICAgQFZpZXdDaGlsZCgnZ2FtZVJlc3VsdE1vZGFsJylcbiAgICBnYW1lUmVzdWx0TW9kYWw6QmxhY2tqYWNrTW9kYWxDb21wb25lbnQ7XG5cblxuICAgIHByaXZhdGUgc2VydmVyOklCbGFja2phY2tTZXJ2ZXI7XG4gICAgcmVhZG9ubHkgZ2FtZTpCbGFja2phY2tHYW1lQ29udHJvbGxlcjtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBhcHA6SU1haW5BcHBDb250ZXh0LFxuICAgICAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsXG4gICAgICAgIHJlYWRvbmx5IG1vZGFsOiBPdmVybGF5TW9kYWxTZXJ2aWNlLFxuICAgICAgICBnb29nbGVBbmFseXRpY3M6SUdvb2dsZUFuYWx5dGljc1NlcnZpY2UsXG4gICAgICAgIHNvdW5kUGxheWVyOklTb3VuZFBsYXllcixcbiAgICAgICAgYWNjb3VudE5hbWVzOklFb3NBY2NvdW50TmFtZXNcbiAgICApIHtcbiAgICAgICAgc3VwZXIoYXBwLGdvb2dsZUFuYWx5dGljcyxzb3VuZFBsYXllcik7XG5cblxuICAgICAgICAvLyAqKiogRm9yIFRlc3Rpbmc6ICoqKlxuICAgICAgICAvL3RoaXMudXNlTG9jYWxTZXJ2ZXIoKTtcbiAgICAgICAgdGhpcy51c2VSZW1vdGVTZXJ2ZXIoYWNjb3VudE5hbWVzKTtcblxuXG4gICAgICAgIHRoaXMuZ2FtZSA9IG5ldyBCbGFja2phY2tHYW1lQ29udHJvbGxlcih0aGlzLHRoaXMuc2VydmVyLHNvdW5kUGxheWVyKTtcblxuICAgICAgICB0aGlzLnNldExpc3RlbmVycygpO1xuICAgIH1cbiAgICBwcml2YXRlIHVzZUxvY2FsU2VydmVyKHNvdW5kUGxheWVyOklTb3VuZFBsYXllcikge1xuICAgICAgICB0aGlzLnNlcnZlciA9IG5ldyBMb2NhbEJsYWNramFja1NlcnZlcih0aGlzLHNvdW5kUGxheWVyKTtcbiAgICB9XG4gICAgcHJpdmF0ZSB1c2VSZW1vdGVTZXJ2ZXIoYWNvdW50TmFtZXM6SUVvc0FjY291bnROYW1lcykge1xuICAgICAgICB0aGlzLnNlcnZlciA9IG5ldyBSZW1vdGVCbGFja2phY2tTZXJ2ZXJCcmlkZ2UoXG4gICAgICAgICAgICB0aGlzLFxuICAgICAgICAgICAgYWNvdW50TmFtZXMsXG4gICAgICAgICAgICB0aGlzLmFwcFxuICAgICAgICApO1xuICAgIH1cblxuICAgIHByaXZhdGUgc2V0TGlzdGVuZXJzKCkge1xuICAgICAgICB0aGlzLmFwcC5zdGF0ZS5zZXRMaXN0ZW5lcih0aGlzKTtcbiAgICAgICAgdGhpcy5hcHAuYmxhY2tqYWNrLmNsaWVudC5zZXRMaXN0ZW5lcih0aGlzKTtcbiAgICAgICAgdGhpcy5hcHAuc3RhdGUuYmV0cy5zZXRMaXN0ZW5lcih0aGlzKTtcbiAgICB9XG5cbiAgICBvbkJsYWNramFja0NsaWVudENvbm5lY3RlZCgpIHtcbiAgICAgICAgdGhpcy5yZXN1bWVHYW1lKCk7XG4gICAgfVxuXG4gICAgb25BdXRoZW50aWNhdGVkKCkge1xuICAgICAgICB0aGlzLnJlc3VtZUdhbWUoKTtcbiAgICB9XG5cbiAgICBuZ0FmdGVyVmlld0luaXQgKCkge1xuICAgICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKGRvY3VtZW50LmJvZHksICdibGFja2phY2stYmcnKTtcblxuICAgICAgICB0aGlzLnJlc3VtZUdhbWUoKTtcblxuXG4gICAgICAgIC8vICoqKiBibGFja2phY2sgZGVwb3NpdCBtb2RhbCAqKipcbiAgICAgICAgLy90aGlzLm1vZGFsLm9wZW4oJ2JsYWNramFja0dhbWVOb3RpY2VNb2RhbCcpO1xuICAgIH1cblxuICAgIG5nT25EZXN0cm95ICgpIHtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5yZW1vdmVDbGFzcyhkb2N1bWVudC5ib2R5LCAnYmxhY2tqYWNrLWJnJyk7XG4gICAgfVxuXG5cbiAgICBkZWJ1ZygpIHtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5nYW1lKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJlc3VtZUdhbWUoKSB7XG4gICAgICAgIGlmICh0aGlzLmFwcC5ibGFja2phY2suY2xpZW50LmlzQ29ubmVjdGVkKSB7XG4gICAgICAgICAgICB0aGlzLnNlcnZlci5yZXN1bWVFeGlzdGluZ0dhbWUoKTtcbiAgICAgICAgfVxuICAgIH1cblxuXG4gICAgb25CZXR0aW5nQ3VycmVuY3lDaGFuZ2VkKCkge1xuICAgICAgICB0aGlzLmNsZWFyVGFibGUoKTtcbiAgICB9XG5cbiAgICBvbkNoaXBTZWxlY3RlZCgpIHtcbiAgICAgICAgdGhpcy5hZGRDaGlwKCk7XG4gICAgfVxuXG4gICAgaGFsZk9mQmV0QW1vdW50KCkge1xuICAgICAgICBpZiAodGhpcy5pc0JldFBsYWNlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCBtaW5pbXVtQmV0ID0gTnVtYmVyKHRoaXMuc2VsZWN0ZWRUb2tlbi5taW5pbXVtQmV0QW1vdW50KTtcblxuICAgICAgICBpZiAodGhpcy5nYW1lLmJldC5hbW91bnQgPT0gbWluaW11bUJldCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICB0aGlzLmdhbWUuYmV0LmhhbGYoKTtcblxuICAgICAgICBpZiAodGhpcy5nYW1lLmJldC5hbW91bnQgPCBtaW5pbXVtQmV0KSB7XG4gICAgICAgICAgICB0aGlzLmdhbWUuYmV0LmFtb3VudCA9IG1pbmltdW1CZXQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhc3luYyBkb3VibGVCZXRBbW91bnQoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzQmV0UGxhY2VkKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IG1heEJldCA9IGF3YWl0IHRoaXMuZ2V0TWF4QmV0KCk7XG5cbiAgICAgICAgaWYgKHRoaXMuZ2FtZS5iZXQuYW1vdW50ID09IG1heEJldCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICB0aGlzLmdhbWUuYmV0LmRvdWJsZSgpO1xuXG4gICAgICAgIGlmICh0aGlzLmdhbWUuYmV0LmFtb3VudCA+IG1heEJldCkge1xuICAgICAgICAgICAgdGhpcy5nYW1lLmJldC5zZXRUb01heChtYXhCZXQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYXN5bmMgYWRkQ2hpcCgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNCZXRQbGFjZWQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgaWYgKCF0aGlzLmlzTWF4Q2hpcFNlbGVjdGVkKSB7XG4gICAgICAgICAgICBjb25zdCBhbW91bnQgPSB0aGlzLmNoaXBzW3RoaXMuc2VsZWN0ZWRDaGlwXTtcblxuICAgICAgICAgICAgdGhpcy5nYW1lLmJldC5hZGQoYW1vdW50KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQuc2V0VG9NYXgoXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5nZXRNYXhCZXQoKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJlbW92ZUNoaXAoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzQmV0UGxhY2VkKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuXG4gICAgICAgIGlmICghdGhpcy5pc01heENoaXBTZWxlY3RlZCkge1xuICAgICAgICAgICAgY29uc3QgYW1vdW50ID0gdGhpcy5jaGlwc1t0aGlzLnNlbGVjdGVkQ2hpcF07XG5cbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQuc3VidHJhY3QoYW1vdW50KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQucmVzZXQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGNsZWFyVGFibGUoKSB7XG4gICAgICAgIGlmICghdGhpcy5pc0JldFBsYWNlZCkge1xuICAgICAgICAgICAgdGhpcy5nYW1lLmJldC5yZXNldCgpO1xuICAgICAgICB9XG4gICAgfVxuXG5cblxuICAgIHBsYWNlQmV0KCkge1xuICAgICAgICBpZiAodGhpcy5nYW1lLmlzU3RhcnRlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5hbGVydC5jbGVhcigpO1xuXG4gICAgICAgIGlmICh0aGlzLmdhbWUuYmV0LmFtb3VudCA9PSAwKSB7XG4gICAgICAgICAgICB0aGlzLmFsZXJ0LmVycm9yKFxuICAgICAgICAgICAgICAgICdQbGFjZSBDaGlwcyBvbiBUYWJsZSB0byBQbGFjZSBZb3VyIEJldCEnXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICAvLyAqKiogQlJPQURDQVNUIEJFVCBUUkFOU0FDVElPTiBUTyBCTE9DSyBDSEFJTiAqKipcbiAgICAgICAgdGhpcy5zZXJ2ZXIucGxhY2VCZXQoXG4gICAgICAgICAgICB0aGlzLmJldEFtb3VudFxuICAgICAgICApO1xuICAgIH1cblxuICAgIGFzeW5jIG9uTXlCZXRSZXNvbHZlZChiZXQ6QmxhY2tqYWNrQmV0UmVzdWx0KSB7XG4gICAgICAgIGF3YWl0IHNsZWVwKDIwMDApO1xuXG4gICAgICAgIGF3YWl0IHRoaXMudXBkYXRlQmFsYW5jZXMoYmV0KTtcblxuICAgICAgICAvLyAqKiogcmVjYWxjdWxhdGUgbWF4IGJldCAqKipcbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgIXRoaXMuaXNCZXRQbGFjZWQgJiZcbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQuaXNNYXhcbiAgICAgICAgKSB7XG4gICAgICAgICAgICB0aGlzLmdhbWUuYmV0LnNldFRvTWF4KFxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuZ2V0TWF4QmV0KClcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIGdldE1heEJldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYmFua3JvbGwuZ2V0TWF4QmV0KDQpO1xuICAgIH1cblxuXG4gICAgZ2V0IGJldEFtb3VudCgpOkVPU0Fzc2V0QW1vdW50IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRUb2tlbi5nZXRBc3NldEFtb3VudChcbiAgICAgICAgICAgIHRoaXMuZ2FtZS5iZXQuYW1vdW50LnRvU3RyaW5nKClcbiAgICAgICAgKTtcbiAgICB9XG5cblxuICAgIGdldCBpc1NwbGl0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5nYW1lLmlzU3BsaXQ7XG4gICAgfVxuXG4gICAgZ2V0IGlzQmV0UGxhY2VkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5nYW1lLmlzQmV0UGxhY2VkO1xuICAgIH1cbn0iXX0=