/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/bet-amount.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var BetAmount = /** @class */ (function () {
    function BetAmount() {
        this._isMax = false;
        this._amount = 0;
    }
    /**
     * @return {?}
     */
    BetAmount.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.amount = 0;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    BetAmount.prototype.add = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.amount += value;
    };
    /**
     * @return {?}
     */
    BetAmount.prototype.double = /**
     * @return {?}
     */
    function () {
        this.amount *= 2;
    };
    /**
     * @return {?}
     */
    BetAmount.prototype.half = /**
     * @return {?}
     */
    function () {
        this.amount *= 0.5;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    BetAmount.prototype.subtract = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.amount -= value;
        if (this.amount < 0) {
            this.amount = 0;
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    BetAmount.prototype.setToMax = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.amount = value;
        this._isMax = true;
    };
    Object.defineProperty(BetAmount.prototype, "amount", {
        get: /**
         * @return {?}
         */
        function () {
            return this._amount;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._isMax = false;
            this._amount = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BetAmount.prototype, "isMax", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isMax;
        },
        enumerable: true,
        configurable: true
    });
    return BetAmount;
}());
export { BetAmount };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BetAmount.prototype._isMax;
    /**
     * @type {?}
     * @private
     */
    BetAmount.prototype._amount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmV0LWFtb3VudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL2NvbXBvbmVudHMvbWFpbi9nYW1lL2JldC1hbW91bnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtJQU1JO1FBSlEsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUVmLFlBQU8sR0FBRyxDQUFDLENBQUM7SUFHcEIsQ0FBQzs7OztJQUVELHlCQUFLOzs7SUFBTDtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3BCLENBQUM7Ozs7O0lBRUQsdUJBQUc7Ozs7SUFBSCxVQUFJLEtBQVk7UUFDWixJQUFJLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQztJQUN6QixDQUFDOzs7O0lBQ0QsMEJBQU07OztJQUFOO1FBQ0ksSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUM7SUFDckIsQ0FBQzs7OztJQUNELHdCQUFJOzs7SUFBSjtRQUNJLElBQUksQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDO0lBQ3ZCLENBQUM7Ozs7O0lBRUQsNEJBQVE7Ozs7SUFBUixVQUFTLEtBQVk7UUFDakIsSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUM7UUFFckIsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztTQUNuQjtJQUNMLENBQUM7Ozs7O0lBRUQsNEJBQVE7Ozs7SUFBUixVQUFTLEtBQVk7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUVELHNCQUFJLDZCQUFNOzs7O1FBS1Y7WUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDeEIsQ0FBQzs7Ozs7UUFQRCxVQUFXLEtBQVk7WUFDbkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDekIsQ0FBQzs7O09BQUE7SUFLRCxzQkFBSSw0QkFBSzs7OztRQUFUO1lBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUM7OztPQUFBO0lBQ0wsZ0JBQUM7QUFBRCxDQUFDLEFBL0NELElBK0NDOzs7Ozs7O0lBN0NHLDJCQUF1Qjs7Ozs7SUFFdkIsNEJBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEJldEFtb3VudFxue1xuICAgIHByaXZhdGUgX2lzTWF4ID0gZmFsc2U7XG5cbiAgICBwcml2YXRlIF9hbW91bnQgPSAwO1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgfVxuXG4gICAgcmVzZXQoKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ID0gMDtcbiAgICB9XG5cbiAgICBhZGQodmFsdWU6bnVtYmVyKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ICs9IHZhbHVlO1xuICAgIH1cbiAgICBkb3VibGUoKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ICo9IDI7XG4gICAgfVxuICAgIGhhbGYoKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ICo9IDAuNTtcbiAgICB9XG5cbiAgICBzdWJ0cmFjdCh2YWx1ZTpudW1iZXIpIHtcbiAgICAgICAgdGhpcy5hbW91bnQgLT0gdmFsdWU7XG5cbiAgICAgICAgaWYgKHRoaXMuYW1vdW50IDwgMCkge1xuICAgICAgICAgICAgdGhpcy5hbW91bnQgPSAwO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc2V0VG9NYXgodmFsdWU6bnVtYmVyKSB7XG4gICAgICAgIHRoaXMuYW1vdW50ID0gdmFsdWU7XG4gICAgICAgIHRoaXMuX2lzTWF4ID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBzZXQgYW1vdW50KHZhbHVlOm51bWJlcikge1xuICAgICAgICB0aGlzLl9pc01heCA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9hbW91bnQgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICBnZXQgYW1vdW50KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fYW1vdW50O1xuICAgIH1cbiAgICBnZXQgaXNNYXgoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc01heDtcbiAgICB9XG59Il19