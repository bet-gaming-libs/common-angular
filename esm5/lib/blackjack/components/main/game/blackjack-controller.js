/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/blackjack-controller.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { sleep } from "earnbet-common";
import { BlackjackDealerHand, BlackjackPlayerHand } from './blackjack-hands';
import { BetAmount } from './bet-amount';
var BlackjackGameController = /** @class */ (function () {
    function BlackjackGameController(component, server, soundPlayer) {
        this.component = component;
        this.server = server;
        this.bet = new BetAmount();
        this.initialBetAmount = undefined;
        this._isSplit = false;
        this.hand1Result = undefined;
        this.hand2Result = undefined;
        this._promptForInsurance = false;
        this._showResult = false;
        this.dealer = new BlackjackDealerHand(soundPlayer);
        this.hand1 = new BlackjackPlayerHand(soundPlayer);
        this.hand2 = new BlackjackPlayerHand(soundPlayer);
    }
    /**
     * @private
     * @return {?}
     */
    BlackjackGameController.prototype.reset = /**
     * @private
     * @return {?}
     */
    function () {
        this.component.gameResultModal.close();
        this.dealer.reset();
        this.hand1.reset();
        this.hand2.reset();
        this.initialBetAmount = undefined;
        this._isSplit = false;
        this.selectedHandIndex = 0;
        this.hand1Result = undefined;
        this.hand2Result = undefined;
        this._promptForInsurance = false;
        this._showResult = false;
    };
    /**
     * @param {?} cards
     * @param {?} betAmount
     * @return {?}
     */
    BlackjackGameController.prototype.start = /**
     * @param {?} cards
     * @param {?} betAmount
     * @return {?}
     */
    function (cards, betAmount) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.reset();
                        if (this.bet.amount == 0) {
                            this.bet.add(betAmount);
                        }
                        // player
                        return [4 /*yield*/, this.hand1.dealFirstCard(cards.playerCards[0], this.bet.amount)];
                    case 1:
                        // player
                        _a.sent();
                        // dealer
                        return [4 /*yield*/, this.dealer.dealFirstCard(cards.dealerCards[0])];
                    case 2:
                        // dealer
                        _a.sent();
                        // player
                        return [4 /*yield*/, this.hand1.dealSecondCard(cards.playerCards[1])];
                    case 3:
                        // player
                        _a.sent();
                        // dealer
                        return [4 /*yield*/, this.dealer.dealAnonymousCard()];
                    case 4:
                        // dealer
                        _a.sent();
                        return [4 /*yield*/, this.checkForInsurance()];
                    case 5:
                        _a.sent();
                        this.initialBetAmount = this.bet.amount;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackGameController.prototype.checkForInsurance = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.dealer.isFirstCardAnAce) return [3 /*break*/, 1];
                        this._promptForInsurance = true;
                        return [2 /*return*/];
                    case 1: return [4 /*yield*/, this.checkForBlackJack()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} yesToInsurance
     * @return {?}
     */
    BlackjackGameController.prototype.respondToInsurance = /**
     * @param {?} yesToInsurance
     * @return {?}
     */
    function (yesToInsurance) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isDealerBlackjack;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this._promptForInsurance = false;
                        if (yesToInsurance) {
                            this.hand1.buyInsurance();
                        }
                        return [4 /*yield*/, this.server.respondToInsurance(yesToInsurance)];
                    case 1:
                        isDealerBlackjack = _a.sent();
                        this.checkForBlackJack(isDealerBlackjack);
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @param {?=} isDealerBlackjack
     * @return {?}
     */
    BlackjackGameController.prototype.checkForBlackJack = /**
     * @private
     * @param {?=} isDealerBlackjack
     * @return {?}
     */
    function (isDealerBlackjack) {
        if (isDealerBlackjack === void 0) { isDealerBlackjack = undefined; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('isDealerBlackjack: ' + isDealerBlackjack);
                        if (!(isDealerBlackjack == undefined)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.server.peekForBlackjack()];
                    case 1:
                        isDealerBlackjack = _a.sent();
                        _a.label = 2;
                    case 2:
                        if (isDealerBlackjack || this.hand1.isBlackJack) {
                            this.conclude(isDealerBlackjack);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackGameController.prototype.split = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var card1, card2, newCards, card3, card4;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.isAbleToSplit) {
                            return [2 /*return*/];
                        }
                        this._isSplit = true;
                        // split hands
                        card1 = this.hand1.cards[0].cardId;
                        card2 = this.hand1.cards[1].cardId;
                        return [4 /*yield*/, this.server.split()];
                    case 1:
                        newCards = _a.sent();
                        card3 = newCards[0];
                        card4 = newCards[1];
                        return [4 /*yield*/, this.hand1.split([card1, card3], this.initialBetAmount)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.hand2.split([card2, card4], this.initialBetAmount)];
                    case 3:
                        _a.sent();
                        if (!this.isWaitingForAction) {
                            this.selectOtherHand();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackGameController.prototype.doubleDown = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var cardId;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.isAbleToDoubleDown) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.server.doubleDown(this.selectedHandIndex)];
                    case 1:
                        cardId = _a.sent();
                        // deal only 1 more card
                        return [4 /*yield*/, this.currentHand.doubleDown(cardId)];
                    case 2:
                        // deal only 1 more card
                        _a.sent();
                        // move to other hand (if applicable)
                        this.selectOtherHand();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackGameController.prototype.hit = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var cardId;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.isWaitingForAction) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.server.hit(this.selectedHandIndex)];
                    case 1:
                        cardId = _a.sent();
                        // deal another card
                        return [4 /*yield*/, this.currentHand.hit(cardId)];
                    case 2:
                        // deal another card
                        _a.sent();
                        if (this.currentHand.isBusted) {
                            this.selectOtherHand();
                        }
                        else if (this.currentHand.is21) {
                            this.stand();
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackGameController.prototype.stand = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.isWaitingForAction) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.server.stand()];
                    case 1:
                        _a.sent();
                        this.currentHand.stand();
                        this.selectOtherHand();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @return {?}
     */
    BlackjackGameController.prototype.selectOtherHand = /**
     * @private
     * @return {?}
     */
    function () {
        // move to other hand if applicable
        if (this.isSplit) {
            this.selectedHandIndex =
                this.selectedHandIndex == 0 ?
                    1 : 0;
            if (this.currentHand.isWaitingForAction) {
                return;
            }
        }
        // otherwise conclude
        this.conclude(false);
    };
    /**
     * @private
     * @param {?} isDealerBlackJack
     * @return {?}
     */
    BlackjackGameController.prototype.conclude = /**
     * @private
     * @param {?} isDealerBlackJack
     * @return {?}
     */
    function (isDealerBlackJack) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var e_1, _a, isBusted, cardId_1, isBlackjackForHand1, cards, cards_1, cards_1_1, cardId, e_1_1;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        isBusted = this.isSplit ?
                            (this.hand1.isBusted &&
                                this.hand2.isBusted) :
                            this.hand1.isBusted;
                        if (!!isBusted) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.server.getDealersSecondCard()];
                    case 1:
                        cardId_1 = _b.sent();
                        return [4 /*yield*/, this.dealer.revealSecondCard(cardId_1)];
                    case 2:
                        _b.sent();
                        _b.label = 3;
                    case 3:
                        isBlackjackForHand1 = !this.isSplit &&
                            this.hand1.isBlackJack;
                        if (!(!isBusted && !isDealerBlackJack && !isBlackjackForHand1)) return [3 /*break*/, 12];
                        // deal additional cards for dealer, if applicable
                        return [4 /*yield*/, this.server.getDealersAdditionalCards()];
                    case 4:
                        cards = _b.sent();
                        _b.label = 5;
                    case 5:
                        _b.trys.push([5, 10, 11, 12]);
                        cards_1 = tslib_1.__values(cards), cards_1_1 = cards_1.next();
                        _b.label = 6;
                    case 6:
                        if (!!cards_1_1.done) return [3 /*break*/, 9];
                        cardId = cards_1_1.value;
                        return [4 /*yield*/, this.dealer.dealAdditionalCard(cardId)];
                    case 7:
                        _b.sent();
                        _b.label = 8;
                    case 8:
                        cards_1_1 = cards_1.next();
                        return [3 /*break*/, 6];
                    case 9: return [3 /*break*/, 12];
                    case 10:
                        e_1_1 = _b.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 12];
                    case 11:
                        try {
                            if (cards_1_1 && !cards_1_1.done && (_a = cards_1.return)) _a.call(cards_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                        return [7 /*endfinally*/];
                    case 12:
                        /*** check hand 1 result */
                        // determine hand 1 result
                        this.hand1Result = this.hand1.determineResult(this.dealer);
                        //alert('Hand 1 Result: '+this.hand1Result);
                        //let result:IBlackjackResult = this.hand1Result;
                        /*** check the result of the second hand (if applicable) ***/
                        if (this.isSplit) {
                            this.hand2Result = this.hand2.determineResult(this.dealer);
                            /*
                            result = new CombinedHandsResult(
                                this.hand1Result,
                                this.hand2Result
                            );
                            */
                            //alert('Hand 2 Result: '+this.hand2Result);
                        }
                        /*** end game: ***/
                        this.initialBetAmount = undefined;
                        this._showResult = true;
                        if (this.hand1Result.isInsuranceSuccess ||
                            this.hand1Result.isBlackjack) {
                            this.component.gameResultModal.showResult(this.hand1Result);
                        }
                        else if (this.isSplit && this.hand2Result.isBlackjack) {
                            this.component.gameResultModal.showResult(this.hand2Result);
                        }
                        return [4 /*yield*/, sleep(3000)];
                    case 13:
                        _b.sent();
                        this._showResult = false;
                        this.component.gameResultModal.close();
                        return [2 /*return*/];
                }
            });
        });
    };
    Object.defineProperty(BlackjackGameController.prototype, "isAbleToSplit", {
        get: /**
         * @return {?}
         */
        function () {
            return !this.isSplit &&
                this.hand1.isAbleToSplit;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isAbleToDoubleDown", {
        get: /**
         * @return {?}
         */
        function () {
            return this.currentHand.isAbleToDoubleDown;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "currentHand", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.selectedHandIndex == 0 ?
                this.hand1 :
                this.hand2;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isSplit", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isSplit;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isFirstHandSelected", {
        get: /**
         * @return {?}
         */
        function () {
            return this.selectedHandIndex == 0 &&
                this.isWaitingForAction;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isSecondHandSelected", {
        get: /**
         * @return {?}
         */
        function () {
            return this.selectedHandIndex == 1 &&
                this.isWaitingForAction;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isWaitingForAction", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isStarted &&
                this.currentHand.isWaitingForAction &&
                !this.server.isWaitingForResponse &&
                !this.isGameOver;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isStarted", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isBetPlaced;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "bet1", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isBetPlaced ?
                this.hand1.bet :
                this.bet;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isLoading", {
        get: /**
         * @return {?}
         */
        function () {
            return this.server.isWaitingForResponse || (this.isBetPlaced &&
                !this.promptForInsurance &&
                !this.isWaitingForAction);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isAbleToPlaceBet", {
        get: /**
         * @return {?}
         */
        function () {
            return !this.isBetPlaced &&
                !this.server.isWaitingForResponse;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isBetPlaced", {
        get: /**
         * @return {?}
         */
        function () {
            return this.initialBetAmount != undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isGameOver", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand1Result != undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isDealerWin", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isSplit ?
                (this.isLossForHand1 &&
                    this.isLossForHand2) :
                this.isLossForHand1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isDealerLoss", {
        get: /**
         * @return {?}
         */
        function () {
            return this.dealer.isBusted ||
                (this.isSplit ?
                    (this.isWinForHand1 &&
                        this.isWinForHand2) :
                    this.isWinForHand1);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "amountWon1", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand1Result ?
                this.hand1Result.amountWon :
                0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "amountWon2", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand2Result ?
                this.hand2Result.amountWon :
                0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isWinForHand1", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand1Result && this.hand1Result.isWin;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "showLossForHand1", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isSplit ?
                (this.isGameOver ?
                    this.isLossForHand1 && this.showResult :
                    this.hand1.isBusted) :
                this.isLossForHand1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isLossForHand1", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand1.isBusted ||
                (this.hand1Result && this.hand1Result.isLoss);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isPushForHand1", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand1Result && this.hand1Result.isPush;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isWinForHand2", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand2Result && this.hand2Result.isWin;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isLossForHand2", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand2.isBusted ||
                (this.hand2Result && this.hand2Result.isLoss);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "isPushForHand2", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand2Result && this.hand2Result.isPush;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "promptForInsurance", {
        get: /**
         * @return {?}
         */
        function () {
            return this._promptForInsurance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "choseInsurance", {
        get: /**
         * @return {?}
         */
        function () {
            return this.hand1.boughtInsurance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackGameController.prototype, "showResult", {
        get: /**
         * @return {?}
         */
        function () {
            return this._showResult;
        },
        enumerable: true,
        configurable: true
    });
    return BlackjackGameController;
}());
export { BlackjackGameController };
if (false) {
    /** @type {?} */
    BlackjackGameController.prototype.dealer;
    /** @type {?} */
    BlackjackGameController.prototype.hand1;
    /** @type {?} */
    BlackjackGameController.prototype.hand2;
    /** @type {?} */
    BlackjackGameController.prototype.bet;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.initialBetAmount;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype._isSplit;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.selectedHandIndex;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.hand1Result;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.hand2Result;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype._promptForInsurance;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype._showResult;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.component;
    /**
     * @type {?}
     * @private
     */
    BlackjackGameController.prototype.server;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL2JsYWNramFjay9jb21wb25lbnRzL21haW4vZ2FtZS9ibGFja2phY2stY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQUUsS0FBSyxFQUEwQixNQUFNLGdCQUFnQixDQUFDO0FBRS9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQzdFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFPekM7SUFpQkksaUNBQ1ksU0FBNkIsRUFDN0IsTUFBdUIsRUFDL0IsV0FBd0I7UUFGaEIsY0FBUyxHQUFULFNBQVMsQ0FBb0I7UUFDN0IsV0FBTSxHQUFOLE1BQU0sQ0FBaUI7UUFiMUIsUUFBRyxHQUFHLElBQUksU0FBUyxFQUFFLENBQUM7UUFDdkIscUJBQWdCLEdBQVUsU0FBUyxDQUFDO1FBQ3BDLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFHakIsZ0JBQVcsR0FBb0IsU0FBUyxDQUFDO1FBQ3pDLGdCQUFXLEdBQW9CLFNBQVMsQ0FBQztRQUN6Qyx3QkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDNUIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFReEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksbUJBQW1CLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Ozs7SUFFTyx1Q0FBSzs7OztJQUFiO1FBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFbkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFNBQVMsQ0FBQztRQUNsQyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDO1FBRTNCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1FBQzdCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQzs7Ozs7O0lBRUssdUNBQUs7Ozs7O0lBQVgsVUFBWSxLQUE0QixFQUFDLFNBQWdCOzs7Ozt3QkFDckQsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUdiLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFOzRCQUN0QixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQzt5QkFDM0I7d0JBR0QsU0FBUzt3QkFDVCxxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FDMUIsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFDcEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQ2xCLEVBQUE7O3dCQUpELFNBQVM7d0JBQ1QsU0FHQyxDQUFDO3dCQUNGLFNBQVM7d0JBQ1QscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFBOzt3QkFEckQsU0FBUzt3QkFDVCxTQUFxRCxDQUFDO3dCQUV0RCxTQUFTO3dCQUNULHFCQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBQTs7d0JBRHJELFNBQVM7d0JBQ1QsU0FBcUQsQ0FBQzt3QkFDdEQsU0FBUzt3QkFDVCxxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQixFQUFFLEVBQUE7O3dCQURyQyxTQUFTO3dCQUNULFNBQXFDLENBQUM7d0JBRXRDLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFBOzt3QkFBOUIsU0FBOEIsQ0FBQzt3QkFHL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDOzs7OztLQUMzQzs7OztJQUVLLG1EQUFpQjs7O0lBQXZCOzs7Ozs2QkFFUSxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUE1Qix3QkFBNEI7d0JBQzVCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7d0JBRWhDLHNCQUFPOzRCQUVQLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFBOzt3QkFBOUIsU0FBOEIsQ0FBQzs7Ozs7O0tBRXRDOzs7OztJQUVLLG9EQUFrQjs7OztJQUF4QixVQUF5QixjQUFzQjs7Ozs7O3dCQUMzQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO3dCQUVqQyxJQUFJLGNBQWMsRUFBRTs0QkFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsQ0FBQzt5QkFDN0I7d0JBR0cscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsRUFBQTs7d0JBRGxELGlCQUFpQixHQUNuQixTQUFvRDt3QkFFeEQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLENBQUM7Ozs7O0tBQzdDOzs7Ozs7SUFFYSxtREFBaUI7Ozs7O0lBQS9CLFVBQWdDLGlCQUFxQztRQUFyQyxrQ0FBQSxFQUFBLDZCQUFxQzs7Ozs7d0JBQ2pFLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEdBQUMsaUJBQWlCLENBQUMsQ0FBQzs2QkFJakQsQ0FBQSxpQkFBaUIsSUFBSSxTQUFTLENBQUEsRUFBOUIsd0JBQThCO3dCQUNWLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsRUFBQTs7d0JBQXhELGlCQUFpQixHQUFHLFNBQW9DLENBQUM7Ozt3QkFHN0QsSUFBSSxpQkFBaUIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRTs0QkFDN0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO3lCQUNwQzs7Ozs7S0FDSjs7OztJQUlLLHVDQUFLOzs7SUFBWDs7Ozs7O3dCQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFOzRCQUNyQixzQkFBTzt5QkFDVjt3QkFHRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzs7d0JBSWYsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU07d0JBQ2xDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNO3dCQUV2QixxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxFQUFBOzt3QkFBcEMsUUFBUSxHQUFHLFNBQXlCO3dCQUVwQyxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7d0JBRXpCLHFCQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUNsQixDQUFDLEtBQUssRUFBQyxLQUFLLENBQUMsRUFDYixJQUFJLENBQUMsZ0JBQWdCLENBQ3hCLEVBQUE7O3dCQUhELFNBR0MsQ0FBQzt3QkFDRixxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FDbEIsQ0FBQyxLQUFLLEVBQUMsS0FBSyxDQUFDLEVBQ2IsSUFBSSxDQUFDLGdCQUFnQixDQUN4QixFQUFBOzt3QkFIRCxTQUdDLENBQUM7d0JBRUYsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTs0QkFDMUIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO3lCQUMxQjs7Ozs7S0FDSjs7OztJQUVLLDRDQUFVOzs7SUFBaEI7Ozs7Ozs2QkFDUSxJQUFJLENBQUMsa0JBQWtCLEVBQXZCLHdCQUF1Qjt3QkFDUixxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FDdkMsSUFBSSxDQUFDLGlCQUFpQixDQUN6QixFQUFBOzt3QkFGSyxNQUFNLEdBQUcsU0FFZDt3QkFFRCx3QkFBd0I7d0JBQ3hCLHFCQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFEekMsd0JBQXdCO3dCQUN4QixTQUF5QyxDQUFDO3dCQUUxQyxxQ0FBcUM7d0JBQ3JDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzs7Ozs7O0tBRTlCOzs7O0lBRUsscUNBQUc7OztJQUFUOzs7Ozs7NkJBQ1EsSUFBSSxDQUFDLGtCQUFrQixFQUF2Qix3QkFBdUI7d0JBQ1IscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQ2hDLElBQUksQ0FBQyxpQkFBaUIsQ0FDekIsRUFBQTs7d0JBRkssTUFBTSxHQUFHLFNBRWQ7d0JBRUQsb0JBQW9CO3dCQUNwQixxQkFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBRGxDLG9CQUFvQjt3QkFDcEIsU0FBa0MsQ0FBQzt3QkFFbkMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRTs0QkFDM0IsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO3lCQUMxQjs2QkFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFOzRCQUM1QixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7eUJBQ2hCOzs7Ozs7S0FFUjs7OztJQUVLLHVDQUFLOzs7SUFBWDs7Ozs7NkJBQ1EsSUFBSSxDQUFDLGtCQUFrQixFQUF2Qix3QkFBdUI7d0JBQ3ZCLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEVBQUE7O3dCQUF6QixTQUF5QixDQUFDO3dCQUUxQixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUV6QixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7Ozs7OztLQUU5Qjs7Ozs7SUFFTyxpREFBZTs7OztJQUF2QjtRQUNJLG1DQUFtQztRQUNuQyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZCxJQUFJLENBQUMsaUJBQWlCO2dCQUNsQixJQUFJLENBQUMsaUJBQWlCLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixFQUFFO2dCQUNyQyxPQUFPO2FBQ1Y7U0FDSjtRQUVELHFCQUFxQjtRQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUVhLDBDQUFROzs7OztJQUF0QixVQUF1QixpQkFBeUI7Ozs7Ozt3QkFDdEMsUUFBUSxHQUNWLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDVixDQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUTtnQ0FDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQ3RCLENBQUMsQ0FBQzs0QkFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVE7NkJBRXZCLENBQUMsUUFBUSxFQUFULHdCQUFTO3dCQUNNLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsRUFBQTs7d0JBQWpELFdBQVMsU0FBd0M7d0JBRXZELHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBTSxDQUFDLEVBQUE7O3dCQUExQyxTQUEwQyxDQUFDOzs7d0JBR3pDLG1CQUFtQixHQUNyQixDQUFDLElBQUksQ0FBQyxPQUFPOzRCQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVzs2QkFHdEIsQ0FBQSxDQUFDLFFBQVEsSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsbUJBQW1CLENBQUEsRUFBdkQseUJBQXVEOzt3QkFFekMscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsRUFBRSxFQUFBOzt3QkFBckQsS0FBSyxHQUFHLFNBQTZDOzs7O3dCQUV4QyxVQUFBLGlCQUFBLEtBQUssQ0FBQTs7Ozt3QkFBZixNQUFNO3dCQUNYLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUE1QyxTQUE0QyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozt3QkFNckQsMkJBQTJCO3dCQUMzQiwwQkFBMEI7d0JBQzFCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUUzRCw0Q0FBNEM7d0JBQzVDLGlEQUFpRDt3QkFHakQsNkRBQTZEO3dCQUM3RCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7NEJBQ2QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7NEJBRTNEOzs7Ozs4QkFLRTs0QkFFRiw0Q0FBNEM7eUJBQy9DO3dCQUlELG1CQUFtQjt3QkFDbkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFNBQVMsQ0FBQzt3QkFFbEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7d0JBR3hCLElBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0I7NEJBQ25DLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUM5Qjs0QkFDRSxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3lCQUMvRDs2QkFDSSxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7NEJBQ25ELElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7eUJBQy9EO3dCQUdELHFCQUFNLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQTs7d0JBQWpCLFNBQWlCLENBQUM7d0JBRWxCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO3dCQUV6QixJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7Ozs7S0FjMUM7SUFJRCxzQkFBSSxrREFBYTs7OztRQUFqQjtZQUNJLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTztnQkFDWixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQztRQUNyQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHVEQUFrQjs7OztRQUF0QjtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQztRQUMvQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFZLGdEQUFXOzs7OztRQUF2QjtZQUNJLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN2QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDRDQUFPOzs7O1FBQVg7WUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx3REFBbUI7Ozs7UUFBdkI7WUFDSSxPQUFPLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxDQUFDO2dCQUMxQixJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDcEMsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSx5REFBb0I7Ozs7UUFBeEI7WUFDSSxPQUFPLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxDQUFDO2dCQUMxQixJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDcEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx1REFBa0I7Ozs7UUFBdEI7WUFDSSxPQUFPLElBQUksQ0FBQyxTQUFTO2dCQUNiLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCO2dCQUNuQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsb0JBQW9CO2dCQUNqQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDN0IsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw4Q0FBUzs7OztRQUFiO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVCLENBQUM7OztPQUFBO0lBRUQsc0JBQUkseUNBQUk7Ozs7UUFBUjtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ3JCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksOENBQVM7Ozs7UUFBYjtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsSUFBSSxDQUN2QyxJQUFJLENBQUMsV0FBVztnQkFDaEIsQ0FBQyxJQUFJLENBQUMsa0JBQWtCO2dCQUN4QixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FDM0IsQ0FBQztRQUNOLENBQUM7OztPQUFBO0lBRUQsc0JBQUkscURBQWdCOzs7O1FBQXBCO1lBQ0ksT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXO2dCQUNoQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUM7UUFDOUMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxnREFBVzs7OztRQUFmO1lBQ0ksT0FBTyxJQUFJLENBQUMsZ0JBQWdCLElBQUksU0FBUyxDQUFDO1FBQzlDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksK0NBQVU7Ozs7UUFBZDtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsSUFBSSxTQUFTLENBQUM7UUFDekMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxnREFBVzs7OztRQUFmO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2IsQ0FDSSxJQUFJLENBQUMsY0FBYztvQkFDbkIsSUFBSSxDQUFDLGNBQWMsQ0FDdEIsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxjQUFjLENBQUM7UUFFaEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxpREFBWTs7OztRQUFoQjtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRO2dCQUN2QixDQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDZCxDQUNJLElBQUksQ0FBQyxhQUFhO3dCQUNsQixJQUFJLENBQUMsYUFBYSxDQUNyQixDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FDckIsQ0FBQztRQUNWLENBQUM7OztPQUFBO0lBRUQsc0JBQUksK0NBQVU7Ozs7UUFBZDtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNqQixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM1QixDQUFDLENBQUM7UUFDZCxDQUFDOzs7T0FBQTtJQUNELHNCQUFJLCtDQUFVOzs7O1FBQWQ7WUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDNUIsQ0FBQyxDQUFDO1FBQ2QsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxrREFBYTs7OztRQUFqQjtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztRQUN0RCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHFEQUFnQjs7OztRQUFwQjtZQUNJLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNiLENBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUNiLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FDMUIsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxjQUFjLENBQUM7UUFDaEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxtREFBYzs7OztRQUFsQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO2dCQUNsQixDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxRCxDQUFDOzs7T0FBQTtJQUNELHNCQUFJLG1EQUFjOzs7O1FBQWxCO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1FBQ3ZELENBQUM7OztPQUFBO0lBRUQsc0JBQUksa0RBQWE7Ozs7UUFBakI7WUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7UUFDdEQsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSxtREFBYzs7OztRQUFsQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO2dCQUNsQixDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxRCxDQUFDOzs7T0FBQTtJQUNELHNCQUFJLG1EQUFjOzs7O1FBQWxCO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1FBQ3ZELENBQUM7OztPQUFBO0lBRUQsc0JBQUksdURBQWtCOzs7O1FBQXRCO1lBQ0ksT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUM7UUFDcEMsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSxtREFBYzs7OztRQUFsQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUM7UUFDdEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQ0FBVTs7OztRQUFkO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVCLENBQUM7OztPQUFBO0lBQ0wsOEJBQUM7QUFBRCxDQUFDLEFBdGJELElBc2JDOzs7O0lBcGJHLHlDQUFvQzs7SUFDcEMsd0NBQW1DOztJQUNuQyx3Q0FBbUM7O0lBRW5DLHNDQUErQjs7Ozs7SUFDL0IsbURBQTRDOzs7OztJQUM1QywyQ0FBeUI7Ozs7O0lBQ3pCLG9EQUFpQzs7Ozs7SUFFakMsOENBQWlEOzs7OztJQUNqRCw4Q0FBaUQ7Ozs7O0lBQ2pELHNEQUFvQzs7Ozs7SUFDcEMsOENBQTRCOzs7OztJQUl4Qiw0Q0FBcUM7Ozs7O0lBQ3JDLHlDQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHNsZWVwLCBJQmxhY2tqYWNrSW5pdGlhbENhcmRzIH0gZnJvbSBcImVhcm5iZXQtY29tbW9uXCI7XG5cbmltcG9ydCB7IEJsYWNramFja0RlYWxlckhhbmQsIEJsYWNramFja1BsYXllckhhbmQgfSBmcm9tICcuL2JsYWNramFjay1oYW5kcyc7XG5pbXBvcnQgeyBCZXRBbW91bnQgfSBmcm9tICcuL2JldC1hbW91bnQnO1xuaW1wb3J0IHsgSUJsYWNramFja1Jlc3VsdCB9IGZyb20gJy4vaGFuZC1yZXN1bHQnO1xuaW1wb3J0IHsgSUJsYWNramFja0NvbXBvbmVudCB9IGZyb20gJy4uLy4uL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSUJsYWNramFja1NlcnZlciB9IGZyb20gJy4uLy4uLy4uL3NlcnZlci9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElTb3VuZFBsYXllciB9IGZyb20gJy4uLy4uLy4uLy4uL19jb21tb24vbWVkaWEvaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGNsYXNzIEJsYWNramFja0dhbWVDb250cm9sbGVyXG57XG4gICAgcmVhZG9ubHkgZGVhbGVyOkJsYWNramFja0RlYWxlckhhbmQ7XG4gICAgcmVhZG9ubHkgaGFuZDE6QmxhY2tqYWNrUGxheWVySGFuZDtcbiAgICByZWFkb25seSBoYW5kMjpCbGFja2phY2tQbGF5ZXJIYW5kO1xuXG4gICAgcmVhZG9ubHkgYmV0ID0gbmV3IEJldEFtb3VudCgpO1xuICAgIHByaXZhdGUgaW5pdGlhbEJldEFtb3VudDpudW1iZXIgPSB1bmRlZmluZWQ7XG4gICAgcHJpdmF0ZSBfaXNTcGxpdCA9IGZhbHNlO1xuICAgIHByaXZhdGUgc2VsZWN0ZWRIYW5kSW5kZXg6bnVtYmVyO1xuXG4gICAgcHJpdmF0ZSBoYW5kMVJlc3VsdDpJQmxhY2tqYWNrUmVzdWx0ID0gdW5kZWZpbmVkO1xuICAgIHByaXZhdGUgaGFuZDJSZXN1bHQ6SUJsYWNramFja1Jlc3VsdCA9IHVuZGVmaW5lZDtcbiAgICBwcml2YXRlIF9wcm9tcHRGb3JJbnN1cmFuY2UgPSBmYWxzZTtcbiAgICBwcml2YXRlIF9zaG93UmVzdWx0ID0gZmFsc2U7XG5cblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGNvbXBvbmVudDpJQmxhY2tqYWNrQ29tcG9uZW50LFxuICAgICAgICBwcml2YXRlIHNlcnZlcjpJQmxhY2tqYWNrU2VydmVyLFxuICAgICAgICBzb3VuZFBsYXllcjpJU291bmRQbGF5ZXJcbiAgICApIHtcbiAgICAgICAgdGhpcy5kZWFsZXIgPSBuZXcgQmxhY2tqYWNrRGVhbGVySGFuZChzb3VuZFBsYXllcik7XG4gICAgICAgIHRoaXMuaGFuZDEgPSBuZXcgQmxhY2tqYWNrUGxheWVySGFuZChzb3VuZFBsYXllcik7XG4gICAgICAgIHRoaXMuaGFuZDIgPSBuZXcgQmxhY2tqYWNrUGxheWVySGFuZChzb3VuZFBsYXllcik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSByZXNldCgpIHtcbiAgICAgICAgdGhpcy5jb21wb25lbnQuZ2FtZVJlc3VsdE1vZGFsLmNsb3NlKCk7XG5cbiAgICAgICAgdGhpcy5kZWFsZXIucmVzZXQoKTtcbiAgICAgICAgdGhpcy5oYW5kMS5yZXNldCgpO1xuICAgICAgICB0aGlzLmhhbmQyLnJlc2V0KCk7XG5cbiAgICAgICAgdGhpcy5pbml0aWFsQmV0QW1vdW50ID0gdW5kZWZpbmVkO1xuICAgICAgICB0aGlzLl9pc1NwbGl0ID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRIYW5kSW5kZXggPSAwO1xuXG4gICAgICAgIHRoaXMuaGFuZDFSZXN1bHQgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuaGFuZDJSZXN1bHQgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuX3Byb21wdEZvckluc3VyYW5jZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9zaG93UmVzdWx0ID0gZmFsc2U7XG4gICAgfVxuXG4gICAgYXN5bmMgc3RhcnQoY2FyZHM6SUJsYWNramFja0luaXRpYWxDYXJkcyxiZXRBbW91bnQ6bnVtYmVyKSB7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcblxuXG4gICAgICAgIGlmICh0aGlzLmJldC5hbW91bnQgPT0gMCkge1xuICAgICAgICAgICAgdGhpcy5iZXQuYWRkKGJldEFtb3VudCk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIC8vIHBsYXllclxuICAgICAgICBhd2FpdCB0aGlzLmhhbmQxLmRlYWxGaXJzdENhcmQoXG4gICAgICAgICAgICBjYXJkcy5wbGF5ZXJDYXJkc1swXSxcbiAgICAgICAgICAgIHRoaXMuYmV0LmFtb3VudFxuICAgICAgICApO1xuICAgICAgICAvLyBkZWFsZXJcbiAgICAgICAgYXdhaXQgdGhpcy5kZWFsZXIuZGVhbEZpcnN0Q2FyZChjYXJkcy5kZWFsZXJDYXJkc1swXSk7XG5cbiAgICAgICAgLy8gcGxheWVyXG4gICAgICAgIGF3YWl0IHRoaXMuaGFuZDEuZGVhbFNlY29uZENhcmQoY2FyZHMucGxheWVyQ2FyZHNbMV0pO1xuICAgICAgICAvLyBkZWFsZXJcbiAgICAgICAgYXdhaXQgdGhpcy5kZWFsZXIuZGVhbEFub255bW91c0NhcmQoKTtcblxuICAgICAgICBhd2FpdCB0aGlzLmNoZWNrRm9ySW5zdXJhbmNlKCk7XG5cblxuICAgICAgICB0aGlzLmluaXRpYWxCZXRBbW91bnQgPSB0aGlzLmJldC5hbW91bnQ7XG4gICAgfVxuXG4gICAgYXN5bmMgY2hlY2tGb3JJbnN1cmFuY2UoKSB7XG4gICAgICAgIC8vIGlmIGRlYWxlciBpcyBzaG93aW5nIGFjZSwgb2ZmZXIgaW5zdXJhbmNlXG4gICAgICAgIGlmICh0aGlzLmRlYWxlci5pc0ZpcnN0Q2FyZEFuQWNlKSB7XG4gICAgICAgICAgICB0aGlzLl9wcm9tcHRGb3JJbnN1cmFuY2UgPSB0cnVlO1xuXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLmNoZWNrRm9yQmxhY2tKYWNrKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhc3luYyByZXNwb25kVG9JbnN1cmFuY2UoeWVzVG9JbnN1cmFuY2U6Ym9vbGVhbikge1xuICAgICAgICB0aGlzLl9wcm9tcHRGb3JJbnN1cmFuY2UgPSBmYWxzZTtcblxuICAgICAgICBpZiAoeWVzVG9JbnN1cmFuY2UpIHtcbiAgICAgICAgICAgIHRoaXMuaGFuZDEuYnV5SW5zdXJhbmNlKCk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBpc0RlYWxlckJsYWNramFjayA9XG4gICAgICAgICAgICBhd2FpdCB0aGlzLnNlcnZlci5yZXNwb25kVG9JbnN1cmFuY2UoeWVzVG9JbnN1cmFuY2UpO1xuXG4gICAgICAgIHRoaXMuY2hlY2tGb3JCbGFja0phY2soaXNEZWFsZXJCbGFja2phY2spO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgY2hlY2tGb3JCbGFja0phY2soaXNEZWFsZXJCbGFja2phY2s6Ym9vbGVhbiA9IHVuZGVmaW5lZCkge1xuICAgICAgICBjb25zb2xlLmxvZygnaXNEZWFsZXJCbGFja2phY2s6ICcraXNEZWFsZXJCbGFja2phY2spO1xuXG5cbiAgICAgICAgLy8gKioqIHBlZWsgZm9yIGJsYWNramFjayAqKipcbiAgICAgICAgaWYgKGlzRGVhbGVyQmxhY2tqYWNrID09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaXNEZWFsZXJCbGFja2phY2sgPSBhd2FpdCB0aGlzLnNlcnZlci5wZWVrRm9yQmxhY2tqYWNrKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaXNEZWFsZXJCbGFja2phY2sgfHwgdGhpcy5oYW5kMS5pc0JsYWNrSmFjaykge1xuICAgICAgICAgICAgdGhpcy5jb25jbHVkZShpc0RlYWxlckJsYWNramFjayk7XG4gICAgICAgIH1cbiAgICB9XG5cblxuXG4gICAgYXN5bmMgc3BsaXQoKSB7XG4gICAgICAgIGlmICghdGhpcy5pc0FibGVUb1NwbGl0KSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuXG4gICAgICAgIHRoaXMuX2lzU3BsaXQgPSB0cnVlO1xuXG5cbiAgICAgICAgLy8gc3BsaXQgaGFuZHNcbiAgICAgICAgY29uc3QgY2FyZDEgPSB0aGlzLmhhbmQxLmNhcmRzWzBdLmNhcmRJZDtcbiAgICAgICAgY29uc3QgY2FyZDIgPSB0aGlzLmhhbmQxLmNhcmRzWzFdLmNhcmRJZDtcblxuICAgICAgICBjb25zdCBuZXdDYXJkcyA9IGF3YWl0IHRoaXMuc2VydmVyLnNwbGl0KCk7XG5cbiAgICAgICAgY29uc3QgY2FyZDMgPSBuZXdDYXJkc1swXTtcbiAgICAgICAgY29uc3QgY2FyZDQgPSBuZXdDYXJkc1sxXTtcblxuICAgICAgICBhd2FpdCB0aGlzLmhhbmQxLnNwbGl0KFxuICAgICAgICAgICAgW2NhcmQxLGNhcmQzXSxcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbEJldEFtb3VudFxuICAgICAgICApO1xuICAgICAgICBhd2FpdCB0aGlzLmhhbmQyLnNwbGl0KFxuICAgICAgICAgICAgW2NhcmQyLGNhcmQ0XSxcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbEJldEFtb3VudFxuICAgICAgICApO1xuXG4gICAgICAgIGlmICghdGhpcy5pc1dhaXRpbmdGb3JBY3Rpb24pIHtcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0T3RoZXJIYW5kKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhc3luYyBkb3VibGVEb3duKCkge1xuICAgICAgICBpZiAodGhpcy5pc0FibGVUb0RvdWJsZURvd24pIHtcbiAgICAgICAgICAgIGNvbnN0IGNhcmRJZCA9IGF3YWl0IHRoaXMuc2VydmVyLmRvdWJsZURvd24oXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEhhbmRJbmRleFxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgLy8gZGVhbCBvbmx5IDEgbW9yZSBjYXJkXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmN1cnJlbnRIYW5kLmRvdWJsZURvd24oY2FyZElkKTtcblxuICAgICAgICAgICAgLy8gbW92ZSB0byBvdGhlciBoYW5kIChpZiBhcHBsaWNhYmxlKVxuICAgICAgICAgICAgdGhpcy5zZWxlY3RPdGhlckhhbmQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIGhpdCgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNXYWl0aW5nRm9yQWN0aW9uKSB7XG4gICAgICAgICAgICBjb25zdCBjYXJkSWQgPSBhd2FpdCB0aGlzLnNlcnZlci5oaXQoXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEhhbmRJbmRleFxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgLy8gZGVhbCBhbm90aGVyIGNhcmRcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuY3VycmVudEhhbmQuaGl0KGNhcmRJZCk7XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRIYW5kLmlzQnVzdGVkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RPdGhlckhhbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuY3VycmVudEhhbmQuaXMyMSkge1xuICAgICAgICAgICAgICAgIHRoaXMuc3RhbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIHN0YW5kKCkge1xuICAgICAgICBpZiAodGhpcy5pc1dhaXRpbmdGb3JBY3Rpb24pIHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuc2VydmVyLnN0YW5kKCk7XG5cbiAgICAgICAgICAgIHRoaXMuY3VycmVudEhhbmQuc3RhbmQoKTtcblxuICAgICAgICAgICAgdGhpcy5zZWxlY3RPdGhlckhhbmQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgc2VsZWN0T3RoZXJIYW5kKCkge1xuICAgICAgICAvLyBtb3ZlIHRvIG90aGVyIGhhbmQgaWYgYXBwbGljYWJsZVxuICAgICAgICBpZiAodGhpcy5pc1NwbGl0KSB7XG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkSGFuZEluZGV4ID1cbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkSGFuZEluZGV4ID09IDAgP1xuICAgICAgICAgICAgICAgICAgICAxIDogMDtcblxuICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudEhhbmQuaXNXYWl0aW5nRm9yQWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLy8gb3RoZXJ3aXNlIGNvbmNsdWRlXG4gICAgICAgIHRoaXMuY29uY2x1ZGUoZmFsc2UpO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgY29uY2x1ZGUoaXNEZWFsZXJCbGFja0phY2s6Ym9vbGVhbikge1xuICAgICAgICBjb25zdCBpc0J1c3RlZCA9XG4gICAgICAgICAgICB0aGlzLmlzU3BsaXQgP1xuICAgICAgICAgICAgICAgIChcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5pc0J1c3RlZCAmJlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmhhbmQyLmlzQnVzdGVkXG4gICAgICAgICAgICAgICAgKSA6XG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5pc0J1c3RlZDtcblxuICAgICAgICBpZiAoIWlzQnVzdGVkKSB7XG4gICAgICAgICAgICBjb25zdCBjYXJkSWQgPSBhd2FpdCB0aGlzLnNlcnZlci5nZXREZWFsZXJzU2Vjb25kQ2FyZCgpO1xuXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmRlYWxlci5yZXZlYWxTZWNvbmRDYXJkKGNhcmRJZCk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBpc0JsYWNramFja0ZvckhhbmQxID1cbiAgICAgICAgICAgICF0aGlzLmlzU3BsaXQgJiZcbiAgICAgICAgICAgIHRoaXMuaGFuZDEuaXNCbGFja0phY2s7XG5cblxuICAgICAgICBpZiAoIWlzQnVzdGVkICYmICFpc0RlYWxlckJsYWNrSmFjayAmJiAhaXNCbGFja2phY2tGb3JIYW5kMSkge1xuICAgICAgICAgICAgLy8gZGVhbCBhZGRpdGlvbmFsIGNhcmRzIGZvciBkZWFsZXIsIGlmIGFwcGxpY2FibGVcbiAgICAgICAgICAgIGNvbnN0IGNhcmRzID0gYXdhaXQgdGhpcy5zZXJ2ZXIuZ2V0RGVhbGVyc0FkZGl0aW9uYWxDYXJkcygpO1xuXG4gICAgICAgICAgICBmb3IgKHZhciBjYXJkSWQgb2YgY2FyZHMpIHtcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmRlYWxlci5kZWFsQWRkaXRpb25hbENhcmQoY2FyZElkKTtcblxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cblxuICAgICAgICAvKioqIGNoZWNrIGhhbmQgMSByZXN1bHQgKi9cbiAgICAgICAgLy8gZGV0ZXJtaW5lIGhhbmQgMSByZXN1bHRcbiAgICAgICAgdGhpcy5oYW5kMVJlc3VsdCA9IHRoaXMuaGFuZDEuZGV0ZXJtaW5lUmVzdWx0KHRoaXMuZGVhbGVyKTtcblxuICAgICAgICAvL2FsZXJ0KCdIYW5kIDEgUmVzdWx0OiAnK3RoaXMuaGFuZDFSZXN1bHQpO1xuICAgICAgICAvL2xldCByZXN1bHQ6SUJsYWNramFja1Jlc3VsdCA9IHRoaXMuaGFuZDFSZXN1bHQ7XG5cblxuICAgICAgICAvKioqIGNoZWNrIHRoZSByZXN1bHQgb2YgdGhlIHNlY29uZCBoYW5kIChpZiBhcHBsaWNhYmxlKSAqKiovXG4gICAgICAgIGlmICh0aGlzLmlzU3BsaXQpIHtcbiAgICAgICAgICAgIHRoaXMuaGFuZDJSZXN1bHQgPSB0aGlzLmhhbmQyLmRldGVybWluZVJlc3VsdCh0aGlzLmRlYWxlcik7XG5cbiAgICAgICAgICAgIC8qXG4gICAgICAgICAgICByZXN1bHQgPSBuZXcgQ29tYmluZWRIYW5kc1Jlc3VsdChcbiAgICAgICAgICAgICAgICB0aGlzLmhhbmQxUmVzdWx0LFxuICAgICAgICAgICAgICAgIHRoaXMuaGFuZDJSZXN1bHRcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICAvL2FsZXJ0KCdIYW5kIDIgUmVzdWx0OiAnK3RoaXMuaGFuZDJSZXN1bHQpO1xuICAgICAgICB9XG5cblxuXG4gICAgICAgIC8qKiogZW5kIGdhbWU6ICoqKi9cbiAgICAgICAgdGhpcy5pbml0aWFsQmV0QW1vdW50ID0gdW5kZWZpbmVkO1xuXG4gICAgICAgIHRoaXMuX3Nob3dSZXN1bHQgPSB0cnVlO1xuXG5cbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgdGhpcy5oYW5kMVJlc3VsdC5pc0luc3VyYW5jZVN1Y2Nlc3MgfHxcbiAgICAgICAgICAgIHRoaXMuaGFuZDFSZXN1bHQuaXNCbGFja2phY2tcbiAgICAgICAgKSB7XG4gICAgICAgICAgICB0aGlzLmNvbXBvbmVudC5nYW1lUmVzdWx0TW9kYWwuc2hvd1Jlc3VsdCh0aGlzLmhhbmQxUmVzdWx0KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLmlzU3BsaXQgJiYgdGhpcy5oYW5kMlJlc3VsdC5pc0JsYWNramFjaykge1xuICAgICAgICAgICAgdGhpcy5jb21wb25lbnQuZ2FtZVJlc3VsdE1vZGFsLnNob3dSZXN1bHQodGhpcy5oYW5kMlJlc3VsdCk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGF3YWl0IHNsZWVwKDMwMDApO1xuXG4gICAgICAgIHRoaXMuX3Nob3dSZXN1bHQgPSBmYWxzZTtcblxuICAgICAgICB0aGlzLmNvbXBvbmVudC5nYW1lUmVzdWx0TW9kYWwuY2xvc2UoKTtcblxuXG5cblxuXG4gICAgICAgIC8vYXdhaXQgdGhpcy5jb21wb25lbnQuZ2FtZVJlc3VsdE1vZGFsLnNob3dSZXN1bHQocmVzdWx0KTtcblxuICAgICAgICAvKlxuICAgICAgICBhd2FpdCBzbGVlcCgzMDAwKTtcbiAgICAgICAgaWYgKCF0aGlzLmlzQmV0UGxhY2VkKSB7XG4gICAgICAgICAgICB0aGlzLnJlc2V0KCk7XG4gICAgICAgIH1cbiAgICAgICAgKi9cbiAgICB9XG5cblxuXG4gICAgZ2V0IGlzQWJsZVRvU3BsaXQoKSB7XG4gICAgICAgIHJldHVybiAhdGhpcy5pc1NwbGl0ICYmXG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5pc0FibGVUb1NwbGl0O1xuICAgIH1cblxuICAgIGdldCBpc0FibGVUb0RvdWJsZURvd24oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRIYW5kLmlzQWJsZVRvRG91YmxlRG93bjtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCBjdXJyZW50SGFuZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRIYW5kSW5kZXggPT0gMCA/XG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMSA6XG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMjtcbiAgICB9XG5cbiAgICBnZXQgaXNTcGxpdCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzU3BsaXQ7XG4gICAgfVxuXG4gICAgZ2V0IGlzRmlyc3RIYW5kU2VsZWN0ZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkSGFuZEluZGV4ID09IDAgJiZcbiAgICAgICAgICAgICAgICB0aGlzLmlzV2FpdGluZ0ZvckFjdGlvbjtcbiAgICB9XG4gICAgZ2V0IGlzU2Vjb25kSGFuZFNlbGVjdGVkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZEhhbmRJbmRleCA9PSAxICYmXG4gICAgICAgICAgICAgICAgdGhpcy5pc1dhaXRpbmdGb3JBY3Rpb247XG4gICAgfVxuXG4gICAgZ2V0IGlzV2FpdGluZ0ZvckFjdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTdGFydGVkICYmXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50SGFuZC5pc1dhaXRpbmdGb3JBY3Rpb24gJiZcbiAgICAgICAgICAgICAgICAhdGhpcy5zZXJ2ZXIuaXNXYWl0aW5nRm9yUmVzcG9uc2UgJiZcbiAgICAgICAgICAgICAgICAhdGhpcy5pc0dhbWVPdmVyO1xuICAgIH1cblxuICAgIGdldCBpc1N0YXJ0ZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzQmV0UGxhY2VkO1xuICAgIH1cblxuICAgIGdldCBiZXQxKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc0JldFBsYWNlZCA/XG4gICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5iZXQgOlxuICAgICAgICAgICAgICAgIHRoaXMuYmV0O1xuICAgIH1cblxuICAgIGdldCBpc0xvYWRpbmcoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlcnZlci5pc1dhaXRpbmdGb3JSZXNwb25zZSB8fCAoXG4gICAgICAgICAgICB0aGlzLmlzQmV0UGxhY2VkICYmXG4gICAgICAgICAgICAhdGhpcy5wcm9tcHRGb3JJbnN1cmFuY2UgJiZcbiAgICAgICAgICAgICF0aGlzLmlzV2FpdGluZ0ZvckFjdGlvblxuICAgICAgICApO1xuICAgIH1cblxuICAgIGdldCBpc0FibGVUb1BsYWNlQmV0KCkge1xuICAgICAgICByZXR1cm4gIXRoaXMuaXNCZXRQbGFjZWQgJiZcbiAgICAgICAgICAgICAgICAhdGhpcy5zZXJ2ZXIuaXNXYWl0aW5nRm9yUmVzcG9uc2U7XG4gICAgfVxuXG4gICAgZ2V0IGlzQmV0UGxhY2VkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pbml0aWFsQmV0QW1vdW50ICE9IHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICBnZXQgaXNHYW1lT3ZlcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaGFuZDFSZXN1bHQgIT0gdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIGdldCBpc0RlYWxlcldpbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTcGxpdCA/XG4gICAgICAgICAgICAgICAgKFxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzTG9zc0ZvckhhbmQxICYmXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNMb3NzRm9ySGFuZDJcbiAgICAgICAgICAgICAgICApIDpcbiAgICAgICAgICAgICAgICB0aGlzLmlzTG9zc0ZvckhhbmQxO1xuXG4gICAgfVxuXG4gICAgZ2V0IGlzRGVhbGVyTG9zcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVhbGVyLmlzQnVzdGVkIHx8XG4gICAgICAgICAgICAoXG4gICAgICAgICAgICAgICAgdGhpcy5pc1NwbGl0ID9cbiAgICAgICAgICAgICAgICAoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNXaW5Gb3JIYW5kMSAmJlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzV2luRm9ySGFuZDJcbiAgICAgICAgICAgICAgICApIDpcbiAgICAgICAgICAgICAgICB0aGlzLmlzV2luRm9ySGFuZDFcbiAgICAgICAgICAgICk7XG4gICAgfVxuXG4gICAgZ2V0IGFtb3VudFdvbjEoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhbmQxUmVzdWx0ID9cbiAgICAgICAgICAgICAgICB0aGlzLmhhbmQxUmVzdWx0LmFtb3VudFdvbiA6XG4gICAgICAgICAgICAgICAgMDtcbiAgICB9XG4gICAgZ2V0IGFtb3VudFdvbjIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhbmQyUmVzdWx0ID9cbiAgICAgICAgICAgICAgICB0aGlzLmhhbmQyUmVzdWx0LmFtb3VudFdvbiA6XG4gICAgICAgICAgICAgICAgMDtcbiAgICB9XG5cbiAgICBnZXQgaXNXaW5Gb3JIYW5kMSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaGFuZDFSZXN1bHQgJiYgdGhpcy5oYW5kMVJlc3VsdC5pc1dpbjtcbiAgICB9XG5cbiAgICBnZXQgc2hvd0xvc3NGb3JIYW5kMSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTcGxpdCA/XG4gICAgICAgICAgICAgICAgKFxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzR2FtZU92ZXIgP1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0xvc3NGb3JIYW5kMSAmJiB0aGlzLnNob3dSZXN1bHQgOlxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oYW5kMS5pc0J1c3RlZFxuICAgICAgICAgICAgICAgICkgOlxuICAgICAgICAgICAgICAgIHRoaXMuaXNMb3NzRm9ySGFuZDE7XG4gICAgfVxuXG4gICAgZ2V0IGlzTG9zc0ZvckhhbmQxKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5oYW5kMS5pc0J1c3RlZCB8fFxuICAgICAgICAgICAgICAgICh0aGlzLmhhbmQxUmVzdWx0ICYmIHRoaXMuaGFuZDFSZXN1bHQuaXNMb3NzKTtcbiAgICB9XG4gICAgZ2V0IGlzUHVzaEZvckhhbmQxKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5oYW5kMVJlc3VsdCAmJiB0aGlzLmhhbmQxUmVzdWx0LmlzUHVzaDtcbiAgICB9XG5cbiAgICBnZXQgaXNXaW5Gb3JIYW5kMigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaGFuZDJSZXN1bHQgJiYgdGhpcy5oYW5kMlJlc3VsdC5pc1dpbjtcbiAgICB9XG4gICAgZ2V0IGlzTG9zc0ZvckhhbmQyKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5oYW5kMi5pc0J1c3RlZCB8fFxuICAgICAgICAgICAgICAgICh0aGlzLmhhbmQyUmVzdWx0ICYmIHRoaXMuaGFuZDJSZXN1bHQuaXNMb3NzKTtcbiAgICB9XG4gICAgZ2V0IGlzUHVzaEZvckhhbmQyKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5oYW5kMlJlc3VsdCAmJiB0aGlzLmhhbmQyUmVzdWx0LmlzUHVzaDtcbiAgICB9XG5cbiAgICBnZXQgcHJvbXB0Rm9ySW5zdXJhbmNlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fcHJvbXB0Rm9ySW5zdXJhbmNlO1xuICAgIH1cbiAgICBnZXQgY2hvc2VJbnN1cmFuY2UoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhbmQxLmJvdWdodEluc3VyYW5jZTtcbiAgICB9XG5cbiAgICBnZXQgc2hvd1Jlc3VsdCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3Nob3dSZXN1bHQ7XG4gICAgfVxufVxuIl19