/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/blackjack-hands.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { nameOfRank, sleep, CardRankNumber } from "earnbet-common";
import { CardController } from '../../../../_common/game/card-controller';
import { BetAmount } from './bet-amount';
import { BustedResult, PushResult, BlackjackResult, InsuranceSuccessResult, LossResult, WinResult } from './hand-result';
var BlackjackHand = /** @class */ (function () {
    function BlackjackHand(soundPlayer) {
        this.soundPlayer = soundPlayer;
        this._cards = [];
        this._numOfAces = 0;
        this.countWithoutAces = 0;
        this._isAllCardsKnown = false;
        this._isFirstCardAnAce = false;
        this.isSplit = false;
    }
    /**
     * @return {?}
     */
    BlackjackHand.prototype.reset = /**
     * @return {?}
     */
    function () {
        this._cards = [];
        this._numOfAces = 0;
        this.countWithoutAces = 0;
        this._isAllCardsKnown = false;
        this._isFirstCardAnAce = false;
        this.isSplit = false;
    };
    /**
     * @param {?} cardId
     * @return {?}
     */
    BlackjackHand.prototype.hit = /**
     * @param {?} cardId
     * @return {?}
     */
    function (cardId) {
        return this.dealCard(cardId, true);
    };
    /**
     * @protected
     * @param {?} cardId
     * @param {?} shouldReveal
     * @param {?=} revealImmediately
     * @return {?}
     */
    BlackjackHand.prototype.dealCard = /**
     * @protected
     * @param {?} cardId
     * @param {?} shouldReveal
     * @param {?=} revealImmediately
     * @return {?}
     */
    function (cardId, shouldReveal, revealImmediately) {
        if (revealImmediately === void 0) { revealImmediately = false; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var card, rankNumber, isAce;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        card = new CardController(this.soundPlayer);
                        this.cards.push(card);
                        rankNumber = cardId % 13;
                        isAce = rankNumber == CardRankNumber.ACE;
                        card.reveal({
                            suit: Math.floor(cardId / 13),
                            rankName: nameOfRank(rankNumber)
                        }, rankNumber, cardId);
                        if (!!revealImmediately) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep(250)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (shouldReveal) {
                            card.dealAndFlip();
                        }
                        else {
                            card.deal();
                        }
                        if (!!revealImmediately) return [3 /*break*/, 4];
                        return [4 /*yield*/, sleep(250)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        if (shouldReveal) {
                            this.addValueOfCard(rankNumber, isAce);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} rankNumber
     * @param {?} isAce
     * @return {?}
     */
    BlackjackHand.prototype.addValueOfCard = /**
     * @private
     * @param {?} rankNumber
     * @param {?} isAce
     * @return {?}
     */
    function (rankNumber, isAce) {
        /** @type {?} */
        var valueOfCard = 0;
        switch (rankNumber) {
            case CardRankNumber.JACK:
            case CardRankNumber.QUEEN:
            case CardRankNumber.KING:
                valueOfCard = 10;
                break;
            case CardRankNumber.ACE:
                // will determine ace value later
                break;
            default:
                valueOfCard = rankNumber;
                break;
        }
        this.countWithoutAces += valueOfCard;
        if (isAce) {
            this._numOfAces++;
            if (this.numOfCards == 1) {
                this._isFirstCardAnAce = true;
            }
        }
    };
    Object.defineProperty(BlackjackHand.prototype, "isBusted", {
        get: /**
         * @return {?}
         */
        function () {
            return this.sum > 21;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "is21", {
        get: /**
         * @return {?}
         */
        function () {
            return this.sum == 21;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "sumAsString", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var sum = '' + this.sum;
            if (this.isSoftCount) {
                sum += '/' +
                    (this.countWithoutAces + this.numOfAces);
            }
            ;
            return sum;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "sum", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var sum = this.countWithoutAces + this.countOfAces;
            if (this.isBlackJack) {
                sum = 21;
            }
            return sum;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "countOfAces", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.isSoftCount) {
                /** @type {?} */
                var largerCount = 11 + (this.numOfAces - 1);
                if (largerCount + this.countWithoutAces <=
                    21) {
                    return largerCount;
                }
            }
            return this.numOfAces;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "isSoftCount", {
        get: /**
         * @return {?}
         */
        function () {
            return !this.isBlackJack &&
                this.numOfAces > 0 &&
                (this.countWithoutAces +
                    11 +
                    (this.numOfAces - 1)) <= 21;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "isBlackJack", {
        get: /**
         * @return {?}
         */
        function () {
            return !this.isSplit &&
                this.numOfCards == 2 &&
                this.numOfAces == 1 &&
                this.countWithoutAces == 10;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "isFirstCardAnAce", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isFirstCardAnAce;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "numOfAces", {
        get: /**
         * @return {?}
         */
        function () {
            return this._numOfAces;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "numOfCards", {
        get: /**
         * @return {?}
         */
        function () {
            return this.cards.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "isAllCardsKnown", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isAllCardsKnown;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackHand.prototype, "cards", {
        get: /**
         * @return {?}
         */
        function () {
            return this._cards;
        },
        enumerable: true,
        configurable: true
    });
    return BlackjackHand;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype._cards;
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype._numOfAces;
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype.countWithoutAces;
    /**
     * @type {?}
     * @protected
     */
    BlackjackHand.prototype._isAllCardsKnown;
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype._isFirstCardAnAce;
    /**
     * @type {?}
     * @protected
     */
    BlackjackHand.prototype.isSplit;
    /**
     * @type {?}
     * @private
     */
    BlackjackHand.prototype.soundPlayer;
}
var BlackjackDealerHand = /** @class */ (function (_super) {
    tslib_1.__extends(BlackjackDealerHand, _super);
    function BlackjackDealerHand() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    BlackjackDealerHand.prototype.dealFirstCard = /**
     * @param {?} cardId
     * @return {?}
     */
    function (cardId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.dealCard(cardId, true)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackDealerHand.prototype.dealAnonymousCard = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    // deal anonymous card for dealer's second card (at first)
                    return [4 /*yield*/, this.dealCard(0, false)];
                    case 1:
                        // deal anonymous card for dealer's second card (at first)
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} cardId
     * @return {?}
     */
    BlackjackDealerHand.prototype.revealSecondCard = /**
     * @param {?} cardId
     * @return {?}
     */
    function (cardId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.numOfCards > 1) {
                            this.cards.pop();
                        }
                        return [4 /*yield*/, this.dealCard(cardId, true)];
                    case 1:
                        _a.sent();
                        this._isAllCardsKnown = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} cardId
     * @return {?}
     */
    BlackjackDealerHand.prototype.dealAdditionalCard = /**
     * @param {?} cardId
     * @return {?}
     */
    function (cardId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.shouldHit) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.hit(cardId)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    Object.defineProperty(BlackjackDealerHand.prototype, "shouldHit", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.isSoftCount ?
                this.sum <= 17 :
                this.sum < 17;
        },
        enumerable: true,
        configurable: true
    });
    return BlackjackDealerHand;
}(BlackjackHand));
export { BlackjackDealerHand };
var BlackjackPlayerHand = /** @class */ (function (_super) {
    tslib_1.__extends(BlackjackPlayerHand, _super);
    function BlackjackPlayerHand(soundPlayer) {
        var _this = _super.call(this, soundPlayer) || this;
        _this.didDoubleDown = false;
        _this._isWaitingForAction = false;
        _this.bet = new BetAmount();
        _this._boughtInsurance = false;
        return _this;
    }
    /**
     * @return {?}
     */
    BlackjackPlayerHand.prototype.reset = /**
     * @return {?}
     */
    function () {
        _super.prototype.reset.call(this);
        this.didDoubleDown = false;
        this._isWaitingForAction = false;
        this.bet.reset();
        this._boughtInsurance = false;
    };
    /**
     * @param {?} cardId
     * @param {?} betAmount
     * @return {?}
     */
    BlackjackPlayerHand.prototype.dealFirstCard = /**
     * @param {?} cardId
     * @param {?} betAmount
     * @return {?}
     */
    function (cardId, betAmount) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.bet.add(betAmount);
                        return [4 /*yield*/, this.hit(cardId)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} cardId
     * @return {?}
     */
    BlackjackPlayerHand.prototype.dealSecondCard = /**
     * @param {?} cardId
     * @return {?}
     */
    function (cardId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.hit(cardId)];
                    case 1:
                        _a.sent();
                        this._isAllCardsKnown = true;
                        if (!this.isBlackJack) {
                            this._isWaitingForAction = true;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackPlayerHand.prototype.buyInsurance = /**
     * @return {?}
     */
    function () {
        /*
        this.bet.add(
            this.bet.amount / 2
        );
        */
        this._boughtInsurance = true;
    };
    /**
     * @param {?} cards
     * @param {?} betAmount
     * @return {?}
     */
    BlackjackPlayerHand.prototype.split = /**
     * @param {?} cards
     * @param {?} betAmount
     * @return {?}
     */
    function (cards, betAmount) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isFirstCardAnAce;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.reset();
                        this.isSplit = true;
                        this.bet.add(betAmount);
                        return [4 /*yield*/, this.dealCard(cards[0], true, true)];
                    case 1:
                        _a.sent();
                        isFirstCardAnAce = this.numOfAces == 1;
                        return [4 /*yield*/, this.hit(cards[1])];
                    case 2:
                        _a.sent();
                        if (isFirstCardAnAce) {
                            this._isWaitingForAction = false;
                        }
                        this._isAllCardsKnown = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} cardId
     * @return {?}
     */
    BlackjackPlayerHand.prototype.doubleDown = /**
     * @param {?} cardId
     * @return {?}
     */
    function (cardId) {
        this.didDoubleDown = true;
        this.bet.double();
        return this.hit(cardId, true);
    };
    /**
     * @param {?} cardId
     * @param {?=} isDoubleDown
     * @return {?}
     */
    BlackjackPlayerHand.prototype.hit = /**
     * @param {?} cardId
     * @param {?=} isDoubleDown
     * @return {?}
     */
    function (cardId, isDoubleDown) {
        if (isDoubleDown === void 0) { isDoubleDown = false; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this._isWaitingForAction = false;
                        return [4 /*yield*/, _super.prototype.hit.call(this, cardId)];
                    case 1:
                        _a.sent();
                        if (this.numOfCards >= 2 &&
                            !this.isBusted &&
                            !isDoubleDown) {
                            this._isWaitingForAction = true;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    BlackjackPlayerHand.prototype.stand = /**
     * @return {?}
     */
    function () {
        this._isWaitingForAction = false;
    };
    /**
     * @param {?} dealer
     * @return {?}
     */
    BlackjackPlayerHand.prototype.determineResult = /**
     * @param {?} dealer
     * @return {?}
     */
    function (dealer) {
        /** @type {?} */
        var betAmount = this.bet.amount;
        /** @type {?} */
        var result;
        if (this.isBusted) {
            result = new BustedResult(betAmount);
        }
        else if (this.isBlackJack) {
            result = dealer.isBlackJack ?
                new PushResult(betAmount) :
                new BlackjackResult(betAmount);
        }
        else if (dealer.isBlackJack) {
            result = this.boughtInsurance ?
                new InsuranceSuccessResult(betAmount) :
                new LossResult(betAmount);
        }
        else if (dealer.isBusted) {
            result = new WinResult(betAmount);
        }
        else if (this.sum == dealer.sum) {
            result = new PushResult(betAmount);
        }
        else {
            result = this.sum > dealer.sum ?
                new WinResult(betAmount) :
                new LossResult(betAmount);
        }
        //this.bet.add(result.amountWon);
        return result;
    };
    Object.defineProperty(BlackjackPlayerHand.prototype, "isAbleToDoubleDown", {
        get: /**
         * @return {?}
         */
        function () {
            return this.numOfCards == 2 &&
                !this.didDoubleDown;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackPlayerHand.prototype, "isAbleToSplit", {
        get: /**
         * @return {?}
         */
        function () {
            return this.numOfCards == 2 &&
                this.cards[0].card ==
                    this.cards[1].card;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackPlayerHand.prototype, "isWaitingForAction", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isWaitingForAction;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackPlayerHand.prototype, "boughtInsurance", {
        get: /**
         * @return {?}
         */
        function () {
            return this._boughtInsurance;
        },
        enumerable: true,
        configurable: true
    });
    return BlackjackPlayerHand;
}(BlackjackHand));
export { BlackjackPlayerHand };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackPlayerHand.prototype.didDoubleDown;
    /**
     * @type {?}
     * @private
     */
    BlackjackPlayerHand.prototype._isWaitingForAction;
    /** @type {?} */
    BlackjackPlayerHand.prototype.bet;
    /**
     * @type {?}
     * @private
     */
    BlackjackPlayerHand.prototype._boughtInsurance;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWhhbmRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svY29tcG9uZW50cy9tYWluL2dhbWUvYmxhY2tqYWNrLWhhbmRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRW5FLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3pDLE9BQU8sRUFBb0IsWUFBWSxFQUFFLFVBQVUsRUFBRSxlQUFlLEVBQUUsc0JBQXNCLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkzSTtJQVNJLHVCQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVBwQyxXQUFNLEdBQW9CLEVBQUUsQ0FBQztRQUM3QixlQUFVLEdBQUcsQ0FBQyxDQUFDO1FBQ2YscUJBQWdCLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLHFCQUFnQixHQUFHLEtBQUssQ0FBQztRQUMzQixzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDeEIsWUFBTyxHQUFHLEtBQUssQ0FBQztJQUcxQixDQUFDOzs7O0lBRUQsNkJBQUs7OztJQUFMO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQzs7Ozs7SUFFRCwyQkFBRzs7OztJQUFILFVBQUksTUFBYTtRQUNiLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Ozs7Ozs7SUFFZSxnQ0FBUTs7Ozs7OztJQUF4QixVQUNJLE1BQWEsRUFBQyxZQUFvQixFQUNsQyxpQkFBeUI7UUFBekIsa0NBQUEsRUFBQSx5QkFBeUI7Ozs7Ozt3QkFFbkIsSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7d0JBQ2pELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUVoQixVQUFVLEdBQUcsTUFBTSxHQUFHLEVBQUU7d0JBRXhCLEtBQUssR0FBRyxVQUFVLElBQUksY0FBYyxDQUFDLEdBQUc7d0JBRTlDLElBQUksQ0FBQyxNQUFNLENBQ1A7NEJBQ0ksSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQzs0QkFDN0IsUUFBUSxFQUFFLFVBQVUsQ0FBQyxVQUFVLENBQUM7eUJBQ25DLEVBQ0QsVUFBVSxFQUNWLE1BQU0sQ0FDVCxDQUFDOzZCQUVFLENBQUMsaUJBQWlCLEVBQWxCLHdCQUFrQjt3QkFDbEIscUJBQU0sS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFBOzt3QkFBaEIsU0FBZ0IsQ0FBQzs7O3dCQUdyQixJQUFJLFlBQVksRUFBRTs0QkFDZCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7eUJBQ3RCOzZCQUFNOzRCQUNILElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzt5QkFDZjs2QkFFRyxDQUFDLGlCQUFpQixFQUFsQix3QkFBa0I7d0JBQ2xCLHFCQUFNLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBQTs7d0JBQWhCLFNBQWdCLENBQUM7Ozt3QkFHckIsSUFBSSxZQUFZLEVBQUU7NEJBQ2QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUMsS0FBSyxDQUFDLENBQUM7eUJBQ3pDOzs7OztLQUNKOzs7Ozs7O0lBQ08sc0NBQWM7Ozs7OztJQUF0QixVQUF1QixVQUFpQixFQUFDLEtBQWE7O1lBQzlDLFdBQVcsR0FBRyxDQUFDO1FBRW5CLFFBQVEsVUFBVSxFQUFFO1lBQ2hCLEtBQUssY0FBYyxDQUFDLElBQUksQ0FBQztZQUN6QixLQUFLLGNBQWMsQ0FBQyxLQUFLLENBQUM7WUFDMUIsS0FBSyxjQUFjLENBQUMsSUFBSTtnQkFDcEIsV0FBVyxHQUFHLEVBQUUsQ0FBQztnQkFDakIsTUFBTTtZQUVWLEtBQUssY0FBYyxDQUFDLEdBQUc7Z0JBQ25CLGlDQUFpQztnQkFDakMsTUFBTTtZQUVWO2dCQUNJLFdBQVcsR0FBRyxVQUFVLENBQUM7Z0JBQ3pCLE1BQU07U0FDYjtRQUVELElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxXQUFXLENBQUM7UUFFckMsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFFbEIsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsRUFBRTtnQkFDdEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQzthQUNqQztTQUNKO0lBQ0wsQ0FBQztJQUVELHNCQUFJLG1DQUFROzs7O1FBQVo7WUFDSSxPQUFPLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksK0JBQUk7Ozs7UUFBUjtZQUNJLE9BQU8sSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUM7UUFDMUIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxzQ0FBVzs7OztRQUFmOztnQkFDUSxHQUFHLEdBQUcsRUFBRSxHQUFDLElBQUksQ0FBQyxHQUFHO1lBRXJCLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDbEIsR0FBRyxJQUFLLEdBQUc7b0JBQ1AsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ2hEO1lBQUEsQ0FBQztZQUVGLE9BQU8sR0FBRyxDQUFDO1FBQ2YsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw4QkFBRzs7OztRQUFQOztnQkFDUSxHQUFHLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXO1lBRWxELElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDbEIsR0FBRyxHQUFHLEVBQUUsQ0FBQzthQUNaO1lBRUQsT0FBTyxHQUFHLENBQUM7UUFDZixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHNDQUFXOzs7O1FBQWY7WUFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7O29CQUNaLFdBQVcsR0FBRyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztnQkFFN0MsSUFDSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGdCQUFnQjtvQkFDbkMsRUFBRSxFQUNKO29CQUNFLE9BQU8sV0FBVyxDQUFDO2lCQUN0QjthQUNKO1lBRUQsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksc0NBQVc7Ozs7UUFBZjtZQUNJLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVztnQkFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDO2dCQUN0QixDQUNJLElBQUksQ0FBQyxnQkFBZ0I7b0JBQ3JCLEVBQUU7b0JBQ0YsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUN2QixJQUFJLEVBQUUsQ0FBQztRQUNaLENBQUM7OztPQUFBO0lBRUQsc0JBQUksc0NBQVc7Ozs7UUFBZjtZQUNJLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTztnQkFDWixJQUFJLENBQUMsVUFBVSxJQUFJLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQztnQkFDbkIsSUFBSSxDQUFDLGdCQUFnQixJQUFJLEVBQUUsQ0FBQztRQUN4QyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDJDQUFnQjs7OztRQUFwQjtZQUNJLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ2xDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksb0NBQVM7Ozs7UUFBYjtZQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUMzQixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHFDQUFVOzs7O1FBQWQ7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQzdCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMENBQWU7Ozs7UUFBbkI7WUFDSSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztRQUNqQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGdDQUFLOzs7O1FBQVQ7WUFDSSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkIsQ0FBQzs7O09BQUE7SUFDTCxvQkFBQztBQUFELENBQUMsQUE3S0QsSUE2S0M7Ozs7OztJQTNLRywrQkFBcUM7Ozs7O0lBQ3JDLG1DQUF1Qjs7Ozs7SUFDdkIseUNBQTZCOzs7OztJQUM3Qix5Q0FBbUM7Ozs7O0lBQ25DLDBDQUFrQzs7Ozs7SUFDbEMsZ0NBQTBCOzs7OztJQUVkLG9DQUFnQzs7QUFzS2hEO0lBQXlDLCtDQUFhO0lBQXREOztJQWdDQSxDQUFDOzs7OztJQTlCUywyQ0FBYTs7OztJQUFuQixVQUFvQixNQUFhOzs7OzRCQUM3QixxQkFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsRUFBQTs7d0JBQWhDLFNBQWdDLENBQUM7Ozs7O0tBQ3BDOzs7O0lBRUssK0NBQWlCOzs7SUFBdkI7Ozs7O29CQUNJLDBEQUEwRDtvQkFDMUQscUJBQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUMsS0FBSyxDQUFDLEVBQUE7O3dCQUQ1QiwwREFBMEQ7d0JBQzFELFNBQTRCLENBQUM7Ozs7O0tBQ2hDOzs7OztJQUVLLDhDQUFnQjs7OztJQUF0QixVQUF1QixNQUFhOzs7Ozt3QkFDaEMsSUFBSSxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsRUFBRTs0QkFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQzt5QkFDcEI7d0JBRUQscUJBQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLEVBQUE7O3dCQUFoQyxTQUFnQyxDQUFDO3dCQUVqQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDOzs7OztLQUNoQzs7Ozs7SUFFSyxnREFBa0I7Ozs7SUFBeEIsVUFBeUIsTUFBYTs7Ozs7NkJBQzlCLElBQUksQ0FBQyxTQUFTLEVBQWQsd0JBQWM7d0JBQ2QscUJBQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQXRCLFNBQXNCLENBQUM7Ozs7OztLQUU5QjtJQUVELHNCQUFZLDBDQUFTOzs7OztRQUFyQjtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNqQixJQUFJLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2dCQUNoQixJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUMxQixDQUFDOzs7T0FBQTtJQUNMLDBCQUFDO0FBQUQsQ0FBQyxBQWhDRCxDQUF5QyxhQUFhLEdBZ0NyRDs7QUFFRDtJQUF5QywrQ0FBYTtJQU9sRCw2QkFBWSxXQUF3QjtRQUFwQyxZQUNJLGtCQUFNLFdBQVcsQ0FBQyxTQUNyQjtRQVBPLG1CQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLHlCQUFtQixHQUFHLEtBQUssQ0FBQztRQUMzQixTQUFHLEdBQUcsSUFBSSxTQUFTLEVBQUUsQ0FBQztRQUN2QixzQkFBZ0IsR0FBRyxLQUFLLENBQUM7O0lBSWpDLENBQUM7Ozs7SUFFRCxtQ0FBSzs7O0lBQUw7UUFDSSxpQkFBTSxLQUFLLFdBQUUsQ0FBQztRQUVkLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDakMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO0lBQ2xDLENBQUM7Ozs7OztJQUVLLDJDQUFhOzs7OztJQUFuQixVQUFvQixNQUFhLEVBQUMsU0FBZ0I7Ozs7O3dCQUM5QyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFFeEIscUJBQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQXRCLFNBQXNCLENBQUM7Ozs7O0tBQzFCOzs7OztJQUNLLDRDQUFjOzs7O0lBQXBCLFVBQXFCLE1BQWE7Ozs7NEJBQzlCLHFCQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUF0QixTQUFzQixDQUFDO3dCQUV2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO3dCQUU3QixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTs0QkFDbkIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQzt5QkFDbkM7Ozs7O0tBQ0o7Ozs7SUFFRCwwQ0FBWTs7O0lBQVo7UUFDSTs7OztVQUlFO1FBQ0YsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztJQUNqQyxDQUFDOzs7Ozs7SUFFSyxtQ0FBSzs7Ozs7SUFBWCxVQUFZLEtBQWMsRUFBQyxTQUFnQjs7Ozs7O3dCQUN2QyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBRWIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7d0JBRXBCLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUV4QixxQkFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBQyxJQUFJLEVBQUMsSUFBSSxDQUFDLEVBQUE7O3dCQUF2QyxTQUF1QyxDQUFDO3dCQUVsQyxnQkFBZ0IsR0FDbEIsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDO3dCQUV2QixxQkFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFBOzt3QkFBeEIsU0FBd0IsQ0FBQzt3QkFFekIsSUFBSSxnQkFBZ0IsRUFBRTs0QkFDbEIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQzt5QkFDcEM7d0JBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQzs7Ozs7S0FDaEM7Ozs7O0lBR0Qsd0NBQVU7Ozs7SUFBVixVQUFXLE1BQWE7UUFDcEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUVsQixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7OztJQUVLLGlDQUFHOzs7OztJQUFULFVBQVUsTUFBYSxFQUFDLFlBQW9CO1FBQXBCLDZCQUFBLEVBQUEsb0JBQW9COzs7Ozt3QkFDeEMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQzt3QkFFakMscUJBQU0saUJBQU0sR0FBRyxZQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBdkIsU0FBdUIsQ0FBQzt3QkFFeEIsSUFDSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUM7NEJBQ3BCLENBQUMsSUFBSSxDQUFDLFFBQVE7NEJBQ2QsQ0FBQyxZQUFZLEVBQ2Y7NEJBQ0UsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQzt5QkFDbkM7Ozs7O0tBQ0o7Ozs7SUFFRCxtQ0FBSzs7O0lBQUw7UUFDSSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO0lBQ3JDLENBQUM7Ozs7O0lBRUQsNkNBQWU7Ozs7SUFBZixVQUFnQixNQUEwQjs7WUFDaEMsU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTTs7WUFFN0IsTUFBdUI7UUFFM0IsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsTUFBTSxHQUFHLElBQUksWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3hDO2FBQ0ksSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3ZCLE1BQU0sR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3JCLElBQUksVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNCLElBQUksZUFBZSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzFDO2FBQ0ksSUFBSSxNQUFNLENBQUMsV0FBVyxFQUFFO1lBQ3pCLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQ3ZCLElBQUksc0JBQXNCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDdkMsSUFBSSxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDckM7YUFDSSxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDdEIsTUFBTSxHQUFHLElBQUksU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3JDO2FBQ0ksSUFBSSxJQUFJLENBQUMsR0FBRyxJQUFJLE1BQU0sQ0FBQyxHQUFHLEVBQUU7WUFDN0IsTUFBTSxHQUFHLElBQUksVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3RDO2FBQ0k7WUFDRCxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzVCLElBQUksU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLElBQUksVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ2pDO1FBRUQsaUNBQWlDO1FBRWpDLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFHRCxzQkFBSSxtREFBa0I7Ozs7UUFBdEI7WUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQztnQkFDbkIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO1FBQ2hDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksOENBQWE7Ozs7UUFBakI7WUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQztnQkFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO29CQUNsQixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMvQixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLG1EQUFrQjs7OztRQUF0QjtZQUNJLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBQ3BDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksZ0RBQWU7Ozs7UUFBbkI7WUFDSSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztRQUNqQyxDQUFDOzs7T0FBQTtJQUNMLDBCQUFDO0FBQUQsQ0FBQyxBQWpKRCxDQUF5QyxhQUFhLEdBaUpyRDs7Ozs7OztJQS9JRyw0Q0FBOEI7Ozs7O0lBQzlCLGtEQUFvQzs7SUFDcEMsa0NBQStCOzs7OztJQUMvQiwrQ0FBaUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBuYW1lT2ZSYW5rLCBzbGVlcCwgQ2FyZFJhbmtOdW1iZXIgfSBmcm9tIFwiZWFybmJldC1jb21tb25cIjtcblxuaW1wb3J0IHsgQ2FyZENvbnRyb2xsZXIgfSBmcm9tICcuLi8uLi8uLi8uLi9fY29tbW9uL2dhbWUvY2FyZC1jb250cm9sbGVyJztcbmltcG9ydCB7IEJldEFtb3VudCB9IGZyb20gJy4vYmV0LWFtb3VudCc7XG5pbXBvcnQgeyBJQmxhY2tqYWNrUmVzdWx0LCBCdXN0ZWRSZXN1bHQsIFB1c2hSZXN1bHQsIEJsYWNramFja1Jlc3VsdCwgSW5zdXJhbmNlU3VjY2Vzc1Jlc3VsdCwgTG9zc1Jlc3VsdCwgV2luUmVzdWx0IH0gZnJvbSAnLi9oYW5kLXJlc3VsdCc7XG5pbXBvcnQgeyBJU291bmRQbGF5ZXIgfSBmcm9tICcuLi8uLi8uLi8uLi9fY29tbW9uL21lZGlhL2ludGVyZmFjZXMnO1xuXG5cbmNsYXNzIEJsYWNramFja0hhbmRcbntcbiAgICBwcml2YXRlIF9jYXJkczpDYXJkQ29udHJvbGxlcltdID0gW107XG4gICAgcHJpdmF0ZSBfbnVtT2ZBY2VzID0gMDtcbiAgICBwcml2YXRlIGNvdW50V2l0aG91dEFjZXMgPSAwO1xuICAgIHByb3RlY3RlZCBfaXNBbGxDYXJkc0tub3duID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBfaXNGaXJzdENhcmRBbkFjZSA9IGZhbHNlO1xuICAgIHByb3RlY3RlZCBpc1NwbGl0ID0gZmFsc2U7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNvdW5kUGxheWVyOklTb3VuZFBsYXllcikge1xuICAgIH1cblxuICAgIHJlc2V0KCkge1xuICAgICAgICB0aGlzLl9jYXJkcyA9IFtdO1xuICAgICAgICB0aGlzLl9udW1PZkFjZXMgPSAwO1xuICAgICAgICB0aGlzLmNvdW50V2l0aG91dEFjZXMgPSAwO1xuICAgICAgICB0aGlzLl9pc0FsbENhcmRzS25vd24gPSBmYWxzZTtcbiAgICAgICAgdGhpcy5faXNGaXJzdENhcmRBbkFjZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmlzU3BsaXQgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBoaXQoY2FyZElkOm51bWJlcikge1xuICAgICAgICByZXR1cm4gdGhpcy5kZWFsQ2FyZChjYXJkSWQsdHJ1ZSk7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGFzeW5jIGRlYWxDYXJkKFxuICAgICAgICBjYXJkSWQ6bnVtYmVyLHNob3VsZFJldmVhbDpib29sZWFuLFxuICAgICAgICByZXZlYWxJbW1lZGlhdGVseSA9IGZhbHNlXG4gICAgKSB7XG4gICAgICAgIGNvbnN0IGNhcmQgPSBuZXcgQ2FyZENvbnRyb2xsZXIodGhpcy5zb3VuZFBsYXllcik7XG4gICAgICAgIHRoaXMuY2FyZHMucHVzaChjYXJkKTtcblxuICAgICAgICBjb25zdCByYW5rTnVtYmVyID0gY2FyZElkICUgMTM7XG5cbiAgICAgICAgY29uc3QgaXNBY2UgPSByYW5rTnVtYmVyID09IENhcmRSYW5rTnVtYmVyLkFDRTtcblxuICAgICAgICBjYXJkLnJldmVhbChcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzdWl0OiBNYXRoLmZsb29yKGNhcmRJZCAvIDEzKSxcbiAgICAgICAgICAgICAgICByYW5rTmFtZTogbmFtZU9mUmFuayhyYW5rTnVtYmVyKVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHJhbmtOdW1iZXIsXG4gICAgICAgICAgICBjYXJkSWRcbiAgICAgICAgKTtcblxuICAgICAgICBpZiAoIXJldmVhbEltbWVkaWF0ZWx5KSB7XG4gICAgICAgICAgICBhd2FpdCBzbGVlcCgyNTApO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHNob3VsZFJldmVhbCkge1xuICAgICAgICAgICAgY2FyZC5kZWFsQW5kRmxpcCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2FyZC5kZWFsKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIXJldmVhbEltbWVkaWF0ZWx5KSB7XG4gICAgICAgICAgICBhd2FpdCBzbGVlcCgyNTApO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHNob3VsZFJldmVhbCkge1xuICAgICAgICAgICAgdGhpcy5hZGRWYWx1ZU9mQ2FyZChyYW5rTnVtYmVyLGlzQWNlKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBwcml2YXRlIGFkZFZhbHVlT2ZDYXJkKHJhbmtOdW1iZXI6bnVtYmVyLGlzQWNlOmJvb2xlYW4pIHtcbiAgICAgICAgbGV0IHZhbHVlT2ZDYXJkID0gMDtcblxuICAgICAgICBzd2l0Y2ggKHJhbmtOdW1iZXIpIHtcbiAgICAgICAgICAgIGNhc2UgQ2FyZFJhbmtOdW1iZXIuSkFDSzpcbiAgICAgICAgICAgIGNhc2UgQ2FyZFJhbmtOdW1iZXIuUVVFRU46XG4gICAgICAgICAgICBjYXNlIENhcmRSYW5rTnVtYmVyLktJTkc6XG4gICAgICAgICAgICAgICAgdmFsdWVPZkNhcmQgPSAxMDtcbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBDYXJkUmFua051bWJlci5BQ0U6XG4gICAgICAgICAgICAgICAgLy8gd2lsbCBkZXRlcm1pbmUgYWNlIHZhbHVlIGxhdGVyXG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgdmFsdWVPZkNhcmQgPSByYW5rTnVtYmVyO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5jb3VudFdpdGhvdXRBY2VzICs9IHZhbHVlT2ZDYXJkO1xuXG4gICAgICAgIGlmIChpc0FjZSkge1xuICAgICAgICAgICAgdGhpcy5fbnVtT2ZBY2VzKys7XG5cbiAgICAgICAgICAgIGlmICh0aGlzLm51bU9mQ2FyZHMgPT0gMSkge1xuICAgICAgICAgICAgICAgIHRoaXMuX2lzRmlyc3RDYXJkQW5BY2UgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0IGlzQnVzdGVkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdW0gPiAyMTtcbiAgICB9XG5cbiAgICBnZXQgaXMyMSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3VtID09IDIxO1xuICAgIH1cblxuICAgIGdldCBzdW1Bc1N0cmluZygpOnN0cmluZyB7XG4gICAgICAgIGxldCBzdW0gPSAnJyt0aGlzLnN1bTtcblxuICAgICAgICBpZiAodGhpcy5pc1NvZnRDb3VudCkge1xuICAgICAgICAgICAgc3VtICs9ICAnLycgK1xuICAgICAgICAgICAgICAgICh0aGlzLmNvdW50V2l0aG91dEFjZXMgKyB0aGlzLm51bU9mQWNlcyk7XG4gICAgICAgIH07XG5cbiAgICAgICAgcmV0dXJuIHN1bTtcbiAgICB9XG5cbiAgICBnZXQgc3VtKCk6bnVtYmVyIHtcbiAgICAgICAgbGV0IHN1bSA9IHRoaXMuY291bnRXaXRob3V0QWNlcyArIHRoaXMuY291bnRPZkFjZXM7XG5cbiAgICAgICAgaWYgKHRoaXMuaXNCbGFja0phY2spIHtcbiAgICAgICAgICAgIHN1bSA9IDIxO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHN1bTtcbiAgICB9XG5cbiAgICBnZXQgY291bnRPZkFjZXMoKTpudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5pc1NvZnRDb3VudCkge1xuICAgICAgICAgICAgY29uc3QgbGFyZ2VyQ291bnQgPSAxMSArICh0aGlzLm51bU9mQWNlcyAtIDEpO1xuXG4gICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgbGFyZ2VyQ291bnQgKyB0aGlzLmNvdW50V2l0aG91dEFjZXMgPD1cbiAgICAgICAgICAgICAgICAyMVxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGxhcmdlckNvdW50O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMubnVtT2ZBY2VzO1xuICAgIH1cblxuICAgIGdldCBpc1NvZnRDb3VudCgpIHtcbiAgICAgICAgcmV0dXJuICF0aGlzLmlzQmxhY2tKYWNrICYmXG4gICAgICAgICAgICB0aGlzLm51bU9mQWNlcyA+IDAgJiZcbiAgICAgICAgKFxuICAgICAgICAgICAgdGhpcy5jb3VudFdpdGhvdXRBY2VzICtcbiAgICAgICAgICAgIDExICtcbiAgICAgICAgICAgICh0aGlzLm51bU9mQWNlcyAtIDEpXG4gICAgICAgICkgPD0gMjE7XG4gICAgfVxuXG4gICAgZ2V0IGlzQmxhY2tKYWNrKCkge1xuICAgICAgICByZXR1cm4gIXRoaXMuaXNTcGxpdCAmJlxuICAgICAgICAgICAgICAgIHRoaXMubnVtT2ZDYXJkcyA9PSAyICYmXG4gICAgICAgICAgICAgICAgdGhpcy5udW1PZkFjZXMgPT0gMSAmJlxuICAgICAgICAgICAgICAgIHRoaXMuY291bnRXaXRob3V0QWNlcyA9PSAxMDtcbiAgICB9XG5cbiAgICBnZXQgaXNGaXJzdENhcmRBbkFjZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzRmlyc3RDYXJkQW5BY2U7XG4gICAgfVxuXG4gICAgZ2V0IG51bU9mQWNlcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX251bU9mQWNlcztcbiAgICB9XG5cbiAgICBnZXQgbnVtT2ZDYXJkcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZHMubGVuZ3RoO1xuICAgIH1cblxuICAgIGdldCBpc0FsbENhcmRzS25vd24oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc0FsbENhcmRzS25vd247XG4gICAgfVxuXG4gICAgZ2V0IGNhcmRzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fY2FyZHM7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgQmxhY2tqYWNrRGVhbGVySGFuZCBleHRlbmRzIEJsYWNramFja0hhbmRcbntcbiAgICBhc3luYyBkZWFsRmlyc3RDYXJkKGNhcmRJZDpudW1iZXIpIHtcbiAgICAgICAgYXdhaXQgdGhpcy5kZWFsQ2FyZChjYXJkSWQsdHJ1ZSk7XG4gICAgfVxuXG4gICAgYXN5bmMgZGVhbEFub255bW91c0NhcmQoKSB7XG4gICAgICAgIC8vIGRlYWwgYW5vbnltb3VzIGNhcmQgZm9yIGRlYWxlcidzIHNlY29uZCBjYXJkIChhdCBmaXJzdClcbiAgICAgICAgYXdhaXQgdGhpcy5kZWFsQ2FyZCgwLGZhbHNlKTtcbiAgICB9XG5cbiAgICBhc3luYyByZXZlYWxTZWNvbmRDYXJkKGNhcmRJZDpudW1iZXIpIHtcbiAgICAgICAgaWYgKHRoaXMubnVtT2ZDYXJkcyA+IDEpIHtcbiAgICAgICAgICAgIHRoaXMuY2FyZHMucG9wKCk7XG4gICAgICAgIH1cblxuICAgICAgICBhd2FpdCB0aGlzLmRlYWxDYXJkKGNhcmRJZCx0cnVlKTtcblxuICAgICAgICB0aGlzLl9pc0FsbENhcmRzS25vd24gPSB0cnVlO1xuICAgIH1cblxuICAgIGFzeW5jIGRlYWxBZGRpdGlvbmFsQ2FyZChjYXJkSWQ6bnVtYmVyKSB7XG4gICAgICAgIGlmICh0aGlzLnNob3VsZEhpdCkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5oaXQoY2FyZElkKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0IHNob3VsZEhpdCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTb2Z0Q291bnQgP1xuICAgICAgICAgICAgICAgIHRoaXMuc3VtIDw9IDE3IDpcbiAgICAgICAgICAgICAgICB0aGlzLnN1bSA8IDE3O1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEJsYWNramFja1BsYXllckhhbmQgZXh0ZW5kcyBCbGFja2phY2tIYW5kXG57XG4gICAgcHJpdmF0ZSBkaWREb3VibGVEb3duID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBfaXNXYWl0aW5nRm9yQWN0aW9uID0gZmFsc2U7XG4gICAgcmVhZG9ubHkgYmV0ID0gbmV3IEJldEFtb3VudCgpO1xuICAgIHByaXZhdGUgX2JvdWdodEluc3VyYW5jZSA9IGZhbHNlO1xuXG4gICAgY29uc3RydWN0b3Ioc291bmRQbGF5ZXI6SVNvdW5kUGxheWVyKSB7XG4gICAgICAgIHN1cGVyKHNvdW5kUGxheWVyKTtcbiAgICB9XG5cbiAgICByZXNldCgpIHtcbiAgICAgICAgc3VwZXIucmVzZXQoKTtcblxuICAgICAgICB0aGlzLmRpZERvdWJsZURvd24gPSBmYWxzZTtcbiAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yQWN0aW9uID0gZmFsc2U7XG4gICAgICAgIHRoaXMuYmV0LnJlc2V0KCk7XG4gICAgICAgIHRoaXMuX2JvdWdodEluc3VyYW5jZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIGFzeW5jIGRlYWxGaXJzdENhcmQoY2FyZElkOm51bWJlcixiZXRBbW91bnQ6bnVtYmVyKSB7XG4gICAgICAgIHRoaXMuYmV0LmFkZChiZXRBbW91bnQpO1xuXG4gICAgICAgIGF3YWl0IHRoaXMuaGl0KGNhcmRJZCk7XG4gICAgfVxuICAgIGFzeW5jIGRlYWxTZWNvbmRDYXJkKGNhcmRJZDpudW1iZXIpIHtcbiAgICAgICAgYXdhaXQgdGhpcy5oaXQoY2FyZElkKTtcblxuICAgICAgICB0aGlzLl9pc0FsbENhcmRzS25vd24gPSB0cnVlO1xuXG4gICAgICAgIGlmICghdGhpcy5pc0JsYWNrSmFjaykge1xuICAgICAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yQWN0aW9uID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGJ1eUluc3VyYW5jZSgpIHtcbiAgICAgICAgLypcbiAgICAgICAgdGhpcy5iZXQuYWRkKFxuICAgICAgICAgICAgdGhpcy5iZXQuYW1vdW50IC8gMlxuICAgICAgICApO1xuICAgICAgICAqL1xuICAgICAgICB0aGlzLl9ib3VnaHRJbnN1cmFuY2UgPSB0cnVlO1xuICAgIH1cblxuICAgIGFzeW5jIHNwbGl0KGNhcmRzOm51bWJlcltdLGJldEFtb3VudDpudW1iZXIpIHtcbiAgICAgICAgdGhpcy5yZXNldCgpO1xuXG4gICAgICAgIHRoaXMuaXNTcGxpdCA9IHRydWU7XG5cbiAgICAgICAgdGhpcy5iZXQuYWRkKGJldEFtb3VudCk7XG5cbiAgICAgICAgYXdhaXQgdGhpcy5kZWFsQ2FyZChjYXJkc1swXSx0cnVlLHRydWUpO1xuXG4gICAgICAgIGNvbnN0IGlzRmlyc3RDYXJkQW5BY2UgPVxuICAgICAgICAgICAgdGhpcy5udW1PZkFjZXMgPT0gMTtcblxuICAgICAgICBhd2FpdCB0aGlzLmhpdChjYXJkc1sxXSk7XG5cbiAgICAgICAgaWYgKGlzRmlyc3RDYXJkQW5BY2UpIHtcbiAgICAgICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvckFjdGlvbiA9IGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5faXNBbGxDYXJkc0tub3duID0gdHJ1ZTtcbiAgICB9XG5cblxuICAgIGRvdWJsZURvd24oY2FyZElkOm51bWJlcikge1xuICAgICAgICB0aGlzLmRpZERvdWJsZURvd24gPSB0cnVlO1xuICAgICAgICB0aGlzLmJldC5kb3VibGUoKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5oaXQoY2FyZElkLHRydWUpO1xuICAgIH1cblxuICAgIGFzeW5jIGhpdChjYXJkSWQ6bnVtYmVyLGlzRG91YmxlRG93biA9IGZhbHNlKSB7XG4gICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvckFjdGlvbiA9IGZhbHNlO1xuXG4gICAgICAgIGF3YWl0IHN1cGVyLmhpdChjYXJkSWQpO1xuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHRoaXMubnVtT2ZDYXJkcyA+PSAyICYmXG4gICAgICAgICAgICAhdGhpcy5pc0J1c3RlZCAmJlxuICAgICAgICAgICAgIWlzRG91YmxlRG93blxuICAgICAgICApIHtcbiAgICAgICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvckFjdGlvbiA9IHRydWU7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzdGFuZCgpIHtcbiAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yQWN0aW9uID0gZmFsc2U7XG4gICAgfVxuXG4gICAgZGV0ZXJtaW5lUmVzdWx0KGRlYWxlcjpCbGFja2phY2tEZWFsZXJIYW5kKTpJQmxhY2tqYWNrUmVzdWx0IHtcbiAgICAgICAgY29uc3QgYmV0QW1vdW50ID0gdGhpcy5iZXQuYW1vdW50O1xuXG4gICAgICAgIGxldCByZXN1bHQ6SUJsYWNramFja1Jlc3VsdDtcblxuICAgICAgICBpZiAodGhpcy5pc0J1c3RlZCkge1xuICAgICAgICAgICAgcmVzdWx0ID0gbmV3IEJ1c3RlZFJlc3VsdChiZXRBbW91bnQpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMuaXNCbGFja0phY2spIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IGRlYWxlci5pc0JsYWNrSmFjayA/XG4gICAgICAgICAgICAgICAgICAgIG5ldyBQdXNoUmVzdWx0KGJldEFtb3VudCkgOlxuICAgICAgICAgICAgICAgICAgICBuZXcgQmxhY2tqYWNrUmVzdWx0KGJldEFtb3VudCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoZGVhbGVyLmlzQmxhY2tKYWNrKSB7XG4gICAgICAgICAgICByZXN1bHQgPSB0aGlzLmJvdWdodEluc3VyYW5jZSA/XG4gICAgICAgICAgICAgICAgICAgIG5ldyBJbnN1cmFuY2VTdWNjZXNzUmVzdWx0KGJldEFtb3VudCkgOlxuICAgICAgICAgICAgICAgICAgICBuZXcgTG9zc1Jlc3VsdChiZXRBbW91bnQpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKGRlYWxlci5pc0J1c3RlZCkge1xuICAgICAgICAgICAgcmVzdWx0ID0gbmV3IFdpblJlc3VsdChiZXRBbW91bnQpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMuc3VtID09IGRlYWxlci5zdW0pIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IG5ldyBQdXNoUmVzdWx0KGJldEFtb3VudCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXN1bHQgPSB0aGlzLnN1bSA+IGRlYWxlci5zdW0gP1xuICAgICAgICAgICAgICAgIG5ldyBXaW5SZXN1bHQoYmV0QW1vdW50KSA6XG4gICAgICAgICAgICAgICAgbmV3IExvc3NSZXN1bHQoYmV0QW1vdW50KTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vdGhpcy5iZXQuYWRkKHJlc3VsdC5hbW91bnRXb24pO1xuXG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfVxuXG5cbiAgICBnZXQgaXNBYmxlVG9Eb3VibGVEb3duKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5udW1PZkNhcmRzID09IDIgJiZcbiAgICAgICAgICAgICAgICAhdGhpcy5kaWREb3VibGVEb3duO1xuICAgIH1cblxuICAgIGdldCBpc0FibGVUb1NwbGl0KCk6Ym9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLm51bU9mQ2FyZHMgPT0gMiAmJlxuICAgICAgICAgICAgICAgIHRoaXMuY2FyZHNbMF0uY2FyZCA9PVxuICAgICAgICAgICAgICAgIHRoaXMuY2FyZHNbMV0uY2FyZDtcbiAgICB9XG5cbiAgICBnZXQgaXNXYWl0aW5nRm9yQWN0aW9uKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faXNXYWl0aW5nRm9yQWN0aW9uO1xuICAgIH1cblxuICAgIGdldCBib3VnaHRJbnN1cmFuY2UoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9ib3VnaHRJbnN1cmFuY2U7XG4gICAgfVxufVxuIl19