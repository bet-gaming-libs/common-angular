/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/hand-result.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * @record
 */
export function IBlackjackResult() { }
if (false) {
    /** @type {?} */
    IBlackjackResult.prototype.isBlackjack;
    /** @type {?} */
    IBlackjackResult.prototype.isWin;
    /** @type {?} */
    IBlackjackResult.prototype.isPush;
    /** @type {?} */
    IBlackjackResult.prototype.isLoss;
    /** @type {?} */
    IBlackjackResult.prototype.isBusted;
    /** @type {?} */
    IBlackjackResult.prototype.isInsuranceSuccess;
    /** @type {?} */
    IBlackjackResult.prototype.value;
    /** @type {?} */
    IBlackjackResult.prototype.amountWon;
}
/**
 * @abstract
 */
var /**
 * @abstract
 */
HandResultBase = /** @class */ (function () {
    function HandResultBase(value, betAmount) {
        this.value = value;
        this.amountWon = value * betAmount;
    }
    Object.defineProperty(HandResultBase.prototype, "isBlackjack", {
        get: /**
         * @return {?}
         */
        function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HandResultBase.prototype, "isWin", {
        get: /**
         * @return {?}
         */
        function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HandResultBase.prototype, "isPush", {
        get: /**
         * @return {?}
         */
        function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HandResultBase.prototype, "isLoss", {
        get: /**
         * @return {?}
         */
        function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HandResultBase.prototype, "isBusted", {
        get: /**
         * @return {?}
         */
        function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HandResultBase.prototype, "isInsuranceSuccess", {
        get: /**
         * @return {?}
         */
        function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    return HandResultBase;
}());
if (false) {
    /** @type {?} */
    HandResultBase.prototype.amountWon;
    /** @type {?} */
    HandResultBase.prototype.value;
}
var BlackjackResult = /** @class */ (function (_super) {
    tslib_1.__extends(BlackjackResult, _super);
    function BlackjackResult(betAmount) {
        return _super.call(this, 1.5, betAmount) || this;
    }
    Object.defineProperty(BlackjackResult.prototype, "isBlackjack", {
        get: /**
         * @return {?}
         */
        function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackResult.prototype, "isWin", {
        get: /**
         * @return {?}
         */
        function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return BlackjackResult;
}(HandResultBase));
export { BlackjackResult };
var WinResult = /** @class */ (function (_super) {
    tslib_1.__extends(WinResult, _super);
    function WinResult(betAmount) {
        return _super.call(this, 1, betAmount) || this;
    }
    Object.defineProperty(WinResult.prototype, "isWin", {
        get: /**
         * @return {?}
         */
        function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return WinResult;
}(HandResultBase));
export { WinResult };
var PushResult = /** @class */ (function (_super) {
    tslib_1.__extends(PushResult, _super);
    function PushResult(betAmount) {
        return _super.call(this, 0, betAmount) || this;
    }
    Object.defineProperty(PushResult.prototype, "isPush", {
        get: /**
         * @return {?}
         */
        function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return PushResult;
}(HandResultBase));
export { PushResult };
var LossResult = /** @class */ (function (_super) {
    tslib_1.__extends(LossResult, _super);
    function LossResult(betAmount) {
        return _super.call(this, -1, betAmount) || this;
    }
    Object.defineProperty(LossResult.prototype, "isLoss", {
        get: /**
         * @return {?}
         */
        function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return LossResult;
}(HandResultBase));
export { LossResult };
var BustedResult = /** @class */ (function (_super) {
    tslib_1.__extends(BustedResult, _super);
    function BustedResult(betAmount) {
        return _super.call(this, -1, betAmount) || this;
    }
    Object.defineProperty(BustedResult.prototype, "isBusted", {
        get: /**
         * @return {?}
         */
        function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BustedResult.prototype, "isLoss", {
        get: /**
         * @return {?}
         */
        function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return BustedResult;
}(HandResultBase));
export { BustedResult };
var InsuranceSuccessResult = /** @class */ (function (_super) {
    tslib_1.__extends(InsuranceSuccessResult, _super);
    function InsuranceSuccessResult(betAmount) {
        return _super.call(this, 0, betAmount) || this;
    }
    Object.defineProperty(InsuranceSuccessResult.prototype, "isInsuranceSuccess", {
        get: /**
         * @return {?}
         */
        function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return InsuranceSuccessResult;
}(HandResultBase));
export { InsuranceSuccessResult };
var CombinedHandsResult = /** @class */ (function () {
    function CombinedHandsResult(result1, result2) {
        this.isInsuranceSuccess = false;
        this.value = result1.value + result2.value;
        this.amountWon = result1.amountWon + result2.amountWon;
        this.isPush = this.amountWon == 0;
        this.isBlackjack = this.value >= 1.5;
        this.isWin = !this.isBlackjack && this.value > 0;
        this.isBusted = result1.isBusted && result2.isBusted;
        this.isLoss = !this.isBusted && this.value < 0;
    }
    return CombinedHandsResult;
}());
export { CombinedHandsResult };
if (false) {
    /** @type {?} */
    CombinedHandsResult.prototype.isInsuranceSuccess;
    /** @type {?} */
    CombinedHandsResult.prototype.value;
    /** @type {?} */
    CombinedHandsResult.prototype.amountWon;
    /** @type {?} */
    CombinedHandsResult.prototype.isPush;
    /** @type {?} */
    CombinedHandsResult.prototype.isBlackjack;
    /** @type {?} */
    CombinedHandsResult.prototype.isWin;
    /** @type {?} */
    CombinedHandsResult.prototype.isBusted;
    /** @type {?} */
    CombinedHandsResult.prototype.isLoss;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGFuZC1yZXN1bHQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL2JsYWNramFjay9jb21wb25lbnRzL21haW4vZ2FtZS9oYW5kLXJlc3VsdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSxzQ0FXQzs7O0lBVEcsdUNBQW9COztJQUNwQixpQ0FBYzs7SUFDZCxrQ0FBZTs7SUFDZixrQ0FBZTs7SUFDZixvQ0FBaUI7O0lBQ2pCLDhDQUEyQjs7SUFFM0IsaUNBQWE7O0lBQ2IscUNBQWlCOzs7OztBQUlyQjs7OztJQUlJLHdCQUNhLEtBQVksRUFDckIsU0FBZ0I7UUFEUCxVQUFLLEdBQUwsS0FBSyxDQUFPO1FBR3JCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxHQUFHLFNBQVMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsc0JBQUksdUNBQVc7Ozs7UUFBZjtZQUNJLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7OztPQUFBO0lBQ0Qsc0JBQUksaUNBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7OztPQUFBO0lBQ0Qsc0JBQUksa0NBQU07Ozs7UUFBVjtZQUNJLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7OztPQUFBO0lBQ0Qsc0JBQUksa0NBQU07Ozs7UUFBVjtZQUNJLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7OztPQUFBO0lBQ0Qsc0JBQUksb0NBQVE7Ozs7UUFBWjtZQUNJLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7OztPQUFBO0lBQ0Qsc0JBQUksOENBQWtCOzs7O1FBQXRCO1lBQ0ksT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQzs7O09BQUE7SUFDTCxxQkFBQztBQUFELENBQUMsQUE3QkQsSUE2QkM7OztJQTNCRyxtQ0FBMEI7O0lBR3RCLCtCQUFxQjs7QUEwQjdCO0lBQXFDLDJDQUFjO0lBRS9DLHlCQUFZLFNBQWdCO2VBQ3hCLGtCQUFNLEdBQUcsRUFBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQUVELHNCQUFJLHdDQUFXOzs7O1FBQWY7WUFDSSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDOzs7T0FBQTtJQUNELHNCQUFJLGtDQUFLOzs7O1FBQVQ7WUFDSSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDOzs7T0FBQTtJQUNMLHNCQUFDO0FBQUQsQ0FBQyxBQVpELENBQXFDLGNBQWMsR0FZbEQ7O0FBRUQ7SUFBK0IscUNBQWM7SUFFekMsbUJBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sQ0FBQyxFQUFDLFNBQVMsQ0FBQztJQUN0QixDQUFDO0lBRUQsc0JBQUksNEJBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7OztPQUFBO0lBQ0wsZ0JBQUM7QUFBRCxDQUFDLEFBVEQsQ0FBK0IsY0FBYyxHQVM1Qzs7QUFFRDtJQUFnQyxzQ0FBYztJQUUxQyxvQkFBWSxTQUFnQjtlQUN4QixrQkFBTSxDQUFDLEVBQUMsU0FBUyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxzQkFBSSw4QkFBTTs7OztRQUFWO1lBQ0ksT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQzs7O09BQUE7SUFDTCxpQkFBQztBQUFELENBQUMsQUFURCxDQUFnQyxjQUFjLEdBUzdDOztBQUVEO0lBQWdDLHNDQUFjO0lBRTFDLG9CQUFZLFNBQWdCO2VBQ3hCLGtCQUFNLENBQUMsQ0FBQyxFQUFDLFNBQVMsQ0FBQztJQUN2QixDQUFDO0lBRUQsc0JBQUksOEJBQU07Ozs7UUFBVjtZQUNJLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7OztPQUFBO0lBQ0wsaUJBQUM7QUFBRCxDQUFDLEFBVEQsQ0FBZ0MsY0FBYyxHQVM3Qzs7QUFFRDtJQUFrQyx3Q0FBYztJQUU1QyxzQkFBWSxTQUFnQjtlQUN4QixrQkFBTSxDQUFDLENBQUMsRUFBQyxTQUFTLENBQUM7SUFDdkIsQ0FBQztJQUVELHNCQUFJLGtDQUFROzs7O1FBQVo7WUFDSSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDOzs7T0FBQTtJQUNELHNCQUFJLGdDQUFNOzs7O1FBQVY7WUFDSSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDOzs7T0FBQTtJQUNMLG1CQUFDO0FBQUQsQ0FBQyxBQVpELENBQWtDLGNBQWMsR0FZL0M7O0FBRUQ7SUFBNEMsa0RBQWM7SUFFdEQsZ0NBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sQ0FBQyxFQUFDLFNBQVMsQ0FBQztJQUN0QixDQUFDO0lBRUQsc0JBQUksc0RBQWtCOzs7O1FBQXRCO1lBQ0ksT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQzs7O09BQUE7SUFDTCw2QkFBQztBQUFELENBQUMsQUFURCxDQUE0QyxjQUFjLEdBU3pEOztBQUVEO0lBYUksNkJBQ0ksT0FBd0IsRUFDeEIsT0FBd0I7UUFibkIsdUJBQWtCLEdBQUcsS0FBSyxDQUFDO1FBZWhDLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO1FBRTNDLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDO1FBRXZELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQztRQUNyQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQztRQUNyRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBQ0wsMEJBQUM7QUFBRCxDQUFDLEFBM0JELElBMkJDOzs7O0lBekJHLGlEQUFvQzs7SUFFcEMsb0NBQXNCOztJQUN0Qix3Q0FBMEI7O0lBRTFCLHFDQUF3Qjs7SUFDeEIsMENBQTZCOztJQUM3QixvQ0FBdUI7O0lBQ3ZCLHVDQUEwQjs7SUFDMUIscUNBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJQmxhY2tqYWNrUmVzdWx0XG57XG4gICAgaXNCbGFja2phY2s6Ym9vbGVhbjtcbiAgICBpc1dpbjpib29sZWFuO1xuICAgIGlzUHVzaDpib29sZWFuO1xuICAgIGlzTG9zczpib29sZWFuO1xuICAgIGlzQnVzdGVkOmJvb2xlYW47XG4gICAgaXNJbnN1cmFuY2VTdWNjZXNzOmJvb2xlYW47XG5cbiAgICB2YWx1ZTpudW1iZXI7XG4gICAgYW1vdW50V29uOm51bWJlcjtcbn1cblxuXG5hYnN0cmFjdCBjbGFzcyBIYW5kUmVzdWx0QmFzZVxue1xuICAgIHJlYWRvbmx5IGFtb3VudFdvbjpudW1iZXI7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcmVhZG9ubHkgdmFsdWU6bnVtYmVyLFxuICAgICAgICBiZXRBbW91bnQ6bnVtYmVyXG4gICAgKSB7XG4gICAgICAgIHRoaXMuYW1vdW50V29uID0gdmFsdWUgKiBiZXRBbW91bnQ7XG4gICAgfVxuXG4gICAgZ2V0IGlzQmxhY2tqYWNrKCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGdldCBpc1dpbigpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBnZXQgaXNQdXNoKCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGdldCBpc0xvc3MoKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgZ2V0IGlzQnVzdGVkKCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGdldCBpc0luc3VyYW5jZVN1Y2Nlc3MoKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBCbGFja2phY2tSZXN1bHQgZXh0ZW5kcyBIYW5kUmVzdWx0QmFzZSBpbXBsZW1lbnRzIElCbGFja2phY2tSZXN1bHRcbntcbiAgICBjb25zdHJ1Y3RvcihiZXRBbW91bnQ6bnVtYmVyKSB7XG4gICAgICAgIHN1cGVyKDEuNSxiZXRBbW91bnQpO1xuICAgIH1cblxuICAgIGdldCBpc0JsYWNramFjaygpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIGdldCBpc1dpbigpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgV2luUmVzdWx0IGV4dGVuZHMgSGFuZFJlc3VsdEJhc2UgaW1wbGVtZW50cyBJQmxhY2tqYWNrUmVzdWx0XG57XG4gICAgY29uc3RydWN0b3IoYmV0QW1vdW50Om51bWJlcikge1xuICAgICAgICBzdXBlcigxLGJldEFtb3VudCk7XG4gICAgfVxuXG4gICAgZ2V0IGlzV2luKCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBQdXNoUmVzdWx0IGV4dGVuZHMgSGFuZFJlc3VsdEJhc2UgaW1wbGVtZW50cyBJQmxhY2tqYWNrUmVzdWx0XG57XG4gICAgY29uc3RydWN0b3IoYmV0QW1vdW50Om51bWJlcikge1xuICAgICAgICBzdXBlcigwLGJldEFtb3VudCk7XG4gICAgfVxuXG4gICAgZ2V0IGlzUHVzaCgpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgTG9zc1Jlc3VsdCBleHRlbmRzIEhhbmRSZXN1bHRCYXNlIGltcGxlbWVudHMgSUJsYWNramFja1Jlc3VsdFxue1xuICAgIGNvbnN0cnVjdG9yKGJldEFtb3VudDpudW1iZXIpIHtcbiAgICAgICAgc3VwZXIoLTEsYmV0QW1vdW50KTtcbiAgICB9XG5cbiAgICBnZXQgaXNMb3NzKCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBCdXN0ZWRSZXN1bHQgZXh0ZW5kcyBIYW5kUmVzdWx0QmFzZSBpbXBsZW1lbnRzIElCbGFja2phY2tSZXN1bHRcbntcbiAgICBjb25zdHJ1Y3RvcihiZXRBbW91bnQ6bnVtYmVyKSB7XG4gICAgICAgIHN1cGVyKC0xLGJldEFtb3VudCk7XG4gICAgfVxuXG4gICAgZ2V0IGlzQnVzdGVkKCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgZ2V0IGlzTG9zcygpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgSW5zdXJhbmNlU3VjY2Vzc1Jlc3VsdCBleHRlbmRzIEhhbmRSZXN1bHRCYXNlIGltcGxlbWVudHMgSUJsYWNramFja1Jlc3VsdFxue1xuICAgIGNvbnN0cnVjdG9yKGJldEFtb3VudDpudW1iZXIpIHtcbiAgICAgICAgc3VwZXIoMCxiZXRBbW91bnQpO1xuICAgIH1cblxuICAgIGdldCBpc0luc3VyYW5jZVN1Y2Nlc3MoKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIENvbWJpbmVkSGFuZHNSZXN1bHQgaW1wbGVtZW50cyBJQmxhY2tqYWNrUmVzdWx0XG57XG4gICAgcmVhZG9ubHkgaXNJbnN1cmFuY2VTdWNjZXNzID0gZmFsc2U7XG5cbiAgICByZWFkb25seSB2YWx1ZTpudW1iZXI7XG4gICAgcmVhZG9ubHkgYW1vdW50V29uOm51bWJlcjtcblxuICAgIHJlYWRvbmx5IGlzUHVzaDpib29sZWFuO1xuICAgIHJlYWRvbmx5IGlzQmxhY2tqYWNrOmJvb2xlYW47XG4gICAgcmVhZG9ubHkgaXNXaW46Ym9vbGVhbjtcbiAgICByZWFkb25seSBpc0J1c3RlZDpib29sZWFuO1xuICAgIHJlYWRvbmx5IGlzTG9zczpib29sZWFuO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHJlc3VsdDE6SUJsYWNramFja1Jlc3VsdCxcbiAgICAgICAgcmVzdWx0MjpJQmxhY2tqYWNrUmVzdWx0XG4gICAgKSB7XG4gICAgICAgIHRoaXMudmFsdWUgPSByZXN1bHQxLnZhbHVlICsgcmVzdWx0Mi52YWx1ZTtcblxuICAgICAgICB0aGlzLmFtb3VudFdvbiA9IHJlc3VsdDEuYW1vdW50V29uICsgcmVzdWx0Mi5hbW91bnRXb247XG4gICAgICAgIFxuICAgICAgICB0aGlzLmlzUHVzaCA9IHRoaXMuYW1vdW50V29uID09IDA7XG4gICAgICAgIHRoaXMuaXNCbGFja2phY2sgPSB0aGlzLnZhbHVlID49IDEuNTtcbiAgICAgICAgdGhpcy5pc1dpbiA9ICF0aGlzLmlzQmxhY2tqYWNrICYmIHRoaXMudmFsdWUgPiAwO1xuICAgICAgICB0aGlzLmlzQnVzdGVkID0gcmVzdWx0MS5pc0J1c3RlZCAmJiByZXN1bHQyLmlzQnVzdGVkO1xuICAgICAgICB0aGlzLmlzTG9zcyA9ICF0aGlzLmlzQnVzdGVkICYmIHRoaXMudmFsdWUgPCAwO1xuICAgIH1cbn0iXX0=