/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/bj-client.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { SocketMessageManager, BlackjackMessage } from 'earnbet-common';
import { SocketClientBase } from 'earnbet-common-front-end';
import { BlackjackBetResult } from '../../_common/models/bets/blackjack-bet';
/** @type {?} */
var NAMESPACE = '/blackjack';
var BlackjackClient = /** @class */ (function (_super) {
    tslib_1.__extends(BlackjackClient, _super);
    function BlackjackClient(app) {
        var _this = _super.call(this, {
            host: 'bjs.eosbet.io',
            port: 443,
            ssl: true
        }, NAMESPACE) || this;
        _this.app = app;
        _this._isConnected = false;
        return _this;
    }
    /**
     * @return {?}
     */
    BlackjackClient.prototype.connect = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var socket = this.connectSocket();
        this._messageManager = new BlackjackMessageManager(socket, this);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    BlackjackClient.prototype.setListener = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.listener = value;
    };
    /**
     * @protected
     * @return {?}
     */
    BlackjackClient.prototype.onConnected = /**
     * @protected
     * @return {?}
     */
    function () {
        this._isConnected = true;
        console.log('blackjack client connected to Blackjack Server!');
        if (this.listener) {
            this.listener.onBlackjackClientConnected();
        }
    };
    /*** MESSAGE LISTENERS: ***/
    /**
     * MESSAGE LISTENERS: **
     * @param {?} data
     * @return {?}
     */
    BlackjackClient.prototype.onResolveBetResult = /**
     * MESSAGE LISTENERS: **
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.app.state.bets.onBetResolved(new BlackjackBetResult(data));
    };
    /***/
    /**
     *
     * @protected
     * @return {?}
     */
    BlackjackClient.prototype.onDisconnected = /**
     *
     * @protected
     * @return {?}
     */
    function () {
        this._isConnected = false;
    };
    Object.defineProperty(BlackjackClient.prototype, "messageManager", {
        get: /**
         * @return {?}
         */
        function () {
            return this._messageManager;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BlackjackClient.prototype, "isConnected", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isConnected;
        },
        enumerable: true,
        configurable: true
    });
    return BlackjackClient;
}(SocketClientBase));
export { BlackjackClient };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BlackjackClient.prototype._isConnected;
    /**
     * @type {?}
     * @private
     */
    BlackjackClient.prototype._messageManager;
    /**
     * @type {?}
     * @private
     */
    BlackjackClient.prototype.listener;
    /**
     * @type {?}
     * @private
     */
    BlackjackClient.prototype.app;
}
var BlackjackMessageManager = /** @class */ (function (_super) {
    tslib_1.__extends(BlackjackMessageManager, _super);
    function BlackjackMessageManager(socket, client) {
        return _super.call(this, socket, BlackjackMessage.PREFIX, client) || this;
    }
    /**
     * @template T
     * @param {?} data
     * @param {?} info
     * @return {?}
     */
    BlackjackMessageManager.prototype.sendGameCommand = /**
     * @template T
     * @param {?} data
     * @param {?} info
     * @return {?}
     */
    function (data, info) {
        return this.makeRequest(BlackjackMessage.GAME_COMMAND, data, info);
    };
    return BlackjackMessageManager;
}(SocketMessageManager));
export { BlackjackMessageManager };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmotY2xpZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svc2VydmVyL2JqLWNsaWVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQUUsb0JBQW9CLEVBQUMsZ0JBQWdCLEVBQWtELE1BQU0sZ0JBQWdCLENBQUM7QUFDdkgsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFJMUQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUNBQXlDLENBQUM7O0lBSXZFLFNBQVMsR0FBRyxZQUFZO0FBRTlCO0lBQXFDLDJDQUFnQjtJQU9qRCx5QkFBb0IsR0FBbUI7UUFBdkMsWUFDSSxrQkFDSTtZQUNJLElBQUksRUFBRSxlQUFlO1lBQ3JCLElBQUksRUFBRSxHQUFHO1lBQ1QsR0FBRyxFQUFFLElBQUk7U0FDWixFQUNELFNBQVMsQ0FDWixTQUNKO1FBVG1CLFNBQUcsR0FBSCxHQUFHLENBQWdCO1FBTC9CLGtCQUFZLEdBQUcsS0FBSyxDQUFDOztJQWM3QixDQUFDOzs7O0lBRUQsaUNBQU87OztJQUFQOztZQUNRLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFO1FBQ2pDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSx1QkFBdUIsQ0FDOUMsTUFBTSxFQUFDLElBQUksQ0FDZCxDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFRCxxQ0FBVzs7OztJQUFYLFVBQVksS0FBOEI7UUFDdEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFUyxxQ0FBVzs7OztJQUFyQjtRQUNJLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBRXpCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaURBQWlELENBQUMsQ0FBQztRQUUvRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDZixJQUFJLENBQUMsUUFBUSxDQUFDLDBCQUEwQixFQUFFLENBQUM7U0FDOUM7SUFDTCxDQUFDO0lBSUQsNEJBQTRCOzs7Ozs7SUFDNUIsNENBQWtCOzs7OztJQUFsQixVQUFtQixJQUEwQjtRQUN6QyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUM3QixJQUFJLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUMvQixDQUFDO0lBQ04sQ0FBQztJQUNELEtBQUs7Ozs7OztJQUtLLHdDQUFjOzs7OztJQUF4QjtRQUNJLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO0lBQzlCLENBQUM7SUFHRCxzQkFBSSwyQ0FBYzs7OztRQUFsQjtZQUNJLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUNoQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHdDQUFXOzs7O1FBQWY7WUFDSSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDN0IsQ0FBQzs7O09BQUE7SUFDTCxzQkFBQztBQUFELENBQUMsQUFoRUQsQ0FBcUMsZ0JBQWdCLEdBZ0VwRDs7Ozs7OztJQTlERyx1Q0FBNkI7Ozs7O0lBRTdCLDBDQUFnRDs7Ozs7SUFDaEQsbUNBQTBDOzs7OztJQUU5Qiw4QkFBMkI7O0FBNEQzQztJQUE2QyxtREFBb0I7SUFFN0QsaUNBQ0ksTUFBNEIsRUFDNUIsTUFBc0I7ZUFFdEIsa0JBQU0sTUFBTSxFQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBQyxNQUFNLENBQUM7SUFDaEQsQ0FBQzs7Ozs7OztJQUVELGlEQUFlOzs7Ozs7SUFBZixVQUNJLElBQVcsRUFBQyxJQUFtQjtRQUUvQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQ25CLGdCQUFnQixDQUFDLFlBQVksRUFDN0IsSUFBSSxFQUNKLElBQUksQ0FDUCxDQUFDO0lBQ04sQ0FBQztJQUNMLDhCQUFDO0FBQUQsQ0FBQyxBQWxCRCxDQUE2QyxvQkFBb0IsR0FrQmhFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU29ja2V0TWVzc2FnZU1hbmFnZXIsQmxhY2tqYWNrTWVzc2FnZSwgSVNpZ25hdHVyZUluZm8sIElSZXNvbHZlZEJsYWNramFja0JldCxJUHJvbWlzZSB9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcbmltcG9ydCB7U29ja2V0Q2xpZW50QmFzZX0gZnJvbSAnZWFybmJldC1jb21tb24tZnJvbnQtZW5kJztcblxuaW1wb3J0IHsgSUJsYWNramFja0NsaWVudExpc3RlbmVyIH0gZnJvbSAnLi4vc2VydmljZXMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJTWFpbkFwcENvbnRleHQgfSBmcm9tICcuLi8uLi9fY29tbW9uL3NlcnZpY2VzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgQmxhY2tqYWNrQmV0UmVzdWx0IH0gZnJvbSAnLi4vLi4vX2NvbW1vbi9tb2RlbHMvYmV0cy9ibGFja2phY2stYmV0JztcblxuXG5cbmNvbnN0IE5BTUVTUEFDRSA9ICcvYmxhY2tqYWNrJztcblxuZXhwb3J0IGNsYXNzIEJsYWNramFja0NsaWVudCBleHRlbmRzIFNvY2tldENsaWVudEJhc2VcbntcbiAgICBwcml2YXRlIF9pc0Nvbm5lY3RlZCA9IGZhbHNlO1xuXG4gICAgcHJpdmF0ZSBfbWVzc2FnZU1hbmFnZXI6QmxhY2tqYWNrTWVzc2FnZU1hbmFnZXI7XG4gICAgcHJpdmF0ZSBsaXN0ZW5lcjpJQmxhY2tqYWNrQ2xpZW50TGlzdGVuZXI7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwcDpJTWFpbkFwcENvbnRleHQpIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaG9zdDogJ2Jqcy5lb3NiZXQuaW8nLFxuICAgICAgICAgICAgICAgIHBvcnQ6IDQ0MyxcbiAgICAgICAgICAgICAgICBzc2w6IHRydWVcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBOQU1FU1BBQ0VcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBjb25uZWN0KCkge1xuICAgICAgICB2YXIgc29ja2V0ID0gdGhpcy5jb25uZWN0U29ja2V0KCk7XG4gICAgICAgIHRoaXMuX21lc3NhZ2VNYW5hZ2VyID0gbmV3IEJsYWNramFja01lc3NhZ2VNYW5hZ2VyKFxuICAgICAgICAgICAgc29ja2V0LHRoaXNcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBzZXRMaXN0ZW5lcih2YWx1ZTpJQmxhY2tqYWNrQ2xpZW50TGlzdGVuZXIpIHtcbiAgICAgICAgdGhpcy5saXN0ZW5lciA9IHZhbHVlO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBvbkNvbm5lY3RlZCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5faXNDb25uZWN0ZWQgPSB0cnVlO1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdibGFja2phY2sgY2xpZW50IGNvbm5lY3RlZCB0byBCbGFja2phY2sgU2VydmVyIScpO1xuXG4gICAgICAgIGlmICh0aGlzLmxpc3RlbmVyKSB7XG4gICAgICAgICAgICB0aGlzLmxpc3RlbmVyLm9uQmxhY2tqYWNrQ2xpZW50Q29ubmVjdGVkKCk7XG4gICAgICAgIH1cbiAgICB9XG5cblxuXG4gICAgLyoqKiBNRVNTQUdFIExJU1RFTkVSUzogKioqL1xuICAgIG9uUmVzb2x2ZUJldFJlc3VsdChkYXRhOklSZXNvbHZlZEJsYWNramFja0JldCkge1xuICAgICAgICB0aGlzLmFwcC5zdGF0ZS5iZXRzLm9uQmV0UmVzb2x2ZWQoXG4gICAgICAgICAgICBuZXcgQmxhY2tqYWNrQmV0UmVzdWx0KGRhdGEpXG4gICAgICAgICk7XG4gICAgfVxuICAgIC8qKiovXG5cblxuXG5cbiAgICBwcm90ZWN0ZWQgb25EaXNjb25uZWN0ZWQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuX2lzQ29ubmVjdGVkID0gZmFsc2U7XG4gICAgfVxuXG5cbiAgICBnZXQgbWVzc2FnZU1hbmFnZXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9tZXNzYWdlTWFuYWdlcjtcbiAgICB9XG5cbiAgICBnZXQgaXNDb25uZWN0ZWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc0Nvbm5lY3RlZDtcbiAgICB9XG59XG5cblxuZXhwb3J0IGNsYXNzIEJsYWNramFja01lc3NhZ2VNYW5hZ2VyIGV4dGVuZHMgU29ja2V0TWVzc2FnZU1hbmFnZXJcbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgc29ja2V0OlNvY2tldElPQ2xpZW50LlNvY2tldCxcbiAgICAgICAgY2xpZW50OkJsYWNramFja0NsaWVudFxuICAgICkge1xuICAgICAgICBzdXBlcihzb2NrZXQsQmxhY2tqYWNrTWVzc2FnZS5QUkVGSVgsY2xpZW50KTtcbiAgICB9XG5cbiAgICBzZW5kR2FtZUNvbW1hbmQ8VD4oXG4gICAgICAgIGRhdGE6c3RyaW5nLGluZm86SVNpZ25hdHVyZUluZm9cbiAgICApOklQcm9taXNlPFQ+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMubWFrZVJlcXVlc3Q8VD4oXG4gICAgICAgICAgICBCbGFja2phY2tNZXNzYWdlLkdBTUVfQ09NTUFORCxcbiAgICAgICAgICAgIGRhdGEsXG4gICAgICAgICAgICBpbmZvXG4gICAgICAgICk7XG4gICAgfVxufVxuIl19