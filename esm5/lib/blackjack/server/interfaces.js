/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IBlackjackServer() { }
if (false) {
    /** @type {?} */
    IBlackjackServer.prototype.isWaitingForResponse;
    /**
     * @return {?}
     */
    IBlackjackServer.prototype.resumeExistingGame = function () { };
    /**
     * @param {?} betAmount
     * @return {?}
     */
    IBlackjackServer.prototype.placeBet = function (betAmount) { };
    /**
     * @param {?} yes
     * @return {?}
     */
    IBlackjackServer.prototype.respondToInsurance = function (yes) { };
    /**
     * @return {?}
     */
    IBlackjackServer.prototype.peekForBlackjack = function () { };
    /**
     * @return {?}
     */
    IBlackjackServer.prototype.split = function () { };
    /**
     * @param {?} handIndex
     * @return {?}
     */
    IBlackjackServer.prototype.hit = function (handIndex) { };
    /**
     * @param {?} handIndex
     * @return {?}
     */
    IBlackjackServer.prototype.doubleDown = function (handIndex) { };
    /**
     * @return {?}
     */
    IBlackjackServer.prototype.stand = function () { };
    /**
     * @return {?}
     */
    IBlackjackServer.prototype.getDealersSecondCard = function () { };
    /**
     * @return {?}
     */
    IBlackjackServer.prototype.getDealersAdditionalCards = function () { };
}
/**
 * @record
 */
export function IJGameBlackjackGameInfo() { }
if (false) {
    /** @type {?} */
    IJGameBlackjackGameInfo.prototype.status;
    /** @type {?} */
    IJGameBlackjackGameInfo.prototype.data;
}
/**
 * @record
 */
export function IRamGameBlackjack() { }
if (false) {
    /** @type {?} */
    IRamGameBlackjack.prototype.id;
    /** @type {?} */
    IRamGameBlackjack.prototype.bettor;
    /** @type {?} */
    IRamGameBlackjack.prototype.is_ezeos;
    /** @type {?} */
    IRamGameBlackjack.prototype.deposit_amt;
    /** @type {?} */
    IRamGameBlackjack.prototype.bet_amt;
    /** @type {?} */
    IRamGameBlackjack.prototype.seed;
    /** @type {?} */
    IRamGameBlackjack.prototype.bet_time;
}
/**
 * @record
 */
export function IJBlackjackRoundInfo() { }
if (false) {
    /** @type {?} */
    IJBlackjackRoundInfo.prototype.status;
    /** @type {?} */
    IJBlackjackRoundInfo.prototype.data;
}
/**
 * @record
 */
export function IRoundInfo() { }
if (false) {
    /** @type {?} */
    IRoundInfo.prototype.round_id;
    /** @type {?} */
    IRoundInfo.prototype.action_id;
}
/**
 * @record
 */
export function IJBlackjackState() { }
if (false) {
    /** @type {?} */
    IJBlackjackState.prototype.status;
    /** @type {?} */
    IJBlackjackState.prototype.data;
}
/**
 * @record
 */
export function IBlackjackState() { }
if (false) {
    /** @type {?} */
    IBlackjackState.prototype.activeHand;
    /** @type {?} */
    IBlackjackState.prototype.result;
    /** @type {?} */
    IBlackjackState.prototype.playerCards;
    /** @type {?} */
    IBlackjackState.prototype.playerValue;
    /** @type {?} */
    IBlackjackState.prototype.dealerCards;
    /** @type {?} */
    IBlackjackState.prototype.dealerValue;
    /** @type {?} */
    IBlackjackState.prototype.wagers;
    /** @type {?} */
    IBlackjackState.prototype.deposits;
    /** @type {?} */
    IBlackjackState.prototype.initWager;
    /** @type {?} */
    IBlackjackState.prototype.insuraceWagers;
    /** @type {?} */
    IBlackjackState.prototype.isComplete;
    /** @type {?|undefined} */
    IBlackjackState.prototype.payout;
    /** @type {?|undefined} */
    IBlackjackState.prototype.netSettlement;
    /** @type {?|undefined} */
    IBlackjackState.prototype.wagersTotal;
}
/**
 * @record
 */
export function CardValue() { }
if (false) {
    /** @type {?} */
    CardValue.prototype.val;
    /** @type {?} */
    CardValue.prototype.isSoft;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL3NlcnZlci9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBSUEsc0NBaUJDOzs7SUFmRyxnREFBc0M7Ozs7SUFFdEMsZ0VBQTBCOzs7OztJQUMxQiwrREFBd0M7Ozs7O0lBRXhDLG1FQUFpRDs7OztJQUNqRCw4REFBb0M7Ozs7SUFFcEMsbURBQTBCOzs7OztJQUMxQiwwREFBc0M7Ozs7O0lBQ3RDLGlFQUE2Qzs7OztJQUM3QyxtREFBeUI7Ozs7SUFFekIsa0VBQXVDOzs7O0lBQ3ZDLHVFQUE4Qzs7Ozs7QUFLbEQsNkNBR0M7OztJQUZHLHlDQUFjOztJQUNkLHVDQUF3Qjs7Ozs7QUFFNUIsdUNBUUM7OztJQVBHLCtCQUFXOztJQUNYLG1DQUFlOztJQUNmLHFDQUFpQjs7SUFDakIsd0NBQW9COztJQUNwQixvQ0FBZ0I7O0lBQ2hCLGlDQUFhOztJQUNiLHFDQUFpQjs7Ozs7QUFHckIsMENBR0M7OztJQUZHLHNDQUFjOztJQUNkLG9DQUFtQjs7Ozs7QUFHdkIsZ0NBQW1FOzs7SUFBckMsOEJBQWlCOztJQUFDLCtCQUFpQjs7Ozs7QUFFakUsc0NBTUM7OztJQUxHLGtDQUFjOztJQUNkLGdDQUdFOzs7OztBQUdOLHFDQWVDOzs7SUFkRyxxQ0FBbUI7O0lBQ25CLGlDQUFpQjs7SUFDakIsc0NBQTZCOztJQUM3QixzQ0FBeUI7O0lBQ3pCLHNDQUFzQjs7SUFDdEIsc0NBQXVCOztJQUN2QixpQ0FBaUI7O0lBQ2pCLG1DQUFtQjs7SUFDbkIsb0NBQW9COztJQUNwQix5Q0FBeUI7O0lBQ3pCLHFDQUFvQjs7SUFDcEIsaUNBQWtCOztJQUNsQix3Q0FBdUI7O0lBQ3ZCLHNDQUFxQjs7Ozs7QUFHekIsK0JBR0M7OztJQUZHLHdCQUFZOztJQUNaLDJCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEVPU0Fzc2V0QW1vdW50IH0gZnJvbSBcImVhcm5iZXQtY29tbW9uXCI7XG5cblxuXG5leHBvcnQgaW50ZXJmYWNlIElCbGFja2phY2tTZXJ2ZXJcbntcbiAgICByZWFkb25seSBpc1dhaXRpbmdGb3JSZXNwb25zZTpib29sZWFuO1xuXG4gICAgcmVzdW1lRXhpc3RpbmdHYW1lKCk6dm9pZDtcbiAgICBwbGFjZUJldChiZXRBbW91bnQ6RU9TQXNzZXRBbW91bnQpOnZvaWQ7XG5cbiAgICByZXNwb25kVG9JbnN1cmFuY2UoeWVzOmJvb2xlYW4pOlByb21pc2U8Ym9vbGVhbj47XG4gICAgcGVla0ZvckJsYWNramFjaygpOlByb21pc2U8Ym9vbGVhbj47XG4gICAgXG4gICAgc3BsaXQoKTpQcm9taXNlPG51bWJlcltdPjtcbiAgICBoaXQoaGFuZEluZGV4Om51bWJlcik6UHJvbWlzZTxudW1iZXI+O1xuICAgIGRvdWJsZURvd24oaGFuZEluZGV4Om51bWJlcik6UHJvbWlzZTxudW1iZXI+O1xuICAgIHN0YW5kKCk6UHJvbWlzZTxib29sZWFuPjtcblxuICAgIGdldERlYWxlcnNTZWNvbmRDYXJkKCk6UHJvbWlzZTxudW1iZXI+O1xuICAgIGdldERlYWxlcnNBZGRpdGlvbmFsQ2FyZHMoKTpQcm9taXNlPG51bWJlcltdPjtcbn1cblxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUpHYW1lQmxhY2tqYWNrR2FtZUluZm8ge1xuICAgIHN0YXR1czogc3RyaW5nXG4gICAgZGF0YTogSVJhbUdhbWVCbGFja2phY2s7XG59XG5leHBvcnQgaW50ZXJmYWNlIElSYW1HYW1lQmxhY2tqYWNrIHtcbiAgICBpZDogc3RyaW5nO1xuICAgIGJldHRvcjogc3RyaW5nO1xuICAgIGlzX2V6ZW9zOiBudW1iZXI7XG4gICAgZGVwb3NpdF9hbXQ6IHN0cmluZztcbiAgICBiZXRfYW10OiBudW1iZXI7XG4gICAgc2VlZDogc3RyaW5nO1xuICAgIGJldF90aW1lOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUpCbGFja2phY2tSb3VuZEluZm8ge1xuICAgIHN0YXR1czogc3RyaW5nXG4gICAgZGF0YTogSVJvdW5kSW5mb1tdO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElSb3VuZEluZm8geyByb3VuZF9pZDogbnVtYmVyLCBhY3Rpb25faWQ6IG51bWJlciB9XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUpCbGFja2phY2tTdGF0ZSB7XG4gICAgc3RhdHVzOiBzdHJpbmdcbiAgICBkYXRhOiB7XG4gICAgICAgIGJsYWNramFjazogSUJsYWNramFja1N0YXRlLFxuICAgICAgICBqYWNrcG90X3NwaW46IHN0cmluZyxcbiAgICB9O1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElCbGFja2phY2tTdGF0ZSB7XG4gICAgYWN0aXZlSGFuZDogbnVtYmVyO1xuICAgIHJlc3VsdDogc3RyaW5nW107XG4gICAgcGxheWVyQ2FyZHM6IEFycmF5PHN0cmluZ1tdPjtcbiAgICBwbGF5ZXJWYWx1ZTogQ2FyZFZhbHVlW107XG4gICAgZGVhbGVyQ2FyZHM6IHN0cmluZ1tdO1xuICAgIGRlYWxlclZhbHVlOiBDYXJkVmFsdWU7XG4gICAgd2FnZXJzOiBzdHJpbmdbXTtcbiAgICBkZXBvc2l0czogc3RyaW5nW107XG4gICAgaW5pdFdhZ2VyOiBzdHJpbmdbXTtcbiAgICBpbnN1cmFjZVdhZ2Vyczogc3RyaW5nW107XG4gICAgaXNDb21wbGV0ZTogYm9vbGVhbjtcbiAgICBwYXlvdXQ/OiBzdHJpbmdbXTtcbiAgICBuZXRTZXR0bGVtZW50Pzogc3RyaW5nO1xuICAgIHdhZ2Vyc1RvdGFsPzogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIENhcmRWYWx1ZSB7XG4gICAgdmFsOiBudW1iZXI7XG4gICAgaXNTb2Z0OiBib29sZWFuO1xufSJdfQ==