/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/local-server.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { BlackjackDealerHand } from '../components/main/game/blackjack-hands';
import { generateRandomCardId } from '../../_common/util/cards-util';
var LocalBlackjackServer = /** @class */ (function () {
    function LocalBlackjackServer(component, soundPlayer) {
        this.component = component;
        this.isWaitingForResponse = false;
        this.dealer = new BlackjackDealerHand(soundPlayer);
    }
    /**
     * @return {?}
     */
    LocalBlackjackServer.prototype.resumeExistingGame = /**
     * @return {?}
     */
    function () { };
    /**
     * @param {?} betAmount
     * @return {?}
     */
    LocalBlackjackServer.prototype.placeBet = /**
     * @param {?} betAmount
     * @return {?}
     */
    function (betAmount) {
        /** @type {?} */
        var dealersCard1 = generateRandomCardId();
        /** @type {?} */
        var dealersCard2 = generateRandomCardId();
        //const dealersCard1 = 1;
        //const dealersCard2 = 10;
        this.dealer.reset();
        this.dealer.dealFirstCard(dealersCard1);
        this.dealer.revealSecondCard(dealersCard2);
        /** @type {?} */
        var playersCard1 = generateRandomCardId();
        /** @type {?} */
        var playersCard2 = generateRandomCardId();
        //const playersCard1 = generateRandomCardId();
        //const playersCard2 = playersCard1;
        this.component.game.start({
            playerCards: [playersCard1, playersCard2],
            dealerCards: [dealersCard1]
        }, Number(betAmount.decimal));
    };
    /**
     * @param {?} yes
     * @return {?}
     */
    LocalBlackjackServer.prototype.respondToInsurance = /**
     * @param {?} yes
     * @return {?}
     */
    function (yes) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, this.dealer.isBlackJack];
            });
        });
    };
    /**
     * @return {?}
     */
    LocalBlackjackServer.prototype.peekForBlackjack = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, this.dealer.isBlackJack];
            });
        });
    };
    /**
     * @return {?}
     */
    LocalBlackjackServer.prototype.getDealersSecondCard = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, this.dealer.cards[1].cardId];
            });
        });
    };
    /**
     * @return {?}
     */
    LocalBlackjackServer.prototype.split = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, [
                        generateRandomCardId(),
                        generateRandomCardId()
                    ]];
            });
        });
    };
    /**
     * @return {?}
     */
    LocalBlackjackServer.prototype.doubleDown = /**
     * @return {?}
     */
    function () {
        return this.hit();
    };
    /**
     * @return {?}
     */
    LocalBlackjackServer.prototype.hit = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, generateRandomCardId()];
            });
        });
    };
    /**
     * @return {?}
     */
    LocalBlackjackServer.prototype.stand = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                // do nothing
                return [2 /*return*/, true];
            });
        });
    };
    /**
     * @return {?}
     */
    LocalBlackjackServer.prototype.getDealersAdditionalCards = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var cards, i;
            return tslib_1.__generator(this, function (_a) {
                cards = [];
                for (i = 0; i < 8; i++) {
                    cards.push(generateRandomCardId());
                }
                return [2 /*return*/, cards];
            });
        });
    };
    return LocalBlackjackServer;
}());
export { LocalBlackjackServer };
if (false) {
    /** @type {?} */
    LocalBlackjackServer.prototype.isWaitingForResponse;
    /**
     * @type {?}
     * @private
     */
    LocalBlackjackServer.prototype.dealer;
    /**
     * @type {?}
     * @private
     */
    LocalBlackjackServer.prototype.component;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtc2VydmVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYW5ndWxhci8iLCJzb3VyY2VzIjpbImxpYi9ibGFja2phY2svc2VydmVyL2xvY2FsLXNlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFHQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUU5RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUlyRTtJQU1JLDhCQUNZLFNBQTZCLEVBQ3JDLFdBQXdCO1FBRGhCLGNBQVMsR0FBVCxTQUFTLENBQW9CO1FBTGhDLHlCQUFvQixHQUFHLEtBQUssQ0FBQztRQVFsQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksbUJBQW1CLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7OztJQUVELGlEQUFrQjs7O0lBQWxCLGNBQXNCLENBQUM7Ozs7O0lBRXZCLHVDQUFROzs7O0lBQVIsVUFBUyxTQUF3Qjs7WUFDdkIsWUFBWSxHQUFHLG9CQUFvQixFQUFFOztZQUNyQyxZQUFZLEdBQUcsb0JBQW9CLEVBQUU7UUFFM0MseUJBQXlCO1FBQ3pCLDBCQUEwQjtRQUcxQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRXBCLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7O1lBR3JDLFlBQVksR0FBRyxvQkFBb0IsRUFBRTs7WUFDckMsWUFBWSxHQUFHLG9CQUFvQixFQUFFO1FBRTNDLDhDQUE4QztRQUM5QyxvQ0FBb0M7UUFHcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUNyQjtZQUNJLFdBQVcsRUFBRSxDQUFDLFlBQVksRUFBQyxZQUFZLENBQUM7WUFDeEMsV0FBVyxFQUFFLENBQUMsWUFBWSxDQUFDO1NBQzlCLEVBQ0QsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FDNUIsQ0FBQztJQUNOLENBQUM7Ozs7O0lBR0ssaURBQWtCOzs7O0lBQXhCLFVBQXlCLEdBQVk7OztnQkFDakMsc0JBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUM7OztLQUNsQzs7OztJQUVLLCtDQUFnQjs7O0lBQXRCOzs7Z0JBQ0ksc0JBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUM7OztLQUNsQzs7OztJQUdLLG1EQUFvQjs7O0lBQTFCOzs7Z0JBQ0ksc0JBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDOzs7S0FDdEM7Ozs7SUFFSyxvQ0FBSzs7O0lBQVg7OztnQkFDSSxzQkFBTzt3QkFDSCxvQkFBb0IsRUFBRTt3QkFDdEIsb0JBQW9CLEVBQUU7cUJBQ3pCLEVBQUM7OztLQUNMOzs7O0lBQ0QseUNBQVU7OztJQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQUNLLGtDQUFHOzs7SUFBVDs7O2dCQUNJLHNCQUFPLG9CQUFvQixFQUFFLEVBQUM7OztLQUNqQzs7OztJQUVLLG9DQUFLOzs7SUFBWDs7O2dCQUNJLGFBQWE7Z0JBQ2Isc0JBQU8sSUFBSSxFQUFDOzs7S0FDZjs7OztJQUdLLHdEQUF5Qjs7O0lBQS9COzs7O2dCQUNVLEtBQUssR0FBWSxFQUFFO2dCQUV6QixLQUFTLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDdEIsS0FBSyxDQUFDLElBQUksQ0FBRSxvQkFBb0IsRUFBRSxDQUFFLENBQUM7aUJBQ3hDO2dCQUVELHNCQUFPLEtBQUssRUFBQzs7O0tBQ2hCO0lBQ0wsMkJBQUM7QUFBRCxDQUFDLEFBdkZELElBdUZDOzs7O0lBckZHLG9EQUFzQzs7Ozs7SUFFdEMsc0NBQW1DOzs7OztJQUcvQix5Q0FBcUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFT1NBc3NldEFtb3VudCB9IGZyb20gXCJlYXJuYmV0LWNvbW1vblwiO1xuXG5pbXBvcnQgeyBJQmxhY2tqYWNrU2VydmVyIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IEJsYWNramFja0RlYWxlckhhbmQgfSBmcm9tICcuLi9jb21wb25lbnRzL21haW4vZ2FtZS9ibGFja2phY2staGFuZHMnO1xuaW1wb3J0IHsgSUJsYWNramFja0NvbXBvbmVudCB9IGZyb20gJy4uL2NvbXBvbmVudHMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBnZW5lcmF0ZVJhbmRvbUNhcmRJZCB9IGZyb20gJy4uLy4uL19jb21tb24vdXRpbC9jYXJkcy11dGlsJztcbmltcG9ydCB7IElTb3VuZFBsYXllciB9IGZyb20gJy4uLy4uL19jb21tb24vbWVkaWEvaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGNsYXNzIExvY2FsQmxhY2tqYWNrU2VydmVyIGltcGxlbWVudHMgSUJsYWNramFja1NlcnZlclxue1xuICAgIHJlYWRvbmx5IGlzV2FpdGluZ0ZvclJlc3BvbnNlID0gZmFsc2U7XG5cbiAgICBwcml2YXRlIGRlYWxlcjpCbGFja2phY2tEZWFsZXJIYW5kO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgY29tcG9uZW50OklCbGFja2phY2tDb21wb25lbnQsXG4gICAgICAgIHNvdW5kUGxheWVyOklTb3VuZFBsYXllclxuICAgICkge1xuICAgICAgICB0aGlzLmRlYWxlciA9IG5ldyBCbGFja2phY2tEZWFsZXJIYW5kKHNvdW5kUGxheWVyKTtcbiAgICB9XG5cbiAgICByZXN1bWVFeGlzdGluZ0dhbWUoKSB7fVxuXG4gICAgcGxhY2VCZXQoYmV0QW1vdW50OkVPU0Fzc2V0QW1vdW50KSB7XG4gICAgICAgIGNvbnN0IGRlYWxlcnNDYXJkMSA9IGdlbmVyYXRlUmFuZG9tQ2FyZElkKCk7XG4gICAgICAgIGNvbnN0IGRlYWxlcnNDYXJkMiA9IGdlbmVyYXRlUmFuZG9tQ2FyZElkKCk7XG5cbiAgICAgICAgLy9jb25zdCBkZWFsZXJzQ2FyZDEgPSAxO1xuICAgICAgICAvL2NvbnN0IGRlYWxlcnNDYXJkMiA9IDEwO1xuXG5cbiAgICAgICAgdGhpcy5kZWFsZXIucmVzZXQoKTtcblxuICAgICAgICB0aGlzLmRlYWxlci5kZWFsRmlyc3RDYXJkKGRlYWxlcnNDYXJkMSk7XG4gICAgICAgIHRoaXMuZGVhbGVyLnJldmVhbFNlY29uZENhcmQoZGVhbGVyc0NhcmQyKTtcblxuXG4gICAgICAgIGNvbnN0IHBsYXllcnNDYXJkMSA9IGdlbmVyYXRlUmFuZG9tQ2FyZElkKCk7XG4gICAgICAgIGNvbnN0IHBsYXllcnNDYXJkMiA9IGdlbmVyYXRlUmFuZG9tQ2FyZElkKCk7XG5cbiAgICAgICAgLy9jb25zdCBwbGF5ZXJzQ2FyZDEgPSBnZW5lcmF0ZVJhbmRvbUNhcmRJZCgpO1xuICAgICAgICAvL2NvbnN0IHBsYXllcnNDYXJkMiA9IHBsYXllcnNDYXJkMTtcblxuXG4gICAgICAgIHRoaXMuY29tcG9uZW50LmdhbWUuc3RhcnQoXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcGxheWVyQ2FyZHM6IFtwbGF5ZXJzQ2FyZDEscGxheWVyc0NhcmQyXSxcbiAgICAgICAgICAgICAgICBkZWFsZXJDYXJkczogW2RlYWxlcnNDYXJkMV1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBOdW1iZXIoYmV0QW1vdW50LmRlY2ltYWwpXG4gICAgICAgICk7XG4gICAgfVxuXG5cbiAgICBhc3luYyByZXNwb25kVG9JbnN1cmFuY2UoeWVzOiBib29sZWFuKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRlYWxlci5pc0JsYWNrSmFjaztcbiAgICB9XG5cbiAgICBhc3luYyBwZWVrRm9yQmxhY2tqYWNrKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5kZWFsZXIuaXNCbGFja0phY2s7XG4gICAgfVxuXG5cbiAgICBhc3luYyBnZXREZWFsZXJzU2Vjb25kQ2FyZCgpOiBQcm9taXNlPG51bWJlcj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5kZWFsZXIuY2FyZHNbMV0uY2FyZElkO1xuICAgIH1cblxuICAgIGFzeW5jIHNwbGl0KCk6IFByb21pc2U8bnVtYmVyW10+IHtcbiAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgIGdlbmVyYXRlUmFuZG9tQ2FyZElkKCksXG4gICAgICAgICAgICBnZW5lcmF0ZVJhbmRvbUNhcmRJZCgpXG4gICAgICAgIF07XG4gICAgfVxuICAgIGRvdWJsZURvd24oKTogUHJvbWlzZTxudW1iZXI+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaGl0KCk7XG4gICAgfVxuICAgIGFzeW5jIGhpdCgpOiBQcm9taXNlPG51bWJlcj4ge1xuICAgICAgICByZXR1cm4gZ2VuZXJhdGVSYW5kb21DYXJkSWQoKTtcbiAgICB9XG5cbiAgICBhc3luYyBzdGFuZCgpIHtcbiAgICAgICAgLy8gZG8gbm90aGluZ1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cblxuICAgIGFzeW5jIGdldERlYWxlcnNBZGRpdGlvbmFsQ2FyZHMoKTogUHJvbWlzZTxudW1iZXJbXT4ge1xuICAgICAgICBjb25zdCBjYXJkczpudW1iZXJbXSA9IFtdO1xuXG4gICAgICAgIGZvciAodmFyIGk9MDsgaSA8IDg7IGkrKykge1xuICAgICAgICAgICAgY2FyZHMucHVzaCggZ2VuZXJhdGVSYW5kb21DYXJkSWQoKSApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGNhcmRzO1xuICAgIH1cbn1cbiJdfQ==