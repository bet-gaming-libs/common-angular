/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/remote-server.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Big } from 'big.js';
import { BlackjackGameCommand } from "earnbet-common";
import { EosErrorParser } from '../../_common/eos/eos-error-parser';
var RemoteBlackjackServerBridge = /** @class */ (function () {
    function RemoteBlackjackServerBridge(component, accountNames, app) {
        this.component = component;
        this.accountNames = accountNames;
        this.app = app;
        this.actionId = 0;
        this._isWaitingForResponse = false;
        this.playersAdditionalCardIndex = undefined;
        this.isResuming = false;
    }
    /**
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.resumeExistingGame = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var e_1, _a, wallet, state, _b, _c, command, parts, commandName, _d, e_1_1;
            return tslib_1.__generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        wallet = this.wallet;
                        if (!wallet.isLoggedIn ||
                            this.isResuming) {
                            return [2 /*return*/];
                        }
                        this.isResuming = true;
                        return [4 /*yield*/, this.sendGameCommand(BlackjackGameCommand.RESUME_GAME, this.accountData)];
                    case 1:
                        state = _e.sent();
                        console.log('Resumed Game State:');
                        console.log(state);
                        this.app.state.bets.setMyBetId(state.id);
                        return [4 /*yield*/, this.startGame(state)];
                    case 2:
                        _e.sent();
                        // *** START SYNCING: ***
                        this.playersAdditionalCardIndex = 0;
                        _e.label = 3;
                    case 3:
                        _e.trys.push([3, 16, 17, 18]);
                        _b = tslib_1.__values(state.commands), _c = _b.next();
                        _e.label = 4;
                    case 4:
                        if (!!_c.done) return [3 /*break*/, 15];
                        command = _c.value;
                        this.actionId++;
                        parts = command.split("~");
                        commandName = parts[0];
                        _d = commandName;
                        switch (_d) {
                            case BlackjackGameCommand.YES_TO_INSURANCE: return [3 /*break*/, 5];
                            case BlackjackGameCommand.NO_TO_INSURANCE: return [3 /*break*/, 5];
                            case BlackjackGameCommand.SPLIT: return [3 /*break*/, 6];
                            case BlackjackGameCommand.HIT: return [3 /*break*/, 8];
                            case BlackjackGameCommand.DOUBLE_DOWN: return [3 /*break*/, 10];
                            case BlackjackGameCommand.STAND: return [3 /*break*/, 12];
                        }
                        return [3 /*break*/, 14];
                    case 5:
                        this.game.respondToInsurance(commandName == BlackjackGameCommand.YES_TO_INSURANCE);
                        return [3 /*break*/, 14];
                    case 6: return [4 /*yield*/, this.game.split()];
                    case 7:
                        _e.sent();
                        return [3 /*break*/, 14];
                    case 8: return [4 /*yield*/, this.game.hit()];
                    case 9:
                        _e.sent();
                        return [3 /*break*/, 14];
                    case 10: return [4 /*yield*/, this.game.doubleDown()];
                    case 11:
                        _e.sent();
                        return [3 /*break*/, 14];
                    case 12: return [4 /*yield*/, this.game.stand()];
                    case 13:
                        _e.sent();
                        return [3 /*break*/, 14];
                    case 14:
                        _c = _b.next();
                        return [3 /*break*/, 4];
                    case 15: return [3 /*break*/, 18];
                    case 16:
                        e_1_1 = _e.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 18];
                    case 17:
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_1) throw e_1.error; }
                        return [7 /*endfinally*/];
                    case 18:
                        // *** STOP SYNCING: ***
                        this.playersAdditionalCardIndex = undefined;
                        this.isResuming = false;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} betAmount
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.placeBet = /**
     * @param {?} betAmount
     * @return {?}
     */
    function (betAmount) {
        var _this = this;
        if (this.component.isInsufficientFunds) {
            this.app.openInsufficentFundsModal();
            return;
        }
        this._isWaitingForResponse = true;
        /** @type {?} */
        var component = this.component;
        /** @type {?} */
        var wallet = component.wallet;
        /** @type {?} */
        var memo = String(betAmount.integer) + "-" + wallet.publicKey + "-" + component.state.referrer + "-" + component.getSeed(false);
        Big.RM = 0 /* RoundDown */;
        /** @type {?} */
        var amtDeposit = new Big(betAmount.decimal).times("4.5");
        /** @type {?} */
        var amtBalance = component.wallet.getBalance(component.selectedToken.symbol);
        /** @type {?} */
        var precision = component.selectedToken.precision;
        if (amtDeposit.gt(amtBalance)) {
            // If there is not enough funds for 4.5 then do not deposit the uneven remainder that cant be used
            /** @type {?} */
            var factor = new Big(amtBalance).div(betAmount.decimal).toFixed(0);
            amtDeposit = new Big(betAmount.decimal).times(factor);
        }
        /** @type {?} */
        var depositAmount = component.selectedToken.getAssetAmount(amtDeposit.toFixed(precision));
        /** @type {?} */
        var eosAccounts = this.app.eos.accounts;
        /** @type {?} */
        var transfers = [{
                toAccount: eosAccounts.games.blackjack,
                amount: depositAmount,
                memo: memo
            }];
        if (component.isJackpotBet) {
            transfers.push({
                toAccount: eosAccounts.jackpot,
                amount: component.selectedToken.jackpotTicketPrice,
                memo: eosAccounts.games.blackjack
            });
        }
        /** @type {?} */
        var promise = wallet.transfer(transfers);
        promise.then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            /** @type {?} */
            var txnId = result.transaction_id;
            _this.placedBetId = component.state.bets.onBetPlaced(txnId);
            _this.component.trackBetWithGoogleAnalytics('blackjack', betAmount.decimal, txnId, false);
            _this._isWaitingForResponse = false;
            _this.waitForGame();
        }), (/**
         * @param {?} errorData
         * @return {?}
         */
        function (errorData) {
            _this._isWaitingForResponse = false;
            /** @type {?} */
            var error = new EosErrorParser(errorData);
            component.alert.error(error.message);
        }));
    };
    /**
     * @private
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.waitForGame = /**
     * @private
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var state;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this._isWaitingForResponse = true;
                        return [4 /*yield*/, this.sendGameCommand(BlackjackGameCommand.START_GAME, this.accountData)];
                    case 1:
                        state = _a.sent();
                        return [4 /*yield*/, this.startGame(state)];
                    case 2:
                        _a.sent();
                        this.isResuming = false;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} state
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.startGame = /**
     * @private
     * @param {?} state
     * @return {?}
     */
    function (state) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var token;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.actionId = 0;
                        this._isWaitingForResponse = false;
                        //this.isDealerBlackjack = state.isDealerBlackjack;
                        this.dealersAdditionalCards = state.additionalDealerCards;
                        this.state = state;
                        token = state.initialBet.token;
                        return [4 /*yield*/, this.game.start(state.initialCards, state.initialBet.amountAsInteger /
                                Math.pow(10, token.precision))];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} yes
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.respondToInsurance = /**
     * @param {?} yes
     * @return {?}
     */
    function (yes) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!this.isSyncing) return [3 /*break*/, 1];
                        _a = this.state;
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.sendGameCommand(yes ?
                            BlackjackGameCommand.YES_TO_INSURANCE :
                            BlackjackGameCommand.NO_TO_INSURANCE)];
                    case 2:
                        _a = _b.sent();
                        _b.label = 3;
                    case 3:
                        response = _a;
                        return [2 /*return*/, response.isDealerBlackjack];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.peekForBlackjack = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!(this.isSyncing || this.state.isDealerBlackjack != undefined)) return [3 /*break*/, 1];
                        _a = this.state;
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.sendGameCommand(BlackjackGameCommand.PEEK_FOR_BLACKJACK)];
                    case 2:
                        _a = _b.sent();
                        _b.label = 3;
                    case 3:
                        response = _a;
                        return [2 /*return*/, response.isDealerBlackjack];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.split = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!this.isSyncing) return [3 /*break*/, 1];
                        _a = this.getAdditionalPlayerCards(2);
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.sendGameCommand(BlackjackGameCommand.SPLIT)];
                    case 2:
                        _a = _b.sent();
                        _b.label = 3;
                    case 3:
                        response = _a;
                        return [2 /*return*/, response];
                }
            });
        });
    };
    /**
     * @param {?} handIndex
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.hit = /**
     * @param {?} handIndex
     * @return {?}
     */
    function (handIndex) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!this.isSyncing) return [3 /*break*/, 1];
                        _a = this.getAdditionalPlayerCards(1);
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.sendGameCommand(BlackjackGameCommand.HIT)];
                    case 2:
                        _a = _b.sent();
                        _b.label = 3;
                    case 3:
                        response = _a;
                        return [2 /*return*/, response[0]];
                }
            });
        });
    };
    /**
     * @param {?} handIndex
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.doubleDown = /**
     * @param {?} handIndex
     * @return {?}
     */
    function (handIndex) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!this.isSyncing) return [3 /*break*/, 1];
                        _a = this.getAdditionalPlayerCards(1);
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.sendGameCommand(BlackjackGameCommand.DOUBLE_DOWN)];
                    case 2:
                        _a = _b.sent();
                        _b.label = 3;
                    case 3:
                        response = _a;
                        return [2 /*return*/, response[0]];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} numOfCards
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.getAdditionalPlayerCards = /**
     * @private
     * @param {?} numOfCards
     * @return {?}
     */
    function (numOfCards) {
        /** @type {?} */
        var cardIds = [];
        for (var i = 0; i < numOfCards; i++) {
            /** @type {?} */
            var index = this.playersAdditionalCardIndex++;
            /** @type {?} */
            var card = this.state.additionalPlayerCards[index];
            cardIds.push(card);
        }
        return cardIds;
    };
    /**
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.stand = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.isSyncing) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.sendGameCommand(BlackjackGameCommand.STAND)];
                    case 1: return [2 /*return*/, _a.sent()];
                    case 2: return [2 /*return*/, false];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.getDealersSecondCard = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var cards, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!this.isSyncing) return [3 /*break*/, 1];
                        _a = this.state.additionalDealerCards;
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.sendGameCommand(BlackjackGameCommand.GET_DEALERS_CARDS)];
                    case 2:
                        _a = _b.sent();
                        _b.label = 3;
                    case 3:
                        cards = _a;
                        this.dealersAdditionalCards = cards.slice(1);
                        return [2 /*return*/, cards[0]];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.getDealersAdditionalCards = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, this.dealersAdditionalCards];
            });
        });
    };
    /**
     * @private
     * @template T
     * @param {?} commandName
     * @param {?=} args
     * @return {?}
     */
    RemoteBlackjackServerBridge.prototype.sendGameCommand = /**
     * @private
     * @template T
     * @param {?} commandName
     * @param {?=} args
     * @return {?}
     */
    function (commandName, args) {
        var _this = this;
        if (args === void 0) { args = []; }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var isResumeGameCommand = commandName == BlackjackGameCommand.RESUME_GAME;
            /** @type {?} */
            var isStartGameCommand = commandName == BlackjackGameCommand.START_GAME;
            /** @type {?} */
            var actionId = isResumeGameCommand || isStartGameCommand ?
                0 :
                ++_this.actionId;
            /** @type {?} */
            var parts = [
                commandName,
                isStartGameCommand ?
                    _this.placedBetId :
                    _this.gameId,
                actionId
            ].concat(args);
            // data format:commandName~gameId~actionId~data...
            /** @type {?} */
            var data = parts.join('~');
            if (!isResumeGameCommand) {
                _this._isWaitingForResponse = true;
            }
            _this.wallet.sign(data, '')
                .then((/**
             * @param {?} info
             * @return {?}
             */
            function (info) {
                //console.log('Sending Game Command: ')
                //console.log(data);
                _this.app.blackjack.client.messageManager.sendGameCommand(data, info).then((/**
                 * @param {?} result
                 * @return {?}
                 */
                function (result) {
                    _this._isWaitingForResponse = false;
                    resolve(result);
                })).catch((/**
                 * @param {?} error
                 * @return {?}
                 */
                function (error) {
                    _this._isWaitingForResponse = false;
                    _this.alert.error(error.code);
                    reject(error);
                }));
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                _this._isWaitingForResponse = false;
                /** @type {?} */
                var e = new EosErrorParser(error);
                _this.alert.error(e.message);
                reject(error);
            }));
        }));
    };
    Object.defineProperty(RemoteBlackjackServerBridge.prototype, "isSyncing", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.playersAdditionalCardIndex != undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RemoteBlackjackServerBridge.prototype, "gameId", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.state ?
                this.state.id :
                '0';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RemoteBlackjackServerBridge.prototype, "isWaitingForResponse", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isWaitingForResponse;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RemoteBlackjackServerBridge.prototype, "game", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.component.game;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RemoteBlackjackServerBridge.prototype, "accountData", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var wallet = this.wallet;
            return wallet.isEasyAccount ?
                [this.accountNames.easyAccount, wallet.accountId, wallet.accountName] :
                [wallet.accountName];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RemoteBlackjackServerBridge.prototype, "wallet", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.app.wallet;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RemoteBlackjackServerBridge.prototype, "alert", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.app.alert;
        },
        enumerable: true,
        configurable: true
    });
    return RemoteBlackjackServerBridge;
}());
export { RemoteBlackjackServerBridge };
if (false) {
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.placedBetId;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.actionId;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype._isWaitingForResponse;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.state;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.dealersAdditionalCards;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.playersAdditionalCardIndex;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.isResuming;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.component;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.accountNames;
    /**
     * @type {?}
     * @private
     */
    RemoteBlackjackServerBridge.prototype.app;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVtb3RlLXNlcnZlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWFuZ3VsYXIvIiwic291cmNlcyI6WyJsaWIvYmxhY2tqYWNrL3NlcnZlci9yZW1vdGUtc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxHQUFHLEVBQWdCLE1BQU0sUUFBUSxDQUFDO0FBRzNDLE9BQU8sRUFBa0Isb0JBQW9CLEVBQXVCLE1BQU0sZ0JBQWdCLENBQUM7QUFLM0YsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBSXBFO0lBV0kscUNBQ1ksU0FBOEIsRUFDOUIsWUFBNkIsRUFDN0IsR0FBbUI7UUFGbkIsY0FBUyxHQUFULFNBQVMsQ0FBcUI7UUFDOUIsaUJBQVksR0FBWixZQUFZLENBQWlCO1FBQzdCLFFBQUcsR0FBSCxHQUFHLENBQWdCO1FBWHZCLGFBQVEsR0FBRyxDQUFDLENBQUM7UUFFYiwwQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFHOUIsK0JBQTBCLEdBQVUsU0FBUyxDQUFDO1FBQzlDLGVBQVUsR0FBRyxLQUFLLENBQUM7SUFPM0IsQ0FBQzs7OztJQUVLLHdEQUFrQjs7O0lBQXhCOzs7Ozs7d0JBQ1UsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNO3dCQUUxQixJQUNJLENBQUMsTUFBTSxDQUFDLFVBQVU7NEJBQ2xCLElBQUksQ0FBQyxVQUFVLEVBQ2pCOzRCQUNFLHNCQUFPO3lCQUNWO3dCQUdELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO3dCQUdULHFCQUFNLElBQUksQ0FBQyxlQUFlLENBQ3BDLG9CQUFvQixDQUFDLFdBQVcsRUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FDbkIsRUFBQTs7d0JBSEssS0FBSyxHQUFHLFNBR2I7d0JBR0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO3dCQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUduQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFHekMscUJBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBQTs7d0JBQTNCLFNBQTJCLENBQUM7d0JBRzVCLHlCQUF5Qjt3QkFDekIsSUFBSSxDQUFDLDBCQUEwQixHQUFHLENBQUMsQ0FBQzs7Ozt3QkFFaEIsS0FBQSxpQkFBQSxLQUFLLENBQUMsUUFBUSxDQUFBOzs7O3dCQUF6QixPQUFPO3dCQUNaLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzt3QkFFVixLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7d0JBRTFCLFdBQVcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUlwQixLQUFBLFdBQVcsQ0FBQTs7aUNBQ1Ysb0JBQW9CLENBQUMsZ0JBQWdCLENBQUMsQ0FBdEMsd0JBQXFDO2lDQUNyQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsQ0FBckMsd0JBQW9DO2lDQU9wQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBM0Isd0JBQTBCO2lDQUkxQixvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBekIsd0JBQXdCO2lDQUl4QixvQkFBb0IsQ0FBQyxXQUFXLENBQUMsQ0FBakMseUJBQWdDO2lDQUloQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBM0IseUJBQTBCOzs7O3dCQWxCM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FDeEIsV0FBVyxJQUFJLG9CQUFvQixDQUFDLGdCQUFnQixDQUN2RCxDQUFBO3dCQUNELHlCQUFNOzRCQUlOLHFCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUE7O3dCQUF2QixTQUF1QixDQUFDO3dCQUN4Qix5QkFBTTs0QkFHTixxQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxFQUFBOzt3QkFBckIsU0FBcUIsQ0FBQzt3QkFDdEIseUJBQU07NkJBR04scUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBQTs7d0JBQTVCLFNBQTRCLENBQUM7d0JBQzdCLHlCQUFNOzZCQUdOLHFCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUE7O3dCQUF2QixTQUF1QixDQUFDO3dCQUN4Qix5QkFBTTs7Ozs7Ozs7Ozs7Ozs7Ozt3QkFJbEIsd0JBQXdCO3dCQUN4QixJQUFJLENBQUMsMEJBQTBCLEdBQUcsU0FBUyxDQUFDO3dCQUU1QyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzs7Ozs7S0FDM0I7Ozs7O0lBRUQsOENBQVE7Ozs7SUFBUixVQUFTLFNBQXlCO1FBQWxDLGlCQThFQztRQTdFRyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLEVBQUU7WUFDcEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1lBRXJDLE9BQU87U0FDVjtRQUdELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7O1lBRzVCLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUzs7WUFDMUIsTUFBTSxHQUFHLFNBQVMsQ0FBQyxNQUFNOztZQUd6QixJQUFJLEdBQU0sTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsU0FBSSxNQUFNLENBQUMsU0FBUyxTQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUSxTQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFHO1FBRXZILEdBQUcsQ0FBQyxFQUFFLG9CQUF5QixDQUFDOztZQUM1QixVQUFVLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7O1lBQ2xELFVBQVUsR0FBVyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQzs7WUFDaEYsU0FBUyxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUMsU0FBUztRQUVuRCxJQUFJLFVBQVUsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLEVBQUU7OztnQkFFckIsTUFBTSxHQUFHLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNwRSxVQUFVLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6RDs7WUFFSyxhQUFhLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQ3hELFVBQVUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQ2hDOztZQUVLLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxRQUFROztZQUVuQyxTQUFTLEdBQUcsQ0FBQztnQkFDZixTQUFTLEVBQUUsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTO2dCQUN0QyxNQUFNLEVBQUUsYUFBYTtnQkFDckIsSUFBSSxNQUFBO2FBQ1AsQ0FBQztRQUVGLElBQUksU0FBUyxDQUFDLFlBQVksRUFBRTtZQUN4QixTQUFTLENBQUMsSUFBSSxDQUFDO2dCQUNYLFNBQVMsRUFBRSxXQUFXLENBQUMsT0FBTztnQkFDOUIsTUFBTSxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsa0JBQWtCO2dCQUNsRCxJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTO2FBQ3BDLENBQUMsQ0FBQztTQUNOOztZQUdLLE9BQU8sR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztRQUUxQyxPQUFPLENBQUMsSUFBSTs7OztRQUNSLFVBQUMsTUFBTTs7Z0JBQ0csS0FBSyxHQUFHLE1BQU0sQ0FBQyxjQUFjO1lBRW5DLEtBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRzNELEtBQUksQ0FBQyxTQUFTLENBQUMsMkJBQTJCLENBQ3RDLFdBQVcsRUFDWCxTQUFTLENBQUMsT0FBTyxFQUNqQixLQUFLLEVBQ0wsS0FBSyxDQUNSLENBQUM7WUFHRixLQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1lBRW5DLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN2QixDQUFDOzs7O1FBQ0QsVUFBQyxTQUFTO1lBQ04sS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQzs7Z0JBRTdCLEtBQUssR0FBRyxJQUFJLGNBQWMsQ0FBQyxTQUFTLENBQUM7WUFFM0MsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLENBQUMsRUFDSixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFYSxpREFBVzs7OztJQUF6Qjs7Ozs7O3dCQUNJLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7d0JBRXBCLHFCQUFNLElBQUksQ0FBQyxlQUFlLENBQ3BDLG9CQUFvQixDQUFDLFVBQVUsRUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FDbkIsRUFBQTs7d0JBSEssS0FBSyxHQUFHLFNBR2I7d0JBRUQscUJBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBQTs7d0JBQTNCLFNBQTJCLENBQUM7d0JBRTVCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDOzs7OztLQUMzQjs7Ozs7O0lBRWEsK0NBQVM7Ozs7O0lBQXZCLFVBQXdCLEtBQXlCOzs7Ozs7d0JBQzdDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO3dCQUNsQixJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO3dCQUVuQyxtREFBbUQ7d0JBQ25ELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUMscUJBQXFCLENBQUM7d0JBRTFELElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO3dCQUViLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUs7d0JBRXBDLHFCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUNqQixLQUFLLENBQUMsWUFBWSxFQUNsQixLQUFLLENBQUMsVUFBVSxDQUFDLGVBQWU7Z0NBQzVCLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FDbkMsRUFBQTs7d0JBSkQsU0FJQyxDQUFDOzs7OztLQUNMOzs7OztJQUVLLHdEQUFrQjs7OztJQUF4QixVQUF5QixHQUFZOzs7Ozs7NkJBRTdCLElBQUksQ0FBQyxTQUFTLEVBQWQsd0JBQWM7d0JBQ1YsS0FBQSxJQUFJLENBQUMsS0FBSyxDQUFBOzs0QkFDVixxQkFBTSxJQUFJLENBQUMsZUFBZSxDQUN0QixHQUFHLENBQUMsQ0FBQzs0QkFDRCxvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOzRCQUN2QyxvQkFBb0IsQ0FBQyxlQUFlLENBQzNDLEVBQUE7O3dCQUpELEtBQUEsU0FJQyxDQUFBOzs7d0JBUEgsUUFBUSxLQU9MO3dCQUVULHNCQUFPLFFBQVEsQ0FBQyxpQkFBaUIsRUFBQzs7OztLQUNyQzs7OztJQUVLLHNEQUFnQjs7O0lBQXRCOzs7Ozs7NkJBRVEsQ0FBQSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLElBQUksU0FBUyxDQUFBLEVBQTNELHdCQUEyRDt3QkFDdkQsS0FBQSxJQUFJLENBQUMsS0FBSyxDQUFBOzs0QkFDVixxQkFBTSxJQUFJLENBQUMsZUFBZSxDQUN0QixvQkFBb0IsQ0FBQyxrQkFBa0IsQ0FDMUMsRUFBQTs7d0JBRkQsS0FBQSxTQUVDLENBQUE7Ozt3QkFMSCxRQUFRLEtBS0w7d0JBRVQsc0JBQU8sUUFBUSxDQUFDLGlCQUFpQixFQUFDOzs7O0tBQ3JDOzs7O0lBR0ssMkNBQUs7OztJQUFYOzs7Ozs7NkJBRVEsSUFBSSxDQUFDLFNBQVMsRUFBZCx3QkFBYzt3QkFDVixLQUFBLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsQ0FBQTs7NEJBQ2hDLHFCQUFNLElBQUksQ0FBQyxlQUFlLENBQ3RCLG9CQUFvQixDQUFDLEtBQUssQ0FDN0IsRUFBQTs7d0JBRkQsS0FBQSxTQUVDLENBQUE7Ozt3QkFMSCxRQUFRLEtBS0w7d0JBRVQsc0JBQU8sUUFBUSxFQUFDOzs7O0tBQ25COzs7OztJQUVLLHlDQUFHOzs7O0lBQVQsVUFBVSxTQUFpQjs7Ozs7OzZCQUVuQixJQUFJLENBQUMsU0FBUyxFQUFkLHdCQUFjO3dCQUNWLEtBQUEsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxDQUFBOzs0QkFDaEMscUJBQU0sSUFBSSxDQUFDLGVBQWUsQ0FDdEIsb0JBQW9CLENBQUMsR0FBRyxDQUMzQixFQUFBOzt3QkFGRCxLQUFBLFNBRUMsQ0FBQTs7O3dCQUxILFFBQVEsS0FLTDt3QkFFVCxzQkFBTyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUM7Ozs7S0FDdEI7Ozs7O0lBRUssZ0RBQVU7Ozs7SUFBaEIsVUFBaUIsU0FBaUI7Ozs7Ozs2QkFFMUIsSUFBSSxDQUFDLFNBQVMsRUFBZCx3QkFBYzt3QkFDVixLQUFBLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsQ0FBQTs7NEJBQ2hDLHFCQUFNLElBQUksQ0FBQyxlQUFlLENBQ3RCLG9CQUFvQixDQUFDLFdBQVcsQ0FDbkMsRUFBQTs7d0JBRkQsS0FBQSxTQUVDLENBQUE7Ozt3QkFMSCxRQUFRLEtBS0w7d0JBRVQsc0JBQU8sUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFDOzs7O0tBQ3RCOzs7Ozs7SUFFTyw4REFBd0I7Ozs7O0lBQWhDLFVBQWlDLFVBQWlCOztZQUN4QyxPQUFPLEdBQVksRUFBRTtRQUUzQixLQUFLLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFOztnQkFDekIsS0FBSyxHQUFHLElBQUksQ0FBQywwQkFBMEIsRUFBRTs7Z0JBRXpDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQztZQUVwRCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3RCO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQzs7OztJQUVLLDJDQUFLOzs7SUFBWDs7Ozs7NkJBQ1EsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFmLHdCQUFlO3dCQUNSLHFCQUFNLElBQUksQ0FBQyxlQUFlLENBQzdCLG9CQUFvQixDQUFDLEtBQUssQ0FDN0IsRUFBQTs0QkFGRCxzQkFBTyxTQUVOLEVBQUM7NEJBRUYsc0JBQU8sS0FBSyxFQUFDOzs7O0tBRXBCOzs7O0lBRUssMERBQW9COzs7SUFBMUI7Ozs7Ozs2QkFFUSxJQUFJLENBQUMsU0FBUyxFQUFkLHdCQUFjO3dCQUNWLEtBQUEsSUFBSSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQTs7NEJBQ2hDLHFCQUFNLElBQUksQ0FBQyxlQUFlLENBQ3RCLG9CQUFvQixDQUFDLGlCQUFpQixDQUN6QyxFQUFBOzt3QkFGRCxLQUFBLFNBRUMsQ0FBQTs7O3dCQUxILEtBQUssS0FLRjt3QkFFVCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFFN0Msc0JBQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFDOzs7O0tBQ25COzs7O0lBRUssK0RBQXlCOzs7SUFBL0I7OztnQkFDSSxzQkFBTyxJQUFJLENBQUMsc0JBQXNCLEVBQUM7OztLQUN0Qzs7Ozs7Ozs7SUFHTyxxREFBZTs7Ozs7OztJQUF2QixVQUNJLFdBQWtCLEVBQ2xCLElBQWtCO1FBRnRCLGlCQTJEQztRQXpERyxxQkFBQSxFQUFBLFNBQWtCO1FBRWxCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFLLFVBQUMsT0FBTyxFQUFDLE1BQU07O2dCQUM1QixtQkFBbUIsR0FDckIsV0FBVyxJQUFJLG9CQUFvQixDQUFDLFdBQVc7O2dCQUU3QyxrQkFBa0IsR0FDcEIsV0FBVyxJQUFJLG9CQUFvQixDQUFDLFVBQVU7O2dCQUc1QyxRQUFRLEdBQ1YsbUJBQW1CLElBQUksa0JBQWtCLENBQUMsQ0FBQztnQkFDdkMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsRUFBRSxLQUFJLENBQUMsUUFBUTs7Z0JBRWpCLEtBQUssR0FBRztnQkFDVixXQUFXO2dCQUNYLGtCQUFrQixDQUFDLENBQUM7b0JBQ2hCLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDbEIsS0FBSSxDQUFDLE1BQU07Z0JBQ2YsUUFBUTthQUNYLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQzs7O2dCQUVSLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUU1QixJQUFJLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ3RCLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7YUFDckM7WUFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUMsRUFBRSxDQUFDO2lCQUN4QixJQUFJOzs7O1lBQUUsVUFBQyxJQUFJO2dCQUNSLHVDQUF1QztnQkFDdkMsb0JBQW9CO2dCQUVwQixLQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FDcEQsSUFBSSxFQUNKLElBQUksQ0FDUCxDQUFDLElBQUk7Ozs7Z0JBQUUsVUFBQyxNQUFNO29CQUNYLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7b0JBRW5DLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDcEIsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztnQkFBRSxVQUFDLEtBQUs7b0JBQ1osS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztvQkFFbkMsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUU3QixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2xCLENBQUMsRUFBQyxDQUFDO1lBQ1AsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztZQUFFLFVBQUMsS0FBSztnQkFDWixLQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDOztvQkFFN0IsQ0FBQyxHQUFHLElBQUksY0FBYyxDQUFDLEtBQUssQ0FBQztnQkFDbkMsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUU1QixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEIsQ0FBQyxFQUFDLENBQUM7UUFDUCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQkFBWSxrREFBUzs7Ozs7UUFBckI7WUFDSSxPQUFPLElBQUksQ0FBQywwQkFBMEIsSUFBSSxTQUFTLENBQUM7UUFDeEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBWSwrQ0FBTTs7Ozs7UUFBbEI7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDWCxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNmLEdBQUcsQ0FBQztRQUNoQixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDZEQUFvQjs7OztRQUF4QjtZQUNJLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDO1FBQ3RDLENBQUM7OztPQUFBO0lBRUQsc0JBQVksNkNBQUk7Ozs7O1FBQWhCO1lBQ0ksT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztRQUMvQixDQUFDOzs7T0FBQTtJQUVELHNCQUFZLG9EQUFXOzs7OztRQUF2Qjs7Z0JBQ1UsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNO1lBRTFCLE9BQVEsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUN0QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JFLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pDLENBQUM7OztPQUFBO0lBRUQsc0JBQVksK0NBQU07Ozs7O1FBQWxCO1lBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUMzQixDQUFDOzs7T0FBQTtJQUVELHNCQUFZLDhDQUFLOzs7OztRQUFqQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7UUFDMUIsQ0FBQzs7O09BQUE7SUFDTCxrQ0FBQztBQUFELENBQUMsQUE5WUQsSUE4WUM7Ozs7Ozs7SUE1WUcsa0RBQTJCOzs7OztJQUMzQiwrQ0FBcUI7Ozs7O0lBRXJCLDREQUFzQzs7Ozs7SUFDdEMsNENBQWtDOzs7OztJQUNsQyw2REFBd0M7Ozs7O0lBQ3hDLGlFQUFzRDs7Ozs7SUFDdEQsaURBQTJCOzs7OztJQUd2QixnREFBc0M7Ozs7O0lBQ3RDLG1EQUFxQzs7Ozs7SUFDckMsMENBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmlnLCBSb3VuZGluZ01vZGUgfSBmcm9tICdiaWcuanMnO1xuXG5cbmltcG9ydCB7IEVPU0Fzc2V0QW1vdW50LCBCbGFja2phY2tHYW1lQ29tbWFuZCwgSUJsYWNramFja0dhbWVTdGF0ZSB9IGZyb20gXCJlYXJuYmV0LWNvbW1vblwiO1xuXG5pbXBvcnQgeyBJQmxhY2tqYWNrU2VydmVyIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElCbGFja2phY2tDb21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSU1haW5BcHBDb250ZXh0IH0gZnJvbSAnLi4vLi4vX2NvbW1vbi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IEVvc0Vycm9yUGFyc2VyIH0gZnJvbSAnLi4vLi4vX2NvbW1vbi9lb3MvZW9zLWVycm9yLXBhcnNlcic7XG5pbXBvcnQgeyBJRW9zQWNjb3VudE5hbWVzIH0gZnJvbSAnLi4vLi4vX2NvbW1vbi9tb2RlbHMvYnVzaW5lc3MtcnVsZXMvaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGNsYXNzIFJlbW90ZUJsYWNramFja1NlcnZlckJyaWRnZSBpbXBsZW1lbnRzIElCbGFja2phY2tTZXJ2ZXJcbntcbiAgICBwcml2YXRlIHBsYWNlZEJldElkOnN0cmluZztcbiAgICBwcml2YXRlIGFjdGlvbklkID0gMDtcblxuICAgIHByaXZhdGUgX2lzV2FpdGluZ0ZvclJlc3BvbnNlID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBzdGF0ZTpJQmxhY2tqYWNrR2FtZVN0YXRlO1xuICAgIHByaXZhdGUgZGVhbGVyc0FkZGl0aW9uYWxDYXJkczpudW1iZXJbXTtcbiAgICBwcml2YXRlIHBsYXllcnNBZGRpdGlvbmFsQ2FyZEluZGV4Om51bWJlciA9IHVuZGVmaW5lZDtcbiAgICBwcml2YXRlIGlzUmVzdW1pbmcgPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGNvbXBvbmVudDogSUJsYWNramFja0NvbXBvbmVudCxcbiAgICAgICAgcHJpdmF0ZSBhY2NvdW50TmFtZXM6SUVvc0FjY291bnROYW1lcyxcbiAgICAgICAgcHJpdmF0ZSBhcHA6SU1haW5BcHBDb250ZXh0XG4gICAgKSB7XG4gICAgfVxuXG4gICAgYXN5bmMgcmVzdW1lRXhpc3RpbmdHYW1lKCkge1xuICAgICAgICBjb25zdCB3YWxsZXQgPSB0aGlzLndhbGxldDtcblxuICAgICAgICBpZiAoXG4gICAgICAgICAgICAhd2FsbGV0LmlzTG9nZ2VkSW4gfHxcbiAgICAgICAgICAgIHRoaXMuaXNSZXN1bWluZ1xuICAgICAgICApIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgdGhpcy5pc1Jlc3VtaW5nID0gdHJ1ZTtcblxuXG4gICAgICAgIGNvbnN0IHN0YXRlID0gYXdhaXQgdGhpcy5zZW5kR2FtZUNvbW1hbmQ8SUJsYWNramFja0dhbWVTdGF0ZT4oXG4gICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5SRVNVTUVfR0FNRSxcbiAgICAgICAgICAgIHRoaXMuYWNjb3VudERhdGFcbiAgICAgICAgKTtcblxuXG4gICAgICAgIGNvbnNvbGUubG9nKCdSZXN1bWVkIEdhbWUgU3RhdGU6Jyk7XG4gICAgICAgIGNvbnNvbGUubG9nKHN0YXRlKTtcblxuXG4gICAgICAgIHRoaXMuYXBwLnN0YXRlLmJldHMuc2V0TXlCZXRJZChzdGF0ZS5pZCk7XG5cblxuICAgICAgICBhd2FpdCB0aGlzLnN0YXJ0R2FtZShzdGF0ZSk7XG5cblxuICAgICAgICAvLyAqKiogU1RBUlQgU1lOQ0lORzogKioqXG4gICAgICAgIHRoaXMucGxheWVyc0FkZGl0aW9uYWxDYXJkSW5kZXggPSAwO1xuXG4gICAgICAgIGZvciAodmFyIGNvbW1hbmQgb2Ygc3RhdGUuY29tbWFuZHMpIHtcbiAgICAgICAgICAgIHRoaXMuYWN0aW9uSWQrKztcblxuICAgICAgICAgICAgY29uc3QgcGFydHMgPSBjb21tYW5kLnNwbGl0KFwiflwiKTtcblxuICAgICAgICAgICAgY29uc3QgY29tbWFuZE5hbWUgPSBwYXJ0c1swXTtcblxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhjb21tYW5kTmFtZSk7XG5cbiAgICAgICAgICAgIHN3aXRjaCAoY29tbWFuZE5hbWUpIHtcbiAgICAgICAgICAgICAgICBjYXNlIEJsYWNramFja0dhbWVDb21tYW5kLllFU19UT19JTlNVUkFOQ0U6XG4gICAgICAgICAgICAgICAgY2FzZSBCbGFja2phY2tHYW1lQ29tbWFuZC5OT19UT19JTlNVUkFOQ0U6XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2FtZS5yZXNwb25kVG9JbnN1cmFuY2UoXG4gICAgICAgICAgICAgICAgICAgICAgICBjb21tYW5kTmFtZSA9PSBCbGFja2phY2tHYW1lQ29tbWFuZC5ZRVNfVE9fSU5TVVJBTkNFXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cblxuICAgICAgICAgICAgICAgIGNhc2UgQmxhY2tqYWNrR2FtZUNvbW1hbmQuU1BMSVQ6XG4gICAgICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuZ2FtZS5zcGxpdCgpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgICAgIGNhc2UgQmxhY2tqYWNrR2FtZUNvbW1hbmQuSElUOlxuICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmdhbWUuaGl0KCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICAgICAgY2FzZSBCbGFja2phY2tHYW1lQ29tbWFuZC5ET1VCTEVfRE9XTjpcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5nYW1lLmRvdWJsZURvd24oKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBjYXNlIEJsYWNramFja0dhbWVDb21tYW5kLlNUQU5EOlxuICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmdhbWUuc3RhbmQoKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAvLyAqKiogU1RPUCBTWU5DSU5HOiAqKipcbiAgICAgICAgdGhpcy5wbGF5ZXJzQWRkaXRpb25hbENhcmRJbmRleCA9IHVuZGVmaW5lZDtcblxuICAgICAgICB0aGlzLmlzUmVzdW1pbmcgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBwbGFjZUJldChiZXRBbW91bnQ6IEVPU0Fzc2V0QW1vdW50KSB7XG4gICAgICAgIGlmICh0aGlzLmNvbXBvbmVudC5pc0luc3VmZmljaWVudEZ1bmRzKSB7XG4gICAgICAgICAgICB0aGlzLmFwcC5vcGVuSW5zdWZmaWNlbnRGdW5kc01vZGFsKCk7XG5cbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yUmVzcG9uc2UgPSB0cnVlO1xuXG5cbiAgICAgICAgY29uc3QgY29tcG9uZW50ID0gdGhpcy5jb21wb25lbnQ7XG4gICAgICAgIGNvbnN0IHdhbGxldCA9IGNvbXBvbmVudC53YWxsZXQ7XG5cblxuICAgICAgICBjb25zdCBtZW1vID0gYCR7U3RyaW5nKGJldEFtb3VudC5pbnRlZ2VyKX0tJHt3YWxsZXQucHVibGljS2V5fS0ke2NvbXBvbmVudC5zdGF0ZS5yZWZlcnJlcn0tJHtjb21wb25lbnQuZ2V0U2VlZChmYWxzZSl9YDtcblxuICAgICAgICBCaWcuUk0gPSBSb3VuZGluZ01vZGUuUm91bmREb3duO1xuICAgICAgICBsZXQgYW10RGVwb3NpdCA9IG5ldyBCaWcoYmV0QW1vdW50LmRlY2ltYWwpLnRpbWVzKFwiNC41XCIpO1xuICAgICAgICBjb25zdCBhbXRCYWxhbmNlOiBzdHJpbmcgPSBjb21wb25lbnQud2FsbGV0LmdldEJhbGFuY2UoY29tcG9uZW50LnNlbGVjdGVkVG9rZW4uc3ltYm9sKTtcbiAgICAgICAgY29uc3QgcHJlY2lzaW9uID0gY29tcG9uZW50LnNlbGVjdGVkVG9rZW4ucHJlY2lzaW9uO1xuXG4gICAgICAgIGlmIChhbXREZXBvc2l0Lmd0KGFtdEJhbGFuY2UpKSB7XG4gICAgICAgICAgICAvLyBJZiB0aGVyZSBpcyBub3QgZW5vdWdoIGZ1bmRzIGZvciA0LjUgdGhlbiBkbyBub3QgZGVwb3NpdCB0aGUgdW5ldmVuIHJlbWFpbmRlciB0aGF0IGNhbnQgYmUgdXNlZFxuICAgICAgICAgICAgY29uc3QgZmFjdG9yID0gbmV3IEJpZyhhbXRCYWxhbmNlKS5kaXYoYmV0QW1vdW50LmRlY2ltYWwpLnRvRml4ZWQoMCk7XG4gICAgICAgICAgICBhbXREZXBvc2l0ID0gbmV3IEJpZyhiZXRBbW91bnQuZGVjaW1hbCkudGltZXMoZmFjdG9yKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGRlcG9zaXRBbW91bnQgPSBjb21wb25lbnQuc2VsZWN0ZWRUb2tlbi5nZXRBc3NldEFtb3VudChcbiAgICAgICAgICAgIGFtdERlcG9zaXQudG9GaXhlZChwcmVjaXNpb24pXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgZW9zQWNjb3VudHMgPSB0aGlzLmFwcC5lb3MuYWNjb3VudHM7XG5cbiAgICAgICAgY29uc3QgdHJhbnNmZXJzID0gW3tcbiAgICAgICAgICAgIHRvQWNjb3VudDogZW9zQWNjb3VudHMuZ2FtZXMuYmxhY2tqYWNrLFxuICAgICAgICAgICAgYW1vdW50OiBkZXBvc2l0QW1vdW50LFxuICAgICAgICAgICAgbWVtb1xuICAgICAgICB9XTtcblxuICAgICAgICBpZiAoY29tcG9uZW50LmlzSmFja3BvdEJldCkge1xuICAgICAgICAgICAgdHJhbnNmZXJzLnB1c2goe1xuICAgICAgICAgICAgICAgIHRvQWNjb3VudDogZW9zQWNjb3VudHMuamFja3BvdCxcbiAgICAgICAgICAgICAgICBhbW91bnQ6IGNvbXBvbmVudC5zZWxlY3RlZFRva2VuLmphY2twb3RUaWNrZXRQcmljZSxcbiAgICAgICAgICAgICAgICBtZW1vOiBlb3NBY2NvdW50cy5nYW1lcy5ibGFja2phY2tcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCBwcm9taXNlID0gd2FsbGV0LnRyYW5zZmVyKHRyYW5zZmVycyk7XG5cbiAgICAgICAgcHJvbWlzZS50aGVuKFxuICAgICAgICAgICAgKHJlc3VsdCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHR4bklkID0gcmVzdWx0LnRyYW5zYWN0aW9uX2lkO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5wbGFjZWRCZXRJZCA9IGNvbXBvbmVudC5zdGF0ZS5iZXRzLm9uQmV0UGxhY2VkKHR4bklkKTtcblxuXG4gICAgICAgICAgICAgICAgdGhpcy5jb21wb25lbnQudHJhY2tCZXRXaXRoR29vZ2xlQW5hbHl0aWNzKFxuICAgICAgICAgICAgICAgICAgICAnYmxhY2tqYWNrJyxcbiAgICAgICAgICAgICAgICAgICAgYmV0QW1vdW50LmRlY2ltYWwsXG4gICAgICAgICAgICAgICAgICAgIHR4bklkLFxuICAgICAgICAgICAgICAgICAgICBmYWxzZVxuICAgICAgICAgICAgICAgICk7XG5cblxuICAgICAgICAgICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvclJlc3BvbnNlID0gZmFsc2U7XG5cbiAgICAgICAgICAgICAgICB0aGlzLndhaXRGb3JHYW1lKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgKGVycm9yRGF0YSkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvclJlc3BvbnNlID0gZmFsc2U7XG5cbiAgICAgICAgICAgICAgICBjb25zdCBlcnJvciA9IG5ldyBFb3NFcnJvclBhcnNlcihlcnJvckRhdGEpO1xuXG4gICAgICAgICAgICAgICAgY29tcG9uZW50LmFsZXJ0LmVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICApO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgd2FpdEZvckdhbWUoKSB7XG4gICAgICAgIHRoaXMuX2lzV2FpdGluZ0ZvclJlc3BvbnNlID0gdHJ1ZTtcblxuICAgICAgICBjb25zdCBzdGF0ZSA9IGF3YWl0IHRoaXMuc2VuZEdhbWVDb21tYW5kPElCbGFja2phY2tHYW1lU3RhdGU+KFxuICAgICAgICAgICAgQmxhY2tqYWNrR2FtZUNvbW1hbmQuU1RBUlRfR0FNRSxcbiAgICAgICAgICAgIHRoaXMuYWNjb3VudERhdGFcbiAgICAgICAgKTtcblxuICAgICAgICBhd2FpdCB0aGlzLnN0YXJ0R2FtZShzdGF0ZSk7XG5cbiAgICAgICAgdGhpcy5pc1Jlc3VtaW5nID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBzdGFydEdhbWUoc3RhdGU6SUJsYWNramFja0dhbWVTdGF0ZSkge1xuICAgICAgICB0aGlzLmFjdGlvbklkID0gMDtcbiAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yUmVzcG9uc2UgPSBmYWxzZTtcblxuICAgICAgICAvL3RoaXMuaXNEZWFsZXJCbGFja2phY2sgPSBzdGF0ZS5pc0RlYWxlckJsYWNramFjaztcbiAgICAgICAgdGhpcy5kZWFsZXJzQWRkaXRpb25hbENhcmRzID0gc3RhdGUuYWRkaXRpb25hbERlYWxlckNhcmRzO1xuXG4gICAgICAgIHRoaXMuc3RhdGUgPSBzdGF0ZTtcblxuICAgICAgICBjb25zdCB0b2tlbiA9IHN0YXRlLmluaXRpYWxCZXQudG9rZW47XG5cbiAgICAgICAgYXdhaXQgdGhpcy5nYW1lLnN0YXJ0KFxuICAgICAgICAgICAgc3RhdGUuaW5pdGlhbENhcmRzLFxuICAgICAgICAgICAgc3RhdGUuaW5pdGlhbEJldC5hbW91bnRBc0ludGVnZXIgL1xuICAgICAgICAgICAgICAgIE1hdGgucG93KDEwLHRva2VuLnByZWNpc2lvbilcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBhc3luYyByZXNwb25kVG9JbnN1cmFuY2UoeWVzOiBib29sZWFuKTpQcm9taXNlPGJvb2xlYW4+IHtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPVxuICAgICAgICAgICAgdGhpcy5pc1N5bmNpbmcgP1xuICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUgOlxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuc2VuZEdhbWVDb21tYW5kPHtpc0RlYWxlckJsYWNramFjazpib29sZWFufT4oXG4gICAgICAgICAgICAgICAgICAgIHllcyA/XG4gICAgICAgICAgICAgICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5ZRVNfVE9fSU5TVVJBTkNFIDpcbiAgICAgICAgICAgICAgICAgICAgICAgIEJsYWNramFja0dhbWVDb21tYW5kLk5PX1RPX0lOU1VSQU5DRVxuICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmlzRGVhbGVyQmxhY2tqYWNrO1xuICAgIH1cblxuICAgIGFzeW5jIHBlZWtGb3JCbGFja2phY2soKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID1cbiAgICAgICAgICAgIHRoaXMuaXNTeW5jaW5nIHx8IHRoaXMuc3RhdGUuaXNEZWFsZXJCbGFja2phY2sgIT0gdW5kZWZpbmVkID9cbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlIDpcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnNlbmRHYW1lQ29tbWFuZDx7aXNEZWFsZXJCbGFja2phY2s6Ym9vbGVhbn0+KFxuICAgICAgICAgICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5QRUVLX0ZPUl9CTEFDS0pBQ0tcbiAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiByZXNwb25zZS5pc0RlYWxlckJsYWNramFjaztcbiAgICB9XG5cblxuICAgIGFzeW5jIHNwbGl0KCk6IFByb21pc2U8bnVtYmVyW10+IHtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPVxuICAgICAgICAgICAgdGhpcy5pc1N5bmNpbmcgP1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0QWRkaXRpb25hbFBsYXllckNhcmRzKDIpIDpcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnNlbmRHYW1lQ29tbWFuZDxudW1iZXJbXT4oXG4gICAgICAgICAgICAgICAgICAgIEJsYWNramFja0dhbWVDb21tYW5kLlNQTElUXG4gICAgICAgICAgICAgICAgKTtcblxuICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgfVxuXG4gICAgYXN5bmMgaGl0KGhhbmRJbmRleDogbnVtYmVyKTogUHJvbWlzZTxudW1iZXI+IHtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPVxuICAgICAgICAgICAgdGhpcy5pc1N5bmNpbmcgP1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0QWRkaXRpb25hbFBsYXllckNhcmRzKDEpIDpcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnNlbmRHYW1lQ29tbWFuZDxudW1iZXJbXT4oXG4gICAgICAgICAgICAgICAgICAgIEJsYWNramFja0dhbWVDb21tYW5kLkhJVFxuICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlWzBdO1xuICAgIH1cblxuICAgIGFzeW5jIGRvdWJsZURvd24oaGFuZEluZGV4OiBudW1iZXIpOiBQcm9taXNlPG51bWJlcj4ge1xuICAgICAgICBjb25zdCByZXNwb25zZSA9XG4gICAgICAgICAgICB0aGlzLmlzU3luY2luZyA/XG4gICAgICAgICAgICAgICAgdGhpcy5nZXRBZGRpdGlvbmFsUGxheWVyQ2FyZHMoMSkgOlxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuc2VuZEdhbWVDb21tYW5kPG51bWJlcltdPihcbiAgICAgICAgICAgICAgICAgICAgQmxhY2tqYWNrR2FtZUNvbW1hbmQuRE9VQkxFX0RPV05cbiAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiByZXNwb25zZVswXTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldEFkZGl0aW9uYWxQbGF5ZXJDYXJkcyhudW1PZkNhcmRzOm51bWJlcik6bnVtYmVyW10ge1xuICAgICAgICBjb25zdCBjYXJkSWRzOm51bWJlcltdID0gW107XG5cbiAgICAgICAgZm9yICh2YXIgaT0wOyBpIDwgbnVtT2ZDYXJkczsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMucGxheWVyc0FkZGl0aW9uYWxDYXJkSW5kZXgrKztcblxuICAgICAgICAgICAgY29uc3QgY2FyZCA9IHRoaXMuc3RhdGUuYWRkaXRpb25hbFBsYXllckNhcmRzW2luZGV4XTtcblxuICAgICAgICAgICAgY2FyZElkcy5wdXNoKGNhcmQpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGNhcmRJZHM7XG4gICAgfVxuXG4gICAgYXN5bmMgc3RhbmQoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgICAgIGlmICghdGhpcy5pc1N5bmNpbmcpIHtcbiAgICAgICAgICAgIHJldHVybiBhd2FpdCB0aGlzLnNlbmRHYW1lQ29tbWFuZDxib29sZWFuPihcbiAgICAgICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5TVEFORFxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIGdldERlYWxlcnNTZWNvbmRDYXJkKCk6IFByb21pc2U8bnVtYmVyPiB7XG4gICAgICAgIGNvbnN0IGNhcmRzID1cbiAgICAgICAgICAgIHRoaXMuaXNTeW5jaW5nID9cbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmFkZGl0aW9uYWxEZWFsZXJDYXJkcyA6XG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5zZW5kR2FtZUNvbW1hbmQ8bnVtYmVyW10+KFxuICAgICAgICAgICAgICAgICAgICBCbGFja2phY2tHYW1lQ29tbWFuZC5HRVRfREVBTEVSU19DQVJEU1xuICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5kZWFsZXJzQWRkaXRpb25hbENhcmRzID0gY2FyZHMuc2xpY2UoMSk7XG5cbiAgICAgICAgcmV0dXJuIGNhcmRzWzBdO1xuICAgIH1cblxuICAgIGFzeW5jIGdldERlYWxlcnNBZGRpdGlvbmFsQ2FyZHMoKTogUHJvbWlzZTxudW1iZXJbXT4ge1xuICAgICAgICByZXR1cm4gdGhpcy5kZWFsZXJzQWRkaXRpb25hbENhcmRzO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBzZW5kR2FtZUNvbW1hbmQ8VD4oXG4gICAgICAgIGNvbW1hbmROYW1lOnN0cmluZyxcbiAgICAgICAgYXJnczpzdHJpbmdbXSA9IFtdXG4gICAgKTpQcm9taXNlPFQ+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPFQ+KCAocmVzb2x2ZSxyZWplY3QpPT57XG4gICAgICAgICAgICBjb25zdCBpc1Jlc3VtZUdhbWVDb21tYW5kID1cbiAgICAgICAgICAgICAgICBjb21tYW5kTmFtZSA9PSBCbGFja2phY2tHYW1lQ29tbWFuZC5SRVNVTUVfR0FNRTtcblxuICAgICAgICAgICAgY29uc3QgaXNTdGFydEdhbWVDb21tYW5kID1cbiAgICAgICAgICAgICAgICBjb21tYW5kTmFtZSA9PSBCbGFja2phY2tHYW1lQ29tbWFuZC5TVEFSVF9HQU1FO1xuXG5cbiAgICAgICAgICAgIGNvbnN0IGFjdGlvbklkID1cbiAgICAgICAgICAgICAgICBpc1Jlc3VtZUdhbWVDb21tYW5kIHx8IGlzU3RhcnRHYW1lQ29tbWFuZCA/XG4gICAgICAgICAgICAgICAgICAgIDAgOlxuICAgICAgICAgICAgICAgICAgICArK3RoaXMuYWN0aW9uSWQ7XG5cbiAgICAgICAgICAgIGNvbnN0IHBhcnRzID0gW1xuICAgICAgICAgICAgICAgIGNvbW1hbmROYW1lLFxuICAgICAgICAgICAgICAgIGlzU3RhcnRHYW1lQ29tbWFuZCA/XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucGxhY2VkQmV0SWQgOlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdhbWVJZCxcbiAgICAgICAgICAgICAgICBhY3Rpb25JZFxuICAgICAgICAgICAgXS5jb25jYXQoYXJncyk7XG4gICAgICAgICAgICAvLyBkYXRhIGZvcm1hdDpjb21tYW5kTmFtZX5nYW1lSWR+YWN0aW9uSWR+ZGF0YS4uLlxuICAgICAgICAgICAgY29uc3QgZGF0YSA9IHBhcnRzLmpvaW4oJ34nKTtcblxuICAgICAgICAgICAgaWYgKCFpc1Jlc3VtZUdhbWVDb21tYW5kKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yUmVzcG9uc2UgPSB0cnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLndhbGxldC5zaWduKGRhdGEsJycpXG4gICAgICAgICAgICAudGhlbiggKGluZm8pPT57XG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZygnU2VuZGluZyBHYW1lIENvbW1hbmQ6ICcpXG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgICAgIHRoaXMuYXBwLmJsYWNramFjay5jbGllbnQubWVzc2FnZU1hbmFnZXIuc2VuZEdhbWVDb21tYW5kPFQ+KFxuICAgICAgICAgICAgICAgICAgICBkYXRhLFxuICAgICAgICAgICAgICAgICAgICBpbmZvXG4gICAgICAgICAgICAgICAgKS50aGVuKCAocmVzdWx0KT0+e1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9pc1dhaXRpbmdGb3JSZXNwb25zZSA9IGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9KS5jYXRjaCggKGVycm9yKT0+e1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9pc1dhaXRpbmdGb3JSZXNwb25zZSA9IGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWxlcnQuZXJyb3IoZXJyb3IuY29kZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pLmNhdGNoKCAoZXJyb3IpPT57XG4gICAgICAgICAgICAgICAgdGhpcy5faXNXYWl0aW5nRm9yUmVzcG9uc2UgPSBmYWxzZTtcblxuICAgICAgICAgICAgICAgIGNvbnN0IGUgPSBuZXcgRW9zRXJyb3JQYXJzZXIoZXJyb3IpO1xuICAgICAgICAgICAgICAgIHRoaXMuYWxlcnQuZXJyb3IoZS5tZXNzYWdlKTtcblxuICAgICAgICAgICAgICAgIHJlamVjdChlcnJvcik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgaXNTeW5jaW5nKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5wbGF5ZXJzQWRkaXRpb25hbENhcmRJbmRleCAhPSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgZ2FtZUlkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZSA/XG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5pZCA6XG4gICAgICAgICAgICAgICAgJzAnO1xuICAgIH1cblxuICAgIGdldCBpc1dhaXRpbmdGb3JSZXNwb25zZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzV2FpdGluZ0ZvclJlc3BvbnNlO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0IGdhbWUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbXBvbmVudC5nYW1lO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0IGFjY291bnREYXRhKCkge1xuICAgICAgICBjb25zdCB3YWxsZXQgPSB0aGlzLndhbGxldDtcblxuICAgICAgICByZXR1cm4gIHdhbGxldC5pc0Vhc3lBY2NvdW50ID9cbiAgICAgICAgICAgICAgICBbdGhpcy5hY2NvdW50TmFtZXMuZWFzeUFjY291bnQsd2FsbGV0LmFjY291bnRJZCx3YWxsZXQuYWNjb3VudE5hbWVdIDpcbiAgICAgICAgICAgICAgICBbd2FsbGV0LmFjY291bnROYW1lXTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCB3YWxsZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC53YWxsZXQ7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgYWxlcnQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC5hbGVydDtcbiAgICB9XG59XG4iXX0=