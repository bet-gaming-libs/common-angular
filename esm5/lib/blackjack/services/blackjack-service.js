/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/services/blackjack-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BlackjackClient } from '../server/bj-client';
var BlackjackAppService = /** @class */ (function () {
    function BlackjackAppService(app) {
        this.client = new BlackjackClient(app);
    }
    /**
     * @return {?}
     */
    BlackjackAppService.prototype.init = /**
     * @return {?}
     */
    function () {
        this.client.connect();
    };
    return BlackjackAppService;
}());
export { BlackjackAppService };
if (false) {
    /** @type {?} */
    BlackjackAppService.prototype.client;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLXNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1hbmd1bGFyLyIsInNvdXJjZXMiOlsibGliL2JsYWNramFjay9zZXJ2aWNlcy9ibGFja2phY2stc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUt0RDtJQUlJLDZCQUFZLEdBQW9CO1FBQzVCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxlQUFlLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7OztJQUVELGtDQUFJOzs7SUFBSjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUNMLDBCQUFDO0FBQUQsQ0FBQyxBQVhELElBV0M7Ozs7SUFURyxxQ0FBaUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCbGFja2phY2tDbGllbnQgfSBmcm9tICcuLi9zZXJ2ZXIvYmotY2xpZW50JztcbmltcG9ydCB7IElNYWluQXBwQ29udGV4dCB9IGZyb20gJy4uLy4uL19jb21tb24vc2VydmljZXMvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQmxhY2tqYWNrQXBwU2VydmljZSB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGNsYXNzIEJsYWNramFja0FwcFNlcnZpY2UgaW1wbGVtZW50cyBJQmxhY2tqYWNrQXBwU2VydmljZVxue1xuICAgIHJlYWRvbmx5IGNsaWVudDogQmxhY2tqYWNrQ2xpZW50O1xuXG4gICAgY29uc3RydWN0b3IoYXBwOiBJTWFpbkFwcENvbnRleHQpIHtcbiAgICAgICAgdGhpcy5jbGllbnQgPSBuZXcgQmxhY2tqYWNrQ2xpZW50KGFwcCk7XG4gICAgfVxuXG4gICAgaW5pdCgpIHtcbiAgICAgICAgdGhpcy5jbGllbnQuY29ubmVjdCgpO1xuICAgIH1cbn1cbiJdfQ==