import { Subject } from 'rxjs';
import { CommonModule } from '@angular/common';
import { __awaiter } from 'tslib';
import { Big } from 'big.js';
import { HostListener, Injectable, Component, NgModule, ViewChild, Input } from '@angular/core';
import { sleep, nameOfRank, CardRankNumber, BlackjackGameCommand, SocketMessageManager, BlackjackMessage } from 'earnbet-common';
import { SocketClientBase } from 'earnbet-common-front-end';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/base/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/business-rules/tokens.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const TokenSymbol = {
    BTC: "BTC",
    ETH: "ETH",
    EOS: "EOS",
    BET: "BET",
    LTC: "LTC",
    XRP: "XRP",
    BCH: "BCH",
    BNB: "BNB",
    WAX: "WAX",
    TRX: "TRX",
    LINK: "LINK",
    DAI: "DAI",
    USDC: "USDC",
    USDT: "USDT",
    FUN: "FUN",
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/base/game-component-base.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const INDEX_OF_MAX_CHIP = 7;
/**
 * @abstract
 */
class GameComponentBase {
    /**
     * @param {?} app
     * @param {?} googleAnalytics
     * @param {?} soundPlayer
     */
    constructor(app, googleAnalytics, soundPlayer) {
        this.app = app;
        this.googleAnalytics = googleAnalytics;
        this.soundPlayer = soundPlayer;
        this.autoBetNonce = 0;
        this._isJackpotBet = false;
        this.selectedChip = 1;
        this.showTooltip = false;
        this.processBetStyle = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.app.scrollToTopOfPage();
        this.app.settings.setListener(this);
        if (this.selectedToken.symbol == 'BET') {
            this.settings.selectCurrency('EOS');
        }
    }
    /**
     * @param {?} i
     * @return {?}
     */
    selectChip(i) {
        this.selectedChip = i;
        this.soundPlayer.play('chipPlace');
        this.onChipSelected();
    }
    /**
     * @protected
     * @return {?}
     */
    onChipSelected() { }
    /**
     * @return {?}
     */
    toggleTooltip() {
        this.showTooltip = !this.showTooltip;
    }
    /**
     * @return {?}
     */
    documentClick() {
        this.showTooltip = false;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isJackpotBet(value) {
        if (this.isJackpotDisabled) {
            return;
        }
        this._isJackpotBet = value;
    }
    /**
     * @return {?}
     */
    get isJackpotBet() {
        return !this.isJackpotDisabled &&
            this._isJackpotBet;
    }
    /**
     * @return {?}
     */
    get isJackpotDisabled() {
        /** @type {?} */
        const amount = Number(this.betAmount.decimal);
        return amount < this.selectedToken.jackpot.minimumBet;
    }
    /**
     * @param {?} isAutoBet
     * @return {?}
     */
    getSeed(isAutoBet) {
        // seed for randomness
        /** @type {?} */
        const seed = this.state.seedForRandomness +
            // add nonce for auto-bet to fairness seed
            (isAutoBet || true ?
                ',' + (++this.autoBetNonce) :
                '');
        return seed;
    }
    /**
     * @param {?} gameName
     * @param {?} betAmount
     * @param {?} txnId
     * @param {?} isAutoBet
     * @return {?}
     */
    trackBetWithGoogleAnalytics(gameName, betAmount, txnId, isAutoBet) {
        /** @type {?} */
        const price = this.currency.getPrice(this.selectedToken.symbol);
        /** @type {?} */
        const amountInUSD = price ?
            (price * Number(betAmount)).toFixed(2) :
            undefined;
        if (amountInUSD) {
            this.googleAnalytics.addTransaction(txnId, gameName, this.selectedToken.symbol, amountInUSD, isAutoBet, this.isJackpotBet);
        }
    }
    /**
     * @protected
     * @param {?} result
     * @return {?}
     */
    updateBalances(result) {
        return __awaiter(this, void 0, void 0, function* () {
            /*** CALCULATE NEW TOKEN BALANCES IN ADVANCE
             *  of the Bet being Resolved on-chain ***/
            /**
             * CALCULATE NEW TOKEN BALANCES IN ADVANCE
             *  of the Bet being Resolved on-chain **
             * @type {?}
             */
            const bet = Number(result.amount) /
                this.selectedToken.betTokenAirDropRate;
            /** @type {?} */
            let profit = result.profit;
            if (result.isJackpotBet) {
                profit -= Number(this.selectedToken.jackpot.ticketPrice);
            }
            /***/
            this.addToBalance = {
                main: profit,
                bet
            };
            // to animate balance in header
            this.app.state.onAddToBalance(true, profit);
            yield sleep(500);
            /*** UPDATE TOKEN BALANCES ***/
            // update main token balance
            this.wallet.addToBalance(this.selectedToken.symbol, profit);
            // update BET token balance
            this.wallet.addToBalance(TokenSymbol.BET, bet);
            /***/
            yield sleep(1500);
            this.addToBalance = undefined;
            this.app.state.onAddToBalance(false);
        });
    }
    // onTooltipShowBet(){
    //     document.getElementById("tooltipBet").setAttribute('data-tooltip', 'aaa');
    // }
    /**
     * @return {?}
     */
    get tooltipBet() {
        /** @type {?} */
        const tokenAlt = this.selectedToken.symbol;
        /** @type {?} */
        const airDrop = this.selectedToken.betTokenAirDropRate;
        return this.settings.translation.tooltipDividends("1", `${airDrop} ${tokenAlt}`);
    }
    /**
     * @return {?}
     */
    get jackpotToolTip() {
        /** @type {?} */
        const token = this.selectedToken;
        /** @type {?} */
        const jackpot = token.jackpot;
        return this.settings.translation.clickThisBoxToReceive1JackpotSpinForPrice(Number(jackpot.ticketPrice) + " " + token.symbol);
        // Currently not using a separate minimum bet for jackpot, 
        // it is currently the same as the standard minimum bet
        //"Bet must be at least "+jackpot.minimumBet+" "+token.symbol+" to enter.";
    }
    /**
     * @return {?}
     */
    get isInsufficientFunds() {
        return new Big(this.currencyBalance).lt(this.selectedToken.minimumBetAmount) ||
            new Big(this.betAmount.decimal).gt(this.currencyBalance);
    }
    /**
     * @return {?}
     */
    get isMaxChipSelected() {
        return this.selectedChip == INDEX_OF_MAX_CHIP;
    }
    /**
     * @return {?}
     */
    get currencyBalance() {
        return this.settings.selectedTokenBalance;
    }
    /**
     * @return {?}
     */
    get chips() {
        return this.selectedToken.chips;
    }
    /**
     * @return {?}
     */
    get selectedTokenPrice() {
        return this.currency.getPrice(this.selectedToken.symbol);
    }
    /**
     * @return {?}
     */
    get currency() {
        return this.app.currency;
    }
    /**
     * @return {?}
     */
    get selectedToken() {
        return this.settings.selectedToken;
    }
    /**
     * @return {?}
     */
    get wallet() {
        return this.settings.wallet;
    }
    /**
     * @return {?}
     */
    get state() {
        return this.app.state;
    }
    /**
     * @return {?}
     */
    get translation() {
        return this.settings.translation;
    }
    /**
     * @return {?}
     */
    get settings() {
        return this.app.settings;
    }
    /**
     * @return {?}
     */
    get alert() {
        return this.app.alert;
    }
    /**
     * @return {?}
     */
    get bankroll() {
        return this.app.bankroll;
    }
}
GameComponentBase.propDecorators = {
    documentClick: [{ type: HostListener, args: ['document:click', [],] }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/eos/eos-error-parser.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class EosErrorParser {
    /**
     * @param {?} data
     */
    constructor(data) {
        this._message = data.message;
        try {
            data = JSON.parse(data);
            if (data.error && data.error.details) {
                this._message = data.error.details[0].message;
            }
        }
        catch (error) {
        }
    }
    /**
     * @return {?}
     */
    get isIncorrectNonce() {
        return this.message ==
            'assertion failure with message: Incorrect nonce.';
    }
    /**
     * @return {?}
     */
    get message() {
        return this._message;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/eos/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/game/card-controller.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const SUITS = ['hearts', 'diamonds', 'spades', 'clubs'];
class CardController {
    /**
     * @param {?} soundPlayer
     */
    constructor(soundPlayer) {
        this.soundPlayer = soundPlayer;
        this._dealt = false;
        this._flipped = false;
        this._isRevealed = false;
    }
    /**
     * @return {?}
     */
    deal() {
        this._dealt = true;
        this.soundPlayer.play('cardPlace');
    }
    /**
     * @param {?} data
     * @param {?=} rankNumber
     * @param {?=} cardId
     * @return {?}
     */
    reveal(data, rankNumber = undefined, cardId = undefined) {
        this._rank = data.rankName.toLowerCase();
        this._suit = SUITS[data.suit];
        this._rankNumber = rankNumber;
        this._cardId = cardId;
        this._isRevealed = true;
    }
    /**
     * @return {?}
     */
    flip() {
        this._flipped = true;
        this.soundPlayer.play('cardFlip');
    }
    /**
     * @return {?}
     */
    unflip() {
        this._flipped = false;
        this.soundPlayer.play('cardFlip');
    }
    /**
     * @return {?}
     */
    dealAndFlip() {
        this._dealt = true;
        this._flipped = true;
        this.soundPlayer.play('cardFlip');
    }
    /**
     * @return {?}
     */
    get dealt() {
        return this._dealt;
    }
    /**
     * @return {?}
     */
    get flipped() {
        return this._flipped;
    }
    /**
     * @return {?}
     */
    get suit() {
        return this._suit;
    }
    /**
     * @return {?}
     */
    get card() {
        return this.rank;
    }
    /**
     * @return {?}
     */
    get rank() {
        return this._rank;
    }
    /**
     * @return {?}
     */
    get rankNumber() {
        return this._rankNumber;
    }
    /**
     * @return {?}
     */
    get cardId() {
        return this._cardId;
    }
    /**
     * @return {?}
     */
    get isRevealed() {
        return this._isRevealed;
    }
    /**
     * @return {?}
     */
    get data() {
        return {
            suit: this.suit,
            dealt: this._dealt,
            flipped: this.flipped,
            card: this.card
        };
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/game/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/business-rules/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/util/math-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} amount
 * @param {?} digits
 * @param {?=} precision
 * @return {?}
 */
function toTotalDigits(amount, digits, precision = undefined) {
    return numStringToTotalDigits(String(amount), digits, precision);
}
/**
 * @param {?} amount
 * @param {?} digits
 * @param {?=} precision
 * @return {?}
 */
function numStringToTotalDigits(amount, digits, precision = undefined) {
    /** @type {?} */
    const rounded = roundStringDownWithPrecision(amount, digits + 1);
    /** @type {?} */
    const numOfDigitsBeforeDecimal = getNumOfDigitsBeforeDecimal(rounded);
    /** @type {?} */
    let numOfDigitsAfterDecimal = digits - numOfDigitsBeforeDecimal;
    if (precision != undefined &&
        numOfDigitsAfterDecimal > precision) {
        numOfDigitsAfterDecimal = precision;
    }
    return numOfDigitsAfterDecimal > 0 ?
        roundStringDownWithPrecision(rounded, numOfDigitsAfterDecimal) :
        rounded.split('.')[0];
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
function roundDownWithPrecision(amount, precision) {
    //amount = Number(amount);
    return roundStringDownWithPrecision(String(amount), precision);
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
function roundDownWithoutTrailingZeros(amount, precision) {
    /** @type {?} */
    const valueWithAllDigits = roundStringDownWithPrecision(amount, precision);
    /** @type {?} */
    const parts = valueWithAllDigits.split('.');
    /** @type {?} */
    const digitsBeforeDecimal = parts[0];
    /** @type {?} */
    const digitsAfterDecimal = parts[1];
    /** @type {?} */
    let numOfDecimalDigitsToInclude = 0;
    for (var i = digitsAfterDecimal.length - 1; i > -1; i--) {
        /** @type {?} */
        const digit = digitsAfterDecimal[i];
        if (digit != '0') {
            numOfDecimalDigitsToInclude = i + 1;
            break;
        }
    }
    return numOfDecimalDigitsToInclude > 0 ?
        digitsBeforeDecimal + '.' + digitsAfterDecimal.substr(0, numOfDecimalDigitsToInclude) :
        digitsBeforeDecimal;
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
function roundStringDownWithPrecision(amount, precision) {
    /** @type {?} */
    var parts = String(amount).split('.');
    /** @type {?} */
    var isWholeNumber = parts.length == 1;
    /** @type {?} */
    var rounded = Number(parts[0] + '.' +
        (isWholeNumber ?
            '0' :
            parts[1].substr(0, precision)));
    /** @type {?} */
    var result = rounded.toFixed(precision);
    return result;
}
/**
 * @param {?} number
 * @return {?}
 */
function getNumOfDigitsBeforeDecimal(number) {
    /** @type {?} */
    const parts = number.split('.');
    return parts[0].length;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/math/integer-math.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class IntBasedNum {
    /**
     * @param {?} precision
     * @param {?=} decimalValue
     * @param {?=} bigDecimal
     */
    constructor(precision, decimalValue = 0, bigDecimal = undefined) {
        this.precision = precision;
        /** @type {?} */
        const decimal = bigDecimal == undefined ?
            new Big(decimalValue).round(precision, 0 /* RoundDown */) :
            bigDecimal;
        this.factor = Math.pow(10, precision);
        this._integer = decimal.times(this.factor).round(0, 0 /* RoundDown */);
    }
    // CLASS
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    static fromInteger(integer, precision) {
        return new IntBasedNum(precision, undefined, new Big(integer).div(Math.pow(10, precision)).round(precision, 0 /* RoundDown */));
    }
    /**
     * MUTATING METHODS **
     * @param {?} other
     * @return {?}
     */
    addAndReplace(other) {
        if (this.precision != other.precision) {
            throw new Error('precision of both operands must be the same!');
        }
        /** @type {?} */
        const sum = this.integer.plus(other.integer);
        this._integer = sum;
        //return this;
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} other
     * @return {THIS}
     */
    subtractAndReplace(other) {
        if ((/** @type {?} */ (this)).precision != other.precision) {
            throw new Error('precision of both operands must be the same!');
        }
        /** @type {?} */
        const diff = (/** @type {?} */ (this)).integer.minus(other.integer);
        (/** @type {?} */ (this))._integer = diff;
        return (/** @type {?} */ (this));
    }
    /**
     *
     * @param {?} other
     * @return {?}
     */
    multiply(other) {
        return integerMultiplication(this, other);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    multiplyDecimal(decimal) {
        /** @type {?} */
        const other = new IntBasedNum(this.precision, decimal);
        return integerMultiplication(this, other);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    divideByDecimal(decimal) {
        /** @type {?} */
        const other = new IntBasedNum(this.precision, decimal.toString());
        return integerDivision(this, other);
    }
    /**
     * @return {?}
     */
    get reciprocal() {
        if (this._reciprocal == undefined) {
            this._reciprocal = integerDivision(new IntBasedNum(this.precision, '1'), this);
        }
        return this._reciprocal;
    }
    /**
     * @return {?}
     */
    get opposite_() {
        return integerMultiplication(new IntBasedNum(this.precision, '-1'), this);
    }
    /**
     * @return {?}
     */
    get decimalAsNumber() {
        return Number(this.decimal);
    }
    /**
     * @return {?}
     */
    get integer() {
        return this._integer;
    }
    /**
     * @return {?}
     */
    toString() {
        return this.decimal;
    }
    /**
     * @return {?}
     */
    get decimalWithoutTrailingZeros() {
        return roundDownWithoutTrailingZeros(this.decimal, this.precision);
    }
    /**
     * @return {?}
     */
    get decimal() {
        Big.RM = 0 /* RoundDown */;
        return this.integer.div(this.factor).toFixed(this.precision);
    }
}
class AssetAmount extends IntBasedNum {
    /**
     * @param {?} data
     */
    constructor(data) {
        /** @type {?} */
        const parts = data.split(' ');
        /** @type {?} */
        const decimal = parts[0];
        /** @type {?} */
        const symbol = parts[1];
        /** @type {?} */
        const precision = decimal.split('.')[1].length;
        super(precision, decimal);
        this._symbol = symbol;
        this._isZero = Number(decimal) == 0;
    }
    /**
     * @return {?}
     */
    get isZero() {
        return this._isZero;
    }
    /**
     * @return {?}
     */
    get symbol() {
        return this._symbol;
    }
}
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerMultiplication(a, b) {
    if (a.precision < b.precision) {
        console.log(a.decimal);
        console.log(b.decimal);
        throw new Error('precision of a: ' + a.precision +
            ', precision of b: ' + b.precision +
            "\nprecision of A must be >= B!");
    }
    /** @type {?} */
    const product = a.integer.times(b.integer);
    /** @type {?} */
    const integer = product.div(b.factor)
        .round(0, 0 /* RoundDown */)
        .toString();
    /** @type {?} */
    const result = IntBasedNum.fromInteger(integer, a.precision);
    //console.log('INT Multiplication: '+a+' * '+b+' = '+result);
    return result;
}
// when doing division between two eos numbers then disregard
// the decimal after the integer
//export 
/**
 * @template T
 * @param {?} a
 * @param {?} b
 * @return {?}
 */
function integerDivision(a, b) {
    if (a.precision < b.precision) {
        throw new Error('precision of A must be >= B!');
    }
    /** @type {?} */
    const quotient = a.integer.div(b.integer);
    /** @type {?} */
    const integer = quotient.times(b.factor)
        .round(0, 0 /* RoundDown */)
        .toString();
    /** @type {?} */
    const result = IntBasedNum.fromInteger(integer, a.precision);
    console.log('INT Division: ' + a + ' / ' + b + ' = ' + result);
    return result;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/staking/constants.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const betTokenFactor = 10000;

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/staking/staking-level-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const stakingLevels = [
    { name: 'shrimp', stakePoints: 1000, houseEdgeReduction: 0.05, baccaratHouseEdge: 1.03 },
    { name: 'crab', stakePoints: 10000, houseEdgeReduction: 0.10, baccaratHouseEdge: 0.99 },
    { name: 'octopus', stakePoints: 25000, houseEdgeReduction: 0.12, baccaratHouseEdge: 0.98 },
    { name: 'squid', stakePoints: 50000, houseEdgeReduction: 0.15, baccaratHouseEdge: 0.96 },
    { name: 'fish', stakePoints: 100000, houseEdgeReduction: 0.20, baccaratHouseEdge: 0.92 },
    { name: 'blowfish', stakePoints: 250000, houseEdgeReduction: 0.25, baccaratHouseEdge: 0.89 },
    { name: 'dolphin', stakePoints: 500000, houseEdgeReduction: 0.35, baccaratHouseEdge: 0.82 },
    { name: 'shark', stakePoints: 1000000, houseEdgeReduction: 0.50, baccaratHouseEdge: 0.71 },
    { name: 'whale', stakePoints: 2500000, houseEdgeReduction: 0.75, baccaratHouseEdge: 0.60 },
    { name: 'blue-whale', stakePoints: 5000000, houseEdgeReduction: 1, baccaratHouseEdge: 0.50 },
];
/**
 * @param {?} stakeWeight
 * @return {?}
 */
function getHouseEdgeReductionAsDecimal(stakeWeight) {
    /** @type {?} */
    const levelIndex = getStakingLevelIndex(stakeWeight);
    /** @type {?} */
    const level = stakingLevels[levelIndex];
    return level ?
        level.houseEdgeReduction / 100 :
        0;
}
/**
 * @param {?} stakeWeight
 * @return {?}
 */
function getStakingLevelName(stakeWeight) {
    /** @type {?} */
    const levelIndex = getStakingLevelIndex(stakeWeight);
    /** @type {?} */
    const level = stakingLevels[levelIndex];
    return level ?
        level.name :
        undefined;
}
/**
 * @param {?} stakeWeight
 * @return {?}
 */
function getStakingLevelIndex(stakeWeight) {
    /** @type {?} */
    const points = stakeWeight / betTokenFactor;
    /** @type {?} */
    const firstLevel = stakingLevels[0];
    if (points < firstLevel.stakePoints) {
        return -1;
    }
    for (let i = 0; i < stakingLevels.length; i++) {
        /** @type {?} */
        const nextLevel = stakingLevels[i + 1];
        if (!nextLevel ||
            points < nextLevel.stakePoints) {
            return i;
        }
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/bets/bet-base.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
class BetBase {
    /**
     * @param {?} data
     */
    constructor(data) {
        this.id = data.id;
        this.time = new Date(data.resolvedMilliseconds);
        this.bettor = data.bettor;
        /** @type {?} */
        const parts = data.amount.split(' ');
        this.betAmount = new AssetAmount(data.amount);
        this.amount = this.betAmount.decimalWithoutTrailingZeros;
        this.tokenSymbol = parts[1];
        this.jackpotSpin = data.jackpotSpin;
        this.isJackpotBet = this.jackpotSpin && this.jackpotSpin.length > 0;
        this.stakeWeight =
            data.stakeWeight ?
                Number(data.stakeWeight) :
                0;
        this.stakingLevelName = getStakingLevelName(this.stakeWeight);
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/bets/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/media/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/services/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/services/overlay-modal.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OverlayModalService {
    constructor() {
        this.subject = new Subject();
    }
    /**
     * @param {?} message
     * @return {?}
     */
    open(message) {
        this.close();
        this.subject.next(message);
    }
    /**
     * @return {?}
     */
    close() {
        this.subject.next();
    }
    /**
     * @return {?}
     */
    getMessage() {
        return this.subject.asObservable();
    }
}
OverlayModalService.decorators = [
    { type: Injectable }
];
OverlayModalService.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/wallets/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-modal/blackjack-modal.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlackjackModalComponent {
    constructor() {
        this._isClosed = false;
        this.result = undefined;
    }
    /**
     * @param {?} result
     * @return {?}
     */
    showResult(result) {
        return __awaiter(this, void 0, void 0, function* () {
            this.result = result;
            this._isClosed = false;
            yield sleep(2 * 1000);
            this.close();
        });
    }
    /**
     * @return {?}
     */
    close() {
        this._isClosed = true;
    }
    /**
     * @return {?}
     */
    get isOpen() {
        return this.result != undefined &&
            !this._isClosed;
    }
    /**
     * @return {?}
     */
    get isWin() {
        return this.result && this.result.isWin;
    }
    /**
     * @return {?}
     */
    get isPush() {
        return this.result && this.result.isPush;
    }
    /**
     * @return {?}
     */
    get isBlackjack() {
        return this.result && this.result.isBlackjack;
    }
    /**
     * @return {?}
     */
    get isLoss() {
        return this.result && this.result.isLoss;
    }
    /**
     * @return {?}
     */
    get isBusted() {
        return this.result && this.result.isBusted;
    }
    /**
     * @return {?}
     */
    get isInsuranceSuccess() {
        return this.result && this.result.isInsuranceSuccess;
    }
    /**
     * @return {?}
     */
    get amountWon() {
        return this.result && this.result.amountWon;
    }
}
BlackjackModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-blackjack-modal',
                template: "<div *ngIf=\"isOpen\">\n  <div class=\"bj-modal\" [ngClass]=\"{\n                'red': isBusted || isLoss,\n                'green': isWin || isBlackjack,\n                'yellow': isPush,\n                'blue': isInsuranceSuccess\n              }\">\n  <button class=\"modal-close\" (click)='close()'>&times;</button>\n\n    <div class=\"align-items-center d-flex flex-column h-100 justify-content-center p-3\">\n      <div class=\"win\" *ngIf=\"isWin && !isBlackjack\">\n        <img class=\"mb-2 small-circle\" src=\"assets/img/blackjack/modal/win.png\" alt=\"win\">\n        <h1 class=\"bj-modal-title\">WIN!</h1>\n        <div class=\"points\">+ {{amountWon | number:'1.0-8'}}</div>\n      </div>\n\n      <div class=\"push\" *ngIf=\"isPush\">\n        <img class=\"mb-2 small-circle\" src=\"assets/img/blackjack/modal/push.png\" alt=\"push\">\n        <h1 class=\"bj-modal-title\">PUSH</h1>\n      </div>\n\n      <div class=\"blackjack\" *ngIf=\"isBlackjack\">\n        <img class=\"bj-image\" src=\"assets/img/blackjack/modal/blackjack.png\" alt=\"blackjack\">\n        <div class=\"points\">+ {{amountWon | number:'1.0-8'}}</div>\n      </div>\n\n      <div class=\"busted\" *ngIf=\"isBusted\">\n        <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/busted.png\" alt=\"busted\">\n        <h1 class=\"bj-modal-title\">BUSTED!</h1>\n      </div>\n\n     <div class=\"busted\" *ngIf=\"isLoss\">\n          <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/busted.png\" alt=\"busted\">\n          <h1 class=\"bj-modal-title\">LOSS</h1>\n      </div> \n\n\n      <!-- TODO: confirm state name -->\n      <div class=\"insurance\" *ngIf=\"isInsuranceSuccess\">\n        <img class=\"small-circle mb-2\" src=\"assets/img/blackjack/modal/success.png\" alt=\"insurance success\">\n        <h1 class=\"bj-modal-title\">Insurance Success</h1>\n      </div>\n    </div>\n  </div>\n\n</div>\n  ",
                styles: [".bj-modal{max-width:305px;height:100%;max-height:200px;width:100%;background-color:#16334a;opacity:.95;border:2px solid transparent;border-radius:8px;letter-spacing:0;font-size:34px;color:#fff;text-align:center;position:absolute;top:50%;left:50%;bottom:50%;right:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);z-index:9999}.bj-modal-wrapper{height:100%;width:100%;position:relative}.bj-image{max-width:175px}.bj-modal.green{border-color:#02f292}.bj-modal.green .bj-modal-title{color:#02f292;margin-bottom:0}.bj-modal.green .points{color:#02f292;font-size:20px;font-weight:900}.bj-modal.blue{border-color:#67ddf2}.bj-modal.blue .bj-modal-title{color:#67ddf2}.bj-modal.yellow{border-color:#ffec77}.bj-modal.yellow .bj-modal-title{color:#ffec77}.bj-modal.red{border-color:#f10260}.bj-modal.red .bj-modal-title{color:#f10260}.small-circle{max-width:66px}.modal-close{font-size:25px;background-color:transparent;position:absolute;top:0;right:0;border:none;color:#fff}"]
            }] }
];
BlackjackModalComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/blackjack.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlackjackModule {
}
BlackjackModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    BlackjackModalComponent,
                ],
                imports: [
                    CommonModule
                ],
                exports: [
                    BlackjackModalComponent,
                ],
                entryComponents: [
                    BlackjackModalComponent
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/bet-amount.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BetAmount {
    constructor() {
        this._isMax = false;
        this._amount = 0;
    }
    /**
     * @return {?}
     */
    reset() {
        this.amount = 0;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    add(value) {
        this.amount += value;
    }
    /**
     * @return {?}
     */
    double() {
        this.amount *= 2;
    }
    /**
     * @return {?}
     */
    half() {
        this.amount *= 0.5;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    subtract(value) {
        this.amount -= value;
        if (this.amount < 0) {
            this.amount = 0;
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setToMax(value) {
        this.amount = value;
        this._isMax = true;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set amount(value) {
        this._isMax = false;
        this._amount = value;
    }
    /**
     * @return {?}
     */
    get amount() {
        return this._amount;
    }
    /**
     * @return {?}
     */
    get isMax() {
        return this._isMax;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/hand-result.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
class HandResultBase {
    /**
     * @param {?} value
     * @param {?} betAmount
     */
    constructor(value, betAmount) {
        this.value = value;
        this.amountWon = value * betAmount;
    }
    /**
     * @return {?}
     */
    get isBlackjack() {
        return false;
    }
    /**
     * @return {?}
     */
    get isWin() {
        return false;
    }
    /**
     * @return {?}
     */
    get isPush() {
        return false;
    }
    /**
     * @return {?}
     */
    get isLoss() {
        return false;
    }
    /**
     * @return {?}
     */
    get isBusted() {
        return false;
    }
    /**
     * @return {?}
     */
    get isInsuranceSuccess() {
        return false;
    }
}
class BlackjackResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(1.5, betAmount);
    }
    /**
     * @return {?}
     */
    get isBlackjack() {
        return true;
    }
    /**
     * @return {?}
     */
    get isWin() {
        return true;
    }
}
class WinResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(1, betAmount);
    }
    /**
     * @return {?}
     */
    get isWin() {
        return true;
    }
}
class PushResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(0, betAmount);
    }
    /**
     * @return {?}
     */
    get isPush() {
        return true;
    }
}
class LossResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(-1, betAmount);
    }
    /**
     * @return {?}
     */
    get isLoss() {
        return true;
    }
}
class BustedResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(-1, betAmount);
    }
    /**
     * @return {?}
     */
    get isBusted() {
        return true;
    }
    /**
     * @return {?}
     */
    get isLoss() {
        return true;
    }
}
class InsuranceSuccessResult extends HandResultBase {
    /**
     * @param {?} betAmount
     */
    constructor(betAmount) {
        super(0, betAmount);
    }
    /**
     * @return {?}
     */
    get isInsuranceSuccess() {
        return true;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/blackjack-hands.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlackjackHand {
    /**
     * @param {?} soundPlayer
     */
    constructor(soundPlayer) {
        this.soundPlayer = soundPlayer;
        this._cards = [];
        this._numOfAces = 0;
        this.countWithoutAces = 0;
        this._isAllCardsKnown = false;
        this._isFirstCardAnAce = false;
        this.isSplit = false;
    }
    /**
     * @return {?}
     */
    reset() {
        this._cards = [];
        this._numOfAces = 0;
        this.countWithoutAces = 0;
        this._isAllCardsKnown = false;
        this._isFirstCardAnAce = false;
        this.isSplit = false;
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    hit(cardId) {
        return this.dealCard(cardId, true);
    }
    /**
     * @protected
     * @param {?} cardId
     * @param {?} shouldReveal
     * @param {?=} revealImmediately
     * @return {?}
     */
    dealCard(cardId, shouldReveal, revealImmediately = false) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const card = new CardController(this.soundPlayer);
            this.cards.push(card);
            /** @type {?} */
            const rankNumber = cardId % 13;
            /** @type {?} */
            const isAce = rankNumber == CardRankNumber.ACE;
            card.reveal({
                suit: Math.floor(cardId / 13),
                rankName: nameOfRank(rankNumber)
            }, rankNumber, cardId);
            if (!revealImmediately) {
                yield sleep(250);
            }
            if (shouldReveal) {
                card.dealAndFlip();
            }
            else {
                card.deal();
            }
            if (!revealImmediately) {
                yield sleep(250);
            }
            if (shouldReveal) {
                this.addValueOfCard(rankNumber, isAce);
            }
        });
    }
    /**
     * @private
     * @param {?} rankNumber
     * @param {?} isAce
     * @return {?}
     */
    addValueOfCard(rankNumber, isAce) {
        /** @type {?} */
        let valueOfCard = 0;
        switch (rankNumber) {
            case CardRankNumber.JACK:
            case CardRankNumber.QUEEN:
            case CardRankNumber.KING:
                valueOfCard = 10;
                break;
            case CardRankNumber.ACE:
                // will determine ace value later
                break;
            default:
                valueOfCard = rankNumber;
                break;
        }
        this.countWithoutAces += valueOfCard;
        if (isAce) {
            this._numOfAces++;
            if (this.numOfCards == 1) {
                this._isFirstCardAnAce = true;
            }
        }
    }
    /**
     * @return {?}
     */
    get isBusted() {
        return this.sum > 21;
    }
    /**
     * @return {?}
     */
    get is21() {
        return this.sum == 21;
    }
    /**
     * @return {?}
     */
    get sumAsString() {
        /** @type {?} */
        let sum = '' + this.sum;
        if (this.isSoftCount) {
            sum += '/' +
                (this.countWithoutAces + this.numOfAces);
        }
        return sum;
    }
    /**
     * @return {?}
     */
    get sum() {
        /** @type {?} */
        let sum = this.countWithoutAces + this.countOfAces;
        if (this.isBlackJack) {
            sum = 21;
        }
        return sum;
    }
    /**
     * @return {?}
     */
    get countOfAces() {
        if (this.isSoftCount) {
            /** @type {?} */
            const largerCount = 11 + (this.numOfAces - 1);
            if (largerCount + this.countWithoutAces <=
                21) {
                return largerCount;
            }
        }
        return this.numOfAces;
    }
    /**
     * @return {?}
     */
    get isSoftCount() {
        return !this.isBlackJack &&
            this.numOfAces > 0 &&
            (this.countWithoutAces +
                11 +
                (this.numOfAces - 1)) <= 21;
    }
    /**
     * @return {?}
     */
    get isBlackJack() {
        return !this.isSplit &&
            this.numOfCards == 2 &&
            this.numOfAces == 1 &&
            this.countWithoutAces == 10;
    }
    /**
     * @return {?}
     */
    get isFirstCardAnAce() {
        return this._isFirstCardAnAce;
    }
    /**
     * @return {?}
     */
    get numOfAces() {
        return this._numOfAces;
    }
    /**
     * @return {?}
     */
    get numOfCards() {
        return this.cards.length;
    }
    /**
     * @return {?}
     */
    get isAllCardsKnown() {
        return this._isAllCardsKnown;
    }
    /**
     * @return {?}
     */
    get cards() {
        return this._cards;
    }
}
class BlackjackDealerHand extends BlackjackHand {
    /**
     * @param {?} cardId
     * @return {?}
     */
    dealFirstCard(cardId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.dealCard(cardId, true);
        });
    }
    /**
     * @return {?}
     */
    dealAnonymousCard() {
        return __awaiter(this, void 0, void 0, function* () {
            // deal anonymous card for dealer's second card (at first)
            yield this.dealCard(0, false);
        });
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    revealSecondCard(cardId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.numOfCards > 1) {
                this.cards.pop();
            }
            yield this.dealCard(cardId, true);
            this._isAllCardsKnown = true;
        });
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    dealAdditionalCard(cardId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.shouldHit) {
                yield this.hit(cardId);
            }
        });
    }
    /**
     * @private
     * @return {?}
     */
    get shouldHit() {
        return this.isSoftCount ?
            this.sum <= 17 :
            this.sum < 17;
    }
}
class BlackjackPlayerHand extends BlackjackHand {
    /**
     * @param {?} soundPlayer
     */
    constructor(soundPlayer) {
        super(soundPlayer);
        this.didDoubleDown = false;
        this._isWaitingForAction = false;
        this.bet = new BetAmount();
        this._boughtInsurance = false;
    }
    /**
     * @return {?}
     */
    reset() {
        super.reset();
        this.didDoubleDown = false;
        this._isWaitingForAction = false;
        this.bet.reset();
        this._boughtInsurance = false;
    }
    /**
     * @param {?} cardId
     * @param {?} betAmount
     * @return {?}
     */
    dealFirstCard(cardId, betAmount) {
        return __awaiter(this, void 0, void 0, function* () {
            this.bet.add(betAmount);
            yield this.hit(cardId);
        });
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    dealSecondCard(cardId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.hit(cardId);
            this._isAllCardsKnown = true;
            if (!this.isBlackJack) {
                this._isWaitingForAction = true;
            }
        });
    }
    /**
     * @return {?}
     */
    buyInsurance() {
        /*
        this.bet.add(
            this.bet.amount / 2
        );
        */
        this._boughtInsurance = true;
    }
    /**
     * @param {?} cards
     * @param {?} betAmount
     * @return {?}
     */
    split(cards, betAmount) {
        return __awaiter(this, void 0, void 0, function* () {
            this.reset();
            this.isSplit = true;
            this.bet.add(betAmount);
            yield this.dealCard(cards[0], true, true);
            /** @type {?} */
            const isFirstCardAnAce = this.numOfAces == 1;
            yield this.hit(cards[1]);
            if (isFirstCardAnAce) {
                this._isWaitingForAction = false;
            }
            this._isAllCardsKnown = true;
        });
    }
    /**
     * @param {?} cardId
     * @return {?}
     */
    doubleDown(cardId) {
        this.didDoubleDown = true;
        this.bet.double();
        return this.hit(cardId, true);
    }
    /**
     * @param {?} cardId
     * @param {?=} isDoubleDown
     * @return {?}
     */
    hit(cardId, isDoubleDown = false) {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            this._isWaitingForAction = false;
            yield _super("hit").call(this, cardId);
            if (this.numOfCards >= 2 &&
                !this.isBusted &&
                !isDoubleDown) {
                this._isWaitingForAction = true;
            }
        });
    }
    /**
     * @return {?}
     */
    stand() {
        this._isWaitingForAction = false;
    }
    /**
     * @param {?} dealer
     * @return {?}
     */
    determineResult(dealer) {
        /** @type {?} */
        const betAmount = this.bet.amount;
        /** @type {?} */
        let result;
        if (this.isBusted) {
            result = new BustedResult(betAmount);
        }
        else if (this.isBlackJack) {
            result = dealer.isBlackJack ?
                new PushResult(betAmount) :
                new BlackjackResult(betAmount);
        }
        else if (dealer.isBlackJack) {
            result = this.boughtInsurance ?
                new InsuranceSuccessResult(betAmount) :
                new LossResult(betAmount);
        }
        else if (dealer.isBusted) {
            result = new WinResult(betAmount);
        }
        else if (this.sum == dealer.sum) {
            result = new PushResult(betAmount);
        }
        else {
            result = this.sum > dealer.sum ?
                new WinResult(betAmount) :
                new LossResult(betAmount);
        }
        //this.bet.add(result.amountWon);
        return result;
    }
    /**
     * @return {?}
     */
    get isAbleToDoubleDown() {
        return this.numOfCards == 2 &&
            !this.didDoubleDown;
    }
    /**
     * @return {?}
     */
    get isAbleToSplit() {
        return this.numOfCards == 2 &&
            this.cards[0].card ==
                this.cards[1].card;
    }
    /**
     * @return {?}
     */
    get isWaitingForAction() {
        return this._isWaitingForAction;
    }
    /**
     * @return {?}
     */
    get boughtInsurance() {
        return this._boughtInsurance;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/game/blackjack-controller.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlackjackGameController {
    /**
     * @param {?} component
     * @param {?} server
     * @param {?} soundPlayer
     */
    constructor(component, server, soundPlayer) {
        this.component = component;
        this.server = server;
        this.bet = new BetAmount();
        this.initialBetAmount = undefined;
        this._isSplit = false;
        this.hand1Result = undefined;
        this.hand2Result = undefined;
        this._promptForInsurance = false;
        this._showResult = false;
        this.dealer = new BlackjackDealerHand(soundPlayer);
        this.hand1 = new BlackjackPlayerHand(soundPlayer);
        this.hand2 = new BlackjackPlayerHand(soundPlayer);
    }
    /**
     * @private
     * @return {?}
     */
    reset() {
        this.component.gameResultModal.close();
        this.dealer.reset();
        this.hand1.reset();
        this.hand2.reset();
        this.initialBetAmount = undefined;
        this._isSplit = false;
        this.selectedHandIndex = 0;
        this.hand1Result = undefined;
        this.hand2Result = undefined;
        this._promptForInsurance = false;
        this._showResult = false;
    }
    /**
     * @param {?} cards
     * @param {?} betAmount
     * @return {?}
     */
    start(cards, betAmount) {
        return __awaiter(this, void 0, void 0, function* () {
            this.reset();
            if (this.bet.amount == 0) {
                this.bet.add(betAmount);
            }
            // player
            yield this.hand1.dealFirstCard(cards.playerCards[0], this.bet.amount);
            // dealer
            yield this.dealer.dealFirstCard(cards.dealerCards[0]);
            // player
            yield this.hand1.dealSecondCard(cards.playerCards[1]);
            // dealer
            yield this.dealer.dealAnonymousCard();
            yield this.checkForInsurance();
            this.initialBetAmount = this.bet.amount;
        });
    }
    /**
     * @return {?}
     */
    checkForInsurance() {
        return __awaiter(this, void 0, void 0, function* () {
            // if dealer is showing ace, offer insurance
            if (this.dealer.isFirstCardAnAce) {
                this._promptForInsurance = true;
                return;
            }
            else {
                yield this.checkForBlackJack();
            }
        });
    }
    /**
     * @param {?} yesToInsurance
     * @return {?}
     */
    respondToInsurance(yesToInsurance) {
        return __awaiter(this, void 0, void 0, function* () {
            this._promptForInsurance = false;
            if (yesToInsurance) {
                this.hand1.buyInsurance();
            }
            /** @type {?} */
            const isDealerBlackjack = yield this.server.respondToInsurance(yesToInsurance);
            this.checkForBlackJack(isDealerBlackjack);
        });
    }
    /**
     * @private
     * @param {?=} isDealerBlackjack
     * @return {?}
     */
    checkForBlackJack(isDealerBlackjack = undefined) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('isDealerBlackjack: ' + isDealerBlackjack);
            // *** peek for blackjack ***
            if (isDealerBlackjack == undefined) {
                isDealerBlackjack = yield this.server.peekForBlackjack();
            }
            if (isDealerBlackjack || this.hand1.isBlackJack) {
                this.conclude(isDealerBlackjack);
            }
        });
    }
    /**
     * @return {?}
     */
    split() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.isAbleToSplit) {
                return;
            }
            this._isSplit = true;
            // split hands
            /** @type {?} */
            const card1 = this.hand1.cards[0].cardId;
            /** @type {?} */
            const card2 = this.hand1.cards[1].cardId;
            /** @type {?} */
            const newCards = yield this.server.split();
            /** @type {?} */
            const card3 = newCards[0];
            /** @type {?} */
            const card4 = newCards[1];
            yield this.hand1.split([card1, card3], this.initialBetAmount);
            yield this.hand2.split([card2, card4], this.initialBetAmount);
            if (!this.isWaitingForAction) {
                this.selectOtherHand();
            }
        });
    }
    /**
     * @return {?}
     */
    doubleDown() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isAbleToDoubleDown) {
                /** @type {?} */
                const cardId = yield this.server.doubleDown(this.selectedHandIndex);
                // deal only 1 more card
                yield this.currentHand.doubleDown(cardId);
                // move to other hand (if applicable)
                this.selectOtherHand();
            }
        });
    }
    /**
     * @return {?}
     */
    hit() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isWaitingForAction) {
                /** @type {?} */
                const cardId = yield this.server.hit(this.selectedHandIndex);
                // deal another card
                yield this.currentHand.hit(cardId);
                if (this.currentHand.isBusted) {
                    this.selectOtherHand();
                }
                else if (this.currentHand.is21) {
                    this.stand();
                }
            }
        });
    }
    /**
     * @return {?}
     */
    stand() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isWaitingForAction) {
                yield this.server.stand();
                this.currentHand.stand();
                this.selectOtherHand();
            }
        });
    }
    /**
     * @private
     * @return {?}
     */
    selectOtherHand() {
        // move to other hand if applicable
        if (this.isSplit) {
            this.selectedHandIndex =
                this.selectedHandIndex == 0 ?
                    1 : 0;
            if (this.currentHand.isWaitingForAction) {
                return;
            }
        }
        // otherwise conclude
        this.conclude(false);
    }
    /**
     * @private
     * @param {?} isDealerBlackJack
     * @return {?}
     */
    conclude(isDealerBlackJack) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const isBusted = this.isSplit ?
                (this.hand1.isBusted &&
                    this.hand2.isBusted) :
                this.hand1.isBusted;
            if (!isBusted) {
                /** @type {?} */
                const cardId = yield this.server.getDealersSecondCard();
                yield this.dealer.revealSecondCard(cardId);
            }
            /** @type {?} */
            const isBlackjackForHand1 = !this.isSplit &&
                this.hand1.isBlackJack;
            if (!isBusted && !isDealerBlackJack && !isBlackjackForHand1) {
                // deal additional cards for dealer, if applicable
                /** @type {?} */
                const cards = yield this.server.getDealersAdditionalCards();
                for (var cardId of cards) {
                    yield this.dealer.dealAdditionalCard(cardId);
                }
            }
            /*** check hand 1 result */
            // determine hand 1 result
            this.hand1Result = this.hand1.determineResult(this.dealer);
            //alert('Hand 1 Result: '+this.hand1Result);
            //let result:IBlackjackResult = this.hand1Result;
            /*** check the result of the second hand (if applicable) ***/
            if (this.isSplit) {
                this.hand2Result = this.hand2.determineResult(this.dealer);
                /*
                result = new CombinedHandsResult(
                    this.hand1Result,
                    this.hand2Result
                );
                */
                //alert('Hand 2 Result: '+this.hand2Result);
            }
            /*** end game: ***/
            this.initialBetAmount = undefined;
            this._showResult = true;
            if (this.hand1Result.isInsuranceSuccess ||
                this.hand1Result.isBlackjack) {
                this.component.gameResultModal.showResult(this.hand1Result);
            }
            else if (this.isSplit && this.hand2Result.isBlackjack) {
                this.component.gameResultModal.showResult(this.hand2Result);
            }
            yield sleep(3000);
            this._showResult = false;
            this.component.gameResultModal.close();
            //await this.component.gameResultModal.showResult(result);
            /*
            await sleep(3000);
            if (!this.isBetPlaced) {
                this.reset();
            }
            */
        });
    }
    /**
     * @return {?}
     */
    get isAbleToSplit() {
        return !this.isSplit &&
            this.hand1.isAbleToSplit;
    }
    /**
     * @return {?}
     */
    get isAbleToDoubleDown() {
        return this.currentHand.isAbleToDoubleDown;
    }
    /**
     * @private
     * @return {?}
     */
    get currentHand() {
        return this.selectedHandIndex == 0 ?
            this.hand1 :
            this.hand2;
    }
    /**
     * @return {?}
     */
    get isSplit() {
        return this._isSplit;
    }
    /**
     * @return {?}
     */
    get isFirstHandSelected() {
        return this.selectedHandIndex == 0 &&
            this.isWaitingForAction;
    }
    /**
     * @return {?}
     */
    get isSecondHandSelected() {
        return this.selectedHandIndex == 1 &&
            this.isWaitingForAction;
    }
    /**
     * @return {?}
     */
    get isWaitingForAction() {
        return this.isStarted &&
            this.currentHand.isWaitingForAction &&
            !this.server.isWaitingForResponse &&
            !this.isGameOver;
    }
    /**
     * @return {?}
     */
    get isStarted() {
        return this.isBetPlaced;
    }
    /**
     * @return {?}
     */
    get bet1() {
        return this.isBetPlaced ?
            this.hand1.bet :
            this.bet;
    }
    /**
     * @return {?}
     */
    get isLoading() {
        return this.server.isWaitingForResponse || (this.isBetPlaced &&
            !this.promptForInsurance &&
            !this.isWaitingForAction);
    }
    /**
     * @return {?}
     */
    get isAbleToPlaceBet() {
        return !this.isBetPlaced &&
            !this.server.isWaitingForResponse;
    }
    /**
     * @return {?}
     */
    get isBetPlaced() {
        return this.initialBetAmount != undefined;
    }
    /**
     * @return {?}
     */
    get isGameOver() {
        return this.hand1Result != undefined;
    }
    /**
     * @return {?}
     */
    get isDealerWin() {
        return this.isSplit ?
            (this.isLossForHand1 &&
                this.isLossForHand2) :
            this.isLossForHand1;
    }
    /**
     * @return {?}
     */
    get isDealerLoss() {
        return this.dealer.isBusted ||
            (this.isSplit ?
                (this.isWinForHand1 &&
                    this.isWinForHand2) :
                this.isWinForHand1);
    }
    /**
     * @return {?}
     */
    get amountWon1() {
        return this.hand1Result ?
            this.hand1Result.amountWon :
            0;
    }
    /**
     * @return {?}
     */
    get amountWon2() {
        return this.hand2Result ?
            this.hand2Result.amountWon :
            0;
    }
    /**
     * @return {?}
     */
    get isWinForHand1() {
        return this.hand1Result && this.hand1Result.isWin;
    }
    /**
     * @return {?}
     */
    get showLossForHand1() {
        return this.isSplit ?
            (this.isGameOver ?
                this.isLossForHand1 && this.showResult :
                this.hand1.isBusted) :
            this.isLossForHand1;
    }
    /**
     * @return {?}
     */
    get isLossForHand1() {
        return this.hand1.isBusted ||
            (this.hand1Result && this.hand1Result.isLoss);
    }
    /**
     * @return {?}
     */
    get isPushForHand1() {
        return this.hand1Result && this.hand1Result.isPush;
    }
    /**
     * @return {?}
     */
    get isWinForHand2() {
        return this.hand2Result && this.hand2Result.isWin;
    }
    /**
     * @return {?}
     */
    get isLossForHand2() {
        return this.hand2.isBusted ||
            (this.hand2Result && this.hand2Result.isLoss);
    }
    /**
     * @return {?}
     */
    get isPushForHand2() {
        return this.hand2Result && this.hand2Result.isPush;
    }
    /**
     * @return {?}
     */
    get promptForInsurance() {
        return this._promptForInsurance;
    }
    /**
     * @return {?}
     */
    get choseInsurance() {
        return this.hand1.boughtInsurance;
    }
    /**
     * @return {?}
     */
    get showResult() {
        return this._showResult;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/util/cards-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @return {?}
 */
function generateRandomCardId() {
    return getRandomIntegerBetween0AndX(51);
}
/**
 * @param {?} x
 * @return {?}
 */
function getRandomIntegerBetween0AndX(x) {
    return Math.floor(Math.random() * x);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/local-server.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LocalBlackjackServer {
    /**
     * @param {?} component
     * @param {?} soundPlayer
     */
    constructor(component, soundPlayer) {
        this.component = component;
        this.isWaitingForResponse = false;
        this.dealer = new BlackjackDealerHand(soundPlayer);
    }
    /**
     * @return {?}
     */
    resumeExistingGame() { }
    /**
     * @param {?} betAmount
     * @return {?}
     */
    placeBet(betAmount) {
        /** @type {?} */
        const dealersCard1 = generateRandomCardId();
        /** @type {?} */
        const dealersCard2 = generateRandomCardId();
        //const dealersCard1 = 1;
        //const dealersCard2 = 10;
        this.dealer.reset();
        this.dealer.dealFirstCard(dealersCard1);
        this.dealer.revealSecondCard(dealersCard2);
        /** @type {?} */
        const playersCard1 = generateRandomCardId();
        /** @type {?} */
        const playersCard2 = generateRandomCardId();
        //const playersCard1 = generateRandomCardId();
        //const playersCard2 = playersCard1;
        this.component.game.start({
            playerCards: [playersCard1, playersCard2],
            dealerCards: [dealersCard1]
        }, Number(betAmount.decimal));
    }
    /**
     * @param {?} yes
     * @return {?}
     */
    respondToInsurance(yes) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.dealer.isBlackJack;
        });
    }
    /**
     * @return {?}
     */
    peekForBlackjack() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.dealer.isBlackJack;
        });
    }
    /**
     * @return {?}
     */
    getDealersSecondCard() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.dealer.cards[1].cardId;
        });
    }
    /**
     * @return {?}
     */
    split() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                generateRandomCardId(),
                generateRandomCardId()
            ];
        });
    }
    /**
     * @return {?}
     */
    doubleDown() {
        return this.hit();
    }
    /**
     * @return {?}
     */
    hit() {
        return __awaiter(this, void 0, void 0, function* () {
            return generateRandomCardId();
        });
    }
    /**
     * @return {?}
     */
    stand() {
        return __awaiter(this, void 0, void 0, function* () {
            // do nothing
            return true;
        });
    }
    /**
     * @return {?}
     */
    getDealersAdditionalCards() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const cards = [];
            for (var i = 0; i < 8; i++) {
                cards.push(generateRandomCardId());
            }
            return cards;
        });
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/remote-server.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RemoteBlackjackServerBridge {
    /**
     * @param {?} component
     * @param {?} accountNames
     * @param {?} app
     */
    constructor(component, accountNames, app) {
        this.component = component;
        this.accountNames = accountNames;
        this.app = app;
        this.actionId = 0;
        this._isWaitingForResponse = false;
        this.playersAdditionalCardIndex = undefined;
        this.isResuming = false;
    }
    /**
     * @return {?}
     */
    resumeExistingGame() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const wallet = this.wallet;
            if (!wallet.isLoggedIn ||
                this.isResuming) {
                return;
            }
            this.isResuming = true;
            /** @type {?} */
            const state = yield this.sendGameCommand(BlackjackGameCommand.RESUME_GAME, this.accountData);
            console.log('Resumed Game State:');
            console.log(state);
            this.app.state.bets.setMyBetId(state.id);
            yield this.startGame(state);
            // *** START SYNCING: ***
            this.playersAdditionalCardIndex = 0;
            for (var command of state.commands) {
                this.actionId++;
                /** @type {?} */
                const parts = command.split("~");
                /** @type {?} */
                const commandName = parts[0];
                //console.log(commandName);
                switch (commandName) {
                    case BlackjackGameCommand.YES_TO_INSURANCE:
                    case BlackjackGameCommand.NO_TO_INSURANCE:
                        this.game.respondToInsurance(commandName == BlackjackGameCommand.YES_TO_INSURANCE);
                        break;
                    case BlackjackGameCommand.SPLIT:
                        yield this.game.split();
                        break;
                    case BlackjackGameCommand.HIT:
                        yield this.game.hit();
                        break;
                    case BlackjackGameCommand.DOUBLE_DOWN:
                        yield this.game.doubleDown();
                        break;
                    case BlackjackGameCommand.STAND:
                        yield this.game.stand();
                        break;
                }
            }
            // *** STOP SYNCING: ***
            this.playersAdditionalCardIndex = undefined;
            this.isResuming = false;
        });
    }
    /**
     * @param {?} betAmount
     * @return {?}
     */
    placeBet(betAmount) {
        if (this.component.isInsufficientFunds) {
            this.app.openInsufficentFundsModal();
            return;
        }
        this._isWaitingForResponse = true;
        /** @type {?} */
        const component = this.component;
        /** @type {?} */
        const wallet = component.wallet;
        /** @type {?} */
        const memo = `${String(betAmount.integer)}-${wallet.publicKey}-${component.state.referrer}-${component.getSeed(false)}`;
        Big.RM = 0 /* RoundDown */;
        /** @type {?} */
        let amtDeposit = new Big(betAmount.decimal).times("4.5");
        /** @type {?} */
        const amtBalance = component.wallet.getBalance(component.selectedToken.symbol);
        /** @type {?} */
        const precision = component.selectedToken.precision;
        if (amtDeposit.gt(amtBalance)) {
            // If there is not enough funds for 4.5 then do not deposit the uneven remainder that cant be used
            /** @type {?} */
            const factor = new Big(amtBalance).div(betAmount.decimal).toFixed(0);
            amtDeposit = new Big(betAmount.decimal).times(factor);
        }
        /** @type {?} */
        const depositAmount = component.selectedToken.getAssetAmount(amtDeposit.toFixed(precision));
        /** @type {?} */
        const eosAccounts = this.app.eos.accounts;
        /** @type {?} */
        const transfers = [{
                toAccount: eosAccounts.games.blackjack,
                amount: depositAmount,
                memo
            }];
        if (component.isJackpotBet) {
            transfers.push({
                toAccount: eosAccounts.jackpot,
                amount: component.selectedToken.jackpotTicketPrice,
                memo: eosAccounts.games.blackjack
            });
        }
        /** @type {?} */
        const promise = wallet.transfer(transfers);
        promise.then((/**
         * @param {?} result
         * @return {?}
         */
        (result) => {
            /** @type {?} */
            const txnId = result.transaction_id;
            this.placedBetId = component.state.bets.onBetPlaced(txnId);
            this.component.trackBetWithGoogleAnalytics('blackjack', betAmount.decimal, txnId, false);
            this._isWaitingForResponse = false;
            this.waitForGame();
        }), (/**
         * @param {?} errorData
         * @return {?}
         */
        (errorData) => {
            this._isWaitingForResponse = false;
            /** @type {?} */
            const error = new EosErrorParser(errorData);
            component.alert.error(error.message);
        }));
    }
    /**
     * @private
     * @return {?}
     */
    waitForGame() {
        return __awaiter(this, void 0, void 0, function* () {
            this._isWaitingForResponse = true;
            /** @type {?} */
            const state = yield this.sendGameCommand(BlackjackGameCommand.START_GAME, this.accountData);
            yield this.startGame(state);
            this.isResuming = false;
        });
    }
    /**
     * @private
     * @param {?} state
     * @return {?}
     */
    startGame(state) {
        return __awaiter(this, void 0, void 0, function* () {
            this.actionId = 0;
            this._isWaitingForResponse = false;
            //this.isDealerBlackjack = state.isDealerBlackjack;
            this.dealersAdditionalCards = state.additionalDealerCards;
            this.state = state;
            /** @type {?} */
            const token = state.initialBet.token;
            yield this.game.start(state.initialCards, state.initialBet.amountAsInteger /
                Math.pow(10, token.precision));
        });
    }
    /**
     * @param {?} yes
     * @return {?}
     */
    respondToInsurance(yes) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing ?
                this.state :
                yield this.sendGameCommand(yes ?
                    BlackjackGameCommand.YES_TO_INSURANCE :
                    BlackjackGameCommand.NO_TO_INSURANCE);
            return response.isDealerBlackjack;
        });
    }
    /**
     * @return {?}
     */
    peekForBlackjack() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing || this.state.isDealerBlackjack != undefined ?
                this.state :
                yield this.sendGameCommand(BlackjackGameCommand.PEEK_FOR_BLACKJACK);
            return response.isDealerBlackjack;
        });
    }
    /**
     * @return {?}
     */
    split() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing ?
                this.getAdditionalPlayerCards(2) :
                yield this.sendGameCommand(BlackjackGameCommand.SPLIT);
            return response;
        });
    }
    /**
     * @param {?} handIndex
     * @return {?}
     */
    hit(handIndex) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing ?
                this.getAdditionalPlayerCards(1) :
                yield this.sendGameCommand(BlackjackGameCommand.HIT);
            return response[0];
        });
    }
    /**
     * @param {?} handIndex
     * @return {?}
     */
    doubleDown(handIndex) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const response = this.isSyncing ?
                this.getAdditionalPlayerCards(1) :
                yield this.sendGameCommand(BlackjackGameCommand.DOUBLE_DOWN);
            return response[0];
        });
    }
    /**
     * @private
     * @param {?} numOfCards
     * @return {?}
     */
    getAdditionalPlayerCards(numOfCards) {
        /** @type {?} */
        const cardIds = [];
        for (var i = 0; i < numOfCards; i++) {
            /** @type {?} */
            const index = this.playersAdditionalCardIndex++;
            /** @type {?} */
            const card = this.state.additionalPlayerCards[index];
            cardIds.push(card);
        }
        return cardIds;
    }
    /**
     * @return {?}
     */
    stand() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.isSyncing) {
                return yield this.sendGameCommand(BlackjackGameCommand.STAND);
            }
            else {
                return false;
            }
        });
    }
    /**
     * @return {?}
     */
    getDealersSecondCard() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const cards = this.isSyncing ?
                this.state.additionalDealerCards :
                yield this.sendGameCommand(BlackjackGameCommand.GET_DEALERS_CARDS);
            this.dealersAdditionalCards = cards.slice(1);
            return cards[0];
        });
    }
    /**
     * @return {?}
     */
    getDealersAdditionalCards() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.dealersAdditionalCards;
        });
    }
    /**
     * @private
     * @template T
     * @param {?} commandName
     * @param {?=} args
     * @return {?}
     */
    sendGameCommand(commandName, args = []) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            const isResumeGameCommand = commandName == BlackjackGameCommand.RESUME_GAME;
            /** @type {?} */
            const isStartGameCommand = commandName == BlackjackGameCommand.START_GAME;
            /** @type {?} */
            const actionId = isResumeGameCommand || isStartGameCommand ?
                0 :
                ++this.actionId;
            /** @type {?} */
            const parts = [
                commandName,
                isStartGameCommand ?
                    this.placedBetId :
                    this.gameId,
                actionId
            ].concat(args);
            // data format:commandName~gameId~actionId~data...
            /** @type {?} */
            const data = parts.join('~');
            if (!isResumeGameCommand) {
                this._isWaitingForResponse = true;
            }
            this.wallet.sign(data, '')
                .then((/**
             * @param {?} info
             * @return {?}
             */
            (info) => {
                //console.log('Sending Game Command: ')
                //console.log(data);
                this.app.blackjack.client.messageManager.sendGameCommand(data, info).then((/**
                 * @param {?} result
                 * @return {?}
                 */
                (result) => {
                    this._isWaitingForResponse = false;
                    resolve(result);
                })).catch((/**
                 * @param {?} error
                 * @return {?}
                 */
                (error) => {
                    this._isWaitingForResponse = false;
                    this.alert.error(error.code);
                    reject(error);
                }));
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                this._isWaitingForResponse = false;
                /** @type {?} */
                const e = new EosErrorParser(error);
                this.alert.error(e.message);
                reject(error);
            }));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    get isSyncing() {
        return this.playersAdditionalCardIndex != undefined;
    }
    /**
     * @private
     * @return {?}
     */
    get gameId() {
        return this.state ?
            this.state.id :
            '0';
    }
    /**
     * @return {?}
     */
    get isWaitingForResponse() {
        return this._isWaitingForResponse;
    }
    /**
     * @private
     * @return {?}
     */
    get game() {
        return this.component.game;
    }
    /**
     * @private
     * @return {?}
     */
    get accountData() {
        /** @type {?} */
        const wallet = this.wallet;
        return wallet.isEasyAccount ?
            [this.accountNames.easyAccount, wallet.accountId, wallet.accountName] :
            [wallet.accountName];
    }
    /**
     * @private
     * @return {?}
     */
    get wallet() {
        return this.app.wallet;
    }
    /**
     * @private
     * @return {?}
     */
    get alert() {
        return this.app.alert;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/main/blackjack.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
@Component({
    selector: 'app-blackjack',
    templateUrl: './blackjack.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./blackjack.component.scss']
})
*/
class BlackjackComponentBase extends GameComponentBase {
    /**
     * @param {?} app
     * @param {?} renderer
     * @param {?} modal
     * @param {?} googleAnalytics
     * @param {?} soundPlayer
     * @param {?} accountNames
     */
    constructor(app, renderer, modal, googleAnalytics, soundPlayer, accountNames) {
        super(app, googleAnalytics, soundPlayer);
        this.renderer = renderer;
        this.modal = modal;
        // *** For Testing: ***
        //this.useLocalServer();
        this.useRemoteServer(accountNames);
        this.game = new BlackjackGameController(this, this.server, soundPlayer);
        this.setListeners();
    }
    /**
     * @private
     * @param {?} soundPlayer
     * @return {?}
     */
    useLocalServer(soundPlayer) {
        this.server = new LocalBlackjackServer(this, soundPlayer);
    }
    /**
     * @private
     * @param {?} acountNames
     * @return {?}
     */
    useRemoteServer(acountNames) {
        this.server = new RemoteBlackjackServerBridge(this, acountNames, this.app);
    }
    /**
     * @private
     * @return {?}
     */
    setListeners() {
        this.app.state.setListener(this);
        this.app.blackjack.client.setListener(this);
        this.app.state.bets.setListener(this);
    }
    /**
     * @return {?}
     */
    onBlackjackClientConnected() {
        this.resumeGame();
    }
    /**
     * @return {?}
     */
    onAuthenticated() {
        this.resumeGame();
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.renderer.addClass(document.body, 'blackjack-bg');
        this.resumeGame();
        // *** blackjack deposit modal ***
        //this.modal.open('blackjackGameNoticeModal');
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.renderer.removeClass(document.body, 'blackjack-bg');
    }
    /**
     * @return {?}
     */
    debug() {
        console.log(this.game);
    }
    /**
     * @private
     * @return {?}
     */
    resumeGame() {
        if (this.app.blackjack.client.isConnected) {
            this.server.resumeExistingGame();
        }
    }
    /**
     * @return {?}
     */
    onBettingCurrencyChanged() {
        this.clearTable();
    }
    /**
     * @return {?}
     */
    onChipSelected() {
        this.addChip();
    }
    /**
     * @return {?}
     */
    halfOfBetAmount() {
        if (this.isBetPlaced) {
            return;
        }
        /** @type {?} */
        const minimumBet = Number(this.selectedToken.minimumBetAmount);
        if (this.game.bet.amount == minimumBet) {
            return;
        }
        this.game.bet.half();
        if (this.game.bet.amount < minimumBet) {
            this.game.bet.amount = minimumBet;
        }
    }
    /**
     * @return {?}
     */
    doubleBetAmount() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isBetPlaced) {
                return;
            }
            /** @type {?} */
            const maxBet = yield this.getMaxBet();
            if (this.game.bet.amount == maxBet) {
                return;
            }
            this.game.bet.double();
            if (this.game.bet.amount > maxBet) {
                this.game.bet.setToMax(maxBet);
            }
        });
    }
    /**
     * @return {?}
     */
    addChip() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isBetPlaced) {
                return;
            }
            if (!this.isMaxChipSelected) {
                /** @type {?} */
                const amount = this.chips[this.selectedChip];
                this.game.bet.add(amount);
            }
            else {
                this.game.bet.setToMax(yield this.getMaxBet());
            }
        });
    }
    /**
     * @return {?}
     */
    removeChip() {
        if (this.isBetPlaced) {
            return;
        }
        if (!this.isMaxChipSelected) {
            /** @type {?} */
            const amount = this.chips[this.selectedChip];
            this.game.bet.subtract(amount);
        }
        else {
            this.game.bet.reset();
        }
    }
    /**
     * @return {?}
     */
    clearTable() {
        if (!this.isBetPlaced) {
            this.game.bet.reset();
        }
    }
    /**
     * @return {?}
     */
    placeBet() {
        if (this.game.isStarted) {
            return;
        }
        this.alert.clear();
        if (this.game.bet.amount == 0) {
            this.alert.error('Place Chips on Table to Place Your Bet!');
            return;
        }
        // *** BROADCAST BET TRANSACTION TO BLOCK CHAIN ***
        this.server.placeBet(this.betAmount);
    }
    /**
     * @param {?} bet
     * @return {?}
     */
    onMyBetResolved(bet) {
        return __awaiter(this, void 0, void 0, function* () {
            yield sleep(2000);
            yield this.updateBalances(bet);
            // *** recalculate max bet ***
            if (!this.isBetPlaced &&
                this.game.bet.isMax) {
                this.game.bet.setToMax(yield this.getMaxBet());
            }
        });
    }
    /**
     * @return {?}
     */
    getMaxBet() {
        return this.bankroll.getMaxBet(4);
    }
    /**
     * @return {?}
     */
    get betAmount() {
        return this.selectedToken.getAssetAmount(this.game.bet.amount.toString());
    }
    /**
     * @return {?}
     */
    get isSplit() {
        return this.game.isSplit;
    }
    /**
     * @return {?}
     */
    get isBetPlaced() {
        return this.game.isBetPlaced;
    }
}
BlackjackComponentBase.propDecorators = {
    gameResultModal: [{ type: ViewChild, args: ['gameResultModal',] }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/chip-stack/chip-stack.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
@Component({
    selector: 'bj-chip-stack',
    templateUrl: './chip-stack.component.html',
    styleUrls: ['./chip-stack.component.scss']
})
*/
class BlackJackChipStackComponentBase {
    /**
     * @param {?} app
     */
    constructor(app) {
        this.app = app;
        this.isMax = false;
        this.amount = 0;
        this.chipstack = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.makeChipStack();
    }
    /**
     * @return {?}
     */
    makeChipStack() {
        /** @type {?} */
        let chipstack = [];
        // is the amount the max for this bet type
        if (this.isMax) {
            chipstack.push(this.chips.length);
        }
        else {
            /** @type {?} */
            const factor = 1 / this.chips[0];
            /** @type {?} */
            let n = Math.round(this.amount * factor);
            for (let i = this.chips.length - 1; i >= 0; i--) {
                if ((this.chips[i] * factor) <= n) {
                    /** @type {?} */
                    let r = Math.floor(n / (this.chips[i] * factor));
                    n -= this.chips[i] * factor * r;
                    for (var j = 0; j < r; j++) {
                        chipstack.push(i);
                    }
                }
            }
        }
        this.chipstack = chipstack;
    }
    /**
     * @return {?}
     */
    get chips() {
        return this.settings.selectedToken.chips;
    }
    /**
     * @return {?}
     */
    get settings() {
        return this.app.settings;
    }
}
BlackJackChipStackComponentBase.propDecorators = {
    isMax: [{ type: Input }],
    amount: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-rules/blackjack-rules.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
@Component({
    selector: "blackjack-rules",
    templateUrl: "./blackjack-rules.component.html",
    styleUrls: ["./blackjack-rules.component.scss"]
})
*/
class BlackjackRulesComponentBase {
    /**
     * @param {?} app
     * @param {?} modal
     */
    constructor(app, modal) {
        this.app = app;
        this.modal = modal;
    }
    /**
     * @return {?}
     */
    get translation() {
        return this.app.settings.translation;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-deposit-modal/blackjack-deposit-modal.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
@Component({
    selector: "blackjack-deposit-modal",
    templateUrl: "./blackjack-deposit-modal.component.html",
    styleUrls: ["./blackjack-deposit-modal.component.scss"]
})
*/
class BlackjackDepositModalBase {
    /**
     * @param {?} app
     * @param {?} modal
     */
    constructor(app, modal) {
        this.app = app;
        this.modal = modal;
    }
    /**
     * @return {?}
     */
    get translation() {
        return this.app.settings.translation;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/components/blackjack-card/blackjack-card.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
@Component({
    selector: '[blackjack-card]',
    templateUrl: './blackjack-card.component.html',
    styleUrls: ['./blackjack-card.component.scss']
})
*/
class BlackjackCardComponentBase {
    constructor() {
        this.direction = 'right';
        this.history = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    get suit() {
        return this.card ?
            this.card.suit :
            undefined;
    }
    /**
     * @return {?}
     */
    get rank() {
        return this.card ?
            this.card.card :
            undefined;
    }
    /**
     * @return {?}
     */
    get flipped() {
        return this.card ?
            this.card.flipped :
            undefined;
    }
    /**
     * @return {?}
     */
    get dealt() {
        return this.card ?
            this.card.dealt :
            undefined;
    }
}
BlackjackCardComponentBase.propDecorators = {
    card: [{ type: Input }],
    direction: [{ type: Input }],
    history: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/_common/models/bets/blackjack-bet.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlackjackBetResult extends BetBase {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data);
        this.game = 'blackjack';
        this.description = describeResults(data.playerResults, data.dealerHandResult);
        /** @type {?} */
        const totalWagered = new AssetAmount(data.totalWagered);
        /** @type {?} */
        const totalPayout = new AssetAmount(data.totalPayout);
        this.payout = totalPayout.decimalWithoutTrailingZeros;
        this.isWin = totalPayout.integer.gte(totalWagered.integer);
        this.profit = Number(totalPayout.subtractAndReplace(totalWagered).decimal);
    }
}
/**
 * @param {?} playerResults
 * @param {?} dealerHandResult
 * @return {?}
 */
function describeResults(playerResults, dealerHandResult) {
    /** @type {?} */
    const parts = [];
    for (var result of playerResults) {
        parts.push(describeResult(result, dealerHandResult));
    }
    return parts.join(' ~ ');
}
/**
 * @param {?} player
 * @param {?} dealer
 * @return {?}
 */
function describeResult(player, dealer) {
    /** @type {?} */
    let left = '' + player.sum;
    /** @type {?} */
    let sign;
    /** @type {?} */
    let right = '' + dealer.sum;
    if (player.isBlackjack) {
        left = 'BJ';
    }
    if (dealer.isBlackjack) {
        right = 'BJ';
    }
    if (player.sum > 21) {
        sign = '>';
        right = '21';
    }
    else if (player.sum < dealer.sum) {
        sign = '<';
    }
    else if (player.sum > dealer.sum) {
        sign = '>';
    }
    else {
        // sums are equal (it may be a push) 
        if (player.sum < 21) {
            // PUSH if less than 21
            sign = '=';
        }
        else {
            // BOTH SUMS ARE EQUAL TO 21
            // PUSH if both have BJ
            if (player.isBlackjack && dealer.isBlackjack) {
                sign = '=';
            }
            else if (player.isBlackjack) {
                sign = '>';
            }
            else {
                // dealer blackjack
                sign = '<';
            }
        }
    }
    return left + ' ' + sign + ' ' + right;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/server/bj-client.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NAMESPACE = '/blackjack';
class BlackjackClient extends SocketClientBase {
    /**
     * @param {?} app
     */
    constructor(app) {
        super({
            host: 'bjs.eosbet.io',
            port: 443,
            ssl: true
        }, NAMESPACE);
        this.app = app;
        this._isConnected = false;
    }
    /**
     * @return {?}
     */
    connect() {
        /** @type {?} */
        var socket = this.connectSocket();
        this._messageManager = new BlackjackMessageManager(socket, this);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setListener(value) {
        this.listener = value;
    }
    /**
     * @protected
     * @return {?}
     */
    onConnected() {
        this._isConnected = true;
        console.log('blackjack client connected to Blackjack Server!');
        if (this.listener) {
            this.listener.onBlackjackClientConnected();
        }
    }
    /**
     * MESSAGE LISTENERS: **
     * @param {?} data
     * @return {?}
     */
    onResolveBetResult(data) {
        this.app.state.bets.onBetResolved(new BlackjackBetResult(data));
    }
    /**
     *
     * @protected
     * @return {?}
     */
    onDisconnected() {
        this._isConnected = false;
    }
    /**
     * @return {?}
     */
    get messageManager() {
        return this._messageManager;
    }
    /**
     * @return {?}
     */
    get isConnected() {
        return this._isConnected;
    }
}
class BlackjackMessageManager extends SocketMessageManager {
    /**
     * @param {?} socket
     * @param {?} client
     */
    constructor(socket, client) {
        super(socket, BlackjackMessage.PREFIX, client);
    }
    /**
     * @template T
     * @param {?} data
     * @param {?} info
     * @return {?}
     */
    sendGameCommand(data, info) {
        return this.makeRequest(BlackjackMessage.GAME_COMMAND, data, info);
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/blackjack/services/blackjack-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlackjackAppService {
    /**
     * @param {?} app
     */
    constructor(app) {
        this.client = new BlackjackClient(app);
    }
    /**
     * @return {?}
     */
    init() {
        this.client.connect();
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: earnbet-common-angular.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { BlackjackComponentBase, BlackJackChipStackComponentBase, BlackjackRulesComponentBase, BlackjackDepositModalBase, BlackjackCardComponentBase, BlackjackModalComponent, BlackjackAppService, GameComponentBase, EosErrorParser, CardController, TokenSymbol, BetBase, getHouseEdgeReductionAsDecimal, getStakingLevelName, getStakingLevelIndex, stakingLevels, betTokenFactor, IntBasedNum, AssetAmount, toTotalDigits, numStringToTotalDigits, roundDownWithPrecision, roundDownWithoutTrailingZeros, roundStringDownWithPrecision, OverlayModalService, BlackjackModule };

//# sourceMappingURL=earnbet-common-angular.js.map