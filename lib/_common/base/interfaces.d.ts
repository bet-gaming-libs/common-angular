import { IAppSettingsListener, IAppStateListener, IAppState } from "../models/interfaces";
import { IBankrollService } from "../services/interfaces";
import { ITranslationBase } from '../translations/interfaces';
import { IBettingToken } from '../models/business-rules/interfaces';
export interface IAutoBetCriteria {
    value: number;
}
export interface IAutoBetCommand {
    name: string;
    value: number;
}
export interface IAutoBetSettings {
    originalBetAmount: number;
    currentBetAmount: number;
    stopCounter: IAutoBetCriteria;
    stopProfit: IAutoBetCriteria;
    stopLoss: IAutoBetCriteria;
    onWin: IAutoBetCommand;
    onLoss: IAutoBetCommand;
}
export interface IAutoBetTracker {
    isLastBetAWin: boolean;
    numOfBets: number;
    totalProfitOrLoss: number;
}
export interface IAutoBetResult {
    isWin: boolean;
    id: string;
    profit: number;
}
export interface IAddToBalanceAmounts {
    main: number;
    bet: number;
}
export interface IStateForBetForm {
    setListener(listener: IAppStateListener): void;
}
export interface IWalletServiceForBetFormBase {
    readonly isLoggedIn: boolean;
    readonly betBalance: string;
}
export interface ISettingsForBetFormBase {
    readonly selectedToken: IBettingToken;
    readonly selectedTokenBalance: string;
    readonly translation: ITranslationBase;
    readonly wallet: IWalletServiceForBetFormBase;
    setListener(listener: IAppSettingsListener): void;
}
export interface IBetFormContextBase {
    readonly settings: ISettingsForBetFormBase;
    readonly bankroll: IBankrollService;
    readonly state: IStateForBetForm;
}
export interface IGameComponent {
    readonly isJackpotBet: boolean;
    readonly state: IAppState;
    readonly selectedToken: IBettingToken;
    readonly selectedTokenPrice: number;
    readonly isInsufficientFunds: boolean;
    getSeed(isAutoBet: boolean): string;
    trackBetWithGoogleAnalytics(gameName: string, betAmount: string, txnId: string, isAutoBet: boolean): void;
}
