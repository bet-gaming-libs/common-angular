export declare class EosErrorParser {
    private _message;
    constructor(data: any);
    readonly isIncorrectNonce: boolean;
    readonly message: string;
}
