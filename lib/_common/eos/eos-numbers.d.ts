export interface IEOSAssetAmount {
    symbol: string;
    contract: string;
    integer: number;
    quantity: string;
    precision: number;
}
