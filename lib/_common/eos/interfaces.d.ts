import { IEosConfigParameters, IEasyAccountBetBalanceRow, IEasyAccountInfoRow, IEasyAccountCurrencyBalanceRow } from 'earnbet-common';
import { IEosAccountNames } from '../models/business-rules/interfaces';
export interface IEosService {
    readonly network: {
        blockchain: string;
        host: string;
        port: number;
        protocol: string;
        chainId: string;
    };
    httpsHost: string;
    readonly config: IEosConfigParameters;
    readonly accounts: IEosAccountNames;
    getTokenBalance(tokenContract: string, tokenHolderAccount: string, tokenSymbol: string, table?: string): Promise<string>;
    getEasyAccountInfoRow(easyAccountId: string): Promise<IEasyAccountInfoRow[]>;
    getEasyAccountCurrencyBalanceRow(tokenSymbol: string, easyAccountId: string): Promise<IEasyAccountCurrencyBalanceRow[]>;
    getEasyAccountBetBalanceRow(easyAccountId: string): Promise<IEasyAccountBetBalanceRow[]>;
}
