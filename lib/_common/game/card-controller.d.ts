import { ICardData } from "earnbet-common";
import { ICardController } from "./interfaces";
import { ISoundPlayer } from '../media/interfaces';
export declare class CardController {
    private soundPlayer;
    private _dealt;
    private _flipped;
    private _isRevealed;
    private _suit;
    private _rank;
    private _rankNumber;
    private _cardId;
    constructor(soundPlayer: ISoundPlayer);
    deal(): void;
    reveal(data: ICardData, rankNumber?: number, cardId?: number): void;
    flip(): void;
    unflip(): void;
    dealAndFlip(): void;
    readonly dealt: boolean;
    readonly flipped: boolean;
    readonly suit: string;
    readonly card: string;
    readonly rank: string;
    readonly rankNumber: number;
    readonly cardId: number;
    readonly isRevealed: boolean;
    readonly data: ICardController;
}
