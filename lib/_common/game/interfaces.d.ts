export interface ICardController {
    suit: string;
    dealt: boolean;
    card: string;
    flipped: boolean;
}
