import { Big } from 'big.js';
export declare class IntBasedNum {
    readonly precision: number;
    static fromInteger(integer: string, precision: number): IntBasedNum;
    readonly factor: number;
    private _integer;
    private _reciprocal;
    constructor(precision: number, decimalValue?: number | string, bigDecimal?: Big);
    /*** MUTATING METHODS ***/
    addAndReplace(other: IntBasedNum): void;
    subtractAndReplace(other: IntBasedNum): this;
    /***/
    multiply(other: IntBasedNum): IntBasedNum;
    multiplyDecimal(decimal: number): IntBasedNum;
    divideByDecimal(decimal: number): IntBasedNum;
    readonly reciprocal: IntBasedNum;
    readonly opposite_: IntBasedNum;
    readonly decimalAsNumber: number;
    readonly integer: Big;
    toString(): string;
    readonly decimalWithoutTrailingZeros: string;
    readonly decimal: string;
}
export declare class AssetAmount extends IntBasedNum {
    private _symbol;
    private _isZero;
    constructor(data: string);
    readonly isZero: boolean;
    readonly symbol: string;
}
