export interface ISoundPlayer {
    play(soundName: string): void;
}
