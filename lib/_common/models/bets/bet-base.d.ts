import { IResolvedBet } from "earnbet-common";
import { AssetAmount } from "../../math/integer-math";
export declare abstract class BetBase {
    readonly id: string;
    readonly time: Date;
    readonly bettor: string;
    readonly amount: string;
    readonly tokenSymbol: string;
    readonly betAmount: AssetAmount;
    readonly payout: string;
    readonly jackpotSpin: string;
    readonly isJackpotBet: boolean;
    readonly stakeWeight: number;
    readonly stakingLevelName: string;
    constructor(data: IResolvedBet);
}
