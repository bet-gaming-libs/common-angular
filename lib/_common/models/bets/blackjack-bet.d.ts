import { IResolvedBlackjackBet } from 'earnbet-common';
import { BetBase } from "./bet-base";
import { IBetResult } from "./interfaces";
export declare class BlackjackBetResult extends BetBase implements IBetResult {
    readonly game: string;
    readonly description: string;
    readonly isWin: boolean;
    readonly payout: string;
    readonly profit: number;
    constructor(data: IResolvedBlackjackBet);
}
