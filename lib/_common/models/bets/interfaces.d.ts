export interface IBetResult {
    id: string;
    game: string;
    time: Date;
    bettor: string;
    description: string;
    tokenSymbol: string;
    amount: string;
    isWin: boolean;
    payout: string;
    profit: number;
    jackpotSpin: string;
    isJackpotBet: boolean;
    betTxnId?: string;
    resolveTxnId?: string;
    chainId?: number;
}
export interface IDiceBetResult extends IBetResult {
}
export interface ICrashBetResult extends IBetResult {
}
export interface IBaccaratBetResult extends IBetResult {
}
export interface IBetsListener {
    onMyBetResolved(bet: IBetResult): Promise<void>;
}
export interface IResolvedBets {
    setListener(listener: IBetsListener): void;
    onBetPlaced(txnId: string): string;
    setMyBetId(id: string): void;
    onBetResolved(newBet: IBetResult): Promise<void>;
}
