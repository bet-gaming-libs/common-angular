import { EOSAssetAmount } from "earnbet-common";
export interface IEosAccountNames {
    eosbetcasino: string;
    burnedTokens: string;
    bankroll: string;
    easyAccount: string;
    jackpot: string;
    betToken: string;
    withdraw: string;
    tokens: string;
    games: {
        dice: string;
        crash: string;
        baccarat: string;
        hilo: string;
        blackjack: string;
    };
}
export interface ICurrencyTokenData {
    symbol: string;
    contract: string;
    precision: number;
}
export interface ICurrencyToken extends ICurrencyTokenData {
    getAssetAmount(value: string): EOSAssetAmount;
}
export interface IBettingTokenData extends ICurrencyTokenData {
    isNativeEosToken: boolean;
    minimumBetAmount: string;
    maximumWinAmount: number;
    jackpot: {
        minimumBet: number;
        ticketPrice: string;
    };
    betTokenAirDropRate: number;
    highRollerAmount: number;
    universalDepositAddress: string;
    pendingDepositTimeInMinutes: number;
    minimumWithdrawAmount: number;
    supportMemoForWithdraw: boolean;
}
export interface IBettingToken extends ICurrencyToken, IBettingTokenData {
    readonly jackpotTicketPrice: EOSAssetAmount;
    readonly chips: number[];
}
