export declare enum TokenSymbol {
    BTC = "BTC",
    ETH = "ETH",
    EOS = "EOS",
    BET = "BET",
    LTC = "LTC",
    XRP = "XRP",
    BCH = "BCH",
    BNB = "BNB",
    WAX = "WAX",
    TRX = "TRX",
    LINK = "LINK",
    DAI = "DAI",
    USDC = "USDC",
    USDT = "USDT",
    FUN = "FUN"
}
