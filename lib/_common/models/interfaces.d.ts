import { ITranslationBase } from '../translations/interfaces';
import { IBettingToken } from './business-rules/interfaces';
import { IWalletServiceBase } from '../wallets/interfaces';
import { IResolvedBets } from './bets/interfaces';
export interface IAppSettingsListener {
    onBettingCurrencyChanged(): void;
}
export interface IAppSettings {
    readonly translation: ITranslationBase;
    readonly selectedToken: IBettingToken;
    readonly selectedTokenBalance: string;
    readonly wallet: IWalletServiceBase;
    setListener(listener: IAppSettingsListener): void;
    selectCurrency(symbol: string): void;
}
export interface IAppStateListener {
    onAuthenticated(): void;
}
export interface IAppState {
    readonly bets: IResolvedBets;
    readonly referrer: string;
    seedForRandomness: string;
    onAddToBalance(toggle: boolean, amt?: number | string): void;
    setListener(listener: IAppStateListener): void;
}
