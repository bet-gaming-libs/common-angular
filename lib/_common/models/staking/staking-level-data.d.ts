export interface IStakingLevelData {
    name: string;
    stakePoints: number;
    houseEdgeReduction: number;
    baccaratHouseEdge: number;
}
export declare const stakingLevels: IStakingLevelData[];
export declare function getHouseEdgeReductionAsDecimal(stakeWeight: number): number;
export declare function getStakingLevelName(stakeWeight: number): string;
export declare function getStakingLevelIndex(stakeWeight: number): number;
