import { IValidatedChatMessage } from 'earnbet-common';
import { IEosService } from '../eos/interfaces';
import { IWalletServiceBase } from '../wallets/interfaces';
import { IAppSettings, IAppState } from '../models/interfaces';
import { IBlackjackAppService } from '../../blackjack/services/interfaces';
export interface IEosAppContext {
    readonly appId: string;
    readonly eos: IEosService;
    readonly wallet: IWalletServiceBase;
    onAuthenticated(): void;
}
export interface IAlertService {
    error(message: string): void;
    success(message: string): void;
    clear(): void;
}
export interface IBankrollService {
    getMaxBet(payoutFactor: number): Promise<number>;
}
export interface ICurrencyService {
    getPrice(tokenSymbol: string): number;
}
export interface IMainAppContext extends IEosAppContext {
    readonly state: IAppState;
    readonly settings: IAppSettings;
    readonly alert: IAlertService;
    readonly bankroll: IBankrollService;
    readonly currency: ICurrencyService;
    readonly blackjack: IBlackjackAppService;
    scrollToTopOfPage(): void;
    openInsufficentFundsModal(): void;
}
export interface IChatAppContext {
    onChatMessage(data: IValidatedChatMessage, isNewMessage: boolean): void;
    getRecentMessages(): void;
}
export interface IChatComponent {
    scrollToBottom(): void;
}
export interface IGoogleAnalyticsService {
    addTransaction(txnId: string, game: string, currency: string, amountInUSD: string, isAutoBet: boolean, isJackpot: boolean): void;
}
