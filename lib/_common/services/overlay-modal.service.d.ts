import { Observable } from 'rxjs';
export declare class OverlayModalService {
    private subject;
    constructor();
    open(message: string): void;
    close(): void;
    getMessage(): Observable<string>;
}
