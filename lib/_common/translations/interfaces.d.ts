export interface ITranslationBase {
    theMinimumBetIs: string;
    theMaximumBetIs: string;
    tooltipDividends(betTokens: string, otherTokens: string): string;
    clickThisBoxToReceive1JackpotSpinForPrice(price: string): string;
}
