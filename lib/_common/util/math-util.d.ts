export declare function toTotalDigits(amount: number, digits: number, precision?: number): string;
export declare function numStringToTotalDigits(amount: string, digits: number, precision?: number): string;
export declare function roundDownWithPrecision(amount: number, precision: number): string;
export declare function roundDownWithoutTrailingZeros(amount: string, precision: number): string;
export declare function roundStringDownWithPrecision(amount: string, precision: number): string;
