import { ITransactionResult, ISignatureInfo } from 'earnbet-common';
import { IEOSAssetAmount } from '../eos/eos-numbers';
export interface IWalletTransferData {
    toAccount: string;
    amount: IEOSAssetAmount;
    memo: string;
}
export interface IWalletServiceBase {
    readonly isPasswordRequiredForLogin: boolean;
    readonly loginPrompt: string;
    readonly isLoggedIn: boolean;
    readonly hasLogout: boolean;
    readonly accountKeyForReferralTable: string;
    readonly myReferralCode: string;
    readonly canClaimDividends: boolean;
    readonly isEasyAccount: boolean;
    readonly accountName: string;
    readonly uniqueAccountName: string;
    readonly accountId: string;
    readonly publicKey: string;
    readonly canSafelySign: boolean;
    readonly betBalance: string;
    login(): Promise<void>;
    syncTokenBalances(): void;
    addToBalance(tokenSymbol: string, amount: number): void;
    transfer(data: IWalletTransferData[]): Promise<ITransactionResult>;
    sign(message: string, reason: string): Promise<ISignatureInfo>;
    logout(): void;
    isReferrerMe(referrer: string): boolean;
    getBalance(tokenSymbol: string): string;
}
