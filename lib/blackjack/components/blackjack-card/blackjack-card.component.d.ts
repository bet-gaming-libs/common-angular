import { ICardController } from '../../../_common/game/interfaces';
export declare class BlackjackCardComponentBase {
    card: ICardController;
    direction: string;
    history: boolean;
    constructor();
    ngOnInit(): void;
    readonly suit: string;
    readonly rank: string;
    readonly flipped: boolean;
    readonly dealt: boolean;
}
