import { IBlackjackResult } from '../main/game/hand-result';
export declare class BlackjackModalComponent {
    private _isClosed;
    private result;
    constructor();
    showResult(result: IBlackjackResult): Promise<void>;
    close(): void;
    readonly isOpen: boolean;
    readonly isWin: boolean;
    readonly isPush: boolean;
    readonly isBlackjack: boolean;
    readonly isLoss: boolean;
    readonly isBusted: boolean;
    readonly isInsuranceSuccess: boolean;
    readonly amountWon: number;
}
