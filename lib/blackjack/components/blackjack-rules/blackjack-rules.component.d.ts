import { IMainAppContext } from '../../../_common/services/interfaces';
import { OverlayModalService } from '../../../_common/services/overlay-modal.service';
import { ITranslationBase } from '../../../_common/translations/interfaces';
export declare class BlackjackRulesComponentBase {
    private app;
    readonly modal: OverlayModalService;
    constructor(app: IMainAppContext, modal: OverlayModalService);
    readonly translation: ITranslationBase;
}
