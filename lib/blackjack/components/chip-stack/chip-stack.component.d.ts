import { OnChanges, OnInit } from '@angular/core';
import { IMainAppContext } from '../../../_common/services/interfaces';
import { IAppSettings } from '../../../_common/models/interfaces';
export declare class BlackJackChipStackComponentBase implements OnChanges, OnInit {
    private app;
    isMax: boolean;
    amount: number;
    chipstack: any[];
    constructor(app: IMainAppContext);
    ngOnInit(): void;
    ngOnChanges(): void;
    makeChipStack(): void;
    readonly chips: number[];
    readonly settings: IAppSettings;
}
export interface IChipsInfo {
    isMax: boolean;
    amount: number;
}
