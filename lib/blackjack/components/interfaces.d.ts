import { IBlackjackResult } from './main/game/hand-result';
import { BlackjackGameController } from './main/game/blackjack-controller';
import { IGameComponent } from '../../_common/base/interfaces';
import { IAlertService } from '../../_common/services/interfaces';
import { IWalletServiceBase } from '../../_common/wallets/interfaces';
export interface IBlackjackModalComponent {
    showResult(results: IBlackjackResult): Promise<void>;
    close(): void;
}
export interface IBlackjackComponent extends IGameComponent {
    readonly game: BlackjackGameController;
    readonly gameResultModal: IBlackjackModalComponent;
    readonly alert: IAlertService;
    readonly wallet: IWalletServiceBase;
    getMaxBet(): Promise<number>;
}
