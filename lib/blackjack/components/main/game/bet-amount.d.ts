export declare class BetAmount {
    private _isMax;
    private _amount;
    constructor();
    reset(): void;
    add(value: number): void;
    double(): void;
    half(): void;
    subtract(value: number): void;
    setToMax(value: number): void;
    amount: number;
    readonly isMax: boolean;
}
