import { CardController } from '../../../../_common/game/card-controller';
import { BetAmount } from './bet-amount';
import { IBlackjackResult } from './hand-result';
import { ISoundPlayer } from '../../../../_common/media/interfaces';
declare class BlackjackHand {
    private soundPlayer;
    private _cards;
    private _numOfAces;
    private countWithoutAces;
    protected _isAllCardsKnown: boolean;
    private _isFirstCardAnAce;
    protected isSplit: boolean;
    constructor(soundPlayer: ISoundPlayer);
    reset(): void;
    hit(cardId: number): Promise<void>;
    protected dealCard(cardId: number, shouldReveal: boolean, revealImmediately?: boolean): Promise<void>;
    private addValueOfCard;
    readonly isBusted: boolean;
    readonly is21: boolean;
    readonly sumAsString: string;
    readonly sum: number;
    readonly countOfAces: number;
    readonly isSoftCount: boolean;
    readonly isBlackJack: boolean;
    readonly isFirstCardAnAce: boolean;
    readonly numOfAces: number;
    readonly numOfCards: number;
    readonly isAllCardsKnown: boolean;
    readonly cards: CardController[];
}
export declare class BlackjackDealerHand extends BlackjackHand {
    dealFirstCard(cardId: number): Promise<void>;
    dealAnonymousCard(): Promise<void>;
    revealSecondCard(cardId: number): Promise<void>;
    dealAdditionalCard(cardId: number): Promise<void>;
    private readonly shouldHit;
}
export declare class BlackjackPlayerHand extends BlackjackHand {
    private didDoubleDown;
    private _isWaitingForAction;
    readonly bet: BetAmount;
    private _boughtInsurance;
    constructor(soundPlayer: ISoundPlayer);
    reset(): void;
    dealFirstCard(cardId: number, betAmount: number): Promise<void>;
    dealSecondCard(cardId: number): Promise<void>;
    buyInsurance(): void;
    split(cards: number[], betAmount: number): Promise<void>;
    doubleDown(cardId: number): Promise<void>;
    hit(cardId: number, isDoubleDown?: boolean): Promise<void>;
    stand(): void;
    determineResult(dealer: BlackjackDealerHand): IBlackjackResult;
    readonly isAbleToDoubleDown: boolean;
    readonly isAbleToSplit: boolean;
    readonly isWaitingForAction: boolean;
    readonly boughtInsurance: boolean;
}
export {};
