export interface IBlackjackResult {
    isBlackjack: boolean;
    isWin: boolean;
    isPush: boolean;
    isLoss: boolean;
    isBusted: boolean;
    isInsuranceSuccess: boolean;
    value: number;
    amountWon: number;
}
declare abstract class HandResultBase {
    readonly value: number;
    readonly amountWon: number;
    constructor(value: number, betAmount: number);
    readonly isBlackjack: boolean;
    readonly isWin: boolean;
    readonly isPush: boolean;
    readonly isLoss: boolean;
    readonly isBusted: boolean;
    readonly isInsuranceSuccess: boolean;
}
export declare class BlackjackResult extends HandResultBase implements IBlackjackResult {
    constructor(betAmount: number);
    readonly isBlackjack: boolean;
    readonly isWin: boolean;
}
export declare class WinResult extends HandResultBase implements IBlackjackResult {
    constructor(betAmount: number);
    readonly isWin: boolean;
}
export declare class PushResult extends HandResultBase implements IBlackjackResult {
    constructor(betAmount: number);
    readonly isPush: boolean;
}
export declare class LossResult extends HandResultBase implements IBlackjackResult {
    constructor(betAmount: number);
    readonly isLoss: boolean;
}
export declare class BustedResult extends HandResultBase implements IBlackjackResult {
    constructor(betAmount: number);
    readonly isBusted: boolean;
    readonly isLoss: boolean;
}
export declare class InsuranceSuccessResult extends HandResultBase implements IBlackjackResult {
    constructor(betAmount: number);
    readonly isInsuranceSuccess: boolean;
}
export declare class CombinedHandsResult implements IBlackjackResult {
    readonly isInsuranceSuccess: boolean;
    readonly value: number;
    readonly amountWon: number;
    readonly isPush: boolean;
    readonly isBlackjack: boolean;
    readonly isWin: boolean;
    readonly isBusted: boolean;
    readonly isLoss: boolean;
    constructor(result1: IBlackjackResult, result2: IBlackjackResult);
}
export {};
