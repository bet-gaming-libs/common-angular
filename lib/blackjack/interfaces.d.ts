import { FactoryProvider } from '@angular/core';
export interface IBlackjackModuleServices {
    appService: FactoryProvider;
}
