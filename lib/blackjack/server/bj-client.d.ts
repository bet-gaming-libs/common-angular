/// <reference types="socket.io-client" />
import { SocketMessageManager, ISignatureInfo, IResolvedBlackjackBet, IPromise } from 'earnbet-common';
import { SocketClientBase } from 'earnbet-common-front-end';
import { IBlackjackClientListener } from '../services/interfaces';
import { IMainAppContext } from '../../_common/services/interfaces';
export declare class BlackjackClient extends SocketClientBase {
    private app;
    private _isConnected;
    private _messageManager;
    private listener;
    constructor(app: IMainAppContext);
    connect(): void;
    setListener(value: IBlackjackClientListener): void;
    protected onConnected(): void;
    /*** MESSAGE LISTENERS: ***/
    onResolveBetResult(data: IResolvedBlackjackBet): void;
    /***/
    protected onDisconnected(): void;
    readonly messageManager: BlackjackMessageManager;
    readonly isConnected: boolean;
}
export declare class BlackjackMessageManager extends SocketMessageManager {
    constructor(socket: SocketIOClient.Socket, client: BlackjackClient);
    sendGameCommand<T>(data: string, info: ISignatureInfo): IPromise<T>;
}
