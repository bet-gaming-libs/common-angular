import { EOSAssetAmount } from "earnbet-common";
export interface IBlackjackServer {
    readonly isWaitingForResponse: boolean;
    resumeExistingGame(): void;
    placeBet(betAmount: EOSAssetAmount): void;
    respondToInsurance(yes: boolean): Promise<boolean>;
    peekForBlackjack(): Promise<boolean>;
    split(): Promise<number[]>;
    hit(handIndex: number): Promise<number>;
    doubleDown(handIndex: number): Promise<number>;
    stand(): Promise<boolean>;
    getDealersSecondCard(): Promise<number>;
    getDealersAdditionalCards(): Promise<number[]>;
}
export interface IJGameBlackjackGameInfo {
    status: string;
    data: IRamGameBlackjack;
}
export interface IRamGameBlackjack {
    id: string;
    bettor: string;
    is_ezeos: number;
    deposit_amt: string;
    bet_amt: number;
    seed: string;
    bet_time: string;
}
export interface IJBlackjackRoundInfo {
    status: string;
    data: IRoundInfo[];
}
export interface IRoundInfo {
    round_id: number;
    action_id: number;
}
export interface IJBlackjackState {
    status: string;
    data: {
        blackjack: IBlackjackState;
        jackpot_spin: string;
    };
}
export interface IBlackjackState {
    activeHand: number;
    result: string[];
    playerCards: Array<string[]>;
    playerValue: CardValue[];
    dealerCards: string[];
    dealerValue: CardValue;
    wagers: string[];
    deposits: string[];
    initWager: string[];
    insuraceWagers: string[];
    isComplete: boolean;
    payout?: string[];
    netSettlement?: string;
    wagersTotal?: string;
}
export interface CardValue {
    val: number;
    isSoft: boolean;
}
