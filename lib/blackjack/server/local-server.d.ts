import { EOSAssetAmount } from "earnbet-common";
import { IBlackjackServer } from './interfaces';
import { IBlackjackComponent } from '../components/interfaces';
import { ISoundPlayer } from '../../_common/media/interfaces';
export declare class LocalBlackjackServer implements IBlackjackServer {
    private component;
    readonly isWaitingForResponse: boolean;
    private dealer;
    constructor(component: IBlackjackComponent, soundPlayer: ISoundPlayer);
    resumeExistingGame(): void;
    placeBet(betAmount: EOSAssetAmount): void;
    respondToInsurance(yes: boolean): Promise<boolean>;
    peekForBlackjack(): Promise<boolean>;
    getDealersSecondCard(): Promise<number>;
    split(): Promise<number[]>;
    doubleDown(): Promise<number>;
    hit(): Promise<number>;
    stand(): Promise<boolean>;
    getDealersAdditionalCards(): Promise<number[]>;
}
