import { BlackjackClient } from '../server/bj-client';
import { IMainAppContext } from '../../_common/services/interfaces';
import { IBlackjackAppService } from './interfaces';
export declare class BlackjackAppService implements IBlackjackAppService {
    readonly client: BlackjackClient;
    constructor(app: IMainAppContext);
    init(): void;
}
