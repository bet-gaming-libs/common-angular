import { BlackjackMessageManager } from '../server/bj-client';
export interface IBlackjackClientListener {
    onBlackjackClientConnected(): void;
}
export interface IBlackjackClient {
    readonly isConnected: boolean;
    readonly messageManager: BlackjackMessageManager;
    setListener(listener: IBlackjackClientListener): void;
}
export interface IBlackjackAppService {
    client: IBlackjackClient;
}
